<?php

namespace G2\Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_consultadisciplina")
 * @Entity
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\VwConsultaDisciplina")
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */

class VwConsultaDisciplina
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     */
    private $id_projetopedagogico;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     */
    private $id_disciplina;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=false, length=255)
     */
    private $st_tituloexibicao;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false)
     */
    private $nu_cargahoraria;

    /**
     * @var string $id_formatoarquivo
     * @Column(name="id_formatoarquivo", type="integer", nullable=false)
     */
    private $id_formatoarquivo;

    /**
     * @var string $st_formatoarquivo
     * @Column(name="st_formatoarquivo", type="string", nullable=false, length=30)
     */
    private $st_formatoarquivo;

    /**
     * @var string $id_disciplinaconteudo
     * @Column(name="id_disciplinaconteudo", type="integer", nullable=false)
     */
    private $id_disciplinaconteudo;

    /**
     * @var string $st_descricaoconteudo
     * @Column(name="st_descricaoconteudo", type="string", nullable=false, length=100)
     */
    private $st_descricaoconteudo;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;


    /**
     * @return integer id_projetopedagogico 
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_discipla
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return string st_tituloexibicao
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param st_tituloexibicao
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return Inteiro nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param nu_numero
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return String st_formatoarquivo
     */
    public function getSt_formatoarquivo()
    {
        return $this->st_formatoarquivo;
    }

    /**
     * @param st_formatoarquivo
     */
    public function setSt_formatoarquivo($st_formatoarquivo)
    {
        $this->st_formatoarquivo = $st_formatoarquivo;
        return $this;
    }


    /**
     * @return Inteiro id_formatoarquivo
     */
    public function getId_formatoarquivo()
    {
        return $this->id_formatoarquivo;
    }

    /**
     * @param id_formatoarquivo
     */
    public function setId_formatoarquivo($id_formatoarquivo)
    {
        $this->id_formatoarquivo = $id_formatoarquivo;
        return $this;
    }

    /**
     * @return Inteiro id_disciplinaconteudo
     */
    public function getId_disciplinaconteudo()
    {
        return $this->id_disciplinaconteudo;
    }

    /**
     * @param id_disciplinaconteudo
     */
    public function setId_disciplinaconteudo($id_disciplinaconteudo)
    {
        $this->id_disciplinaconteudo = $id_disciplinaconteudo;
    }

        /**
     * @return string st_descricaoconteudo
     */
    public function getSt_descricaoconteudo()
    {
        return $this->st_descricaoconteudo;
    }

    /**
     * @param string $st_descricaoconteudo
     */
    public function setSt_descricaoconteudo($st_descricaoconteudo)
    {
        $this->st_descricaoconteudo = $st_descricaoconteudo;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}
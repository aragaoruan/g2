<?php

namespace G2\Entity;

/**
 * Entity para vw_turmadisciplina
 * @Entity(repositoryClass="G2\Repository\VwTurmaDisciplina")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_turmadisciplina")
 */
class VwTurmaDisciplina
{

    /**
     * @var integer $id_turma
     * @Column(type="integer")
     * @Id
     */
    private $id_turma;

    /**
     * @var string $id_turma
     * @Column(type="string")
     */
    private $st_turma;

    /**
     * @var string $id_disciplina
     * @Column(type="string")
     */
    private $st_disciplina;

    /**
     * @var integer $id_turma
     * @Column(type="integer")
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $id_turma
     * @Column(type="integer")
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $id_evolucao
     * @Column(type="integer")
     */
    private $id_evolucao;

    /**
     * @var integer $id_situacao
     * @Column(type="integer")
     */
    private $id_situacao;
    /**
     * @var
     * @column(type="integer")
     */
    private $id_situacaodisciplina;
    /**
     * @return integer
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     *
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     *
     * @return integer
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     *
     * @return integer
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     *
     * @param integer $id_turma
     * @return \G2\Entity\VwTurmaDisciplina
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     *
     * @param integer $st_turma
     * @return \G2\Entity\VwTurmaDisciplina
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     *
     * @param integer $id_entidade
     * @return \G2\Entity\VwTurmaDisciplina
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     *
     * @param integer $id_disciplina
     * @return \G2\Entity\VwTurmaDisciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param int $id_evolucao
     * @return $this
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    /**
     * @return mixed
     */
    public function getId_situacaodisciplina()
    {
        return $this->id_situacaodisciplina;
    }

    /**
     * @param mixed $id_situacaodisciplina
     */
    public function setId_situacaodisciplina($id_situacaodisciplina)
    {
        $this->id_situacaodisciplina = $id_situacaodisciplina;
    }




}

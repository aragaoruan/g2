<?php

namespace G2\Entity;


use G2\G2Entity;


/**
 * @EntityView
 * @Entity(repositoryClass="\G2\Repository\Matricula")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alunogradeintegracaopos")
 */
class VwAlunoGradeIntegracaoPos extends G2Entity
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_alocacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_modulo;

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidadeintegracao;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaula;


    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var datetime2 $dt_abertura
     * @Column(name="dt_abertura", type="datetime2", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var datetime2 $dt_encerramentoportal
     * @Column(name="dt_encerramentoportal", type="datetime2", nullable=true, length=3)
     */
    private $dt_encerramentoportal;

    /**
     * @var datetime2 $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime2", nullable=true, length=8)
     */
    private $dt_encerramento;

    /**
     * @var datetime2 $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="datetime2", nullable=true, length=8)
     */
    private $dt_encerramentosala;

    /**
     * @var integer $nu_diasacesso
     * @Column(name="nu_diasacesso", type="integer", nullable=true, length=4)
     */
    private $nu_diasacesso;


    /**
     * @var integer $id_professor
     * @Column(name="id_professor", type="integer", nullable=true, length=4)
     */
    private $id_professor;


    /**
     * @var integer $id_status
     * @Column(name="id_status", type="integer", nullable=false, length=4)
     */
    private $id_status;

    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer", nullable=true, length=4)
     */
    private $id_situacaotcc;

    /**
     * @var integer $id_aproparcial
     * @Column(name="id_aproparcial", type="integer", nullable=false, length=4)
     */
    private $id_aproparcial;

    /**
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=true, length=4)
     */
    private $id_tiposaladeaula;


    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true, length=4)
     */
    private $id_categoriasala;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadesala
     * @Column(name="id_entidadesala", type="integer", nullable=true, length=4)
     */
    private $id_entidadesala;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=true, length=1)
     */
    private $bl_encerrado;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_nomeexibicao
     * @Column(name="st_nomeexibicao", type="string", nullable=true, length=255)
     */
    private $st_nomeexibicao;

    /**
     * @var string $st_urlavatar
     * @Column(name="st_urlavatar", type="string", nullable=true, length=300)
     */
    private $st_urlavatar;

    /**
     * @var string $st_tituloexibicaoprojeto
     * @Column(name="st_tituloexibicaoprojeto", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaoprojeto;

    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(name="st_tituloexibicaomodulo", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaomodulo;

    /**
     * @var string $st_descricaodisciplina
     * @Column(name="st_descricaodisciplina", type="string", nullable=true)
     */
    private $st_descricaodisciplina;

    /**
     * @var string $st_integracao
     * @Column(name="st_integracao", type="string", nullable=false, length=1)
     */
    private $st_integracao;

    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=12)
     */
    private $st_status;

    /**
     * @var string $st_situacaotcc
     * @Column(name="st_situacaotcc", type="string", nullable=true, length=255)
     */
    private $st_situacaotcc;

    /**
     * @var string $st_codsistemaent
     * @Column(name="st_codsistemaent", type="string", nullable=true)
     */
    private $st_codsistemaent;

    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=true, length=300)
     */
    private $st_professor;

    /**
     * @var string $st_coordenador
     * @Column(name="st_coordenador", type="string", nullable=true, length=300)
     */
    private $st_coordenador;

    /**
     * @var string $st_caminho
     * @Column(name="st_caminho", type="string", nullable=true)
     */
    private $st_caminho;

    /**
     * @var string $st_imagemdisciplina
     * @Column(name="st_imagemdisciplina", type="string", nullable=true, length=500)
     */
    private $st_imagemdisciplina;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=true)
     */
    private $st_codusuario;

    /**
     * @var string $st_loginintegrado
     * @Column(name="st_loginintegrado", type="string", nullable=true)
     */
    private $st_loginintegrado;

    /**
     * @var string $st_senhaintegrada
     * @Column(name="st_senhaintegrada", type="string", nullable=true)
     */
    private $st_senhaintegrada;

    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=true, length=30)
     */
    private $st_codsistemacurso;

    /**
     * @var string $st_codalocacao
     * @Column(name="st_codalocacao", type="string", nullable=true)
     */
    private $st_codalocacao;

    /**
     * @var integer $nu_ordenacao
     * @Column(name="nu_ordenacao", type="integer", nullable=true, length=1)
     */
    private $nu_ordenacao;

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_alocacao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param int $id_modulo
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param date $dt_abertura
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_encerramentoportal()
    {
        return $this->dt_encerramentoportal;
    }

    /**
     * @param date $dt_encerramentoportal
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setDt_encerramentoportal($dt_encerramentoportal)
    {
        $this->dt_encerramentoportal = $dt_encerramentoportal;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param datetime2 $dt_encerramento
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    /**
     * @param datetime2 $dt_encerramentosala
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setDt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    /**
     * @param int $nu_diasacesso
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @param int $id_professor
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_professor($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param int $id_status
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @param int $id_situacaotcc
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_aproparcial()
    {
        return $this->id_aproparcial;
    }

    /**
     * @param int $id_aproparcial
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_aproparcial($id_aproparcial)
    {
        $this->id_aproparcial = $id_aproparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tiposaladeaula()
    {
        return $this->id_tiposaladeaula;
    }

    /**
     * @param int $id_tiposaladeaula
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_tiposaladeaula($id_tiposaladeaula)
    {
        $this->id_tiposaladeaula = $id_tiposaladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param int $id_categoriasala
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadesala()
    {
        return $this->id_entidadesala;
    }

    /**
     * @param int $id_entidadesala
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_entidadesala($id_entidadesala)
    {
        $this->id_entidadesala = $id_entidadesala;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bool $bl_encerrado
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    /**
     * @param string $st_nomeexibicao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param string $st_urlavatar
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicaoprojeto()
    {
        return $this->st_tituloexibicaoprojeto;
    }

    /**
     * @param string $st_tituloexibicaoprojeto
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_tituloexibicaoprojeto($st_tituloexibicaoprojeto)
    {
        $this->st_tituloexibicaoprojeto = $st_tituloexibicaoprojeto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @param string $st_tituloexibicaomodulo
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricaodisciplina()
    {
        return $this->st_descricaodisciplina;
    }

    /**
     * @param string $st_descricaodisciplina
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_descricaodisciplina($st_descricaodisciplina)
    {
        $this->st_descricaodisciplina = $st_descricaodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_integracao()
    {
        return $this->st_integracao;
    }

    /**
     * @param string $st_integracao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_integracao($st_integracao)
    {
        $this->st_integracao = $st_integracao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }

    /**
     * @param string $st_status
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_situacaotcc()
    {
        return $this->st_situacaotcc;
    }

    /**
     * @param string $st_situacaotcc
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_situacaotcc($st_situacaotcc)
    {
        $this->st_situacaotcc = $st_situacaotcc;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistemaent()
    {
        return $this->st_codsistemaent;
    }

    /**
     * @param string $st_codsistemaent
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_codsistemaent($st_codsistemaent)
    {
        $this->st_codsistemaent = $st_codsistemaent;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_professor()
    {
        return $this->st_professor;
    }

    /**
     * @param string $st_professor
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_coordenador()
    {
        return $this->st_coordenador;
    }

    /**
     * @param string $st_coordenador
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_caminho()
    {
        return $this->st_caminho;
    }

    /**
     * @param string $st_caminho
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_caminho($st_caminho)
    {
        $this->st_caminho = $st_caminho;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_imagemdisciplina()
    {
        return $this->st_imagemdisciplina;
    }

    /**
     * @param string $st_imagemdisciplina
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_imagemdisciplina($st_imagemdisciplina)
    {
        $this->st_imagemdisciplina = $st_imagemdisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param string $st_codusuario
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_loginintegrado()
    {
        return $this->st_loginintegrado;
    }

    /**
     * @param string $st_loginintegrado
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_loginintegrado($st_loginintegrado)
    {
        $this->st_loginintegrado = $st_loginintegrado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_senhaintegrada()
    {
        return $this->st_senhaintegrada;
    }

    /**
     * @param string $st_senhaintegrada
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_senhaintegrada($st_senhaintegrada)
    {
        $this->st_senhaintegrada = $st_senhaintegrada;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param string $st_codsistemacurso
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codalocacao()
    {
        return $this->st_codalocacao;
    }

    /**
     * @param string $st_codalocacao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setSt_codalocacao($st_codalocacao)
    {
        $this->st_codalocacao = $st_codalocacao;
        return $this;
    }

    /**
     * @return integer
     */
    public function getNu_ordenacao()
    {
        return $this->nu_ordenacao;
    }

    /**
     * @param integer $nu_ordenacao
     * @return VwAlunoGradeIntegracaoPos
     */
    public function setNu_ordenacao($nu_ordenacao)
    {
        $this->nu_ordenacao = $nu_ordenacao;
        return $this;
    }
}

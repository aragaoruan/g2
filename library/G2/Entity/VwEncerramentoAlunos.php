<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @Entity()
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_encerramentoalunos")
 */
class VwEncerramentoAlunos extends G2Entity
{

    /**
     * @Id
     * @Column(name="id_matricula", type="integer")
     */
    private $id_matricula;

    /**
     * @Column(name="id_saladeaula", type="integer")
     */
    private $id_saladeaula;

    /**
     * @Column(name="id_disciplina", type="integer")
     */
    private $id_disciplina;

    /**
     * @Column(name="id_usuariocoordenador", type="integer")
     */
    private $id_usuariocoordenador;

    /**
     * @Column(name="id_usuariopedagogico", type="integer")
     */
    private $id_usuariopedagogico;

    /**
     * @Column(name="id_usuarioprofessor", type="integer")
     */
    private $id_usuarioprofessor;

    /**
     * @Column(name="id_usuariofinanceiro", type="integer")
     */
    private $id_usuariofinanceiro;

    /**
     * @Column(name="dt_encerramentoprofessor", type="datetime2")
     */
    private $dt_encerramentoprofessor;

    /**
     * @Column(name="dt_encerramentocoordenador", type="datetime2")
     */
    private $dt_encerramentocoordenador;

    /**
     * @Column(name="dt_encerramentopedagogico", type="datetime2")
     */
    private $dt_encerramentopedagogico;

    /**
     * @Column(name="dt_encerramentofinanceiro", type="datetime2")
     */
    private $dt_encerramentofinanceiro;

    /**
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @Column(name="id_tipodisciplina", type="integer")
     */
    private $id_tipodisciplina;

    /**
     * @Column(name="id_alocacao", type="integer")
     */
    public $id_alocacao;

    /**
     * @Column(name="id_usuario", type="integer")
     */
    public $id_usuario;

    /**
     * @Column(name="id_matriculadisciplina", type="integer")
     */
    public $id_matriculadisciplina;

    /**
     * @Column(name="st_nota", type="string")
     */
    public $st_nota;

    /**
     * @Column(name="st_nomecompleto", type="string")
     */
    public $st_nomecompleto;

    /**
     * @Column(name="id_logacesso", type="integer")
     */
    public $id_logacesso;

    /**
     * @Column(name="id_sistema", type="integer")
     */
    public $id_sistema;

    /**
     * @Column(name="nu_perfilalunoencerrado", type="integer")
     */
    public $nu_perfilalunoencerrado;

    /**
     * @Column(name="id_upload", type="integer")
     */
    public $id_upload;

    /**
     * @Column(name="nu_cargahoraria", type="integer")
     */
    public $nu_cargahoraria;

    /**
     * @Column(name="dt_recusa", type="datetime2")
     */
    public $dt_recusa;

    /**
     * @Column(name="st_motivorecusa", type="string")
     */
    public $st_motivorecusa;

    /**
     * @Column(name="id_tiponota", type="integer")
     */
    public $id_tiponota;

    /**
     * @return mixed
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param mixed $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param mixed $id_saladeaula
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param mixed $id_disciplina
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuariocoordenador()
    {
        return $this->id_usuariocoordenador;
    }

    /**
     * @param mixed $id_usuariocoordenador
     */
    public function setid_usuariocoordenador($id_usuariocoordenador)
    {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuariopedagogico()
    {
        return $this->id_usuariopedagogico;
    }

    /**
     * @param mixed $id_usuariopedagogico
     */
    public function setid_usuariopedagogico($id_usuariopedagogico)
    {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuarioprofessor()
    {
        return $this->id_usuarioprofessor;
    }

    /**
     * @param mixed $id_usuarioprofessor
     */
    public function setid_usuarioprofessor($id_usuarioprofessor)
    {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuariofinanceiro()
    {
        return $this->id_usuariofinanceiro;
    }

    /**
     * @param mixed $id_usuariofinanceiro
     */
    public function setid_usuariofinanceiro($id_usuariofinanceiro)
    {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_encerramentoprofessor()
    {
        return $this->dt_encerramentoprofessor;
    }

    /**
     * @param mixed $dt_encerramentoprofessor
     */
    public function setdt_encerramentoprofessor($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_encerramentocoordenador()
    {
        return $this->dt_encerramentocoordenador;
    }

    /**
     * @param mixed $dt_encerramentocoordenador
     */
    public function setdt_encerramentocoordenador($dt_encerramentocoordenador)
    {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_encerramentopedagogico()
    {
        return $this->dt_encerramentopedagogico;
    }

    /**
     * @param mixed $dt_encerramentopedagogico
     */
    public function setdt_encerramentopedagogico($dt_encerramentopedagogico)
    {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_encerramentofinanceiro()
    {
        return $this->dt_encerramentofinanceiro;
    }

    /**
     * @param mixed $dt_encerramentofinanceiro
     */
    public function setdt_encerramentofinanceiro($dt_encerramentofinanceiro)
    {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param mixed $id_tipodisciplina
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param mixed $id_alocacao
     */
    public function setid_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param mixed $id_matriculadisciplina
     */
    public function setid_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nota()
    {
        return $this->st_nota;
    }

    /**
     * @param mixed $st_nota
     */
    public function setst_nota($st_nota)
    {
        $this->st_nota = $st_nota;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param mixed $st_nomecompleto
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_logacesso()
    {
        return $this->id_logacesso;
    }

    /**
     * @param mixed $id_logacesso
     */
    public function setid_logacesso($id_logacesso)
    {
        $this->id_logacesso = $id_logacesso;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param mixed $id_sistema
     */
    public function setid_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getnu_perfilalunoencerrado()
    {
        return $this->nu_perfilalunoencerrado;
    }

    /**
     * @param mixed $nu_perfilalunoencerrado
     */
    public function setnu_perfilalunoencerrado($nu_perfilalunoencerrado)
    {
        $this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param mixed $id_upload
     */
    public function setid_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getnu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param mixed $nu_cargahoraria
     */
    public function setnu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_recusa()
    {
        return $this->dt_recusa;
    }

    /**
     * @param mixed $dt_recusa
     */
    public function setdt_recusa($dt_recusa)
    {
        $this->dt_recusa = $dt_recusa;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_motivorecusa()
    {
        return $this->st_motivorecusa;
    }

    /**
     * @param mixed $st_motivorecusa
     */
    public function setst_motivorecusa($st_motivorecusa)
    {
        $this->st_motivorecusa = $st_motivorecusa;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_tiponota()
    {
        return $this->id_tiponota;
    }

    /**
     * @param mixed $id_tiponota
     */
    public function setid_tiponota($id_tiponota)
    {
        $this->id_tiponota = $id_tiponota;
        return $this;

    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_assuntoco")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwAssuntoCo
{

    /**
     *
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_assuntoco;

    /**
     *
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200) 
     */
    private $st_assuntoco;

    /**
     *
     * @var boolean $bl_abertura
     * @Column(name="bl_abertura", type="boolean", nullable=false) 
     */
    private $bl_abertura;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false) 
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_assuntocopai
     * @Column(name="id_assuntocopai", type="integer", nullable=true) 
     */
    private $id_assuntocopai;

    /**
     *
     * @var string $st_assuntocopai
     * @Column(name="st_assuntocopai", type="string", nullable=true, length=200) 
     */
    private $st_assuntocopai;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=200) 
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=false) 
     */
    private $id_tipoocorrencia;

    /**
     *
     * @var string $st_tipoocorrencia
     * @Column(name="st_tipoocorrencia", type="string", nullable=false, length=200) 
     */
    private $st_tipoocorrencia;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true) 
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false) 
     */
    private $id_entidadecadastro;

    /**
     *
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=true) 
     */
    private $id_funcao;

    /**
     *
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=true) 
     */
    private $id_textosistema;

    /**
     *
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=true) 
     */
    private $id_nucleoco;

    public function getId_assuntoco ()
    {
        return $this->id_assuntoco;
    }

    public function setId_assuntoco ($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    public function getSt_assuntoco ()
    {
        return $this->st_assuntoco;
    }

    public function setSt_assuntoco ($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
        return $this;
    }

    public function getBl_abertura ()
    {
        return $this->bl_abertura;
    }

    public function setBl_abertura ($bl_abertura)
    {
        $this->bl_abertura = $bl_abertura;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_assuntocopai ()
    {
        return $this->id_assuntocopai;
    }

    public function setId_assuntocopai ($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
        return $this;
    }

    public function getSt_assuntocopai ()
    {
        return $this->st_assuntocopai;
    }

    public function setSt_assuntocopai ($st_assuntocopai)
    {
        $this->st_assuntocopai = $st_assuntocopai;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_tipoocorrencia ()
    {
        return $this->id_tipoocorrencia;
    }

    public function setId_tipoocorrencia ($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function getSt_tipoocorrencia ()
    {
        return $this->st_tipoocorrencia;
    }

    public function setSt_tipoocorrencia ($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getId_funcao ()
    {
        return $this->id_funcao;
    }

    public function setId_funcao ($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema ($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getId_nucleoco ()
    {
        return $this->id_nucleoco;
    }

    public function setId_nucleoco ($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
        return $this;
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_etiquetausuario")
 * @Entity
 * @EntityView
 * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
 */
class VwEtiquetaUsuario extends G2Entity
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", length=200)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     * @Id
     */
    private $id_matricula;

    /**
     *
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string")
     */
    private $st_endereco;

    /**
     *
     * @var string $st_numero
     * @Column(name="st_numero", type="string", nullable=true, length=200)
     */
    private $st_numero;

    /**
     *
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", length=200)
     */
    private $st_bairro;

    /**
     *
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", length=255)
     */
    private $st_cidade;

    /**
     *
     * @var string $st_uf
     * @Column(name="st_uf", type="string")
     */
    private $st_uf;

    /**
     *
     * @var string $st_cep
     * @Column(name="st_cep", type="string")
     */
    private $st_cep;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string")
     */
    private $st_complemento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2")
     */
    private $dt_solicitacao;

    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2")
     */
    private $dt_entrega;

    /**
     * @var string $st_servico
     * @Column(name="st_servico", type="string")
     */
    private $st_servico;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string")
     */
    private $st_email;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return string
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @return string
     */
    public function getSt_numero()
    {
        return $this->st_numero;
    }

    /**
     * @param string $st_numero
     */
    public function setSt_numero($st_numero)
    {
        $this->st_numero = $st_numero;
    }

    /**
     * @return string
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param string $st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @return string
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param string $st_cidade
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
    }

    /**
     * @return string
     */
    public function getSt_uf()
    {
        return $this->st_uf;
    }

    /**
     * @param string $st_uf
     */
    public function setSt_uf($st_uf)
    {
        $this->st_uf = $st_uf;
    }

    /**
     * @return string
     */
    public function getSt_cep()
    {
        return $this->st_cep;
    }

    /**
     * @param string $st_cep
     */
    public function setSt_cep($st_cep)
    {
        $this->st_cep = $st_cep;
    }

    /**
     * @return string
     */
    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param string $st_complemento
     */
    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return datetime2
     */
    public function getDt_solicitacao()
    {
        return $this->dt_solicitacao;
    }

    /**
     * @param datetime2 $dt_solicitacao
     */
    public function setDt_solicitacao($dt_solicitacao)
    {
        $this->dt_solicitacao = $dt_solicitacao;
    }

    /**
     * @return datetime2
     */
    public function getDt_entrega()
    {
        return $this->dt_entrega;
    }

    /**
     * @param datetime2 $dt_entrega
     */
    public function setDt_entrega($dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
    }

    /**
     * @return string
     */
    public function getSt_servico()
    {
        return $this->st_servico;
    }

    /**
     * @param string $st_servico
     */
    public function setSt_servico($st_servico)
    {
        $this->st_servico = $st_servico;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }




}


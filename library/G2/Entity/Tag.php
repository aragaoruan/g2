<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tag")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Tag
{

    /**
     * @var integer $id_tag
     * @Column(type="integer", nullable=false, name="id_tag")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tag;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_tag
     * @Column(type="string", length=100, nullable=false, name="st_tag")
     */
    private $st_tag;

    /**
     * @var datetime2 $dt_cadastro 
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @return integer $this->id_tag
     */
    public function getId_tag ()
    {
        return $this->id_tag;
    }

    /**
     * @param integer $id_tag
     * @return this
     */
    public function setId_tag ($id_tag)
    {
        $this->id_tag = $id_tag;
        return $this;
    }

    /**
     * @return $this->id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * 
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return this
     */
    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string $this->st_tag
     */
    public function getSt_tag ()
    {
        return $this->st_tag;
    }

    /**
     * @param string $st_tag
     * @return this
     */
    public function setSt_tag ($st_tag)
    {
        $this->st_tag = $st_tag;
        return $this;
    }

    /**
     * @return string $this->dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return this
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_setor")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwSetor
{

    /**
     *
     * @var integer $id_setor
     * @Column(name="id_setor", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_setor;

    /**
     *
     * @var string $st_setor
     * @Column(name="st_setor", type="string", nullable=false, length=150) 
     */
    private $st_setor;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false) 
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false) 
     */
    private $id_entidadecadastro;

    /**
     *
     * @var string $st_nomeentidadecadastro
     * @Column(name="st_nomeentidadecadastro", type="string", nullable=true, length=2500) 
     */
    private $st_nomeentidadecadastro;

    /**
     *
     * @var integer $id_funcaocadastro
     * @Column(name="id_funcaocadastro", type="integer", nullable=false) 
     */
    private $id_funcaocadastro;

    /**
     *
     * @var string $st_funcaocadastro
     * @Column(name="st_funcaocadastro", type="string", nullable=true, length=100) 
     */
    private $st_funcaocadastro;

    /**
     *
     * @var integer $id_setorpai
     * @Column(name="id_setorpai", type="integer", nullable=true) 
     */
    private $id_setorpai;

    /**
     *
     * @var string $st_setorpai
     * @Column(name="st_setorpai", type="string", nullable=true, length=150) 
     */
    private $st_setorpai;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255) 
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false) 
     */
    private $id_usuariocadastro;

    /**
     *
     * @var string $st_usuariocadastro
     * @Column(name="st_usuariocadastro", type="string", nullable=true, length=300) 
     */
    private $st_usuariocadastro;

    /**
     *
     * @var string $st_localizacao
     * @Column(name="st_localizacao", type="string", nullable=false, length=150) 
     */
    private $st_localizacao;

    public function getId_setor ()
    {
        return $this->id_setor;
    }

    public function setId_setor ($id_setor)
    {
        $this->id_setor = $id_setor;
        return $this;
    }

    public function getSt_setor ()
    {
        return $this->st_setor;
    }

    public function setSt_setor ($st_setor)
    {
        $this->st_setor = $st_setor;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_nomeentidadecadastro ()
    {
        return $this->st_nomeentidadecadastro;
    }

    public function setSt_nomeentidadecadastro ($st_nomeentidadecadastro)
    {
        $this->st_nomeentidadecadastro = $st_nomeentidadecadastro;
        return $this;
    }

    public function getId_funcaocadastro ()
    {
        return $this->id_funcaocadastro;
    }

    public function setId_funcaocadastro ($id_funcaocadastro)
    {
        $this->id_funcaocadastro = $id_funcaocadastro;
        return $this;
    }

    public function getSt_funcaocadastro ()
    {
        return $this->st_funcaocadastro;
    }

    public function setSt_funcaocadastro ($st_funcaocadastro)
    {
        $this->st_funcaocadastro = $st_funcaocadastro;
        return $this;
    }

    public function getId_setorpai ()
    {
        return $this->id_setorpai;
    }

    public function setId_setorpai ($id_setorpai)
    {
        $this->id_setorpai = $id_setorpai;
        return $this;
    }

    public function getSt_setorpai ()
    {
        return $this->st_setorpai;
    }

    public function setSt_setorpai ($st_setorpai)
    {
        $this->st_setorpai = $st_setorpai;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getSt_usuariocadastro ()
    {
        return $this->st_usuariocadastro;
    }

    public function setSt_usuariocadastro ($st_usuariocadastro)
    {
        $this->st_usuariocadastro = $st_usuariocadastro;
        return $this;
    }

    public function getSt_localizacao ()
    {
        return $this->st_localizacao;
    }

    public function setSt_localizacao ($st_localizacao)
    {
        $this->st_localizacao = $st_localizacao;
        return $this;
    }

}
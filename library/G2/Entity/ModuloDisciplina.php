<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_modulodisciplina")
 * @Entity(repositoryClass="\G2\Repository\ModuloDisciplina")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ModuloDisciplina
{

    /**
     * @var integer $id_modulodisciplina
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_modulodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_modulodisciplina;

    /**
     * @var Modulo $id_modulo
     * @ManyToOne(targetEntity="Modulo")
     * @JoinColumn(name="id_modulo", nullable=false, referencedColumnName="id_modulo")
     */
    private $id_modulo;

    /**
     * @var Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", nullable=false, referencedColumnName="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @var Serie $id_serie
     * @ManyToOne(targetEntity="Serie")
     * @JoinColumn(name="id_serie", nullable=false, referencedColumnName="id_serie")
     */
    private $id_serie;

    /**
     * @var NivelEnsino $id_nivelensino
     * @ManyToOne(targetEntity="NivelEnsino")
     * @JoinColumn(name="id_nivelensino", nullable=false, referencedColumnName="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     * @var AreaConhecimento $id_areaconhecimento
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", nullable=true, referencedColumnName="id_areaconhecimento")
     */
    private $id_areaconhecimento;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true, length=4)
     */
    private $nu_ordem;

    /**
     * @var Usuario $id_usuarioponderacao
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioponderacao", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuarioponderacao;

    /**
     * @var integer $nu_importancia
     * @Column(name="nu_importancia", type="integer", nullable=true, length=4)
     */
    private $nu_importancia;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_obrigatoria
     * @Column(name="bl_obrigatoria", type="boolean", nullable=false, length=1)
     */
    private $bl_obrigatoria;

    /**
     * @var decimal $nu_ponderacaocalculada
     * @Column(name="nu_ponderacaocalculada", type="decimal", nullable=true, length=5)
     */
    private $nu_ponderacaocalculada;

    /**
     * @var decimal $nu_ponderacaoaplicada
     * @Column(name="nu_ponderacaoaplicada", type="decimal", nullable=true, length=5)
     */
    private $nu_ponderacaoaplicada;

    /**
     * @var datetime2 $dt_cadastroponderacao
     * @Column(name="dt_cadastroponderacao", type="datetime2", nullable=true)
     */
    private $dt_cadastroponderacao;


    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true, length=4)
     */
    private $nu_cargahoraria;


    /**
     * @var integer $nu_obrigatorioalocacao
     * @Column(name="nu_obrigatorioalocacao", type="integer", nullable=true, length=4)
     */
    private $nu_obrigatorioalocacao;


    /**
     * @var integer $nu_disponivelapartirdo
     * @Column(name="nu_disponivelapartirdo", type="integer", nullable=true, length=4)
     */
    private $nu_disponivelapartirdo;

    /**
     * @var integer $nu_percentualsemestre
     * @Column(name="nu_percentualsemestre" ,type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_percentualsemestre;

    /**
     * @return integer
     */
    public function getId_modulodisciplina()
    {
        return $this->id_modulodisciplina;
    }

    /**
     * @param integer $id_modulodisciplina
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_modulodisciplina($id_modulodisciplina)
    {
        $this->id_modulodisciplina = $id_modulodisciplina;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\Modulo
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     *
     * @param \G2\Entity\Modulo $id_modulo
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_modulo(Modulo $id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\Disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     *
     * @param \G2\Entity\Disciplina $id_disciplina
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_disciplina(Disciplina $id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\Serie
     */
    public function getId_serie()
    {
        return $this->id_serie;
    }

    /**
     *
     * @param \G2\Entity\Serie $id_serie
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_serie(Serie $id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\NivelEnsino
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     *
     * @param \G2\Entity\NivelEnsino $id_nivelensino
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_nivelensino(NivelEnsino $id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\AreaConhecimento
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     *
     * @param \G2\Entity\AreaConhecimento $id_areaconhecimento
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_areaconhecimento(AreaConhecimento $id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     *
     * @param integer $nu_ordem
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     *
     * @return \G2\Entity\Usuario
     */
    public function getId_usuarioponderacao()
    {
        return $this->id_usuarioponderacao;
    }

    /**
     *
     * @param \G2\Entity\Usuario $id_usuarioponderacao
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setId_usuarioponderacao(Usuario $id_usuarioponderacao)
    {
        $this->id_usuarioponderacao = $id_usuarioponderacao;
        return $this;
    }

    /**
     *
     * @return integer
     */
    public function getNu_importancia()
    {
        return $this->nu_importancia;
    }

    /**
     *
     * @param integer $nu_importancia
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setNu_importancia($nu_importancia)
    {
        $this->nu_importancia = $nu_importancia;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     *
     * @param boolean $bl_ativo
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     *
     * @return boolean
     */
    public function getBl_obrigatoria()
    {
        return $this->bl_obrigatoria;
    }

    /**
     *
     * @param boolean $bl_obrigatoria
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setBl_obrigatoria($bl_obrigatoria)
    {
        $this->bl_obrigatoria = $bl_obrigatoria;
        return $this;
    }

    /**
     *
     * @return decimal
     */
    public function getNu_ponderacaocalculada()
    {
        return $this->nu_ponderacaocalculada;
    }

    /**
     *
     * @param decimal $nu_ponderacaocalculada
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setNu_ponderacaocalculada($nu_ponderacaocalculada)
    {
        $this->nu_ponderacaocalculada = $nu_ponderacaocalculada;
        return $this;
    }

    /**
     *
     * @return decimal
     */
    public function getNu_ponderacaoaplicada()
    {
        return $this->nu_ponderacaoaplicada;
    }

    /**
     *
     * @param decimal $nu_ponderacaoaplicada
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setNu_ponderacaoaplicada($nu_ponderacaoaplicada)
    {
        $this->nu_ponderacaoaplicada = $nu_ponderacaoaplicada;
        return $this;
    }

    /**
     *
     * @return datetime2
     */
    public function getDt_cadastroponderacao()
    {
        return $this->dt_cadastroponderacao;
    }

    /**
     *
     * @param datetime2 $dt_cadastroponderacao
     * @return \G2\Entity\ModuloDisciplina
     */
    public function setDt_cadastroponderacao($dt_cadastroponderacao)
    {
        $this->dt_cadastroponderacao = $dt_cadastroponderacao;
        return $this;
    }

    /**
     * @param int $nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @return int
     */
    public function getNu_obrigatorioalocacao()
    {
        return $this->nu_obrigatorioalocacao;
    }

    /**
     * @param int $nu_obrigatorioalocacao
     */
    public function setNu_obrigatorioalocacao($nu_obrigatorioalocacao)
    {
        $this->nu_obrigatorioalocacao = $nu_obrigatorioalocacao;
    }

    /**
     * @return int
     */
    public function getNu_disponivelapartirdo()
    {
        return $this->nu_disponivelapartirdo;
    }

    /**
     * @param int $nu_disponivelapartirdo
     */
    public function setNu_disponivelapartirdo($nu_disponivelapartirdo)
    {
        $this->nu_disponivelapartirdo = $nu_disponivelapartirdo;
    }

    /**
     * @return int
     */
    public function getNu_percentualsemestre()
    {
        return $this->nu_percentualsemestre;
    }

    /**
     * @param int $nu_percentualsemestre
     */
    public function setNu_percentualsemestre($nu_percentualsemestre)
    {
        $this->nu_percentualsemestre = $nu_percentualsemestre;
    }



}

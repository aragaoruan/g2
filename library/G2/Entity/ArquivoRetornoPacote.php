<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_arquivoretornopacote")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class ArquivoRetornoPacote {

    /**
     * @Id
     * @var integer $id_arquivoretornopacote
     * @Column(name="id_arquivoretornopacote", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_arquivoretornopacote;


    /**
     * @var ArquivoRetornoMaterial $id_arquivoretornomaterial
     * @ManyToOne(targetEntity="ArquivoRetornoMaterial")
     * @JoinColumn(name="id_arquivoretornomaterial", referencedColumnName="id_arquivoretornomaterial")
     */
    private $id_arquivoretornomaterial;


    /**
     * @var Pacote $id_pacote
     * @ManyToOne(targetEntity="Pacote")
     * @JoinColumn(name="id_pacote", referencedColumnName="id_pacote")
     */
    private $id_pacote;


    /**
     * @var ItemDeMaterial $id_itemdematerial
     * @ManyToOne(targetEntity="ItemDeMaterial")
     * @JoinColumn(name="id_itemdematerial", referencedColumnName="id_itemdematerial")
     */
    private $id_itemdematerial;

    /**
     * @var string $st_codrastreamento
     * @Column(name="st_codrastreamento", type="string",  nullable=false, length=60)
     */
    private $st_codrastreamento;

    /**
     * @var datetime $dt_postagem
     * @Column(name="dt_postagem", type="datetime",  nullable=true, length=8)
     */
    private $dt_postagem;

    /**
     * @var datetime $dt_devolucao
     * @Column(name="dt_devolucao", type="datetime",  nullable=true, length=8)
     */
    private $dt_devolucao;

    /**
     * @var string $st_resultado
     * @Column(name="st_resultado", type="string",  nullable=false, length=200)
     */
    private $st_resultado;

    /**
     * @var Situacao $id_situacaopostagem
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacaopostagem", referencedColumnName="id_situacao")
     */
    private $id_situacaopostagem;

    public function getId_arquivoretornopacote()
    {
        return $this->id_arquivoretornopacote;
    }

    public function setId_arquivoretornopacote($id_arquivoretornopacote)
    {
        $this->id_arquivoretornopacote = $id_arquivoretornopacote;
    }

    public function getId_arquivoretornomaterial()
    {
        return $this->id_arquivoretornomaterial;
    }

    public function setId_arquivoretornomaterial($id_arquivoretornomaterial)
    {
        $this->id_arquivoretornomaterial = $id_arquivoretornomaterial;
    }

    public function getId_pacote()
    {
        return $this->id_pacote;
    }

    public function setId_pacote($id_pacote)
    {
        $this->id_pacote = $id_pacote;
    }

    public function getId_itemdematerial()
    {
        return $this->id_itemdematerial;
    }

    public function setId_itemdematerial($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
    }

    public function getSt_codrastreamento()
    {
        return $this->st_codrastreamento;
    }

    public function setSt_codrastreamento($st_codrastreamento)
    {
        $this->st_codrastreamento = $st_codrastreamento;
    }

    public function getDt_postagem()
    {
        return $this->dt_postagem;
    }

    public function setDt_postagem($dt_postagem)
    {
        $this->dt_postagem = $dt_postagem;
    }

    public function getDt_devolucao()
    {
        return $this->dt_devolucao;
    }

    public function setDt_devolucao($dt_devolucao)
    {
        $this->dt_devolucao = $dt_devolucao;
    }

    public function getSt_resultado()
    {
        return $this->st_resultado;
    }

    public function setSt_resultado($st_resultado)
    {
        $this->st_resultado = $st_resultado;
    }

    public function getId_situacaopostagem()
    {
        return $this->id_situacaopostagem;
    }

    public function setId_situacaopostagem($id_situacaopostagem)
    {
        $this->id_situacaopostagem = $id_situacaopostagem;
    }





}

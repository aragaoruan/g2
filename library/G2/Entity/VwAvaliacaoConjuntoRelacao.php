<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoconjuntorelacao")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAvaliacaoConjuntoRelacao
{

    /**
     *
     * @var integer $id_avaliacaoconjuntorelacao
     * @Column(name="id_avaliacaoconjuntorelacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoconjuntorelacao;
    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false)
     */
    private $id_avaliacao;
    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false, length=100)
     */
    private $st_avaliacao;
    /**
     * @var integer $id_avaliacaorecupera
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true)
     */
    private $id_avaliacaorecupera;
    /**
     * @var string $st_avaliacaorecupera
     * @Column(name="st_avaliacaorecupera", type="string", nullable=true)
     */
    private $st_avaliacaorecupera;
    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false)
     */
    private $id_avaliacaoconjunto;
    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="integer", nullable=false)
     */
    private $nu_valor;
    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false)
     */
    private $id_tipoavaliacao;
    /**
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=true)
     */
    private $st_tipoavaliacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @Column(name="bl_recuperacao", type="boolean", nullable=true)
     * @var boolean $bl_recuperacao
     */
    private $bl_recuperacao;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    public function getId_avaliacaoconjuntorelacao()
    {
        return $this->id_avaliacaoconjuntorelacao;
    }

    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function getSt_avaliacao()
    {
        return $this->st_avaliacao;
    }

    public function getId_avaliacaorecupera()
    {
        return $this->id_avaliacaorecupera;
    }

    public function getSt_avaliacaorecupera()
    {
        return $this->st_avaliacaorecupera;
    }

    public function getId_avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    public function getId_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    public function getSt_tipoavaliacao()
    {
        return $this->st_tipoavaliacao;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao)
    {
        $this->id_avaliacaoconjuntorelacao = $id_avaliacaoconjuntorelacao;
    }

    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
    }

    public function setSt_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
    }

    public function setId_avaliacaorecupera($id_avaliacaorecupera)
    {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
    }

    public function setSt_avaliacaorecupera($st_avaliacaorecupera)
    {
        $this->st_avaliacaorecupera = $st_avaliacaorecupera;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
    }

    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
    }

    public function setSt_tipoavaliacao($st_tipoavaliacao)
    {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getBl_recuperacao()
    {
        return $this->bl_recuperacao;
    }

    public function setBl_recuperacao($bl_recuperacao)
    {
        $this->bl_recuperacao = $bl_recuperacao;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }


}

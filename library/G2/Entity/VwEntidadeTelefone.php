<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidadetelefone")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEntidadeTelefone
{

    /**
     * @var integer $id_telefone
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_telefone", type="integer", nullable=false, length=4)
     */
    private $id_telefone;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_tipotelefone
     * @Column(name="id_tipotelefone", type="integer", nullable=false, length=4)
     */
    private $id_tipotelefone;

    /**
     * @var integer $nu_ddi
     * @Column(name="nu_ddi", type="integer", nullable=false, length=4)
     */
    private $nu_ddi;

    /**
     * @var integer $nu_ddd
     * @Column(name="nu_ddd", type="integer", nullable=false, length=4)
     */
    private $nu_ddd;

    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false, length=1)
     */
    private $bl_padrao;

    /**
     * @var string $st_tipotelefone
     * @Column(name="st_tipotelefone", type="string", nullable=false, length=255)
     */
    private $st_tipotelefone;

    /**
     * @var string $nu_telefone
     * @Column(name="nu_telefone", type="string", nullable=true, length=8)
     */
    private $nu_telefone;

    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=800)
     */
    private $st_descricao;

    public function getId_telefone ()
    {
        return $this->id_telefone;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function getId_tipotelefone ()
    {
        return $this->id_tipotelefone;
    }

    public function getNu_ddi ()
    {
        return $this->nu_ddi;
    }

    public function getNu_ddd ()
    {
        return $this->nu_ddd;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function getSt_tipotelefone ()
    {
        return $this->st_tipotelefone;
    }

    public function getNu_telefone ()
    {
        return $this->nu_telefone;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setId_telefone ($id_telefone)
    {
        $this->id_telefone = $id_telefone;
        return $this;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_tipotelefone ($id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
        return $this;
    }

    public function setNu_ddi ($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
        return $this;
    }

    public function setNu_ddd ($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function setSt_tipotelefone ($st_tipotelefone)
    {
        $this->st_tipotelefone = $st_tipotelefone;
        return $this;
    }

    public function setNu_telefone ($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_matriculadisciplina")
 * @Entity(repositoryClass="\G2\Repository\MatriculaDisciplina")
 * @EntityLog
 */
class MatriculaDisciplina extends G2Entity
{

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio", type="boolean", nullable=false, length=1)
     */
    private $bl_obrigatorio;
    /**
     * @var integer $nu_aprovafinal
     * @Column(name="nu_aprovafinal", type="integer", nullable=true, length=5)
     */
    private $nu_aprovafinal;

    /**
     * @Column(name="dt_conclusao", type="date", nullable=true)
     */
    private $dt_conclusao;
    /**
     * @var Matricula $id_matriculaoriginal
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matriculaoriginal", referencedColumnName="id_matricula")
     */
    private $id_matriculaoriginal;
    /**
     * @var integer $id_pacote
     * @Column(name="id_pacote", type="integer", nullable=true, length=4)
     */
    private $id_pacote;

    /**
     * @var string $st_disciplinaoriginal
     * @Column(name="st_disciplinaoriginal", type="string", nullable=true, length=255)
     */
    private $st_disciplinaoriginal;

    /**
     * @var string $st_instituicaoaproveitamento
     * @Column(name="st_instituicaoaproveitamento", type="string", nullable=true, length=255)
     */
    private $st_instituicaoaproveitamento;

    /**
     * @var Uf $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", referencedColumnName="sg_uf")
     */
    private $sg_uf;

    /**
     * @var integer $id_usuarioaproveitamento
     * @Column(name="id_usuarioaproveitamento", type="integer", nullable=true, length=4)
     */
    private $id_usuarioaproveitamento;

    /**
     * @var Aproveitamento $id_aproveitamento
     * @ManyToOne(targetEntity="Aproveitamento")
     * @JoinColumn(name="id_aproveitamento", referencedColumnName="id_aproveitamento")
     */
    private $id_aproveitamento;

    /**
     * @var Situacao $id_situacaoagendamento
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacaoagendamento", referencedColumnName="id_situacao")
     */
    private $id_situacaoagendamento;

    /**
     * @var integer $nu_semestre
     * @Column(name="nu_semestre", type="integer", nullable=true, length=5)
     */
    private $nu_semestre;

    /**
     * @var datetime2 $dt_aptoagendamento
     * @Column(name="dt_aptoagendamento", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_aptoagendamento;

    /**
     * @var integer $nu_cargahorariaaproveitamento
     * @Column(name="nu_cargahorariaaproveitamento", type="integer", nullable=true, length=5)
     */
    private $nu_cargahorariaaproveitamento;

    /**
     * @var datetime2 $dt_tentativaalocacao
     * @Column(name="dt_tentativaalocacao", type="datetime2", nullable=true)
     */
    private $dt_tentativaalocacao;

    /**
     * @var integer $id_situacaotiposervico
     * @Column(name="id_situacaotiposervico", type="integer", nullable=true, length=4)
     */
    private $id_situacaotiposervico;

    /**
     * @return int
     */
    public function getId_situacaotiposervico()
    {
        return $this->id_situacaotiposervico;
    }

    /**
     * @param int $id_situacaotiposervico
     */
    public function setId_situacaotiposervico($id_situacaotiposervico)
    {
        $this->id_situacaotiposervico = $id_situacaotiposervico;
    }
    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean", nullable=true)
     */
    private $bl_cancelamento;

    public function setDt_conclusao($dt_conclusao)
    {
        $this->dt_conclusao = $dt_conclusao;
        return $this;
    }


    public function getDt_conclusao()
    {
        return $this->dt_conclusao;
    }


    public function setBl_obrigatorio($bl_obrigatorio)
    {
        $this->bl_obrigatorio = $bl_obrigatorio;
        return $this;
    }


    public function getBl_obrigatorio()
    {
        return $this->bl_obrigatorio;
    }


    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }


    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }


    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }


    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }


    public function getId_matricula()
    {
        return $this->id_matricula;
    }


    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }


    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }


    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }


    public function getId_situacao()
    {
        return $this->id_situacao;
    }


    public function setNu_aprovafinal($nu_aprovafinal)
    {
        $this->nu_aprovafinal = $nu_aprovafinal;
        return $this;
    }


    public function getNu_aprovafinal()
    {
        return $this->nu_aprovafinal;
    }

    public function setId_matriculaoriginal($id_matriculaoriginal)
    {
        $this->id_matriculaoriginal = $id_matriculaoriginal;
        return $this;
    }


    public function getId_matriculaoriginal()
    {
        return $this->id_matriculaoriginal;
    }

    /**
     * @return int
     */
    public function getId_pacote()
    {
        return $this->id_pacote;
    }

    /**
     * @param int $id_pacote
     */
    public function setId_pacote($id_pacote)
    {
        $this->id_pacote = $id_pacote;
    }


    /**
     * @return string st_disciplinaoriginal
     */
    public function getSt_disciplinaoriginal()
    {
        return $this->st_disciplinaoriginal;
    }

    /**
     * @param st_disciplinaoriginal
     */
    public function setSt_disciplinaoriginal($st_disciplinaoriginal)
    {
        $this->st_disciplinaoriginal = $st_disciplinaoriginal;
        return $this;
    }

    /**
     * @return string st_instituicaoaproveitamento
     */
    public function getSt_instituicaoaproveitamento()
    {
        return $this->st_instituicaoaproveitamento;
    }

    /**
     * @param st_instituicaoaproveitamento
     */
    public function setSt_instituicaoaproveitamento($st_instituicaoaproveitamento)
    {
        $this->st_instituicaoaproveitamento = $st_instituicaoaproveitamento;
        return $this;
    }

    /**
     * @return string sg_uf
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }


    /**
     * @return integer id_usuarioaproveitamento
     */
    public function getId_usuarioaproveitamento()
    {
        return $this->id_usuarioaproveitamento;
    }

    /**
     * @param id_usuarioaproveitamento
     */
    public function setId_usuarioaproveitamento($id_usuarioaproveitamento)
    {
        $this->id_usuarioaproveitamento = $id_usuarioaproveitamento;
        return $this;
    }

    /**
     * @return integer id_aproveitamento
     */
    public function getId_aproveitamento()
    {
        return $this->id_aproveitamento;
    }

    /**
     * @param id_aproveitamento
     */
    public function setId_aproveitamento($id_aproveitamento)
    {
        $this->id_aproveitamento = $id_aproveitamento;
        return $this;
    }

    /**
     * @return Situacao
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param int $id_situacaoagendamento
     * @return $this
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
        return $this;
    }


    public function getNu_semestre()
    {
        return $this->nu_semestre;
    }

    public function setNu_semestre($nu_semestre)
    {
        $this->nu_semestre = $nu_semestre;
    }

    /**
     * @return datetime2
     */
    public function getDt_aptoagendamento()
    {
        return $this->dt_aptoagendamento;
    }

    /**
     * @param datetime2 $dt_aptoagendamento
     * @return $this
     */
    public function setDt_aptoagendamento($dt_aptoagendamento)
    {
        $this->dt_aptoagendamento = $dt_aptoagendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahorariaaproveitamento()
    {
        return $this->nu_cargahorariaaproveitamento;
    }

    /**
     * @param int $nu_cargahorariaaproveitamento
     */
    public function setNu_cargahorariaaproveitamento($nu_cargahorariaaproveitamento)
    {
        $this->nu_cargahorariaaproveitamento = $nu_cargahorariaaproveitamento;
    }

    /**
     * @return \G2\Entity\Disciplina|null
     */
    public function getDisciplina()
    {
        $negocio = new \G2\Negocio\Negocio();
        return $negocio->find('\G2\Entity\Disciplina', $this->id_disciplina);
    }

    /**
     * @return mixed
     */
    public function getDt_tentativaalocacao()
    {
        return $this->dt_tentativaalocacao;
    }

    /**
     * @param mixed $dt_tentativaalocacao
     * @return $this
     */
    public function setDt_tentativaalocacao($dt_tentativaalocacao)
    {
        $this->dt_tentativaalocacao = $dt_tentativaalocacao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }



}

<?php

namespace G2\Entity;

use G2\G2Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_origemareaconhecimento")
 * @Entity
 */
class OrigemAreaConhecimento extends G2Entity
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_origemareaconhecimento
     * @Column(name="id_origemareaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_origemareaconhecimento;

    /**
     * @var string $st_origemareaconhecimento
     * @Column(name="st_origemareaconhecimento", type="string", nullable=true, length=140)
     */
    private $st_origemareaconhecimento;

    /**
     * @return int
     */
    public function getId_origemareaconhecimento()
    {
        return $this->id_origemareaconhecimento;
    }

    /**
     * @param int $id_origemareaconhecimento
     * @return $this
     */
    public function setId_origemareaconhecimento($id_origemareaconhecimento)
    {
        $this->id_origemareaconhecimento = $id_origemareaconhecimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_origemareaconhecimento()
    {
        return $this->st_origemareaconhecimento;
    }

    /**
     * @param string $st_origemareaconhecimento
     * @return $this
     */
    public function setSt_origemareaconhecimento($st_origemareaconhecimento)
    {
        $this->st_origemareaconhecimento = $st_origemareaconhecimento;
        return $this;
    }


}
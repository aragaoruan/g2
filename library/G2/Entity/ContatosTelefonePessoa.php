<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_contatostelefonepessoa")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ContatosTelefonePessoa
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var ContatosTelefone $id_telefone
     * @Column(name="id_telefone", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_telefone;

    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=true)
     */
    private $bl_padrao;

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuario
     * @return \G2\Entity\ContatosTelefonePessoa
     */
    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\ContatosTelefonePessoa
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\ContatosTelefone
     */
    public function getId_telefone ()
    {
        return $this->id_telefone;
    }

    /**
     * @param \G2\Entity\ContatosTelefone $id_telefone
     * @return \G2\Entity\ContatosTelefonePessoa
     */
    public function setId_telefone ($id_telefone)
    {
        $this->id_telefone = $id_telefone;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    /**
     * @param boolean $bl_padrao
     * @return \G2\Entity\ContatosTelefonePessoa
     */
    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

}
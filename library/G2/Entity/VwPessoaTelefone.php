<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pessoatelefone")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPessoaTelefone extends G2Entity
{

    /**
     *
     * @var integer $id_telefone
     * @column(name="id_telefone", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_telefone;

    /**
     * @var string $nu_telefone
     * @column(name="nu_telefone", nullable=true, type="string", length=15)
     */
    private $nu_telefone;

    /**
     * @var integer $id_tipotelefone
     * @column(name="id_tipotelefone", nullable=false, type="integer")
     */
    private $id_tipotelefone;

    /**
     * @var string $st_tipotelefone
     * @column(name="st_tipotelefone", nullable=false, type="string", length=255)
     */
    private $st_tipotelefone;

    /**
     * @var integer $id_usuario
     * @column(name="id_usuario", nullable=false, type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @column(name="id_entidade", nullable=false, type="integer")
     */
    private $id_entidade;

    /**
     * @var boolean $bl_padrao
     * @column(name="bl_padrao", nullable=false, type="boolean")
     */
    private $bl_padrao;

    /**
     * @var string $nu_ddd
     * @column(name="nu_ddd", nullable=false, type="string")
     */
    private $nu_ddd;

    /**
     * @var integer $nu_ddi
     * @column(name="nu_ddi", nullable=false, type="integer")
     */
    private $nu_ddi;

    public function getId_telefone ()
    {
        return $this->id_telefone;
    }

    public function setId_telefone ($id_telefone)
    {
        $this->id_telefone = $id_telefone;
        return $this;
    }

    public function getNu_telefone ()
    {
        return $this->nu_telefone;
    }

    public function setNu_telefone ($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    public function getId_tipotelefone ()
    {
        return $this->id_tipotelefone;
    }

    public function setId_tipotelefone ($id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
        return $this;
    }

    public function getSt_tipotelefone ()
    {
        return $this->st_tipotelefone;
    }

    public function setSt_tipotelefone ($st_tipotelefone)
    {
        $this->st_tipotelefone = $st_tipotelefone;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getNu_ddd ()
    {
        return $this->nu_ddd;
    }

    public function setNu_ddd ($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    public function getNu_ddi ()
    {
        return $this->nu_ddi;
    }

    public function setNu_ddi ($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
        return $this;
    }

    /**
     * Return array of object
     * @return array
     */
    public function _toArray ()
    {
        return array(
            'id_telefone' => $this->id_telefone,
            'nu_telefone' => $this->nu_telefone,
            'id_tipotelefone' => $this->id_tipotelefone,
            'st_tipotelefone' => $this->st_tipotelefone,
            'id_usuario' => $this->id_usuario,
            'id_entidade' => $this->id_entidade,
            'bl_padrao' => $this->bl_padrao,
            'nu_ddd' => $this->nu_ddd,
            'nu_ddi' => $this->nu_ddi
        );
    }

}
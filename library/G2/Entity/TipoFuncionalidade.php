<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipofuncionalidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoFuncionalidade extends G2Entity
{

    /**
     * @var integer $id_tipofuncionalidade
     * @Column(name="id_tipofuncionalidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipofuncionalidade;

    /**
     * @var string $st_tipofuncionalidade
     * @Column(name="st_tipofuncionalidade", type="string", nullable=false, length=255)
     */
    private $st_tipofuncionalidade;

    public function getId_tipofuncionalidade ()
    {
        return $this->id_tipofuncionalidade;
    }

    public function setId_tipofuncionalidade ($id_tipofuncionalidade)
    {
        $this->id_tipofuncionalidade = $id_tipofuncionalidade;
        return $this;
    }

    public function getSt_tipofuncionalidade ()
    {
        return $this->st_tipofuncionalidade;
    }

    public function setSt_tipofuncionalidade ($st_tipofuncionalidade)
    {
        $this->st_tipofuncionalidade = $st_tipofuncionalidade;
        return $this;
    }

}

<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 18/11/13
 * Time: 11:25
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_justificativaviacontrato")
 * @EntityLog
 */
class JustificativaViaContrato extends G2Entity
{
    /**
     * @Id
     * @var integer $id_justificativaviacontrato
     * @Column(name="id_justificativaviacontrato", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_justificativaviacontrato;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @var integer $nu_viacontrato
     * @Column(name="nu_viacontrato", type="integer", nullable=false, length=4)
     */
    private $nu_viacontrato;
    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var string $st_justificativa
     * @Column(name="st_justificativa", type="string", nullable=false, length=1000)
     */
    private $st_justificativa;


    /**
     * @return integer id_justificativaviacontrato
     */
    public function getId_justificativaviacontrato() {
        return $this->id_justificativaviacontrato;
    }

    /**
     * @param id_justificativaviacontrato
     */
    public function setId_justificativaviacontrato($id_justificativaviacontrato) {
        $this->id_justificativaviacontrato = $id_justificativaviacontrato;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer nu_viacontrato
     */
    public function getNu_viacontrato() {
        return $this->nu_viacontrato;
    }

    /**
     * @param nu_viacontrato
     */
    public function setNu_viacontrato($nu_viacontrato) {
        $this->nu_viacontrato = $nu_viacontrato;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string st_justificativa
     */
    public function getSt_justificativa() {
        return $this->st_justificativa;
    }

    /**
     * @param st_justificativa
     */
    public function setSt_justificativa($st_justificativa) {
        $this->st_justificativa = $st_justificativa;
        return $this;
    }

}
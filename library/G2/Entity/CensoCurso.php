<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_censocurso")
 * @Entity
 */
class CensoCurso
{

    /**
     * @var int $id_censocurso
     * @Column(name="id_censocurso", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_censocurso;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_ativo;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $nu_codigocurso
     * @Column(name="nu_codigocurso", type="integer", nullable=false)
     */
    private $nu_codigocurso;

    /**
     * @return int
     */
    public function getId_censocurso()
    {
        return $this->id_censocurso;
    }

    /**
     * @param int $id_censocurso
     * @return $this
     */
    public function setId_censocurso($id_censocurso)
    {
        $this->id_censocurso = $id_censocurso;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return ProjetoPedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param ProjetoPedagogico $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_codigocurso()
    {
        return $this->nu_codigocurso;
    }

    /**
     * @param int $nu_codigocurso
     * @return $this
     */
    public function setNu_codigocurso($nu_codigocurso)
    {
        $this->nu_codigocurso = $nu_codigocurso;
        return $this;
    }

}

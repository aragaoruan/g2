<?php

namespace G2\Entity;

/**
 * @Table(name="tb_linhadenegocio")
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class LinhaDeNegocio
{

    /**
     * @var integer $id_linhadenegocio
     * @Column(name="id_linhadenegocio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_linhadenegocio;

    /**
     * @var string $st_linhadenegocio
     * @Column(name="st_linhadenegocio", type="string", nullable=false, length=200)
     */
    private $st_linhadenegocio;

    public function getId_linhadenegocio()
    {
        return $this->id_linhadenegocio;
    }

    public function setId_linhadenegocio($id_linhadenegocio)
    {
        $this->id_linhadenegocio = $id_linhadenegocio;
    }

    public function getSt_linhadenegocio()
    {
        return $this->st_linhadenegocio;
    }

    public function setSt_linhadenegocio($st_linhadenegocio)
    {
        $this->st_linhadenegocio = $st_linhadenegocio;
    }

}
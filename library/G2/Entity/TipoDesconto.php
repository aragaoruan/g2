<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipodesconto")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoDesconto
{

    /**
     *
     * @var integer $id_tipodesconto
     * @Column(name="id_tipodesconto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodesconto;

    /**
     *
     * @var string $st_tipodesconto
     * @Column(name="st_tipodesconto", type="string", nullable=false, length=255)
     */
    private $st_tipodesconto;

    public function getId_tipodesconto ()
    {
        return $this->id_tipodesconto;
    }

    public function setId_tipodesconto ($id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;
    }

    public function getSt_tipodesconto ()
    {
        return $this->st_tipodesconto;
    }

    public function setSt_tipodesconto ($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;
    }

}
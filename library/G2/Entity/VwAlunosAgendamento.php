<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunosagendamento")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAlunosAgendamento
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=200)
     */
    private $st_nomecompleto;


    /**
     *
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=false, length=200)
     */
    private $st_cpf;

    /**
     *
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=false, length=200)
     */
    private $st_email;


    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", nullable=false, type="integer")
     */
    private $id_situacaoagendamento;

    /**
     * @var Matricula $id_matricula
     * @Column(name="id_matricula", nullable=false, type="integer")
     */
    private $id_matricula;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", nullable=true, type="integer")
     */
    private $id_aplicadorprova;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", nullable=true, type="string" , length=200)
     */
    private $st_aplicadorprova;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;


    /**
     *
     * @var boolean $bl_unica
     * @Column(name="bl_unica", type="boolean", nullable=true)
     */
    private $bl_unica;


    /**
     * @var datetime2 $dt_antecedenciaminima
     * @Column(name="dt_antecedenciaminima", type="datetime2", nullable=true)
     */
    private $dt_antecedenciaminima;


    /**
     * @var datetime2 $dt_alteracaolimite
     * @Column(name="dt_alteracaolimite", type="datetime2", nullable=true)
     */
    private $dt_alteracaolimite;


    /**
     * @var datetime2 $dt_aplicacao
     * @Column(name="dt_aplicacao", type="datetime2", nullable=true)
     */
    private $dt_aplicacao;


    /**
     * @var Usuario $id_usuariocadastro
     * @Column(name="id_usuariocadastro", nullable=true, type="integer")
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", nullable=true, type="integer")
     */
    private $id_horarioaula;


    /**
     * @var integer $nu_maxaplicacao
     * @Column(name="nu_maxaplicacao", nullable=true, type="integer")
     */
    private $nu_maxaplicacao;

    /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", nullable=true, type="integer")
     */
    private $id_endereco;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", nullable=true, type="integer")
     */
    private $id_avaliacaoaplicacao;


    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", nullable=true, type="string" , length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var Entidade $id_entidade
     * @Column(name="id_entidade", nullable=true, type="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", nullable=true, type="integer")
     */
    private $id_projetopedagogico;


    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", nullable=true, type="string" , length=2)
     */
    private $sg_uf;

    /**
     * @var boolean $bl_ativoavagendamento
     * @Column(name="bl_ativoavagendamento", type="boolean", nullable=true)
     */
    private $bl_ativoavagendamento;

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", nullable=true, type="integer")
     */
    private $id_avaliacaoagendamento;


    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_unica
     */
    public function setBl_unica($bl_unica)
    {
        $this->bl_unica = $bl_unica;
    }

    /**
     * @return boolean
     */
    public function getBl_unica()
    {
        return $this->bl_unica;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_alteracaolimite
     */
    public function setDt_alteracaolimite($dt_alteracaolimite)
    {
        $this->dt_alteracaolimite = $dt_alteracaolimite;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_alteracaolimite()
    {
        return $this->dt_alteracaolimite;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_antecedenciaminima
     */
    public function setDt_antecedenciaminima($dt_antecedenciaminima)
    {
        $this->dt_antecedenciaminima = $dt_antecedenciaminima;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_antecedenciaminima()
    {
        return $this->dt_antecedenciaminima;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_aplicacao
     */
    public function setDt_aplicacao($dt_aplicacao)
    {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_aplicacao()
    {
        return $this->dt_aplicacao;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param int $id_avaliacaoaplicacao
     */
    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @param int $id_endereco
     */
    public function setId_endereco($id_endereco)
    {
        $this->id_endereco = $id_endereco;
    }

    /**
     * @return int
     */
    public function getId_endereco()
    {
        return $this->id_endereco;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_horarioaula
     */
    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
    }

    /**
     * @return int
     */
    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    /**
     * @param \G2\Entity\Matricula $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return \G2\Entity\Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_situacaoagendamento
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $nu_maxaplicacao
     */
    public function setNu_maxaplicacao($nu_maxaplicacao)
    {
        $this->nu_maxaplicacao = $nu_maxaplicacao;
    }

    /**
     * @return int
     */
    public function getNu_maxaplicacao()
    {
        return $this->nu_maxaplicacao;
    }

    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param int $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param boolean $bl_ativoavagendamento
     */
    public function setBl_ativoavagendamento($bl_ativoavagendamento)
    {
        $this->bl_ativoavagendamento = $bl_ativoavagendamento;
    }

    /**
     * @return boolean
     */
    public function getBl_ativoavagendamento()
    {
        return $this->bl_ativoavagendamento;
    }

    /**
     * @param int $id_avaliacaoagendamento
     */
    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }




}
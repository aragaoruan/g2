<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_alocacao")
 * @Entity(repositoryClass="\G2\Repository\Alocacao")
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @update Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Alocacao extends G2Entity
{
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_alocacao;

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=true, length=3)
    */
    private $dt_inicio;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;


    /**
     * @var SalaDeAula $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")
     */
    private $id_saladeaula;

    /**
     * @var Evolucao $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;


    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;


    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;


    /**
     * @var MatriculaDisciplina $id_matriculadisciplina
     * @ManyToOne(targetEntity="MatriculaDisciplina")
     * @JoinColumn(name="id_matriculadisciplina", referencedColumnName="id_matriculadisciplina")
     */
    private $id_matriculadisciplina;


    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer", nullable=false, length=4)
     */
    private $id_situacaotcc;


    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true, length=4)
     */
    private $id_categoriasala;


    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_desalocar
     * @Column(name="bl_desalocar", type="boolean", nullable=false, length=1)
     */
    private $bl_desalocar;

    /**
     * @var integer $nu_diasextensao
     * @Column(name="nu_diasextensao",  type="integer", nullable=true, length=4)
     */
    private $nu_diasextensao;

    /**
     * @var boolean $bl_trancamento
     * @Column(name="bl_trancamento", type="boolean", nullable=false, length=1)
     */
    private $bl_trancamento = 0;

    /**
     * @var Usuario $id_usuariotrancamento
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariotrancamento", referencedColumnName="id_usuario")
     */
    private $id_usuariotrancamento;

    /**
     * @var datetime $dt_trancamento
     * @Column(name="dt_trancamento", type="datetime", nullable=true, length=8)
     */
    private $dt_trancamento;


    /**
     * @param boolean $bl_desalocar
     */
    public function setBl_desalocar($bl_desalocar)
    {
        $this->bl_desalocar = $bl_desalocar;
    }

    /**
     * @return boolean
     */
    public function getBl_desalocar()
    {
        return $this->bl_desalocar;
    }

    /**
     * @param int $nu_diasextensao
     */
    public function setNu_diasextensao($nu_diasextensao)
    {
        $this->nu_diasextensao = $nu_diasextensao;
    }

    /**
     * @return int
     */
    public function getNu_diasextensao()
    {
        return $this->nu_diasextensao;
    }


    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function getId_alocacao() {
        return $this->id_alocacao;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    public function getId_situacaotcc() {
        return $this->id_situacaotcc;
    }

    public function getId_categoriasala() {
        return $this->id_categoriasala;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setId_alocacao($id_alocacao) {
        $this->id_alocacao = $id_alocacao;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }

    public function setId_situacaotcc($id_situacaotcc) {
        $this->id_situacaotcc = $id_situacaotcc;
        return $this;
    }

    public function setId_categoriasala($id_categoriasala) {
        $this->id_categoriasala = $id_categoriasala;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_trancamento()
    {
        return $this->bl_trancamento;
    }

    public function setBl_trancamento($bl_trancamento)
    {
        $this->bl_trancamento = $bl_trancamento;
    }

    public function getId_usuariotrancamento()
    {
        return $this->id_usuariotrancamento;
    }

    public function setId_usuariotrancamento($id_usuariotrancamento)
    {
        $this->id_usuariotrancamento = $id_usuariotrancamento;
    }

    public function getDt_trancamento()
    {
        return $this->dt_trancamento;
    }

    public function setDt_trancamento($dt_trancamento)
    {
        $this->dt_trancamento = $dt_trancamento;
    }


}

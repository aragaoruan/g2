<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_origemvenda")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class OrigemVenda {

    /**
     * @Id
     * @var integer $id_origemvenda
     * @Column(name="id_origemvenda", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_origemvenda;

    /**
     * @var string $st_origemvenda
     * @Column(name="st_origemvenda", type="string", nullable=false, length=25)
     */
    private $st_origemvenda;

    public function getId_origemvenda() {
        return $this->id_origemvenda;
    }

    public function setId_origemvenda($id_origemvenda) {
        $this->id_origemvenda = $id_origemvenda;
        return $this;
    }

    public function getSt_origemvenda() {
        return $this->st_origemvenda;
    }

    public function setSt_origemvenda($st_origemvenda) {
        $this->st_origemvenda = $st_origemvenda;
        return $this;
    }

}

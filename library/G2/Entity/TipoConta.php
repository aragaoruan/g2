<?php

namespace G2\Entity;

use \G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipodeconta")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoConta extends G2Entity
{

    /**
     *
     * @var integer $id_tipodeconta
     * @Column(name="id_tipodeconta", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodeconta;

    /**
     *
     * @var string $st_tipodeconta
     * @Column(name="st_tipodeconta", type="string", nullable=false, length=100)
     */
    private $st_tipodeconta;

    public function getId_tipodeconta ()
    {
        return $this->id_tipodeconta;
    }

    public function setId_tipodeconta ($id_tipodeconta)
    {
        $this->id_tipodeconta = $id_tipodeconta;
        return $this;
    }

    public function getSt_tipodeconta ()
    {
        return $this->st_tipodeconta;
    }

    public function setSt_tipodeconta ($st_tipodeconta)
    {
        $this->st_tipodeconta = $st_tipodeconta;
        return $this;
    }

}

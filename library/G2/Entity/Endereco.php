<?php

namespace G2\Entity;
use G2\G2Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_endereco")
 * @Entity
 * @EntityLog
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Endereco extends G2Entity
{

    /**
     *
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_endereco;

    /**
     *
     * @var Pais $id_pais
     * @ManyToOne(targetEntity="Pais")
     * @JoinColumn(name="id_pais", nullable=false, referencedColumnName="id_pais")
     */
    private $id_pais;

    /**
     *
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2) 
     */
    private $sg_uf;

    /**
     *
     * @var Municipio $id_municipio
     * @ManyToOne(targetEntity="Municipio")
     * @JoinColumn(name="id_municipio", nullable=false, referencedColumnName="id_municipio")
     */
    private $id_municipio;

    /**
     *
     * @var TipoEndereco $id_tipoendereco
     * @ManyToOne(targetEntity="TipoEndereco")
     * @JoinColumn(name="id_tipoendereco", nullable=false, referencedColumnName="id_tipoendereco")
     */
    private $id_tipoendereco;

    /**
     *
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12) 
     */
    private $st_cep;

    /**
     *
     * @var text $st_endereco
     * @Column(name="st_endereco", type="text", nullable=true) 
     */
    private $st_endereco;

    /**
     *
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255) 
     */
    private $st_bairro;

    /**
     *
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500) 
     */
    private $st_complemento;

    /**
     *
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30) 
     */
    private $nu_numero;

    /**
     *
     * @var string $st_estadoprovincia
     * @Column(name="st_estadoprovincia", type="string", nullable=true, length=255) 
     */
    private $st_estadoprovincia;

    /**
     *
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255) 
     */
    private $st_cidade;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    public function getId_endereco ()
    {
        return $this->id_endereco;
    }

    public function setId_endereco ($id_endereco)
    {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    public function getId_pais ()
    {
        return $this->id_pais;
    }

    public function setId_pais (Pais $id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getId_municipio ()
    {
        return $this->id_municipio;
    }

    public function setId_municipio (Municipio $id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getId_tipoendereco ()
    {
        return $this->id_tipoendereco;
    }

    public function setId_tipoendereco (TipoEndereco $id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
        return $this;
    }

    public function getSt_cep ()
    {
        return $this->st_cep;
    }

    public function setSt_cep ($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    public function getSt_endereco ()
    {
        return $this->st_endereco;
    }

    public function setSt_endereco ($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function getSt_bairro ()
    {
        return $this->st_bairro;
    }

    public function setSt_bairro ($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    public function getSt_complemento ()
    {
        return $this->st_complemento;
    }

    public function setSt_complemento ($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    public function getNu_numero ()
    {
        return $this->nu_numero;
    }

    public function setNu_numero ($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    public function getSt_estadoprovincia ()
    {
        return $this->st_estadoprovincia;
    }

    public function setSt_estadoprovincia ($st_estadoprovincia)
    {
        $this->st_estadoprovincia = $st_estadoprovincia;
        return $this;
    }

    public function getSt_cidade ()
    {
        return $this->st_cidade;
    }

    public function setSt_cidade ($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
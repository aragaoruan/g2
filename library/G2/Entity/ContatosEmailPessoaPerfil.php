<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_contatosemailpessoaperfil")
 * @Entity
 * @author Felipe Pastor
 */
class ContatosEmailPessoaPerfil
{

    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_perfil;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_email
     * @Column(name="id_email", type="integer", nullable=false, length=4)
     */
    private $id_email;

    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false, length=1)
     */
    private $bl_padrao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_padrao()
    {
        return $this->bl_padrao;
    }

    /**
     * @param boolean $bl_padrao
     */
    public function setBl_padrao($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
    }

    /**
     * @return int
     */
    public function getId_email()
    {
        return $this->id_email;
    }

    /**
     * @param int $id_email
     */
    public function setId_email($id_email)
    {
        $this->id_email = $id_email;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }
}
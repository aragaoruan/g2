<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_modelocarteirinha")
 * @Entity
 * @author Elcio Mauro Guimarães <elcio.guimaraes@unyleya.com.br>
 */
use \G2\G2Entity;

class ModeloCarteirinha extends G2Entity
{

    const ATIVA   = 140;
    const INATIVA = 141;

    /**
     *
     * @var integer $id_modelocarteirinha
     * @Column(name="id_modelocarteirinha", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_modelocarteirinha;

    /**
     * @var string $st_modelocarteirinha
     * @Column(name="st_modelocarteirinha", type="string", nullable=false, length=200)
     */
    private $st_modelocarteirinha;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_modelocarteirinha()
    {
        return $this->id_modelocarteirinha;
    }

    /**
     * @return string
     */
    public function getSt_modelocarteirinha()
    {
        return $this->st_modelocarteirinha;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param integer $id_modelocarteirinha
     * @return $this
     */
    public function setId_modelocarteirinha($id_modelocarteirinha)
    {
        $this->id_modelocarteirinha = $id_modelocarteirinha;
        return $this;
    }

    /**
     * @param string $st_modelocarteirinha
     * @return $this
     */
    public function setSt_modelocarteirinha($st_modelocarteirinha)
    {
        $this->st_modelocarteirinha = $st_modelocarteirinha;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param Situacao $id_situacao
     * @return $this
     */
    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_turmasproduto")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-02-12
 */
class VwTurmasProduto {

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turma;
    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=false, length=3)
     */
    private $dt_inicioinscricao;
    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=3)
     */
    private $dt_fiminscricao;
    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false, length=3)
     */
    private $dt_inicio;
    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true, length=3)
     */
    private $dt_fim;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $nu_alunos
     * @Column(name="nu_alunos", type="integer", nullable=true, length=4)
     */
    private $nu_alunos;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=true, length=4)
     */
    private $id_entidadecadastro;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=false, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=false, length=200)
     */
    private $st_tituloexibicao;




    /**
     * @return date dt_inicioinscricao
     */
    public function getDt_inicioinscricao() {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param dt_inicioinscricao
     */
    public function setDt_inicioinscricao($dt_inicioinscricao) {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return date dt_fiminscricao
     */
    public function getDt_fiminscricao() {
        return $this->dt_fiminscricao;
    }

    /**
     * @param dt_fiminscricao
     */
    public function setDt_fiminscricao($dt_fiminscricao) {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return date dt_fim
     */
    public function getDt_fim() {
        return $this->dt_fim;
    }

    /**
     * @param dt_fim
     */
    public function setDt_fim($dt_fim) {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer nu_alunos
     */
    public function getNu_alunos() {
        return $this->nu_alunos;
    }

    /**
     * @param nu_alunos
     */
    public function setNu_alunos($nu_alunos) {
        $this->nu_alunos = $nu_alunos;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_entidadecadastro
     */
    public function getId_entidadecadastro() {
        return $this->id_entidadecadastro;
    }

    /**
     * @param id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro) {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    /**
     * @param nu_maxalunos
     */
    public function setNu_maxalunos($nu_maxalunos) {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto() {
        return $this->id_produto;
    }

    /**
     * @param id_produto
     */
    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma() {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma) {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return string st_tituloexibicao
     */
    public function getSt_tituloexibicao() {
        return $this->st_tituloexibicao;
    }

    /**
     * @param st_tituloexibicao
     */
    public function setSt_tituloexibicao($st_tituloexibicao) {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }


}

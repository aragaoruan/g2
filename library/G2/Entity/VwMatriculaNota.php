<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculanota")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwMatriculaNota {

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     */
    private $id_matricula;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false)
     * @Id
     */
    private $id_saladeaula;

    /**
     * @var integer $id_alocacao
     * @Id
     * @Column(name="id_alocacao", type="integer", nullable=false)
     */
    private $id_alocacao;

    /**
     * @var integer $id_encerramentosala
     * @Id
     * @Column(name="id_encerramentosala", type="integer", nullable=false)
     */
    private $id_encerramentosala;

    /**
     * @var integer $id_avaliacaoatividade
     * @Id
     * @Column(name="id_avaliacaoatividade", type="integer", nullable=true)
     */
    private $id_avaliacaoatividade;

    /**
     * @var integer $id_disciplinaatividade
     * @Id
     * @Column(name="id_disciplinaatividade", type="integer", nullable=true)
     */
    private $id_disciplinaatividade;

    /**
     * @var string $st_notaatividade
     * @Column(name="st_notaatividade", type="string", nullable=true)
     */
    private $st_notaatividade;

    /**
     * @var integer $id_tiponotaatividade
     * @Id
     * @Column(name="id_tiponotaatividade", type="integer", nullable=true)
     */
    private $id_tiponotaatividade;

    /**
     * @var string $st_avaliacaoatividade
     * @Column(name="st_avaliacaoatividade", type="string", nullable=true)
     */
    private $st_avaliacaoatividade;

    /**
     * @var integer $nu_notamaxatividade
     * @Column(name="nu_notamaxatividade", type="integer", nullable=true)
     */
    private $nu_notamaxatividade;

    /**
     * @var integer $id_avaliacaorecuperacao
     * @Id
     * @Column(name="id_avaliacaorecuperacao", type="integer", nullable=true)
     */
    private $id_avaliacaorecuperacao;

    /**
     * @var integer $id_disciplinarecuperacao
     * @Id
     * @Column(name="id_disciplinarecuperacao", type="integer", nullable=true)
     */
    private $id_disciplinarecuperacao;

    /**
     * @var string $st_notarecuperacao
     * @Column(name="st_notarecuperacao", type="string", nullable=true)
     */
    private $st_notarecuperacao;

    /**
     * @var integer $id_tiponotarecuperacao
     * @Id
     * @Column(name="id_tiponotarecuperacao", type="integer", nullable=true)
     */
    private $id_tiponotarecuperacao;

    /**
     * @var string $st_avaliacaorecuperacao
     * @Column(name="st_avaliacaorecuperacao", type="string", nullable=true)
     */
    private $st_avaliacaorecuperacao;

    /**
     * @var integer $nu_notamaxrecuperacao
     * @Column(name="nu_notamaxrecuperacao", type="integer", nullable=true)
     */
    private $nu_notamaxrecuperacao;

    /**
     * @var integer $id_avaliacaofinal
     * @Id
     * @Column(name="id_avaliacaofinal", type="integer", nullable=true)
     */
    private $id_avaliacaofinal;

    /**
     * @var integer $id_disciplinafinal
     * @Id
     * @Column(name="id_disciplinafinal", type="integer", nullable=true)
     */
    private $id_disciplinafinal;

    /**
     * @var string $st_disciplinafinal
     * @Column(name="st_disciplinafinal", type="string", nullable=true)
     */
    private $st_disciplinafinal;

    /**
     * @var string $st_notafinal
     * @Column(name="st_notafinal", type="string", nullable=false, length=5)
     */
    private $st_notafinal;

    /**
     * @var integer $id_tipoavaliacao
     * @Id
     * @Column(name="id_tipoavaliacao", type="integer", nullable=true)
     */
    private $id_tipoavaliacao;

    /**
     * @var integer $id_tiponotafinal
     * @Id
     * @Column(name="id_tiponotafinal", type="integer", nullable=true)
     */
    private $id_tiponotafinal;

    /**
     * @var string $st_avaliacaofinal
     * @Column(name="st_avaliacaofinal", type="string", nullable=true)
     */
    private $st_avaliacaofinal;

    /**
     * @var integer $nu_notamaxfinal
     * @Column(name="nu_notamaxfinal", type="integer", nullable=true)
     */
    private $nu_notamaxfinal;

    /**
     * @var integer $id_avaliacaoead
     * @Id
     * @Column(name="id_avaliacaoead", type="integer", nullable=true)
     */
    private $id_avaliacaoead;

    /**
     * @var integer $id_disciplinaead
     * @Id
     * @Column(name="id_disciplinaead", type="integer", nullable=true)
     */
    private $id_disciplinaead;

    /**
     * @var string $st_notaead
     * @Column(name="st_notaead", type="string", nullable=false, length=5)
     */
    private $st_notaead;

    /**
     * @var integer $id_tiponotaead
     * @Id
     * @Column(name="id_tiponotaead", type="integer", nullable=true)
     */
    private $id_tiponotaead;

    /**
     * @var string $st_avaliacaoead
     * @Column(name="st_avaliacaoead", type="string", nullable=false, length=5)
     */
    private $st_avaliacaoead;

    /**
     * @var integer $nu_notamaxead
     * @Column(name="nu_notamaxead", type="integer", nullable=true)
     */
    private $nu_notamaxead;

    /**
     * @var integer $id_avaliacaoaluno
     * @Id
     * @Column(name="id_avaliacaoaluno", type="integer", nullable=true)
     */
    private $id_avaliacaoaluno;

    /**
     * @var integer $id_avaliacaotcc
     * @Id
     * @Column(name="id_avaliacaotcc", type="integer", nullable=true)
     */
    private $id_avaliacaotcc;

    /**
     * @var integer $id_disciplinatcc
     * @Id
     * @Column(name="id_disciplinatcc", type="integer", nullable=true)
     */
    private $id_disciplinatcc;

    /**
     * @var string $st_notatcc
     * @Column(name="st_notatcc", type="string", nullable=false, length=5)
     */
    private $st_notatcc;

    /**
     * @var string $st_avaliacaotcc
     * @Column(name="st_avaliacaotcc", type="string", nullable=false, length=5)
     */
    private $st_avaliacaotcc;

    /**
     * @var integer $nu_notamaxtcc
     * @Column(name="nu_notamaxtcc", type="integer", nullable=true)
     */
    private $nu_notamaxtcc;
    /**
     * @var integer $nu_notatotal
     * @Column(name="nu_notatotal", type="integer", nullable=true)
     */
    private $nu_notatotal;

    /**
     * @var integer $nu_notaaprovacao
     * @Column(name="nu_notaaprovacao", type="integer", nullable=true)
     */
    private $nu_notaaprovacao;

    /**
     * @var boolean $bl_calcular
     * @Column(name="bl_calcular", type="boolean", nullable=false)
     */
    private $bl_calcular;

    /**
     * @var boolean $bl_aprovado
     * @Column(name="bl_aprovado", type="boolean", nullable=false)
     */
    private $bl_aprovado;

    /**
     * @var integer $nu_percentualfinal
     * @Column(name="nu_percentualfinal", type="integer", nullable=true)
     */
    private $nu_percentualfinal;

    //*****Geters e Seters*****

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }

    /**
     * @return int
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    /**
     * @return int
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_alocacao
     */
    public function setId_alocacao($id_alocacao) {
        $this->id_alocacao = $id_alocacao;
    }

    /**
     * @return int
     */
    public function getId_alocacao() {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    /**
     * @return int
     */
    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    /**
     * @param int $id_avaliacaoatividade
     */
    public function setId_avaliacaoatividade($id_avaliacaoatividade) {
        $this->id_avaliacaoatividade = $id_avaliacaoatividade;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoatividade(){
        return $this->id_avaliacaoatividade;
    }

    /**
     * @param int $id_disciplinaatividade
     */
    public function setId_disciplinaatividade($id_disciplinaatividade) {
        $this->id_disciplinaatividade = $id_disciplinaatividade;
    }

    /**
     * @return int
     */
    public function getId_disciplinaatividade(){
        return $this->id_disciplinaatividade;
    }

    /**
     * @param string $st_notaatividade
     */
    public function setSt_notaatividade($st_notaatividade) {
        $this->st_notaatividade = $st_notaatividade;
    }

    /**
     * @return string
     */
    public function getSt_notaatividade(){
        return $this->st_notaatividade;
    }

    /**
     * @param int $id_tiponotaatividade
     */
    public function setId_tiponotaatividade($id_tiponotaatividade) {
        $this->id_tiponotaatividade = $id_tiponotaatividade;
    }

    /**
     * @return int
     */
    public function getId_tiponotaatividade(){
        return $this->id_tiponotaatividade;
    }

    /**
     * @param string $st_avaliacaoatividade
     */
    public function setSt_avaliacaoatividade($st_avaliacaoatividade) {
        $this->st_avaliacaoatividade = $st_avaliacaoatividade;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaoatividade(){
        return $this->st_avaliacaoatividade;
    }

    /**
     * @param int $nu_notamaxatividade
     */
    public function setNu_notamaxatividade($nu_notamaxatividade) {
        $this->nu_notamaxatividade = $nu_notamaxatividade;
    }

    /**
     * @return int
     */
    public function getNu_notamaxatividade() {
        return $this->nu_notamaxatividade;
    }

    //***************


    /**
     * @param int $id_avaliacaorecuperacao
     */
    public function setId_avaliacaorecuperacao($id_avaliacaorecuperacao) {
        $this->id_avaliacaorecuperacao = $id_avaliacaorecuperacao;
    }

    /**
     * @return int
     */
    public function getId_avaliacaorecuperacao(){
        return $this->id_avaliacaorecuperacao;
    }

    /**
     * @param int $id_disciplinarecuperacao
     */
    public function setId_disciplinarecuperacao($id_disciplinarecuperacao) {
        $this->id_disciplinarecuperacao = $id_disciplinarecuperacao;
    }

    /**
     * @return int
     */
    public function getId_disciplinarecuperacao(){
        return $this->id_disciplinarecuperacao;
    }

    /**
     * @param string $st_notarecuperacao
     */
    public function setSt_notarecuperacao($st_notarecuperacao) {
        $this->st_notarecuperacao = $st_notarecuperacao;
    }

    /**
     * @return string
     */
    public function getSt_notarecuperacao(){
        return $this->st_notarecuperacao;
    }

    /**
     * @param int $id_tiponotarecuperacao
     */
    public function setId_tiponotarecuperacao($id_tiponotarecuperacao) {
        $this->id_tiponotarecuperacao = $id_tiponotarecuperacao;
    }

    /**
     * @return int
     */
    public function getId_tiponotarecuperacao(){
        return $this->id_tiponotarecuperacao;
    }

    /**
     * @param string $st_avaliacaorecuperacao
     */
    public function setSt_avaliacaorecuperacao($st_avaliacaorecuperacao) {
        $this->st_avaliacaorecuperacao = $st_avaliacaorecuperacao;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaorecuperacao(){
        return $this->st_avaliacaorecuperacao;
    }

    /**
     * @param int $nu_notamaxrecuperacao
     */
    public function setNu_notamaxrecuperacao($nu_notamaxrecuperacao) {
        $this->nu_notamaxrecuperacao = $nu_notamaxrecuperacao;
    }

    /**
     * @return int
     */
    public function getNu_notamaxrecuperacao() {
        return $this->nu_notamaxrecuperacao;
    }

    //*******************

    /**
     * @param int $id_avaliacaofinal
     */
    public function setId_avaliacaofinal($id_avaliacaofinal) {
        $this->id_avaliacaofinal = $id_avaliacaofinal;
    }

    /**
     * @return int
     */
    public function getId_avaliacaofinal() {
        return $this->id_avaliacaofinal;
    }

    /**
     * @param int $id_disciplinafinal
     */
    public function setId_disciplinafinal($id_disciplinafinal) {
        $this->id_disciplinafinal = $id_disciplinafinal;
    }

    /**
     * @return int
     */
    public function getId_disciplinafinal() {
        return $this->id_disciplinafinal;
    }

    /**
     * @param string $st_disciplinafinal
     */
    public function setSt_disciplinafinal($st_disciplinafinal) {
        $this->st_disciplinafinal = $st_disciplinafinal;
    }

    /**
     * @return string
     */
    public function getSt_disciplinafinal() {
        return $this->st_disciplinafinal;
    }

    /**
     * @param string $st_notafinal
     */
    public function setSt_notafinal($st_notafinal) {
        $this->st_notafinal = $st_notafinal;
    }

    /**
     * @return string
     */
    public function getSt_notafinal() {
        return $this->st_notafinal;
    }

    /**
     * @param int $id_tipoavaliacao
     */
    public function setId_tipoavaliacao($id_tipoavaliacao) {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
    }

    /**
     * @return int
     */
    public function getId_tipoavaliacao() {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param int $id_tiponotafinal
     */
    public function setId_tiponotafinal($id_tiponotafinal) {
        $this->id_tiponotafinal = $id_tiponotafinal;
    }

    /**
     * @return int
     */
    public function getId_tiponotafinal() {
        return $this->id_tiponotafinal;
    }

    /**
     * @param string $st_avaliacaofinal
     */
    public function setSt_avaliacaofinal($st_avaliacaofinal) {
        $this->st_avaliacaofinal = $st_avaliacaofinal;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaofinal(){
        return $this->st_avaliacaofinal;
    }

    /**
     * @param int $nu_notamaxfinal
     */
    public function setNu_notamaxfinal($nu_notamaxfinal) {
        $this->nu_notamaxfinal = $nu_notamaxfinal;
    }

    /**
     * @return int
     */
    public function getNu_notamaxfinal() {
        return $this->nu_notamaxfinal;
    }

    //**********************

    /**
     * @param int $id_avaliacaoead
     */
    public function setId_avaliacaoead($id_avaliacaoead) {
        $this->id_avaliacaoead = $id_avaliacaoead;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoead() {
        return $this->id_avaliacaoead;
    }

    /**
     * @return int
     */
    public function getId_disciplinaead() {
        return $this->id_disciplinaead;
    }

    /**
     * @param int $id_disciplinaead
     */
    public function setId_disciplinaead($id_disciplinaead) {
        $this->id_disciplinaead = $id_disciplinaead;
    }

    /**
     * @param string $st_notaead
     */
    public function setSt_notaead($st_notaead) {
        $this->st_notaead = $st_notaead;
    }

    /**
     * @return string
     */
    public function getSt_notaead() {
        return $this->st_notaead;
    }

    /**
     * @param int $id_tiponotaead
     */
    public function setId_tiponotaead($id_tiponotaead) {
        $this->id_tiponotaead = $id_tiponotaead;
    }

    /**
     * @return int
     */
    public function getId_tiponotaead() {
        return $this->id_tiponotaead;
    }

    /**
     * @param string $st_avaliacaoead
     */
    public function setSt_avaliacaoead($st_avaliacaoead) {
        $this->st_avaliacaoead = $st_avaliacaoead;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaoead(){
        return $this->st_avaliacaoead;
    }

    /**
     * @param int $nu_notamaxead
     */
    public function setNu_notamaxead($nu_notamaxead) {
        $this->nu_notamaxead = $nu_notamaxead;
    }

    /**
     * @return int
     */
    public function getNu_notamaxead() {
        return $this->nu_notamaxead;
    }

    /**
     * @param int $id_avaliacaoaluno
     */
    public function setId_avaliacaoaluno($id_avaliacaoaluno) {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoaluno() {
        return $this->id_avaliacaoaluno;
    }

    //************************

    /**
     * @param int $id_avaliacaotcc
     */
    public function setId_avaliacaotcc($id_avaliacaotcc) {
        $this->id_avaliacaotcc = $id_avaliacaotcc;
    }

    /**
     * @return int
     */
    public function getId_avaliacaotcc() {
        return $this->id_avaliacaotcc;
    }

    /**
     * @param int $id_disciplinatcc
     */
    public function setId_disciplinatcc($id_disciplinatcc) {
        $this->id_disciplinatcc = $id_disciplinatcc;
    }

    /**
     * @return int
     */
    public function getId_disciplinatcc() {
        return $this->id_disciplinatcc;
    }

    /**
     * @param string $st_notatcc
     */
    public function setSt_notatcc($st_notatcc) {
        $this->st_notatcc = $st_notatcc;
    }

    /**
     * @return string
     */
    public function getSt_notatcc() {
        return $this->st_notatcc;
    }

    /**
     * @param string $st_avaliacaotcc
     */
    public function setSt_avaliacaotcc($st_avaliacaotcc) {
        $this->st_avaliacaotcc = $st_avaliacaotcc;
    }

    /**
     * @return string
     */
    public function getSt_avaliacaotcc(){
        return $this->st_avaliacaotcc;
    }

    /**
     * @param int $nu_notamaxtcc
     */
    public function setNu_notamaxtcc($nu_notamaxtcc) {
        $this->nu_notamaxtcc = $nu_notamaxtcc;
    }

    /**
     * @return int
     */
    public function getNu_notamaxtcc() {
        return $this->nu_notamaxtcc;
    }

    //****************

    /**
     * @param int $nu_notatotal
     */
    public function setNu_notatotal($nu_notatotal) {
        $this->nu_notatotal = $nu_notatotal;
    }

    /**
     * @return int
     */
    public function getNu_notatotal() {
        return $this->nu_notatotal;
    }

    /**
     * @param int $nu_notaaprovacao
     */
    public function setNu_notaaprovacao($nu_notaaprovacao) {
        $this->nu_notaaprovacao = $nu_notaaprovacao;
    }

    /**
     * @return int
     */
    public function getNu_notaaprovacao() {
        return $this->nu_notaaprovacao;
    }

    /**
     * @param boolean $bl_calcular
     */
    public function setBl_calcular($bl_calcular) {
        $this->bl_calcular = $bl_calcular;
    }

    /**
     * @return boolean
     */
    public function getBl_calcular() {
        return $this->bl_calcular;
    }

    /**
     * @param boolean $bl_aprovado
     */
    public function setBl_aprovado($bl_aprovado) {
        $this->bl_aprovado = $bl_aprovado;
    }

    /**
     * @return boolean
     */
    public function getBl_aprovado() {
        return $this->bl_aprovado;
    }

    /**
     * @param int $nu_percentualfinal
     */
    public function setNu_percentualfinal($nu_percentualfinal) {
        $this->nu_percentualfinal = $nu_percentualfinal;
    }

    /**
     * @return int
     */
    public function getNu_percentualfinal() {
        return $this->nu_percentualfinal;
    }

}

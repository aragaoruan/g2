<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @SWG\Definition(required={"st_login", "st_nomecompleto"}, @SWG\Xml(name="Usuario"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuario")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> - 2013-11-18
 * @EntityLog
 */
class Usuario extends G2Entity
{

    /**
     * ID do Usuário
     * @SWG\Property(format="integer")
     * @var integer - Id do Usuario
     * @Column(name="id_usuario", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @EntityLog
     */
    private $id_usuario;

    /**
     * @SWG\Property(example="nome.sobrenome")
     * @Column(name="st_login", type="string",length=40,nullable=false)
     * @var string - Nome de Usuário
     */
    private $st_login;

    /**
     * @SWG\Property()
     * @Column(name="st_senha", type="string",length=32,nullable=true)
     * @var string - Senha do usuário
     */
    private $st_senha;

    /**
     * @SWG\Property()
     * @Column(name="st_nomecompleto", type="string",length=300,nullable=false)
     * @var string - Nome completo
     */
    private $st_nomecompleto;

    /**
     * @SWG\Property()
     * @var RegistroPessoa - id_registropessoa encontrada na tabela tb_registropessoa
     * @ManyToOne(targetEntity="RegistroPessoa")
     * @JoinColumn(name="id_registropessoa", nullable=true, referencedColumnName="id_registropessoa")
     */
    private $id_registropessoa;

    /**
     * @SWG\Property()
     * @Column(name="st_cpf", type="string", length=11, nullable=true)
     * @var string - CPF do Usuário formato 99999999999 sem formatação de pontos e/ou traços
     */
    private $st_cpf;

    /**
     * @SWG\Property()
     * @var Boolean - Situação do cadastro, no G2S não exclúimos dados fisicamente, este campo marca quando o registro está ativo ou inativo
     * @Column(name="bl_ativo", type="boolean", nullable=true);
     */
    private $bl_ativo;

    /**
     * @SWG\Property()
     * @var Boolean - Se 1 (true) exige a redefinição de senha por parte do Usuário
     * @Column(name="bl_redefinicaosenha", type="boolean", nullable=true);
     */
    private $bl_redefinicaosenha;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariopai", referencedColumnName="id_usuario")
     * */
    private $id_usuariopai;

    /**
     * @SWG\Property()
     * @ManyToOne(targetEntity="Titulacao")
     * @var integer - Título do Usuário encontrado na tb_titulacao (Sr. , Sra. etc)
     * @JoinColumn(name="id_titulacao", nullable=true, referencedColumnName="id_titulacao")
     */
    private $id_titulacao;

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_login ()
    {
        return $this->st_login;
    }

    public function setSt_login ($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    public function getSt_senha ()
    {
        return $this->st_senha;
    }

    public function setSt_senha ($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getId_registropessoa ()
    {
        return $this->id_registropessoa;
    }

    public function setId_registropessoa ($registropessoa)
    {
        $this->id_registropessoa = $registropessoa;
        return $this;
    }

    public function getSt_cpf ()
    {
        return $this->st_cpf;
    }

    public function setSt_cpf ($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getBl_redefinicaosenha ()
    {
        return $this->bl_redefinicaosenha;
    }

    public function setBl_redefinicaosenha ($bl_redefinicaosenha)
    {
        $this->bl_redefinicaosenha = $bl_redefinicaosenha;
        return $this;
    }

    public function getId_usuariopai ()
    {
        return $this->id_usuariopai;
    }

    public function setId_usuariopai ($usuariopai)
    {
        $this->id_usuariopai = $usuariopai;
        return $this;
    }

    public function getId_titulacao()
    {
        return $this->id_titulacao;
    }

    public function setId_titulacao($id_titulacao)
    {
        $this->id_titulacao = $id_titulacao;
    }


}
<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_areaprojetosala")
 * @Entity
 * @EntityLog
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class AreaProjetoSala
{
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=true, length=4)
     */
    private $id_nivelensino;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_areaprojetosala
     * @Column(name="id_areaprojetosala", type="integer", nullable=false, length=4)
     */
    private $id_areaprojetosala;
    /**
     * @var integer $nu_diasacesso
     * @Column(name="nu_diasacesso", type="integer", nullable=true, length=4)
     */
    private $nu_diasacesso;
    /**
     * @var string $st_referencia
     * @Column(name="st_referencia", type="string", nullable=true, length=200)
     */
    private $st_referencia;


    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_nivelensino
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param id_nivelensino
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return integer id_areaprojetosala
     */
    public function getId_areaprojetosala()
    {
        return $this->id_areaprojetosala;
    }

    /**
     * @param id_areaprojetosala
     */
    public function setId_areaprojetosala($id_areaprojetosala)
    {
        $this->id_areaprojetosala = $id_areaprojetosala;
        return $this;
    }

    /**
     * @return integer nu_diasacesso
     */
    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    /**
     * @param nu_diasacesso
     */
    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
        return $this;
    }

    /**
     * @return string st_referencia
     */
    public function getSt_referencia()
    {
        return $this->st_referencia;
    }

    /**
     * @param st_referencia
     */
    public function setSt_referencia($st_referencia)
    {
        $this->st_referencia = $st_referencia;
        return $this;
    }

}
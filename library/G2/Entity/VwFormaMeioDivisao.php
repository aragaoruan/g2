<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_formameiodivisao")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwFormaMeioDivisao
{

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;

    /**
     * @var integer $id_tipodivisaofinanceira
     * @Column(name="id_tipodivisaofinanceira", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipodivisaofinanceira;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_meiopagamento;

    /**
     * @var integer $nu_parcelaquantidademin
     * @Column(name="nu_parcelaquantidademin", type="integer", nullable=true, length=4)
     */
    private $nu_parcelaquantidademin;

    /**
     * @var integer $nu_parcelaquantidademax
     * @Column(name="nu_parcelaquantidademax", type="integer", nullable=true, length=4)
     */
    private $nu_parcelaquantidademax;

    /**
     * @var integer $nu_maxparcelas
     * @Column(name="nu_maxparcelas", type="integer", nullable=false, length=4)
     */
    private $nu_maxparcelas;

    /**
     * @var integer $id_formapagamentoaplicacao
     * @Column(name="id_formapagamentoaplicacao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamentoaplicacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", nullable=true, length=17)
     */
    private $nu_juros;

    /**
     * @var string $st_formapagamento
     * @Column(name="st_formapagamento", type="string", nullable=false, length=255)
     */
    private $st_formapagamento;

    /**
     * @var string $st_tipodivisaofinanceira
     * @Column(name="st_tipodivisaofinanceira", type="string", nullable=false, length=255)
     */
    private $st_tipodivisaofinanceira;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=false, length=255)
     */
    private $st_meiopagamento;

    /**
     * @return integer
     */
    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    /**
     * @return integer
     */
    public function getId_tipodivisaofinanceira ()
    {
        return $this->id_tipodivisaofinanceira;
    }

    /**
     * @return integer
     */
    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @return integer
     */
    public function getNu_parcelaquantidademin ()
    {
        return $this->nu_parcelaquantidademin;
    }

    /**
     * @return integer
     */
    public function getNu_parcelaquantidademax ()
    {
        return $this->nu_parcelaquantidademax;
    }

    /**
     * @return integer
     */
    public function getNu_maxparcelas ()
    {
        return $this->nu_maxparcelas;
    }

    /**
     * @return integer
     */
    public function getId_formapagamentoaplicacao ()
    {
        return $this->id_formapagamentoaplicacao;
    }

    /**
     * @return integer
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @return decimal
     */
    public function getNu_juros ()
    {
        return $this->nu_juros;
    }

    /**
     * @return string
     */
    public function getSt_formapagamento ()
    {
        return $this->st_formapagamento;
    }

    /**
     * @return string
     */
    public function getSt_tipodivisaofinanceira ()
    {
        return $this->st_tipodivisaofinanceira;
    }

    /**
     * @return string
     */
    public function getSt_meiopagamento ()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param integer $id_formapagamento
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setId_formapagamento ($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    /**
     * @param integer $id_tipodivisaofinanceira
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setId_tipodivisaofinanceira ($id_tipodivisaofinanceira)
    {
        $this->id_tipodivisaofinanceira = $id_tipodivisaofinanceira;
        return $this;
    }

    /**
     * @param integer $id_meiopagamento
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setId_meiopagamento ($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @param integer $nu_parcelaquantidademin
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setNu_parcelaquantidademin ($nu_parcelaquantidademin)
    {
        $this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
        return $this;
    }

    /**
     * @param integer $nu_parcelaquantidademax
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setNu_parcelaquantidademax ($nu_parcelaquantidademax)
    {
        $this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
        return $this;
    }

    /**
     * @param integer $nu_maxparcelas
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setNu_maxparcelas ($nu_maxparcelas)
    {
        $this->nu_maxparcelas = $nu_maxparcelas;
        return $this;
    }

    /**
     * @param integer $id_formapagamentoaplicacao
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setId_formapagamentoaplicacao ($id_formapagamentoaplicacao)
    {
        $this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
        return $this;
    }

    /**
     * @param integer $id_entidade
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param decimal $nu_juros
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setNu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    /**
     * @param string $st_formapagamento
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setSt_formapagamento ($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
        return $this;
    }

    /**
     * 
     * @param string $st_tipodivisaofinanceira
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setSt_tipodivisaofinanceira ($st_tipodivisaofinanceira)
    {
        $this->st_tipodivisaofinanceira = $st_tipodivisaofinanceira;
        return $this;
    }

    /**
     * @param string $st_meiopagamento
     * @return \G2\Entity\VwFormaMeioDivisao
     */
    public function setSt_meiopagamento ($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

}

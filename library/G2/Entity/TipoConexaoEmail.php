<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoconexaoemail")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoConexaoEmail
{

    /**
     *
     * @var integer $id_tipoconexaoemail
     * @Column(name="id_tipoconexaoemail", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoconexaoemail;

    /**
     *
     * @var string $st_tipoconexaoemail
     * @Column(name="st_tipoconexaoemail", type="string", nullable=false, length=20) 
     */
    private $st_tipoconexaoemail;

    public function getId_tipoconexaoemail ()
    {
        return $this->id_tipoconexaoemail;
    }

    public function setId_tipoconexaoemail ($id_tipoconexaoemail)
    {
        $this->id_tipoconexaoemail = $id_tipoconexaoemail;
        return $this;
    }

    public function getSt_tipoconexaoemail ()
    {
        return $this->st_tipoconexaoemail;
    }

    public function setSt_tipoconexaoemail ($st_tipoconexaoemail)
    {
        $this->st_tipoconexaoemail = $st_tipoconexaoemail;
        return $this;
    }

}
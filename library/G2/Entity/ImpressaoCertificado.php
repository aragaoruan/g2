<?php

namespace G2\Entity;

use G2\G2Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_impressaocertificado")
 * @Entity
 * @EntityLog
 * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
 */

class ImpressaoCertificado extends G2Entity
{
    /**
     * @var integer $id_impressaocertificado
     * @Column(name="id_impressaocertificado", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     * @Id
     */
    private $id_impressaocertificado;

    /**
     * @var text $st_textocertificado
     * @Column(name="st_textocertificado", type="text", nullable=true, length=16)
     */
    private $st_textocertificado;

    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var \DateTime $dt_inativo
     * @Column(name="dt_inativo", type="datetime2", nullable=true, length=8)
     */
    private $dt_inativo;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @return int
     */
    public function getId_impressaocertificado()
    {
        return $this->id_impressaocertificado;
    }

    /**
     * @param int $id_impressaocertificado
     */
    public function setId_impressaocertificado($id_impressaocertificado)
    {
        $this->id_impressaocertificado = $id_impressaocertificado;
    }

    /**
     * @return text
     */
    public function getSt_textocertificado()
    {
        return $this->st_textocertificado;
    }

    /**
     * @param text $st_textocertificado
     */
    public function setSt_textocertificado($st_textocertificado)
    {
        $this->st_textocertificado = $st_textocertificado;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \DateTime
     */
    public function getDt_inativo()
    {
        return $this->dt_inativo;
    }

    /**
     * @param \DateTime $dt_inativo
     */
    public function setDt_inativo($dt_inativo)
    {
        $this->dt_inativo = $dt_inativo;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }



}
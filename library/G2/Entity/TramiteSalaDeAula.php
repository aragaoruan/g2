<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tramitesaladeaula")
 * @Entity
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 * Class TramiteSalaDeAula
 * @package G2\Entity
 */
class TramiteSalaDeAula extends G2Entity
{
    /**
     * @var integer $id_tramitesaladeaula
     * @Column(name="id_tramitesaladeaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramitesaladeaula;


    /**
     * @var Tramite $id_tramite
     * @ManyToOne(targetEntity="Tramite")
     * @JoinColumn(name="id_tramite", referencedColumnName="id_tramite")
     */
    private $id_tramite;

    /**
     * @var SalaDeAula $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula")
     */
    private $id_saladeaula;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;
    /**
     * @return int
     */
    public function getId_tramitesaladeaula()
    {
        return $this->id_tramitesaladeaula;
    }

    /**
     * @param int $id_tramitesaladeaula
     * @return TramiteSalaDeAula
     */
    public function setId_tramitesaladeaula($id_tramitesaladeaula)
    {
        $this->id_tramitesaladeaula = $id_tramitesaladeaula;
        return $this;
    }

    /**
     * @return Tramite
     */
    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    /**
     * @param Tramite $id_tramite
     * @return TramiteSalaDeAula
     */
    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
        return $this;
    }

    /**
     * @return SalaDeAula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param SalaDeAula $id_saladeaula
     * @return TramiteSalaDeAula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

}

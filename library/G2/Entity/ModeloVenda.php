<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_modelovenda")
 * @Entity
 * @author Elcio Mauro Guimarães
 */
use G2\G2Entity;

class ModeloVenda extends G2Entity
{
	
	
	const PADRAO 		= 1;
	const ASSINATURA 	= 2;
	
    /**
     *
     * @var integer $id_modelovenda
     * @Column(name="id_modelovenda", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_modelovenda;

    /**
     *
     * @var string $st_modelovenda
     * @Column(name="st_modelovenda", type="string", nullable=true, length=255) 
     */
    private $st_modelovenda;

    
    
    
    public function getId_modelovenda ()
    {
        return $this->id_modelovenda;
    }

    public function setId_modelovenda ($id_modelovenda)
    {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    public function getSt_modelovenda ()
    {
        return $this->st_modelovenda;
    }

    public function setSt_modelovenda ($st_modelovenda)
    {
        $this->st_modelovenda = $st_modelovenda;
        return $this;
    }

}
<?php


namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entregamaterial")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.ocm.br>
 */
class VwEntregaMaterial {

    /**
     * @var date $dt_criacaopacote
     * @Column(name="dt_criacaopacote", type="date", nullable=true)
     */
    private $dt_criacaopacote;
    /**
     * @var date $dt_entrega
     * @Column(name="dt_entrega", type="date", nullable=true)
     */
    private $dt_entrega;
    /**
     * @var date $dt_devolucao
     * @Column(name="dt_devolucao", type="date", nullable=true, length=8)
     */
    private $dt_devolucao;
    /**
     * @var integer $id_entregamaterial
     * @Column(name="id_entregamaterial", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entregamaterial;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_lotematerial
     * @Column(name="id_lotematerial", type="integer", nullable=true, length=4)
     */
    private $id_lotematerial;
    /**
     * @var integer $id_pacote
     * @Column(name="id_pacote", type="integer", nullable=false, length=4)
     */
    private $id_pacote;
    /**
     * @var integer $id_situacaopacote
     * @Column(name="id_situacaopacote", type="integer", nullable=true, length=4)
     */
    private $id_situacaopacote;
    /**
     * @var integer $id_situacaoentrega
     * @Column(name="id_situacaoentrega", type="integer", nullable=false, length=4)
     */
    private $id_situacaoentrega;
    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false, length=4)
     */
    private $id_itemdematerial;
    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_itemdematerial
     * @Column(name="st_itemdematerial", type="string", nullable=false, length=255)
     */
    private $st_itemdematerial;

    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @var string $st_situacaopacote
     * @Column(name="st_situacaopacote", type="string", nullable=false, length=300)
     */
    private $st_situacaopacote;

    /**
     * @var string $st_codrastreamento
     * @Column(name="st_codrastreamento", type="string")
     */
    private $st_codrastreamento;

    /**
     * @return date dt_criacaopacote
     */
    public function getDt_criacaopacote() {
        return $this->dt_criacaopacote;
    }


    public function setDt_criacaopacote($dt_criacaopacote) {
        $this->dt_criacaopacote = $dt_criacaopacote;
        return $this;
    }


    public function getDt_entrega() {
        return $this->dt_entrega;
    }

    public function setDt_entrega($dt_entrega) {
        $this->dt_entrega = $dt_entrega;
        return $this;
    }


    public function getDt_devolucao() {
        return $this->dt_devolucao;
    }


    public function setDt_devolucao($dt_devolucao) {
        $this->dt_devolucao = $dt_devolucao;
        return $this;
    }

    /**
     * @return integer id_entregamaterial
     */
    public function getId_entregamaterial() {
        return $this->id_entregamaterial;
    }

    /**
     * @param id_entregamaterial
     */
    public function setId_entregamaterial($id_entregamaterial) {
        $this->id_entregamaterial = $id_entregamaterial;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_lotematerial
     */
    public function getId_lotematerial() {
        return $this->id_lotematerial;
    }

    /**
     * @param id_lotematerial
     */
    public function setId_lotematerial($id_lotematerial) {
        $this->id_lotematerial = $id_lotematerial;
        return $this;
    }

    /**
     * @return integer id_pacote
     */
    public function getId_pacote() {
        return $this->id_pacote;
    }

    /**
     * @param id_pacote
     */
    public function setId_pacote($id_pacote) {
        $this->id_pacote = $id_pacote;
        return $this;
    }

    /**
     * @return integer id_situacaopacote
     */
    public function getId_situacaopacote() {
        return $this->id_situacaopacote;
    }

    /**
     * @param id_situacaopacote
     */
    public function setId_situacaopacote($id_situacaopacote) {
        $this->id_situacaopacote = $id_situacaopacote;
        return $this;
    }

    /**
     * @return integer id_situacaoentrega
     */
    public function getId_situacaoentrega() {
        return $this->id_situacaoentrega;
    }

    /**
     * @param id_situacaoentrega
     */
    public function setId_situacaoentrega($id_situacaoentrega) {
        $this->id_situacaoentrega = $id_situacaoentrega;
        return $this;
    }

    /**
     * @return integer id_itemdematerial
     */
    public function getId_itemdematerial() {
        return $this->id_itemdematerial;
    }

    /**
     * @param id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial) {
        $this->id_itemdematerial = $id_itemdematerial;
        return $this;
    }

    /**
     * @return integer id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_itemdematerial
     */
    public function getSt_itemdematerial() {
        return $this->st_itemdematerial;
    }

    /**
     * @param st_itemdematerial
     */
    public function setSt_itemdematerial($st_itemdematerial) {
        $this->st_itemdematerial = $st_itemdematerial;
        return $this;
    }


    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return string st_areaconhecimento
     */
    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return string st_situacaopacote
     */
    public function getSt_situacaopacote() {
        return $this->st_situacaopacote;
    }

    /**
     * @param st_situacaopacote
     */
    public function setSt_situacaopacote($st_situacaopacote) {
        $this->st_situacaopacote = $st_situacaopacote;
        return $this;
    }

    /**
     * @return string st_codrastreamento
     */
    public function getSt_codrastreamento() {
        return $this->st_codrastreamento;
    }

    /**
     * @param st_codrastreamento
     */
    public function setSt_codrastreamento($st_codrastreamento) {
        $this->st_codrastreamento = $st_codrastreamento;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_orientacaotexto")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class OrientacaoTexto
{

    /**
     * @var integer $id_orientacaotexto
     * @Column(name="id_orientacaotexto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_orientacaotexto;

    /**
     * @var string $st_orientacaotexto
     * @Column(name="st_orientacaotexto", type="string", nullable=false, length=15)
     */
    private $st_orientacaotexto;

    public function getId_orientacaotexto ()
    {
        return $this->id_orientacaotexto;
    }

    public function setId_orientacaotexto ($id_orientacaotexto)
    {
        $this->id_orientacaotexto = $id_orientacaotexto;
        return $this;
    }

    public function getSt_orientacaotexto ()
    {
        return $this->st_orientacaotexto;
    }

    public function setSt_orientacaotexto ($st_orientacaotexto)
    {
        $this->st_orientacaotexto = $st_orientacaotexto;
        return $this;
    }

}
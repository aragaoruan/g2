<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_classeafiliacao")
 * @Entity
 */
class ClasseAfiliacao
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_classeafiliacao
     * @Column(name="id_classeafiliacao", type="integer", nullable=false, length=4)
     */
    private $id_classeafiliacao;

    /**
     * @var string $st_classeafiliacao
     * @Column(name="st_classeafiliacao", type="string", nullable=true, length=255)
     */
    private $st_classeafiliacao;

    /**
     * @return int
     */
    public function getId_classeafiliacao()
    {
        return $this->id_classeafiliacao;
    }

    /**
     * @param int $id_classeafiliacao
     * @return $this
     */
    public function setId_classeafiliacao($id_classeafiliacao)
    {
        $this->id_classeafiliacao = $id_classeafiliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_classeafiliacao()
    {
        return $this->st_classeafiliacao;
    }

    /**
     * @param string $st_classeafiliacao
     * @return $this
     */
    public function setSt_classeafiliacao($st_classeafiliacao)
    {
        $this->st_classeafiliacao = $st_classeafiliacao;
        return $this;
    }
}
<?php

namespace G2\Entity;

/**
 * Classe criada no teste, MAIS UTILIZADA SOMENTE NO TESTE, favor ao aplica-la
 * retirar este comentario (Rafael Bruno).
 * 
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_fundamentolegal")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class FundamentoLegal
{

    /**
     * @var date $dt_publicacao
     * @Column(name="dt_publicacao", type="date", nullable=false, length=3)
     */
    private $dt_publicacao;
    /**
     * @var date $dt_vigencia
     * @Column(name="dt_vigencia", type="date", nullable=true, length=3)
     */
    private $dt_vigencia;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_fundamentolegal
     * @Column(name="id_fundamentolegal", type="integer", nullable=false, length=4)
     */
    private $id_fundamentolegal;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_tipofundamentolegal
     * @Column(name="id_tipofundamentolegal", type="integer", nullable=false, length=4)
     */
    private $id_tipofundamentolegal;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=true, length=4)
     */
    private $id_nivelensino;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=false, length=255)
     */
    private $nu_numero;
    /**
     * @var string $st_orgaoexped
     * @Column(name="st_orgaoexped", type="string", nullable=true, length=255)
     */
    private $st_orgaoexped;


    /**
     * @return date dt_publicacao
     */
    public function getDt_publicacao()
    {
        return $this->dt_publicacao;
    }

    /**
     * @param dt_publicacao
     */
    public function setDt_publicacao($dt_publicacao)
    {
        $this->dt_publicacao = $dt_publicacao;
        return $this;
    }

    /**
     * @return date dt_vigencia
     */
    public function getDt_vigencia()
    {
        return $this->dt_vigencia;
    }

    /**
     * @param dt_vigencia
     */
    public function setDt_vigencia($dt_vigencia)
    {
        $this->dt_vigencia = $dt_vigencia;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_fundamentolegal
     */
    public function getId_fundamentolegal()
    {
        return $this->id_fundamentolegal;
    }

    /**
     * @param id_fundamentolegal
     */
    public function setId_fundamentolegal($id_fundamentolegal)
    {
        $this->id_fundamentolegal = $id_fundamentolegal;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_tipofundamentolegal
     */
    public function getId_tipofundamentolegal()
    {
        return $this->id_tipofundamentolegal;
    }

    /**
     * @param id_tipofundamentolegal
     */
    public function setId_tipofundamentolegal($id_tipofundamentolegal)
    {
        $this->id_tipofundamentolegal = $id_tipofundamentolegal;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_nivelensino
     */
    public function getId_nivelensino()
    {
        return $this->id_nivelensino;
    }

    /**
     * @param id_nivelensino
     */
    public function setId_nivelensino($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string nu_numero
     */
    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param nu_numero
     */
    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string st_orgaoexped
     */
    public function getSt_orgaoexped()
    {
        return $this->st_orgaoexped;
    }

    /**
     * @param st_orgaoexped
     */
    public function setSt_orgaoexped($st_orgaoexped)
    {
        $this->st_orgaoexped = $st_orgaoexped;
        return $this;
    }


} 
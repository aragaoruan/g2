<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadecartaoconfig")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class EntidadeCartaoConfig
{

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_cartaoconfig
     * @Column(name="id_cartaoconfig", type="integer", nullable=false, length=4)
     */
    private $id_cartaoconfig;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_entidadefinanceiro
     * @Column(name="id_entidadefinanceiro", type="integer", nullable=false, length=4)
     */
    private $id_entidadefinanceiro;

    public function setId_cartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
        return $this;
    }

    public function getId_cartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    public function setId_entidadefinanceiro($id_entidadefinanceiro)
    {
        $this->id_entidadefinanceiro = $id_entidadefinanceiro;
        return $this;
    }

    public function getId_entidadefinanceiro()
    {
        return $this->id_entidadefinanceiro;
    }
}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_entidaderelacao")
 * @Entity
 * @author Arthur Luiz Lara Quites
 */
class VwEntidadeRelacao
{
    /**
     * @var integer $id_entidade
     * @Id
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=false, length=4)
     */
    private $id_entidadepai;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_entidadeclasse
     * @Column(name="id_entidadeclasse", type="integer", nullable=false, length=4)
     */
    private $id_entidadeclasse;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_tipoendereco
     * @Column(name="st_tipoendereco", type="string", nullable=false, length=255)
     */
    private $st_tipoendereco;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true, length=500)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;

    /**
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=false, length=255)
     */
    private $st_nomemunicipio;

    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12)
     */
    private $st_cep;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_wschave
     * @Column(name="st_wschave", type="string", nullable=true, length=40)
     */
    private $st_wschave;

    /**
     * @var string $st_cnpj
     * @Column(name="st_cnpj", type="string", nullable=true, length=15)
     */
    private $st_cnpj;

    /**
     * @var string $st_uf
     * @Column(name="st_uf", type="string", nullable=false, length=100)
     */
    private $st_uf;

    /**
     * @var decimal $id_municipio
     * @Column(name="id_municipio", type="decimal", nullable=false, length=17)
     */
    private $id_municipio;

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_entidadepai
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param $id_entidadepai
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_entidadeclasse
     */
    public function getId_entidadeclasse()
    {
        return $this->id_entidadeclasse;
    }

    /**
     * @param $id_entidadeclasse
     */
    public function setId_entidadeclasse($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }


    /**
     * @return string st_tipoendereco
     */
    public function getSt_tipoendereco()
    {
        return $this->st_tipoendereco;
    }

    /**
     * @param $st_tipoendereco
     */
    public function setSt_tipoendereco($st_tipoendereco)
    {
        $this->st_tipoendereco = $st_tipoendereco;
        return $this;
    }

    /**
     * @return string $st_endereco
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return string st_complemento
     */
    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param $st_complemento
     */
    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * @return string nu_numero
     */
    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param $nu_numero
     */
    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string st_cidade
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param $st_cidade
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    /**
     * @return string st_nomemunicipio
     */
    public function getSt_nomemunicipio()
    {
        return $this->st_nomemunicipio;
    }

    /**
     * @param $st_nomemunicipio
     */
    public function setSt_nomemunicipio($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    /**
     * @return string st_cep
     */
    public function getSt_cep()
    {
        return $this->st_cep;
    }

    /**
     * @param $st_cep
     */
    public function setSt_cep($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    /**
     * @return string sg_uf
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return string st_razaosocial
     */
    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    /**
     * @param $st_razaosocial
     */
    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    /**
     * @return string st_bairro
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param $st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    /**
     * @return string st_wschave
     */
    public function getSt_wschave()
    {
        return $this->st_wschave;
    }

    /**
     * @param $st_wschave
     */
    public function setSt_wschave($st_wschave)
    {
        $this->st_wschave = $st_wschave;
        return $this;
    }

    /**
     * @return string st_cnpj
     */
    public function getSt_cnpj()
    {
        return $this->st_cnpj;
    }

    /**
     * @param $st_cnpj
     */
    public function setSt_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
        return $this;
    }

    /**
     * @return decimal id_municipio
     */
    public function getId_municipio() {
        return $this->id_municipio;
    }

    /**
     * @param id_municipio
     */
    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    /**
     * @return string st_uf
     */
    public function getSt_uf() {
        return $this->st_uf;
    }

    /**
     * @param st_uf
     */
    public function setSt_uf($st_uf) {
        $this->st_uf = $st_uf;
        return $this;
    }
}

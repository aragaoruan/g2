<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Class Entity for VwResumoFinanceiro
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_resumofinanceiro")
 * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
 */
class VwResumoFinanceiro extends G2Entity
{

    /**
     * @var string $st_nossonumero
     * @Column(name="st_nossonumero", type="string")
     */
    private $st_nossonumero;

    /**
     * @var string $st_codtransacaooperadora
     * @Column(name="st_codtransacaooperadora", type="string")
     */
    private $st_codtransacaooperadora;

    /**
     * @var boolean $bl_original
     * @Column(type="boolean")
     */
    private $bl_original;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean")
     */
    private $bl_ativo;

    /**
     * @var string $st_numcheque
     * @Column(type="string")
     */
    private $st_numcheque;

    /**
     * var string $recorrente_orderid
     * @Column(name="recorrente_orderid", type="string")
     */
    private $recorrente_orderid;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer", nullable=true, length=4)
     */
    private $id_lancamento;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;

    /**
     * @var boolean $bl_entrada
     * @Column(name="bl_entrada", type="boolean", nullable=true, length=1)
     */
    private $bl_entrada;

    /**
     * @var string $st_banco
     * @Column(name="st_banco", type="string", nullable=true, length=3)
     */
    private $st_banco;

    /**
     * @var string $st_emissor
     * @Column(name="st_emissor", type="string", nullable=true, length=255)
     */
    private $st_emissor;

    /**
     * @var datetime2 $dt_prevquitado
     * @Column(name="dt_prevquitado", type="datetime2", nullable=true, length=8)
     */
    private $dt_prevquitado;

    /**
     * @var string $st_coddocumento
     * @Column(name="st_coddocumento", type="string", nullable=true, length=255)
     */
    private $st_coddocumento;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;

    /**
     * @var string $st_estadoprovincia
     * @Column(name="st_estadoprovincia", type="string", nullable=true, length=255)
     */
    private $st_estadoprovincia;

    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=12)
     */
    private $st_cep;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=30)
     */
    private $nu_numero;

    /**
     * @var boolean $bl_quitado
     * @Column(name="bl_quitado", type="boolean", nullable=true, length=1)
     */
    private $bl_quitado;

    /**
     * @var date $dt_quitado
     * @Column(name="dt_quitado", type="date", nullable=true, length=3)
     */
    private $dt_quitado;

    /**
     * @var decimal $nu_quitado
     * @Column(name="nu_quitado", type="decimal", nullable=true, length=17)
     */
    private $nu_quitado;

    /**
     * @var string $st_entradaparcela
     * @Column(name="st_entradaparcela", type="string", nullable=true, length=8)
     */
    private $st_entradaparcela;

    /**
     * @var integer $nu_parcela
     * @Column(name="nu_parcela", type="decimal", nullable=true, length=4)
     */
    private $nu_parcela;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=true, length=4)
     */
    private $id_meiopagamento;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=true, length=255)
     */
    private $st_meiopagamento;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=true, length=17)
     */
    private $nu_valor;

    /**
     * @var decimal $nu_vencimento
     * @Column(name="nu_vencimento", type="decimal", nullable=true, length=17)
     */
    private $nu_vencimento;

    /**
     * @var date $dt_vencimento
     * @Column(name="dt_vencimento", type="date", nullable=true, length=3)
     */
    private $dt_vencimento;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=10)
     */
    private $st_situacao;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true, length=4)
     */
    private $id_usuariolancamento;

    /**
     * @var integer $id_entidadelancamento
     * @Column(name="id_entidadelancamento", type="integer", nullable=true, length=4)
     */
    private $id_entidadelancamento;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_textosistemarecibo
     * @Column(name="id_textosistemarecibo", type="integer", nullable=true, length=4)
     */
    private $id_textosistemarecibo;

    /**
     * @var integer $id_reciboconsolidado
     * @Column(name="id_reciboconsolidado", type="integer", nullable=true, length=4)
     */
    private $id_reciboconsolidado;

    /**
     * @var datetime $dt_atualizado
     * @Column(name="dt_atualizado", type="datetime2", nullable=true, length=3)
     */
    private $dt_atualizado;

    /**
     * var string $st_situacaopedido
     * @Column(name="st_situacaopedido", type="string")
     */
    private $st_situacaopedido;

    /**
     * var string $st_situacaolancamento
     * @Column(name="st_situacaolancamento", type="string")
     */
    private $st_situacaolancamento;

    /**
     * @var integer $id_acordo
     * @Column(name="id_acordo", type="integer", nullable=true, length=4)
     */
    private $id_acordo;

    /**
     * @var boolean bl_cartaovencendo
     * @Column(name="bl_cartaovencendo", type="boolean")
     */
    private $bl_cartaovencendo;

    /**
     * @var float $nu_desconto
     * @Column(name="nu_desconto", type="float")
     */
    private $nu_desconto;

    /**
     * @var float $nu_juros
     * @Column(name="nu_juros", type="float")
     */
    private $nu_juros;

    /**
     * @var float $nu_descontoboleto
     * @Column(name="nu_descontoboleto", type="float")
     */
    private $nu_descontoboleto;

    /**
     * @var float $nu_jurosboleto
     * @Column(name="nu_jurosboleto", type="float")
     */
    private $nu_jurosboleto;

    /**
     * @var string  $st_autorizacao
     * @Column(type="string", length=32, nullable=true, name="st_autorizacao")
     */
    private $st_autorizacao;

    /**
     * @var string $st_ultimosdigitos
     * @Column(type="string", length=32, nullable=true, name="st_ultimosdigitos")
     */
    private $st_ultimosdigitos;

    /**
     * @var string $st_codigobarras
     * @Column(type="string", length=100, nullable=true, name="st_codigobarras")
     */
    private $st_codigobarras;

    /**
     * @var string $st_urlboleto
     * @Column(type="string", length=100, nullable=true, name="st_urlboleto")
     */
    private $st_urlboleto;

    /**
     * @var datetime2 $dt_venda
     * @Column(type="datetime2", nullable=true, name="dt_venda")
     */
    private $dt_venda;

    /**
     * @var boolean $bl_chequedevolvido
     * @Column(name="bl_chequedevolvido", type="boolean", nullable=true, length=1)
     */
    private $bl_chequedevolvido;

    /**
     * @var integer $id_tipoendereco
     * @Column(name="id_tipoendereco", type="integer", nullable=true)
     */
    private $id_tipoendereco;

    /**
     * @var integer $id_transacaoexterna
     * @Column(name="id_transacaoexterna", type="integer", nullable=true)
     */
    private $id_transacaoexterna;

    /**
     * @var boolean $bl_recorrente
     * @Column(name="bl_recorrente", type="boolean", nullable=true, length=1)
     */
    private $bl_recorrente;

    /**
     * @var integer $id_vendaaditivo
     * @Column(name="id_vendaaditivo", type="integer", nullable=true)
     */
    private $id_vendaaditivo;

    /**
     * @var integer $id_evolucaovenda
     * @Column(name="id_evolucaovenda", type="integer", nullable= true)
     */
    private $id_evolucaovenda;

    /**
     * @var string $st_evolucaovenda
     * @Column(name="st_evolucaovenda", type="string")
     */
    private $st_evolucaovenda;

    /**
     * @var integer $id_tipolancamento
     * @Column(name="id_tipolancamento", type="integer", nullable= true)
     */
    private $id_tipolancamento;

    /**
     * @return int
     */
    public function getid_transacaoexterna()
    {
        return $this->id_transacaoexterna;
    }

    /**
     * @param int $id_transacaoexterna
     */
    public function setid_transacaoexterna($id_transacaoexterna)
    {
        $this->id_transacaoexterna = $id_transacaoexterna;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_urlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param string $st_urlboleto
     */
    public function setst_urlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_codigobarras()
    {
        return $this->st_codigobarras;
    }

    /**
     * @param string $st_codigobarras
     * @return VwResumoFinanceiro
     */
    public function setStCodigobarras($st_codigobarras)
    {
        $this->st_codigobarras = $st_codigobarras;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isBlRecorrente()
    {
        return $this->bl_recorrente;
    }

    /**
     * @param boolean $bl_recorrente
     */
    public function setBlRecorrente($bl_recorrente)
    {
        $this->bl_recorrente = $bl_recorrente;
    }

    function getRecorrente_orderid ()
    {
        return $this->recorrente_orderid;
    }

    function getId_acordo ()
    {
        return $this->id_acordo;
    }

    function getSt_autorizacao ()
    {
        return $this->st_autorizacao;
    }

    function getSt_ultimosdigitos ()
    {
        return $this->st_ultimosdigitos;
    }

    function getDt_venda ()
    {
        return $this->dt_venda;
    }

    function setRecorrente_orderid ($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
    }

    function setId_acordo ($id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    function setSt_autorizacao ($st_autorizacao)
    {
        $this->st_autorizacao = $st_autorizacao;
    }

    function setSt_ultimosdigitos ($st_ultimosdigitos)
    {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    function setDt_venda ($dt_confirmacao)
    {
        $this->dt_venda = $dt_confirmacao;
    }

    public function getSt_situacaopedido ()
    {
        return $this->st_situacaopedido;
    }

    public function setSt_situacaopedido ($st_situacaopedido)
    {
        $this->st_situacaopedido = $st_situacaopedido;
    }

    public function getSt_situacaolancamento ()
    {
        return $this->st_situacaolancamento;
    }

    public function setSt_situacaolancamento ($st_situacaolancamento)
    {
        $this->st_situacaolancamento = $st_situacaolancamento;
    }

    /**
     * @return mixed
     */
    public function getRecorrenteOrderid ()
    {
        return $this->recorrente_orderid;
    }

    /**
     * @param mixed $recorrente_orderid
     */
    public function setRecorrenteOrderid ($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
    }

    public function getId_lancamento ()
    {
        return $this->id_lancamento;
    }

    public function getId_venda ()
    {
        return $this->id_venda;
    }

    public function getBl_entrada ()
    {
        return $this->bl_entrada;
    }

    public function getSt_banco ()
    {
        return $this->st_banco;
    }

    public function getSt_emissor ()
    {
        return $this->st_emissor;
    }

    public function getDt_prevquitado ()
    {
        return $this->dt_prevquitado;
    }

    public function getSt_coddocumento ()
    {
        return $this->st_coddocumento;
    }

    public function getSt_endereco ()
    {
        return $this->st_endereco;
    }

    public function getSt_cidade ()
    {
        return $this->st_cidade;
    }

    public function getSt_bairro ()
    {
        return $this->st_bairro;
    }

    public function getSt_estadoprovincia ()
    {
        return $this->st_estadoprovincia;
    }

    public function getSt_cep ()
    {
        return $this->st_cep;
    }

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function getNu_numero ()
    {
        return $this->nu_numero;
    }

    public function getBl_quitado ()
    {
        return $this->bl_quitado;
    }

    public function getDt_quitado ()
    {
        return $this->dt_quitado;
    }

    public function getNu_quitado ()
    {
        return $this->nu_quitado;
    }

    public function getSt_entradaparcela ()
    {
        return $this->st_entradaparcela;
    }

    public function getNu_parcela ()
    {
        return $this->nu_parcela;
    }

    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    public function getSt_meiopagamento ()
    {
        return $this->st_meiopagamento;
    }

    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    public function getNu_vencimento ()
    {
        return $this->nu_vencimento;
    }

    public function getDt_vencimento ()
    {
        return $this->dt_vencimento;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function getId_textosistemarecibo ()
    {
        return $this->id_textosistemarecibo;
    }

    public function getId_reciboconsolidado ()
    {
        return $this->id_reciboconsolidado;
    }

    public function setId_lancamento ($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }

    public function setId_venda ($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function setBl_entrada ($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
        return $this;
    }

    public function setSt_banco ($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

    public function setSt_emissor ($st_emissor)
    {
        $this->st_emissor = $st_emissor;
        return $this;
    }

    public function setDt_prevquitado ($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
        return $this;
    }

    public function setSt_coddocumento ($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
        return $this;
    }

    public function setSt_endereco ($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function setSt_cidade ($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    public function setSt_bairro ($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    public function setSt_estadoprovincia ($st_estadoprovincia)
    {
        $this->st_estadoprovincia = $st_estadoprovincia;
        return $this;
    }

    public function setSt_cep ($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function setNu_numero ($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    public function setBl_quitado ($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
        return $this;
    }

    public function setDt_quitado ($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
        return $this;
    }

    public function setNu_quitado ($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
        return $this;
    }

    public function setSt_entradaparcela ($st_entradaparcela)
    {
        $this->st_entradaparcela = $st_entradaparcela;
        return $this;
    }

    public function setNu_parcela ($nu_parcela)
    {
        $this->nu_parcela = $nu_parcela;
        return $this;
    }

    public function setId_meiopagamento ($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    public function setSt_meiopagamento ($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function setNu_vencimento ($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
        return $this;
    }

    public function setDt_vencimento ($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
        return $this;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_textosistemarecibo ($id_textosistemarecibo)
    {
        $this->id_textosistemarecibo = $id_textosistemarecibo;
        return $this;
    }

    public function setId_reciboconsolidado ($id_reciboconsolidado)
    {
        $this->id_reciboconsolidado = $id_reciboconsolidado;
        return $this;
    }

    public function getSt_nossonumero ()
    {
        return $this->st_nossonumero;
    }

    public function getSt_codtransacaooperadora ()
    {
        return $this->st_codtransacaooperadora;
    }

    public function getBl_original ()
    {
        return $this->bl_original;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setSt_nossonumero ($st_nossonumero)
    {
        $this->st_nossonumero = $st_nossonumero;
    }

    public function setSt_codtransacaooperadora ($st_codtransacaooperadora)
    {
        $this->st_codtransacaooperadora = $st_codtransacaooperadora;
    }

    public function setBl_original ($bl_original)
    {
        $this->bl_original = $bl_original;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getSt_numcheque ()
    {
        return $this->st_numcheque;
    }

    public function setSt_numcheque ($st_numcheque)
    {
        $this->st_numcheque = $st_numcheque;
    }

    public function getDt_atualizado ()
    {
        return $this->dt_atualizado;
    }

    public function setDt_atualizado (date $dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @return int
     */
    public function getIdAcordo ()
    {
        return $this->id_acordo;
    }

    /**
     * @param int $id_acordo
     */
    public function setIdAcordo ($id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @return boolean
     */
    public function getbl_cartaovencendo ()
    {
        return $this->bl_cartaovencendo;
    }

    /**
     * @param boolean $bl_cartaovencendo
     */
    public function setbl_cartaovencendo ($bl_cartaovencendo)
    {
        $this->bl_cartaovencendo = $bl_cartaovencendo;
    }

    /**
     * @return float
     */
    public function getnu_desconto ()
    {
        return $this->nu_desconto;
    }

    /**
     * @param float $nu_desconto
     */
    public function setnu_desconto ($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
    }

    /**
     * @return float
     */
    public function getnu_juros ()
    {
        return $this->nu_juros;
    }

    /**
     * @param float $nu_juros
     */
    public function setnu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return float
     */
    public function getnu_descontoboleto ()
    {
        return $this->nu_descontoboleto;
    }

    /**
     * @param float $nu_descontoboleto
     */
    public function setnu_descontoboleto ($nu_descontoboleto)
    {
        $this->nu_descontoboleto = $nu_descontoboleto;
    }

    /**
     * @return float
     */
    public function getnu_jurosboleto ()
    {
        return $this->nu_jurosboleto;
    }

    /**
     * @param float $nu_jurosboleto
     */
    public function setnu_jurosboleto ($nu_jurosboleto)
    {
        $this->nu_jurosboleto = $nu_jurosboleto;
    }

    /**
     * @param boolean $bl_cartaovencendo
     */
    public function setBlCartaovencendo ($bl_cartaovencendo)
    {
        $this->bl_cartaovencendo = $bl_cartaovencendo;
    }

    /**
     * @return boolean
     */
    public function getBlCartaovencendo ()
    {
        return $this->bl_cartaovencendo;
    }

    /**
     * @param boolean $bl_chequedevolvido
     */
    public function setBl_chequedevolvido($bl_chequedevolvido)
    {
        $this->bl_chequedevolvido = $bl_chequedevolvido;
    }

    /**
     * @return boolean
     */
    public function getBl_chequedevolvido()
    {
        return $this->bl_chequedevolvido;
    }

    /**
     * @return boolean
     */
    public function getBl_recorrente()
    {
        return $this->bl_recorrente;
    }

    /**
     * @param boolean $bl_recorrente
     */
    public function setBl_recorrente($bl_recorrente)
    {
        $this->bl_recorrente = $bl_recorrente;
    }

    /**
     * @return int
     */
    public function getId_tipoendereco()
    {
        return $this->id_tipoendereco;
    }

    /**
     * @param int $id_tipoendereco
     */
    public function setId_tipoendereco($id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
    }

    /**
     * @return int
     */
    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @param int $id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
    }

    /**
     * @return int
     */
    public function getId_entidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @param int $id_entidadelancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
    }

    /**
     * @return int
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
    }

    /**
     * @return int
     */
    public function getId_evolucaovenda()
    {
        return $this->id_evolucaovenda;
    }

    /**
     * @param int $id_evolucaovenda
     */
    public function setId_evolucaovenda($id_evolucaovenda)
    {
        $this->id_evolucaovenda = $id_evolucaovenda;
    }

    /**
     * @return string
     */
    public function getSt_evolucaovenda()
    {
        return $this->st_evolucaovenda;
    }

    /**
     * @param string $st_evolucaovenda
     */
    public function setSt_evolucaovenda($st_evolucaovenda)
    {
        $this->st_evolucaovenda = $st_evolucaovenda;
    }

    /**
     * @return int
     */
    public function getId_tipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @param int $id_tipolancamento
     */
    public function setId_tipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
    }



}

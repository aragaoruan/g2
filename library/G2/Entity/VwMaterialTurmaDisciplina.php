<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_materialturmadisciplina")
 * @Entity
 * @EntityView
 * @author Helder Fernandes <helder.silva@unyleya.com.br>
 */
class VwMaterialTurmaDisciplina {

    /**
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     *  @Id
     */
    private $id_itemdematerial;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_tipodematerial
     * @Column(name="id_tipodematerial", type="integer", nullable=false, length=4)
     */
    private $id_tipodematerial;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $nu_qtdepaginas
     * @Column(name="nu_qtdepaginas", type="integer", nullable=true, length=4)
     */
    private $nu_qtdepaginas;
    /**
     * @var integer $nu_qtdestoque
     * @Column(name="nu_qtdestoque", type="integer", nullable=true, length=4)
     */
    private $nu_qtdestoque;
    /**
     * @var integer $nu_peso
     * @Column(name="nu_peso", type="integer", nullable=true, length=4)
     */
    private $nu_peso;
    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;
    /**
     * @var integer $nu_encontro
     * @Column(name="nu_encontro", type="integer", nullable=true, length=4)
     */
    private $nu_encontro;
    /**
     * @var integer $id_professor
     * @Column(name="id_professor", type="integer", nullable=true, length=4)
     */
    private $id_professor;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     *
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     *  @Id
     */
    private $id_turma;
    /**
     * @var boolean $bl_portal
     * @Column(name="bl_portal", type="boolean", nullable=true, length=1)
     */
    private $bl_portal;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;
    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=true, length=9)
     */
    private $nu_valor;
    /**
     * @var string $st_itemdematerial
     * @Column(name="st_itemdematerial", type="string", nullable=false, length=255)
     */
    private $st_itemdematerial;
    /**
     * @var string $st_entidade
     * @Column(name="st_entidade", type="string", nullable=true, length=150)
     */
    private $st_entidade;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=false, length=300)
     */
    private $st_professor;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_tipodematerial
     * @Column(name="st_tipodematerial", type="string", nullable=false, length=510)
     */
    private $st_tipodematerial;


    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_itemdematerial
     */
    public function getId_itemdematerial() {
        return $this->id_itemdematerial;
    }

    /**
     * @param id_itemdematerial
     */
    public function setId_itemdematerial($id_itemdematerial) {
        $this->id_itemdematerial = $id_itemdematerial;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_tipodematerial
     */
    public function getId_tipodematerial() {
        return $this->id_tipodematerial;
    }

    /**
     * @param id_tipodematerial
     */
    public function setId_tipodematerial($id_tipodematerial) {
        $this->id_tipodematerial = $id_tipodematerial;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer nu_qtdepaginas
     */
    public function getNu_qtdepaginas() {
        return $this->nu_qtdepaginas;
    }

    /**
     * @param nu_qtdepaginas
     */
    public function setNu_qtdepaginas($nu_qtdepaginas) {
        $this->nu_qtdepaginas = $nu_qtdepaginas;
        return $this;
    }

    /**
     * @return integer nu_peso
     */
    public function getNu_peso() {
        return $this->nu_peso;
    }

    /**
     * @param nu_peso
     */
    public function setNu_peso($nu_peso) {
        $this->nu_peso = $nu_peso;
        return $this;
    }

    /**
     * @return integer id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return integer nu_encontro
     */
    public function getNu_encontro() {
        return $this->nu_encontro;
    }

    /**
     * @param nu_encontro
     */
    public function setNu_encontro($nu_encontro) {
        $this->nu_encontro = $nu_encontro;
        return $this;
    }

    /**
     * @return integer id_professor
     */
    public function getId_professor() {
        return $this->id_professor;
    }

    /**
     * @param id_professor
     */
    public function setId_professor($id_professor) {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma() {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma) {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return boolean bl_portal
     */
    public function getBl_portal() {
        return $this->bl_portal;
    }

    /**
     * @param bl_portal
     */
    public function setBl_portal($bl_portal) {
        $this->bl_portal = $bl_portal;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return decimal nu_valor
     */
    public function getNu_valor() {
        return $this->nu_valor;
    }

    /**
     * @param nu_valor
     */
    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return string st_itemdematerial
     */
    public function getSt_itemdematerial() {
        return $this->st_itemdematerial;
    }

    /**
     * @param st_itemdematerial
     */
    public function setSt_itemdematerial($st_itemdematerial) {
        $this->st_itemdematerial = $st_itemdematerial;
        return $this;
    }

    /**
     * @return string st_entidade
     */
    public function getSt_entidade() {
        return $this->st_entidade;
    }

    /**
     * @param st_entidade
     */
    public function setSt_entidade($st_entidade) {
        $this->st_entidade = $st_entidade;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_professor
     */
    public function getSt_professor() {
        return $this->st_professor;
    }

    /**
     * @param st_professor
     */
    public function setSt_professor($st_professor) {
        $this->st_professor = $st_professor;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma() {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma) {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return st_tipodematerial
     */
    public function getSt_tipodematerial() {
        return $this->st_tipodematerial;
    }

    /**
     * @param st_tipodematerial
     */
    public function setSt_tipodematerial($st_tipodematerial) {
        $this->st_tipodematerial = $st_tipodematerial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getNu_qtdestoque()
    {
        return $this->nu_qtdestoque;
    }

    /**
     * @param int $nu_qtdestoque
     */
    public function setNu_qtdestoque($nu_qtdestoque)
    {
        $this->nu_qtdestoque = $nu_qtdestoque;
    }




}
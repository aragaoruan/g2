<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_aplicadorprova")
 * @Entity
 * @EntityLog
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AplicadorProva {

    const ATIVO = 127;
    const INATIVO = 128;

    /**
     * @Id
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_aplicadorprova;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2",  nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_entidadeaplicador
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadeaplicador", referencedColumnName="id_entidade")
     */
    private $id_entidadeaplicador;


    /**
     * @var integer $id_usuarioaplicador
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioaplicador", referencedColumnName="id_usuario")
     */
    private $id_usuarioaplicador;

    /**
     * @var integer $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;


    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string",  nullable=false, length=200)
     */
    private $st_aplicadorprova;


    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @var datetime2 $dt_sincronizado
     * @Column(name="dt_sincronizado", type="datetime2",  nullable=true, length=8)
     */
    private $dt_sincronizado;

    /**
     * @var boolean $bl_sincronizado
     * @Column(name="bl_sincronizado", type="boolean",  nullable=false, length=1)
     */
    private $bl_sincronizado = 0;

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }


    public function getId ()
    {
        return $this->id_aplicadorprova;
    }

    public function setId ($id)
    {
        $this->id_aplicadorprova = $id;
        return $this;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param int $id_entidadeaplicador
     */
    public function setId_entidadeaplicador($id_entidadeaplicador)
    {
        $this->id_entidadeaplicador = $id_entidadeaplicador;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeaplicador()
    {
        return $this->id_entidadeaplicador;
    }

    /**
     * @param int $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_usuarioaplicador
     */
    public function setId_usuarioaplicador($id_usuarioaplicador)
    {
        $this->id_usuarioaplicador = $id_usuarioaplicador;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuarioaplicador()
    {
        return $this->id_usuarioaplicador;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @return \DateTime
     */
    public function getDt_sincronizado()
    {
        return $this->dt_sincronizado;
    }

    /**
     * @param \DateTime $dt_sincronizado
     */
    public function setDt_sincronizado($dt_sincronizado)
    {
        $this->dt_sincronizado = $dt_sincronizado;
    }

    /**
     * @return bool
     */
    public function isBl_sincronizado()
    {
        return $this->bl_sincronizado;
    }

    /**
     * @param bool $bl_sincronizado
     */
    public function setBl_sincronizado($bl_sincronizado)
    {
        $this->bl_sincronizado = $bl_sincronizado;
    }



}

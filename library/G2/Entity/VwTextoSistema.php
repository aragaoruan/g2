<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_textosistema")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwTextoSistema
{

    /**
     *
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_textosistema;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_textoexibicao
     * @Column(name="id_textoexibicao", type="integer", nullable=true)
     */
    private $id_textoexibicao;

    /**
     *
     * @var integer $id_textocategoria
     * @Column(name="id_textocategoria", type="integer", nullable=true)
     */
    private $id_textocategoria;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     *
     * @var integer $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=false, length=200)
     */
    private $st_textosistema;

    /**
     *
     * @var text $st_texto
     * @Column(name="st_texto", type="text", nullable=false)
     */
    private $st_texto;

    /**
     *
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;

    /**
     *
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true)
     */
    private $dt_fim;

    /**
     *
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_orientacaotexto
     * @Column(name="id_orientacaotexto", type="integer", nullable=true)
     */
    private $id_orientacaotexto;

    /**
     *
     * @var string $st_textoexibicao
     * @Column(name="st_textoexibicao", type="string", nullable=false, length=200)
     */
    private $st_textoexibicao;

    /**
     *
     * @var string $st_textocategoria
     * @Column(name="st_textocategoria", type="string", nullable=false, length=100)
     */
    private $st_textocategoria;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var string $st_descricaosituacao
     * @Column(name="st_descricaosituacao", type="string", nullable=false, length=8000)
     */
    private $st_descricaosituacao;

    /**
     *
     * @var string $st_orientacaotexto
     * @Column(name="st_orientacaotexto", type="string", nullable=false, length=15)
     */
    private $st_orientacaotexto;

    /**
     * @var boolean $bl_edicao
     * @Column(name="bl_edicao", type="boolean", nullable=false)
     */
    private $bl_edicao;

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema ($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_textoexibicao ()
    {
        return $this->id_textoexibicao;
    }

    public function setId_textoexibicao ($id_textoexibicao)
    {
        $this->id_textoexibicao = $id_textoexibicao;
        return $this;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria ($id_textocategoria)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_textosistema ()
    {
        return $this->st_textosistema;
    }

    public function setSt_textosistema ($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

    public function getSt_texto ()
    {
        return $this->st_texto;
    }

    public function setSt_texto ($st_texto)
    {
        $this->st_texto = $st_texto;
        return $this;
    }

    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_orientacaotexto ()
    {
        return $this->id_orientacaotexto;
    }

    public function setId_orientacaotexto ($id_orientacaotexto)
    {
        $this->id_orientacaotexto = $id_orientacaotexto;
        return $this;
    }

    public function getSt_textoexibicao ()
    {
        return $this->st_textoexibicao;
    }

    public function setSt_textoexibicao ($st_textoexibicao)
    {
        $this->st_textoexibicao = $st_textoexibicao;
        return $this;
    }

    public function getSt_textocategoria ()
    {
        return $this->st_textocategoria;
    }

    public function setSt_textocategoria ($st_textocategoria)
    {
        $this->st_textocategoria = $st_textocategoria;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getSt_descricaosituacao ()
    {
        return $this->st_descricaosituacao;
    }

    public function setSt_descricaosituacao ($st_descricaosituacao)
    {
        $this->st_descricaosituacao = $st_descricaosituacao;
        return $this;
    }

    public function getSt_orientacaotexto ()
    {
        return $this->st_orientacaotexto;
    }

    public function setSt_orientacaotexto ($st_orientacaotexto)
    {
        $this->st_orientacaotexto = $st_orientacaotexto;
        return $this;
    }

    /**
     * @return string
     */
    public function getBl_edicao()
    {
        return $this->bl_edicao;
    }

    /**
     * @param string $bl_edicao
     */
    public function setBl_edicao($bl_edicao)
    {
        $this->bl_edicao = $bl_edicao;
    }

}
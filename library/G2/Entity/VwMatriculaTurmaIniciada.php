<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matricula_turmainiciada")
 * @Entity
 * @EntityView
 * @author Helder Fernandes <helder.silva@unyleya.com.br>
 */
class VwMatriculaTurmaIniciada
{

    /**
     * @Id
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_turma;
    /**
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false, length=3)
     */
    private $dt_inicio;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false, length=4)
     */
    private $id_entidadecadastro;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @Id
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_categoria;
    /**
     * @var integer $nu_matriculas
     * @Column(name="nu_matriculas", type="integer", nullable=true, length=4)
     */
    private $nu_matriculas;
    /**
     * @var decimal $st_cargahoraria
     * @Column(name="st_cargahoraria", type="decimal", nullable=true, length=9)
     */
    private $st_cargahoraria;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=false, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_categoria
     * @Column(name="st_categoria", type="string", nullable=false, length=200)
     */
    private $st_categoria;
    /**
     * @var string $st_entidade
     * @Column(name="st_entidade", type="string", nullable=true, length=150)
     */
    private $st_entidade;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=false, length=255)
     */
    private $st_turno;

    /**
     * @var integer $nu_freepass
     * @Column(name="nu_freepass", type="integer", nullable=true, length=4)
     */
    private $nu_freepass;

    /**
     * @var integer $nu_garantiaduo
     * @Column(name="nu_garantiaduo", type="integer", nullable=true, length=4)
     */
    private $nu_garantiaduo;

    /**
     * @return integer nu_freepass
     */
    public function getNu_freepass()
    {
        return $this->nu_freepass;
    }

    /**
     * @param nu_freepass
     */
    public function setNu_freepass($nu_freepass)
    {
        $this->nu_freepass = $nu_freepass;
        return $this;
    }

    /**
     * @return integer nu_garantiaduo
     */
    public function getNu_garantiaduo()
    {
        return $this->nu_garantiaduo;
    }

    /**
     * @param nu_garantiaduo
     */
    public function setNu_garantiaduo($nu_garantiaduo)
    {
        $this->nu_garantiaduo = $nu_garantiaduo;
        return $this;
    }

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_categoria
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

    /**
     * @param id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    /**
     * @return integer nu_matriculas
     */
    public function getNu_matriculas()
    {
        return $this->nu_matriculas;
    }

    /**
     * @param nu_matriculas
     */
    public function setNu_matriculas($nu_matriculas)
    {
        $this->nu_matriculas = $nu_matriculas;
        return $this;
    }

    /**
     * @return decimal st_cargahoraria
     */
    public function getSt_cargahoraria()
    {
        return $this->st_cargahoraria;
    }

    /**
     * @param st_cargahoraria
     */
    public function setSt_cargahoraria($st_cargahoraria)
    {
        $this->st_cargahoraria = $st_cargahoraria;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return string st_categoria
     */
    public function getSt_categoria()
    {
        return $this->st_categoria;
    }

    /**
     * @param st_categoria
     */
    public function setSt_categoria($st_categoria)
    {
        $this->st_categoria = $st_categoria;
        return $this;
    }

    /**
     * @return string st_entidade
     */
    public function getSt_entidade()
    {
        return $this->st_entidade;
    }

    /**
     * @param st_entidade
     */
    public function setSt_entidade($st_entidade)
    {
        $this->st_entidade = $st_entidade;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_turno
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @param st_turno
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }


}

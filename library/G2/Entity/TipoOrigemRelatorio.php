<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoorigemrelatorio")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoOrigemRelatorio
{

    /**
     *
     * @var integer $id_tipoorigemrelatorio
     * @Column(name="id_tipoorigemrelatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoorigemrelatorio;

    /**
     *
     * @var string $st_tipoorigemrelatorio
     * @Column(name="st_tipoorigemrelatorio", type="string", length=100, nullable=false)
     */
    private $st_tipoorigemrelatorio;

    public function getId_tipoorigemrelatorio ()
    {
        return $this->id_tipoorigemrelatorio;
    }

    public function setId_tipoorigemrelatorio ($id_tipoorigemrelatorio)
    {
        $this->id_tipoorigemrelatorio = $id_tipoorigemrelatorio;
        return $this;
    }

    public function getSt_tipoorigemrelatorio ()
    {
        return $this->st_tipoorigemrelatorio;
    }

    public function setSt_tipoorigemrelatorio ($st_tipoorigemrelatorio)
    {
        $this->st_tipoorigemrelatorio = $st_tipoorigemrelatorio;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_trilha")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */

use G2\G2Entity;

class Trilha extends G2Entity
{

    /**
     * @var integer $id_trilha
     * @Column(type="integer", nullable=false, name="id_trilha")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_trilha;

    /**
     * @var TipoTrilha $id_tipotrilha
     * @ManyToOne(targetEntity="TipoTrilha")
     * @JoinColumn(name="id_tipotrilhafixa", referencedColumnName="id_tipotrilha", nullable=false)
     */
    private $id_tipotrilha;
    /**
     * @var decimal $nu_disciplinassimultaneasmai
     * @Column(type="decimal",nullable=true, name="nu_disciplinassimultaneasmai")
     */
    private $nu_disciplinassimultaneasmai;
    /**
     * @var decimal $nu_disciplinassimultaneaspendencia
     * @Column(type="decimal",nullable=true, name="nu_disciplinassimultaneaspendencia")
     */
    private $nu_disciplinassimultaneaspendencia;
    /**
     * @var decimal $nu_disciplinassimultaneasmen
     * @Column(type="decimal",nullable=true, name="nu_disciplinassimultaneasmen")
     */
    private $nu_disciplinassimultaneasmen;
    /**
     * @var TipoTrilhaFixa $id_tipotrilhafixa
     * @ManyToOne(targetEntity="TipoTrilhaFixa")
     * @JoinColumn(name="id_tipotrilhafixa", referencedColumnName="id_tipotrilhafixa", nullable=true)
     */
    private $id_tipotrilhafixa;
    /**
     * @var decimal $nu_disciplinaspendencia
     * @Column(type="decimal",nullable=true, name="nu_disciplinaspendencia")
     */
    private $nu_disciplinaspendencia;

    public function setId_tipotrilha($id_tipotrilha)
    {
        $this->id_tipotrilha = $id_tipotrilha;
    }

    public function getId_tipotrilha()
    {
        return $this->id_tipotrilha;
    }

    /**
     * @param \G2\Entity\TipoTrilhaFixa $id_tipotrilhafixa
     */
    public function setId_tipotrilhafixa($id_tipotrilhafixa)
    {
        $this->id_tipotrilhafixa = $id_tipotrilhafixa;
    }

    /**
     * @return \G2\Entity\TipoTrilhaFixa
     */
    public function getId_tipotrilhafixa()
    {
        return $this->id_tipotrilhafixa;
    }

    /**
     * @param int $id_trilha
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
    }

    /**
     * @return int
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @param \G2\Entity\decimal $nu_disciplinaspendencia
     */
    public function setNu_disciplinaspendencia($nu_disciplinaspendencia)
    {
        $this->nu_disciplinaspendencia = $nu_disciplinaspendencia;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_disciplinaspendencia()
    {
        return $this->nu_disciplinaspendencia;
    }

    /**
     * @param \G2\Entity\decimal $nu_disciplinassimultaneasmai
     */
    public function setNu_disciplinassimultaneasmai($nu_disciplinassimultaneasmai)
    {
        $this->nu_disciplinassimultaneasmai = $nu_disciplinassimultaneasmai;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_disciplinassimultaneasmai()
    {
        return $this->nu_disciplinassimultaneasmai;
    }

    /**
     * @param \G2\Entity\decimal $nu_disciplinassimultaneasmen
     */
    public function setNu_disciplinassimultaneasmen($nu_disciplinassimultaneasmen)
    {
        $this->nu_disciplinassimultaneasmen = $nu_disciplinassimultaneasmen;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_disciplinassimultaneasmen()
    {
        return $this->nu_disciplinassimultaneasmen;
    }

    /**
     * @param \G2\Entity\decimal $nu_disciplinassimultaneaspendencia
     */
    public function setNu_disciplinassimultaneaspendencia($nu_disciplinassimultaneaspendencia)
    {
        $this->nu_disciplinassimultaneaspendencia = $nu_disciplinassimultaneaspendencia;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_disciplinassimultaneaspendencia()
    {
        return $this->nu_disciplinassimultaneaspendencia;
    }


}

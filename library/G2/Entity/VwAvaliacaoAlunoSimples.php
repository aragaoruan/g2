<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avaliacaoaluno_simples")
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAvaliacaoAlunoSimples
{


    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_avaliacaoaluno
     * @Column(name="id_avaliacaoaluno", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaluno;

    /**
     * @var \DateTime $dt_defesa
     * @Column(name="dt_defesa", type="date", nullable=true, length=3)
     */
    private $dt_defesa;
    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true, length=4)
     */
    private $id_entidadematricula;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false, length=4)
     */
    private $id_avaliacao;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;
    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;
    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjunto;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_tiponota
     * @Column(name="id_tiponota", type="integer", nullable=true, length=4)
     */
    private $id_tiponota;
    /**
     * @var integer $id_evolucaodisciplina
     * @Column(name="id_evolucaodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_evolucaodisciplina;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipocalculoavaliacao;
    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false, length=4)
     */
    private $id_tipoavaliacao;
    /**
     * @var integer $id_avaliacaorecupera
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaorecupera;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;
    /**
     * @var decimal $st_nota
     * @Column(name="st_nota", type="decimal", nullable=true, length=5)
     */
    private $st_nota;
    /**
     * @var decimal $nu_notamaxima
     * @Column(name="nu_notamaxima", type="decimal", nullable=true, length=9)
     */
    private $nu_notamaxima;
    /**
     * @var decimal $nu_percentualaprovacao
     * @Column(name="nu_percentualaprovacao", type="decimal", nullable=true, length=5)
     */
    private $nu_percentualaprovacao;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false, length=100)
     */
    private $st_avaliacao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;
    /**
     * @var string $nu_notamax
     * @Column(name="nu_notamax", type="string", nullable=true, length=100)
     */
    private $nu_notamax;
    /**
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=false, length=200)
     */
    private $st_tipoavaliacao;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_tituloavaliacao
     * @Column(name="st_tituloavaliacao", type="string", nullable=true, length=500)
     */
    private $st_tituloavaliacao;
    /**
     * @var string $st_evolucaodisciplina
     * @Column(name="st_evolucaodisciplina", type="string", nullable=false, length=255)
     */
    private $st_evolucaodisciplina;



    /**
     * @return dt_defesa
     */
    public function getDt_defesa() {
        return $this->dt_defesa;
    }

    /**
     * @param dt_defesa
     */
    public function setDt_defesa($dt_defesa) {
        $this->dt_defesa = $dt_defesa;
        return $this;
    }

    /**
     * @return integer id_entidadematricula
     */
    public function getId_entidadematricula() {
        return $this->id_entidadematricula;
    }

    /**
     * @param id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula) {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    /**
     * @return integer id_avaliacaoaluno
     */
    public function getId_avaliacaoaluno() {
        return $this->id_avaliacaoaluno;
    }

    /**
     * @param id_avaliacaoaluno
     */
    public function setId_avaliacaoaluno($id_avaliacaoaluno) {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_avaliacao
     */
    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    /**
     * @param id_avaliacao
     */
    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_tipoprova
     */
    public function getId_tipoprova() {
        return $this->id_tipoprova;
    }

    /**
     * @param id_tipoprova
     */
    public function setId_tipoprova($id_tipoprova) {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    /**
     * @return integer nu_cargahoraria
     */
    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    /**
     * @param nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return integer id_avaliacaoconjunto
     */
    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    /**
     * @param id_avaliacaoconjunto
     */
    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_entidadeatendimento
     */
    public function getId_entidadeatendimento() {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento) {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_tiponota
     */
    public function getId_tiponota() {
        return $this->id_tiponota;
    }

    /**
     * @param id_tiponota
     */
    public function setId_tiponota($id_tiponota) {
        $this->id_tiponota = $id_tiponota;
        return $this;
    }

    /**
     * @return integer id_evolucaodisciplina
     */
    public function getId_evolucaodisciplina() {
        return $this->id_evolucaodisciplina;
    }

    /**
     * @param id_evolucaodisciplina
     */
    public function setId_evolucaodisciplina($id_evolucaodisciplina) {
        $this->id_evolucaodisciplina = $id_evolucaodisciplina;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_tipocalculoavaliacao
     */
    public function getId_tipocalculoavaliacao() {
        return $this->id_tipocalculoavaliacao;
    }

    /**
     * @param id_tipocalculoavaliacao
     */
    public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
        return $this;
    }

    /**
     * @return integer id_tipoavaliacao
     */
    public function getId_tipoavaliacao() {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param id_tipoavaliacao
     */
    public function setId_tipoavaliacao($id_tipoavaliacao) {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    /**
     * @return integer id_avaliacaorecupera
     */
    public function getId_avaliacaorecupera() {
        return $this->id_avaliacaorecupera;
    }

    /**
     * @param id_avaliacaorecupera
     */
    public function setId_avaliacaorecupera($id_avaliacaorecupera) {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return decimal st_nota
     */
    public function getSt_nota() {
        return $this->st_nota;
    }

    /**
     * @param st_nota
     */
    public function setSt_nota($st_nota) {
        $this->st_nota = $st_nota;
        return $this;
    }

    /**
     * @return decimal nu_notamaxima
     */
    public function getNu_notamaxima() {
        return $this->nu_notamaxima;
    }

    /**
     * @param nu_notamaxima
     */
    public function setNu_notamaxima($nu_notamaxima) {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    /**
     * @return decimal nu_percentualaprovacao
     */
    public function getNu_percentualaprovacao() {
        return $this->nu_percentualaprovacao;
    }

    /**
     * @param nu_percentualaprovacao
     */
    public function setNu_percentualaprovacao($nu_percentualaprovacao) {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_avaliacao
     */
    public function getSt_avaliacao() {
        return $this->st_avaliacao;
    }

    /**
     * @param st_avaliacao
     */
    public function setSt_avaliacao($st_avaliacao) {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_tituloexibicaodisciplina
     */
    public function getSt_tituloexibicaodisciplina() {
        return $this->st_tituloexibicaodisciplina;
    }

    /**
     * @param st_tituloexibicaodisciplina
     */
    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    /**
     * @return string nu_notamax
     */
    public function getNu_notamax() {
        return $this->nu_notamax;
    }

    /**
     * @param nu_notamax
     */
    public function setNu_notamax($nu_notamax) {
        $this->nu_notamax = $nu_notamax;
        return $this;
    }

    /**
     * @return string st_tipoavaliacao
     */
    public function getSt_tipoavaliacao() {
        return $this->st_tipoavaliacao;
    }

    /**
     * @param st_tipoavaliacao
     */
    public function setSt_tipoavaliacao($st_tipoavaliacao) {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_tituloavaliacao
     */
    public function getSt_tituloavaliacao() {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao) {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
        return $this;
    }

    /**
     * @return string st_evolucaodisciplina
     */
    public function getSt_evolucaodisciplina() {
        return $this->st_evolucaodisciplina;
    }

    /**
     * @param st_evolucaodisciplina
     */
    public function setSt_evolucaodisciplina($st_evolucaodisciplina) {
        $this->st_evolucaodisciplina = $st_evolucaodisciplina;
        return $this;
    }


}

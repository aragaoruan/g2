<?php

namespace G2\Entity;

/**
 * Class VwCampanhaPremioProduto
 * @package G2\Entity
 * @Entity(repositoryClass="\G2\Repository\CampanhaComercial")
 * @EntityView
 * @Table(name="vw_campanhapremioproduto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-07-14
 */
class VwCampanhaPremioProduto
{

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var decimal $nu_valorproduto
     * @Column(type="decimal", nullable=false)
     */
    private $nu_valorproduto;

    /**
     * @var integer $id_campanhacomercial
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     * @Column(type="string", nullable=false)
     */
    private $st_campanhacomercial;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $id_premio
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_premio;

    /**
     * @var boolean $id_premio
     * @Column(type="boolean", nullable=false)
     */
    private $bl_compraacima;

    /**
     * @var decimal $nu_comprasacima
     * @Column(type="decimal", nullable=false)
     */
    private $nu_comprasacima;

    /**
     * @var boolean $bl_compraunidade
     * @Column(type="boolean", nullable=false)
     */
    private $bl_compraunidade;

    /**
     * @var integer $nu_compraunidade
     * @Column(type="integer", nullable=false)
     */
    private $nu_compraunidade;

    /**
     * @var string $st_prefixocupompremio
     * @Column(type="string", nullable=false)
     */
    private $st_prefixocupompremio;

    /**
     * @var decimal $nu_descontopremio
     * @Column(type="decimal", nullable=false)
     */
    private $nu_descontopremio;

    /**
     * @var integer $id_tipodescontopremio
     * @Column(type="integer", nullable=false)
     * @Id
     */
    private $id_tipodescontopremio;


    /**
     * @var string $st_tipodescontopremio
     * @Column(type="string", nullable=false)
     */
    private $st_tipodescontopremio;

    /**
     * @var integer $id_tipopremio
     * @Column(type="integer", nullable=true)
     * @Id
     */
    private $id_tipopremio;

    /**
     * @var boolean $bl_promocaocumulativa
     * @Column(type="boolean", nullable=false)
     */
    private $bl_promocaocumulativa;

    /**
     * @var integer $id_cupomcampanha
     * @Column(type="integer", nullable=true)
     * @Id
     */
    private $id_cupomcampanha;

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @return decimal
     */
    public function getNu_valorproduto()
    {
        return $this->nu_valorproduto;
    }

    /**
     * @return int
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return string
     */
    public function getSt_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_premio()
    {
        return $this->id_premio;
    }

    /**
     * @return bool
     */
    public function getBl_compraacima()
    {
        return $this->bl_compraacima;
    }

    /**
     * @return decimal
     */
    public function getNu_comprasacima()
    {
        return $this->nu_comprasacima;
    }

    /**
     * @return bool
     */
    public function getBl_compraunidade()
    {
        return $this->bl_compraunidade;
    }

    /**
     * @return int
     */
    public function getNu_compraunidade()
    {
        return $this->nu_compraunidade;
    }

    /**
     * @return string
     */
    public function getSt_prefixocupompremio()
    {
        return $this->st_prefixocupompremio;
    }

    /**
     * @return decimal
     */
    public function getNu_descontopremio()
    {
        return $this->nu_descontopremio;
    }

    /**
     * @return int
     */
    public function getId_tipodescontopremio()
    {
        return $this->id_tipodescontopremio;
    }

    /**
     * @return int
     */
    public function getId_tipopremio()
    {
        return $this->id_tipopremio;
    }

    /**
     * @return bool
     */
    public function getBl_promocaocumulativa()
    {
        return $this->bl_promocaocumulativa;
    }

    /**
     * @return int
     */
    public function getId_cupomcampanha()
    {
        return $this->id_cupomcampanha;
    }

    /**
     * @param $id_produto
     * @return $this
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @param $nu_valorproduto
     * @return $this
     */
    public function setNu_valorproduto($nu_valorproduto)
    {
        $this->nu_valorproduto = $nu_valorproduto;
        return $this;
    }

    /**
     * @param $id_campanhacomercial
     * @return $this
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @param $st_campanhacomercial
     * @return $this
     */
    public function setSt_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param $id_premio
     * @return $this
     */
    public function setId_premio($id_premio)
    {
        $this->id_premio = $id_premio;
        return $this;
    }

    /**
     * @param $bl_compraacima
     * @return $this
     */
    public function setBl_compraacima($bl_compraacima)
    {
        $this->bl_compraacima = $bl_compraacima;
        return $this;
    }

    /**
     * @param $nu_comprasacima
     * @return $this
     */
    public function setNu_comprasacima($nu_comprasacima)
    {
        $this->nu_comprasacima = $nu_comprasacima;
        return $this;
    }

    /**
     * @param $bl_compraunidade
     * @return $this
     */
    public function setBl_compraunidade($bl_compraunidade)
    {
        $this->bl_compraunidade = $bl_compraunidade;
        return $this;
    }

    /**
     * @param $nu_compraunidade
     * @return $this
     */
    public function setNu_compraunidade($nu_compraunidade)
    {
        $this->nu_compraunidade = $nu_compraunidade;
        return $this;
    }

    /**
     * @param $st_prefixocupompremio
     * @return $this
     */
    public function setSt_prefixocupompremio($st_prefixocupompremio)
    {
        $this->st_prefixocupompremio = $st_prefixocupompremio;
        return $this;
    }

    /**
     * @param  $nu_descontopremio
     * @return $this
     */
    public function setNu_descontopremio($nu_descontopremio)
    {
        $this->nu_descontopremio = $nu_descontopremio;
        return $this;
    }

    /**
     * @param $id_tipodescontopremio
     * @return $this
     */
    public function setId_tipodescontopremio($id_tipodescontopremio)
    {
        $this->id_tipodescontopremio = $id_tipodescontopremio;
        return $this;
    }

    /**
     * @param $id_tipopremio
     * @return $this
     */
    public function setId_tipopremio($id_tipopremio)
    {
        $this->id_tipopremio = $id_tipopremio;
        return $this;
    }

    /**
     * @param $bl_promocaocumulativa
     * @return $this
     */
    public function setBl_promocaocumulativa($bl_promocaocumulativa)
    {
        $this->bl_promocaocumulativa = $bl_promocaocumulativa;
        return $this;
    }

    /**
     * @param $id_cupomcampanha
     * @return $this
     */
    public function setId_cupomcampanha($id_cupomcampanha)
    {
        $this->id_cupomcampanha = $id_cupomcampanha;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tipodescontopremio()
    {
        return $this->st_tipodescontopremio;
    }

    /**
     * @param string $st_tipodescontopremio
     * @return $this
     */
    public function setSt_tipodescontopremio($st_tipodescontopremio)
    {
        $this->st_tipodescontopremio = $st_tipodescontopremio;
        return $this;
    }
}

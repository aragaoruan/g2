<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoconjuntodisciplina")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAvaliacaoConjuntoDisciplina {

    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_avaliacaoconjunto;
    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var string $st_avaliacaoconjunto
     * @Column(name="st_avaliacaoconjunto", type="string", nullable=false, length=100)
     */
    private $st_avaliacaoconjunto;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_tipodisciplina
     * @Column(name="st_tipodisciplina", type="string", nullable=false, length=255)
     */
    private $st_tipodisciplina;


    /**
     * @return integer id_avaliacaoconjunto
     */
    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    /**
     * @param id_avaliacaoconjunto
     */
    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    /**
     * @return integer id_tipoprova
     */
    public function getId_tipoprova() {
        return $this->id_tipoprova;
    }

    /**
     * @param id_tipoprova
     */
    public function setId_tipoprova($id_tipoprova) {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_tipodisciplina
     */
    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    /**
     * @param id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return string st_avaliacaoconjunto
     */
    public function getSt_avaliacaoconjunto() {
        return $this->st_avaliacaoconjunto;
    }

    /**
     * @param st_avaliacaoconjunto
     */
    public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
        $this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_tipodisciplina
     */
    public function getSt_tipodisciplina() {
        return $this->st_tipodisciplina;
    }

    /**
     * @param st_tipodisciplina
     */
    public function setSt_tipodisciplina($st_tipodisciplina) {
        $this->st_tipodisciplina = $st_tipodisciplina;
        return $this;
    }


}

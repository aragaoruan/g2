<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_areaconhecimento")
 * @Entity
 * @EntityView
 * @author Denise XAvier <denise.xavier@unyleya.com.br>
 */
class VwAreaConhecimento
{

    /**
     * @var integer $id_areaconhecimento
     * @Column(type="integer", nullable=false, name="id_areaconhecimento")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_areaconhecimentopai
     * @Column(type="integer", nullable=true, name="id_areaconhecimentopai")
     */
    private $id_areaconhecimentopai;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var string $st_areaconhecimento
     * @Column(type="string", length=255, nullable=false, name="st_areaconhecimento")
     */
    private $st_areaconhecimento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
    * @var integer $id_situacao
    * @Column(type="integer", nullable=false, name="id_situacao")
    */
    private $id_situacao;


    /**
     * @var string $st_descricao
     * @Column(type="string", length=1500, nullable=false, name="st_descricao")
     */
    private $st_descricao;

    /**
     * @var integer $id_tipoareaconhecimento
     * @Column(type="integer", nullable=true, name="id_tipoareaconhecimento")
     */
    private $id_tipoareaconhecimento;

    /**
     * @var string $st_tituloexibicao
     * @Column(type="string", length=400, nullable=true, name="st_tituloexibicao")
     */
    private $st_tituloexibicao;

    /**
     * @var boolean $bl_extras
     * @Column(name="bl_extras", type="boolean", nullable=false)
     */
    private $bl_extras;

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_extras($bl_extras)
    {
        $this->bl_extras = $bl_extras;
    }

    public function getBl_extras()
    {
        return $this->bl_extras;
    }

    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimentopai($id_areaconhecimentopai)
    {
        $this->id_areaconhecimentopai = $id_areaconhecimentopai;
    }

    public function getId_areaconhecimentopai()
    {
        return $this->id_areaconhecimentopai;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_tipoareaconhecimento($id_tipoareaconhecimento)
    {
        $this->id_tipoareaconhecimento = $id_tipoareaconhecimento;
    }

    public function getId_tipoareaconhecimento()
    {
        return $this->id_tipoareaconhecimento;
    }

    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @return array Array for attributes of the entity
     */
    public function _toArray ()
    {
        return array(
            'id_areaconhecimento' =>$this->id_areaconhecimento,
            'id_areaconhecimentopai' => $this->id_areaconhecimentopai,
            'id_entidade' => $this->id_entidade,
            'st_areaconhecimento' => $this->st_areaconhecimento,
            'bl_ativo' => $this->bl_ativo,
            'id_situacao' => $this->id_situacao,
            'st_descricao' => $this->st_descricao,
            'id_tipoareaconhecimento' => $this->id_tipoareaconhecimento,
            'st_tituloexibicao' => $this->st_tituloexibicao,
            'bl_extras' => $this->bl_extras
        );
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 03/04/2018
 * Time: 15:32
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Table(name="tb_certificadoparcialdisciplina")
 * @Entity
 *
 */
class CertificadoParcialDisciplina extends G2Entity
{

    /**
     * @var integer $id_certificadoparcialdisciplina
     * @Column(name="id_certificadoparcialdisciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_certificadoparcialdisciplina;

    /**
     * @var integer $id_certificadoparcial
     * @ManyToOne(targetEntity="CertificadoParcial")
     * @JoinColumn(name="id_certificadoparcial",referencedColumnName="id_certificadoparcial", nullable=false)
     */
    private $id_certificadoparcial;

    /**
     * @var integer $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", referencedColumnName="id_disciplina", nullable=false)
     */
    private $id_disciplina;

    /**
     * @return int
     */
    public function getId_certificadoparcialdisciplina()
    {
        return $this->id_certificadoparcialdisciplina;
    }

    /**
     * @param int $id_certificadoparcialdisciplina
     */
    public function setId_certificadoparcialdisciplina($id_certificadoparcialdisciplina)
    {
        $this->id_certificadoparcialdisciplina = $id_certificadoparcialdisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_certificadoparcial()
    {
        return $this->id_certificadoparcial;
    }

    /**
     * @param int $id_certificadoparcial
     */
    public function setId_certificadoparcial($id_certificadoparcial)
    {
        $this->id_certificadoparcial = $id_certificadoparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turno")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Turno
{
    /**
     * @Id
     * @var integer $id_turno
     * @Column(name="id_turno", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */

    private $id_turno;

    /**
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=false, length=255)
     */
    private $st_turno;

    /**
     * @return integer id_turno
     */
    public function getId_turno()
    {
        return $this->id_turno;
    }

    /**
     * @param id_turno
     */
    public function setId_turno($id_turno)
    {
        $this->id_turno = $id_turno;
        return $this;
    }

    /**
     * @return string st_turno
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @param st_turno
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_emailentidademensagem")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EmailEntidadeMensagem
{

    /**
     *
     * @var EmailConfig $id_emailconfig
     * @ManyToOne(targetEntity="EmailConfig")
     * @JoinColumn(name="id_emailconfig", nullable=false, referencedColumnName="id_emailconfig")
     */
    private $id_emailconfig;

    /**
     *
     * @var Entidade $id_entidade
     * @Column(type="integer", nullable=false)
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var MensagemPadrao $id_mensagempadrao
     * @Column(type="integer", nullable=false)
     * @ManyToOne(targetEntity="MensagemPadrao")
     * @JoinColumn(name="id_mensagempadrao", nullable=false, referencedColumnName="id_mensagempadrao")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_mensagempadrao;

    /**
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    public function getId_emailconfig ()
    {
        return $this->id_emailconfig;
    }

    public function setId_emailconfig (EmailConfig $id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_mensagempadrao ()
    {
        return $this->id_mensagempadrao;
    }

    public function setId_mensagempadrao ($id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
        return $this;
    }

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema (TextoSistema $id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

}
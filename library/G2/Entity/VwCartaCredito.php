<?php

namespace G2\Entity;

/**
 * Class Entity para vw Carta de Credito
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_cartacredito")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-01-13
 */
class VwCartaCredito
{

    /**
     * @var integer $id_cartacredito
     * @Column(name="id_cartacredito", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_cartacredito;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime $dt_validade
     * @Column(name="dt_validade", type="datetime", nullable=false, length=8)
     */
    private $dt_validade;


    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var decimal $nu_valororiginal
     * @Column(name="nu_valororiginal", type="decimal", nullable=false, length=9)
     */
    private $nu_valororiginal;

    /**
     * @var decimal $nu_valorutilizado
     * @Column(name="nu_valorutilizado", type="decimal", nullable=true, length=17)
     */
    private $nu_valorutilizado;

    /**
     * @var decimal $nu_valordisponivel
     * @Column(name="nu_valordisponivel", type="decimal", nullable=true, length=17)
     */
    private $nu_valordisponivel;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @return int
     */
    public function getId_cartacredito()
    {
        return $this->id_cartacredito;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return decimal
     */
    public function getNu_valororiginal()
    {
        return $this->nu_valororiginal;
    }

    /**
     * @return decimal
     */
    public function getNu_valorutilizado()
    {
        return $this->nu_valorutilizado;
    }

    /**
     * @return decimal
     */
    public function getNu_valordisponivel()
    {
        return $this->nu_valordisponivel;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param int $id_cartacredito
     * @return $this
     */
    public function setId_cartacredito($id_cartacredito)
    {
        $this->id_cartacredito = $id_cartacredito;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param int $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @param int $id_situacao
     * @return $this
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param decimal $nu_valororiginal
     * @return $this
     */
    public function setNu_valororiginal($nu_valororiginal)
    {
        $this->nu_valororiginal = $nu_valororiginal;
        return $this;
    }

    /**
     * @param decimal $nu_valorutilizado
     * @return $this
     */
    public function setNu_valorutilizado($nu_valorutilizado)
    {
        $this->nu_valorutilizado = $nu_valorutilizado;
        return $this;
    }

    /**
     * @param decimal $nu_valordisponivel
     * @return $this
     */
    public function setNu_valordisponivel($nu_valordisponivel)
    {
        $this->nu_valordisponivel = $nu_valordisponivel;
        return $this;
    }

    /**
     * @param string $st_situacao
     * @return $this
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @param string $st_nomeentidade
     * @return $this
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getDt_validade()
    {
        return $this->dt_validade;
    }

    /**
     * @param datetime $dt_validade
     */
    public function setDt_validade($dt_validade)
    {
        $this->dt_validade = $dt_validade;
    }


}

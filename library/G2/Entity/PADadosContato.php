<?php

namespace G2\Entity;

use G2\Entity\Entidade;
use G2\Entity\Usuario;
use G2\Entity\TextoSistema;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pa_dadoscontato")
 * @Entity(repositoryClass="G2\Repository\PADadosContatoRepository")
 * @author rafael.rocha <rafael.rocha@unyleya.com.br>
 */
class PADadosContato {

    /**
     * @Id
     * @GeneratedValue
     * @Column(name="id_pa_dadoscontato", type="integer")
     */
    private $id_pa_dadoscontato;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textomensagem", referencedColumnName="id_textosistema")
     */
    private $id_textomensagem;

    /**
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textoinstrucao", referencedColumnName="id_textosistema")
     */
    private $id_textoinstrucao;

    /**
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntoco", referencedColumnName="id_assuntoco")
     */
    private $id_assuntoco;

    /**
     * @ManyToOne(targetEntity="CategoriaOcorrencia")
     * @JoinColumn(name="id_categoriaocorrencia", referencedColumnName="id_categoriaocorrencia")
     */
    private $id_categoriaocorrencia;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /** @Column(name="dt_cadastro", type="datetime2", nullable=false) */
    private $dt_cadastro;

    function __construct() {
        $this->id_usuariocadastro = new Usuario();
        $this->id_textomensagem = new TextoSistema();
        $this->id_textoinstrucao = new TextoSistema();
        $this->id_entidade = new Entidade();
        $this->id_assuntoco = new AssuntoCo();
        $this->id_categoriaocorrencia = new CategoriaOcorrencia();
    }

    public function getId_pa_dadoscontato() {
        return $this->id_pa_dadoscontato;
    }

    public function getId_textomensagem() {
        return $this->id_textomensagem;
    }

    public function getId_textoinstrucao() {
        return $this->id_textoinstrucao;
    }

    public function getId_assuntoco() {
        return $this->id_assuntoco;
    }

    public function getId_categoriaocorrencia() {
        return $this->id_categoriaocorrencia;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_pa_dadoscontato($id_pa_dadoscontato) {
        $this->id_pa_dadoscontato = $id_pa_dadoscontato;
    }

    public function setId_textomensagem($id_textomensagem) {
        $this->id_textomensagem = $id_textomensagem;
    }

    public function setId_textoinstrucao($id_textoinstrucao) {
        $this->id_textoinstrucao = $id_textoinstrucao;
    }

    public function setId_assuntoco($id_assuntoco) {
        $this->id_assuntoco = $id_assuntoco;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia) {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

}
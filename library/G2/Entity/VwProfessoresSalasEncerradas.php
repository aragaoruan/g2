<?php


namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_professoressalasencerradas")
 */

class VwProfessoresSalasEncerradas
{

    /**
     * @var integer $id_perfilreferenciaintegracao
     * @Column(name="id_perfilreferenciaintegracao", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_perfilreferenciaintegracao;

    /**
     * @var integer $id_perfilreferencia
     * @Column(name="id_perfilreferencia", type="integer", nullable=false)
     * @Id
     */
    private $id_perfilreferencia;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidade;

    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=true, length=255)
     */
    private $st_codsistemacurso;

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true)
     * @Id
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwProfessoresSalasEncerradas
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


    /**
     * @return int
     */
    public function getId_perfilreferencia()
    {
        return $this->id_perfilreferencia;
    }

    /**
     * @param int $id_perfilreferencia
     * @return $this
     */
    public function setId_perfilreferencia($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return $this
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return $this
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param mixed $st_codsistemacurso
     * @return $this
     */
    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_perfilreferenciaintegracao()
    {
        return $this->id_perfilreferenciaintegracao;
    }

    /**
     * @param int $id_perfilreferenciaintegracao
     * @return $this
     */
    public function setId_perfilreferenciaintegracao($id_perfilreferenciaintegracao)
    {
        $this->id_perfilreferenciaintegracao = $id_perfilreferenciaintegracao;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pa_marcacaoetapa")
 * @Entity
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class PaMarcacaoEtapa
{
  /**
   * @var integer $id_venda
   * @Column(name="id_venda", type="integer", nullable=true, length=4)
   */
  private $id_venda;
  /**
   * @Id
   * @var integer $id_pa_marcacaoetapa
   * @Column(name="id_pa_marcacaoetapa", type="integer", nullable=false, length=4)
   */
  private $id_pa_marcacaoetapa;
  /**
   * @var datetime2 $dt_checkin
   * @Column(name="dt_checkin", type="datetime2", nullable=false, length=8)
   */
  private $dt_checkin;
  /**
   * @var datetime2 $dt_aceita
   * @Column(name="dt_aceita", type="datetime2", nullable=true, length=8)
   */
  private $dt_aceita;
  /**
   * @var boolean $bl_mensageminformativa
   * @Column(name="bl_mensageminformativa", type="boolean", nullable=false, length=1)
   */
  private $bl_mensageminformativa;
  /**
   * @var boolean $bl_dadoscontato
   * @Column(name="bl_dadoscontato", type="boolean", nullable=false, length=1)
   */
  private $bl_dadoscontato;
  /**
   * @var boolean $bl_dadoscadastrais
   * @Column(name="bl_dadoscadastrais", type="boolean", nullable=false, length=1)
   */
  private $bl_dadoscadastrais;
  /**
   * @var boolean $bl_boasvindas
   * @Column(name="bl_boasvindas", type="boolean", nullable=false, length=1)
   */
  private $bl_boasvindas;
  /**
   * @var boolean $bl_aceitacaocontrato
   * @Column(name="bl_aceitacaocontrato", type="boolean", nullable=false, length=1)
   */
  private $bl_aceitacaocontrato;

  /**
   * @return int
   */
  public function getId_venda()
  {
    return $this->id_venda;
  }

  /**
   * @param int $id_venda
   */
  public function setId_venda($id_venda)
  {
    $this->id_venda = $id_venda;
  }

  /**
   * @return int
   */
  public function getId_pamarcacaoetapa()
  {
    return $this->id_pa_marcacaoetapa;
  }

  /**
   * @param int $id_pa_marcacaoetapa
   */
  public function setId_pamarcacaoetapa($id_pa_marcacaoetapa)
  {
    $this->id_pa_marcacaoetapa = $id_pa_marcacaoetapa;
  }

  /**
   * @return datetime2
   */
  public function getDt_checkin()
  {
    return $this->dt_checkin;
  }

  /**
   * @param datetime2 $dt_checkin
   */
  public function setDt_checkin($dt_checkin)
  {
    $this->dt_checkin = $dt_checkin;
  }

  /**
   * @return datetime2
   */
  public function getDt_aceita()
  {
    return $this->dt_aceita;
  }

  /**
   * @param datetime2 $dt_aceita
   */
  public function setDt_aceita($dt_aceita)
  {
    $this->dt_aceita = $dt_aceita;
  }

  /**
   * @return boolean
   */
  public function getBl_mensageminformativa()
  {
    return $this->bl_mensageminformativa;
  }

  /**
   * @param boolean $bl_mensageminformativa
   */
  public function setBl_mensageminformativa($bl_mensageminformativa)
  {
    $this->bl_mensageminformativa = $bl_mensageminformativa;
  }

  /**
   * @return boolean
   */
  public function getBl_dadoscontato()
  {
    return $this->bl_dadoscontato;
  }

  /**
   * @param boolean $bl_dadoscontato
   */
  public function setBl_dadoscontato($bl_dadoscontato)
  {
    $this->bl_dadoscontato = $bl_dadoscontato;
  }

  /**
   * @return boolean
   */
  public function getBl_dadoscadastrais()
  {
    return $this->bl_dadoscadastrais;
  }

  /**
   * @param boolean $bl_dadoscadastrais
   */
  public function setBl_dadoscadastrais($bl_dadoscadastrais)
  {
    $this->bl_dadoscadastrais = $bl_dadoscadastrais;
  }

  /**
   * @return boolean
   */
  public function getBl_boasvindas()
  {
    return $this->bl_boasvindas;
  }

  /**
   * @param boolean $bl_boasvindas
   */
  public function setBl_boasvindas($bl_boasvindas)
  {
    $this->bl_boasvindas = $bl_boasvindas;
  }

  /**
   * @return boolean
   */
  public function getBl_aceitacaocontrato()
  {
    return $this->bl_aceitacaocontrato;
  }

  /**
   * @param boolean $bl_aceitacaocontrato
   */
  public function setBl_aceitacaocontrato($bl_aceitacaocontrato)
  {
    $this->bl_aceitacaocontrato = $bl_aceitacaocontrato;
  }


}

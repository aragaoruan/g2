<?php

namespace G2\Entity;

use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_notificacaoentidade")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class NotificacaoEntidade
{

    /**
     * @Id
     * @var integer $id_notificacaoentidade
     * @Column(name="id_notificacaoentidade", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_notificacaoentidade;

    /**
     * @var integer $id_notificacao
     * @ManyToOne(targetEntity="Notificacao")
     * @JoinColumn(name="id_notificacao", referencedColumnName="id_notificacao")
     */
    private $id_notificacao;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_enviaparaemail
     * @Column(name="bl_enviaparaemail", type="boolean", nullable=true, length=1)
     */
    private $bl_enviaparaemail;

    /**
     * @var integer $nu_percentuallimitealunosala
     * @Column(name="nu_percentuallimitealunosala", type="integer", nullable=true, length=4)
     */
    private $nu_percentuallimitealunosala;


    /**
     * @return integer id_notificacaoentidade
     */
    public function getId_notificacaoentidade()
    {
        return $this->id_notificacaoentidade;
    }

    /**
     * @param id_notificacaoentidade
     */
    public function setId_notificacaoentidade($id_notificacaoentidade)
    {
        $this->id_notificacaoentidade = $id_notificacaoentidade;
        return $this;
    }

    /**
     * @return integer id_notificacao
     */
    public function getId_notificacao()
    {
        return $this->id_notificacao;
    }

    /**
     * @param id_notificacao
     */
    public function setId_notificacao(\G2\Entity\Notificacao $id_notificacao)
    {
        $this->id_notificacao = $id_notificacao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade(\G2\Entity\Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro(\DateTime $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean bl_enviaparaemail
     */
    public function getBl_enviaparaemail()
    {
        return $this->bl_enviaparaemail;
    }

    /**
     * @param bl_enviaparaemail
     */
    public function setBl_enviaparaemail($bl_enviaparaemail)
    {
        $this->bl_enviaparaemail = $bl_enviaparaemail;
        return $this;
    }

    /**
     * @return integer nu_percentuallimitealunosala
     */
    public function getNu_percentuallimitealunosala()
    {
        return $this->nu_percentuallimitealunosala;
    }

    /**
     * @param nu_percentuallimitealunosala
     */
    public function setNu_percentuallimitealunosala($nu_percentuallimitealunosala)
    {
        $this->nu_percentuallimitealunosala = $nu_percentuallimitealunosala;
        return $this;
    }



}
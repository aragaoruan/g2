<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_aplicadorintegracao")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAplicadorIntegracao
{

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_aplicadorprova;

    /**
     *
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=false, length=200)
     */
    private $st_aplicadorprova;


    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    /**
     * @var datetime2 $dt_sincronizado
     * @Column(type="datetime2", nullable=false, name="dt_sincronizado")
     */
    private $dt_sincronizado;


    /**
     *
     * @var boolean $bl_sincronizado
     * @Column(name="bl_sincronizado", type="boolean", nullable=false)
     */
    private $bl_sincronizado;

    /**
     *
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=200)
     */
    private $st_endereco;

    /**
     *
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=200)
     */
    private $st_nomemunicipio;

    /**
     *
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }


    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @return datetime2
     */
    public function getDt_sincronizado()
    {
        return $this->dt_sincronizado;
    }

    /**
     * @param datetime2 $dt_sincronizado
     */
    public function setDt_sincronizado($dt_sincronizado)
    {
        $this->dt_sincronizado = $dt_sincronizado;
    }

    /**
     * @return bool
     */
    public function isBl_sincronizado()
    {
        return $this->bl_sincronizado;
    }

    /**
     * @param bool $bl_sincronizado
     */
    public function setBl_sincronizado($bl_sincronizado)
    {
        $this->bl_sincronizado = $bl_sincronizado;
    }

    /**
     * @return string
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @return string
     */
    public function getSt_nomemunicipio()
    {
        return $this->st_nomemunicipio;
    }

    /**
     * @param string $st_nomemunicipio
     */
    public function setSt_nomemunicipio($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }


}

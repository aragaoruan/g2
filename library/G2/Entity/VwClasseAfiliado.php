<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_classeafiliado")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwClasseAfiliado
{

    /**
     *
     * @var integer $id_classeafiliado
     * @Column(name="id_classeafiliado", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_classeafiliado;

    /**
     *
     * @var integer $id_tipopessoa
     * @Column(name="id_tipopessoa", type="integer", nullable=false)
     */
    private $id_tipopessoa;

    /**
     *
     * @var string $st_tipopessoa
     * @Column(name="st_tipopessoa", type="string", nullable=false, length=20)
     */
    private $st_tipopessoa;

    /**
     *
     * @var integer $id_tipovalorcomissao
     * @Column(name="id_tipovalorcomissao", type="integer", nullable=false)
     */
    private $id_tipovalorcomissao;

    /**
     *
     * @var string $st_tipovalorcomissao
     * @Column(name="st_tipovalorcomissao", type="string", nullable=false, length=150)
     */
    private $st_tipovalorcomissao;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_textosistemacontrato
     * @Column(name="id_textosistemacontrato", type="integer", nullable=true)
     */
    private $id_textosistemacontrato;

    /**
     *
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=false, length=200)
     */
    private $st_textosistema;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var string $st_classeafiliado
     * @Column(name="st_classeafiliado", type="string", nullable=false, length=200)
     */
    private $st_classeafiliado;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=200)
     */
    private $st_descricao;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var string $st_contratoafiliado
     * @Column(name="st_contratoafiliado", type="string", nullable=true, length=20)
     */
    private $st_contratoafiliado;

    public function getId_classeafiliado ()
    {
        return $this->id_classeafiliado;
    }

    public function setId_classeafiliado ($id_classeafiliado)
    {
        $this->id_classeafiliado = $id_classeafiliado;
        return $this;
    }

    public function getId_tipopessoa ()
    {
        return $this->id_tipopessoa;
    }

    public function setId_tipopessoa ($id_tipopessoa)
    {
        $this->id_tipopessoa = $id_tipopessoa;
        return $this;
    }

    public function getSt_tipopessoa ()
    {
        return $this->st_tipopessoa;
    }

    public function setSt_tipopessoa ($st_tipopessoa)
    {
        $this->st_tipopessoa = $st_tipopessoa;
        return $this;
    }

    public function getId_tipovalorcomissao ()
    {
        return $this->id_tipovalorcomissao;
    }

    public function setId_tipovalorcomissao ($id_tipovalorcomissao)
    {
        $this->id_tipovalorcomissao = $id_tipovalorcomissao;
        return $this;
    }

    public function getSt_tipovalorcomissao ()
    {
        return $this->st_tipovalorcomissao;
    }

    public function setSt_tipovalorcomissao ($st_tipovalorcomissao)
    {
        $this->st_tipovalorcomissao = $st_tipovalorcomissao;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_textosistemacontrato ()
    {
        return $this->id_textosistemacontrato;
    }

    public function setId_textosistemacontrato ($id_textosistemacontrato)
    {
        $this->id_textosistemacontrato = $id_textosistemacontrato;
        return $this;
    }

    public function getSt_textosistema ()
    {
        return $this->st_textosistema;
    }

    public function setSt_textosistema ($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getSt_classeafiliado ()
    {
        return $this->st_classeafiliado;
    }

    public function setSt_classeafiliado ($st_classeafiliado)
    {
        $this->st_classeafiliado = $st_classeafiliado;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_contratoafiliado ()
    {
        return $this->st_contratoafiliado;
    }

    public function setSt_contratoafiliado ($st_contratoafiliado)
    {
        $this->st_contratoafiliado = $st_contratoafiliado;
        return $this;
    }

}
<?php

/*
 * Entity DocumentosUtilizacaoRelacao
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-01
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentosutilizacaorelacao")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DocumentosUtilizacaoRelacao
{
    /**
     *
     * @var integer $id_documentos
     * @Column(name="id_documentos", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_documentos;

    /**
     * @var integer $id_documentosutilizacao
     * @Column(name="id_documentosutilizacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_documentosutilizacao;


    public function getId_documentos()
    {
        return $this->id_documentos;
    }

    public function getId_documentosutilizacao()
    {
        return $this->id_documentosutilizacao;
    }

    public function setId_documentos($id_documentos)
    {
        $this->id_documentos = $id_documentos;
    }

    public function setId_documentosutilizacao($id_documentosutilizacao)
    {
        $this->id_documentosutilizacao = $id_documentosutilizacao;
    }
}
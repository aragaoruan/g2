<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_campanhaprodutocategoria")
 * @Entity(repositoryClass="\G2\Repository\CampanhaComercial")
 * @EntityView
 * @author Elcio Guimarães <elcio.guimaraes@unyleya.com.br>
 */
class VwCampanhaProdutoCategoria
{

    /**
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=false, length=255)
     */
    private $st_campanhacomercial;

    /**
     * @var integer $id_tipodesconto
     * @Column(name="id_tipodesconto", type="integer", nullable=false, length=4)
     */
    private $id_tipodesconto;

    /**
     * @var string $st_tipodesconto
     * @Column(name="st_tipodesconto", type="string", nullable=false, length=255)
     */
    private $st_tipodesconto;

    /**
     * @var integer $id_finalidadecampanha
     * @Column(name="id_finalidadecampanha", type="integer", nullable=false, length=4)
     */
    private $id_finalidadecampanha;

    /**
     * @var string $st_finalidadecampanha
     * @Column(name="st_finalidadecampanha", type="string", nullable=false, length=255)
     */
    private $st_finalidadecampanha;

    /**
     * @var decimal $nu_valordesconto
     * @Column(type="decimal", nullable=false, name="nu_valordesconto")
     */
    private $nu_valordesconto;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var datetime2 $dt_fim
     * @Column(name="dt_fim", type="datetime2", nullable=true)
     */
    private $dt_fim;

    /**
     *
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=10)
     */
    private $id_entidade;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=10)
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var integer $id_tipocampanha
     * @Column(name="id_tipocampanha", type="integer", nullable=false, length=10)
     */
    private $id_tipocampanha;

    /**
     * @var string $st_tipocampanha
     * @Column(name="st_tipocampanha", type="string", nullable=false, length=255)
     */
    private $st_tipocampanha;

    /**
     *
     * @var boolean $bl_vigente
     * @Column(name="bl_vigente", type="boolean", nullable=false)
     */
    private $bl_vigente;

    /**
     * @var decimal $nu_valororiginal
     * @Column(type="decimal", nullable=false, name="nu_valororiginal")
     */
    private $nu_valororiginal;

    /**
     * @var decimal $nu_valorvenda
     * @Column(type="decimal", nullable=false, name="nu_valorvenda")
     */
    private $nu_valorvenda;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;

    /**
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false, length=4)
     */
    private $id_tipoproduto;

    /**
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=false, length=255)
     */
    private $st_tipoproduto;

    /**
     * @return string
     */
    public function getst_tipodesconto()
    {
        return $this->st_tipodesconto;
    }

    /**
     * @param string $st_tipodesconto
     */
    public function setst_tipodesconto($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setid_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @param string $st_campanhacomercial
     */
    public function setst_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipodesconto()
    {
        return $this->id_tipodesconto;
    }

    /**
     * @param int $id_tipodesconto
     */
    public function setid_tipodesconto($id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_finalidadecampanha()
    {
        return $this->id_finalidadecampanha;
    }

    /**
     * @param int $id_finalidadecampanha
     */
    public function setid_finalidadecampanha($id_finalidadecampanha)
    {
        $this->id_finalidadecampanha = $id_finalidadecampanha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_finalidadecampanha()
    {
        return $this->st_finalidadecampanha;
    }

    /**
     * @param mixed $st_finalidadecampanha
     */
    public function setst_finalidadecampanha($st_finalidadecampanha)
    {
        $this->st_finalidadecampanha = $st_finalidadecampanha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getnu_valordesconto()
    {
        return $this->nu_valordesconto;
    }

    /**
     * @param mixed $nu_valordesconto
     */
    public function setnu_valordesconto($nu_valordesconto)
    {
        $this->nu_valordesconto = $nu_valordesconto;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;

    }

    /**
     * @return datetime2
     */
    public function getdt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param datetime2 $dt_fim
     */
    public function setdt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;

    }

    /**
     * @return datetime2
     */
    public function getdt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param datetime2 $dt_inicio
     */
    public function setdt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param int $id_tipocampanha
     */
    public function setid_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipocampanha()
    {
        return $this->st_tipocampanha;
    }

    /**
     * @param string $st_tipocampanha
     */
    public function setst_tipocampanha($st_tipocampanha)
    {
        $this->st_tipocampanha = $st_tipocampanha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_vigente()
    {
        return $this->bl_vigente;
    }

    /**
     * @param mixed $bl_vigente
     */
    public function setbl_vigente($bl_vigente)
    {
        $this->bl_vigente = $bl_vigente;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getnu_valororiginal()
    {
        return $this->nu_valororiginal;
    }

    /**
     * @param mixed $nu_valororiginal
     */
    public function setnu_valororiginal($nu_valororiginal)
    {
        $this->nu_valororiginal = $nu_valororiginal;
        return $this;

    }

    /**
     * @return decimal
     */
    public function getnu_valorvenda()
    {
        return $this->nu_valorvenda;
    }

    /**
     * @param decimal $nu_valorvenda
     */
    public function setnu_valorvenda($nu_valorvenda)
    {
        $this->nu_valorvenda = $nu_valorvenda;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     */
    public function setid_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_produto()
    {
        return $this->st_produto;
    }

    /**
     * @param string $st_produto
     */
    public function setst_produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @param int $id_tipoproduto
     */
    public function setid_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipoproduto()
    {
        return $this->st_tipoproduto;
    }

    /**
     * @param string $st_tipoproduto
     */
    public function setst_tipoproduto($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;

    }


}

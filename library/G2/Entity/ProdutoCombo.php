<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtocombo")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
use G2\G2Entity;

class ProdutoCombo extends G2Entity
{

    /**
     *
     * @var integer $id_produtocombo
     * @Column(name="id_produtocombo", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtocombo;

    /**
     *
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     *
     * @var integer $id_produtoitem
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produtoitem", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produtoitem;
    
    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;
    
    
    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;
    
    
    /**
     *
     * @var string $nu_descontoporcentagem
     * @Column(name="nu_descontoporcentagem", type="decimal", nullable=true)
     */
    private $nu_descontoporcentagem;
    
    
    
	/**
	 * @return the $id_produtocombo
	 */
	public function getId_produtocombo() {
		return $this->id_produtocombo;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_produtoitem
	 */
	public function getId_produtoitem() {
		return $this->id_produtoitem;
	}

	/**
	 * @return the $id_usuario
	 */
	public function getId_usuario() {
		return $this->id_usuario;
	}

	/**
	 * @return the $dt_cadastro
	 */
	public function getDt_cadastro() {
		return $this->dt_cadastro;
	}

	/**
	 * @return the $nu_descontoporcentagem
	 */
	public function getNu_descontoporcentagem() {
		return $this->nu_descontoporcentagem;
	}

	/**
	 * @param number $id_produtocombo
	 */
	public function setId_produtocombo($id_produtocombo) {
		$this->id_produtocombo = $id_produtocombo;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param number $id_produtoitem
	 */
	public function setId_produtoitem($id_produtoitem) {
		$this->id_produtoitem = $id_produtoitem;
		return $this;
	}

	/**
	 * @param \G2\Entity\Usuario $id_usuario
	 */
	public function setId_usuario($id_usuario) {
		$this->id_usuario = $id_usuario;
		return $this;
	}

	/**
	 * @param \G2\Entity\datetime2 $dt_cadastro
	 */
	public function setDt_cadastro($dt_cadastro) {
		$this->dt_cadastro = $dt_cadastro;
		return $this;
	}

	/**
	 * @param string $nu_descontoporcentagem
	 */
	public function setNu_descontoporcentagem($nu_descontoporcentagem) {
		$this->nu_descontoporcentagem = $nu_descontoporcentagem;
		return $this;
	}

    
    

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_prematriculadisciplina")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @EntityLog
 */
class PreMatriculaDisciplina extends G2Entity
{

    /**
     * @var integer $id_prematriculadisciplina
     * @Column(name="id_prematriculadisciplina", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_prematriculadisciplina;
    /**
     * @var \G2\Entity\Venda $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda", nullable=false)
     */
    private $id_venda;
    /**
     * @var \G2\Entity\Disciplina $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", referencedColumnName="id_disciplina", nullable=false)
     */
    private $id_disciplina;
    /**
     * @var \G2\Entity\SalaDeAula $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", referencedColumnName="id_saladeaula", nullable=false)
     */
    private $id_saladeaula;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var \G2\Entity\Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_alocado
     * @Column(name="bl_alocado", type="boolean", nullable=false, length=1)
     */
    private $bl_alocado = false;

    /**
     * @return int
     */
    public function getId_prematriculadisciplina()
    {
        return $this->id_prematriculadisciplina;
    }

    /**
     * @param int $id_prematriculadisciplina
     * @return PreMatriculaDisciplina
     */
    public function setId_prematriculadisciplina($id_prematriculadisciplina)
    {
        $this->id_prematriculadisciplina = $id_prematriculadisciplina;
        return $this;
    }

    /**
     * @return Venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param Venda $id_venda
     * @return PreMatriculaDisciplina
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return Disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param Disciplina $id_disciplina
     * @return PreMatriculaDisciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return SalaDeAula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param SalaDeAula $id_saladeaula
     * @return PreMatriculaDisciplina
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_alocado()
    {
        return $this->bl_alocado;
    }

    /**
     * @param bool $bl_alocado
     * @return $this
     */
    public function setBl_alocado($bl_alocado)
    {
        $this->bl_alocado = $bl_alocado;
        return $this;
    }
}
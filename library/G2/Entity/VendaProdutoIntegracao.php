<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity()
 * @Table( name="tb_vendaprodutointegracao")
 * @HasLifecycleCallbacks
 */
class VendaProdutoIntegracao
{

    /**
     * @var integer $id_vendaprodutointegracao
     * @id
     * @Column(name="id_vendaprodutointegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_vendaprodutointegracao;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumns({
     *      @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     * })
     */
    private $id_sistema;

    /**
     * @var Entidade
     *
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumns({
     *      @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     * })
     */
    private $id_entidade;

    /**
     * @var Venda
     *
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumns({
     *      @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     * })
     */
    private $id_venda;

    /**
     * @var Produto
     *
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumns({
     *      @JoinColumn(name="id_produto", referencedColumnName="id_produto")
     * })
     */
    private $id_produto;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var datetime $dt_sincronizado
     * @Column(name="dt_sincronizado", type="datetime", nullable=false)
     */
    private  $dt_sincronizado;

    /**
     * @var string $st_codvenda
     * @Column(name="st_codvenda", type="string", nullable=true)
     */
    private $st_codvenda;

    /**
     * @var string $st_codigoprodutoexterno
     * @Column(name="st_codigoprodutoexterno", type="string")
     */
    private $st_codigoprodutoexterno;

    /**
     * @return int
     */
    public function getId_vendaprodutointegracao()
    {
        return $this->id_vendaprodutointegracao;
    }

    /**
     * @param int $id_vendaprodutointegracao
     */
    public function setId_vendaprodutointegracao($id_vendaprodutointegracao)
    {
        $this->id_vendaprodutointegracao = $id_vendaprodutointegracao;
    }

    /**
     * @return Sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param Sistema $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return Venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param Venda $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return Produto
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param Produto $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return datetime
     */
    public function getDt_sincronizado()
    {
        return $this->dt_sincronizado;
    }

    /**
     * @param datetime $dt_sincronizado
     */
    public function setDt_sincronizado($dt_sincronizado)
    {
        $this->dt_sincronizado = $dt_sincronizado;
    }

    /**
     * @return string
     */
    public function getSt_codvenda()
    {
        return $this->st_codvenda;
    }

    /**
     * @param string $st_codvenda
     */
    public function setSt_codvenda($st_codvenda)
    {
        $this->st_codvenda = $st_codvenda;
    }

    /**
     * @return string
     */
    public function getSt_codigoprodutoexterno()
    {
        return $this->st_codigoprodutoexterno;
    }

    /**
     * @param string $st_codigoprodutoexterno
     */
    public function setSt_codigoprodutoexterno($st_codigoprodutoexterno)
    {
        $this->st_codigoprodutoexterno = $st_codigoprodutoexterno;
    }

    /**
     * @PrePersist
     */
    public function prePersist(){
        $this->setDt_cadastro(new \DateTime());
        $this->setDt_sincronizado(new \DateTime());
    }

}
<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadefinanceiro")
 * @Entity
 * @EntityLog
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadeFinanceiro
{

    /**
     * @var integer $id_entidadefinanceiro
     * @Column(name="id_entidadefinanceiro", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidadefinanceiro;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_automatricula
     * @Column(name="bl_automatricula", type="boolean", nullable=false)
     */
    private $bl_automatricula;

    /**
     * @var TextoSistema $id_textosistemarecibo
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistemarecibo", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textosistemarecibo;

    /**
     * @var boolean $bl_protocolo
     * @Column(name="bl_protocolo", type="boolean", nullable=false)
     */
    private $bl_protocolo;

    /**
     * @var string $st_linkloja
     * @Column(name="st_linkloja", type="string", nullable=true)
     */
    private $st_linkloja;

    /**
     * @var string $nu_avisoatraso
     * @Column(name="nu_avisoatraso", type="integer", nullable=true)
     */
    private $nu_avisoatraso;

    /**
     * @var TextoSistema $id_textoavisoatraso
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textoavisoatraso", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textoavisoatraso;

    /**
     * @var string $nu_diaspagamentoentrada
     * @Column(name="nu_diaspagamentoentrada", type="integer", nullable=true)
     */
    private $nu_diaspagamentoentrada;

    /**
     * @var TextoSistema $id_reciboconsolidado
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_reciboconsolidado", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_reciboconsolidado;

    /**
     *
     * @var $boletoConfig
     * @ManyToMany(targetEntity="BoletoConfig", inversedBy="entidadeFinaceira")
     * @JoinTable(name="tb_entidadeboletoconfig",
     *   joinColumns={@JoinColumn(name="id_entidadefinanceiro", referencedColumnName="id_entidadefinanceiro")},
     *   inverseJoinColumns={@JoinColumn(name="id_boletoconfig", referencedColumnName="id_boletoconfig")}
     * )
     */
    private $boletoConfig;

    /**
     *
     * @var $cartaoConfig
     * @ManyToMany(targetEntity="CartaoConfig", inversedBy="entidadeFinaceira")
     * @JoinTable(name="tb_entidadecartaoconfig",
     *  joinColumns={@JoinColumn(name="id_entidadefinanceiro", referencedColumnName="id_entidadefinanceiro")},
     *  inverseJoinColumns={@JoinColumn(name="id_cartaoconfig", referencedColumnName="id_cartaoconfig")}
     * )
     */
    private $cartaoConfig;

    /**
     * @var TextoSistema $id_textochequesdevolvidos
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textochequesdevolvidos", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textochequesdevolvidos;

    /**
     * @var decimal $nu_multacomcarta
     * @Column(name="nu_multacomcarta", type="decimal", nullable=true)
     */
    private $nu_multacomcarta;

    /**
     * @var decimal $nu_multasemcarta
     * @Column(name="nu_multasemcarta", type="decimal", nullable=true)
     */
    private $nu_multasemcarta;

    /**
     * @var TextoSistema $id_textoimpostorenda
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textoimpostorenda", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textoimpostorenda;

    public function __construct()
    {
        $this->boletoConfig = new ArrayCollection();
        $this->cartaoConfig = new ArrayCollection();
    }

    public function getBoletoConfig()
    {
        return $this->boletoConfig;
    }

    public function getCartaoConfig()
    {
        return $this->cartaoConfig;
    }

    public function getId_entidadefinanceiro()
    {
        return $this->id_entidadefinanceiro;
    }

    public function setId_entidadefinanceiro($id_entidadefinanceiro)
    {
        $this->id_entidadefinanceiro = $id_entidadefinanceiro;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getBl_automatricula()
    {
        return $this->bl_automatricula;
    }

    public function setBl_automatricula($bl_automatricula)
    {
        $this->bl_automatricula = $bl_automatricula;
        return $this;
    }

    public function getId_textosistemarecibo()
    {
        return $this->id_textosistemarecibo;
    }

    public function setId_textosistemarecibo(TextoSistema $id_textosistemarecibo)
    {
        $this->id_textosistemarecibo = $id_textosistemarecibo;
        return $this;
    }

    public function getBl_protocolo()
    {
        return $this->bl_protocolo;
    }

    public function setBl_protocolo($bl_protocolo)
    {
        $this->bl_protocolo = $bl_protocolo;
        return $this;
    }

    public function getSt_linkloja()
    {
        return $this->st_linkloja;
    }

    public function setSt_linkloja($st_linkloja)
    {
        $this->st_linkloja = $st_linkloja;
        return $this;
    }

    public function getNu_avisoatraso()
    {
        return $this->nu_avisoatraso;
    }

    public function setNu_avisoatraso($nu_avisoatraso)
    {
        $this->nu_avisoatraso = $nu_avisoatraso;
        return $this;
    }

    public function getId_textoavisoatraso()
    {
        return $this->id_textoavisoatraso;
    }

    public function setId_textoavisoatraso(TextoSistema $id_textoavisoatraso)
    {
        $this->id_textoavisoatraso = $id_textoavisoatraso;
        return $this;
    }

    public function getNu_diaspagamentoentrada()
    {
        return $this->nu_diaspagamentoentrada;
    }

    public function setNu_diaspagamentoentrada($nu_diaspagamentoentrada)
    {
        $this->nu_diaspagamentoentrada = $nu_diaspagamentoentrada;
        return $this;
    }

    public function getId_reciboconsolidado()
    {
        return $this->id_reciboconsolidado;
    }

    public function setId_reciboconsolidado(TextoSistema $id_reciboconsolidado)
    {
        $this->id_reciboconsolidado = $id_reciboconsolidado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_textochequesdevolvidos()
    {
        return $this->id_textochequesdevolvidos;
    }

    /**
     * @param mixed $id_textochequesdevolvidos
     */
    public function setId_textochequesdevolvidos($id_textochequesdevolvidos)
    {
        $this->id_textochequesdevolvidos = $id_textochequesdevolvidos;
    }

    /**
     * @return decimal
     */
    public function getNu_multacomcarta()
    {
        return $this->nu_multacomcarta;
    }

    /**
     * @param decimal $nu_multacomcarta
     */
    public function setNu_multacomcarta($nu_multacomcarta)
    {
        $this->nu_multacomcarta = $nu_multacomcarta;
    }

    /**
     * @return decimal
     */
    public function getNu_multasemcarta()
    {
        return $this->nu_multasemcarta;
    }

    /**
     * @param decimal $nu_multasemcarta
     */
    public function setNu_multasemcarta($nu_multasemcarta)
    {
        $this->nu_multasemcarta = $nu_multasemcarta;
    }

    /**
     * @return TextoSistema
     */
    public function getId_textoimpostorenda()
    {
        return $this->id_textoimpostorenda;
    }

    /**
     * @param TextoSistema $id_textoimpostorenda
     */
    public function setId_textoimpostorenda($id_textoimpostorenda)
    {
        $this->id_textoimpostorenda = $id_textoimpostorenda;
    }


}

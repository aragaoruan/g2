<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtoentidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyeleya.com.br>
 */
use G2\G2Entity;

class ProdutoEntidade extends G2Entity
{

    /**
     * @var integer $id_produtoentidade
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_produtoentidade", type="integer", nullable=false, length=4)
     */
    private $id_produtoentidade;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=true, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @return integer
     */
    public function getId_produtoentidade ()
    {
        return $this->id_produtoentidade;
    }

    /**
     * @param integer $id_produtoentidade
     * @return \G2\Entity\ProdutoEntidade
     */
    public function setId_produtoentidade ($id_produtoentidade)
    {
        $this->id_produtoentidade = $id_produtoentidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\ProdutoEntidade
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Produto
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * @param \G2\Entity\Produto $id_produto
     * @return \G2\Entity\ProdutoEntidade
     */
    public function setId_produto (Produto $id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwImportaSalasMoodle"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_importasalasmoodle")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwImportaSalasMoodle
{

    /**
     * @SWG\Property()
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;


    /**
     * @SWG\Property()
     *
     * @var string $nu_dias
     * @Column(name="nu_dias", type="integer", nullable=true)
     */
    private $nu_dias;


    /**
     * @SWG\Property()
     *
     * @var integer $id_disciplinaintegracao
     * @Column(name="id_disciplinaintegracao", type="integer", nullable=true)
     * @Id
     */
    private $id_disciplinaintegracao;

    /**
     * @SWG\Property()
     *
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=true, length=30)
     */
    private $st_codsistema;

    /**
     * @SWG\Property()
     *
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=true, length=30)
     */
    private $st_codsistemacurso;


    /**
     * @SWG\Property()
     *
     * @var string $st_salareferencia
     * @Column(name="st_salareferencia", type="string", nullable=true, length=255)
     */
    private $st_salareferencia;

    /**
     * @SWG\Property()
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     * @Id
     */
    private $id_entidade;

    /**
     * @SWG\Property()
     *
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    /**
     * @SWG\Property()
     *
     * @var integer $id_saladeaulaintegracao
     * @Column(name="id_saladeaulaintegracao", type="integer", nullable=true)
     * @Id
     */
    private $id_saladeaulaintegracao;

    /**
     * @SWG\Property()
     *
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true)
     * @Id
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwImportaSalasMoodle
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getNu_dias()
    {
        return $this->nu_dias;
    }

    public function setNu_dias($nu_dias)
    {
        $this->nu_dias = $nu_dias;
    }

    public function getId_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    public function setId_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
    }

    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }

    public function getSt_salareferencia()
    {
        return $this->st_salareferencia;
    }

    public function setSt_salareferencia($st_salareferencia)
    {
        $this->st_salareferencia = $st_salareferencia;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
    }


}

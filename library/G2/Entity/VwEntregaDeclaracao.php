<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entregadeclaracao")
 * @Entity
 * @EntityView
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class VwEntregaDeclaracao
{
    /**
     * @var integer $id_entregadeclaracao
     * @Column(name="id_entregadeclaracao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entregadeclaracao;
    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_solicitacao;
    /**
     * @var datetime2 $dt_envio
     * @Column(name="dt_envio", type="datetime2", nullable=true, length=8)
     */
    private $dt_envio;
    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2", nullable=true, length=8)
     */
    private $dt_entrega;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=false, length=4)
     */
    private $id_textosistema;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=false, length=200)
     */
    private $st_textosistema;


    /**
     * @return datetime2 dt_solicitacao
     */
    public function getDt_solicitacao()
    {
        return $this->dt_solicitacao;
    }

    /**
     * @param dt_solicitacao
     */
    public function setDt_solicitacao($dt_solicitacao)
    {
        $this->dt_solicitacao = $dt_solicitacao;
        return $this;
    }

    /**
     * @return datetime2 dt_envio
     */
    public function getDt_envio()
    {
        return $this->dt_envio;
    }

    /**
     * @param dt_envio
     */
    public function setDt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
        return $this;
    }

    /**
     * @return datetime2 dt_entrega
     */
    public function getDt_entrega()
    {
        return $this->dt_entrega;
    }

    /**
     * @param dt_entrega
     */
    public function setDt_entrega($dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_entregadeclaracao
     */
    public function getId_entregadeclaracao()
    {
        return $this->id_entregadeclaracao;
    }

    /**
     * @param id_entregadeclaracao
     */
    public function setId_entregadeclaracao($id_entregadeclaracao)
    {
        $this->id_entregadeclaracao = $id_entregadeclaracao;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_textosistema
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param id_textosistema
     */
    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_textosistema
     */
    public function getSt_textosistema()
    {
        return $this->st_textosistema;
    }

    /**
     * @param st_textosistema
     */
    public function setSt_textosistema($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

}

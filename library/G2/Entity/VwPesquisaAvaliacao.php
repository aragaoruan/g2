<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_pesquisaavaliacao")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisaAvaliacao
{

    /**
     *
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacao;

    /**
     *
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=false, length=100)
     */
    private $st_avaliacao;

    /**
     *
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false)
     */
    private $id_tipoavaliacao;

    /**
     *
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false)
     */
    private $nu_valor;

    /**
     *
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", length=200)
     */
    private $st_tipoavaliacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @Column(name="bl_recuperacao", type="boolean", nullable=true)
     * @var boolean $bl_recuperacao
     */
    private $bl_recuperacao;


    /**
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true)
     * @var integer $id_avaliacaorecupera
     */
    private $id_avaliacaorecupera;

    /**
     *
     * @var string $st_entidade
     * @Column(name="st_entidade", type="string", nullable=false)
     */
    private $st_entidade;

    /**
     *
     * @var integer $id_entidadeavaliacao
     * @Column(name="id_entidadeavaliacao", type="integer", nullable=false)
     */
    private $id_entidadeavaliacao;

    /**
     *
     * @var string $st_entidadeavaliacao
     * @Column(name="st_entidadeavaliacao", type="string", nullable=false)
     */
    private $st_entidadeavaliacao;

    public function getId_avaliacao ()
    {
        return $this->id_avaliacao;
    }

    public function setId_avaliacao ($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function getSt_avaliacao ()
    {
        return $this->st_avaliacao;
    }

    public function setSt_avaliacao ($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    public function getId_tipoavaliacao ()
    {
        return $this->id_tipoavaliacao;
    }

    public function setId_tipoavaliacao ($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_tipoavaliacao ()
    {
        return $this->st_tipoavaliacao;
    }

    public function setSt_tipoavaliacao ($st_tipoavaliacao)
    {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getBl_recuperacao()
    {
        return $this->bl_recuperacao;
    }

    public function setBl_recuperacao($bl_recuperacao)
    {
        $this->bl_recuperacao = $bl_recuperacao;
    }

    /**
     * @return int
     */
    public function getId_avaliacaorecupera()
    {
        return $this->id_avaliacaorecupera;
    }

    /**
     * @param int $id_avaliacaorecupera
     */
    public function setId_avaliacaorecupera($id_avaliacaorecupera)
    {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
    }

    /**
     * @return string
     */
    public function getSt_entidade()
    {
        return $this->st_entidade;
    }

    /**
     * @param string $st_entidade
     * @return VwPesquisaAvaliacao
     */
    public function setSt_entidade($st_entidade)
    {
        $this->st_entidade = $st_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadeavaliacao()
    {
        return $this->id_entidadeavaliacao;
    }

    /**
     * @param int $id_entidadeavaliacao
     * @return VwPesquisaAvaliacao
     */
    public function setId_entidadeavaliacao($id_entidadeavaliacao)
    {
        $this->id_entidadeavaliacao = $id_entidadeavaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_entidadeavaliacao()
    {
        return $this->st_entidadeavaliacao;
    }

    /**
     * @param string $st_entidadeavaliacao
     * @return VwPesquisaAvaliacao
     */
    public function setSt_entidadeavaliacao($st_entidadeavaliacao)
    {
        $this->st_entidadeavaliacao = $st_entidadeavaliacao;
        return $this;
    }

}
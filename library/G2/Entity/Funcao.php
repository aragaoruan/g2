<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_funcao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Funcao
{

    /**
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_funcao;

    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=true, length=100)
     */
    private $st_funcao;

    /**
     * @var TipoFuncao $id_tipofuncao
     * @ManyToOne(targetEntity="TipoFuncao")
     * @JoinColumn(name="id_tipofuncao", nullable=false, referencedColumnName="id_tipofuncao")
     */
    private $id_tipofuncao;

    public function setId_funcao ($id_funcao)
    {
        $this->id_funcao = $id_funcao;
    }

    public function getId_funcao ()
    {
        return $this->id_funcao;
    }

    public function setId_tipofuncao ($id_tipofuncao)
    {
        $this->id_tipofuncao = $id_tipofuncao;
    }

    public function getId_tipofuncao ()
    {
        return $this->id_tipofuncao;
    }

    public function setSt_funcao ($st_funcao)
    {
        $this->st_funcao = $st_funcao;
    }

    public function getSt_funcao ()
    {
        return $this->st_funcao;
    }

}
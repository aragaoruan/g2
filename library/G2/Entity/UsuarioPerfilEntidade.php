<?php

namespace G2\Entity;

/**
 * Description of UsuarioPerfilEntidade
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_usuarioperfilentidade")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class UsuarioPerfilEntidade {

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false, length=3)
     */
    private $dt_inicio;

    /**
     * @var date $dt_termino
     * @Column(name="dt_termino", type="date", nullable=true, length=3)
     */
    private $dt_termino;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8, options={"default":NULL})
     */
    private $dt_cadastro;

    /**
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @Id
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_entidadeidentificacao
     * @Column(name="id_entidadeidentificacao", type="integer", nullable=true, length=4)
     */
    private $id_entidadeidentificacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_tutorial
     * @Column(name="bl_tutorial", type="boolean", nullable=true, length=1)
     */
    private $bl_tutorial;


    /**
     * @Column(name="st_token", type="string",length=32,nullable=true)
     * @var string
     */
    private $st_token;

    /**
     * @return string
     */
    public function getStToken()
    {
        return $this->st_token;
    }

    /**
     * @param string $st_token
     */
    public function setStToken($st_token)
    {
        $this->st_token = $st_token;
    }




    /**
     * @return date dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return date dt_termino
     */
    public function getDt_termino() {
        return $this->dt_termino;
    }

    /**
     * @param dt_termino
     */
    public function setDt_termino($dt_termino) {
        $this->dt_termino = $dt_termino;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil() {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_entidadeidentificacao
     */
    public function getId_entidadeidentificacao() {
        return $this->id_entidadeidentificacao;
    }

    /**
     * @param id_entidadeidentificacao
     */
    public function setId_entidadeidentificacao($id_entidadeidentificacao) {
        $this->id_entidadeidentificacao = $id_entidadeidentificacao;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return boolean bl_tutorial
     */
    public function getBl_tutorial() {
        return $this->bl_tutorial;
    }

    /**
     * @param bl_tutorial
     */
    public function setBl_tutorial($bl_tutorial) {
        $this->bl_tutorial = $bl_tutorial;
        return $this;
    }

}

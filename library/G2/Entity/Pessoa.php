<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Class Pessoa
 * @package G2\Entity
 * @SWG\Definition(@SWG\Xml(name="Pessoa"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pessoa")
 * @Entity(repositoryClass="\G2\Repository\Pessoa")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 *
 */
class Pessoa extends G2Entity
{

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @Column(name="id_usuario",type="integer",nullable=false)
     * @Id
     */
    private $id_usuario;

    /**
     * @SWG\Property(format="integer")
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     * @Id
     */
    private $id_entidade;

    /**
     * @SWG\Property(format="string")
     * @var string $st_sexo
     * @Column(type="string", length=1, nullable=true, name="st_sexo")
     */
    private $st_sexo;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomeexibicao
     * @Column(type="string", length=255, nullable=true, name="st_nomeexibicao")
     */
    private $st_nomeexibicao;

    /**
     * @SWG\Property(format="date")
     * @var date $dt_nascimento
     * @Column(type="date", nullable=true, name="dt_nascimento")
     */
    private $dt_nascimento;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomepai
     * @Column(type="string", length=255, nullable=true, name="st_nomepai")
     */
    private $st_nomepai;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomemae
     * @Column(type="string", length=100, nullable=true, name="st_nomemae")
     */
    private $st_nomemae;

    /**
     * @SWG\Property(format="string")
     * @var Pais $id_pais
     * @ManyToOne(targetEntity="Pais")
     * @JoinColumn(name="id_pais", nullable=true, referencedColumnName="id_pais")
     */
    private $id_pais;

    /**
     * @SWG\Property(format="string")
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @SWG\Property(format="integer")
     * @var Municipio $id_municipio
     * @ManyToOne(targetEntity="Municipio")
     * @JoinColumn(name="id_municipio", nullable=true, referencedColumnName="id_municipio")
     */
    private $id_municipio;

    /**
     * @SWG\Property(format="datetime2")
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=true, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;

    /**
     * @SWG\Property(format="string")
     * @var integer $st_passaporte
     * @Column(type="string", length=255, nullable=true, name="st_passaporte")
     */
    private $st_passaporte;

    /**
     * @SWG\Property(format="string")
     * @var integer $st_zonaeleitoral
     * @Column(type="string", length=255, nullable=true, name="st_zonaeleitoral")
     */
    private $st_zonaeleitoral;

    /**
     * @SWG\Property(format="string")
     * @var integer $st_secaoeleitoral
     * @Column(type="string", length=255, nullable=true, name="st_secaoeleitoral")
     */
    private $st_secaoeleitoral;

    /**
     * @SWG\Property(format="string")
     * @var integer $st_municipioeleitoral
     * @Column(type="string", length=255, nullable=true, name="st_municipioeleitoral")
     */
    private $st_municipioeleitoral;

    /**
     * @SWG\Property(format="datetime2")
     * @var datetime2 $dt_expedicaocertificadoreservista
     * @Column(type="datetime2", nullable=false, name="dt_expedicaocertificadoreservista")
     */
    private $dt_expedicaocertificadoreservista;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_categoriaservicomilitar
     * @Column(type="integer", nullable=true, name="id_categoriaservicomilitar")
     */
    private $id_categoriaservicomilitar;

    /**
     * @SWG\Property(format="integer")
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo")
     */
    private $bl_ativo;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_alterado
     * @Column(type="boolean", nullable=false, name="bl_alterado")
     */
    private $bl_alterado;

    /**
     * @SWG\Property(format="integer")
     * @var EstadoCivil $id_estadocivil
     * @ManyToOne(targetEntity="EstadoCivil")
     * @JoinColumn(name="id_estadocivil", referencedColumnName="id_estadocivil", nullable=false)
     */
    private $id_estadocivil;

    /**
     * @SWG\Property(format="string")
     * @var text $st_urlavatar
     * @Column(type="text", length=300, nullable=true, name="st_urlavatar")
     */
    private $st_urlavatar;

    /**
     * @SWG\Property(format="string")
     * @var text $st_cidade
     * @Column(type="text", length=300, nullable=true, name="st_cidade")
     */
    private $st_cidade;

    /**
     * @SWG\Property(format="string")
     * @var string $st_identificacao
     * @Column(type="string", length=255, nullable=true, name="st_identificacao")
     */
    private $st_identificacao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_tituloeleitor
     * @Column(name="st_tituloeleitor", type="string", nullable=true, length=30)
     */
    private $st_tituloeleitor;

    /**
     * @SWG\Property(format="string")
     * @var string $st_certificadoreservista
     * @Column(name="st_certificadoreservista", type="string", nullable=true, length=30)
     */
    private $st_certificadoreservista;

    /**
     * @SWG\Property(format="string")
     * @var string $st_reparticaoexpedidora
     * @Column(name="st_reparticaoexpedidora", type="string", nullable=true, length=30)
     */
    private $st_reparticaoexpedidora;

    /**
     * @SWG\Property(format="string")
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_ufnascimento", nullable=true, referencedColumnName="sg_uf")
     */
    private $sg_ufnascimento;

    /**
     * @SWG\Property(format="string")
     * @ManyToOne(targetEntity="Municipio")
     * @JoinColumn(name="id_municipionascimento", nullable=true, referencedColumnName="id_municipio")
     */
    private $id_municipionascimento;


    /**
     * @SWG\Property(format="string")
     * @var string $st_descricaocoordenador
     * @Column(name="st_descricaocoordenador", type="string", nullable=true, length=160)
     */
    private $st_descricaocoordenador;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_sincronizarg1
     * @Column(type="boolean", nullable=false, name="bl_sincronizarg1")
     */
    private $bl_sincronizarg1;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_sincronizarfluxus
     * @Column(type="boolean", nullable=false, name="bl_sincronizarfluxus")
     */
    private $bl_sincronizarfluxus;

    /**
     * @var TipoSanguineo $id_tiposanguineo
     * @ManyToOne(targetEntity="TipoSanguineo")
     * @JoinColumn(name="id_tiposanguineo", nullable=true, referencedColumnName="id_tiposanguineo")
     */
    private $id_tiposanguineo;

    /**
     * @SWG\Property(format="string")
     * @var string $sg_fatorrh
     * @Column(name="sg_fatorrh", type="string", nullable=true, length=1)
     */
    private $sg_fatorrh;

    /**
     * @return boolean
     */
    public function getBl_alterado()
    {
        return $this->bl_alterado;
    }

    /**
     * @param bool $bl_alterado
     * @return $this
     */
    public function setBl_alterado($bl_alterado)
    {
        $this->bl_alterado = $bl_alterado;
        return $this;
    }


    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade Reference of Entidade
     * @return \G2\Entity\Pessoa
     */
    public function setId_entidade(Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_sexo()
    {
        return $this->st_sexo;
    }

    public function setSt_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    public function getSt_nomeexibicao()
    {
        return $this->st_nomeexibicao;
    }

    public function setSt_nomeexibicao($st_nomeexibicao)
    {
        $this->st_nomeexibicao = $st_nomeexibicao;
        return $this;
    }

    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    public function getId_pais()
    {
        return $this->id_pais;
    }

    /**
     * @param \G2\Entity\Pais $id_pais Reference of Pais
     * @return \G2\Entity\Pessoa
     */
    public function setId_pais(Pais $id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getId_municipio()
    {
        return $this->id_municipio;
    }

    /**
     * @param \G2\Entity\Municipio $id_municipio Reference of Municipio
     * @return \G2\Entity\Pessoa
     */
    public function setId_municipio(Municipio $id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getSt_passaporte()
    {
        return $this->st_passaporte;
    }

    public function setSt_passaporte($st_passaporte)
    {
        $this->st_passaporte = $st_passaporte;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao Reference of Situacao
     * @return \G2\Entity\Pessoa
     */
    public function setId_situacao(Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_estadocivil()
    {
        return $this->id_estadocivil;
    }

    public function setId_estadocivil($id_estadocivil)
    {
        $this->id_estadocivil = $id_estadocivil;
        return $this;
    }

    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
        return $this;
    }

    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }


    /**
     * @return string
     */
    public function getSt_certificadoreservista()
    {
        return $this->st_certificadoreservista;
    }

    /**
     * @param string $st_certificadoreservista
     */
    public function setSt_certificadoreservista($st_certificadoreservista)
    {
        $this->st_certificadoreservista = $st_certificadoreservista;
    }

    /**
     * @return char
     */
    public function getSg_ufnascimento()
    {
        return $this->sg_ufnascimento;
    }

    /**
     * @param char $sg_ufnascimento
     */
    public function setSg_ufnascimento($sg_ufnascimento)
    {
        $this->sg_ufnascimento = $sg_ufnascimento;
    }

    /**
     * @return decimal
     */
    public function getId_municipionascimento()
    {
        return $this->id_municipionascimento;
    }

    /**
     * @param \G2\Entity\Municipio $id_municipio Reference of Municipio
     * @return \G2\Entity\Pessoa
     */
    public function setId_municipionascimento($id_municipionascimento)
    {
        $this->id_municipionascimento = $id_municipionascimento;
    }

    /**
     * @return string
     */
    public function getSt_tituloeleitor()
    {
        return $this->st_tituloeleitor;
    }

    /**
     * @param string $st_tituloeleitor
     */
    public function setSt_tituloeleitor($st_tituloeleitor)
    {
        $this->st_tituloeleitor = $st_tituloeleitor;
    }

    /**
     * @return string
     */
    public function getSt_descricaocoordenador()
    {
        return $this->st_descricaocoordenador;
    }

    /**
     * @param string $st_descricaocoordenador
     */
    public function setSt_descricaocoordenador($st_descricaocoordenador)
    {
        $this->st_descricaocoordenador = $st_descricaocoordenador;
    }

    /**
     * @return mixed
     */
    public function getBl_sincronizarg1()
    {
        return $this->bl_sincronizarg1;
    }

    /**
     * @param mixed $bl_sincronizarg1
     */
    public function setBl_sincronizarg1($bl_sincronizarg1)
    {
        $this->bl_sincronizarg1 = $bl_sincronizarg1;
    }

    /**
     * @return boolean
     */
    public function getBl_sincronizarfluxus()
    {
        return $this->bl_sincronizarfluxus;
    }

    /**
     * @param boolean $bl_sincronizarfluxus
     */
    public function setBl_sincronizarfluxus($bl_sincronizarfluxus)
    {
        $this->bl_sincronizarfluxus = $bl_sincronizarfluxus;
    }

    /**
     * @return int
     */
    public function getSt_zonaeleitoral()
    {
        return $this->st_zonaeleitoral;
    }

    /**
     * @param int $st_zonaeleitoral
     */
    public function setSt_zonaeleitoral($st_zonaeleitoral)
    {
        $this->st_zonaeleitoral = $st_zonaeleitoral;
    }

    /**
     * @return int
     */
    public function getSt_secaoeleitoral()
    {
        return $this->st_secaoeleitoral;
    }

    /**
     * @param int $st_secaoeleitoral
     */
    public function setSt_secaoeleitoral($st_secaoeleitoral)
    {
        $this->st_secaoeleitoral = $st_secaoeleitoral;
    }

    /**
     * @return int
     */
    public function getSt_municipioeleitoral()
    {
        return $this->st_municipioeleitoral;
    }

    /**
     * @param int $st_municipioeleitoral
     */
    public function setSt_municipioeleitoral($st_municipioeleitoral)
    {
        $this->st_municipioeleitoral = $st_municipioeleitoral;
    }

    /**
     * @return datetime2
     */
    public function getDt_expedicaocertificadoreservista()
    {
        return $this->dt_expedicaocertificadoreservista;
    }

    /**
     * @param datetime2 $dt_expedicaocertificadoreservista
     */
    public function setDt_expedicaocertificadoreservista($dt_expedicaocertificadoreservista)
    {
        $this->dt_expedicaocertificadoreservista = $dt_expedicaocertificadoreservista;
    }

    /**
     * @return int
     */
    public function getId_categoriaservicomilitar()
    {
        return $this->id_categoriaservicomilitar;
    }

    /**
     * @param int $id_categoriaservicomilitar
     */
    public function setId_categoriaservicomilitar($id_categoriaservicomilitar)
    {
        $this->id_categoriaservicomilitar = $id_categoriaservicomilitar;
    }

    /**
     * @return mixed
     */
    public function getSt_reparticaoexpedidora()
    {
        return $this->st_reparticaoexpedidora;
    }

    /**
     * @param mixed $st_reparticaoexpedidora
     */
    public function setSt_reparticaoexpedidora($st_reparticaoexpedidora)
    {
        $this->st_reparticaoexpedidora = $st_reparticaoexpedidora;
    }

    /**
     * @return TipoSanguineo
     */
    public function getId_tiposanguineo()
    {
        return $this->id_tiposanguineo;
    }

    /**
     * @param TipoSanguineo $id_tiposanguineo
     * @return $this
     */
    public function setId_tiposanguineo($id_tiposanguineo)
    {
        $this->id_tiposanguineo = $id_tiposanguineo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSg_fatorrh()
    {
        return $this->sg_fatorrh;
    }

    /**
     * @param string $sg_fatorrh
     * @return $this
     */
    public function setSg_fatorrh($sg_fatorrh)
    {
        $this->sg_fatorrh = $sg_fatorrh;
        return $this;
    }

}

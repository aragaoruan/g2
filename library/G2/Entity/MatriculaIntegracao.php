<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_matriculaintegracao")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class MatriculaIntegracao
{

    /**
     * @Id
     * @var integer $id_matriculaintegracao
     * @Column(name="id_matriculaintegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_matriculaintegracao;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula", nullable=false)
     */
    private $id_matricula;

    /**
     * @var integer $id_matriculasistema
     * @Column(name="id_matriculasistema", type="integer", nullable=false)
     */
    private $id_matriculasistema;

    /**
     * @var integer $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=false)
     */
    private $bl_encerrado;

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_matriculaintegracao()
    {
        return $this->id_matriculaintegracao;
    }

    /**
     * @param int $id_matriculaintegracao
     */
    public function setId_matriculaintegracao($id_matriculaintegracao)
    {
        $this->id_matriculaintegracao = $id_matriculaintegracao;
    }

    /**
     * @return int
     */
    public function getId_matriculasistema()
    {
        return $this->id_matriculasistema;
    }

    /**
     * @param int $id_matriculasistema
     */
    public function setId_matriculasistema($id_matriculasistema)
    {
        $this->id_matriculasistema = $id_matriculasistema;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return boolean
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param boolean $bl_encerrado
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
    }
}
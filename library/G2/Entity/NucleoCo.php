<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_nucleoco")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @EntityLog
 */
class NucleoCo extends G2Entity
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false, length=4)
     */
    private $id_nucleoco;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_tipoocorrencia;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_textonotificacao
     * @Column(name="id_textonotificacao", type="integer", nullable=true, length=4)
     */
    private $id_textonotificacao;
    /**
     * @var integer $nu_horasmeta
     * @Column(name="nu_horasmeta", type="integer", nullable=true, length=4)
     */
    private $nu_horasmeta;
    /**
     * @var integer $id_nucleofinalidade
     * @Column(name="id_nucleofinalidade", type="integer", nullable=false, length=4)
     */
    private $id_nucleofinalidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var boolean $bl_notificaatendente
     * @Column(name="bl_notificaatendente", type="boolean", nullable=false, length=1)
     */
    private $bl_notificaatendente;
    /**
     * @var boolean $bl_notificaresponsavel
     * @Column(name="bl_notificaresponsavel", type="boolean", nullable=false, length=1)
     */
    private $bl_notificaresponsavel;
    /**
     * @var string $st_nucleoco
     * @Column(name="st_nucleoco", type="string", nullable=false, length=150)
     */
    private $st_nucleoco;
    /**
     * @var string $st_corinteracao
     * @Column(name="st_corinteracao", type="string", nullable=true, length=10)
     */
    private $st_corinteracao;
    /**
     * @var string $st_cordevolvida
     * @Column(name="st_cordevolvida", type="string", nullable=true, length=10)
     */
    private $st_cordevolvida;
    /**
     * @var string $st_coratrasada
     * @Column(name="st_coratrasada", type="string", nullable=true, length=10)
     */
    private $st_coratrasada;
    /**
     * @var string $st_cordefault
     * @Column(name="st_cordefault", type="string", nullable=true, length=10)
     */
    private $st_cordefault;

    /**
     * @var integer $id_assuntocoresgate
     * @Column(name="id_assuntocoresgate", type="integer", nullable=false, length=4)
     */
    private $id_assuntocoresgate;

    /**
     * @var integer $id_assuntorecuperacao
     * @Column(name="id_assuntorecuperacao", type="integer", nullable=false, length=4)
     */
    private $id_assuntorecuperacao;

    /**
     * @var integer $id_assuntorenovacao
     * @Column(name="id_assuntorenovacao", type="integer", nullable=false, length=4)
     */
    private $id_assuntorenovacao;

    /**
     * @var Holding
     * @ManyToOne(targetEntity="Holding")
     * @JoinColumn(name="id_holdingcompartilhamento", referencedColumnName="id_entidade")
     * */
    private $id_holdingcompartilhamento;

    /**
     * @var integer $id_assuntocancelamento
     * @Column(name="id_assuntocancelamento", type="integer", nullable=true)
     */
    private $id_assuntocancelamento;

    /**
     * @return int
     */
    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param int $id_tipoocorrencia
     */
    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getId_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    /**
     * @param int $id_textonotificacao
     */
    public function setId_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
    }

    /**
     * @return int
     */
    public function getNu_horasmeta()
    {
        return $this->nu_horasmeta;
    }

    /**
     * @param int $nu_horasmeta
     */
    public function setNu_horasmeta($nu_horasmeta)
    {
        $this->nu_horasmeta = $nu_horasmeta;
    }

    /**
     * @return int
     */
    public function getId_nucleofinalidade()
    {
        return $this->id_nucleofinalidade;
    }

    /**
     * @param int $id_nucleofinalidade
     */
    public function setId_nucleofinalidade($id_nucleofinalidade)
    {
        $this->id_nucleofinalidade = $id_nucleofinalidade;
    }

    /**
     * @return boolean
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getbl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param boolean $bl_notificaatendente
     */
    public function setBl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
    }

    /**
     * @return boolean
     */
    public function getbl_notificaresponsavel()
    {
        return $this->bl_notificaresponsavel;
    }

    /**
     * @param boolean $bl_notificaresponsavel
     */
    public function setBl_notificaresponsavel($bl_notificaresponsavel)
    {
        $this->bl_notificaresponsavel = $bl_notificaresponsavel;
    }

    /**
     * @return string
     */
    public function getSt_nucleoco()
    {
        return $this->st_nucleoco;
    }

    /**
     * @param string $st_nucleoco
     */
    public function setSt_nucleoco($st_nucleoco)
    {
        $this->st_nucleoco = $st_nucleoco;
    }

    /**
     * @return string
     */
    public function getSt_corinteracao()
    {
        return $this->st_corinteracao;
    }

    /**
     * @param string $st_corinteracao
     */
    public function setSt_corinteracao($st_corinteracao)
    {
        $this->st_corinteracao = $st_corinteracao;
    }

    /**
     * @return string
     */
    public function getSt_cordevolvida()
    {
        return $this->st_cordevolvida;
    }

    /**
     * @param string $st_cordevolvida
     */
    public function setSt_cordevolvida($st_cordevolvida)
    {
        $this->st_cordevolvida = $st_cordevolvida;
    }

    /**
     * @return string
     */
    public function getSt_coratrasada()
    {
        return $this->st_coratrasada;
    }

    /**
     * @param string $st_coratrasada
     */
    public function setSt_coratrasada($st_coratrasada)
    {
        $this->st_coratrasada = $st_coratrasada;
    }

    /**
     * @return string
     */
    public function getSt_cordefault()
    {
        return $this->st_cordefault;
    }

    /**
     * @param string $st_cordefault
     */
    public function setSt_cordefault($st_cordefault)
    {
        $this->st_cordefault = $st_cordefault;
    }

    /**
     * @param int $id_assuntocoresgate
     * @return NucleoCo
     */
    public function setId_assuntocoresgate($id_assuntocoresgate)
    {
        $this->id_assuntocoresgate = $id_assuntocoresgate;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_assuntocoresgate()
    {
        return $this->id_assuntocoresgate;
    }

    /**
     * @param int $id_assuntorecuperacao
     * @return NucleoCo
     */
    public function setId_assuntorecuperacao($id_assuntorecuperacao)
    {
        $this->id_assuntorecuperacao = $id_assuntorecuperacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_assuntorecuperacao()
    {
        return $this->id_assuntorecuperacao;
    }

    /**
     * @param int $id_assuntorenovacao
     * @return NucleoCo
     */
    public function setId_assuntorenovacao($id_assuntorenovacao)
    {
        $this->id_assuntorenovacao = $id_assuntorenovacao;
        return $this;
    }
    /**
     * @return int
     */
    public function getid_assuntocancelamento()
    {
        return $this->id_assuntocancelamento;
    }

    /**
     * @param int $id_assuntocancelamento
     * @return NucleoCo
     */
    public function setid_assuntocancelamento($id_assuntocancelamento)
    {
        $this->id_assuntocancelamento = $id_assuntocancelamento;
        return $this;
    }


    /**
     * @return int
     */
    public function getId_assuntorenovacao()
    {
        return $this->id_assuntorenovacao;
    }

    /**
     * @return Holding
     */
    public function getId_holdingcompartilhamento()
    {
        return $this->id_holdingcompartilhamento;
    }

    /**
     * @param $id_holdingcompartilhamento
     * @return $this
     */
    public function setId_holdingcompartilhamento($id_holdingcompartilhamento)
    {
        $this->id_holdingcompartilhamento = $id_holdingcompartilhamento;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeendereco")
 * @Entity
 * @EntityLog
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadedeEndereco
{
    /**
     *
     * @var integer $id_entidadeendereco
     * @Column(name="id_entidadeendereco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidadeendereco;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var Endereco $id_endereco
     * @ManyToOne(targetEntity="Endereco")
     * @JoinColumn(name="id_endereco", nullable=false, referencedColumnName="id_endereco")
     */
    private $id_endereco;

    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false)
     */
    private $bl_padrao;

    /**
     * @return int
     */
    public function getId_entidadeendereco()
    {
        return $this->id_entidadeendereco;
    }

    /**
     * @param $id_entidadeendereco
     * @return $this
     */
    public function setId_entidadeendereco($id_entidadeendereco)
    {
        $this->id_entidadeendereco = $id_entidadeendereco;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return Endereco
     */
    public function getId_endereco()
    {
        return $this->id_endereco;
    }


    /**
     * @param $id_endereco
     * @return $this
     */
    public function setId_endereco($id_endereco)
    {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_padrao()
    {
        return $this->bl_padrao;
    }

    /**
     * @param $bl_padrao
     * @return $this
     */
    public function setBl_padrao($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

}
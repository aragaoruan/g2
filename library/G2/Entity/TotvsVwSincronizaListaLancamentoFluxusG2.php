<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 18/11/13
 * Time: 10:41
 */

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="totvs.vw_sincroniza_listalancamento_fluxus_g2")
 */
class TotvsVwSincronizaListaLancamentoFluxusG2 {

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     * @Id
     */
    private $id_venda;

    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer")
     * @Id
     */
    private $id_lancamento;

    /**
     * @var string $st_codlancamento
     * @Column(name="st_codlancamento", type="string")
     */
    private $st_codlancamento;

    /**
     * @param int $id_lancamento
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return int
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param mixed $st_codlancamento
     */
    public function setStCodlancamento($st_codlancamento)
    {
        $this->st_codlancamento = $st_codlancamento;
    }

    /**
     * @return mixed
     */
    public function getStCodlancamento()
    {
        return $this->st_codlancamento;
    }


} 
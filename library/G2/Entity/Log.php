<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Description of Log
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_log")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class Log extends G2Entity
{

    /**
     * @var integer $id_log
     * @Column(name="id_log", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_log;

    /**
     * @var \G2\Entity\Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var \G2\Entity\Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var \G2\Entity\Perfil $id_perfil
     * @ManyToOne(targetEntity="Perfil")
     * @JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     */
    private $id_perfil;

    /**
     * @var \G2\Entity\Funcionalidade $id_funcionalidade
     * @ManyToOne(targetEntity="Funcionalidade")
     * @JoinColumn(name="id_funcionalidade", referencedColumnName="id_funcionalidade")
     */
    private $id_funcionalidade;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string $st_antes
     * @Column(name="st_antes", type="string", nullable=false)
     */
    private $st_antes;

    /**
     * @var string $st_depois
     * @Column(name="st_depois", type="string", nullable=false)
     */
    private $st_depois;

    /**
     * @var string $st_tabela
     * @Column(name="st_tabela", type="string", nullable=false)
     */
    private $st_tabela;

    /**
     * @var string $st_motivo
     * @Column(name="st_motivo", type="string", nullable=false)
     */
    private $st_motivo;

    /**
     * @var string $id_tabela
     * @Column(name="id_tabela", type="string", nullable=false)
     */
    private $id_tabela;

    /**
     * @var string $st_coluna
     * @Column(name="st_coluna", type="string", nullable=true)
     */
    private $st_coluna;

    /**
     * @var \G2\Entity\OperacaoLog $id_operacao
     * @ManyToOne(targetEntity="OperacaoLog")
     * @JoinColumn(name="id_operacao", referencedColumnName="id_operacaolog")
     */
    private $id_operacao;

    /**
     * @return int
     */
    public function getid_tabela()
    {
        return $this->id_tabela;
    }

    /**
     * @param varchar $id_tabela
     * @return $this
     */
    public function setid_tabela($id_tabela)
    {
        $this->id_tabela = $id_tabela;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_log()
    {
        return $this->id_log;
    }

    /**
     * @param int $id_log
     * @return $this
     */
    public function setid_log($id_log)
    {
        $this->id_log = $id_log;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     * @return $this
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return Perfil
     */
    public function getid_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param Perfil $id_perfil
     * @return $this
     */
    public function setid_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return Funcionalidade
     */
    public function getid_funcionalidade()
    {
        return $this->id_funcionalidade;
    }

    /**
     * @param Funcionalidade $id_funcionalidade
     * @return $this
     */
    public function setid_funcionalidade($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_antes()
    {
        return $this->st_antes;
    }

    /**
     * @param string $st_antes
     * @return $this
     */
    public function setst_antes($st_antes)
    {
        $this->st_antes = $st_antes;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_depois()
    {
        return $this->st_depois;
    }

    /**
     * @param string $st_depois
     * @return $this
     */
    public function setst_depois($st_depois)
    {
        $this->st_depois = $st_depois;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tabela()
    {
        return $this->st_tabela;
    }

    /**
     * @param $st_tabela
     * @return $this
     */
    public function setst_tabela($st_tabela)
    {
        $this->st_tabela = $st_tabela;
        return $this;
    }

    public function getst_motivo()
    {
        return $this->st_motivo;
    }

    /**
     * @param string $st_motivo
     * @return $this
     */
    public function setst_motivo($st_motivo)
    {
        $this->st_motivo = $st_motivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_coluna()
    {
        return $this->st_coluna;
    }

    /**
     * @return OperacaoLog
     */
    public function getid_operacao()
    {
        return $this->id_operacao;
    }

    /**
     * @param $st_coluna
     * @return $this
     */
    public function setst_coluna($st_coluna)
    {
        $this->st_coluna = $st_coluna;
        return $this;
    }

    /**
     * @param OperacaoLog $id_operacao
     * @return $this
     */
    public function setid_operacao(\G2\Entity\OperacaoLog $id_operacao)
    {
        $this->id_operacao = $id_operacao;
        return $this;
    }

}

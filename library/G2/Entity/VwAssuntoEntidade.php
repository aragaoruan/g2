<?php

namespace G2\Entity;

/**
 * Classe Entity para VwAssuntoEntide
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_assuntoentidade")
 * @Entity
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwAssuntoEntidade {

    /**
     * @Id
     * @var integer $id_assuntopai
     * @Column(name="id_assuntopai", type="integer", nullable=true, length=4)
     */
    private $id_assuntopai;

    /**
     * @var integer $id_subassunto
     * @Column(name="id_subassunto", type="integer", nullable=true, length=4)
     */
    private $id_subassunto;

    /**
     * @Id
     * @var integer $id_assunto
     * @Column(name="id_assunto", type="integer", nullable=false, length=4)
     */
    private $id_assunto;

    /**
     * @var integer $id_assuntoentidadeco
     * @Column(name="id_assuntoentidadeco", type="integer", nullable=true, length=4)
     */
    private $id_assuntoentidadeco;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false, length=4)
     */
    private $id_entidadecadastro;

    /**
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_tipoocorrencia;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_assuntopai
     * @Column(name="st_assuntopai", type="string", nullable=false, length=200)
     */
    private $st_assuntopai;

    /**
     * @var string $st_subassunto
     * @Column(name="st_subassunto", type="string", nullable=true, length=200)
     */
    private $st_subassunto;

    public function getId_assuntopai()
    {
        return $this->id_assuntopai;
    }

    public function getId_subassunto()
    {
        return $this->id_subassunto;
    }

    public function getId_assunto()
    {
        return $this->id_assunto;
    }

    public function getId_assuntoentidadeco()
    {
        return $this->id_assuntoentidadeco;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function getSt_assuntopai()
    {
        return $this->st_assuntopai;
    }

    public function getSt_subassunto()
    {
        return $this->st_subassunto;
    }

    public function setId_assuntopai($id_assuntopai)
    {
        $this->id_assuntopai = $id_assuntopai;
        return $this;
    }

    public function setId_subassunto($id_subassunto)
    {
        $this->id_subassunto = $id_subassunto;
        return $this;
    }

    public function setId_assunto($id_assunto)
    {
        $this->id_assunto = $id_assunto;
        return $this;
    }

    public function setId_assuntoentidadeco($id_assuntoentidadeco)
    {
        $this->id_assuntoentidadeco = $id_assuntoentidadeco;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function setSt_assuntopai($st_assuntopai)
    {
        $this->st_assuntopai = $st_assuntopai;
        return $this;
    }

    public function setSt_subassunto($st_subassunto)
    {
        $this->st_subassunto = $st_subassunto;
        return $this;
    }

}

<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipoaula")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoAula extends G2Entity
{

    /**
     * @Id
     * @var integer $id_tipoaula
     * @Column(name="id_tipoaula", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoaula;

    /**
     * @var string $st_tipoaula
     * @Column(name="st_tipoaula", type="string", nullable=false, length=255)
     */
    private $st_tipoaula;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    /**
     * @return int
     */
    public function getId_tipoaula()
    {
        return $this->id_tipoaula;
    }

    /**
     * @param int $id_tipoaula
     */
    public function setId_tipoaula($id_tipoaula)
    {
        $this->id_tipoaula = $id_tipoaula;
    }

    /**
     * @return string
     */
    public function getSt_tipoaula()
    {
        return $this->st_tipoaula;
    }

    /**
     * @param string $st_tipoaula
     */
    public function setSt_tipoaula($st_tipoaula)
    {
        $this->st_tipoaula = $st_tipoaula;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }




}

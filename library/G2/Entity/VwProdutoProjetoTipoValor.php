<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtoprojetotipovalor")
 * @Entity
 * @EntityView
 * @author Denise XAvier <denise.xavier@unyleya.com.br>
 */
class VwProdutoProjetoTipoValor
{

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false, name="id_produto")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var integer $id_situacao
     * @Column(type="integer", nullable=false, name="id_situacao")
     */
    private $id_situacao;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadematriz
     * @Column(type="integer", nullable=false, name="id_entidadematriz")
     */
    private $id_entidadematriz;

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false, name="id_projetopedagogico")
     */
    private $id_projetopedagogico;


    /**
     * @var integer $id_produtovalor
     * @Column(type="integer", nullable=false, name="id_produtovalor")
     */
    private $id_produtovalor;


    /**
     * @var integer $id_tipoprodutovalor
     * @Column(type="integer", nullable=false, name="id_tipoprodutovalor")
     */
    private $id_tipoprodutovalor;


    /**
     * @var integer $id_nivelensino
     * @Column(type="integer", nullable=false, name="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     * @var integer $id_areaconhecimento
     * @Column(type="integer", nullable=false, name="id_areaconhecimento")
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_contratoregr
     * @Column(type="integer", nullable=false, name="id_contratoregra")
     */
    private $id_contratoregra;

    /**
     * @var string $st_produto
     * @Column(type="string", length=250, nullable=false, name="st_produto")
     */
    private $st_produto;

    /**
     * @var string $st_situacao
     * @Column(type="string", length=255, nullable=false, name="st_situacao")
     */
    private $st_situacao;

    /**
     * @var string $st_descricaosituacao
     * @Column(type="string", length=200, nullable=false, name="st_descricaosituacao")
     */
    private $st_descricaosituacao;


    /**
     * @var string $st_nomeentidade
     * @Column(type="string", length=150, nullable=true, name="st_nomeentidade")
     */
    private $st_nomeentidade;


    /**
     * @var string $st_tituloexibicao
     * @Column(type="string", length=255, nullable=true, name="st_tituloexibicao")
     */
    private $st_tituloexibicao;

    /**
     * @var string $st_tipoprodutovalor;
     * @Column(type="string", length=250, nullable=false, name="st_tipoprodutovalor")
     */
    private $st_tipoprodutovalor;


    /**
     * @var string $st_nivelensino;
     * @Column(type="string", length=255, nullable=true, name="st_nivelensino")
     */
    private $st_nivelensino;


    /**
     * @var string $st_projetopedagogico;
     * @Column(type="string", length=255, nullable=false, name="st_projetopedagogico")
     */
    private $st_projetopedagogico;



    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false)
     */
    private $bl_todasformas;

    /**
     * @var boolean $bl_todascampanhas
     * @Column(name="bl_todascampanhas", type="boolean", nullable=false)
     */
    private $bl_todascampanhas;

    /**
     * @var datetime2 $dt_inicio
     * @Column(name="dt_inicio", type="datetime2", nullable=false)
     */
    private $dt_inicio;


    /**
     * @var datetime2 $dt_termino
     * @Column(name="dt_termino", type="datetime2", nullable=true)
     */
    private $dt_termino;


    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="integer", nullable=false)
     */
    private $nu_valor;

    /**
     * @var integer $nu_valormensal
     * @Column(name="nu_valormensal", type="integer", nullable=true)
     */
    private $nu_valormensal;


    /**
     * @var integer $nu_basepropor
     * @Column(name="nu_basepropor", type="integer", nullable=true)
     */
    private $nu_basepropor;

    /**
     * @var boolean $bl_turma
     * @Column(name="bl_turma", type="boolean", nullable=false)
     */
    private $bl_turma;


    /**
     * @var string $st_areaconhecimento;
     * @Column(type="string", length=255, nullable=true, name="st_areaconhecimento")
     */
    private $st_areaconhecimento;




    public function getBl_turma() {
        return $this->bl_turma;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_entidadematriz() {
        return $this->id_entidadematriz;
    }


    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_produtovalor() {
        return $this->id_produtovalor;
    }

    public function getId_tipoprodutovalor() {
        return $this->id_tipoprodutovalor;
    }

    public function getId_nivelensino() {
        return $this->id_nivelensino;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getSt_produto() {
        return $this->st_produto;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function getSt_descricaosituacao() {
        return $this->st_descricaosituacao;
    }

    public function getSt_nomeentidade() {
        return $this->st_nomeentidade;
    }

    public function getSt_tituloexibicao() {
        return $this->st_tituloexibicao;
    }

    public function getSt_tipoprodutovalor() {
        return $this->st_tipoprodutovalor;
    }

    public function getSt_nivelensino() {
        return $this->st_nivelensino;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getBl_todasformas() {
        return $this->bl_todasformas;
    }

    public function getBl_todascampanhas() {
        return $this->bl_todascampanhas;
    }

    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function getDt_termino() {
        return $this->dt_termino;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function getNu_valormensal() {
        return $this->nu_valormensal;
    }

    public function getNu_basepropor() {
        return $this->nu_basepropor;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_entidadematriz($id_entidadematriz) {
        $this->id_entidadematriz = $id_entidadematriz;
    }


    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_produtovalor($id_produtovalor) {
        $this->id_produtovalor = $id_produtovalor;
    }

    public function setId_tipoprodutovalor($id_tipoprodutovalor) {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
    }

    public function setId_nivelensino($id_nivelensino) {
        $this->id_nivelensino = $id_nivelensino;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setSt_produto($st_produto) {
        $this->st_produto = $st_produto;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    public function setSt_descricaosituacao($st_descricaosituacao) {
        $this->st_descricaosituacao = $st_descricaosituacao;
    }

    public function setSt_nomeentidade($st_nomeentidade) {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    public function setSt_tituloexibicao($st_tituloexibicao) {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    public function setSt_tipoprodutovalor($st_tipoprodutovalor) {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
    }

    public function setSt_nivelensino($st_nivelensino) {
        $this->st_nivelensino = $st_nivelensino;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setBl_todasformas($bl_todasformas) {
        $this->bl_todasformas = $bl_todasformas;
    }

    public function setBl_todascampanhas($bl_todascampanhas) {
        $this->bl_todascampanhas = $bl_todascampanhas;
    }

    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_termino($dt_termino) {
        $this->dt_termino = $dt_termino;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function setNu_valormensal($nu_valormensal) {
        $this->nu_valormensal = $nu_valormensal;
    }

    public function setNu_basepropor($nu_basepropor) {
        $this->nu_basepropor = $nu_basepropor;
    }
    public function getId_contratoregra() {
        return $this->id_contratoregra;
    }

    public function setId_contratoregra($id_contratoregra) {
        $this->id_contratoregra = $id_contratoregra;
    }

    public function setBl_turma($bl_turma) {
        $this->bl_turma = $bl_turma;
    }

    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }




    /**
     * @return array Array for attributes of the entity
     */
    public function _toArray ()
    {
        return array(
            'id_produto' => $this->id_produto,
            'st_produto' => $this->st_produto,
            'id_situacao' => $this->id_situacao,
            'st_situacao' => $this->st_situacao,
            'st_descricaosituacao' => $this->st_descricaosituacao,
            'bl_ativo'  => $this->bl_ativo,
            'bl_todasformas' => $this->bl_todasformas,
            'bl_todascampanhas' => $this->bl_todascampanhas,
            'id_entidade' => $this->id_entidade,
            'id_entidadematriz' => $this->id_entidadematriz,
            'st_projetopedagogico' => $this->st_projetopedagogico,
            'id_contratoregra' => $this->id_contratoregra,
            'bl_turma' => $this->bl_turma,
            'st_nomeentidade' => $this->st_nomeentidade,
            'id_projetopedagogico' => $this->id_projetopedagogico,
            'st_tituloexibicao' => $this->st_tituloexibicao,
            'id_produtovalor' => $this->id_produtovalor,
            'dt_inicio' => $this->dt_inicio,
            'dt_termino' => $this->dt_termino,
            'id_tipoprodutovalor' => $this->id_tipoprodutovalor,
            'st_tipoprodutovalor' => $this->st_tipoprodutovalor,
            'nu_valor' => $this->nu_valor,
            'nu_valormensal' => $this->nu_valormensal,
            'nu_basepropor' => $this->nu_basepropor,
            'id_nivelensino' => $this->id_nivelensino,
            'st_nivelensino' => $this->st_nivelensino,
            'id_areaconhecimento' =>$this->id_areaconhecimento,
            'st_areaconhecimento' => $this->st_areaconhecimento

        );
    }

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_disciplinaintegracao")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @EntityLog
 */
class DisciplinaIntegracao extends G2Entity
{

    /**
     * @var integer $id_disciplinaintegracao
     * @Column(type="integer", nullable=false, name="id_disciplinaintegracao")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplinaintegracao;

    /**
     * @var string $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema", nullable=false)
     */
    private $id_sistema;


    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=false)
     */
    private $id_usuariocadastro;


    /**
     * @var string $st_codsistema
     * @Column(type="string",length=255, nullable=true, name="st_codsistema")
     */
    private $st_codsistema;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean",nullable=false, name="bl_ativo")
     */
    private $bl_ativo;

    /**
     * @var string $st_salareferencia
     * @Column(type="string",length=255, nullable=true, name="st_salareferencia")
     */
    private $st_salareferencia;

    /**
     * @var EntidadeIntegracao $id_entidade
     * @ManyToOne(targetEntity="EntidadeIntegracao")
     * @JoinColumn(name="id_entidadeintegracao", referencedColumnName="id_entidadeintegracao", nullable=true)
     */
    private $id_entidadeintegracao;

    /**
     * @return EntidadeIntegracao
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param EntidadeIntegracao $id_entidadeintegracao
     * @return DisciplinaIntegracao
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    /**
     * @param int $id_disciplinaintegracao
     */
    public function setId_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
    }

    /**
     * @return string
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param string $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return Sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param Sistema $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return string
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return string
     */
    public function getSt_salareferencia()
    {
        return $this->st_salareferencia;
    }

    /**
     * @param string $st_salareferencia
     */
    public function setSt_salareferencia($st_salareferencia)
    {
        $this->st_salareferencia = $st_salareferencia;
    }


}

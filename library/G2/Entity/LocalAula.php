<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_localaula")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @EntityLog
 */
class LocalAula
{

    /**
     *
     * @var integer $id_localaula
     * @Column(name="id_localaula", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_localaula;

    /**
     * @Column(type="string",length=30,nullable=false)
     * @var string
     */
    private $st_localaula;

    /**
     * @Column(type="datetime2", nullable=false)
     * @var datetime2
     */
    private $dt_cadastro;

    /**
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     * @var @id_usuariocadastro
     */
    private $id_usuariocadastro;

    /**
     * @Column(name="id_situacao", type="integer", nullable=false)
     * @var @id_situacao
     */
    private $id_situacao;

    /**
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @var @id_entidade
     */
    private $id_entidade;

    /**
     * @ManyToOne(targetEntity="Catraca")
     * @JoinColumn(name="id_catraca", referencedColumnName="id_catraca")
     * @var @id_catraca
     */
    private $id_catraca;

    /**
     * @Column(name="nu_maxalunos", type="integer", nullable=false)
     * @var @nu_maxalunos
     */
    private $nu_maxalunos;

    /**
     * @return int
     * @deprecated
     */
    public function getId_localaula()
    {
        return $this->id_localaula;
    }

    /**
     * @param int $id_localaula
     * @return LocalAula
     * @deprecated
     */
    public function setId_localaula($id_localaula)
    {
        $this->id_localaula = $id_localaula;
        return $this;
    }


    public function getId()
    {
        return $this->id_localaula;
    }

    public function setId($id)
    {
        $this->id_localaula = $id;
    }

    public function getSt_localaula()
    {
        return $this->st_localaula;
    }

    public function setSt_localaula($st_localaula)
    {
        $this->st_localaula = $st_localaula;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_catraca()
    {
        return $this->id_catraca;
    }

    public function setId_catraca($id_catraca)
    {
        $this->id_catraca = $id_catraca;
    }

    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_aplicador")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAplicador
{

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_aplicadorprova;

    /**
     *
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=false, length=200)
     */
    private $st_aplicadorprova;



    /**
     * @var Entidade $id_entidadecadastro
     * @Column(name="id_entidadecadastro", nullable=false, type="integer")
     */
    private $id_entidadecadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;


    /**
     *
     * @var integer $id_entidadeaplicador
     * @Column(name="id_entidadeaplicador", type="integer", nullable=false)
     */
    private $id_entidadeaplicador;


    /**
     *
     * @var integer $id_usuarioaplicador
     * @Column(name="id_usuarioaplicador", type="integer", nullable=false)
     */
    private $id_usuarioaplicador;



    /**
     * @var Usario $id_usuariocadastro
     * @Column(name="id_usuariocadastro", nullable=false, type="integer")
     */
    private $id_usuariocadastro;


    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;


    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;


    /**
     *
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidadeaplicador
     */
    public function setId_entidadeaplicador($id_entidadeaplicador)
    {
        $this->id_entidadeaplicador = $id_entidadeaplicador;
    }

    /**
     * @return int
     */
    public function getId_entidadeaplicador()
    {
        return $this->id_entidadeaplicador;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidadecadastro
     */
    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param int $id_usuarioaplicador
     */
    public function setId_usuarioaplicador($id_usuarioaplicador)
    {
        $this->id_usuarioaplicador = $id_usuarioaplicador;
    }

    /**
     * @return int
     */
    public function getId_usuarioaplicador()
    {
        return $this->id_usuarioaplicador;
    }

    /**
     * @param \G2\Entity\Usario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return \G2\Entity\Usario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }




}
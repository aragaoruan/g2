<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoconjuntoreferencia")
 * @Entity
 * @EntityLog
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvaliacaoConjuntoReferencia
{

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoconjuntoreferencia;
    /**
     * @Column(name="id_avaliacaoconjunto", type="integer")
     * @var integer $id_avaliacaoconjunto
     */
    private $id_avaliacaoconjunto;
    /**
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;
    /**
     * @Column(name="id_saladeaula", type="integer")
     * @var integer $id_saladeaula
     */
    private $id_saladeaula;
    /**
     * @ManyToOne(targetEntity="Modulo")
     * @JoinColumn(name="id_modulo", referencedColumnName="id_modulo")
     */
    private $id_modulo;
    /**
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;
    /**
     * @Column(name="dt_fim", type="date", nullable=true)
     */
    private $dt_fim;

    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getId_avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
    }

    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
    }


}

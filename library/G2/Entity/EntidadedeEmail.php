<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeemail")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadedeEmail
{

    /**
     * @var integer $id_entidadeemail
     * @Column(name="id_entidadeemail", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidadeemail;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var EmailConfig $id_emailconfig
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="EmailConfig")
     * @JoinColumn(name="id_emailconfig", nullable=true, referencedColumnName="id_emailconfig")
     */
    private $id_emailconfig;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=200)
     */
    private $st_email;

    public function getId_entidadeemail ()
    {
        return $this->id_entidadeemail;
    }

    public function setId_entidadeemail ($id_entidadeemail)
    {
        $this->id_entidadeemail = $id_entidadeemail;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_emailconfig ()
    {
        return $this->id_emailconfig;
    }

    public function setId_emailconfig (EmailConfig $id_emailconfig)
    {
        $this->id_emailconfig = $id_emailconfig;
        return $this;
    }

    public function getSt_email ()
    {
        return $this->st_email;
    }

    public function setSt_email ($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_acordopagarme")
 */

class AcordoPagarme {

    /**
     * @var integer $id_acordopagarme
     * @Column(name="id_acordopagarme", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_acordopagarme;

    /**
     * @var integer $id_acordo
     * @Column(name="id_acordo", type="integer", nullable=false)
     */
    private $id_acordo;
    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer", nullable=false)
     */
    private $id_lancamento;
    /**
     * @var integer $id_assinatura
     * @Column(name="id_assinatura", type="integer", nullable=false)
     */
    private $id_assinatura;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @return int
     */
    public function getId_acordopagarme()
    {
        return $this->id_acordopagarme;
    }

    /**
     * @param int $id_acordopagarme
     */
    public function setId_acordopagarme( $id_acordopagarme)
    {
        $this->id_acordopagarme = $id_acordopagarme;
    }

    /**
     * @return int
     */
    public function getId_acordo()
    {
        return $this->id_acordo;
    }

    /**
     * @param int $id_acordo
     */
    public function setId_acordo( $id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @return int
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamento
     */
    public function setId_lancamento( $id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return int
     */
    public function getId_assinatura()
    {
        return $this->id_assinatura;
    }

    /**
     * @param int $id_assinatura
     */
    public function setId_assinatura( $id_assinatura)
    {
        $this->id_assinatura = $id_assinatura;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime $dt_cadastro
     */
    public function setDt_cadastro( $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

}

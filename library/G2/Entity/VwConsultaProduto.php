<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_consultaproduto")
 * @Entity(repositoryClass="\G2\Repository\Produto")
 * @EntityView
 * @author Felipe Pastor
 */
class VwConsultaProduto
{

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produto;
    /**
     *
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=true, length=250)
     */
    private $st_produto;
    /**
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false)
     */
    private $id_tipoproduto;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;
    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;
    /**
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false)
     */
    private $bl_todasformas;
    /**
     * @var boolean $bl_todascampanhas
     * @Column(name="bl_todascampanhas", type="boolean", nullable=false)
     */
    private $bl_todascampanhas;
    /**
     * @var integer $nu_gratuito
     * @Column(name="nu_gratuito", type="integer", nullable=false)
     */
    private $nu_gratuito;
    /**
     * @var integer $id_produtoimagempadrao
     * @Column(name="id_produtoimagempadrao", type="integer", nullable=true)
     */
    private $id_produtoimagempadrao;
    /**
     *
     * @var boolean $bl_unico
     * @Column(name="bl_unico", type="boolean", nullable=false)
     */
    private $bl_unico;
    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false)
     */
    private $st_descricao;
    /**
     *
     * @var string $st_observacoes
     * @Column(name="st_observacoes", type="string", nullable=false)
     */
    private $st_observacoes;
    /**
     *
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=true, length=250)
     */
    private $st_tipoproduto;
    /**
     *
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=false)
     */
    private $id_produtovalor;
    /**
     *
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false)
     */
    private $nu_valor;
    /**
     *
     * @var integer $nu_valorpromocional
     * @Column(name="nu_valorpromocional", type="decimal", nullable=false)
     */
    private $nu_valorpromocional;
    /**
     *
     * @var integer $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=false)
     */
    private $nu_valormensal;
    /**
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=false)
     */
    private $id_tipoprodutovalor;
    /**
     * @var string $st_tipoprodutovalor
     * @Column(name="st_tipoprodutovalor", type="string", nullable=true, length=250)
     */
    private $st_tipoprodutovalor;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;
    /**
     * @var integer $id_planopagamento
     * @Column(name="id_planopagamento", type="integer", nullable=false)
     */
    private $id_planopagamento;
    /**
     * @var string $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=false)
     */
    private $nu_parcelas;
    /**
     * @var string $nu_valorentrada
     * @Column(name="nu_valorentrada", type="decimal", nullable=false)
     */
    private $nu_valorentrada;
    /**
     * @var string $nu_valorparcela
     * @Column(name="nu_valorparcela", type="decimal", nullable=false)
     */
    private $nu_valorparcela;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", nullable=false)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_subtitulo
     * @Column(name="st_subtitulo", nullable=false)
     */
    private $st_subtitulo;
    /**
     * @var string $st_slug
     * @Column(name="st_slug", nullable=false)
     */
    private $st_slug;
    /**
     *
     * @var boolean $bl_destaque
     * @Column(name="bl_destaque", type="boolean", nullable=false)
     */
    private $bl_destaque;
    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer", nullable=false)
     */
    private $id_categoria;
    /**
     * @var integer $id_concurso
     * @Column(name="id_concurso", type="integer", nullable=false)
     */
    private $id_concurso;
    /**
     *
     * @var string $st_concurso
     * @Column(name="st_concurso", type="string", nullable=true, length=250)
     */
    private $st_concurso;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;
    /**
     *
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=true, length=250)
     */
    private $st_evolucao;
    /**
     *
     * @var boolean $bl_mostrarpreco
     * @Column(name="bl_mostrarpreco", type="boolean", nullable=false)
     */
    private $bl_mostrarpreco;

    /**
     * @return the $id_produto
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @return the $st_produto
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @return the $id_tipoproduto
     */
    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $bl_todasformas
     */
    public function getBl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @return the $bl_todascampanhas
     */
    public function getBl_todascampanhas()
    {
        return $this->bl_todascampanhas;
    }

    /**
     * @return the $nu_gratuito
     */
    public function getNu_gratuito()
    {
        return $this->nu_gratuito;
    }

    /**
     * @return the $id_produtoimagempadrao
     */
    public function getId_produtoimagempadrao()
    {
        return $this->id_produtoimagempadrao;
    }

    /**
     * @return the $bl_unico
     */
    public function getBl_unico()
    {
        return $this->bl_unico;
    }

    /**
     * @return the $st_tipoproduto
     */
    public function getSt_tipoproduto()
    {
        return $this->st_tipoproduto;
    }

    /**
     * @return the $id_produtovalor
     */
    public function getId_produtovalor()
    {
        return $this->id_produtovalor;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @return the $nu_valormensal
     */
    public function getNu_valormensal()
    {
        return $this->nu_valormensal;
    }

    /**
     * @return the $id_tipoprodutovalor
     */
    public function getId_tipoprodutovalor()
    {
        return $this->id_tipoprodutovalor;
    }

    /**
     * @return the $st_tipoprodutovalor
     */
    public function getSt_tipoprodutovalor()
    {
        return $this->st_tipoprodutovalor;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @return the $id_planopagamento
     */
    public function getId_planopagamento()
    {
        return $this->id_planopagamento;
    }

    /**
     * @return the $nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @return the $nu_valorentrada
     */
    public function getNu_valorentrada()
    {
        return $this->nu_valorentrada;
    }

    /**
     * @return the $nu_valorparcela
     */
    public function getNu_valorparcela()
    {
        return $this->nu_valorparcela;
    }

    /**
     * @param number $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @param string $st_produto
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @param \G2\Entity\TipoProduto $id_tipoproduto
     */
    public function setId_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param boolean $bl_todasformas
     */
    public function setBl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;
    }

    /**
     * @param boolean $bl_todascampanhas
     */
    public function setBl_todascampanhas($bl_todascampanhas)
    {
        $this->bl_todascampanhas = $bl_todascampanhas;
        return $this;
    }

    /**
     * @param number $nu_gratuito
     */
    public function setNu_gratuito($nu_gratuito)
    {
        $this->nu_gratuito = $nu_gratuito;
        return $this;
    }

    /**
     * @param number $id_produtoimagempadrao
     */
    public function setId_produtoimagempadrao($id_produtoimagempadrao)
    {
        $this->id_produtoimagempadrao = $id_produtoimagempadrao;
        return $this;
    }

    /**
     * @param boolean $bl_unico
     */
    public function setBl_unico($bl_unico)
    {
        $this->bl_unico = $bl_unico;
        return $this;
    }

    /**
     * @param string $st_tipoproduto
     */
    public function setSt_tipoproduto($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    /**
     * @param number $id_produtovalor
     */
    public function setId_produtovalor($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    /**
     * @param string $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @param string $nu_valormensal
     */
    public function setNu_valormensal($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    /**
     * @param number $id_tipoprodutovalor
     */
    public function setId_tipoprodutovalor($id_tipoprodutovalor)
    {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    /**
     * @param string $st_tipoprodutovalor
     */
    public function setSt_tipoprodutovalor($st_tipoprodutovalor)
    {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
        return $this;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @param number $id_planopagamento
     */
    public function setId_planopagamento($id_planopagamento)
    {
        $this->id_planopagamento = $id_planopagamento;
        return $this;
    }

    /**
     * @param string $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @param string $nu_valorentrada
     */
    public function setNu_valorentrada($nu_valorentrada)
    {
        $this->nu_valorentrada = $nu_valorentrada;
        return $this;
    }

    /**
     * @param string $nu_valorparcela
     */
    public function setNu_valorparcela($nu_valorparcela)
    {
        $this->nu_valorparcela = $nu_valorparcela;
        return $this;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @param string $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_observacoes
     */
    public function setSt_observacoes($st_observacoes)
    {
        $this->st_observacoes = $st_observacoes;
    }

    /**
     * @return string
     */
    public function getSt_observacoes()
    {
        return $this->st_observacoes;
    }

    /**
     * @param boolean $bl_destaque
     */
    public function setBl_destaque($bl_destaque)
    {
        $this->bl_destaque = $bl_destaque;
    }

    /**
     * @return boolean
     */
    public function getBl_destaque()
    {
        return $this->bl_destaque;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    /**
     * @return int
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

    /**
     * @param int $id_concurso
     */
    public function setId_concurso($id_concurso)
    {
        $this->id_concurso = $id_concurso;
    }

    /**
     * @return int
     */
    public function getId_concurso()
    {
        return $this->id_concurso;
    }

    /**
     * @param string $st_slug
     */
    public function setSt_slug($st_slug)
    {
        $this->st_slug = $st_slug;
    }

    /**
     * @return string
     */
    public function getSt_slug()
    {
        return $this->st_slug;
    }

    /**
     * @param string $st_subtitulo
     */
    public function setSt_subtitulo($st_subtitulo)
    {
        $this->st_subtitulo = $st_subtitulo;
    }

    /**
     * @return string
     */
    public function getSt_subtitulo()
    {
        return $this->st_subtitulo;
    }

    /**
     * @param string $st_concurso
     */
    public function setSt_concurso($st_concurso)
    {
        $this->st_concurso = $st_concurso;
    }

    /**
     * @return string
     */
    public function getSt_concurso()
    {
        return $this->st_concurso;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @return boolean
     */
    public function getBl_mostrarpreco()
    {
        return $this->bl_mostrarpreco;
    }

    /**
     * @param boolean $bl_mostrarpreco
     */
    public function setBl_mostrarpreco($bl_mostrarpreco)
    {
        $this->bl_mostrarpreco = $bl_mostrarpreco;
    }

    /**
     * @return int
     */
    public function getNu_valorpromocional()
    {
        return $this->nu_valorpromocional;
    }

    /**
     * @param int $nu_valorpromocional
     */
    public function setNu_valorpromocional($nu_valorpromocional)
    {
        $this->nu_valorpromocional = $nu_valorpromocional;
    }

}
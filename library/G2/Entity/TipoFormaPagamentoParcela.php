<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoformapagamentoparcela")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoFormaPagamentoParcela
{

    /**
     * @var integer $id_tipoformapagamentoparcela
     * @Column(name="id_tipoformapagamentoparcela", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoformapagamentoparcela;

    /**
     * @var string $st_tipoformapagamentoparcela
     * @Column(name="st_tipoformapagamentoparcela", type="string", nullable=false, length=255)
     */
    private $st_tipoformapagamentoparcela;

    public function getId_tipoformapagamentoparcela ()
    {
        return $this->id_tipoformapagamentoparcela;
    }

    public function setId_tipoformapagamentoparcela ($id_tipoformapagamentoparcela)
    {
        $this->id_tipoformapagamentoparcela = $id_tipoformapagamentoparcela;
    }

    public function getSt_tipoformapagamentoparcela ()
    {
        return $this->st_tipoformapagamentoparcela;
    }

    public function setSt_tipoformapagamentoparcela ($st_tipoformapagamentoparcela)
    {
        $this->st_tipoformapagamentoparcela = $st_tipoformapagamentoparcela;
    }

}
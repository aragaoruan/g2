<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_operacaorelatorio")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-14
 */
class OperacaoRelatorio {

    /**
     *
     * @var integer $id_operacaorelatorio
     * @Column(name="id_operacaorelatorio", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_operacaorelatorio;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_operacaorelatorio")
     * @var string
     */
    private $st_operacaorelatorio;

    /**
     * @return int
     */
    public function getId_operacaorelatorio()
    {
        return $this->id_operacaorelatorio;
    }

    /**
     * @param int $id_operacaorelatorio
     */
    public function setId_operacaorelatorio($id_operacaorelatorio)
    {
        $this->id_operacaorelatorio = $id_operacaorelatorio;
    }

    /**
     * @return string
     */
    public function getSt_operacaorelatorio()
    {
        return $this->st_operacaorelatorio;
    }

    /**
     * @param string $st_operacaorelatorio
     */
    public function setSt_operacaorelatorio($st_operacaorelatorio)
    {
        $this->st_operacaorelatorio = $st_operacaorelatorio;
    }





}

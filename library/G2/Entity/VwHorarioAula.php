<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_horarioaula")
 * @Entity
 * @EntityView
 * @author Helder silva <helder.silva@unyleya.com.br>
 */
class VwHorarioAula
{

    /**
     * @Id
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_horarioaula;


    /**
     * @var integer $id_codhorarioacesso
     * @Column(name="id_codhorarioacesso", type="integer", nullable=true)
     */
    private $id_codhorarioacesso;

    /**
     *
     * ----- Foi necessário tratar o horario como string, pois o doctrine
     * não aceita o tipo time com a sua precisão.-----
     *
     * @var string $hr_inicio
     * @Column(name="hr_inicio", type="string", nullable=false)
     */
    private $hr_inicio;

    /**
     * @var string $hr_fim
     * @Column(name="hr_fim", type="string", nullable=false)
     */
    private $hr_fim;


    /**
     * @var integer $id_turno
     * @Column(name="id_turno", type="integer", nullable=false, length=4)
     */
    private $id_turno;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $bl_segunda
     * @Column(name="bl_segunda", type="integer", nullable=false, length=4)
     */
    private $bl_segunda;

    /**
     * @var integer $bl_terca
     * @Column(name="bl_terca", type="integer", nullable=false, length=4)
     */
    private $bl_terca;

    /**
     * @var integer $bl_quarta
     * @Column(name="bl_quarta", type="integer", nullable=false, length=4)
     */
    private $bl_quarta;

    /**
     * @var integer $bl_quinta
     * @Column(name="bl_quinta", type="integer", nullable=false, length=4)
     */
    private $bl_quinta;

    /**
     * @var integer $bl_sexta
     * @Column(name="bl_sexta", type="integer", nullable=false, length=4)
     */
    private $bl_sexta;

    /**
     * @var integer $bl_sabado
     * @Column(name="bl_sabado", type="integer", nullable=false, length=4)
     */
    private $bl_sabado;

    /**
     * @var integer $bl_domingo
     * @Column(name="bl_domingo", type="integer", nullable=false, length=4)
     */
    private $bl_domingo;

    /**
     * @var string $st_horarioaula
     * @Column(name="st_horarioaula", type="string", nullable=false, length=255)
     */
    private $st_horarioaula;

    /**
     * @var string $st_codhorarioacesso
     * @Column(name="st_codhorarioacesso", type="string", nullable=true, length=100)
     */
    private $st_codhorarioacesso;

    /**
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=false, length=255)
     */
    private $st_turno;

    /**
     * @var integer $id_tipoaula
     * @Column(name="id_tipoaula", type="integer", nullable=false, length=4)
     */
    private $id_tipoaula;

    /**
     * @var string $st_tipoaula
     * @Column(name="st_tipoaula", type="string", nullable=false, length=255)
     */
    private $st_tipoaula;

    /**
     * @return string
     */
    public function getst_codhorarioacesso()
    {
        return $this->st_codhorarioacesso;
    }

    /**
     * @param string $st_codhorarioacesso
     */
    public function setst_codhorarioacesso($st_codhorarioacesso)
    {
        $this->st_codhorarioacesso = $st_codhorarioacesso;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getid_codhorarioacesso()
    {
        return $this->id_codhorarioacesso;
    }

    /**
     * @param mixed $id_codhorarioacesso
     */
    public function setid_codhorarioacesso($id_codhorarioacesso)
    {
        $this->id_codhorarioacesso = $id_codhorarioacesso;
        return $this;
    }

    /**
     * @return time hr_inicio
     */
    public function getHr_inicio()
    {
        return $this->hr_inicio;
    }

    /**
     * @param hr_inicio
     */
    public function setHr_inicio($hr_inicio)
    {
        $this->hr_inicio = $hr_inicio;
        return $this;
    }

    /**
     * @return time hr_fim
     */
    public function getHr_fim()
    {
        return $this->hr_fim;
    }

    /**
     * @param hr_fim
     */
    public function setHr_fim($hr_fim)
    {
        $this->hr_fim = $hr_fim;
        return $this;
    }

    /**
     * @return integer id_horarioaula
     */
    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    /**
     * @param id_horarioaula
     */
    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
        return $this;
    }

    /**
     * @return integer id_turno
     */
    public function getId_turno()
    {
        return $this->id_turno;
    }

    /**
     * @param id_turno
     */
    public function setId_turno($id_turno)
    {
        $this->id_turno = $id_turno;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer bl_segunda
     */
    public function getBl_segunda()
    {
        return $this->bl_segunda;
    }

    /**
     * @param bl_segunda
     */
    public function setBl_segunda($bl_segunda)
    {
        $this->bl_segunda = $bl_segunda;
        return $this;
    }

    /**
     * @return integer bl_terca
     */
    public function getBl_terca()
    {
        return $this->bl_terca;
    }

    /**
     * @param bl_terca
     */
    public function setBl_terca($bl_terca)
    {
        $this->bl_terca = $bl_terca;
        return $this;
    }

    /**
     * @return integer bl_quarta
     */
    public function getBl_quarta()
    {
        return $this->bl_quarta;
    }

    /**
     * @param bl_quarta
     */
    public function setBl_quarta($bl_quarta)
    {
        $this->bl_quarta = $bl_quarta;
        return $this;
    }

    /**
     * @return integer bl_quinta
     */
    public function getBl_quinta()
    {
        return $this->bl_quinta;
    }

    /**
     * @param bl_quinta
     */
    public function setBl_quinta($bl_quinta)
    {
        $this->bl_quinta = $bl_quinta;
        return $this;
    }

    /**
     * @return integer bl_sexta
     */
    public function getBl_sexta()
    {
        return $this->bl_sexta;
    }

    /**
     * @param bl_sexta
     */
    public function setBl_sexta($bl_sexta)
    {
        $this->bl_sexta = $bl_sexta;
        return $this;
    }

    /**
     * @return integer bl_sabado
     */
    public function getBl_sabado()
    {
        return $this->bl_sabado;
    }

    /**
     * @param bl_sabado
     */
    public function setBl_sabado($bl_sabado)
    {
        $this->bl_sabado = $bl_sabado;
        return $this;
    }

    /**
     * @return integer bl_domingo
     */
    public function getBl_domingo()
    {
        return $this->bl_domingo;
    }

    /**
     * @param bl_domingo
     */
    public function setBl_domingo($bl_domingo)
    {
        $this->bl_domingo = $bl_domingo;
        return $this;
    }

    /**
     * @return string st_horarioaula
     */
    public function getSt_horarioaula()
    {
        return $this->st_horarioaula;
    }

    /**
     * @param st_horarioaula
     */
    public function setSt_horarioaula($st_horarioaula)
    {
        $this->st_horarioaula = $st_horarioaula;
        return $this;
    }

    /**
     * @return string st_turno
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @param st_turno
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipoaula()
    {
        return $this->id_tipoaula;
    }


    /**
     * @return string
     */
    public function getSt_tipoaula()
    {
        return $this->st_tipoaula;
    }

}

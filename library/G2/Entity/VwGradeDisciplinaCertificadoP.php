<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 17/04/2018
 * Time: 13:18
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_gradedisciplinacertificadop")
 * @Entity
 * @EntityView
 */

class VwGradeDisciplinaCertificadoP
{
    /**
     * @var integer $id_certificadoparcial
     * @Column(name="id_certificadoparcial", type="integer", nullable=false)
     */
    private $id_certificadoparcial;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=false)
     */
    private $st_tituloexibicao;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @var string $st_semestre
     * @Column(name="st_semestre", type="string", nullable=false)
     */
    private $st_semestre;

    /**
     * @var decimal $nu_aprovafinal
     * @Column(name="nu_aprovafinal", type="decimal", nullable=true, length=5)
     */
    private $nu_aprovafinal;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false)
     */
    private $st_evolucao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     * @return int
     */
    public function getId_certificadoparcial()
    {
        return $this->id_certificadoparcial;
    }

    /**
     * @param int $id_certificadoparcial
     * @return $this
     */
    public function setId_certificadoparcial($id_certificadoparcial)
    {
        $this->id_certificadoparcial = $id_certificadoparcial;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param string $st_tituloexibicao
     * @return $this
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_semestre()
    {
        return $this->st_semestre;
    }

    /**
     * @param string $st_semestre
     * @return $this
     */
    public function setSt_semestre($st_semestre)
    {
        $this->st_semestre = $st_semestre;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_aprovafinal()
    {
        return $this->nu_aprovafinal;
    }

    /**
     * @param decimal $nu_aprovafinal
     * @return $this
     */
    public function setNu_aprovafinal($nu_aprovafinal)
    {
        $this->nu_aprovafinal = $nu_aprovafinal;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return $this
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return $this
     *
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_sistemaentidademensagem")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class SistemaEntidadeMensagem
{

    /**
     * @var integer $id_sistemaentidademensagem
     * @Column(name="id_sistemaentidademensagem", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_sistemaentidademensagem;

    /**
     *
     * @var MensagemPadrao $id_mensagempadrao
     * @ManyToOne(targetEntity="MensagemPadrao")
     * @JoinColumn(name="id_mensagempadrao", nullable=false, referencedColumnName="id_mensagempadrao")
     */
    private $id_mensagempadrao;

    /**
     *
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_textosistema;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro") 
     */
    private $dt_cadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo") 
     */
    private $bl_ativo;

    /**
     *
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;


    /**
     *
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    public function getId_sistemaentidademensagem ()
    {
        return $this->id_sistemaentidademensagem;
    }

    public function setId_sistemaentidademensagem ($id_sistemaentidademensagem)
    {
        $this->id_sistemaentidademensagem = $id_sistemaentidademensagem;
        return $this;
    }

    public function getId_mensagempadrao ()
    {
        return $this->id_mensagempadrao;
    }

    public function setId_mensagempadrao (MensagemPadrao $id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
        return $this;
    }

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema (TextoSistema $id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwEnviarMensagemAluno"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_enviarmensagemaluno")
 * @Entity (repositoryClass="\G2\Repository\MensagemRepository")
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwEnviarMensagemAluno
{

    const EVOLUCAO_NAO_LIDA = 42;
    const EVOLUCAO_LIDA = 43;

    /**
     * @SWG\Property(format="integer", example="1")
     * @var integer $id_enviomensagem
     * @Column(name="id_enviomensagem", type="integer", nullable=true, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_enviomensagem;

    /**
     * @SWG\Property(format="string", example="2016-06-07 15:18:28.0000000")
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;


    /**
     * @SWG\Property(format="string", example="2016-06-07 15:18:28.0000000")
     * @var datetime2 $dt_enviar
     * @Column(name="dt_enviar", type="datetime2", nullable=false, length=6)
     */
    private $dt_enviar;


    /**
     * @SWG\Property(format="string", example="2016-06-07 15:18:28.0000000")
     * @var datetime2 $dt_cadastromsg
     * @Column(name="dt_cadastromsg", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastromsg;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @SWG\Property(format="boolean")
     * @var integer $bl_importante
     * @Column(name="bl_importante", type="integer", nullable=true)
     * @GeneratedValue(strategy="NONE")
     */
    private $bl_importante;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_mensagem
     * @Column(name="id_mensagem", type="integer", nullable=false, length=4)
     */
    private $id_mensagem;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tipoenvio
     * @Column(name="id_tipoenvio", type="integer", nullable=false, length=4)
     */
    private $id_tipoenvio;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;


    /**
     * @SWG\Property()
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=100)
     */
    private $st_endereco;


    /**
     * @SWG\Property()
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false)
     */
    private $st_mensagem;


    /**
     * @SWG\Property()
     * @var string $st_texto
     * @Column(name="st_texto", type="string", nullable=false)
     */
    private $st_texto;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;

    /**
     * @SWG\Property()
     * @var bit $bl_entrega
     * @Column(name="bl_entrega", type="bit", nullable=true)
     */
    private $bl_entrega;

    /**
     * @SWG\Property()
     * @var int $id_enviodestinatario
     * @Id
     * @Column(name="id_enviodestinatario", type="integer", nullable=true)
     */
    private $id_enviodestinatario;


    /**
     * @SWG\Property()
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true)
     */
    private $id_entidadematricula;

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastromsg
     */
    public function setDt_cadastromsg($dt_cadastromsg)
    {
        $this->dt_cadastromsg = $dt_cadastromsg;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastromsg()
    {
        return $this->dt_cadastromsg;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_enviar
     */
    public function setDt_enviar($dt_enviar)
    {
        $this->dt_enviar = $dt_enviar;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_enviar()
    {
        return $this->dt_enviar;
    }

    /**
     * @param mixed $id_enviomensagem
     */
    public function setId_enviomensagem($id_enviomensagem)
    {
        $this->id_enviomensagem = $id_enviomensagem;
    }

    /**
     * @return mixed
     */
    public function getId_enviomensagem()
    {
        return $this->id_enviomensagem;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_mensagem
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
    }

    /**
     * @return int
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @param int $id_tipoenvio
     */
    public function setId_tipoenvio($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
    }

    /**
     * @return int
     */
    public function getId_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param string $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @return string
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_mensagem
     */
    public function setSt_mensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @return string
     */
    public function getSt_mensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @param string $st_texto
     */
    public function setSt_texto($st_texto)
    {
        $this->st_texto = $st_texto;
    }

    /**
     * @return string
     */
    public function getSt_texto()
    {
        return $this->st_texto;
    }


    /**
     * @param mixed $bl_importante
     */
    public function setBl_importante($bl_importante)
    {
        $this->bl_importante = $bl_importante;
    }

    /**
     * @return mixed
     */
    public function getBl_importante()
    {
        return $this->bl_importante;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return int
     */
    public function getBl_entrega()
    {
        return $this->bl_entrega;
    }

    /**
     * @param int $bl_entrega
     */
    public function setBl_entrega($bl_entrega)
    {
        $this->bl_entrega = $bl_entrega;
    }

    /**
     * @return bit
     */
    public function getId_enviodestinatario()
    {
        return $this->id_enviodestinatario;
    }

    /**
     * @param bit $id_enviodestinatario
     */
    public function setId_enviodestinatario($id_enviodestinatario)
    {
        $this->id_enviodestinatario = $id_enviodestinatario;
    }


    /**
     * @return integer
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param integer $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_enviodestinatario = $id_entidadematricula;
    }


}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\Venda")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_vendasrecebimento")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwVendasRecebimento extends G2Entity
{

    /**
     * @var integer $id_venda
     * @Column(type="integer", nullable=false, name="id_venda")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_venda;


    /**
     * @var integer $id_usuario
     * @Column(type="integer", nullable=false, name="id_usuario")
     */
    private $id_usuario;


    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var string $st_nomecompleto
     * @Column(type="string", nullable=false, name="st_nomecompleto")
     */
    private $st_nomecompleto;

    /**
     * @var integer $id_formapagamentoaplicacao
     * @Column(type="integer", nullable=false, name="id_formapagamentoaplicacao")
     */
    private $id_formapagamentoaplicacao;

    /**
     * @return integer
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return integer
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return integer
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param integer $id_usuario
     * @return \G2\Entity\VwVendasRecebimento
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     *
     * @param integer $id_venda
     * @return \G2\Entity\VwVendasRecebimento
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @param integer $id_entidade
     * @return \G2\Entity\VwVendasRecebimento
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param string $st_nomecompleto
     * @return \G2\Entity\VwVendasRecebimento
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_formapagamentoaplicacao()
    {
        return $this->id_formapagamentoaplicacao;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pesquisamensagemportal")
 * @Entity
 * @author Denise  Xavier <denise.xavier@unyleya.com.br>
 * @EntityView
 */
class VwPesquisaMensagemPortal
{

    /**
     * @var integer $id_mensagem
     * @Column(name="id_mensagem", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_mensagem;


    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false, length=8000)
     */
    private $st_mensagem;


    /**
     * @var boolean $bl_importante
     * @Column(name="bl_importante", type="boolean", nullable=false)
     */
    private $bl_importante;


    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_texto
     * @Column(name="st_texto", type="string", nullable=false, length=8000)
     */
    private $st_texto;


    /**
     * @var datetime $dt_cadastromsg
     * @Column(name="dt_cadastromsg", type="datetime", nullable=true)
     */
    private $dt_cadastromsg;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     * @var integer $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false)
     */
    private $st_situacao;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true)
     */
    private $id_areaconhecimento;


    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true)
     */
    private $id_turma;


    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true)
     */
    private $id_evolucao;


    /**
     * @var datetime $dt_envio
     * @Column(name="dt_envio", type="datetime", nullable=true)
     */
    private $dt_envio;

    /**
     * @var datetime $dt_saida
     * @Column(name="dt_saida", type="datetime", nullable=true)
     */
    private $dt_saida;

    /**
     * @var datetime $dt_enviar
     * @Column(name="dt_enviar", type="datetime", nullable=false)
     */
    private $dt_enviar;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=8000)
     */
    private $st_evolucao;


    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=300)
     */
    private $st_projetopedagogico;


    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true, length=300)
     */
    private $st_areaconhecimento;


    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=300)
     */
    private $st_turma;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @return int
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @param int $id_mensagem
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
    }

    /**
     * @return string
     */
    public function getSt_mensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @param string $st_mensagem
     */
    public function setSt_mensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @return boolean
     */
    public function getBl_importante()
    {
        return $this->bl_importante;
    }

    /**
     * @param boolean $bl_importante
     */
    public function setBl_importante($bl_importante)
    {
        $this->bl_importante = $bl_importante;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return string
     */
    public function getSt_texto()
    {
        return $this->st_texto;
    }

    /**
     * @param string $st_texto
     */
    public function setSt_texto($st_texto)
    {
        $this->st_texto = $st_texto;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastromsg()
    {
        return $this->dt_cadastromsg;
    }

    /**
     * @param datetime $dt_cadastromsg
     */
    public function setDt_cadastromsg($dt_cadastromsg)
    {
        $this->dt_cadastromsg = $dt_cadastromsg;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param int $id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return datetime
     */
    public function getDt_envio()
    {
        return $this->dt_envio;
    }

    /**
     * @param datetime $dt_envio
     */
    public function setDt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
    }

    /**
     * @return datetime
     */
    public function getDt_saida()
    {
        return $this->dt_saida;
    }

    /**
     * @param datetime $dt_saida
     */
    public function setDt_saida($dt_saida)
    {
        $this->dt_saida = $dt_saida;
    }

    /**
     * @return datetime
     */
    public function getDt_enviar()
    {
        return $this->dt_enviar;
    }

    /**
     * @param datetime $dt_enviar
     */
    public function setDt_enviar($dt_enviar)
    {
        $this->dt_enviar = $dt_enviar;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param string $st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    /**
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param string $st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
    }


    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_turma
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_turma = $st_nomecompleto;
    }


}

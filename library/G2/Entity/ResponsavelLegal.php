<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 03/12/14
 * Time: 15:11
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_responsavellegal")
 * @Entity
 */
class ResponsavelLegal
{
    /**
     * @Id
     * @var integer $id_responsavellegal
     * @Column(name="id_responsavellegal", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_responsavellegal;
    /**
     * @var integer $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;
    /**
     * @var string $st_cpf
     * @Column(type="string", length=11, nullable=false, name="st_cpf")
     */
    private $st_cpf;
    /**
     * @var string $st_nomecompleto
     * @Column(type="string", length=255, nullable=false, name="st_nomecompleto")
     */
    private $st_nomecompleto;
    /**
     * @var integer $nu_ddd
     * @Column(name="nu_ddd", type="integer", nullable=false, length=4)
     */
    private $nu_ddd;
    /**
     * @var integer $nu_telefone
     * @Column(name="nu_telefone", type="integer", nullable=false, length=4)
     */
    private $nu_telefone;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @return integer id_responsavellegal
     */
    public function getId_responsavellegal() {
        return $this->id_responsavellegal;
    }

    /**
     * @param id_responsavellegal
     */
    public function setId_responsavellegal($id_responsavellegal) {
        $this->id_responsavellegal = $id_responsavellegal;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf() {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return integer nu_ddd
     */
    public function getNu_ddd() {
        return $this->nu_ddd;
    }

    /**
     * @param nu_ddd
     */
    public function setNu_ddd($nu_ddd) {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    /**
     * @return integer nu_telefone
     */
    public function getNu_telefone() {
        return $this->nu_telefone;
    }

    /**
     * @param nu_telefone
     */
    public function setNu_telefone($nu_telefone) {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }



}
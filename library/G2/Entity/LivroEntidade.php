<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_livroentidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class LivroEntidade
{

    /**
     * @var integer $id_livroentidade
     * @Column(type="integer", length=4, nullable=false, name="id_livroentidade")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_livroentidade;

    /**
     * @var Livro $id_livro
     * @ManyToOne(targetEntity="Livro")
     * @JoinColumn(name="id_livro", referencedColumnName="id_livro", nullable=false)
     */
    private $id_livro;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false) 
     */
    private $id_entidade;

    /**
     * @return integer id_livroentidade
     */
    public function getId_livroentidade ()
    {
        return $this->id_livroentidade;
    }

    /**
     * 
     * @param integer $id_livroentidade
     * @return \G2\Entity\LivroEntidade
     */
    public function setId_livroentidade ($id_livroentidade)
    {
        $this->id_livroentidade = $id_livroentidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Livro
     */
    public function getId_livro ()
    {
        return $this->id_livro;
    }

    /**
     * @param \G2\Entity\Livro $id_livro
     * @return \G2\Entity\LivroEntidade
     */
    public function setId_livro (Livro $id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\LivroEntidade
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
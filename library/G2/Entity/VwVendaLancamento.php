<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;
use G2\G2Entity;

/**
 * @Entity(repositoryClass="G2\Repository\VwVendaLancamento")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="vw_vendalancamento")
 */
class VwVendaLancamento extends G2Entity
{

    /**
     * @var integer $id_lancamento
     * @Id
     * @Column(name="id_lancamento", type="integer", nullable=false)
     */
    private $id_lancamento;

    /**
     * @var integer $id_venda
     * @Id
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var integer $id_usuariovenda
     * @Column(name="id_usuariovenda", type="integer")
     * */
    private $id_usuariovenda;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer")
     */
    private $id_formapagamento;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer")
     */
    private $nu_parcelas;

    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="float", precision=30, scale=2, nullable=false)
     */
    private $nu_valorliquido;

    /**
     * @var boolean $bl_entrada
     * @Column(name="bl_entrada", type="boolean", nullable=false)
     */
    private $bl_entrada;

    /**
     * @var integer $nu_orderm
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="float", precision=30, scale=2, nullable=false)
     */
    private $nu_valor;

    /**
     * @var decimal $nu_valoratualizado
     * @Column(name="nu_valoratualizado", type="float", precision=30, scale=2, nullable=false)
     */
    private $nu_valoratualizado;

    /**
     * @var decimal $nu_vencimento
     * @Column(name="nu_vencimento", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_vencimento;

    /**
     * @var integer $id_tipolancamento
     * @Column(name="id_tipolancamento", type="integer", nullable=true)
     */
    private $id_tipolancamento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=true)
     */
    private $id_meiopagamento;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true)
     */
    private $id_usuariolancamento;

    /**
     * @var integer $id_entidadelancamento
     * @Column(name="id_entidadelancamento", type="integer", nullable=true)
     */
    private $id_entidadelancamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_banco
     * @Column(name="st_banco", type="string")
     */
    private $st_banco;

    /**
     * @var integer $id_cartaoconfig
     * @Column(name="id_cartaoconfig", type="integer", nullable=true)
     */
    private $id_cartaoconfig;

    /**
     * @var integer $id_boletoconfig
     * @Column(name="id_boletoconfig", type="integer", nullable=true)
     */
    private $id_boletoconfig;

    /**
     * @var boolean $bl_quitado
     * @Column(name="bl_quitado", type="boolean", nullable=false)
     */
    private $bl_quitado;

    /**
     * @Column(name="dt_vencimento", type="date", nullable=true)
     */
    private $dt_vencimento;

    /**
     * @var date $dt_quitado
     * @Column(name="dt_quitado", type="date", nullable=true)
     */
    private $dt_quitado;

    /**
     * @var datetime2 $dt_emissao
     * @Column(name="dt_emissao", type="datetime2", nullable=true)
     */
    private $dt_emissao;

    /**
     * @var datetime2 $dt_prevquitado
     * @Column(name="dt_prevquitado", type="datetime2", nullable=true)
     */
    private $dt_prevquitado;

    /**
     * @var string $st_emissor
     * @Column(name="st_emissor", type="string", nullable=true)
     */
    private $st_emissor;

    /**
     * @var string $st_urlpagamento
     * @Column(name="st_urlpagamento", type="string", nullable=true)
     */
    private $st_urlpagamento;

    /**
     * @var string $st_coddocumento
     * @Column(name="st_coddocumento", type="string", nullable=true)
     */
    private $st_coddocumento;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="float", precision=30, scale=2, nullable=true)
     */
    private $nu_juros;

    /**
     * @var decimal $nu_desconto
     * @Column(name="nu_desconto", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_desconto;

    /**
     * @var decimal $nu_quitado
     * @Column(name="nu_quitado", type="decimal", precision=30, scale=5, nullable=true)
     */
    private $nu_quitado;

    /**
     * @var decimal $nu_multa
     * @Column(name="nu_multa", type="decimal", precision=30, scale=2, nullable=true)
     */
    private $nu_multa;

    /**
     * @var integer $nu_verificacao
     * @Column(name="nu_verificacao", type="integer", nullable=false)
     */
    private $nu_verificacao;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=true)
     */
    private $st_meiopagamento;

    /**
     * @var decimal $nu_jurosatraso
     * @Column(name="nu_jurosatraso", type="float", precision=30, scale=2, nullable=true)
     */
    private $nu_jurosatraso;

    /**
     * @var decimal $nu_multaatraso
     * @Column(name="nu_multaatraso", type="float", precision=30, scale=2, nullable=true)
     */
    private $nu_multaatraso;

    /**
     * @var integer $nu_diavencimento
     * @Column(name="nu_diavencimento", type="integer", nullable=false)
     */
    private $nu_diavencimento;

    /**
     * @var string $st_tipolancamento
     * @Column(name="st_tipolancamento", type="string", nullable=true)
     */
    private $st_tipolancamento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var string $st_codlancamento
     * @Column(name="st_codlancamento", type="string", nullable=true)
     */
    private $st_codlancamento;

    /**
     * @var boolean $bl_original
     * @Column(name="bl_original", type="boolean", nullable=false, options={"default":0})
     */
    private $bl_original = 0;

    /**
     * @var string $recorrente_orderid
     * @Column(name="recorrente_orderid", type="string")
     */
    private $recorrente_orderid;

    /**
     * @var string $st_retornoverificacao
     * @Column(name="st_retornoverificacao", type="string")
     */
    private $st_retornoverificacao;

    /**
     * @var integer $nu_cartao
     * @Column(name="nu_cartao", type="integer")
     */
    private $nu_cartao;

    /**
     * @var integer $id_sistemacobranca
     * @Column(name="id_sistemacobranca", type="integer", nullable=true)
     */
    private $id_sistemacobranca;

    /**
     * @var string $st_urlboleto
     * @Column(name="st_urlboleto", type="string", nullable=true)
     */
    private $st_urlboleto;

    /**
     * @var string $st_transacaoexterna
     * @Column(name="id_transacaoexterna", type="string", nullable=true)
     */
    private $st_transacaoexterna;

    /**
     * @var integer $id_vendaaditivo
     * @Column(name="id_vendaaditivo", type="integer", nullable=true)
     */
    private $id_vendaaditivo;

    /**
     * @var integer $id_acordo
     * @Column(name="id_acordo", type="integer", nullable=true, length=4)
     */
    private $id_acordo;
    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean", nullable=false)
     */
    private $bl_cancelamento;
    /**
     * @var integer $nu_cobranca
     * @Column(name="nu_cobranca", type="integer", nullable=true, length=4)
     */
    private $nu_cobranca;

    /**
     * @var integer $nu_acordos
     * @Column(name="nu_acordos", type="integer")
     */
    private $nu_acordos;

    /**
     * @return int
     */
    public function getNu_acordos()
    {
        return $this->nu_acordos;
    }

    /**
     * @param int $nu_acordos
     */
    public function setNu_acordos($nu_acordos)
    {
        $this->nu_acordos = $nu_acordos;
        return $this;

    }

    /**
     * @deprecated Criados somente para evitarmos problemas
     * @return int
     */
    public function getNuacordos()
    {
        return $this->nu_acordos;
    }

    /**
     * @deprecated Criados somente para evitarmos problemas
     * @param int $nu_acordos
     */
    public function setNuacordos($nu_acordos)
    {
        $this->nu_acordos = $nu_acordos;
        return $this;

    }

    /**
     * @return int
     */
    public function getNu_cobranca()
    {
        return $this->nu_cobranca;
    }

    /**
     * @param int $nu_cobranca
     */
    public function setNu_cobranca($nu_cobranca)
    {
        $this->nu_cobranca = $nu_cobranca;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_assinatura()
    {
        return $this->nu_assinatura;
    }

    /**
     * @param int $nu_assinatura
     */
    public function setNu_assinatura($nu_assinatura)
    {
        $this->nu_assinatura = $nu_assinatura;
        return $this;
    }

    /**
     * @var integer $nu_assinatura
     * @Column(name="nu_assinatura", type="integer", nullable=true, length=4)
     */
    private $nu_assinatura;

    /**
     * @return int
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamento
     * @return VwVendaLancamento
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     * @return VwVendaLancamento
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariovenda()
    {
        return $this->id_usuariovenda;
    }

    /**
     * @param int $id_usuariovenda
     * @return VwVendaLancamento
     */
    public function setId_usuariovenda($id_usuariovenda)
    {
        $this->id_usuariovenda = $id_usuariovenda;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwVendaLancamento
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwVendaLancamento
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @param int $id_formapagamento
     * @return VwVendaLancamento
     */
    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param int $nu_parcelas
     * @return VwVendaLancamento
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param decimal $nu_valorliquido
     * @return VwVendaLancamento
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_entrada()
    {
        return $this->bl_entrada;
    }

    /**
     * @param boolean $bl_entrada
     * @return VwVendaLancamento
     */
    public function setBl_entrada($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param int $nu_ordem
     * @return VwVendaLancamento
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param decimal $nu_valor
     * @return VwVendaLancamento
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_valoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    /**
     * @param decimal $nu_valoratualizado
     * @return VwVendaLancamento
     */
    public function setNu_valoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_vencimento()
    {
        return $this->nu_vencimento;
    }

    /**
     * @param decimal $nu_vencimento
     * @return VwVendaLancamento
     */
    public function setNu_vencimento($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @param int $id_tipolancamento
     * @return VwVendaLancamento
     */
    public function setId_tipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return VwVendaLancamento
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param int $id_meiopagamento
     * @return VwVendaLancamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @param int $id_usuariolancamento
     * @return VwVendaLancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @param int $id_entidadelancamento
     * @return VwVendaLancamento
     */
    public function setId_entidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwVendaLancamento
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return VwVendaLancamento
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_banco()
    {
        return $this->st_banco;
    }

    /**
     * @param string $st_banco
     * @return VwVendaLancamento
     */
    public function setSt_banco($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_cartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @param int $id_cartaoconfig
     * @return VwVendaLancamento
     */
    public function setId_cartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_boletoconfig()
    {
        return $this->id_boletoconfig;
    }

    /**
     * @param int $id_boletoconfig
     * @return VwVendaLancamento
     */
    public function setId_boletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_quitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @param boolean $bl_quitado
     * @return VwVendaLancamento
     */
    public function setBl_quitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param mixed $dt_vencimento
     * @return VwVendaLancamento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
        return $this;
    }

    /**
     * @return date
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param date $dt_quitado
     * @return VwVendaLancamento
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_emissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @param datetime2 $dt_emissao
     * @return VwVendaLancamento
     */
    public function setDt_emissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_prevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @param datetime2 $dt_prevquitado
     * @return VwVendaLancamento
     */
    public function setDt_prevquitado($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_emissor()
    {
        return $this->st_emissor;
    }

    /**
     * @param string $st_emissor
     * @return VwVendaLancamento
     */
    public function setSt_emissor($st_emissor)
    {
        $this->st_emissor = $st_emissor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlpagamento()
    {
        return $this->st_urlpagamento;
    }

    /**
     * @param string $st_urlpagamento
     * @return VwVendaLancamento
     */
    public function setSt_urlpagamento($st_urlpagamento)
    {
        $this->st_urlpagamento = $st_urlpagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_coddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @param string $st_coddocumento
     * @return VwVendaLancamento
     */
    public function setSt_coddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @param decimal $nu_juros
     * @return VwVendaLancamento
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_desconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param decimal $nu_desconto
     * @return VwVendaLancamento
     */
    public function setNu_desconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_quitado()
    {
        return $this->nu_quitado;
    }

    /**
     * @param decimal $nu_quitado
     * @return VwVendaLancamento
     */
    public function setNu_quitado($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_multa()
    {
        return $this->nu_multa;
    }

    /**
     * @param decimal $nu_multa
     * @return VwVendaLancamento
     */
    public function setNu_multa($nu_multa)
    {
        $this->nu_multa = $nu_multa;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_verificacao()
    {
        return $this->nu_verificacao;
    }

    /**
     * @param int $nu_verificacao
     * @return VwVendaLancamento
     */
    public function setNu_verificacao($nu_verificacao)
    {
        $this->nu_verificacao = $nu_verificacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param string $st_meiopagamento
     * @return VwVendaLancamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_jurosatraso()
    {
        return $this->nu_jurosatraso;
    }

    /**
     * @param decimal $nu_jurosatraso
     * @return VwVendaLancamento
     */
    public function setNu_jurosatraso($nu_jurosatraso)
    {
        $this->nu_jurosatraso = $nu_jurosatraso;
        return $this;
    }

    /**
     * @return decimal
     */
    public function getNu_multaatraso()
    {
        return $this->nu_multaatraso;
    }

    /**
     * @param decimal $nu_multaatraso
     * @return VwVendaLancamento
     */
    public function setNu_multaatraso($nu_multaatraso)
    {
        $this->nu_multaatraso = $nu_multaatraso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_diavencimento()
    {
        return $this->nu_diavencimento;
    }

    /**
     * @param int $nu_diavencimento
     * @return VwVendaLancamento
     */
    public function setNu_diavencimento($nu_diavencimento)
    {
        $this->nu_diavencimento = $nu_diavencimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tipolancamento()
    {
        return $this->st_tipolancamento;
    }

    /**
     * @param string $st_tipolancamento
     * @return VwVendaLancamento
     */
    public function setSt_tipolancamento($st_tipolancamento)
    {
        $this->st_tipolancamento = $st_tipolancamento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return VwVendaLancamento
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codlancamento()
    {
        return $this->st_codlancamento;
    }

    /**
     * @param string $st_codlancamento
     * @return VwVendaLancamento
     */
    public function setSt_codlancamento($st_codlancamento)
    {
        $this->st_codlancamento = $st_codlancamento;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_original()
    {
        return $this->bl_original;
    }

    /**
     * @param boolean $bl_original
     * @return VwVendaLancamento
     */
    public function setBl_original($bl_original)
    {
        $this->bl_original = $bl_original;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecorrente_orderid()
    {
        return $this->recorrente_orderid;
    }

    /**
     * @param string $recorrente_orderid
     * @return VwVendaLancamento
     */
    public function setRecorrente_orderid($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_retornoverificacao()
    {
        return $this->st_retornoverificacao;
    }

    /**
     * @param string $st_retornoverificacao
     * @return VwVendaLancamento
     */
    public function setSt_retornoverificacao($st_retornoverificacao)
    {
        $this->st_retornoverificacao = $st_retornoverificacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cartao()
    {
        return $this->nu_cartao;
    }

    /**
     * @param int $nu_cartao
     * @return VwVendaLancamento
     */
    public function setNu_cartao($nu_cartao)
    {
        $this->nu_cartao = $nu_cartao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_sistemacobranca()
    {
        return $this->id_sistemacobranca;
    }

    /**
     * @param int $id_sistemacobranca
     * @return VwVendaLancamento
     */
    public function setId_sistemacobranca($id_sistemacobranca)
    {
        $this->id_sistemacobranca = $id_sistemacobranca;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param string $st_urlboleto
     * @return VwVendaLancamento
     */
    public function setSt_urlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_transacaoexterna()
    {
        return $this->st_transacaoexterna;
    }

    /**
     * @param string $st_transacaoexterna
     * @return VwVendaLancamento
     */
    public function setSt_transacaoexterna($st_transacaoexterna)
    {
        $this->st_transacaoexterna = $st_transacaoexterna;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     * @return VwVendaLancamento
     */
    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdLancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdLancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariovenda()
    {
        return $this->id_usuariovenda;
    }

    /**
     * @param int $id_usuariovenda
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdUsuariovenda($id_usuariovenda)
    {
        $this->id_usuariovenda = $id_usuariovenda;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStNomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStNomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdEvolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdEvolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdFormapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @param int $id_formapagamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdFormapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuParcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param int $nu_parcelas
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuParcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuValorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param decimal $nu_valorliquido
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuValorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlEntrada()
    {
        return $this->bl_entrada;
    }

    /**
     * @param boolean $bl_entrada
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setBlEntrada($bl_entrada)
    {
        $this->bl_entrada = $bl_entrada;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuOrdem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param int $nu_ordem
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuOrdem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuValor()
    {
        return $this->nu_valor;
    }

    /**
     * @param decimal $nu_valor
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuValor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuValoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    /**
     * @param decimal $nu_valoratualizado
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuValoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuVencimento()
    {
        return $this->nu_vencimento;
    }

    /**
     * @param decimal $nu_vencimento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuVencimento($nu_vencimento)
    {
        $this->nu_vencimento = $nu_vencimento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdTipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @param int $id_tipolancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdTipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
        return $this;
    }

    /**
     * @return datetime2
     * @deprecated
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdMeiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param int $id_meiopagamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdMeiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @param int $id_usuariolancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdUsuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdEntidadelancamento()
    {
        return $this->id_entidadelancamento;
    }

    /**
     * @param int $id_entidadelancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdEntidadelancamento($id_entidadelancamento)
    {
        $this->id_entidadelancamento = $id_entidadelancamento;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStBanco()
    {
        return $this->st_banco;
    }

    /**
     * @param string $st_banco
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStBanco($st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdCartaoconfig()
    {
        return $this->id_cartaoconfig;
    }

    /**
     * @param int $id_cartaoconfig
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdCartaoconfig($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdBoletoconfig()
    {
        return $this->id_boletoconfig;
    }

    /**
     * @param int $id_boletoconfig
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdBoletoconfig($id_boletoconfig)
    {
        $this->id_boletoconfig = $id_boletoconfig;
        return $this;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlQuitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @param boolean $bl_quitado
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setBlQuitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
        return $this;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getDtVencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param mixed $dt_vencimento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setDtVencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
        return $this;
    }

    /**
     * @return date
     * @deprecated
     */
    public function getDtQuitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param date $dt_quitado
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setDtQuitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
        return $this;
    }

    /**
     * @return datetime2
     * @deprecated
     */
    public function getDtEmissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @param datetime2 $dt_emissao
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setDtEmissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
        return $this;
    }

    /**
     * @return datetime2
     * @deprecated
     */
    public function getDtPrevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @param datetime2 $dt_prevquitado
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setDtPrevquitado($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStEmissor()
    {
        return $this->st_emissor;
    }

    /**
     * @param string $st_emissor
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStEmissor($st_emissor)
    {
        $this->st_emissor = $st_emissor;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStUrlpagamento()
    {
        return $this->st_urlpagamento;
    }

    /**
     * @param string $st_urlpagamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStUrlpagamento($st_urlpagamento)
    {
        $this->st_urlpagamento = $st_urlpagamento;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStCoddocumento()
    {
        return $this->st_coddocumento;
    }

    /**
     * @param string $st_coddocumento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStCoddocumento($st_coddocumento)
    {
        $this->st_coddocumento = $st_coddocumento;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuJuros()
    {
        return $this->nu_juros;
    }

    /**
     * @param decimal $nu_juros
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuJuros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuDesconto()
    {
        return $this->nu_desconto;
    }

    /**
     * @param decimal $nu_desconto
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuDesconto($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuQuitado()
    {
        return $this->nu_quitado;
    }

    /**
     * @param decimal $nu_quitado
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuQuitado($nu_quitado)
    {
        $this->nu_quitado = $nu_quitado;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuMulta()
    {
        return $this->nu_multa;
    }

    /**
     * @param decimal $nu_multa
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuMulta($nu_multa)
    {
        $this->nu_multa = $nu_multa;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuVerificacao()
    {
        return $this->nu_verificacao;
    }

    /**
     * @param int $nu_verificacao
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuVerificacao($nu_verificacao)
    {
        $this->nu_verificacao = $nu_verificacao;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStMeiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param string $st_meiopagamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStMeiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuJurosatraso()
    {
        return $this->nu_jurosatraso;
    }

    /**
     * @param decimal $nu_jurosatraso
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuJurosatraso($nu_jurosatraso)
    {
        $this->nu_jurosatraso = $nu_jurosatraso;
        return $this;
    }

    /**
     * @return decimal
     * @deprecated
     */
    public function getNuMultaatraso()
    {
        return $this->nu_multaatraso;
    }

    /**
     * @param decimal $nu_multaatraso
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuMultaatraso($nu_multaatraso)
    {
        $this->nu_multaatraso = $nu_multaatraso;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuDiavencimento()
    {
        return $this->nu_diavencimento;
    }

    /**
     * @param int $nu_diavencimento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuDiavencimento($nu_diavencimento)
    {
        $this->nu_diavencimento = $nu_diavencimento;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStTipolancamento()
    {
        return $this->st_tipolancamento;
    }

    /**
     * @param string $st_tipolancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStTipolancamento($st_tipolancamento)
    {
        $this->st_tipolancamento = $st_tipolancamento;
        return $this;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlAtivo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setBlAtivo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStCodlancamento()
    {
        return $this->st_codlancamento;
    }

    /**
     * @param string $st_codlancamento
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStCodlancamento($st_codlancamento)
    {
        $this->st_codlancamento = $st_codlancamento;
        return $this;
    }

    /**
     * @return boolean
     * @deprecated
     */
    public function getBlOriginal()
    {
        return $this->bl_original;
    }

    /**
     * @param boolean $bl_original
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setBlOriginal($bl_original)
    {
        $this->bl_original = $bl_original;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getRecorrenteOrderid()
    {
        return $this->recorrente_orderid;
    }

    /**
     * @param string $recorrente_orderid
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setRecorrenteOrderid($recorrente_orderid)
    {
        $this->recorrente_orderid = $recorrente_orderid;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStRetornoverificacao()
    {
        return $this->st_retornoverificacao;
    }

    /**
     * @param string $st_retornoverificacao
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStRetornoverificacao($st_retornoverificacao)
    {
        $this->st_retornoverificacao = $st_retornoverificacao;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuCartao()
    {
        return $this->nu_cartao;
    }

    /**
     * @param int $nu_cartao
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setNuCartao($nu_cartao)
    {
        $this->nu_cartao = $nu_cartao;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdSistemacobranca()
    {
        return $this->id_sistemacobranca;
    }

    /**
     * @param int $id_sistemacobranca
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdSistemacobranca($id_sistemacobranca)
    {
        $this->id_sistemacobranca = $id_sistemacobranca;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStUrlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param string $st_urlboleto
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStUrlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
        return $this;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStTransacaoexterna()
    {
        return $this->st_transacaoexterna;
    }

    /**
     * @param string $st_transacaoexterna
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setStTransacaoexterna($st_transacaoexterna)
    {
        $this->st_transacaoexterna = $st_transacaoexterna;
        return $this;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdVendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    /**
     * @param int $id_vendaaditivo
     * @return VwVendaLancamento
     * @deprecated
     */
    public function setIdVendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
        return $this;
    }

    /**
     * @return integer id_acordo
     */
    public function getId_acordo() {
        return $this->id_acordo;
    }

    /**
     * @param id_acordo
     */
    public function setId_acordo($id_acordo) {
        $this->id_acordo = $id_acordo; return $this;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
    }



}

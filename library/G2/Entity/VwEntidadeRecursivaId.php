<?php

namespace G2\Entity;

/**
 * Entity VwEntidadeRecursivaId
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidaderecursivaid")
 * @Entity(repositoryClass="\G2\Repository\Entidade")
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwEntidadeRecursivaId {

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=true, length=4)
     */
    private $id_entidadepai;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $nu_entidadepai
     * @Column(name="nu_entidadepai", type="string", nullable=true, length=30)
     */
    private $nu_entidadepai;

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function getNu_entidadepai()
    {
        return $this->nu_entidadepai;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function setNu_entidadepai($nu_entidadepai)
    {
        $this->nu_entidadepai = $nu_entidadepai;
        return $this;
    }

}

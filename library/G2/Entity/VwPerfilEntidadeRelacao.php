<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_perfilentidaderelacao")
 * @Entity(repositoryClass="\G2\Repository\Perfil")
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class VwPerfilEntidadeRelacao
{
    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=false, length=4)
     */
    private $id_entidadepai;

    /**
     * @var integer $id_entidadeclasse
     * @Column(name="id_entidadeclasse", type="integer", nullable=false, length=4)
     */
    private $id_entidadeclasse;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @Id
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_entidadeclasse
     * @Column(name="st_entidadeclasse", type="string", nullable=false, length=255)
     */
    private $st_entidadeclasse;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=100)
     */
    private $st_nomeperfil;

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_entidadepai
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param id_entidadepai
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    /**
     * @return integer id_entidadeclasse
     */
    public function getId_entidadeclasse()
    {
        return $this->id_entidadeclasse;
    }

    /**
     * @param id_entidadeclasse
     */
    public function setId_entidadeclasse($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_entidadeclasse
     */
    public function getSt_entidadeclasse()
    {
        return $this->st_entidadeclasse;
    }

    /**
     * @param st_entidadeclasse
     */
    public function setSt_entidadeclasse($st_entidadeclasse)
    {
        $this->st_entidadeclasse = $st_entidadeclasse;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_nomeperfil
     */
    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    /**
     * @param st_nomeperfil
     */
    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
        return $this;
    }


}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_afiliadoclasseentidade")
 * @Entity
 */
class AfiliadoClasseEntidade
{

    /**
     * @var Afiliado
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="Afiliado")
     * @JoinColumn(name="id_afiliado", referencedColumnName="id_afiliado")
     */
    private $id_afiliado;

    /**
     * @var ClasseAfiliacao
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="ClasseAfiliacao")
     * @JoinColumn(name="id_classeafiliacao", referencedColumnName="id_classeafiliacao")
     */
    private $id_classeafiliacao;

    /**
     * @var Entidade
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @return Afiliado
     */
    public function getId_afiliado()
    {
        return $this->id_afiliado;
    }

    /**
     * @param Afiliado $id_afiliado
     * @return $this
     */
    public function setId_afiliado($id_afiliado)
    {
        $this->id_afiliado = $id_afiliado;
        return $this;
    }

    /**
     * @return ClasseAfiliacao
     */
    public function getId_classeafiliacao()
    {
        return $this->id_classeafiliacao;
    }

    /**
     * @param ClasseAfiliacao $id_classeafiliacao
     * @return $this
     */
    public function setId_classeafiliacao($id_classeafiliacao)
    {
        $this->id_classeafiliacao = $id_classeafiliacao;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }
}
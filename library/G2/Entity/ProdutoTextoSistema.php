<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtotextosistema")
 * @Entity
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class ProdutoTextoSistema
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_produtotextosistema
     * @Column(name="id_produtotextosistema", type="integer", nullable=false, length=4)
     */
    private $id_produtotextosistema;
    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=false, length=4)
     */
    private $id_textosistema;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;
    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=false, length=4)
     */
    private $id_upload;
    /**
     * @var integer $id_formadisponibilizacao
     * @Column(name="id_formadisponibilizacao", type="integer", nullable=false, length=4)
     */
    private $id_formadisponibilizacao;

    /**
     * @return integer id_produtotextosistema
     */
    public function getId_produtotextosistema() {
        return $this->id_produtotextosistema;
    }

    /**
     * @param id_produtotextosistema
     */
    public function setId_produtotextosistema($id_produtotextosistema) {
        $this->id_produtotextosistema = $id_produtotextosistema;
        return $this;
    }

    /**
     * @return integer id_textosistema
     */
    public function getId_textosistema() {
        return $this->id_textosistema;
    }

    /**
     * @param id_textosistema
     */
    public function setId_textosistema($id_textosistema) {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto() {
        return $this->id_produto;
    }

    /**
     * @param id_produto
     */
    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return integer id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return integer id_formadisponibilizacao
     */
    public function getId_formadisponibilizacao() {
        return $this->id_formadisponibilizacao;
    }

    /**
     * @param id_formadisponibilizacao
     */
    public function setId_formadisponibilizacao($id_formadisponibilizacao) {
        $this->id_formadisponibilizacao = $id_formadisponibilizacao;
        return $this;
    }


} 
<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_listagem_projetos_sala")
 * @Entity
 * @EntityView
 */
class VwListagemProjetosSala extends G2Entity
{

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;
    /**
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true, length=4)
     */
    private $id_perfilpedagogico;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_status
     * @Column(name="id_status", type="integer", nullable=false, length=4)
     */
    private $id_status;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @var string $st_coordenador
     * @Column(name="st_coordenador", type="string", nullable=false, length=255)
     */
    private $st_coordenador;
    /**
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=12)
     */
    private $st_status;

    /**
     * @return string
     */
    public function getst_coordenador()
    {
        return $this->st_coordenador;
    }

    /**
     * @param string $st_coordenador
     */
    public function setst_coordenador($st_coordenador)
    {
        $this->st_coordenador = $st_coordenador;
        return $this;

    }









    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_perfil
     */
    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }

    /**
     * @return int
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param int $id_perfilpedagogico
     */
    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }

    /**
     * @return int
     */
    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_status
     */
    public function setId_status($id_status)
    {
        $this->id_status = $id_status;
    }

    /**
     * @return int
     */
    public function getId_status()
    {
        return $this->id_status;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param string $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_status
     */
    public function setSt_status($st_status)
    {
        $this->st_status = $st_status;
    }

    /**
     * @return string
     */
    public function getSt_status()
    {
        return $this->st_status;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_vendedor")
 * @Entity
 */
class Vendedor
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_vendedor
     * @Column(name="id_vendedor", type="integer", nullable=false, length=4)
     */
    private $id_vendedor;

    /**
     * @var string $st_vendedor
     * @Column(name="st_vendedor", type="string", nullable=false, length=255)
     */
    private $st_vendedor;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var Entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @return int
     */
    public function getId_vendedor()
    {
        return $this->id_vendedor;
    }

    /**
     * @param int $id_vendedor
     * @return $this
     */
    public function setId_vendedor($id_vendedor)
    {
        $this->id_vendedor = $id_vendedor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_vendedor()
    {
        return $this->st_vendedor;
    }

    /**
     * @param string $st_vendedor
     * @return $this
     */
    public function setSt_vendedor($st_vendedor)
    {
        $this->st_vendedor = $st_vendedor;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param Entidade $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }
}
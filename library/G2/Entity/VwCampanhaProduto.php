<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_campanhaproduto")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwCampanhaProduto
{
    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;


    /**
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false, length=4)
     */
    private $id_tipoproduto;

    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=false, length=255)
     */
    private $st_campanhacomercial;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=false, length=250)
     */
    private $st_produto;

    /**
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=false, length=255)
     */
    private $st_tipoproduto;

    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    public function getId_produto ()
    {
        return $this->id_produto;
    }

    public function getId_tipoproduto ()
    {
        return $this->id_tipoproduto;
    }

    public function getSt_campanhacomercial ()
    {
        return $this->st_campanhacomercial;
    }

    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    public function getSt_tipoproduto ()
    {
        return $this->st_tipoproduto;
    }

    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function setId_tipoproduto ($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    public function setSt_campanhacomercial ($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    public function setSt_tipoproduto ($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

}

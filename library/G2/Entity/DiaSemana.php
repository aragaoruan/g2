<?php

/*
 * Entity DiaSemana
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-09-12
 */

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_diasemana")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DiaSemana {

    /**
     *
     * @var integer $id_diasemana
     * @Column(name="id_diasemana", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_diasemana;

    /**
     * @var $horarioAula
     * @ManyToMany(targetEntity="HorarioAula", inversedBy="diaSemana")
     * @JoinTable(name="tb_horariodiasemana",
     *   joinColumns={@JoinColumn(name="id_diasemana", referencedColumnName="id_diasemana")},
     *   inverseJoinColumns={@JoinColumn(name="id_horarioaula", referencedColumnName="id_horarioaula")}
     * )
     */
     private $horarioAula;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_diasemana")
     * @var string
     */
    private $st_diasemana;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_abreviatura")
     * @var string
     */
    private $st_abreviatura;


    public function __construct ()
    {
        $this->horarioAula= new ArrayCollection();
    }

    public function getHorarioAula() {
        return $this->horarioAula;
    }




    public function getId_diasemana() {
        return $this->id_diasemana;
    }

    public function getSt_diasemana() {
        return $this->st_diasemana;
    }

    public function getSt_abreviatura() {
        return $this->st_abreviatura;
    }

    public function setId_diasemana($id_diasemana) {
        $this->id_diasemana = $id_diasemana;
    }

    public function setSt_diasemana($st_diasemana) {
        $this->st_diasemana = $st_diasemana;
    }

    public function setSt_abreviatura($st_abreviatura) {
        $this->st_abreviatura= $st_abreviatura;
    }


}

<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_campanhasistemaexibicao")
 * @Entity
 * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
 */
class CampanhaSistemasExibicao
{
    /**
     * @var integer $id_campanhasistemaexibicao
     * @Column(name="id_campanhasistemaexibicao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_campanhasistemaexibicao;

    /**
     * @var boolean $bl_portaldoaluno
     * @Column(name="bl_portaldoaluno", type="boolean", nullable=true, length=1)
     */

    private $bl_portaldoaluno;

    /**
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=true, length=4)
     */
    private $id_campanhacomercial;

    /**
     * @var boolean $bl_mobile
     * @Column(name="bl_mobile", type="boolean", nullable=true, length=1)
     */
    private $bl_mobile;

    /**
     * @return Integer
     */
    public function getId_campanhasistemaexibicao()
    {
        return $this->id_campanhasistemaexibicao;
    }

    /**
     *
     * @param \G2\Entity\TipoDesconto $id_tipodesconto
     * @return \G2\Entity\CampanhaSistemasExibicao
     */
    public function setId_campanhasistemaexibicao($id_campanhasistemaexibicao)
    {
        $this->id_campanhasistemaexibicao = $id_campanhasistemaexibicao;
        return $this;
    }

    /**
     * @return Integer
     */
    public function getBl_portaldoaluno()
    {
        return $this->bl_portaldoaluno;
    }

    /**
     *
     * @param $bl_portaldoaluno
     * @return $this
     */
    public function setBl_portaldoaluno($bl_portaldoaluno)
    {
        $this->bl_portaldoaluno = $bl_portaldoaluno;
        return $this;
    }

    /**
     * @return Integer
     */
    public function getBl_mobile()
    {
        return $this->bl_mobile;
    }

    /**
     *
     * @param $bl_mobile
     * @return $this
     */
    public function setBl_mobile($bl_mobile)
    {
        $this->bl_mobile = $bl_mobile;
        return $this;
    }

    /**
     * @return Integer
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     *
     * @return Integer
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

}

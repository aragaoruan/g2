<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_projetoarea")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwProjetoArea
{

    /**
     *
     * @var integer $id_areaconhecimento
     * @Column(type="integer", name="id_areaconhecimento", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;


    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;


    /**
     *
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;


    /**
     *
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     *
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicao;


    /**
     *
     * @var integer $tipo
     * @Column(type="integer", name="tipo", nullable=true)
     */
    private $tipo;


    /**
     *
     * @var integer $id_entidade
     * @Column(type="integer", name="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $nu_diasacesso
     * @Column(type="integer", name="nu_diasacesso", nullable=false)
     */
    private $nu_diasacesso;


    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
    }

    public function getTipo()
    {
        return $this->tipo;
    }

    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getNu_diasacesso()
    {
        return $this->nu_diasacesso;
    }

    public function setNu_diasacesso($nu_diasacesso)
    {
        $this->nu_diasacesso = $nu_diasacesso;
    }


}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_funcionalidade")
 * @Entity(repositoryClass="\G2\Repository\Funcionalidade")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Funcionalidade extends G2Entity
{

    /**
     * @var integer $id_funcionalidade
     * @Column(name="id_funcionalidade", type="integer", nullable=false)
     * @OneToMany(targetEntity="Entidade", mappedBy="id_funcionalidadepai")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_funcionalidade;

    /**
     * @var string $st_funcionalidade
     * @Column(name="st_funcionalidade", type="string", nullable=false, length=100)
     */
    private $st_funcionalidade;

    /**
     * @var Funcionalidade $id_funcionalidadepai
     * @ManyToOne(targetEntity="Funcionalidade")
     * @JoinColumn(name="id_funcionalidadepai", nullable=true, referencedColumnName="id_funcionalidade")
     */
    private $id_funcionalidadepai;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var string $st_classeflex
     * @Column(name="st_classeflex", type="string", nullable=true, length=300)
     */
    private $st_classeflex;

    /**
     * @var string $st_urlicone
     * @Column(name="st_urlicone", type="string", nullable=true, length=1000)
     */
    private $st_urlicone;

    /**
     * @var TipoFuncionalidade $id_tipofuncionalidade
     * @ManyToOne(targetEntity="TipoFuncionalidade")
     * @JoinColumn(name="id_tipofuncionalidade", nullable=true, referencedColumnName="id_tipofuncionalidade")
     */
    private $id_tipofuncionalidade;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @var integer $st_classeflexbotao
     * @Column(name="st_classeflexbotao", type="integer", nullable=true, length=300)
     */
    private $st_classeflexbotao;

    /**
     * @var boolean $bl_pesquisa
     * @Column(name="bl_pesquisa", type="boolean", nullable=false)
     */
    private $bl_pesquisa;

    /**
     * @var boolean $bl_lista
     * @Column(name="bl_lista", type="boolean", nullable=false)
     */
    private $bl_lista;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", nullable=false, referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var text $st_ajuda
     * @Column(name="st_ajuda", type="text", nullable=true)
     */
    private $st_ajuda;

    /**
     * @var string $st_target
     * @Column(name="st_target", type="string", nullable=true, length=100)
     */
    private $st_target;

    /**
     * @var string $st_urlcaminho
     * @Column(name="st_urlcaminho", type="string", nullable=true, length=50)
     */
    private $st_urlcaminho;

    /**
     *
     * @var boolean $bl_relatorio
     * @Column(name="bl_relatorio", type="boolean", nullable=true)
     */
    private $bl_relatorio;

    /**
     *
     * @var string $st_urlcaminhoedicao
     * @Column(type="string", length=50, nullable=true, name="st_urlcaminhoedicao")
     */
    private $st_urlcaminhoedicao;

    /**
     *
     * @var boolean $bl_delete
     * @Column(name="bl_delete", type="boolean", nullable=false)
     */
    private $bl_delete;

    public function getSt_urlcaminhoedicao ()
    {
        return $this->st_urlcaminhoedicao;
    }

    public function setSt_urlcaminhoedicao ($st_urlcaminhoedicao)
    {
        $this->st_urlcaminhoedicao = $st_urlcaminhoedicao;
        return $this;
    }

    public function getId_funcionalidade ()
    {
        return $this->id_funcionalidade;
    }

    public function setId_funcionalidade ($id_funcionalidade)
    {
        $this->id_funcionalidade = $id_funcionalidade;
        return $this;
    }

    public function getSt_funcionalidade ()
    {
        return $this->st_funcionalidade;
    }

    public function setSt_funcionalidade ($st_funcionalidade)
    {
        $this->st_funcionalidade = $st_funcionalidade;
        return $this;
    }

    public function getId_funcionalidadepai ()
    {
        return $this->id_funcionalidadepai;
    }

    /**
     *
     * @param \G2\Entity\Funcionalidade $id_funcionalidadepai
     * @return \G2\Entity\Funcionalidade
     */
    public function setId_funcionalidadepai (Funcionalidade $id_funcionalidadepai)
    {
        $this->id_funcionalidadepai = $id_funcionalidadepai;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     *
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\Funcionalidade
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_classeflex ()
    {
        return $this->st_classeflex;
    }

    public function setSt_classeflex ($st_classeflex)
    {
        $this->st_classeflex = $st_classeflex;
        return $this;
    }

    public function getSt_urlicone ()
    {
        return $this->st_urlicone;
    }

    public function setSt_urlicone ($st_urlicone)
    {
        $this->st_urlicone = $st_urlicone;
        return $this;
    }

    public function getId_tipofuncionalidade ()
    {
        return $this->id_tipofuncionalidade;
    }

    /**
     *
     * @param \G2\Entity\TipoFuncionalidade $id_tipofuncionalidade
     * @return \G2\Entity\Funcionalidade
     */
    public function setId_tipofuncionalidade (TipoFuncionalidade $id_tipofuncionalidade)
    {
        $this->id_tipofuncionalidade = $id_tipofuncionalidade;
        return $this;
    }

    public function getNu_ordem ()
    {
        return $this->nu_ordem;
    }

    public function setNu_ordem ($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
        return $this;
    }

    public function getSt_classeflexbotao ()
    {
        return $this->st_classeflexbotao;
    }

    public function setSt_classeflexbotao ($st_classeflexbotao)
    {
        $this->st_classeflexbotao = $st_classeflexbotao;
        return $this;
    }

    public function getBl_pesquisa ()
    {
        return $this->bl_pesquisa;
    }

    public function setBl_pesquisa ($bl_pesquisa)
    {
        $this->bl_pesquisa = $bl_pesquisa;
        return $this;
    }

    public function getBl_lista ()
    {
        return $this->bl_lista;
    }

    public function setBl_lista ($bl_lista)
    {
        $this->bl_lista = $bl_lista;
        return $this;
    }

    public function getId_sistema ()
    {
        return $this->id_sistema;
    }

    /**
     *
     * @param \G2\Entity\Sistema $id_sistema
     * @return \G2\Entity\Funcionalidade
     */
    public function setId_sistema (Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    public function getSt_ajuda ()
    {
        return $this->st_ajuda;
    }

    public function setSt_ajuda ($st_ajuda)
    {
        $this->st_ajuda = $st_ajuda;
        return $this;
    }

    public function getSt_target ()
    {
        return $this->st_target;
    }

    public function setSt_target ($st_target)
    {
        $this->st_target = $st_target;
        return $this;
    }

    public function getSt_urlcaminho ()
    {
        return $this->st_urlcaminho;
    }

    public function setSt_urlcaminho ($st_urlcaminho)
    {
        $this->st_urlcaminho = $st_urlcaminho;
        return $this;
    }

    public function getBl_relatorio ()
    {
        return $this->bl_relatorio;
    }

    public function setBl_relatorio ($bl_relatorio)
    {
        $this->bl_relatorio = $bl_relatorio;
        return $this;
    }

    public function getBl_delete()
    {
        return $this->bl_delete;
    }

    public function setBl_delete($bl_delete)
    {
        $this->bl_delete = $bl_delete;
    }


}

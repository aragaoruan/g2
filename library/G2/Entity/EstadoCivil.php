<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_estadocivil")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EstadoCivil
{

    /**
     * @var integer $id_estadocivil
     * @Column(type="integer", nullable=false, name="id_estadocivil") 
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_estadocivil;

    /**
     * @var string $id_estadocivil
     * @Column(type="string", nullable=false, length=255, name="st_estadocivil") 
     */
    private $st_estadocivil;

    public function getId_estadocivil ()
    {
        return $this->id_estadocivil;
    }

    public function setId_estadocivil ($id_estadocivil)
    {
        $this->id_estadocivil = $id_estadocivil;
        return $this;
    }

    public function getSt_estadocivil ()
    {
        return $this->st_estadocivil;
    }

    public function setSt_estadocivil ($st_estadocivil)
    {
        $this->st_estadocivil = $st_estadocivil;
        return $this;
    }

}
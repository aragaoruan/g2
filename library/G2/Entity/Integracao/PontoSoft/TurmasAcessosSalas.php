<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="turmas_acessos_salas")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */

class TurmasAcessosSalas
{

    /**
     *
     * @var string $cod_turma
     * @Column(name="cod_turma", type="string", nullable=false)
     * @Id
     */
    private $cod_turma;

    /**
     *
     * @var integer $tipo_treinamento
     * @Column(name="tipo_treinamento", type="integer", nullable=false)
     */
    private $tipo_treinamento;

    /**
     *
     * @var \Datetime $data_inicial_turma
     * @Column(name="data_inicial_turma", type="datetime", nullable=false)
     */
    private $data_inicial_turma;

    /**
     *
     * @var integer $cod_horario_acesso
     * @Column(name="cod_horario_acesso", type="integer", nullable=false)
     */
    private $cod_horario_acesso;

    /**
     *
     * @var integer $dia_semana
     * @Column(name="dia_semana", type="integer", nullable=false)
     */
    private $dia_semana;

    /**
     *
     * @var integer $cod_sala
     * @Column(name="cod_sala", type="integer", nullable=false)
     */
    private $cod_sala;

    /**
     * @return int
     */
    public function getcod_turma()
    {
        return $this->cod_turma;
    }

    /**
     * @param int $cod_turma
     */
    public function setcod_turma($cod_turma)
    {
        $this->cod_turma = $cod_turma;
        return $this;
    }

    /**
     * @return mixed
     */
    public function gettipo_treinamento()
    {
        return $this->tipo_treinamento;
    }

    /**
     * @param mixed $tipo_treinamento
     */
    public function settipo_treinamento($tipo_treinamento)
    {
        $this->tipo_treinamento = $tipo_treinamento;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdata_inicial_turma()
    {
        return $this->data_inicial_turma;
    }

    /**
     * @param mixed $data_inicial_turma
     */
    public function setdata_inicial_turma($data_inicial_turma)
    {
        $this->data_inicial_turma = $data_inicial_turma;
        return $this;
    }

    /**
     * @return int
     */
    public function getcod_horario_acesso()
    {
        return $this->cod_horario_acesso;
    }

    /**
     * @param int $cod_horario_acesso
     */
    public function setcod_horario_acesso($cod_horario_acesso)
    {
        $this->cod_horario_acesso = $cod_horario_acesso;
        return $this;
    }

    /**
     * @return int
     */
    public function getdia_semana()
    {
        return $this->dia_semana;
    }

    /**
     * @param int $dia_semana
     */
    public function setdia_semana($dia_semana)
    {
        $this->dia_semana = $dia_semana;
        return $this;
    }

    /**
     * @return int
     */
    public function getcod_sala()
    {
        return $this->cod_sala;
    }

    /**
     * @param int $cod_sala
     */
    public function setcod_sala($cod_sala)
    {
        $this->cod_sala = $cod_sala;
        return $this;
    }



}
<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="tipos_pessoas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class TiposPessoas
{

    /**
     *
     * @var integer $tipo_pessoa
     * @Column(name="data_cadastro", type="datetime", nullable=false)
     * @Id
     */
    private $tipo_pessoa;

    /**
     * @return int
     */
    public function getTipoPessoa()
    {
        return $this->tipo_pessoa;
    }

    /**
     * @param int $tipo_pessoa
     */
    public function setTipoPessoa($tipo_pessoa)
    {
        $this->tipo_pessoa = $tipo_pessoa;
    }
}
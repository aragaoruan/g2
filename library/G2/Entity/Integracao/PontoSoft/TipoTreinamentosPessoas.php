<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="tipos_treinamentos_pessoas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class TipoTreinamentosPessoas
{

    /**
     * @var integer $tipo_treinamento
     * @Column(name="tipo_treinamento", type="integer", nullable=false)
     * @Id
     */
    private $tipo_treinamento;

    /**
     *
     * @var string $descr_treinamento
     * @Column(name="descr_treinamento", type="string", nullable=false)
     */
    private $descr_treinamento;

    /**
     *
     * @var string $conteudo
     * @Column(name="conteudo", type="string", nullable=false)
     */
    private $conteudo;

    /**
     *
     * @var string $carga_horaria_padrao
     * @Column(name="carga_horaria_padrao", type="integer", nullable=false)
     */
    private $carga_horaria_padrao;

    /**
     * @return string
     */
    public function getCargaHorariaPadrao()
    {
        return $this->carga_horaria_padrao;
    }

    /**
     * @param string $carga_horaria_padrao
     */
    public function setCargaHorariaPadrao($carga_horaria_padrao)
    {
        $this->carga_horaria_padrao = $carga_horaria_padrao;
    }

    /**
     * @return string
     */
    public function getConteudo()
    {
        return $this->conteudo;
    }

    /**
     * @param string $conteudo
     */
    public function setConteudo($conteudo)
    {
        $this->conteudo = $conteudo;
    }

    /**
     * @return string
     */
    public function getDescrTreinamento()
    {
        return $this->descr_treinamento;
    }

    /**
     * @param string $descr_treinamento
     */
    public function setDescrTreinamento($descr_treinamento)
    {
        $this->descr_treinamento = $descr_treinamento;
    }

    /**
     * @return int
     */
    public function getTipoTreinamento()
    {
        return $this->tipo_treinamento;
    }

    /**
     * @param int $tipo_treinamento
     */
    public function setTipoTreinamento($tipo_treinamento)
    {
        $this->tipo_treinamento = $tipo_treinamento;
    }


}
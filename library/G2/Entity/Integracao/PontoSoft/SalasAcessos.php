<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="salas_acessos")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */

class SalasAcessos
{

    /**
     * @Id
     * @var integer $cod_sala
     * @Column(name="cod_sala", type="integer", nullable=false)
     */
    private $cod_sala;


    /**
     *
     * @var string $nome_sala
     * @Column(name="nome_sala", type="string", nullable=false)
     */
    private $nome_sala;

    /**
     *
     * @var integer $nro_vagas
     * @Column(name="nro_vagas", type="integer", nullable=false)
     */
    private $nro_vagas;

    /**
     * @return int
     */
    public function getcod_sala()
    {
        return $this->cod_sala;
    }

    /**
     * @param int $cod_sala
     */
    public function setcod_sala($cod_sala)
    {
        $this->cod_sala = $cod_sala;
        return $this;
    }

    /**
     * @return string
     */
    public function getnome_sala()
    {
        return $this->nome_sala;
    }

    /**
     * @param string $nome_sala
     */
    public function setnome_sala($nome_sala)
    {
        $this->nome_sala = $nome_sala;
        return $this;
    }

    /**
     * @return int
     */
    public function getnro_vagas()
    {
        return $this->nro_vagas;
    }

    /**
     * @param int $nro_vagas
     */
    public function setnro_vagas($nro_vagas)
    {
        $this->nro_vagas = $nro_vagas;
        return $this;
    }




}
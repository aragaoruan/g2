<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="controles_pessoas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class ControlesPessoas
{

    /**
     *
     * @var integer $cod_pessoa
     * @Column(name="cod_pessoa", type="integer", nullable=false)
     * @Id
     */
    private $cod_pessoa;

    /**
     *
     * @var string $nome_pessoa
     * @Column(name="nome_pessoa", type="string", nullable=false)
     */
    private $nome_pessoa;

    /**
     *
     * @var string $acesso_liberado
     * @Column(name="acesso_liberado", type="string", nullable=false)
     */
    private $acesso_liberado;

    /**
     *
     * @var string $ponto_liberado
     * @Column(name="ponto_liberado", type="string", nullable=false)
     */
    private $ponto_liberado;

    /**
     *
     * @var string $sexo
     * @Column(name="sexo", type="string", nullable=false)
     */
    private $sexo;

    /**
     *
     * @var integer $cpf
     * @Column(name="cpf", type="integer", nullable=false)
     */
    private $cpf;

    /**
     *
     * @var integer $cod_cartao
     * @Column(name="cod_cartao", type="integer", nullable=false)
     */
    private $cod_cartao;

    /**
     *
     * @var string $cartao_bloqueado
     * @Column(name="cartao_bloqueado", type="string", nullable=false)
     */
    private $cartao_bloqueado;

    /**
     *
     * @var string $tipo_acesso
     * @Column(name="tipo_acesso", type="string", nullable=false)
     */
    private $tipo_acesso;

    /**
     * @return int
     */
    public function getCodPessoa()
    {
        return $this->cod_pessoa;
    }

    /**
     * @param int $cod_pessoa
     */
    public function setCodPessoa($cod_pessoa)
    {
        $this->cod_pessoa = $cod_pessoa;
    }

    /**
     * @return mixed
     */
    public function getNomePessoa()
    {
        return $this->nome_pessoa;
    }

    /**
     * @param mixed $nome_pessoa
     */
    public function setNomePessoa($nome_pessoa)
    {
        $this->nome_pessoa = $nome_pessoa;
    }

    /**
     * @return string
     */
    public function getAcessoLiberado()
    {
        return $this->acesso_liberado;
    }

    /**
     * @param string $acesso_liberado
     */
    public function setAcessoLiberado($acesso_liberado)
    {
        $this->acesso_liberado = $acesso_liberado;
    }

    /**
     * @return string
     */
    public function getPontoLiberado()
    {
        return $this->ponto_liberado;
    }

    /**
     * @param string $ponto_liberado
     */
    public function setPontoLiberado($ponto_liberado)
    {
        $this->ponto_liberado = $ponto_liberado;
    }

    /**
     * @return string
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param string $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return int
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param int $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return int
     */
    public function getCodCartao()
    {
        return $this->cod_cartao;
    }

    /**
     * @param int $cod_cartao
     */
    public function setCodCartao($cod_cartao)
    {
        $this->cod_cartao = $cod_cartao;
    }

    /**
     * @return mixed
     */
    public function getCartaoBloqueado()
    {
        return $this->cartao_bloqueado;
    }

    /**
     * @param mixed $cartao_bloqueado
     */
    public function setCartaoBloqueado($cartao_bloqueado)
    {
        $this->cartao_bloqueado = $cartao_bloqueado;
    }

    /**
     * @return mixed
     */
    public function getTipoAcesso()
    {
        return $this->tipo_acesso;
    }

    /**
     * @param mixed $tipo_acesso
     */
    public function setTipoAcesso($tipo_acesso)
    {
        $this->tipo_acesso = $tipo_acesso;
    }
}
<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="horarios_acessos")
 * @Entity
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */

class HorariosAcessos
{
    /**
     *
     * @var integer $cod_horario_acesso
     * @Column(name="cod_horario_acesso", type="integer", nullable=false)
     * @Id
     */
    private $cod_horario_acesso;

    /**
     *
     * @var integer $limite_horario_movel
     * @Column(name="limite_horario_movel", type="integer", nullable=false)
     */
    private $limite_horario_movel;

    /**
     *
     * @var string $descr_horario_acesso
     * @Column(name="descr_horario_acesso", type="string", nullable=false)
     */
    private $descr_horario_acesso;

    /**
     *
     * @var string $vetor_horarios
     * @Column(name="vetor_horarios", type="string", nullable=false)
     */
    private $vetor_horarios;

    /**
     *
     * @var string $padrao_hora_extra
     * @Column(name="padrao_hora_extra", type="string", nullable=false)
     */
    private $padrao_hora_extra;

    /**
     * @return int
     */
    public function getcod_horario_acesso()
    {
        return $this->cod_horario_acesso;
    }

    /**
     * @param int $cod_horario_acesso
     */
    public function setcod_horario_acesso($cod_horario_acesso)
    {
        $this->cod_horario_acesso = $cod_horario_acesso;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getlimite_horario_movel()
    {
        return $this->limite_horario_movel;
    }

    /**
     * @param mixed $limite_horario_movel
     */
    public function setlimite_horario_movel($limite_horario_movel)
    {
        $this->limite_horario_movel = $limite_horario_movel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getdescr_horario_acesso()
    {
        return $this->descr_horario_acesso;
    }

    /**
     * @param mixed $descr_horario_acesso
     */
    public function setdescr_horario_acesso($descr_horario_acesso)
    {
        $this->descr_horario_acesso = $descr_horario_acesso;
        return $this;
    }

    /**
     * @return string
     */
    public function getvetor_horarios()
    {
        return $this->vetor_horarios;
    }

    /**
     * @param string $vetor_horarios
     */
    public function setvetor_horarios($vetor_horarios)
    {
        $this->vetor_horarios = $vetor_horarios;
        return $this;
    }

    /**
     * @return string
     */
    public function getpadrao_hora_extra()
    {
        return $this->padrao_hora_extra;
    }

    /**
     * @param string $padrao_hora_extra
     */
    public function setpadrao_hora_extra($padrao_hora_extra)
    {
        $this->padrao_hora_extra = $padrao_hora_extra;
        return $this;
    }




}
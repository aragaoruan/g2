<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="turmas_pessoas_acessos")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class TurmasPessoasAcessos
{
    /**
     *
     * @var integer $cod_pessoa
     * @Column(name="cod_pessoa", type="integer", nullable=false)
     * @Id
     */
    private $cod_pessoa;

    /**
     *
     * @var \Datetime $data_inicial
     * @Column(name="data_inicial", type="datetime", nullable=false)
     */
    private $data_inicial;

    /**
     *
     * @var integer $tipo_treinamento
     * @Column(name="tipo_treinamento", type="integer", nullable=false)
     */
    private $tipo_treinamento;

    /**
     *
     * @var string $cod_turma
     * @Column(name="cod_turma", type="string", nullable=false)
     */
    private $cod_turma;

    /**
     *
     * @var \Datetime $data_inicial_turma
     * @Column(name="data_inicial_turma", type="datetime", nullable=false)
     */
    private $data_inicial_turma;

    /**
     *
     * @var \Datetime $data_final
     * @Column(name="data_final", type="datetime", nullable=false)
     */
    private $data_final;

    /**
     * @return int
     */
    public function getCodPessoa()
    {
        return $this->cod_pessoa;
    }

    /**
     * @param int $cod_pessoa
     */
    public function setCodPessoa($cod_pessoa)
    {
        $this->cod_pessoa = $cod_pessoa;
    }

    /**
     * @return string
     */
    public function getCodTurma()
    {
        return $this->cod_turma;
    }

    /**
     * @param string $cod_turma
     */
    public function setCodTurma($cod_turma)
    {
        $this->cod_turma = $cod_turma;
    }

    /**
     * @return \Datetime
     */
    public function getDataFinal()
    {
        return $this->data_final;
    }

    /**
     * @param \Datetime $data_final
     */
    public function setDataFinal($data_final)
    {
        $this->data_final = $data_final;
    }

    /**
     * @return \Datetime
     */
    public function getDataInicial()
    {
        return $this->data_inicial;
    }

    /**
     * @param \Datetime $data_inicial
     */
    public function setDataInicial($data_inicial)
    {
        $this->data_inicial = $data_inicial;
    }

    /**
     * @return \Datetime
     */
    public function getDataInicialTurma()
    {
        return $this->data_inicial_turma;
    }

    /**
     * @param \Datetime $data_inicial_turma
     */
    public function setDataInicialTurma($data_inicial_turma)
    {
        $this->data_inicial_turma = $data_inicial_turma;
    }

    /**
     * @return int
     */
    public function getTipoTreinamento()
    {
        return $this->tipo_treinamento;
    }

    /**
     * @param int $tipo_treinamento
     */
    public function setTipoTreinamento($tipo_treinamento)
    {
        $this->tipo_treinamento = $tipo_treinamento;
    }


}
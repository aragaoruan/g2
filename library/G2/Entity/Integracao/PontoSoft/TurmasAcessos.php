<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="turmas_acessos")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class TurmasAcessos
{

    /**
     *
     * @var string $cod_turma
     * @Column(name="cod_turma", type="string", nullable=false)
     * @Id
     */
    private $cod_turma;

    /**
     *
     * @var integer $tipo_treinamento
     * @Column(name="tipo_treinamento", type="integer", nullable=false)
     */
    private $tipo_treinamento;

    /**
     *
     * @var \Datetime $data_inicial_turma
     * @Column(name="data_inicial_turma", type="datetime", nullable=false)
     */
    private $data_inicial_turma;

    /**
     *
     * @var \Datetime $data_final_turma
     * @Column(name="data_final_turma", type="datetime", nullable=false)
     */
    private $data_final_turma;

    /**
     * @return string
     */
    public function getCodTurma()
    {
        return $this->cod_turma;
    }

    /**
     * @param string $cod_turma
     */
    public function setCodTurma($cod_turma)
    {
        $this->cod_turma = $cod_turma;
    }

    /**
     * @return \Datetime
     */
    public function getDataFinalTurma()
    {
        return $this->data_final_turma;
    }

    /**
     * @param \Datetime $data_final_turma
     */
    public function setDataFinalTurma($data_final_turma)
    {
        $this->data_final_turma = $data_final_turma;
    }

    /**
     * @return \Datetime
     */
    public function getDataInicialTurma()
    {
        return $this->data_inicial_turma;
    }

    /**
     * @param \Datetime $data_inicial_turma
     */
    public function setDataInicialTurma($data_inicial_turma)
    {
        $this->data_inicial_turma = $data_inicial_turma;
    }

    /**
     * @return string
     */
    public function getTipoTreinamento()
    {
        return $this->tipo_treinamento;
    }

    /**
     * @param string $tipo_treinamento
     */
    public function setTipoTreinamento($tipo_treinamento)
    {
        $this->tipo_treinamento = $tipo_treinamento;
    }


}
<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="tipos_controles_pessoas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class TiposControlesPessoas
{

    /**
     *
     * @var integer $cod_pessoa
     * @Column(name="cod_pessoa", type="integer", nullable=false)
     * @Id
     */
    private $cod_pessoa;

    /**
     *
     * @var integer $tipo_pessoa
     * @Column(name="tipo_pessoa", type="integer", nullable=false)
     */
    private $tipo_pessoa;

    /**
     *
     * @var \Datetime $data_cadastro
     * @Column(name="data_cadastro", type="datetime", nullable=false)
     */
    private $data_cadastro;

    /**
     * @return int
     */
    public function getCodPessoa()
    {
        return $this->cod_pessoa;
    }

    /**
     * @param int $cod_pessoa
     */
    public function setCodPessoa($cod_pessoa)
    {
        $this->cod_pessoa = $cod_pessoa;
    }

    /**
     * @return \Datetime
     */
    public function getDataCadastro()
    {
        return $this->data_cadastro;
    }

    /**
     * @param \Datetime $data_cadastro
     */
    public function setDataCadastro($data_cadastro)
    {
        $this->data_cadastro = $data_cadastro;
    }

    /**
     * @return int
     */
    public function getTipoPessoa()
    {
        return $this->tipo_pessoa;
    }

    /**
     * @param int $tipo_pessoa
     */
    public function setTipoPessoa($tipo_pessoa)
    {
        $this->tipo_pessoa = $tipo_pessoa;
    }
}
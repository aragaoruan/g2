<?php

namespace G2\Entity\Integracao\PontoSoft;

/**
 * @Table (name="perfis_acessos_pessoas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */

class PerfisAcessosPessoas
{

    /**
     *
     * @var integer $cod_pessoa
     * @Column(name="cod_pessoa", type="integer", nullable=false)
     * @Id
     */
    private $cod_pessoa;

    /**
     *
     * @var \Datetime $data_inicio
     * @Column(name="data_inicio", type="datetime", nullable=false)
     */
    private $data_inicio;

    /**
     *
     * @var integer $cod_perfil
     * @Column(name="cod_perfil", type="integer", nullable=false)
     */
    private $cod_perfil;

    /**
     *
     * @var \Datetime $data_fim
     * @Column(name="data_fim", type="datetime", nullable=false)
     */
    private $data_fim;

    /**
     * @return int
     */
    public function getCodPerfil()
    {
        return $this->cod_perfil;
    }

    /**
     * @param int $cod_perfil
     */
    public function setCodPerfil($cod_perfil)
    {
        $this->cod_perfil = $cod_perfil;
    }

    /**
     * @return int
     */
    public function getCodPessoa()
    {
        return $this->cod_pessoa;
    }

    /**
     * @param int $cod_pessoa
     */
    public function setCodPessoa($cod_pessoa)
    {
        $this->cod_pessoa = $cod_pessoa;
    }

    /**
     * @return \Datetime
     */
    public function getDataInicio()
    {
        return $this->data_inicio;
    }

    /**
     * @param \Datetime $data_inicio
     */
    public function setDataInicio($data_inicio)
    {
        $this->data_inicio = $data_inicio;
    }

    /**
     * @return \Datetime
     */
    public function getDataFim()
    {
        return $this->data_fim;
    }

    /**
     * @param \Datetime $data_fim
     */
    public function setDataFim($data_fim)
    {
        $this->data_fim = $data_fim;
    }
}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_prevenda")
 * @Entity
 * @author Paulo Silva <paulo.silva@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2013-11-06
 */
class Prevenda
{

    /**
     * @Id
     * @var integer $id_prevenda
     * @Column(name="id_prevenda", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_prevenda;

    /**
     * @var date $dt_nascimento
     * @Column(name="dt_nascimento", type="date", nullable=true)
     */
    private $dt_nascimento;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var FormaPagamento $id_situacao
     * @ManyToOne(targetEntity="FormaPagamento")
     * @JoinColumn(name="id_formapagamento", referencedColumnName="id_formapagamento")
     */
    private $id_formapagamento;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=true)
     */

    /**
     *
     * @var MeioPagamento $id_meiopagamento
     * @ManyToOne(targetEntity="MeioPagamento")
     * @JoinColumn(name="id_meiopagamento", referencedColumnName="id_meiopagamento")
     */
    private $id_meiopagamento;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var integer $nu_dddcelular
     * @Column(name="nu_dddcelular", type="integer", nullable=true)
     */
    private $nu_dddcelular;

    /**
     * @var integer $id_paisnascimento
     * @Column(name="id_paisnascimento", type="integer", nullable=true)
     */
    private $id_paisnascimento;

    /**
     * @var integer $nu_lancamentos
     * @Column(name="nu_lancamentos", type="integer", nullable=true)
     */
    private $nu_lancamentos;

    /**
     *
     * @var TipoEndereco $id_tipoendereco
     * @ManyToOne(targetEntity="TipoEndereco")
     * @JoinColumn(name="id_tipoendereco", referencedColumnName="id_tipoendereco")
     */
    private $id_tipoendereco;

    /**
     *
     * @var NivelEnsino $id_nivelensino
     * @ManyToOne(targetEntity="NivelEnsino")
     * @JoinColumn(name="id_nivelensino", referencedColumnName="id_nivelensino")
     */
    private $id_nivelensino;

    /**
     * @var integer $nu_ddicelular
     * @Column(name="nu_ddicelular", type="integer", nullable=true)
     */
    private $nu_ddicelular;

    /**
     * @var integer $nu_dddresidencial
     * @Column(name="nu_dddresidencial", type="integer", nullable=true)
     */
    private $nu_dddresidencial;

    /**
     * @var integer $nu_ddiresidencial
     * @Column(name="nu_ddiresidencial", type="integer", nullable=true)
     */
    private $nu_ddiresidencial;

    /**
     * @var integer $nu_dddcomercial
     * @Column(name="nu_dddcomercial", type="integer", nullable=true)
     */
    private $nu_dddcomercial;

    /**
     * @var integer $nu_ddicomercial
     * @Column(name="nu_ddicomercial", type="integer", nullable=true)
     */
    private $nu_ddicomercial;

    /**
     * @var integer $id_pais
     * @Column(name="id_pais", type="integer", nullable=true)
     */
    private $id_pais;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=true)
     */
    private $id_municipio;

    /**
     * @var integer $id_municipionascimento
     * @Column(name="id_municipionascimento", type="integer")
     */
    private $id_municipionascimento;

    /**
     * @var integer $nu_juros
     * @Column(name="nu_juros", type="integer", nullable=true)
     */
    private $nu_juros;

    /**
     * @var integer $nu_desconto
     * @Column(name="nu_desconto", type="integer", nullable=true)
     */
    private $nu_desconto;

    /**
     * @var integer $nu_valorliquido
     * @Column(name="nu_valorliquido", type="integer", nullable=true)
     */
    private $nu_valorliquido;

    /**
     * @var text $st_nomecompleto
     * @Column(name="st_nomecompleto", type="text", nullable=true)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @var string $nu_telefonecelular
     * @Column(name="nu_telefonecelular", type="string", nullable=true, length=8)
     */
    private $nu_telefonecelular;

    /**
     * @var string $nu_telefoneresidencial
     * @Column(name="nu_telefoneresidencial", type="string", nullable=true, length=8)
     */
    private $nu_telefoneresidencial;

    /**
     * @var string $nu_telefonecomercial
     * @Column(name="nu_telefonecomercial", type="string", nullable=true, length=8)
     */
    private $nu_telefonecomercial;

    /**
     * @var string $st_orgaoexpeditor
     * @Column(name="st_orgaoexpeditor", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpeditor;

    /**
     * @var text $st_instituicao
     * @Column(name="st_instituicao", type="text", nullable=true)
     */
    private $st_instituicao;

    /**
     * @var text $st_graduacao
     * @Column(name="st_graduacao", type="text", nullable=true)
     */
    private $st_graduacao;

    /**
     * @var text $st_localtrabalho
     * @Column(name="st_localtrabalho", type="text", nullable=true)
     */
    private $st_localtrabalho;

    /**
     * @var text $st_recomendacao
     * @Column(name="st_recomendacao", type="text", nullable=true)
     */
    private $st_recomendacao;

    /**
     * @var text $st_endereco
     * @Column(name="st_endereco", type="text", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true, length=255)
     */
    private $st_bairro;

    /**
     * @var text $st_complemento
     * @Column(name="st_complemento", type="text", nullable=true, length=500)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true, length=10)
     */
    private $nu_numero;

    /**
     * @var string $st_rg
     * @Column(name="st_rg", type="string", nullable=true, length=20)
     */
    private $st_rg;

    /**
     * @var text $st_contato
     * @Column(name="st_contato", type="text", nullable=true)
     */
    private $st_contato;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true, length=8)
     */
    private $st_cep;

    /**
     * @var string $st_sexo
     * @Column(name="st_sexo", type="string", nullable=true, length=1)
     */
    private $st_sexo;

    /**
     * @var string $sg_ufnascimento
     * @Column(name="sg_ufnascimento", type="string", nullable=true, length=2)
     */
    private $sg_ufnascimento;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Usuario $id_usuarioatendimento
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioatendimento", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuarioatendimento;

    /**
     * @var Usuario $id_usuariodescarte
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariodescarte", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariodescarte;

    /**
     * @var datetime2 $dt_atendimento
     * @Column(type="datetime2", nullable=true, name="dt_atendimento")
     */
    private $dt_atendimento;

    /**
     * @var datetime2 $dt_descarte
     * @Column(type="datetime2", nullable=true, name="dt_descarte")
     */
    private $dt_descarte;

    public function getId_usuarioatendimento ()
    {
        return $this->id_usuarioatendimento;
    }

    public function setId_usuarioatendimento (Usuario $id_usuarioatendimento)
    {
        $this->id_usuarioatendimento = $id_usuarioatendimento;
        return $this;
    }

    public function getId_usuariodescarte ()
    {
        return $this->id_usuariodescarte;
    }

    public function setId_usuariodescarte (Usuario $id_usuariodescarte)
    {
        $this->id_usuariodescarte = $id_usuariodescarte;
        return $this;
    }

    public function getDt_atendimento ()
    {
        return $this->dt_atendimento;
    }

    public function setDt_atendimento ($dt_atendimento)
    {
        $this->dt_atendimento = $dt_atendimento;
        return $this;
    }

    public function getDt_descarte ()
    {
        return $this->dt_descarte;
    }

    public function setDt_descarte ($dt_descarte)
    {
        $this->dt_descarte = $dt_descarte;
        return $this;
    }

    public function getId_prevenda ()
    {
        return $this->id_prevenda;
    }

    public function setId_prevenda ($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
        return $this;
    }

    public function getDt_nascimento ()
    {
        return $this->dt_nascimento;
    }

    public function setDt_nascimento ($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    public function setId_formapagamento (FormaPagamento $id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    public function getId_meiopagamento ()
    {
        return $this->id_meiopagamento;
    }

    public function setId_meiopagamento (MeioPagamento $id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getNu_dddcelular ()
    {
        return $this->nu_dddcelular;
    }

    public function setNu_dddcelular ($nu_dddcelular)
    {
        $this->nu_dddcelular = $nu_dddcelular;
        return $this;
    }

    public function getId_paisnascimento ()
    {
        return $this->id_paisnascimento;
    }

    public function setId_paisnascimento ($id_paisnascimento)
    {
        $this->id_paisnascimento = $id_paisnascimento;
        return $this;
    }

    public function getNu_lancamentos ()
    {
        return $this->nu_lancamentos;
    }

    public function setNu_lancamentos ($nu_lancamentos)
    {
        $this->nu_lancamentos = $nu_lancamentos;
        return $this;
    }

    /**
     * Get \G2\Entity\Prevenda
     * @return \G2\Entity\Prevenda
     */
    public function getId_tipoendereco ()
    {
        return $this->id_tipoendereco;
    }

    /**
     * Set \G2\Entity\Prevenda
     * @param \G2\Entity\TipoEndereco $id_tipoendereco
     * @return \G2\Entity\Prevenda
     */
    public function setId_tipoendereco (TipoEndereco $id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
        return $this;
    }

    /**
     * \G2\Entity\Prevenda
     * @return \G2\Entity\Prevenda
     */
    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    /**
     * Set \G2\Entity\Prevenda
     * @param \G2\Entity\NivelEnsino $id_nivelensino
     * @return \G2\Entity\Prevenda
     */
    public function setId_nivelensino (NivelEnsino $id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    public function getNu_ddicelular ()
    {
        return $this->nu_ddicelular;
    }

    public function setNu_ddicelular ($nu_ddicelular)
    {
        $this->nu_ddicelular = $nu_ddicelular;
        return $this;
    }

    public function getNu_dddresidencial ()
    {
        return $this->nu_dddresidencial;
    }

    public function setNu_dddresidencial ($nu_dddresidencial)
    {
        $this->nu_dddresidencial = $nu_dddresidencial;
        return $this;
    }

    public function getNu_ddiresidencial ()
    {
        return $this->nu_ddiresidencial;
    }

    public function setNu_ddiresidencial ($nu_ddiresidencial)
    {
        $this->nu_ddiresidencial = $nu_ddiresidencial;
        return $this;
    }

    public function getNu_dddcomercial ()
    {
        return $this->nu_dddcomercial;
    }

    public function setNu_dddcomercial ($nu_dddcomercial)
    {
        $this->nu_dddcomercial = $nu_dddcomercial;
        return $this;
    }

    public function getNu_ddicomercial ()
    {
        return $this->nu_ddicomercial;
    }

    public function setNu_ddicomercial ($nu_ddicomercial)
    {
        $this->nu_ddicomercial = $nu_ddicomercial;
        return $this;
    }

    public function getId_pais ()
    {
        return $this->id_pais;
    }

    public function setId_pais ($id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_municipio ()
    {
        return $this->id_municipio;
    }

    public function setId_municipio ($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getId_municipionascimento ()
    {
        return $this->id_municipionascimento;
    }

    public function setId_municipionascimento ($id_municipionascimento)
    {
        $this->id_municipionascimento = $id_municipionascimento;
        return $this;
    }

    public function getNu_juros ()
    {
        return $this->nu_juros;
    }

    public function setNu_juros ($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    public function getNu_desconto ()
    {
        return $this->nu_desconto;
    }

    public function setNu_desconto ($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    public function getNu_valorliquido ()
    {
        return $this->nu_valorliquido;
    }

    public function setNu_valorliquido ($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_email ()
    {
        return $this->st_email;
    }

    public function setSt_email ($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    public function getNu_telefonecelular ()
    {
        return $this->nu_telefonecelular;
    }

    public function setNu_telefonecelular ($nu_telefonecelular)
    {
        $this->nu_telefonecelular = $nu_telefonecelular;
        return $this;
    }

    public function getNu_telefoneresidencial ()
    {
        return $this->nu_telefoneresidencial;
    }

    public function setNu_telefoneresidencial ($nu_telefoneresidencial)
    {
        $this->nu_telefoneresidencial = $nu_telefoneresidencial;
        return $this;
    }

    public function getNu_telefonecomercial ()
    {
        return $this->nu_telefonecomercial;
    }

    public function setNu_telefonecomercial ($nu_telefonecomercial)
    {
        $this->nu_telefonecomercial = $nu_telefonecomercial;
        return $this;
    }

    public function getSt_orgaoexpeditor ()
    {
        return $this->st_orgaoexpeditor;
    }

    public function setSt_orgaoexpeditor ($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    public function getSt_instituicao ()
    {
        return $this->st_instituicao;
    }

    public function setSt_instituicao ($st_instituicao)
    {
        $this->st_instituicao = $st_instituicao;
        return $this;
    }

    public function getSt_graduacao ()
    {
        return $this->st_graduacao;
    }

    public function setSt_graduacao ($st_graduacao)
    {
        $this->st_graduacao = $st_graduacao;
        return $this;
    }

    public function getSt_localtrabalho ()
    {
        return $this->st_localtrabalho;
    }

    public function setSt_localtrabalho ($st_localtrabalho)
    {
        $this->st_localtrabalho = $st_localtrabalho;
        return $this;
    }

    public function getSt_recomendacao ()
    {
        return $this->st_recomendacao;
    }

    public function setSt_recomendacao ($st_recomendacao)
    {
        $this->st_recomendacao = $st_recomendacao;
        return $this;
    }

    public function getSt_endereco ()
    {
        return $this->st_endereco;
    }

    public function setSt_endereco ($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function getSt_bairro ()
    {
        return $this->st_bairro;
    }

    public function setSt_bairro ($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    public function getSt_complemento ()
    {
        return $this->st_complemento;
    }

    public function setSt_complemento ($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    public function getNu_numero ()
    {
        return $this->nu_numero;
    }

    public function setNu_numero ($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    public function getSt_rg ()
    {
        return $this->st_rg;
    }

    public function setSt_rg ($st_rg)
    {
        $this->st_rg = $st_rg;
        return $this;
    }

    public function getSt_contato ()
    {
        return $this->st_contato;
    }

    public function setSt_contato ($st_contato)
    {
        $this->st_contato = $st_contato;
        return $this;
    }

    public function getSt_cpf ()
    {
        return $this->st_cpf;
    }

    public function setSt_cpf ($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getSt_cep ()
    {
        return $this->st_cep;
    }

    public function setSt_cep ($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    public function getSt_sexo ()
    {
        return $this->st_sexo;
    }

    public function setSt_sexo ($st_sexo)
    {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    public function getSg_ufnascimento ()
    {
        return $this->sg_ufnascimento;
    }

    public function setSg_ufnascimento ($sg_ufnascimento)
    {
        $this->sg_ufnascimento = $sg_ufnascimento;
        return $this;
    }

    /**
     * Get \G2\Entity\Prevenda
     * @return \G2\Entity\Prevenda
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * Set \G2\Entity\Prevenda
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\Prevenda
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

}

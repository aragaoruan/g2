<?php

/*
 * Entity HorarioAula
 * @author: Paulo Silva <paulo.silva@unyleya.com.br>
 * @since: 2013-11-20
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentos")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class Documentos {

    /**
     *
     * @var integer $id_documentos
     * @Column(name="id_documentos", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_documentos;

    /**
     * @var boolean $bl_digitaliza
     * @Column(name="bl_digitaliza", type="boolean", nullable=false)
     */
    private $bl_digitaliza;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_documentos")
     * @var string
     */
    private $st_documentos;

    /**
     * @var TipoDocumento $id_tipodocumento
     * @ManyToOne(targetEntity="TipoDocumento")
     * @JoinColumn(name="id_tipodocumento", referencedColumnName="id_tipodocumento")
     */
    private $id_tipodocumento;

    /**
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio", type="boolean", nullable=false)
     */
    private $bl_obrigatorio;

    /**
     * @var boolean $nu_quantidade
     * @Column(name="nu_quantidade", type="integer", nullable=false)
     */
    private $nu_quantidade;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var boolean $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var DocumentosObrigatoriedade $id_documentosobrigatoriedade
     * @ManyToOne(targetEntity="DocumentosObrigatoriedade")
     * @JoinColumn(name="id_documentosobrigatoriedade", referencedColumnName="id_documentosobrigatoriedade")
     */
    private $id_documentosobrigatoriedade;

    /**
     * @var DocumentosCategoria $id_documentoscategoria
     * @ManyToOne(targetEntity="DocumentosCategoria")
     * @JoinColumn(name="id_documentoscategoria", referencedColumnName="id_documentoscategoria")
     */
    private $id_documentoscategoria;

    /**
     * @var integer $nu_ordenacao
     * @Column(name="nu_ordenacao", type="integer", nullable=true)
     */
    private $nu_ordenacao;


    public function getId_documentos() {
        return $this->id_documentos;
    }

    public function getBl_digitaliza() {
        return $this->bl_digitaliza;
    }

    public function getSt_documentos() {
        return $this->st_documentos;
    }

    public function getId_tipodocumento() {
        return $this->id_tipodocumento;
    }

    public function getBl_obrigatorio() {
        return $this->bl_obrigatorio;
    }

    public function getNu_quantidade() {
        return $this->nu_quantidade;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getId_documentosobrigatoriedade() {
        return $this->id_documentosobrigatoriedade;
    }

    public function getId_documentoscategoria() {
        return $this->id_documentoscategoria;
    }

    public function getNu_ordenacao() {
        return $this->nu_ordenacao;
    }

    public function setId_documentos($id_documentos) {
        $this->id_documentos = $id_documentos;
    }

    public function setBl_digitaliza($bl_digitaliza) {
        $this->bl_digitaliza= $bl_digitaliza;
    }

    public function setSt_documentos($st_documentos) {
        $this->st_documentos = $st_documentos;
    }

    public function setId_tipodocumento($id_tipodocumento) {
        $this->id_tipodocumento= $id_tipodocumento;
    }

    public function setBl_obrigatorio($bl_obrigatorio) {
        $this->bl_obrigatorio= $bl_obrigatorio;
    }

    public function setNu_quantidade($nu_quantidade) {
        $this->nu_quantidade= $nu_quantidade;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao= $id_situacao;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setId_documentosobrigatoriedade($id_documentosobrigatoriedade) {
        $this->id_documentosobrigatoriedade = $id_documentosobrigatoriedade;
    }

    public function setId_documentoscategoria($id_documentoscategoria) {
        $this->id_documentoscategoria = $id_documentoscategoria;
    }

    public function setNu_ordenacao($nu_ordenacao) {
        $this->nu_ordenacao= $nu_ordenacao;
    }
}

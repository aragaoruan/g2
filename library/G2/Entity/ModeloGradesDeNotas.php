<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_modelogradenotas")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class ModeloGradesDeNotas
{

    /**
     * @var integer $id_modelogradenotas
     * @Column(name="id_modelogradenotas", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_modelogradenotas;
    /**
     * @var string $st_modelogradenotas
     * @Column(name="st_modelogradenotas", type="string", nullable=true, length=100)
     */
    private $st_modelogradenotas;

    /**
     * @param int $id_modelogradenotas
     */
    public function setId_modelogradenotas($id_modelogradenotas)
    {
        $this->id_modelogradenotas = $id_modelogradenotas;
    }

    /**
     * @return int
     */
    public function getId_modelogradenotas()
    {
        return $this->id_modelogradenotas;
    }

    /**
     * @param string $st_modelogradenotas
     */
    public function setSt_modelogradenotas($st_modelogradenotas)
    {
        $this->st_modelogradenotas = $st_modelogradenotas;
    }

    /**
     * @return string
     */
    public function getSt_modelogradenotas()
    {
        return $this->st_modelogradenotas;
    }
}
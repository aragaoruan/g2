<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_consultapagamento")
 * @Entity
 * @EntityView
 * @update Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwConsultaPagamento {



    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;

    /**
     * @var integer $id_encerramentosala
     * @Column(name="id_encerramentosala", type="integer")
     */
    private $id_encerramentosala;

    /**
     * @var integer $id_usuarioprofessor
     * @Column(name="id_usuarioprofessor", type="integer")
     */
    private $id_usuarioprofessor;

    /**
     * @var datetime2 $dt_encerramentoprofessor
     * @Column(name="dt_encerramentoprofessor", type="datetime2", nullable=true)
     */
    private $dt_encerramentoprofessor;

    /**
     * @var string $st_encerramentoprofessor
     * @Column(type="string", length=30, nullable=true, name="st_encerramentoprofessor")
     */
    private $st_encerramentoprofessor;

    /**
     * @var integer $id_usuariocoordenador
     * @Column(name="id_usuariocoordenador", type="integer", nullable=true)
     */
    private $id_usuariocoordenador;

    /**
     * @var datetime2 $dt_encerramentocoordenador
     * @Column(name="dt_encerramentocoordenador", type="datetime2", nullable=true)
     */
    private $dt_encerramentocoordenador;

    /**
     * @var string $st_encerramentocoordenador
     * @Column(type="string", length=30, nullable=true, name="st_encerramentocoordenador")
     */
    private $st_encerramentocoordenador;

    /**
     * @var integer $id_usuariopedagogico
     * @Column(name="id_usuariopedagogico", type="integer", nullable=true)
     */
    private $id_usuariopedagogico;


    /**
     * @var datetime2 $dt_encerramentopedagogico
     * @Column(name="dt_encerramentopedagogico", type="datetime2", nullable=true)
     */
    private $dt_encerramentopedagogico;

    /**
     * @var string $st_encerramentopedagogico
     * @Column(type="string", length=30, nullable=true, name="st_encerramentopedagogico")
     */
    private $st_encerramentopedagogico;

    /**
     * @var integer $id_usuariofinanceiro
     * @Column(name="id_usuariofinanceiro", type="integer", nullable=true)
     */
    private $id_usuariofinanceiro;

    /**
     * @var datetime2 $dt_encerramentofinanceiro
     * @Column(name="dt_encerramentofinanceiro", type="datetime2", nullable=true)
     */
    private $dt_encerramentofinanceiro;

    /**
     * @var string $st_encerramentofinanceiro
     * @Column(type="string", length=30, nullable=true, name="st_encerramentofinanceiro")
     */
    private $st_encerramentofinanceiro;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(type="string", length=255, nullable=true, name="st_disciplina")
     */
    private $st_disciplina;

    /**
     * @var string $st_saladeaula
     * @Column(type="string", length=255, name="st_saladeaula")
     */
    private $st_saladeaula;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true)
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_coordenador
     * @Column(name="id_coordenador", type="integer")
     */
    private $id_coordenador;

    /**
     * @var integer $nu_alunos
     * @Column(name="nu_alunos", type="integer", nullable=true)
     */
    private $nu_alunos;

    /**
     * @var string $st_aluno
     * @Column(type="string", length=300, nullable=true, name="st_aluno")
     */
    private $st_aluno;

    /**
     * @var string $st_tituloavaliacao
     * @Column(type="string", length=500, nullable=true, name="st_tituloavaliacao")
     */
    private $st_tituloavaliacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    /**
     * @var string $st_upload
     * @Column(type="string", length=8000, nullable=true, name="st_upload")
     */
    private $st_upload;

    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", length=255, nullable=true, name="st_projetopedagogico")
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_professor
     * @Column(type="string", length=300, nullable=true, name="st_professor")
     */
    private $st_professor;


    /**
     * @var integer $id_aluno
     * @Column(name="id_aluno", type="integer", nullable=true)
     */
    private $id_aluno;

    /**
     * @var float $nu_valorpago
     * @Column(type="float")
     */
    private $nu_valorpago;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true)
     */
    private $nu_cargahoraria;


    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true)
     */
    private $id_categoriasala;

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_encerramentosala()
    {
        return $this->id_encerramentosala;
    }

    public function setId_encerramentosala($id_encerramentosala)
    {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    public function getId_usuarioprofessor()
    {
        return $this->id_usuarioprofessor;
    }

    public function setId_usuarioprofessor($id_usuarioprofessor)
    {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    public function getDt_encerramentoprofessor()
    {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor)
    {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }


    public function getSt_Encerramentoprofessor()
    {
        return $this->st_encerramentoprofessor;
    }

    public function setSt_encerramentoprofessor($st_encerramentoprofessor)
    {
        $this->st_encerramentoprofessor = $st_encerramentoprofessor;
    }

    public function getId_usuariocoordenador()
    {
        return $this->id_usuariocoordenador;
    }

    public function setId_usuariocoordenador($id_usuariocoordenador)
    {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
    }

    public function getDt_encerramentocoordenador()
    {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador)
    {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
    }

    public function getSt_encerramentocoordenador()
    {
        return $this->st_encerramentocoordenador;
    }

    public function setSt_encerramentocoordenador($st_encerramentocoordenador)
    {
        $this->st_encerramentocoordenador = $st_encerramentocoordenador;
    }

    public function getId_usuariopedagogico()
    {
        return $this->id_usuariopedagogico;
    }

    public function setId_usuariopedagogico($id_usuariopedagogico)
    {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
    }

    public function getDt_encerramentopedagogico()
    {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico)
    {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
    }

    public function getSt_encerramentopedagogico()
    {
        return $this->st_encerramentopedagogico;
    }

    public function setSt_encerramentopedagogico($st_encerramentopedagogico)
    {
        $this->st_encerramentopedagogico = $st_encerramentopedagogico;
    }

    public function getId_usuariofinanceiro()
    {
        return $this->id_usuariofinanceiro;
    }

    public function setId_usuariofinanceiro($id_usuariofinanceiro)
    {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentofinanceiro()
    {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro)
    {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    public function getSt_encerramentofinanceiro()
    {
        return $this->st_encerramentofinanceiro;
    }

    public function setSt_encerramentofinanceiro($st_encerramentofinanceiro)
    {
        $this->st_encerramentofinanceiro = $st_encerramentofinanceiro;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    public function getId_coordenador()
    {
        return $this->id_coordenador;
    }

    public function setId_coordenador($id_coordenador)
    {
        $this->id_coordenador = $id_coordenador;
    }
    public function get_nuAlunos()
    {
        return $this->nu_alunos;
    }

    public function set_nuAlunos($nu_alunos)
    {
        $this->nu_alunos = $nu_alunos;
    }

    public function getSt_aluno()
    {
        return $this->st_aluno;
    }

    public function setSt_aluno($st_aluno)
    {
        $this->st_aluno = $st_aluno;
    }

    public function getSt_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    public function setSt_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function getSt_upload()
    {
        return $this->st_upload;
    }

    public function setSt_upload($st_upload)
    {
        $this->st_upload = $st_upload;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function getSt_professor()
    {
        return $this->st_professor;
    }

    public function setSt_professor($st_professor)
    {
        $this->st_professor = $st_professor;
    }

    public function getId_aluno()
    {
        return $this->id_aluno;
    }

    public function setId_aluno($id_aluno)
    {
        $this->id_aluno = $id_aluno;
    }

    public function getNu_valorpago()
    {
        return $this->nu_valorpago;
    }

    public function setNu_valorpago($nu_valorpago)
    {
        $this->nu_valorpago = $nu_valorpago;
    }

    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
    }

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }




}

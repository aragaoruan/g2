<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_aplicadorprovaentidade")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AplicadorProvaEntidade {

    /**
     * @Id
     * @var integer $id_aplicadorprovaentidade
     * @Column(name="id_aplicadorprovaentidade", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_aplicadorprovaentidade;


    /**
     * @var integer $id_aplicadorprova
     * @ManyToOne(targetEntity="AplicadorProva")
     * @JoinColumn(name="id_aplicadorprova", referencedColumnName="id_aplicadorprova")
     */
    private $id_aplicadorprova;


    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade")
     */
    private $id_entidade;

    public function __construct()
    {
        $this->id_aplicadorprova = new \G2\Entity\AplicadorProva();
        $this->id_entidade = new \G2\Entity\Entidade();
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    /**
     * @param int $id_aplicadorprovaentidade
     */
    public function setId_aplicadorprovaentidade($id_aplicadorprovaentidade)
    {
        $this->id_aplicadorprovaentidade = $id_aplicadorprovaentidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprovaentidade()
    {
        return $this->id_aplicadorprovaentidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }






}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_dash_pendentes_moodle")
 * @Entity
 * @EntityView
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-20
 */
class VwDashPendentesMoodle {

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;
    /**
     * @var integer $nu_alunospendentes
     * @Column(name="nu_alunospendentes", type="integer", nullable=true, length=4)
     */
    private $nu_alunospendentes;
    /**
     * @var integer $nu_colaboradorespendentes
     * @Column(name="nu_colaboradorespendentes", type="integer", nullable=true, length=4)
     */
    private $nu_colaboradorespendentes;



    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer nu_alunospendentes
     */
    public function getNu_alunospendentes() {
        return $this->nu_alunospendentes;
    }

    /**
     * @param nu_alunospendentes
     */
    public function setNu_alunospendentes($nu_alunospendentes) {
        $this->nu_alunospendentes = $nu_alunospendentes;
        return $this;
    }

    /**
     * @return integer nu_colaboradorespendentes
     */
    public function getNu_colaboradorespendentes() {
        return $this->nu_colaboradorespendentes;
    }

    /**
     * @param nu_colaboradorespendentes
     */
    public function setNu_colaboradorespendentes($nu_colaboradorespendentes) {
        $this->nu_colaboradorespendentes = $nu_colaboradorespendentes;
        return $this;
    }


}

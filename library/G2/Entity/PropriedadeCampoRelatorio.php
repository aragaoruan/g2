<?php

/*
 * Entity PropriedadeCampoRelatorio
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-14
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_propriedadecamporelatorio")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @EntityLog
 */
class PropriedadeCampoRelatorio {

    /**
     * @var integer $id_camporelatorio
     * @Column(name="id_camporelatorio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_camporelatorio;

    /**
     *
     * @var integer $id_tipopropriedadecamporel
     * @Column(name="id_tipopropriedadecamporel", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_tipopropriedadecamporel;


    /**
	 * @var string $st_valor
	 * @Column(name="st_valor", type="string", nullable=false, length=255)
	 */
	private $st_valor;


    public function getId_camporelatorio() {
        return $this->id_camporelatorio;
    }

    public function getId_tipopropriedadecamporel() {
            return $this->id_tipopropriedadecamporel;
        }

	public function getSt_valor() {
		return $this->st_valor;
	}

    public function setId_camporelatorio($id_camporelatorio) {
        $this->id_camporelatorio = $id_camporelatorio;
    }

    public function setId_tipopropriedadecamporel($id_tipopropriedadecamporel) {
        $this->id_tipopropriedadecamporel = $id_tipopropriedadecamporel;
    }

	public function setSt_valor($st_valor) {
        $this->st_valor = $st_valor;
    }

}

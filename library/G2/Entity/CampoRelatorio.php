<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_camporelatorio")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-12
 * @EntityLog
 */
class CampoRelatorio {

    /**
     *
     * @var integer $id_camporelatorio
     * @Column(name="id_camporelatorio", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_camporelatorio;


    /**
     * @var integer $id_relatorio
     * @Column(name="id_relatorio", type="integer", nullable=true)
     */
    private $id_relatorio;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_camporelatorio")
     * @var string
     */
    private $st_camporelatorio;

    /**
     * @Column(type="string",length=30,nullable=false, name="st_titulocampo")
     * @var string
     */
    private $st_titulocampo;

    /**
     * @var boolean $bl_filtro
     * @Column(name="bl_filtro", type="boolean", nullable=false)
     */
    private $bl_filtro;

    /**
     * @var boolean $bl_exibido
     * @Column(name="bl_exibido", type="boolean", nullable=false)
     */
    private $bl_exibido;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @var boolean $bl_obrigatorio
     * @Column(name="bl_obrigatorio", type="boolean", nullable=false)
     */
    private $bl_obrigatorio;

    /**
     * @return boolean
     */
    public function isBlExibido()
    {
        return $this->bl_exibido;
    }

    /**
     * @param boolean $bl_exibido
     */
    public function setBl_exibido($bl_exibido)
    {
        $this->bl_exibido = $bl_exibido;
    }

    /**
     * @return boolean
     */
    public function isBlFiltro()
    {
        return $this->bl_filtro;
    }

    /**
     * @param boolean $bl_filtro
     */
    public function setBl_filtro($bl_filtro)
    {
        $this->bl_filtro = $bl_filtro;
    }

    /**
     * @return boolean
     */
    public function isBlObrigatorio()
    {
        return $this->bl_obrigatorio;
    }

    /**
     * @param boolean $bl_obrigatorio
     */
    public function setBl_obrigatorio($bl_obrigatorio)
    {
        $this->bl_obrigatorio = $bl_obrigatorio;
    }

    /**
     * @return mixed
     */
    public function getId_camporelatorio()
    {
        return $this->id_camporelatorio;
    }

    /**
     * @param mixed $id_camporelatorio
     */
    public function setId_camporelatorio($id_camporelatorio)
    {
        $this->id_camporelatorio = $id_camporelatorio;
    }

    /**
     * @return Relatorio
     */
    public function getId_relatorio()
    {
        return $this->id_relatorio;
    }

    /**
     * @param Relatorio $id_relatorio
     */
    public function setId_relatorio($id_relatorio)
    {
        $this->id_relatorio = $id_relatorio;
    }

    /**
     * @return int
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param int $nu_ordem
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    /**
     * @return string
     */
    public function getSt_camporelatorio()
    {
        return $this->st_camporelatorio;
    }

    /**
     * @param string $st_camporelatorio
     */
    public function setSt_camporelatorio($st_camporelatorio)
    {
        $this->st_camporelatorio = $st_camporelatorio;
    }

    /**
     * @return mixed
     */
    public function getSt_titulocampo()
    {
        return $this->st_titulocampo;
    }

    /**
     * @param mixed $st_titulocampo
     */
    public function setSt_titulocampo($st_titulocampo)
    {
        $this->st_titulocampo = $st_titulocampo;
    }





}

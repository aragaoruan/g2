<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwPessoa"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pessoa")
 * @Entity
 * @EntityView
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class VwPessoa extends G2Entity
{

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @column(name="id_entidade", nullable=false, type="integer")
     * @Id
     */
    private $id_entidade;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @column(name="id_usuario", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @SWG\Property(format="string")
     * @var string $st_email
     * @column(name="st_email", nullable=false, type="string", length=255)
     */
    private $st_email;

    /**
     * @SWG\Property(format="string")
     * @var string $st_login
     * @column(name="st_login", nullable=false, type="string", length=255)
     */
    private $st_login;

    /**
     * @SWG\Property(format="string")
     * @var string $st_loginentidade
     * @column(name="st_loginentidade", nullable=false, type="string", length=255)
     */
    private $st_loginentidade;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomecompleto
     * @column(name="st_nomecompleto", nullable=false, type="string", length=255)
     */
    private $st_nomecompleto;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cpf
     * @column(name="st_cpf", nullable=true, type="string", length=255)
     */
    private $st_cpf;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_registropessoa
     * @column(name="id_registropessoa", nullable=true, type="integer")
     */
    private $id_registropessoa;

    /**
     * @SWG\Property(format="string")
     * @var string $st_senha
     * @column(name="st_senha", nullable=false, type="string", length=255)
     */
    private $st_senha;

    /**
     * @SWG\Property(format="string")
     * @var string $st_senhaentidade
     * @column(name="st_senhaentidade", nullable=true, type="string", length=255)
     */
    private $st_senhaentidade;

    /**
     * @SWG\Property(format="string")
     * @var string $st_sexo
     * @column(name="st_sexo", nullable=true, type="string", length=255)
     */
    private $st_sexo;

    /**
     * @SWG\Property(format="date")
     * @var date $dt_nascimento
     * @Column(type="date", nullable=true, name="dt_nascimento")
     */
    private $dt_nascimento;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_emailpadrao
     * @column(name="bl_emailpadrao", nullable=false, type="boolean")
     */
    private $bl_emailpadrao;

    /**
     * @SWG\Property(format="interger")
     * @var string $id_telefone
     * @column(name="id_telefone", nullable=true, type="integer")
     */
    private $id_telefone;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_telefonepadrao
     * @column(name="bl_telefonepadrao", nullable=false, type="boolean")
     */
    private $bl_telefonepadrao;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_telefone
     * @column(name="nu_telefone", nullable=true, type="string", length=15)
     */
    private $nu_telefone;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_tipotelefone
     * @column(name="id_tipotelefone", nullable=true, type="integer")
     */
    private $id_tipotelefone;

    /**
     * @SWG\Property(format="string")
     * @var string $st_tipotelefone
     * @column(name="st_tipotelefone", nullable=true, type="string", length=255)
     */
    private $st_tipotelefone;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_ddd
     * @column(name="nu_ddd", nullable=false, type="string")
     */
    private $nu_ddd;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_ddi
     * @column(name="nu_ddi", nullable=false, type="string")
     */
    private $nu_ddi;

    /**
     * @SWG\Property(format="string")
     * @var string $st_rg
     * @column(name="st_rg", nullable=true, type="string", length=255)
     */
    private $st_rg;

    /**
     * @SWG\Property(format="string")
     * @var string $st_orgaoexpeditor
     * @column(name="st_orgaoexpeditor", nullable=true, type="string", length=255)
     */
    private $st_orgaoexpeditor;

    /**
     * @SWG\Property(format="date")
     * @var date $dt_dataexpedicao
     * @Column(type="date", nullable=true, name="dt_dataexpedicao")
     */
    private $dt_dataexpedicao;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_endereco
     * @column(name="id_endereco", nullable=true, type="integer")
     */
    private $id_endereco;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_enderecopadrao
     * @column(name="bl_enderecopadrao", nullable=false, type="boolean")
     */
    private $bl_enderecopadrao;

    /**
     * @SWG\Property(format="string")
     * @var string $st_endereco
     * @column(name="st_endereco", nullable=true, type="string", length=255)
     */
    private $st_endereco;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cep
     * @column(name="st_cep", nullable=true, type="string", length=255)
     */
    private $st_cep;

    /**
     * @SWG\Property(format="string")
     * @var string $st_bairro
     * @column(name="st_bairro", nullable=true, type="string", length=255)
     */
    private $st_bairro;

    /**
     * @SWG\Property(format="string")
     * @var string $st_complemento
     * @column(name="st_complemento", nullable=true, type="string", length=255)
     */
    private $st_complemento;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_numero
     * @column(name="nu_numero", nullable=true, type="string", length=15)
     */
    private $nu_numero;

    /**
     * @SWG\Property(format="string")
     * @var string $st_estadoprovincia
     * @column(name="st_estadoprovincia", nullable=true, type="string", length=255)
     */
    private $st_estadoprovincia;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cidade
     * @column(name="st_cidade", nullable=true, type="string", length=255)
     */
    private $st_cidade;

    /**
     * @SWG\Property(format="string")
     * @var string $st_cidadenascimento
     * @column(name="st_cidadenascimento", nullable=true, type="string", length=255)
     */
    private $st_cidadenascimento;

    /**
     * @SWG\Property(format="string")
     * @var string $id_pais
     * @column(name="id_pais", nullable=true, type="integer")
     */
    private $id_pais;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_municipio
     * @column(name="id_municipio", nullable=true, type="integer")
     */
    private $id_municipio;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_municipionascimento
     * @column(name="id_municipionascimento", nullable=true, type="integer")
     */
    private $id_municipionascimento;

    /**
     * @SWG\Property(format="string")
     * @var string $st_municipionascimento
     * @column(name="st_municipionascimento", nullable=true, type="string")
     */
    private $st_municipionascimento;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_tipoendereco
     * @column(name="id_tipoendereco", nullable=true, type="integer")
     */
    private $id_tipoendereco;

    /**
     * @SWG\Property(format="string")
     * @var string $sg_uf
     * @column(name="sg_uf", nullable=true, type="string", length=255)
     */
    private $sg_uf;

    /**
     * @SWG\Property(format="string")
     * @var string $sg_ufnascimento
     * @column(name="sg_ufnascimento", nullable=true, type="string", length=255)
     */
    private $sg_ufnascimento;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_estatocivil
     * @column(name="id_estadocivil", nullable=true, type="integer")
     */
    private $id_estadocivil;

    /**
     * @SWG\Property(format="string")
     * @var string $st_estadocivil
     * @column(name="st_estadocivil", nullable=true, type="string", length=255)
     */
    private $st_estadocivil;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomepais
     * @column(name="st_nomepais", nullable=true, type="string", length=255)
     */
    private $st_nomepais;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomemunicipio
     * @column(name="st_nomemunicipio", nullable=true, type="string", length=255)
     */
    private $st_nomemunicipio;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomemae
     * @column(name="st_nomemae", nullable=true, type="string", length=255)
     */
    private $st_nomemae;

    /**
     * @SWG\Property(format="string")
     * @var string $st_nomepai
     * @column(name="st_nomepai", nullable=true, type="string", length=255)
     */
    private $st_nomepai;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_telefonealternativo
     * @column(name="id_telefonealternativo", nullable=true, type="integer")
     */
    private $id_telefonealternativo;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_telefonealternativo
     * @column(name="nu_telefonealternativo", nullable=true, type="string", length=15)
     */
    private $nu_telefonealternativo;

    /**
     * @SWG\Property(format="integer")
     * @var string $id_tipotelefonealternativo
     * @column(name="id_tipotelefonealternativo", nullable=true, type="integer")
     */
    private $id_tipotelefonealternativo;

    /**
     * @SWG\Property(format="string")
     * @var string $st_tipotelefonealternativo
     * @column(name="st_tipotelefonealternativo", nullable=true, type="string", length=255)
     */
    private $st_tipotelefonealternativo;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_dddalternativo
     * @column(name="nu_dddalternativo", nullable=false, type="string")
     */
    private $nu_dddalternativo;

    /**
     * @SWG\Property(format="string")
     * @var string $nu_ddialternativo
     * @column(name="nu_ddialternativo", nullable=false, type="string")
     */
    private $nu_ddialternativo;

    /**
     * @SWG\Property(format="string")
     * @var string $st_identificacao
     * @Column(name="st_identificacao", nullable=true, type="string")
     */
    private $st_identificacao;

    /**
     * @SWG\Property(format="string")
     * @var string $sst_urlavatar
     * @Column(name="st_urlavatar", nullable=true, type="string")
     */
    private $st_urlavatar;

    /**
     * @SWG\Property(format="string")
     * @var string $id_paisnascimento
     * @Column(name="id_paisnascimento", nullable=true, type="integer")
     */
    private $id_paisnascimento;

    /**
     * @param boolean $bl_emailpadrao
     */
    public function setbl_emailpadrao($bl_emailpadrao)
    {
        $this->bl_emailpadrao = $bl_emailpadrao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getbl_emailpadrao()
    {
        return $this->bl_emailpadrao;
    }

    /**
     * @param boolean $bl_enderecopadrao
     */
    public function setbl_enderecopadrao($bl_enderecopadrao)
    {
        $this->bl_enderecopadrao = $bl_enderecopadrao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getbl_enderecopadrao()
    {
        return $this->bl_enderecopadrao;
    }

    /**
     * @param boolean $bl_telefonepadrao
     */
    public function setbl_telefonepadrao($bl_telefonepadrao)
    {
        $this->bl_telefonepadrao = $bl_telefonepadrao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getbl_telefonepadrao()
    {
        return $this->bl_telefonepadrao;
    }

    /**
     * @param \G2\Entity\date $dt_dataexpedicao
     */
    public function setdt_dataexpedicao($dt_dataexpedicao)
    {
        $this->dt_dataexpedicao = $dt_dataexpedicao;
        return $this;
    }

    /**
     * @return \G2\Entity\date
     */
    public function getdt_dataexpedicao()
    {
        return $this->dt_dataexpedicao;
    }

    /**
     * @param \G2\Entity\date $dt_nascimento
     */
    public function setdt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    /**
     * @return \G2\Entity\date
     */
    public function getdt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @param string $id_endereco
     */
    public function setid_endereco($id_endereco)
    {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_endereco()
    {
        return $this->id_endereco;
    }

    /**
     * @param int $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param string $id_estadocivil
     */
    public function setid_estadocivil($id_estadocivil)
    {
        $this->id_estadocivil = $id_estadocivil;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_estadocivil()
    {
        return $this->id_estadocivil;
    }

    /**
     * @param string $id_municipio
     */
    public function setid_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_municipio()
    {
        return $this->id_municipio;
    }

    /**
     * @param string $id_municipionascimento
     */
    public function setid_municipionascimento($id_municipionascimento)
    {
        $this->id_municipionascimento = $id_municipionascimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_municipionascimento()
    {
        return $this->id_municipionascimento;
    }

    /**
     * @param string $id_pais
     */
    public function setid_pais($id_pais)
    {
        $this->id_pais = $id_pais;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_pais()
    {
        return $this->id_pais;
    }

    /**
     * @param string $id_registropessoa
     */
    public function setid_registropessoa($id_registropessoa)
    {
        $this->id_registropessoa = $id_registropessoa;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_registropessoa()
    {
        return $this->id_registropessoa;
    }

    /**
     * @param string $id_telefone
     */
    public function setid_telefone($id_telefone)
    {
        $this->id_telefone = $id_telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_telefone()
    {
        return $this->id_telefone;
    }

    /**
     * @param string $id_telefonealternativo
     */
    public function setid_telefonealternativo($id_telefonealternativo)
    {
        $this->id_telefonealternativo = $id_telefonealternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_telefonealternativo()
    {
        return $this->id_telefonealternativo;
    }

    /**
     * @param string $id_tipoendereco
     */
    public function setid_tipoendereco($id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_tipoendereco()
    {
        return $this->id_tipoendereco;
    }

    /**
     * @param string $id_tipotelefone
     */
    public function setid_tipotelefone($id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_tipotelefone()
    {
        return $this->id_tipotelefone;
    }

    /**
     * @param string $id_tipotelefonealternativo
     */
    public function setid_tipotelefonealternativo($id_tipotelefonealternativo)
    {
        $this->id_tipotelefonealternativo = $id_tipotelefonealternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getid_tipotelefonealternativo()
    {
        return $this->id_tipotelefonealternativo;
    }

    /**
     * @param int $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param string $nu_ddd
     */
    public function setnu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_ddd()
    {
        return $this->nu_ddd;
    }

    /**
     * @param string $nu_dddalternativo
     */
    public function setnu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    /**
     * @param string $nu_ddi
     */
    public function setnu_ddi($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_ddi()
    {
        return $this->nu_ddi;
    }

    /**
     * @param string $nu_ddialternativo
     */
    public function setnu_ddialternativo($nu_ddialternativo)
    {
        $this->nu_ddialternativo = $nu_ddialternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_ddialternativo()
    {
        return $this->nu_ddialternativo;
    }

    /**
     * @param string $nu_numero
     */
    public function setnu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param string $nu_telefone
     */
    public function setnu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_telefone()
    {
        return $this->nu_telefone;
    }

    /**
     * @param string $nu_telefonealternativo
     */
    public function setnu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    /**
     * @param string $sg_uf
     */
    public function setsg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return string
     */
    public function getsg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_ufnascimento
     */
    public function setsg_ufnascimento($sg_ufnascimento)
    {
        $this->sg_ufnascimento = $sg_ufnascimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getsg_ufnascimento()
    {
        return $this->sg_ufnascimento;
    }

    /**
     * @param string $st_bairro
     */
    public function setst_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param string $st_cep
     */
    public function setst_cep($st_cep)
    {
        $this->st_cep = $st_cep;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cep()
    {
        return $this->st_cep;
    }

    /**
     * @param string $st_cidade
     */
    public function setst_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param string $st_cidadenascimento
     */
    public function setst_cidadenascimento($st_cidadenascimento)
    {
        $this->st_cidadenascimento = $st_cidadenascimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cidadenascimento()
    {
        return $this->st_cidadenascimento;
    }

    /**
     * @param string $st_complemento
     */
    public function setst_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param string $st_cpf
     */
    public function setst_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_email
     */
    public function setst_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_endereco
     */
    public function setst_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_estadocivil
     */
    public function setst_estadocivil($st_estadocivil)
    {
        $this->st_estadocivil = $st_estadocivil;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_estadocivil()
    {
        return $this->st_estadocivil;
    }

    /**
     * @param string $st_estadoprovincia
     */
    public function setst_estadoprovincia($st_estadoprovincia)
    {
        $this->st_estadoprovincia = $st_estadoprovincia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_estadoprovincia()
    {
        return $this->st_estadoprovincia;
    }

    /**
     * @param string $st_login
     */
    public function setst_login($st_login)
    {
        $this->st_login = $st_login;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_login()
    {
        return $this->st_login;
    }

    /**
     * @param string $st_loginentidade
     */
    public function setst_loginentidade($st_loginentidade)
    {
        $this->st_loginentidade = $st_loginentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_loginentidade()
    {
        return $this->st_loginentidade;
    }

    /**
     * @param string $st_nomecompleto
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomemae
     */
    public function setst_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomemae()
    {
        return $this->st_nomemae;
    }

    /**
     * @param string $st_nomemunicipio
     */
    public function setst_nomemunicipio($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomemunicipio()
    {
        return $this->st_nomemunicipio;
    }

    /**
     * @param string $st_nomepai
     */
    public function setst_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomepai()
    {
        return $this->st_nomepai;
    }

    /**
     * @param string $st_nomepais
     */
    public function setst_nomepais($st_nomepais)
    {
        $this->st_nomepais = $st_nomepais;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomepais()
    {
        return $this->st_nomepais;
    }

    /**
     * @param string $st_orgaoexpeditor
     */
    public function setst_orgaoexpeditor($st_orgaoexpeditor)
    {
        $this->st_orgaoexpeditor = $st_orgaoexpeditor;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_orgaoexpeditor()
    {
        return $this->st_orgaoexpeditor;
    }

    /**
     * @param string $st_rg
     */
    public function setst_rg($st_rg)
    {
        $this->st_rg = $st_rg;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_rg()
    {
        return $this->st_rg;
    }

    /**
     * @param string $st_senha
     */
    public function setst_senha($st_senha)
    {
        $this->st_senha = $st_senha;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_senha()
    {
        return $this->st_senha;
    }

    /**
     * @param string $st_senhaentidade
     */
    public function setst_senhaentidade($st_senhaentidade)
    {
        $this->st_senhaentidade = $st_senhaentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_senhaentidade()
    {
        return $this->st_senhaentidade;
    }

    /**
     * @param string $st_sexo
     */
    public function setst_sexo($st_sexo)
    {
        $this->st_sexo = $st_sexo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_sexo()
    {
        return $this->st_sexo;
    }

    /**
     * @param string $st_tipotelefone
     */
    public function setst_tipotelefone($st_tipotelefone)
    {
        $this->st_tipotelefone = $st_tipotelefone;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tipotelefone()
    {
        return $this->st_tipotelefone;
    }

    /**
     * @param string $st_tipotelefonealternativo
     */
    public function setst_tipotelefonealternativo($st_tipotelefonealternativo)
    {
        $this->st_tipotelefonealternativo = $st_tipotelefonealternativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tipotelefonealternativo()
    {
        return $this->st_tipotelefonealternativo;
    }


    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }

    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
        return $this;
    }

    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_municipionascimento()
    {
        return $this->st_municipionascimento;
    }

    /**
     * @param string $st_municipionascimento
     */
    public function setSt_municipionascimento($st_municipionascimento)
    {
        $this->st_municipionascimento = $st_municipionascimento;
    }

    /**
     * @return string
     */
    public function getId_paisnascimento()
    {
        return $this->id_paisnascimento;
    }

    /**
     * @param string $id_paisnascimento
     */
    public function setId_paisnascimento($id_paisnascimento)
    {
        $this->id_paisnascimento = $id_paisnascimento;
    }



}

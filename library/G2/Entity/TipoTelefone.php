<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipotelefone")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoTelefone
{

    /**
     * @var integer $id_tipotelefone
     * @Column(type="integer", nullable=false, name="id_tipotelefone")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipotelefone;

    /**
     *
     * @var string $st_tipotelefone
     * @Column(type="string", length=255, nullable=false, name="st_tipotelefone")
     */
    private $st_tipotelefone;

    public function getId_tipotelefone ()
    {
        return $this->id_tipotelefone;
    }

    public function setId_tipotelefone ($id_tipotelefone)
    {
        $this->id_tipotelefone = $id_tipotelefone;
        return $this;
    }

    public function getSt_tipotelefone ()
    {
        return $this->st_tipotelefone;
    }

    public function setSt_tipotelefone ($st_tipotelefone)
    {
        $this->st_tipotelefone = $st_tipotelefone;
        return $this;
    }

}
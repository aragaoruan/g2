<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_categoriacampanha")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CategoriaCampanha
{

	const CUPOM 	= 1; //	Cupom
	const PROMOCAO 	= 2; //	Promoção
	const PRAZO 	= 3; //	Prazo
	const PREMIO = 4;
    /**
     * @var integer $id_categoriacampanha
     * @Column(type="integer", nullable=false, name="id_categoriacampanha")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_categoriacampanha;

    /**
     *
     * @var string $st_categoriacampanha
     * @Column(type="string", length=50,nullable=false, name="st_categoriacampanha")
     */
    private $st_categoriacampanha;

    /**
     * @return integer id_categoriacampanha
     */
    public function getId_categoriacampanha ()
    {
        return $this->id_categoriacampanha;
    }

    /**
     * @return string st_categoriacampanha
     */
    public function getSt_categoriacampanha ()
    {
        return $this->st_categoriacampanha;
    }

    /**
     * @param integer $id_categoriacampanha
     * @return \G2\Entity\CategoriaCampanha
     */
    public function setId_categoriacampanha ($id_categoriacampanha)
    {
        $this->id_categoriacampanha = $id_categoriacampanha;
        return $this;
    }

    /**
     * @param string $st_categoriacampanha
     * @return \G2\Entity\CategoriaCampanha
     */
    public function setSt_categoriacampanha ($st_categoriacampanha)
    {
        $this->st_categoriacampanha = $st_categoriacampanha;
        return $this;
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/11/13
 * Time: 09:56
 */

namespace G2\Entity;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_meiopagamentointegracao")
 */
class MeioPagamentoIntegracao {

    /**
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=true)
     */
    private $id_cartaobandeira;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false)
     */
    private $id_sistema;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=false)
     */
    private $id_meiopagamento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", length=30, nullable=false)
     */
    private $st_codsistema;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string $st_codcontacaixa
     * @Column(name="st_codcontacaixa", type="string", nullable=true)
     */
    private $st_codcontacaixa;

    /**
     * @var integer $id_meiopagamentointegracao
     * @Column(name="id_meiopagamentointegracao", type="string", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_meiopagamentointegracao;

    /**
     * @param \G2\Entity\datetime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param int $id_cartaobandeira
     */
    public function setId_cartaobandeira($id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
    }

    /**
     * @return int
     */
    public function getId_cartaobandeira()
    {
        return $this->id_cartaobandeira;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return int
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param int $id_meiopagamentointegracao
     */
    public function setId_meiopagamentointegracao($id_meiopagamentointegracao)
    {
        $this->id_meiopagamentointegracao = $id_meiopagamentointegracao;
    }

    /**
     * @return int
     */
    public function getId_meiopagamentointegracao()
    {
        return $this->id_meiopagamentointegracao;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param string $st_codcontacaixa
     */
    public function setSt_codcontacaixa($st_codcontacaixa)
    {
        $this->st_codcontacaixa = $st_codcontacaixa;
    }

    /**
     * @return string
     */
    public function getSt_codcontacaixa()
    {
        return $this->st_codcontacaixa;
    }

    /**
     * @param string $st_codsistema
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
    }

    /**
     * @return string
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

}
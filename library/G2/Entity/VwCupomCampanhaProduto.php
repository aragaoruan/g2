<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\Cupom")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_cupomcampanhaproduto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-21
 */
class VwCupomCampanhaProduto
{

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false, name="id_produto")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produto;

    /**
     * @var integer $id_cupom
     * @Column(type="integer", nullable=false, name="id_cupom") 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_cupom;

    /**
     * @var string $st_produto
     * @Column(type="string", length=250, nullable=false, name="st_produto") 
     */
    private $st_produto;

    /**
     * @var string $st_codigocupom
     * @Column(type="string", length=22, nullable=false, name="st_codigocupom")
     */
    private $st_codigocupom;

    /**
     * @var datetime2 $dt_inicio
     * @Column(type="datetime2", nullable=false, name="dt_inicio") 
     */
    private $dt_inicio;

    /**
     * @var datetime2 $dt_fim
     * @Column(type="datetime2", nullable=false, name="dt_fim") 
     */
    private $dt_fim;

    /**
     * @var integer $id_campanhacomercial
     * @Column(type="integer", nullable=false, name="id_campanhacomercial") 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     * @Column(type="string", nullable=false, length=255, name="st_campanhacomercial") 
     */
    private $st_campanhacomercial;

    /**
     * @var integer $id_tipodesconto
     * @Column(type="integer", nullable=false, name="id_tipodesconto") 
     */
    private $id_tipodesconto;

    /**
     * @var integer $st_tipodesconto
     * @Column(type="string", nullable=false, length=255, name="st_tipodesconto") 
     */
    private $st_tipodesconto;

    /**
     * @var decimal $nu_desconto
     * @Column(type="decimal", nullable=false, name="nu_desconto") 
     */
    private $nu_desconto;

    /**
     * @var decimal $nu_valor
     * @Column(type="decimal", nullable=true, name="nu_valor") 
     */
    private $nu_valor;

    /**
     * @var decimal $nu_valorliquido
     * @Column(type="decimal", nullable=true, name="nu_valorliquido") 
     */
    private $nu_valorliquido;

    /**
     * @return string
     */
    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    /**
     * @param string $st_produto
     * @return \G2\Entity\VwCupomCampanhaProduto
     */
    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * @return integer
     */
    public function getId_cupom ()
    {
        return $this->id_cupom;
    }

    /**
     * @return string
     */
    public function getSt_codigocupom ()
    {
        return $this->st_codigocupom;
    }

    /**
     * @return decimal
     */
    public function getNu_desconto ()
    {
        return $this->nu_desconto;
    }

    /**
     * @return datetime2
     */
    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    /**
     * @return datetime2
     */
    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    /**
     * @return integer
     */
    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return string
     */
    public function getSt_campanhacomercial ()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @return integer
     */
    public function getId_tipodesconto ()
    {
        return $this->id_tipodesconto;
    }

    /**
     * @return string
     */
    public function getSt_tipodesconto ()
    {
        return $this->st_tipodesconto;
    }

    /**
     * @param integer $id_produto
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @param integer $id_cupom
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setId_cupom ($id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @param string $st_codigocupom
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setSt_codigocupom ($st_codigocupom)
    {
        $this->st_codigocupom = $st_codigocupom;
        return $this;
    }

    /**
     * @param decimal $nu_desconto
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setNu_desconto ($nu_desconto)
    {
        $this->nu_desconto = $nu_desconto;
        return $this;
    }

    /**
     * @param datetime2 $dt_inicio
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @param datetime2 $dt_fim
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @param integer $id_campanhacomercial
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    /**
     * @param string $st_campanhacomercial
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setSt_campanhacomercial ($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    /**
     * @param integer $id_tipodesconto
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setId_tipodesconto ($id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;
    }

    /**
     * @param string $st_tipodesconto
     * @return \G2\Negocio\VwCupomCampanhaProduto
     */
    public function setSt_tipodesconto ($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;
    }

    /**
     * @return decimal $nu_valorliquido
     */
    public function getNu_valorliquido ()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @return decimal $nu_valor
     */
    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    /**
     * @param decimal $nu_valorliquido
     * @return \G2\Entity\VwCupomCampanhaProduto
     */
    public function setNu_valorliquido ($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    /**
     * @param decimal $nu_valor
     * @return \G2\Entity\VwCupomCampanhaProduto
     */
    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

}

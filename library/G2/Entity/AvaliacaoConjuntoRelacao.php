<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoconjuntorelacao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvaliacaoConjuntoRelacao {


    /**
     * @var integer $id_avaliacaoconjuntorelacao
     * @Column(name="id_avaliacaoconjuntorelacao", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_avaliacaoconjuntorelacao;
    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false, length=4)
     */
    private $id_avaliacao;
    /**
     * @var integer $id_avaliacaorecupera
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaorecupera;
    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjunto;


    public function setId_Avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }


    public function getId_Avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function setId_Avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

      public function getId_Avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    public function setId_Avaliacaoconjuntorelacao($id_avaliacaoconjuntorelacao)
    {
        $this->id_avaliacaoconjuntorelacao = $id_avaliacaoconjuntorelacao;
        return $this;
    }

    public function getId_Avaliacaoconjuntorelacao()
    {
        return $this->id_avaliacaoconjuntorelacao;
    }

    public function setId_Avaliacaorecupera($id_avaliacaorecupera)
    {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
        return $this;
    }

    public function getId_Avaliacaorecupera()
    {
        return $this->id_avaliacaorecupera;
    }


}

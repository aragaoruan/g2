<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_perfisentidadenotificacao")
 * @Entity(repositoryClass="\G2\Repository\Perfil")
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */

class VwPerfisEntidadeNotificacao
{
    /**
     * @var integer $id_notificacao
     * @Column(name="id_notificacao", type="integer", nullable=true, length=4)
     */
    private $id_notificacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_percentuallimitealunosala
     * @Column(name="nu_percentuallimitealunosala", type="integer", nullable=true, length=4)
     */
    private $nu_percentuallimitealunosala;

    /**
     * @Id
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     */
    private $id_perfil;

    /**
     * @var boolean $bl_enviaparaemail
     * @Column(name="bl_enviaparaemail", type="boolean", nullable=true, length=1)
     */
    private $bl_enviaparaemail;

    /**
     * @var string $st_notificacao
     * @Column(name="st_notificacao", type="string", nullable=true, length=50)
     */
    private $st_notificacao;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=100)
     */
    private $st_nomeperfil;

    /**
     * @var string $id_notificacaoentidade
     * @Column(name="id_notificacaoentidade", type="integer", nullable=false, length=100)
     */
    private $id_notificacaoentidade;

    /**
     * @var string $id_notificacaoentidadeperfil
     * @Column(name="id_notificacaoentidadeperfil", type="integer", nullable=false, length=100)
     */
    private $id_notificacaoentidadeperfil;

    /**
     * @return integer id_notificacao
     */
    public function getId_notificacao() {
        return $this->id_notificacao;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return integer nu_percentuallimitealunosala
     */
    public function getNu_percentuallimitealunosala() {
        return $this->nu_percentuallimitealunosala;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil() {
        return $this->id_perfil;
    }

    /**
     * @return boolean bl_enviaparaemail
     */
    public function getBl_enviaparaemail() {
        return $this->bl_enviaparaemail;
    }

    /**
     * @return string st_notificacao
     */
    public function getSt_notificacao() {
        return $this->st_notificacao;
    }

    /**
     * @return string st_nomeperfil
     */
    public function getSt_nomeperfil() {
        return $this->st_nomeperfil;
    }

    /**
     * @return string id_notificacaoentidade
     */
    public function getId_notificacaoentidade() {
        return $this->id_notificacaoentidade;
    }

    /**
     * @return string id_notificacaoentidadeperfil
     */
    public function getId_notificacaoentidadeperfil() {
        return $this->id_notificacaoentidadeperfil;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_municipio")
 * @Entity(repositoryClass="\G2\Repository\Municipio")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Municipio
{

    /**
     *
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_municipio;

    /**
     *
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=255)
     */
    private $st_nomemunicipio;

    /**
     *
     * @var decimal $nu_codigoibge
     * @Column(name="nu_codigoibge", type="decimal", nullable=true, length=255)
     */
    private $nu_codigoibge;

    /**
     *
     * @var Uf $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", nullable=true, referencedColumnName="sg_uf")
     */
    private $sg_uf;

    public function getId_municipio()
    {
        return $this->id_municipio;
    }

    public function setId_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getSt_nomemunicipio()
    {
        return $this->st_nomemunicipio;
    }

    public function setSt_nomemunicipio($st_nomemunicipio)
    {
        $this->st_nomemunicipio = $st_nomemunicipio;
        return $this;
    }

    public function getNu_codigoibge()
    {
        return $this->nu_codigoibge;
    }

    public function setNu_codigoibge($nu_codigoibge)
    {
        $this->nu_codigoibge = $nu_codigoibge;
        return $this;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function setSg_uf(Uf $sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

}
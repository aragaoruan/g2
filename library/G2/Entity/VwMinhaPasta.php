<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_minhapasta")
 * @Entity
 * @author Neemias Santos <neemias.santos@gmail.com>
 */
class VwMinhaPasta
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_minhapasta
     * @Column(name="id_minhapasta", type="integer", nullable=false, length=4)
     */
    private $id_minhapasta;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;
    /**
     * @var datetime $dt_acao
     * @Column(name="dt_acao", type="datetime", nullable=true, length=8)
     */
    private $dt_acao;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_contrato
     * @Column(name="st_contrato", type="string", nullable=false, length=300)
     */
    private $st_contrato;
    /**
     * @Id
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @return integer id_minhapasta
     */
    public function getId_minhapasta()
    {
        return $this->id_minhapasta;
    }

    /**
     * @param id_minhapasta
     */
    public function setId_minhapasta($id_minhapasta)
    {
        $this->id_minhapasta = $id_minhapasta;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return datetime dt_acao
     */
    public function getDt_acao()
    {
        return $this->dt_acao;
    }

    /**
     * @param dt_acao
     */
    public function setDt_acao($dt_acao)
    {
        $this->dt_acao = $dt_acao;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_contrato
     */
    public function getSt_contrato()
    {
        return $this->st_contrato;
    }

    /**
     * @param st_contrato
     */
    public function setSt_contrato($st_contrato)
    {
        $this->st_contrato = $st_contrato;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }


}
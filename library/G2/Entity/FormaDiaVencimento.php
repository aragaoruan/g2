<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_formadiavencimento")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class FormaDiaVencimento
{

    /**
     * @var integer $id_formadiavencimento
     * @Column(name="id_formadiavencimento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_formadiavencimento;

    /**
     * @var FormaPagamento $id_formapagamento
     * @ManyToOne(targetEntity="FormaPagamento")
     * @JoinColumn(name="id_formapagamento", referencedColumnName="id_formapagamento")
     */
    private $id_formapagamento;

    /**
     * @var integer $nu_diavencimento
     * @Column(name="nu_diavencimento", type="integer", nullable=true)
     */
    private $nu_diavencimento;

    /**
     * @return integer
     */
    public function getId_formadiavencimento()
    {
        return $this->id_formadiavencimento;
    }

    /**
     * @return FormaPagamento
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @return integer
     */
    public function getNu_diavencimento()
    {
        return $this->nu_diavencimento;
    }

    /**
     * @param integer $id_formadiavencimento
     * @return \G2\Entity\FormaDiaVencimento
     */
    public function setId_formadiavencimento($id_formadiavencimento)
    {
        $this->id_formadiavencimento = $id_formadiavencimento;
        return $this;
    }

    /**
     * @param \G2\Entity\FormaPagamento $id_formapagamento
     * @return \G2\Entity\FormaDiaVencimento
     */
    public function setId_formapagamento(FormaPagamento $id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    /**
     * @param integer $nu_diavencimento
     * @return \G2\Entity\FormaDiaVencimento
     */
    public function setNu_diavencimento($nu_diavencimento)
    {
        $this->nu_diavencimento = $nu_diavencimento;
        return $this;
    }

}

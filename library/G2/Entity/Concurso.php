<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_concurso")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
use G2\G2Entity;

class Concurso extends G2Entity
{

    /**
     * @var integer $id_concurso
     * @Column(name="id_concurso", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_concurso;

    /**
     * @var string $st_concurso
     * @Column(name="st_concurso", type="string", nullable=false)
     */
    private $st_concurso;

    /**
     * @var string $st_slug
     * @Column(name="st_slug", type="string", nullable=true, length=200)
     */
    private $st_slug;

    /**
     * @var string $st_imagem
     * @Column(name="st_imagem", type="string", nullable=true)
     */
    private $st_imagem;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string $st_orgao
     * @Column(name="st_orgao", type="string", nullable=true, length=200)
     */
    private $st_orgao;

    /**
     * @var date $dt_prova
     * @Column(name="dt_prova", type="date", nullable=true)
     */
    private $dt_prova;

    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true)
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true)
     */
    private $dt_fiminscricao;

    /**
     * @var integer $nu_vagas
     * @Column(name="nu_vagas", type="integer", nullable=true, length=4)
     */
    private $nu_vagas;

    /**
     * @var string $st_banca
     * @Column(name="st_banca", type="string", nullable=false, length=200)
     */
    private $st_banca;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     * @var CargoConcurso $cargos
     * @OneToMany(targetEntity="CargoConcurso", mappedBy="id_concurso")
     */
    private $cargos;

    /**
     * @var ConcursoUf $ufs
     * @OneToMany(targetEntity="ConcursoUf", mappedBy="id_concurso")
     */
    private $ufs;

    /**
     *
     * @var ConcursoNivelEnsino  $niveisEnsino
     * @OneToMany(targetEntity="ConcursoNivelEnsino", mappedBy="id_concurso")
     */
    private $niveisEnsino;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

   
    /**
     * Retorna array de objetos de ConcursoUf
     * @return \G2\Entity\ConcursoUf
     */
    public function getUfs ()
    {
        return $this->ufs;
    }

    /**
     * Retorna array de objetos de ConcursoNivelEnsino
     * @return \G2\Entity\ConcursoNivelEnsino
     */
    public function getNiveisEnsino ()
    {
        return $this->niveisEnsino;
    }

    /**
     * Retorna array de objeto de CargoConcurso realacionados ao concurso
     * @return CargoConcurso cargos
     */
    public function getCargos ()
    {
        return $this->cargos;
    }

    /**
     * @return Entidade \G2\Entity\Concurso::id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\Concurso
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * 
     * @return integer \G2\Entity\Concurso::id_concurso
     */
    public function getId_concurso ()
    {
        return $this->id_concurso;
    }

    /**
     * @param integer $id_concurso
     * @return \G2\Entity\Concurso
     */
    public function setId_concurso ($id_concurso)
    {
        $this->id_concurso = $id_concurso;
        return $this;
    }

    /**
     * @return string \G2\Entity\Concurso::st_concurso
     */
    public function getSt_concurso ()
    {
        return $this->st_concurso;
    }

    /**
     * @param string $st_concurso
     * @return \G2\Entity\Concurso
     */
    public function setSt_concurso ($st_concurso)
    {
        $this->st_concurso = $st_concurso;
        return $this;
    }

    /**
     * @return string \G2\Entity\Concurso::st_slug
     */
    public function getSt_slug ()
    {
        return $this->st_slug;
    }

    /**
     * @param string $st_slug
     * @return \G2\Entity\Concurso
     */
    public function setSt_slug ($st_slug)
    {
        $this->st_slug = $st_slug;
        return $this;
    }

    /**
     * @return string \G2\Entity\Concurso::st_imagem
     */
    public function getSt_imagem ()
    {
        return $this->st_imagem;
    }

    /**
     * @param string $st_imagem
     * @return \G2\Entity\Concurso
     */
    public function setSt_imagem ($st_imagem)
    {
        $this->st_imagem = $st_imagem;
        return $this;
    }

    /**
     * @return date \G2\Entity\Concurso::dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param date $dt_cadastro
     * @return \G2\Entity\Concurso
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * 
     * @return string \G2\Entity\Concurso::st_orgao
     */
    public function getSt_orgao ()
    {
        return $this->st_orgao;
    }

    /**
     * 
     * @param string $st_orgao
     * @return \G2\Entity\Concurso
     */
    public function setSt_orgao ($st_orgao)
    {
        $this->st_orgao = $st_orgao;
        return $this;
    }

    /**
     * @return date \G2\Entity\Concurso::dt_prova
     */
    public function getDt_prova ()
    {
        return $this->dt_prova;
    }

    /**
     * @param date $dt_prova
     * @return \G2\Entity\Concurso
     */
    public function setDt_prova ($dt_prova)
    {
        $this->dt_prova = $dt_prova;
        return $this;
    }

    /**
     * @return date \G2\Entity\Concurso::dt_inicioinscricao
     */
    public function getDt_inicioinscricao ()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param date $dt_inicioinscricao
     * @return \G2\Entity\Concurso
     */
    public function setDt_inicioinscricao ($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return date \G2\Entity\Concurso::dt_fiminscricao
     */
    public function getDt_fiminscricao ()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * 
     * @param string $dt_fiminscricao
     * @return  \G2\Entity\Concurso
     */
    public function setDt_fiminscricao ($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return integer \G2\Entity\Concurso::nu_vagas
     */
    public function getNu_vagas ()
    {
        return $this->nu_vagas;
    }

    /**
     * 
     * @param integer $nu_vagas
     * @return \G2\Entity\Concurso
     */
    public function setNu_vagas ($nu_vagas)
    {
        $this->nu_vagas = $nu_vagas;
        return $this;
    }

    /**
     * @return string \G2\Entity\Concurso::st_banca
     */
    public function getSt_banca ()
    {
        return $this->st_banca;
    }

    /**
     * @param string $st_banca
     * @return \G2\Entity\Concurso
     */
    public function setSt_banca ($st_banca)
    {
        $this->st_banca = $st_banca;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_Situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setId_Situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }



    /**
     * Return array attributes
     * @return array \G2\Entity\Concurso
     */
    public function toArray ()
    {
        return array(
            'id_concurso' => $this->id_concurso,
            'st_concurso' => $this->st_concurso,
            'st_slug' => $this->st_slug,
            'st_imagem' => $this->st_imagem,
            'dt_cadastro' => $this->dt_cadastro,
            'st_orgao' => $this->st_orgao,
            'dt_prova' => $this->dt_prova,
            'dt_inicioinscricao' => $this->dt_inicioinscricao,
            'dt_fiminscricao' => $this->dt_fiminscricao,
            'nu_vagas' => $this->nu_vagas,
            'st_banca' => $this->st_banca,
            'id_situacao' => $this->id_situacao
        );
    }

}

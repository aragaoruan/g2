<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * Entity EntregaDeclaracao
 * @author: Neemias Santos <neemias.santos@unyleya.com.br>
 * @since: 14/01/2015
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_entregadeclaracao")
 * @Entity
 */

class EntregaDeclaracao extends G2Entity {

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_entregadeclaracao
     * @Column(name="id_entregadeclaracao", type="integer", nullable=false, length=4)
     */
    private $id_entregadeclaracao;

    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_solicitacao;
    /**
     * @var datetime2 $dt_envio
     * @Column(name="dt_envio", type="datetime2", nullable=true, length=8)
     */
    private $dt_envio;
    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="datetime2", nullable=true, length=8)
     */
    private $dt_entrega;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;
    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;
    /**
     * @var integer $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
    */
    private $id_venda;
    /**
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema")
     */
    private $id_textosistema;
    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_vendaproduto
     * @ManyToOne(targetEntity="VendaProduto")
     * @JoinColumn(name="id_vendaproduto", referencedColumnName="id_vendaproduto")
     */
    private $id_vendaproduto;

    /**
     * @return int
     */
    public function getid_entregadeclaracao()
    {
        return $this->id_entregadeclaracao;
    }

    /**
     * @param int $id_entregadeclaracao
     */
    public function setid_entregadeclaracao($id_entregadeclaracao)
    {
        $this->id_entregadeclaracao = $id_entregadeclaracao;
    }

    /**
     * @return datetime2
     */
    public function getdt_solicitacao()
    {
        return $this->dt_solicitacao;
    }

    /**
     * @param datetime2 $dt_solicitacao
     */
    public function setdt_solicitacao($dt_solicitacao)
    {
        $this->dt_solicitacao = $dt_solicitacao;
    }

    /**
     * @return datetime2
     */
    public function getdt_envio()
    {
        return $this->dt_envio;
    }

    /**
     * @param datetime2 $dt_envio
     */
    public function setdt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
    }

    /**
     * @return datetime2
     */
    public function getdt_entrega()
    {
        return $this->dt_entrega;
    }

    /**
     * @param datetime2 $dt_entrega
     */
    public function setdt_entrega($dt_entrega)
    {
        $this->dt_entrega = $dt_entrega;
    }

    /**
     * @return datetime2
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return Matricula
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return Situacao
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param Situacao $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return int
     */
    public function getid_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setid_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return TextoSistema
     */
    public function getid_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param TextoSistema $id_textosistema
     */
    public function setid_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
    }

    /**
     * @return Usuario
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getid_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param int $id_vendaproduto
     */
    public function setid_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
    }


}

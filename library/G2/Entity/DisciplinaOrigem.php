<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_disciplinaorigem")
 * @Entity
 * @EntityLog
 */

class DisciplinaOrigem
{
    /**
     * @var integer $id_disciplinaorigem
     * @Column(name="id_disciplinaorigem", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplinaorigem;

    /**
     * @var string $dt_cadastro
     * @Column(name="dt_cadastro", type="string", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var Usuario $id_aluno
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_aluno", referencedColumnName="id_usuario")
     */
    private $id_aluno;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=140)
     */
    private $st_disciplina;

    /**
     * @var string $st_instituicao
     * @Column(name="st_instituicao", type="string", nullable=true, length=140)
     */
    private $st_instituicao;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=false, length=2)
     */
    private $sg_uf;

    /**
     * @return int
     */
    public function getId_disciplinaorigem()
    {
        return $this->id_disciplinaorigem;
    }

    /**
     * @param int $id_disciplinaorigem
     * @return $this
     */
    public function setId_disciplinaorigem($id_disciplinaorigem)
    {
        $this->id_disciplinaorigem = $id_disciplinaorigem;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param string $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_aluno()
    {
        return $this->id_aluno;
    }

    /**
     * @param Usuario $id_aluno
     * @return $this
     */
    public function setId_aluno($id_aluno)
    {
        $this->id_aluno = $id_aluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param int $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     * @return $this
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_instituicao()
    {
        return $this->st_instituicao;
    }

    /**
     * @param string $st_instituicao
     * @return $this
     */
    public function setSt_instituicao($st_instituicao)
    {
        $this->st_instituicao = $st_instituicao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_uf
     * @return $this
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }
}

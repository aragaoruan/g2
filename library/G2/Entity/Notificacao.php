<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_notificacao")
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Notificacao
{

    /**
     * @Id
     * @var integer $id_notificacao
     * @Column(name="id_notificacao", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_notificacao;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var string $st_notificacao
     * @Column(name="st_notificacao", type="string", nullable=true, length=50)
     */
    private $st_notificacao;

    /**
     * @return integer id_notificacao
     */
    public function getId_notificacao()
    {
        return $this->id_notificacao;
    }

    /**
     * @param id_notificacao
     */
    public function setId_notificacao($id_notificacao)
    {
        $this->id_notificacao = $id_notificacao;
        return $this;
    }


    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro(\DateTime $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string st_notificacao
     */
    public function getSt_notificacao()
    {
        return $this->st_notificacao;
    }

    /**
     * @param st_notificacao
     */
    public function setSt_notificacao($st_notificacao)
    {
        $this->st_notificacao = $st_notificacao;
        return $this;
    }

} 
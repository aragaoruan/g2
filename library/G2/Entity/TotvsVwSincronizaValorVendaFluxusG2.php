<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 08/01/14
 * Time: 14:14
 */

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="totvs.vw_sincroniza_valorvenda_fluxus_g2")
 */
class TotvsVwSincronizaValorVendaFluxusG2 {


    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     * @Id
     */
    private $id_venda;

    /**
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valor;

    /**
     * @param int $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param \G2\Entity\decimal $nu_valor
     */
    public function setNuValor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNuValor()
    {
        return $this->nu_valor;
    }


} 
<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 18/11/13
 * Time: 11:25
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_tramitevenda")
 * @EntityLog
 */
class TramiteVenda extends G2Entity
{
    /**
     * @Id
     * @var integer $id_tramitevenda
     * @Column(name="id_tramitevenda", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramitevenda;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;
    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=true, length=4)
     */
    private $id_tramite;


    /**
     * @return integer id_tramitevenda
     */
    public function getId_tramitevenda() {
        return $this->id_tramitevenda;
    }

    /**
     * @param id_tramitevenda
     */
    public function setId_tramitevenda($id_tramitevenda) {
        $this->id_tramitevenda = $id_tramitevenda;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_tramite
     */
    public function getId_tramite() {
        return $this->id_tramite;
    }

    /**
     * @param id_tramite
     */
    public function setId_tramite($id_tramite) {
        $this->id_tramite = $id_tramite;
        return $this;
    }

}
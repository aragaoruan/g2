<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\AvaliacaoAgendamento")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avaliacaoaluno")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwAvaliacaoAluno
{
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_avaliacaoaluno
     * @Column(name="id_avaliacaoaluno", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaluno;
    /**
     * @var date $dt_agendamento
     * @Column(name="dt_agendamento", type="date", nullable=true, length=3)
     */
    private $dt_agendamento;

    /**
     * @var date $dt_avaliacao
     * @Column(name="dt_avaliacao", type="date", nullable=true, length=3)
     */
    private $dt_avaliacao;

    /**
     * @var date $dt_defesa
     * @Column(name="dt_defesa", type="date", nullable=true, length=3)
     */
    private $dt_defesa;

    /**
     * @var date $dt_aberturasala
     * @Column(name="dt_aberturasala", type="date", nullable=true, length=3)
     */
    private $dt_aberturasala;

    /**
     * @var date $dt_encerramentosala
     * @Column(name="dt_encerramentosala", type="date", nullable=true, length=3)
     */
    private $dt_encerramentosala;

    /**
     * @var datetime2 $dt_cadastrosala
     * @Column(name="dt_cadastrosala", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastrosala;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_matricula;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=true, length=4)
     */
    private $id_modulo;

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @Id
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoagendamento;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacao;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=true, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_tiponota
     * @Column(name="id_tiponota", type="integer", nullable=true, length=4)
     */
    private $id_tiponota;

    /**
     * @var integer $id_avaliacaorecupera
     * @Column(name="id_avaliacaorecupera", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaorecupera;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoconjunto;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=true, length=4)
     */
    private $id_entidadeatendimento;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true, length=4)
     */
    private $id_tipoprova;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipocalculoavaliacao;

    /**
     * @Id
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipoavaliacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @var float $st_nota
     * @Column(name="st_nota", type="float", nullable=true, length=5)
     */
    private $st_nota;

    /**
     * @var float $nu_notamaxima
     * @Column(name="nu_notamaxima", type="float", nullable=true, length=9)
     */
    private $nu_notamaxima;

    /**
     * @var float $nu_percentualaprovacao
     * @Column(name="nu_percentualaprovacao", type="float", nullable=true, length=5)
     */
    private $nu_percentualaprovacao;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_tituloexibicaomodulo
     * @Column(name="st_tituloexibicaomodulo", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaomodulo;

    /**
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=true, length=100)
     */
    private $st_avaliacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=true, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $nu_notamax
     * @Column(name="nu_notamax", type="string", nullable=true, length=100)
     */
    private $nu_notamax;

    /**
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=true, length=200)
     */
    private $st_tipoavaliacao;

    /**
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="text", nullable=true)
     */
    private $st_codusuario;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_tituloavaliacao
     * @Column(name="st_tituloavaliacao", type="string", nullable=true, length=500)
     */
    private $st_tituloavaliacao;

    /**
     * @var string $st_justificativa
     * @Column(name="st_justificativa", type="string", nullable=true, length=2000)
     */
    private $st_justificativa;

    /**
     * @var integer $bl_agendamento
     * @Column(name="bl_agendamento", type="integer", nullable=true, length=4)
     */
    private $bl_agendamento;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true, length=4)
     */
    private $bl_provaglobal;

    /**
     * @var date $dt_cadastroavaliacao
     * @Column(name="dt_cadastroavaliacao", type="datetime", nullable=true)
     */
    private $dt_cadastroavaliacao;

    /**
     * @var integer $id_usuariocadavaliacao
     * @Column(name="id_usuariocadavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadavaliacao;

    /**
     * @var string $st_usuariocadavaliacao
     * @Column(name="st_usuariocadavaliacao", type="string", nullable=true)
     */
    private $st_usuariocadavaliacao;
    /**
     * @var integer $id_evolucaodisciplina
     * @Column(name="id_evolucaodisciplina", type="integer", nullable=true, length=4)
     */
    private $id_evolucaodisciplina;

    /**
     * @var string $st_evolucaodisciplina
     * @Column(name="st_evolucaodisciplina", type="string", nullable=true)
     */
    private $st_evolucaodisciplina;
    /**
     * @var integer $id_provasrealizadas
     * @Column(name="id_provasrealizadas", type="integer", nullable=true)
     */
    private $id_provasrealizadas;

    /**
     * @var string $dt_cadastrotcc
     * @Column(name="dt_cadastrotcc", type="string", nullable=true)
     */
    private $dt_cadastrotcc;

    /**
     * @var string $st_situacaosala
     * @Column(name="st_situacaosala", type="string", nullable=true)
     */
    private $st_situacaosala;

    /**
     * @var string $st_upload
     * @Column(name="st_upload", type="string", nullable=true)
     */
    private $st_upload;

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true)
     */
    private $id_categoriasala;

    /**
     * @var string $st_dtavaliacao
     * @Column(name="st_dtavaliacao", type="string", nullable=true)
     */
    private $st_dtavaliacao;

    /**
     * @var boolean $bl_recuperacao
     * @Column(name="bl_recuperacao", type="boolean", nullable=true, length=1)
     */
    private $bl_recuperacao;

    /**
     * @var integer
     * @Column(name="id_situacaoagendamento", type="integer", nullable=true)
     * */
    private $id_situacaoagendamento;

    /**
     * @var string $st_situacaomatricula
     * @Column(name="st_situacaomatricula", type="string", nullable=true)
     */
    private $st_situacaomatricula;
    /**
     * @var string $st_notaconceitual
     * @Column(name="st_notaconceitual", type="string", nullable=true)
     */
    private $st_notaconceitual;
    /**
     * @var integer $id_notaconceitual
     * @Column(name="id_notaconceitual", type="integer", nullable=true)
     */
    private $id_notaconceitual;

    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer", nullable=true, length=4)
     */
    private $id_situacaotcc;

    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     */
    private $id_alocacao;


    /**
     * @return int
     */
    public function getid_avaliacaoaluno()
    {
        return $this->id_avaliacaoaluno;
    }

    /**
     * @param int $id_avaliacaoaluno
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacaoaluno($id_avaliacaoaluno)
    {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_agendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @param date $dt_agendamento
     * @return VwAvaliacaoAluno
     */
    public function setdt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_avaliacao()
    {
        return $this->dt_avaliacao;
    }

    /**
     * @param date $dt_avaliacao
     * @return VwAvaliacaoAluno
     */
    public function setdt_avaliacao($dt_avaliacao)
    {
        $this->dt_avaliacao = $dt_avaliacao;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_defesa()
    {
        return $this->dt_defesa;
    }

    /**
     * @param date $dt_defesa
     * @return VwAvaliacaoAluno
     */
    public function setdt_defesa($dt_defesa)
    {
        $this->dt_defesa = $dt_defesa;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_aberturasala()
    {
        return $this->dt_aberturasala;
    }

    /**
     * @param date $dt_aberturasala
     * @return VwAvaliacaoAluno
     */
    public function setdt_aberturasala($dt_aberturasala)
    {
        $this->dt_aberturasala = $dt_aberturasala;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_encerramentosala()
    {
        return $this->dt_encerramentosala;
    }

    /**
     * @param date $dt_encerramentosala
     * @return VwAvaliacaoAluno
     */
    public function setdt_encerramentosala($dt_encerramentosala)
    {
        $this->dt_encerramentosala = $dt_encerramentosala;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getdt_cadastrosala()
    {
        return $this->dt_cadastrosala;
    }

    /**
     * @param datetime2 $dt_cadastrosala
     * @return VwAvaliacaoAluno
     */
    public function setdt_cadastrosala($dt_cadastrosala)
    {
        $this->dt_cadastrosala = $dt_cadastrosala;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAvaliacaoAluno
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param int $id_modulo
     * @return VwAvaliacaoAluno
     */
    public function setid_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwAvaliacaoAluno
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    /**
     * @param int $id_avaliacaoagendamento
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacao()
    {
        return $this->id_avaliacao;
    }

    /**
     * @param int $id_avaliacao
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return VwAvaliacaoAluno
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     * @return VwAvaliacaoAluno
     */
    public function setid_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tiponota()
    {
        return $this->id_tiponota;
    }

    /**
     * @param int $id_tiponota
     * @return VwAvaliacaoAluno
     */
    public function setid_tiponota($id_tiponota)
    {
        $this->id_tiponota = $id_tiponota;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacaorecupera()
    {
        return $this->id_avaliacaorecupera;
    }

    /**
     * @param int $id_avaliacaorecupera
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacaorecupera($id_avaliacaorecupera)
    {
        $this->id_avaliacaorecupera = $id_avaliacaorecupera;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwAvaliacaoAluno
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    /**
     * @param int $id_avaliacaoconjunto
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwAvaliacaoAluno
     */
    public function setid_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param int $id_entidadeatendimento
     * @return VwAvaliacaoAluno
     */
    public function setid_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param int $id_upload
     * @return VwAvaliacaoAluno
     */
    public function setid_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     * @return VwAvaliacaoAluno
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipoprova()
    {
        return $this->id_tipoprova;
    }

    /**
     * @param int $id_tipoprova
     * @return VwAvaliacaoAluno
     */
    public function setid_tipoprova($id_tipoprova)
    {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param int $id_avaliacaoconjuntoreferencia
     * @return VwAvaliacaoAluno
     */
    public function setid_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param int $id_evolucao
     * @return VwAvaliacaoAluno
     */
    public function setid_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipocalculoavaliacao()
    {
        return $this->id_tipocalculoavaliacao;
    }

    /**
     * @param int $id_tipocalculoavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setid_tipocalculoavaliacao($id_tipocalculoavaliacao)
    {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    /**
     * @param int $id_tipoavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setid_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return VwAvaliacaoAluno
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return float
     */
    public function getst_nota()
    {
        return $this->st_nota;
    }

    /**
     * @param float $st_nota
     * @return VwAvaliacaoAluno
     */
    public function setst_nota($st_nota)
    {
        $this->st_nota = $st_nota;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_notamaxima()
    {
        return $this->nu_notamaxima;
    }

    /**
     * @param float $nu_notamaxima
     * @return VwAvaliacaoAluno
     */
    public function setnu_notamaxima($nu_notamaxima)
    {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    /**
     * @return float
     */
    public function getnu_percentualaprovacao()
    {
        return $this->nu_percentualaprovacao;
    }

    /**
     * @param float $nu_percentualaprovacao
     * @return VwAvaliacaoAluno
     */
    public function setnu_percentualaprovacao($nu_percentualaprovacao)
    {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param string $st_nomecompleto
     * @return VwAvaliacaoAluno
     */
    public function setst_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tituloexibicaomodulo()
    {
        return $this->st_tituloexibicaomodulo;
    }

    /**
     * @param string $st_tituloexibicaomodulo
     * @return VwAvaliacaoAluno
     */
    public function setst_tituloexibicaomodulo($st_tituloexibicaomodulo)
    {
        $this->st_tituloexibicaomodulo = $st_tituloexibicaomodulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tituloexibicaodisciplina()
    {
        return $this->st_tituloexibicaodisciplina;
    }

    /**
     * @param string $st_tituloexibicaodisciplina
     * @return VwAvaliacaoAluno
     */
    public function setst_tituloexibicaodisciplina($st_tituloexibicaodisciplina)
    {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_avaliacao()
    {
        return $this->st_avaliacao;
    }

    /**
     * @param string $st_avaliacao
     * @return VwAvaliacaoAluno
     */
    public function setst_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     * @return VwAvaliacaoAluno
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param string $st_evolucao
     * @return VwAvaliacaoAluno
     */
    public function setst_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getnu_notamax()
    {
        return $this->nu_notamax;
    }

    /**
     * @param string $nu_notamax
     * @return VwAvaliacaoAluno
     */
    public function setnu_notamax($nu_notamax)
    {
        $this->nu_notamax = $nu_notamax;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tipoavaliacao()
    {
        return $this->st_tipoavaliacao;
    }

    /**
     * @param string $st_tipoavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setst_tipoavaliacao($st_tipoavaliacao)
    {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param string $st_codusuario
     * @return VwAvaliacaoAluno
     */
    public function setst_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param string $st_projetopedagogico
     * @return VwAvaliacaoAluno
     */
    public function setst_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_tituloavaliacao()
    {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param string $st_tituloavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setst_tituloavaliacao($st_tituloavaliacao)
    {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_justificativa()
    {
        return $this->st_justificativa;
    }

    /**
     * @param string $st_justificativa
     * @return VwAvaliacaoAluno
     */
    public function setst_justificativa($st_justificativa)
    {
        $this->st_justificativa = $st_justificativa;
        return $this;
    }

    /**
     * @return int
     */
    public function getbl_agendamento()
    {
        return $this->bl_agendamento;
    }

    /**
     * @param int $bl_agendamento
     * @return VwAvaliacaoAluno
     */
    public function setbl_agendamento($bl_agendamento)
    {
        $this->bl_agendamento = $bl_agendamento;
        return $this;
    }

    /**
     * @return int
     */
    public function getbl_provaglobal()
    {
        return $this->bl_provaglobal;
    }

    /**
     * @param int $bl_provaglobal
     * @return VwAvaliacaoAluno
     */
    public function setbl_provaglobal($bl_provaglobal)
    {
        $this->bl_provaglobal = $bl_provaglobal;
        return $this;
    }

    /**
     * @return date
     */
    public function getdt_cadastroavaliacao()
    {
        return $this->dt_cadastroavaliacao;
    }

    /**
     * @param date $dt_cadastroavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setdt_cadastroavaliacao($dt_cadastroavaliacao)
    {
        $this->dt_cadastroavaliacao = $dt_cadastroavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuariocadavaliacao()
    {
        return $this->id_usuariocadavaliacao;
    }

    /**
     * @param int $id_usuariocadavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setid_usuariocadavaliacao($id_usuariocadavaliacao)
    {
        $this->id_usuariocadavaliacao = $id_usuariocadavaliacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_usuariocadavaliacao()
    {
        return $this->st_usuariocadavaliacao;
    }

    /**
     * @param string $st_usuariocadavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setst_usuariocadavaliacao($st_usuariocadavaliacao)
    {
        $this->st_usuariocadavaliacao = $st_usuariocadavaliacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_evolucaodisciplina()
    {
        return $this->id_evolucaodisciplina;
    }

    /**
     * @param int $id_evolucaodisciplina
     * @return VwAvaliacaoAluno
     */
    public function setid_evolucaodisciplina($id_evolucaodisciplina)
    {
        $this->id_evolucaodisciplina = $id_evolucaodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_evolucaodisciplina()
    {
        return $this->st_evolucaodisciplina;
    }

    /**
     * @param string $st_evolucaodisciplina
     * @return VwAvaliacaoAluno
     */
    public function setst_evolucaodisciplina($st_evolucaodisciplina)
    {
        $this->st_evolucaodisciplina = $st_evolucaodisciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_provasrealizadas()
    {
        return $this->id_provasrealizadas;
    }

    /**
     * @param int $id_provasrealizadas
     * @return VwAvaliacaoAluno
     */
    public function setid_provasrealizadas($id_provasrealizadas)
    {
        $this->id_provasrealizadas = $id_provasrealizadas;
        return $this;
    }

    /**
     * @return string
     */
    public function getdt_cadastrotcc()
    {
        return $this->dt_cadastrotcc;
    }

    /**
     * @param string $dt_cadastrotcc
     * @return VwAvaliacaoAluno
     */
    public function setdt_cadastrotcc($dt_cadastrotcc)
    {
        $this->dt_cadastrotcc = $dt_cadastrotcc;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_situacaosala()
    {
        return $this->st_situacaosala;
    }

    /**
     * @param string $st_situacaosala
     * @return VwAvaliacaoAluno
     */
    public function setst_situacaosala($st_situacaosala)
    {
        $this->st_situacaosala = $st_situacaosala;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_upload()
    {
        return $this->st_upload;
    }

    /**
     * @param string $st_upload
     * @return VwAvaliacaoAluno
     */
    public function setst_upload($st_upload)
    {
        $this->st_upload = $st_upload;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_categoriasala()
    {
        return $this->id_categoriasala;
    }

    /**
     * @param int $id_categoriasala
     * @return VwAvaliacaoAluno
     */
    public function setid_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_dtavaliacao()
    {
        return $this->st_dtavaliacao;
    }

    /**
     * @param string $st_dtavaliacao
     * @return VwAvaliacaoAluno
     */
    public function setst_dtavaliacao($st_dtavaliacao)
    {
        $this->st_dtavaliacao = $st_dtavaliacao;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_recuperacao()
    {
        return $this->bl_recuperacao;
    }

    /**
     * @param boolean $bl_recuperacao
     * @return $this
     */
    public function setBl_recuperacao($bl_recuperacao)
    {
        $this->bl_recuperacao = $bl_recuperacao;
        return $this;
    }

    /**
     * @return integer
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param integer $id_situacaoagendamento
     * @return VwAvaliacaoAluno
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = (integer) $id_situacaoagendamento;
        return $this;
    }

    public function getSt_situacaomatricula()
    {
        return $this->st_situacaomatricula;
    }

    public function setSt_situacaomatricula($st_situacaomatricula)
    {
        return $this->st_situacaomatricula = trim($st_situacaomatricula);
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_notaconceitual()
    {
        return $this->st_notaconceitual;
    }

    /**
     * @param string $st_notaconceitual
     */
    public function setSt_notaconceitual($st_notaconceitual)
    {
        $this->st_notaconceitual = $st_notaconceitual;
    }

    /**
     * @return int
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param int $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }

    /**
     * @return int
     */
    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @param int $id_situacaotcc
     */
    public function setId_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
    }

    /**
     * @return int
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
    }

}

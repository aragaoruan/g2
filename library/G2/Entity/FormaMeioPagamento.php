<?php

/*
 * Entity FormaMeioPagamento
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-30
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formameiopagamento")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class FormaMeioPagamento
{

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;

    /**
     *
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_meiopagamento;

    /**
     * @var integer $id_tipodivisaofinanceira
     * @Column(name="id_tipodivisaofinanceira", type="integer", nullable=false)
     */
    private $id_tipodivisaofinanceira;


    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    public function getId_tipodivisaofinanceira()
    {
        return $this->id_tipodivisaofinanceira;
    }

    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    public function setId_tipodivisaofinanceira($tipodivisaofinanceira)
    {
        $this->id_tipodivisaofinanceira = $tipodivisaofinanceira;
    }

}

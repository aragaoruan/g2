<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidadeclasse")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadedeClasse
{

    /**
     *
     * @var integer $id_entidadeclasse
     * @Column(name="id_entidadeclasse", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_entidadeclasse;

    /**
     * @var string $st_entidadeclasse
     * @Column(name="st_entidadeclasse",type="string", nullable=true, length=255)
     */
    private $st_entidadeclasse;

    public function getId_entidadeclasse ()
    {
        return $this->id_entidadeclasse;
    }

    public function setId_entidadeclasse ($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    public function getSt_entidadeclasse ()
    {
        return $this->st_entidadeclasse;
    }

    public function setSt_entidadeclasse ($st_entidadeclasse)
    {
        $this->st_entidadeclasse = $st_entidadeclasse;
        return $this;
    }

}

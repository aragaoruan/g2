<?php
/**
 * Created by PhpStorm.
 * User: helderfernandes
 * Date: 13/09/17
 * Time: 09:20
 */

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_errocontrato")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwErroContrato extends G2Entity
{
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_venda;
    /**
     * @var /datetime $dt_aceita
     * @Column(name="dt_aceita", type="datetime2", nullable=true)
     */
    private $dt_aceita;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true)
     */
    private $st_nomeentidade;


    /**
     * @return datetime dt_aceita
     */
    public function getDt_aceita()
    {
        return $this->dt_aceita;
    }

    /**
     * @param dt_aceita
     */
    public function setDt_aceita($dt_aceita)
    {
        $this->dt_aceita = $dt_aceita;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

}
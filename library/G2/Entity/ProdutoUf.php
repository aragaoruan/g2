<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_produtouf")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimarães@gmail.com> 
 */

use G2\G2Entity;

class ProdutoUf extends G2Entity
{

    /**
     * @var integer $id_produtouf
     * @Column(name="id_produtouf", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtouf;

    /**
     *
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    
    /**
     *
     * @var string $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", nullable=false, referencedColumnName="sg_uf")
     */
    private $sg_uf;
    
    
	/**
	 * @return the $id_produtouf
	 */
	public function getId_produtouf() {
		return $this->id_produtouf;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $sg_uf
	 */
	public function getSg_uf() {
		return $this->sg_uf;
	}

	/**
	 * @param number $id_produtouf
	 */
	public function setId_produtouf($id_produtouf) {
		$this->id_produtouf = $id_produtouf;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param string $sg_uf
	 */
	public function setSg_uf($sg_uf) {
		$this->sg_uf = $sg_uf;
		return $this;
	}

}
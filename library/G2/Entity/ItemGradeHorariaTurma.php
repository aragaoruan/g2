<?php

namespace G2\Entity;

/**
 * Entity of ItemGradeHorariaTurma
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Entity(repositoryClass="\G2\Repository\GradeHoraria")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_itemgradehorariaturma")
 */
class ItemGradeHorariaTurma
{

    /**
     * @var integer $id_itemgradehorariaturma
     * @Column(name="id_itemgradehorariaturma", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemgradehorariaturma;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var ItemGradeHoraria $id_itemgradehoraria
     * @ManyToOne(targetEntity="ItemGradeHoraria")
     * @JoinColumn(name="id_itemgradehoraria", nullable=false, referencedColumnName="id_itemgradehoraria")
     */
    private $id_itemgradehoraria;

    /**
     * @var Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", nullable=false, referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;


    /**
     * @return int
     */
    public function getId_itemgradehorariaturma()
    {
        return $this->id_itemgradehorariaturma;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return ItemGradeHoraria
     */
    public function getId_itemgradehoraria()
    {
        return $this->id_itemgradehoraria;
    }

    /**
     * @return Turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $id_itemgradehorariaturma
     * @return $this
     */
    public function setId_itemgradehorariaturma($id_itemgradehorariaturma)
    {
        $this->id_itemgradehorariaturma = $id_itemgradehorariaturma;
        return $this;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param ItemGradeHoraria $id_itemgradehoraria
     * @return $this
     */
    public function setId_itemgradehoraria(ItemGradeHoraria $id_itemgradehoraria)
    {
        $this->id_itemgradehoraria = $id_itemgradehoraria;
        return $this;
    }

    /**
     * @param Turma $id_turma
     * @return $this
     */
    public function setId_turma(Turma $id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

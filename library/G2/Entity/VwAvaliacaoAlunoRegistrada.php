<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avaliacaoalunoregistrada")
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwAvaliacaoAlunoRegistrada {

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_avaliacaoaluno
     * @Column(name="id_avaliacaoaluno", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoaluno;

    /**
     * @var date $dt_avaliacao
     * @Column(name="dt_avaliacao", type="date", nullable=true, length=3)
     */
    private $dt_avaliacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=false, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=false, length=4)
     */
    private $id_avaliacao;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_upload
     * @Column(name="id_upload", type="integer", nullable=true, length=4)
     */
    private $id_upload;

    /**
     * @var integer $id_tiponota
     * @Column(name="id_tiponota", type="integer", nullable=false, length=4)
     */
    private $id_tiponota;

    /**
     * @var string $st_tituloavaliacao
     * @Column(name="st_tituloavaliacao", type="string", nullable=true, length=500)
     */
    private $st_tituloavaliacao;

    /**
     * @var string $st_nomeusuario
     * @Column(name="st_nomeusuario", type="string", nullable=true, length=255)
     */
    private $st_nomeusuario;

    /**
     * @var string $st_nota
     * @Column(name="st_nota", type="string", nullable=true, length=3)
     */
    private $st_nota;

    /**
     * @var string $st_dtavaliacao
     * @Column(name="st_dtavaliacao", type="string", nullable=true, length=13)
     */
    private $st_dtavaliacao;

    /**
     * @var string $st_dtalteracao
     * @Column(name="st_dtalteracao", type="string", nullable=true, length=13)
     */
    private $st_dtalteracao;

    /**
     * @var string $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;

    /**
     * @Id
     * @var string $st_acao
     * @Column(name="st_acao", type="string")
     */
    private $st_acao;

    /**
     * @var datetime $dt_alteracao
     * @Column(name="dt_alteracao", type="datetime", nullable=true)
     */
    private $dt_alteracao;

    /**
     * @var datetime $id_ordem
     * @Column(name="id_ordem", type="integer")
     */
    private $id_ordem;


    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer")
     */
    private $id_sistema;


    /**
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string")
     */
    private $st_sistema;

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return string
     */
    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

    /**
     * @param string $st_sistema
     */
    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
    }



    /**
     * @var integer $id_notaconceitual
     * @Column(name="id_notaconceitual", type="integer", nullable=false, length=4)
     */
    private $id_notaconceitual;
    /**
     * @return date $dt_avaliacao
     */
    public function getDt_avaliacao() {
        return $this->dt_avaliacao;
    }

    /**
     * @param $dt_avaliacao
     */
    public function setDt_avaliacao($dt_avaliacao) {
        $this->dt_avaliacao = $dt_avaliacao;
        return $this;
    }

    /**
     * @return integer $id_avaliacaoaluno
     */
    public function getId_avaliacaoaluno() {
        return $this->id_avaliacaoaluno;
    }

    /**
     * @param $id_avaliacaoaluno
     */
    public function setId_avaliacaoaluno($id_avaliacaoaluno) {
        $this->id_avaliacaoaluno = $id_avaliacaoaluno;
        return $this;
    }

    /**
     * @return integer $id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param $id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer $id_avaliacaoconjuntoreferencia
     */
    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param $id_avaliacaoconjuntoreferencia
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return integer $id_avaliacao
     */
    public function getId_avaliacao() {
        return $this->id_avaliacao;
    }

    /**
     * @param $id_avaliacao
     */
    public function setId_avaliacao($id_avaliacao) {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    /**
     * @return integer $id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer $id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param $id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer $id_upload
     */
    public function getId_upload() {
        return $this->id_upload;
    }

    /**
     * @param $id_upload
     */
    public function setId_upload($id_upload) {
        $this->id_upload = $id_upload;
        return $this;
    }

    /**
     * @return integer $id_tiponota
     */
    public function getId_tiponota() {
        return $this->id_tiponota;
    }

    /**
     * @param $id_tiponota
     */
    public function setId_tiponota($id_tiponota) {
        $this->id_tiponota = $id_tiponota;
        return $this;
    }

    /**
     * @return string $st_tituloavaliacao
     */
    public function getSt_tituloavaliacao() {
        return $this->st_tituloavaliacao;
    }

    /**
     * @param $st_tituloavaliacao
     */
    public function setSt_tituloavaliacao($st_tituloavaliacao) {
        $this->st_tituloavaliacao = $st_tituloavaliacao;
        return $this;
    }

    /**
     * @return string $st_nomeusuario
     */
    public function getSt_nomeusuario() {
        return $this->st_nomeusuario;
    }

    /**
     * @param $st_nomeusuario
     */
    public function setSt_nomeusuario($st_nomeusuario) {
        $this->st_nomeusuario = $st_nomeusuario;
        return $this;
    }

    /**
     * @return string $st_nota
     */
    public function getSt_nota() {
        return $this->st_nota;
    }

    /**
     * @param $st_nota
     */
    public function setSt_nota($st_nota) {
        $this->st_nota = $st_nota;
        return $this;
    }

    /**
     * @return string $st_dtavaliacao
     */
    public function getSt_dtavaliacao() {
        return $this->st_dtavaliacao;
    }

    /**
     * @param $st_dtavaliacao
     */
    public function setSt_dtavaliacao($st_dtavaliacao) {
        $this->st_dtavaliacao = $st_dtavaliacao;
        return $this;
    }

    /**
     * @return boolean $bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param $bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string $st_acao
     */
    public function getSt_acao() {
        return $this->st_acao;
    }

    /**
     * @param $st_acao
     */
    public function setSt_acao($st_acao) {
        $this->st_acao = $st_acao;
        return $this;
    }

    /**
     * @return string $st_dtalteracao
     */
    public function getSt_dtalteracao() {
        return $this->st_dtalteracao;
    }

    /**
     * @param $st_dtalteracao
     */
    public function setSt_dtalteracao($st_dtalteracao) {
        $this->st_dtalteracao = $st_dtalteracao;
    }

    /**
     * @return datetime $dt_alteracao
     */
    public function getDt_alteracao() {
        return $this->dt_alteracao;
    }

    /**
     * @param $dt_alteracao
     */
    public function setDt_alteracao($dt_alteracao) {
        $this->dt_alteracao = $dt_alteracao;
    }

    /**
     * @return integer $id_ordem
     */
    public function getId_ordem() {
        return $this->id_ordem;
    }

    /**
     * @param $id_ordem
     */
    public function setId_ordem($id_ordem) {
        $this->id_ordem = $id_ordem;
    }

    /**
     * @return int
     */
    public function getId_notaconceitual()
    {
        return $this->id_notaconceitual;
    }

    /**
     * @param int $id_notaconceitual
     */
    public function setId_notaconceitual($id_notaconceitual)
    {
        $this->id_notaconceitual = $id_notaconceitual;
    }


}

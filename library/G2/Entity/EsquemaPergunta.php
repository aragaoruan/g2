<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\PerguntasFrequentes")
 * @Table(name="tb_esquemapergunta")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class EsquemaPergunta
{

    /**
     * @var integer $id_esquemapergunta
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_esquemapergunta;

    /**
     * @var string $st_esquemapergunta
     * @Column(type="string", length=100, nullable=false)
     */
    private $st_esquemapergunta;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;


    /**
     * @return int
     */
    public function getId_esquemapergunta()
    {
        return $this->id_esquemapergunta;
    }

    /**
     * @return string
     */
    public function getSt_esquemapergunta()
    {
        return $this->st_esquemapergunta;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param $id_esquemapergunta
     * @return $this
     */
    public function setId_esquemapergunta($id_esquemapergunta)
    {
        $this->id_esquemapergunta = $id_esquemapergunta;
        return $this;
    }

    /**
     * @param $st_esquemapergunta
     * @return $this
     */
    public function setSt_esquemapergunta($st_esquemapergunta)
    {
        $this->st_esquemapergunta = $st_esquemapergunta;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro(Entidade $id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }


}

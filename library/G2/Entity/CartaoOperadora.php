<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_cartaooperadora")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CartaoOperadora
{

    /**
     *
     * @var integer $id_cartaooperadora
     * @Column(name="id_cartaooperadora", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cartaooperadora;

    /**
     *
     * @var string $st_cartaooperadora
     * @Column(name="st_cartaooperadora", type="string", nullable=false, length=50)
     */
    private $st_cartaooperadora;

    public function getId_cartaooperadora ()
    {
        return $this->id_cartaooperadora;
    }

    public function setId_cartaooperadora ($id_cartaooperadora)
    {
        $this->id_cartaooperadora = $id_cartaooperadora;
        return $this;
    }

    public function getSt_cartaooperadora ()
    {
        return $this->st_cartaooperadora;
    }

    public function setSt_cartaooperadora ($st_cartaooperadora)
    {
        $this->st_cartaooperadora = $st_cartaooperadora;
        return $this;
    }

}


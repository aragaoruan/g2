<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tiporegracontrato")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoRegraContrato
{
    /**
     * @Id
     * @var integer $id_tiporegracontrato
     * @Column(name="id_tiporegracontrato", type="integer", nullable=false, length=4)
     */
    private $id_tiporegracontrato;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_tiporegracontrato
     * @Column(name="st_tiporegracontrato", type="string", nullable=false, length=255)
     */
    private $st_tiporegracontrato;


    /**
     * @return integer id_tiporegracontrato
     */
    public function getId_tiporegracontrato() {
        return $this->id_tiporegracontrato;
    }

    /**
     * @param id_tiporegracontrato
     */
    public function setId_tiporegracontrato($id_tiporegracontrato) {
        $this->id_tiporegracontrato = $id_tiporegracontrato;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string st_tiporegracontrato
     */
    public function getSt_tiporegracontrato() {
        return $this->st_tiporegracontrato;
    }

    /**
     * @param st_tiporegracontrato
     */
    public function setSt_tiporegracontrato($st_tiporegracontrato) {
        $this->st_tiporegracontrato = $st_tiporegracontrato;
        return $this;
    }

}
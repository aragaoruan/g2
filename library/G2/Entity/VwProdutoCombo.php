<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtocombo")
 * @Entity
 * @EntityView
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class VwProdutoCombo
{

    /**
     *
     * @var integer $id_produtocombo
     * @Column(name="id_produtocombo", type="integer", nullable=false)
     * @Id
     */
    private $id_produtocombo;

    /**
     *
     * @var integer $id_combo
     * @Column(name="id_combo", type="integer", nullable=false)
     */
    private $id_combo;

    /**
     *
     * @var string $st_combo
     * @Column(name="st_combo", type="string", nullable=true, length=250)
     */
    private $st_combo;

    /**
     *
     * @var integer $id_produtoitem
     * @Column(name="id_produtoitem", type="integer", nullable=false)
     */
    private $id_produtoitem;

    /**
     *
     * @var decimal $nu_descontoporcentagem
     * @Column(name="nu_descontoporcentagem", type="decimal", nullable=true)
     */
    private $nu_descontoporcentagem;

    /**
     *
     * @var decimal $nu_valorprodutoitem
     * @Column(name="nu_valorprodutoitem", type="decimal", nullable=true)
     */
    private $nu_valorprodutoitem;

    /**
     *
     * @var decimal $nu_valordescontoitem
     * @Column(name="nu_valordescontoitem", type="decimal", nullable=true)
     */
    private $nu_valordescontoitem;

    /**
     *
     * @var decimal $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=true)
     */
    private $nu_valor;

    /**
     *
     * @var decimal $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=true)
     */
    private $nu_valormensal;

    /**
     *
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=false)
     */
    private $id_tipoprodutovalor;

    /**
     *
     * @var string $st_tipoprodutovalor
     * @Column(name="st_tipoprodutovalor", type="string", nullable=true, length=250)
     */
    private $st_tipoprodutovalor;

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     */
    private $id_produto;

    /**
     *
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=true, length=250)
     */
    private $st_produto;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var string $nu_gratuito
     * @Column(name="nu_gratuito", type="decimal", nullable=true)
     */
    private $nu_gratuito;

    /**
     *
     * @var integer $id_tipoproduto
     * @Column(name="id_tipoproduto", type="integer", nullable=false)
     */
    private $id_tipoproduto;

    /**
     *
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=true, length=255)
     */
    private $st_tipoproduto;

    /**
     *
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=false)
     */
    private $id_produtovalor;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var decimal $nu_valorcombo
     * @Column(type="decimal", nullable=false, name="nu_valorcombo")
     */
    private $nu_valorcombo;

    public function getId_produtocombo ()
    {
        return $this->id_produtocombo;
    }

    public function getId_combo ()
    {
        return $this->id_combo;
    }

    public function getSt_combo ()
    {
        return $this->st_combo;
    }

    public function getId_produtoitem ()
    {
        return $this->id_produtoitem;
    }

    public function getNu_descontoporcentagem ()
    {
        return $this->nu_descontoporcentagem;
    }

    public function getNu_valorprodutoitem ()
    {
        return $this->nu_valorprodutoitem;
    }

    public function getNu_valordescontoitem ()
    {
        return $this->nu_valordescontoitem;
    }

    public function getNu_valor ()
    {
        return $this->nu_valor;
    }

    public function getNu_valormensal ()
    {
        return $this->nu_valormensal;
    }

    public function getId_tipoprodutovalor ()
    {
        return $this->id_tipoprodutovalor;
    }

    public function getSt_tipoprodutovalor ()
    {
        return $this->st_tipoprodutovalor;
    }

    public function getId_produto ()
    {
        return $this->id_produto;
    }

    public function getSt_produto ()
    {
        return $this->st_produto;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function getNu_gratuito ()
    {
        return $this->nu_gratuito;
    }

    public function getId_tipoproduto ()
    {
        return $this->id_tipoproduto;
    }

    public function getSt_tipoproduto ()
    {
        return $this->st_tipoproduto;
    }

    public function getId_produtovalor ()
    {
        return $this->id_produtovalor;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function getNu_valorcombo ()
    {
        return $this->nu_valorcombo;
    }

    /**
     * 
     * @param integer $id_produtocombo
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_produtocombo ($id_produtocombo)
    {
        $this->id_produtocombo = $id_produtocombo;
        return $this;
    }

    /**
     * 
     * @param integer $id_combo
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_combo ($id_combo)
    {
        $this->id_combo = $id_combo;
        return $this;
    }

    /**
     * 
     * @param string $st_combo
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setSt_combo ($st_combo)
    {
        $this->st_combo = $st_combo;
        return $this;
    }

    /**
     * 
     * @param integer $id_produtoitem
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_produtoitem ($id_produtoitem)
    {
        $this->id_produtoitem = $id_produtoitem;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_descontoporcentagem
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_descontoporcentagem ($nu_descontoporcentagem)
    {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_valorprodutoitem
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_valorprodutoitem ($nu_valorprodutoitem)
    {
        $this->nu_valorprodutoitem = $nu_valorprodutoitem;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_valordescontoitem
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_valordescontoitem ($nu_valordescontoitem)
    {
        $this->nu_valordescontoitem = $nu_valordescontoitem;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_valor
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_valor ($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * 
     * @param decimal $nu_valormensal
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_valormensal ($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    /**
     * 
     * @param integer $id_tipoprodutovalor
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_tipoprodutovalor ($id_tipoprodutovalor)
    {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    /**
     * 
     * @param string $st_tipoprodutovalor
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setSt_tipoprodutovalor ($st_tipoprodutovalor)
    {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
        return $this;
    }

    /**
     * 
     * @param integer $id_produto
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * 
     * @param string $st_produto
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setSt_produto ($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * 
     * @param integer $id_entidade
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * 
     * @param integer $nu_gratuito
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_gratuito ($nu_gratuito)
    {
        $this->nu_gratuito = $nu_gratuito;
        return $this;
    }

    /**
     * 
     * @param integer $id_tipoproduto
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_tipoproduto ($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * 
     * @param string $st_tipoproduto
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setSt_tipoproduto ($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    /**
     * 
     * @param integer $id_produtovalor
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_produtovalor ($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    /**
     * 
     * @param integer $id_situacao
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * 
     * @param string $st_situacao
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @param decimal $nu_valorcombo
     * @return \G2\Entity\VwProdutoCombo
     */
    public function setNu_valorcombo ($nu_valorcombo)
    {
        $this->nu_valorcombo = $nu_valorcombo;
        return $this;
    }

}

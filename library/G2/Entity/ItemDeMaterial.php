<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_itemdematerial")
 * @Entity(repositoryClass = "\G2\Repository\ItemDeMaterial")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ItemDeMaterial
{

    /**
     *
     * @var integer $id_itemdematerial
     * @Column(name="id_itemdematerial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemdematerial;

    /**
     *
     * @var string $st_itemdematerial
     * @Column(name="st_itemdematerial", type="string", nullable=false, length=255)
     */
    private $st_itemdematerial;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var TipodeMaterial $id_tipodematerial
     * @ManyToOne(targetEntity="TipodeMaterial")
     * @JoinColumn(name="id_tipodematerial", nullable=false, referencedColumnName="id_tipodematerial")
     */
    private $id_tipodematerial;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var integer $nu_qtdepaginas
     * @Column(name="nu_qtdepaginas", type="integer", nullable=true)
     */
    private $nu_qtdepaginas;
    /**
     * @var integer $nu_peso
     * @Column(name="nu_peso", type="integer", nullable=true)
     */
    private $nu_peso;
    /**
     * @var Upload $id_upload
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_upload", nullable=false, referencedColumnName="id_upload")
     */
    private $id_upload;

    /**
     * @var decimal $nu_peso
     * @Column(name="nu_valor", type="decimal", nullable=true)
     */
    private $nu_valor;

    /**
     * @var boolean $bl_portal
     * @Column(name="bl_portal", type="boolean", nullable=true, length=1)
     */
    private $bl_portal;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;


    /**
     * @var integer $nu_encontro
     * @Column(name="nu_encontro", type="integer", nullable=true, length=4)
     */
    private $nu_encontro;

    /**
     * @var Usuario $id_professor
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_professor", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_professor;

    /**
     * @var integer $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $nu_qtdestoque
     * @Column(name="nu_qtdestoque", type="integer", nullable=true, length=4)
     */
    private $nu_qtdestoque;

    /**
     * @return int
     */
    public function getNu_qtdestoque()
    {
        return $this->nu_qtdestoque;
    }

    /**
     * @param int $nu_qtdestoque
     */
    public function setNu_qtdestoque($nu_qtdestoque)
    {
        $this->nu_qtdestoque = $nu_qtdestoque;
    }



    public function getId_itemdematerial ()
    {
        return $this->id_itemdematerial;
    }

    public function setId_itemdematerial ($id_itemdematerial)
    {
        $this->id_itemdematerial = $id_itemdematerial;
        return $this;
    }

    public function getSt_itemdematerial ()
    {
        return $this->st_itemdematerial;
    }

    public function setSt_itemdematerial ($st_itemdematerial)
    {
        $this->st_itemdematerial = $st_itemdematerial;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_tipodematerial ()
    {
        return $this->id_tipodematerial;
    }

    public function setId_tipodematerial (TipodeMaterial $id_tipodematerial)
    {
        $this->id_tipodematerial = $id_tipodematerial;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }


    /**
     * @return int
     */
    public function getNu_qtdepaginas()
    {
        return $this->nu_qtdepaginas;
    }

    /**
     * @param int $nu_qtdepaginas
     */
    public function setNu_qtdepaginas($nu_qtdepaginas)
    {
        $this->nu_qtdepaginas = $nu_qtdepaginas;
        return $this;

    }

    /**
     * @return int
     */
    public function getNu_peso()
    {
        return $this->nu_peso;
    }

    /**
     * @param int $nu_peso
     */
    public function setNu_peso($nu_peso)
    {
        $this->nu_peso = $nu_peso;
        return $this;

    }

    /**
     * @return int
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param int $id_upload
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
        return $this;

    }

    /**
     * @return numeric
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param numeric $nu_valor
     */
    public function setNu_Valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return mixed
     */
    public function getBl_portal()
    {
        return $this->bl_portal;
    }

    /**
     * @param mixed $bl_portal
     */
    public function setBl_portal($bl_portal)
    {
        $this->bl_portal = $bl_portal;
    }

    /**
     * @return int
     */
    public function getNu_encontro()
    {
        return $this->nu_encontro;
    }

    /**
     * @param int $nu_encontro
     */
    public function setNu_encontro($nu_encontro)
    {
        $this->nu_encontro = $nu_encontro;
    }

    /**
     * @return Usuario
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @param Usuario $id_professor
     */
    public function setId_professor($id_professor)
    {
        $this->id_professor = $id_professor;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }





}
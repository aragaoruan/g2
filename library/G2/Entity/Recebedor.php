<?php

/*
 * Entity Recebedor
 * @author: Helder Silva <helder.silva@unyleya.com.br>
 * @since: 2017-02-01
 */

namespace G2\Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_recebedor")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Recebedor
{
    /**
     * @Id
     * @var integer $id_recebedor
     * @Column(name="id_recebedor", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_recebedor;

    /**
     * @var integer $id_recebedorexterno
     * @Column(name="id_recebedorexterno", type="string", nullable=true, length=255)
     */
    private $id_recebedorexterno;

    /**
     * @var string $st_recebedor
     * @Column(name="st_recebedor", type="string", nullable=false, length=255)
     */
    private $st_recebedor;

    /**
     * @return int
     */
    public function getId_recebedor()
    {
        return $this->id_recebedor;
    }

    /**
     * @param int $id_recebedor
     */
    public function setId_recebedor($id_recebedor)
    {
        $this->id_recebedor = $id_recebedor;
    }

    /**
     * @return int
     */
    public function getId_recebedorexterno()
    {
        return $this->id_recebedorexterno;
    }

    /**
     * @param int $id_recebedorexterno
     */
    public function setId_recebedorexterno($id_recebedorexterno)
    {
        $this->id_recebedorexterno = $id_recebedorexterno;
    }

    /**
     * @return string
     */
    public function getSt_recebedor()
    {
        return $this->st_recebedor;
    }

    /**
     * @param string $st_recebedor
     */
    public function setSt_recebedor($st_recebedor)
    {
        $this->st_recebedor = $st_recebedor;
    }


}

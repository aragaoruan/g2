<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_logfinanceiro")
 * @Entity
 * @EntityView
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 */
class VwLogFinanceiro extends G2Entity
{
    /**
     * @var integer $id_log
     * @Column(name="id_log", type="integer", nullable=false)
     * @Id
     */
    private $id_log;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var string $st_campo
     * @Column(name="st_campo", type="string", nullable=false)
     */
    private $st_campo;

    /**
     * @var string $st_depois
     * @Column(name="st_depois", type="string", nullable=false)
     */
    private $st_depois;

    /**
     * @var string $st_usuario
     * @Column(name="st_usuario", type="string", nullable=false)
     */
    private $st_usuario;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=false)
     */
    private $st_nomeentidade;

    public function __set($name, $value)
    {
        $this->$name = $this->$value;
    }

    public function __get($name)
    {
        return $this->$name;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Entity(repositoryClass="\G2\Repository\ComissaoLancamento")
 * @EntityView
 * @Table(name="vw_comissaodireitos")
 */
class VwComissaoDireitos {

    /**
     * @var integer $id_usuario
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var string $st_nomecompleto
     * @Column(type="string", nullable=false) 
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(type="string", nullable=false) 
     */
    private $st_cpf;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var string $st_funcao
     * @Column(type="string", nullable=false) 
     */
    private $st_funcao;

    /**
     * @var integer $id_funcao
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_funcao;

    /**
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(type="string", nullable=false) 
     */
    private $st_disciplina;

    /**
     * @var integer $id_vendaproduto
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_vendaproduto;

    /**
     * @var float $nu_comissaoreceber
     * @Column(type="float", nullable=false) 
     */
    private $nu_comissaoreceber;

    /**
     * @var float $nu_valorliquido
     * @Column(type="float", nullable=false) 
     */
    private $nu_valorliquido;

    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", nullable=false) 
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;

    /**
     * @var datetime2 $dt_confirmacao
     * @Column(type="datetime2", nullable=false) 
     */
    private $dt_confirmacao;

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_cpf() {
        return $this->st_cpf;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getSt_funcao() {
        return $this->st_funcao;
    }

    public function getId_funcao() {
        return $this->id_funcao;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function getId_vendaproduto() {
        return $this->id_vendaproduto;
    }

    public function getNu_comissaoreceber() {
        return $this->nu_comissaoreceber;
    }

    public function getNu_valorliquido() {
        return $this->nu_valorliquido;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getDt_confirmacao() {
        return $this->dt_confirmacao;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setSt_funcao($st_funcao) {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    public function setId_funcao($id_funcao) {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function setId_vendaproduto($id_vendaproduto) {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    public function setNu_comissaoreceber($nu_comissaoreceber) {
        $this->nu_comissaoreceber = $nu_comissaoreceber;
        return $this;
    }

    public function setNu_valorliquido($nu_valorliquido) {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function setDt_confirmacao($dt_confirmacao) {
        $this->dt_confirmacao = $dt_confirmacao;
        return $this;
    }

}

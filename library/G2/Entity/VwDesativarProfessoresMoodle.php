<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_desativarprofessormoodle")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwDesativarProfessoresMoodle
{
    /**
     * @var integer $id_perfilreferencia
     * @Column(name="id_perfilreferencia", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_perfilreferencia;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;


    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidade;


    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=false, length=30)
     */
    private $st_codsistemacurso;


    /**
     * @var $bl_desativarmoodle
     * @Column(name="bl_desativarmoodle", type="boolean", nullable=true, length=1)
     */
    private $bl_desativarmoodle;

    public function getId_perfilreferencia()
    {
        return $this->id_perfilreferencia;
    }


    public function setId_perfilreferencia($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
    }


    public function getId_usuario()
    {
        return $this->id_usuario;
    }


    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }


    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


    public function getSt_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }


    public function setSt_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
    }


    public function getBl_desativarmoodle()
    {
        return $this->bl_desativarmoodle;
    }


    public function setBl_desativarmoodle($bl_desativarmoodle)
    {
        $this->bl_desativarmoodle = $bl_desativarmoodle;
    }

}

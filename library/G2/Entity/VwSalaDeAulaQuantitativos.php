<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\Id;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladeaulaquantitativos")
 * @Entity(repositoryClass="G2\Repository\SalaDeAula")
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwSalaDeAulaQuantitativos
{

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_saladeaula;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;

    /**
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false, length=1)
     */
    private $bl_ativa;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $id_modalidadesaladeaula
     * @Column(name="id_modalidadesaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_modalidadesaladeaula;

    /**
     * @var date $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="date", nullable=true, length=3)
     */
    private $dt_inicioinscricao;

    /**
     * @var date $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="date", nullable=true, length=3)
     */
    private $dt_fiminscricao;

    /**
     * @var datetime $dt_abertura
     * @Column(name="dt_abertura", type="datetime", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var string $st_localizacao
     * @Column(name="st_localizacao", type="string", nullable=true, length=255)
     */
    private $st_localizacao;

    /**
     * @var integer $id_tiposaladeaula
     * @Column(name="id_tiposaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_tiposaladeaula;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=true, length=4)
     */
    private $id_periodoletivo;

    /**
     * @var boolean $bl_usardoperiodoletivo
     * @Column(name="bl_usardoperiodoletivo", type="boolean", nullable=false, length=1)
     */
    private $bl_usardoperiodoletivo;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;

    /**
     * @var decimal $nu_diasencerramento
     * @Column(name="nu_diasencerramento", type="decimal", nullable=true, length=9)
     */
    private $nu_diasencerramento;

    /**
     * @var boolean $bl_semencerramento
     * @Column(name="bl_semencerramento", type="boolean", nullable=false, length=1)
     */
    private $bl_semencerramento;

    /**
     * @var decimal $nu_diasaluno
     * @Column(name="nu_diasaluno", type="decimal", nullable=true, length=9)
     */
    private $nu_diasaluno;

    /**
     * @var boolean $bl_semdiasaluno
     * @Column(name="bl_semdiasaluno", type="boolean", nullable=false, length=1)
     */
    private $bl_semdiasaluno;

    /**
     * @var string $st_modalidadesaladeaula
     * @Column(name="st_modalidadesaladeaula", type="string", nullable=false, length=255)
     */
    private $st_modalidadesaladeaula;

    /**
     * @var integer $nu_alunos
     * @Column(name="nu_alunos", type="integer", nullable=true, length=4)
     */
    private $nu_alunos;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @return boolean bl_ativa
     */
    public function getBl_ativa() {
        return $this->bl_ativa;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @return integer id_modalidadesaladeaula
     */
    public function getId_modalidadesaladeaula() {
        return $this->id_modalidadesaladeaula;
    }

    /**
     * @return date dt_inicioinscricao
     */
    public function getDt_inicioinscricao() {
        return $this->dt_inicioinscricao;
    }

    /**
     * @return date dt_fiminscricao
     */
    public function getDt_fiminscricao() {
        return $this->dt_fiminscricao;
    }

    /**
     * @return date dt_abertura
     */
    public function getDt_abertura() {
        return $this->dt_abertura;
    }

    /**
     * @return string st_localizacao
     */
    public function getSt_localizacao() {
        return $this->st_localizacao;
    }

    /**
     * @return integer id_tiposaladeaula
     */
    public function getId_tiposaladeaula() {
        return $this->id_tiposaladeaula;
    }

    /**
     * @return date dt_encerramento
     */
    public function getDt_encerramento() {
        return $this->dt_encerramento;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @return integer id_periodoletivo
     */
    public function getId_periodoletivo() {
        return $this->id_periodoletivo;
    }

    /**
     * @return boolean bl_usardoperiodoletivo
     */
    public function getBl_usardoperiodoletivo() {
        return $this->bl_usardoperiodoletivo;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    /**
     * @return decimal nu_diasencerramento
     */
    public function getNu_diasencerramento() {
        return $this->nu_diasencerramento;
    }

    /**
     * @return boolean bl_semencerramento
     */
    public function getBl_semencerramento() {
        return $this->bl_semencerramento;
    }

    /**
     * @return decimal nu_diasaluno
     */
    public function getNu_diasaluno() {
        return $this->nu_diasaluno;
    }

    /**
     * @return boolean bl_semdiasaluno
     */
    public function getBl_semdiasaluno() {
        return $this->bl_semdiasaluno;
    }

    /**
     * @return string st_modalidadesaladeaula
     */
    public function getSt_modalidadesaladeaula() {
        return $this->st_modalidadesaladeaula;
    }

    /**
     * @return integer nu_alunos
     */
    public function getNu_alunos() {
        return $this->nu_alunos;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

}
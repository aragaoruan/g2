<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_perfilreferenciaintegracao")
 * @Entity
 * @EntityLog
 */
class PerfilReferenciaIntegracao
{

    /**
     * @var integer $id_perfilreferenciaintegracao
     * @Column(name="id_perfilreferenciaintegracao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_perfilreferenciaintegracao;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", nullable=false, referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var UsuarioPerfilEntidadeReferencia $id_perfilreferencia
     * @ManyToOne(targetEntity="UsuarioPerfilEntidadeReferencia")
     * @JoinColumn(name="id_perfilreferencia", nullable=false, referencedColumnName="id_perfilreferencia")
     */
    private $id_perfilreferencia;

    /**
     * @var SalaDeAulaIntegracao $id_saladeaulaintegracao
     * @ManyToOne(targetEntity="SalaDeAulaIntegracao")
     * @JoinColumn(name="id_saladeaulaintegracao", nullable=false, referencedColumnName="id_saladeaulaintegracao")
     */
    private $id_saladeaulaintegracao;

    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", length=255, nullable=false)
     */
    private $st_codsistema;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(type="datetime", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=true, length=1)
     */
    private $bl_encerrado;

    /**
     * @var \DateTime $dt_tentativaencerramento
     * @Column(type="datetime", nullable=false, name="dt_tentativaencerramento")
     */
    private $dt_tentativaencerramento;

    /**
     * @return int
     */
    public function getId_perfilreferenciaintegracao()
    {
        return $this->id_perfilreferenciaintegracao;
    }

    /**
     * @param int $id_perfilreferenciaintegracao
     * @return $this
     */
    public function setId_perfilreferenciaintegracao($id_perfilreferenciaintegracao)
    {
        $this->id_perfilreferenciaintegracao = $id_perfilreferenciaintegracao;
        return $this;
    }

    /**
     * @return Sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param Sistema $id_sistema
     * @return $this
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return UsuarioPerfilEntidadeReferencia
     */
    public function getId_perfilreferencia()
    {
        return $this->id_perfilreferencia;
    }

    /**
     * @param UsuarioPerfilEntidadeReferencia $id_perfilreferencia
     * @return $this
     */
    public function setId_perfilreferencia($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
        return $this;
    }

    /**
     * @return SalaDeAulaIntegracao
     */
    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param SalaDeAulaIntegracao $id_saladeaulaintegracao
     * @return $this
     */
    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistema
     * @return $this
     */
    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bool $bl_encerrado
     * @return $this
     */
    public function setBl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_tentativaencerramento()
    {
        return $this->dt_tentativaencerramento;
    }

    /**
     * @param \DateTime $dt_tentativaencerramento
     * @return $this
     */
    public function setDt_tentativaencerramento($dt_tentativaencerramento)
    {
        $this->dt_tentativaencerramento = $dt_tentativaencerramento;
        return $this;
    }
}
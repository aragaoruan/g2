<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipoenvio")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoEnvio
{

    const EMAIL = 3;
    const HTML = 4;
    const SMS = 5;

    /**
     *
     * @var integer $id_tipoenvio
     * @Column(name="id_tipoenvio", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoenvio;

    /**
     *
     * @var string $st_tipoenvio
     * @Column(name="st_tipoenvio", type="string", nullable=false, length=100)
     */
    private $st_tipoenvio;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true, length=100)
     */
    private $st_descricao;

    public function getId_tipoenvio ()
    {
        return $this->id_tipoenvio;
    }

    public function setId_tipoenvio ($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    public function getSt_tipoenvio ()
    {
        return $this->st_tipoenvio;
    }

    public function setSt_tipoenvio ($st_tipoenvio)
    {
        $this->st_tipoenvio = $st_tipoenvio;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
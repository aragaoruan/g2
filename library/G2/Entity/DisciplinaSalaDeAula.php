<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table("tb_disciplinasaladeaula")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @EntityLog
 */
class DisciplinaSalaDeAula extends G2Entity
{

    /**
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     */
    private $id_saladeaula;


    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }


}

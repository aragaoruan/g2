<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_produtoarea")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimarães@gmail.com> 
 */

use G2\G2Entity;

class ProdutoArea extends G2Entity
{

    /**
     * @var integer $id_produtoarea
     * @Column(name="id_produtoarea", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtoarea;

    /**
     *
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    
    /**
     *
     * @var integer $id_areaconhecimento
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", nullable=false, referencedColumnName="id_areaconhecimento")
     */
    private $id_areaconhecimento;
    
    
	/**
	 * @return the $id_produtoarea
	 */
	public function getId_produtoarea() {
		return $this->id_produtoarea;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @param number $id_produtoarea
	 */
	public function setId_produtoarea($id_produtoarea) {
		$this->id_produtoarea = $id_produtoarea;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param number $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
		return $this;
	}

}
<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidaderecursiva")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwEntidadeRecursiva {

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;


    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=2500)
     */
    private $st_nomeentidade;


    /**
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=true)
     */
    private $id_entidadepai;


    /**
     * @var integer $nu_nivelhierarquia
     * @Column(name="nu_nivelhierarquia", type="integer", nullable=true)
     */
    private $nu_nivelhierarquia;


    /**
     * @var string $cadeiaentidades
     * @Column(name="cadeiaentidades", type="string", nullable=true, length=30)
     */
    private $cadeiaentidades;

    /**
     * @param string $cadeiaentidades
     */
    public function setCadeiaentidades($cadeiaentidades)
    {
        $this->cadeiaentidades = $cadeiaentidades;
    }

    /**
     * @return string
     */
    public function getCadeiaentidades()
    {
        return $this->cadeiaentidades;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidadepai
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
    }

    /**
     * @return int
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param int $nu_nivelhierarquia
     */
    public function setNu_nivelhierarquia($nu_nivelhierarquia)
    {
        $this->nu_nivelhierarquia = $nu_nivelhierarquia;
    }

    /**
     * @return int
     */
    public function getNu_nivelhierarquia()
    {
        return $this->nu_nivelhierarquia;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }




}
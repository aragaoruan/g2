<?php

namespace G2\Entity;

/**
 * * @SWG\Definition(
 *     required={},
 *     @SWG\Xml(name="CategoriaOcorrencia")
 * )
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_categoriaocorrencia")
 * @Entity
 * @author Kayo Silva <rafael.oliveira@unyleya.com.br>
 */
class CategoriaOcorrencia
{

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     *
     * @var integer $id_categoriaocorrencia
     * @Column(name="id_categoriaocorrencia", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @SWG\Property(format="integer")
     */
    private $id_categoriaocorrencia;

    /**
     * @var integer $id_tipoocorrencia
     * @ManyToOne(targetEntity="TipoOcorrencia", cascade={"all"})
     * @JoinColumn(name="id_tipoocorrencia", nullable=false, referencedColumnName="id_tipoocorrencia")
     */
    private $id_tipoocorrencia;

    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var string $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade", cascade={"all"})
     * @JoinColumn(name="id_entidadecadastro", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var string $st_categoriaocorrencia
     * @Column(name="st_categoriaocorrencia", nullable=false, type="string", length=250)
     * @SWG\Property(format="string")
     */
    private $st_categoriaocorrencia;

    /**
     *
     * @var string $dt_cadastro
     * @Column(name="dt_cadastro", nullable=false, type="datetime2")
     */
    private $dt_cadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", nullable=false, type="boolean")
     */
    private $bl_ativo;

    public function getId_categoriaocorrencia()
    {
        return $this->id_categoriaocorrencia;
    }

    public function setId_categoriaocorrencia($id_categoriaocorrencia)
    {
        $this->id_categoriaocorrencia = $id_categoriaocorrencia;
        return $this;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getSt_categoriaocorrencia()
    {
        return $this->st_categoriaocorrencia;
    }

    public function setSt_categoriaocorrencia($st_categoriaocorrencia)
    {
        $this->st_categoriaocorrencia = $st_categoriaocorrencia;
        return $this;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
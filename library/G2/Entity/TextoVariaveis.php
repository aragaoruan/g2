<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_textovariaveis")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TextoVariaveis extends G2Entity
{

    /**
     *
     * @var integer $id_textovariaveis
     * @Column(name="id_textovariaveis", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_textovariaveis;

    /**
     *
     * @var TextoCategoria $id_textocategoria
     * @ManyToOne(targetEntity="TextoCategoria")
     * @JoinColumn(name="id_textocategoria", nullable=false, referencedColumnName="id_textocategoria")
     */
    private $id_textocategoria;

    /**
     * @var string $st_camposubstituir
     * @Column(name="st_camposubstituir", type="string", nullable=true, length=100)
     */
    private $st_camposubstituir;

    /**
     * @var string $st_textovariaveis
     * @Column(name="st_textovariaveis", type="string", nullable=false, length=100)
     */
    private $st_textovariaveis;

    /**
     * @var string $st_mascara
     * @Column(name="st_mascara", type="string", nullable=true, length=100)
     */
    private $st_mascara;

    /**
     * @var TipoTextoVariavel $id_tipotextovariavel
     * @ManyToOne(targetEntity="TipoTextoVariavel")
     * @JoinColumn(name="id_tipotextovariavel", referencedColumnName="id_tipotextovariavel")
     */
    private $id_tipotextovariavel;

    /**
     * @var boolean $bl_exibicao
     * @Column(name="bl_exibicao", type="boolean", nullable=false)
     */
    private $bl_exibicao;

    public function getId_textovariaveis ()
    {
        return $this->id_textovariaveis;
    }

    public function setId_textovariaveis ($id_textovariaveis)
    {
        $this->id_textovariaveis = $id_textovariaveis;
        return $this;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria (TextoCategoria $id_textocategoria)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getSt_camposubstituir ()
    {
        return $this->st_camposubstituir;
    }

    public function setSt_camposubstituir ($st_camposubstituir)
    {
        $this->st_camposubstituir = $st_camposubstituir;
        return $this;
    }

    public function getSt_textovariaveis ()
    {
        return $this->st_textovariaveis;
    }

    public function setSt_textovariaveis ($st_textovariaveis)
    {
        $this->st_textovariaveis = $st_textovariaveis;
        return $this;
    }

    public function getSt_mascara ()
    {
        return $this->st_mascara;
    }

    public function setSt_mascara ($st_mascara)
    {
        $this->st_mascara = $st_mascara;
        return $this;
    }

    public function getId_tipotextovariavel ()
    {
        return $this->id_tipotextovariavel;
    }

    public function setId_tipotextovariavel (TipoTextoVariavel $id_tipotextovariavel)
    {
        $this->id_tipotextovariavel = $id_tipotextovariavel;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_exibicao()
    {
        return $this->bl_exibicao;
    }

    /**
     * @param boolean $bl_exibicao
     */
    public function setBl_exibicao($bl_exibicao)
    {
        $this->bl_exibicao = $bl_exibicao;
    }

}

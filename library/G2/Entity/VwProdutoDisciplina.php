<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produtodisciplina")
 * @Entity
 * @EntityView
 * @author Felipe Pastor
 */
class VwProdutoDisciplina
{

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produto;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var integer  $nu_cargahoraria
     * @Column(type="integer", nullable=false, name="nu_cargahoraria")
     */
    private $nu_cargahoraria;


    /**
     * @var integer $id_professor
     * @Column(type="integer", nullable=true, name="id_professor")
     */
    private $id_professor;

    /**
     * @var string $st_professor
     * @Column(name="st_professor", type="string", nullable=true, length=300)
     */
    private $st_professor;

    public function getId_professor ()
    {
        return $this->id_professor;
    }

    /**
     * @param integer $id_professor
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setId_professor ($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param integer $nu_cargahoraria
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * 
     * @return integer
     */
    public function getId_produto ()
    {
        return $this->id_produto;
    }

    /**
     * 
     * @return integer
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * 
     * @return integer
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     * 
     * @return string
     */
    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    /**
     * 
     * @return string
     */
    public function getSt_professor ()
    {
        return $this->st_professor;
    }

    /**
     * 
     * @param integer $id_produto
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setId_produto ($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * 
     * @param integer $id_projetopedagogico
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setId_projetopedagogico ($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * 
     * @param integer $id_disciplina
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * 
     * @param string $st_disciplina
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }


    /**
     * 
     * @param string $st_professor
     * @return \G2\Entity\VwProdutoDisciplina
     */
    public function setSt_professor ($st_professor)
    {
        $this->st_professor = $st_professor;
        return $this;
    }

}

<?php

namespace G2\Entity;

use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @SWG\Definition(@SWG\Xml(name="DisciplinaConteudo"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_disciplinaconteudo")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class DisciplinaConteudo
{

    /**
     * @SWG\Property()
     * @Id
     * @var integer $id_disciplinaconteudo
     * @Column(name="id_disciplinaconteudo", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplinaconteudo;

    /**
     * @SWG\Property()
     * @var integer $id_disciplina
     * @ManyToOne(targetEntity="Disciplina")
     * @JoinColumn(name="id_disciplina", nullable=false, referencedColumnName="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @SWG\Property()
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @SWG\Property(example="Descrição do Conteúdo")
     * @var string $st_descricaoconteudo
     * @Column(name="st_descricaoconteudo", type="string", nullable=true, length=100)
     */
    private $st_descricaoconteudo;

    /**
     * @SWG\Property()
     * @var integer $id_formatoarquivo
     * @ManyToOne(targetEntity="FormatoArquivo")
     * @JoinColumn(name="id_formatoarquivo", nullable=false, referencedColumnName="id_formatoarquivo")
     */
    private $id_formatoarquivo;

    /**
     * @SWG\Property()
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=3)
     */
    private $dt_cadastro;

    /**
     * @SWG\Property()
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=3, options="{default: 1}")
     */
    private $bl_ativo;

    /**
     * @return integer id_disciplinaconteudo
     */
    public function getId_disciplinaconteudo()
    {
        return $this->id_disciplinaconteudo;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return string st_descricaoconteudo
     */
    public function getSt_descricaoconteudo()
    {
        return $this->st_descricaoconteudo;
    }

    /**
     * @return string st_formato
     */
    public function getSt_formato()
    {
        return $this->st_formato;
    }

    /**
     * @return date dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return bit $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return integer $id_formatoarquivo
     */
    public function getId_formatoarquivo()
    {
        return $this->id_formatoarquivo;
    }

    /**
     * @return int $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_disciplinaconteudo
     */
    public function setId_disciplinaconteudo($id_disciplinaconteudo)
    {
        $this->id_disciplinaconteudo = $id_disciplinaconteudo;
        return $this;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @param st_descricaoconteudo
     */
    public function setSt_descricaoconteudo($st_descricaoconteudo)
    {
        $this->st_descricaoconteudo = $st_descricaoconteudo;
        return $this;
    }

    /**
     * @param id_formatoarquivo
     */
    public function setId_formatoarquivo($id_formatoarquivo)
    {
        $this->id_formatoarquivo = $id_formatoarquivo;
        return $this;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param bit $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

}

<?php

/**
 * Entity ContratoResponsavel
 * @author: Denise Xavier <denise.xavier@unyleya.com.br>
 * @since: 16/10/2014
 */
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_contratoresponsavel")
 * @Entity
 */
class ContratoResponsavel {


    /**
     * @var integer $id_contratoresponsavel
     * @Column(name="id_contratoresponsavel", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_contratoresponsavel;

    /**
     * @var int $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @var Contrato $id_contrato
     * @ManyToOne(targetEntity="Contrato")
     * @JoinColumn(name="id_contrato", referencedColumnName="id_contrato")
     */
    private $id_contrato;

    /**
     * @var TipoContratoResponsavel $id_tipocontratoresponsavel
     * @ManyToOne(targetEntity="TipoContratoResponsavel")
     * @JoinColumn(name="id_tipocontratoresponsavel", referencedColumnName="id_tipocontratoresponsavel")
     */
    private $id_tipocontratoresponsavel;

    /**
     * @var boolean $nu_porcentagem
     * @Column(name="nu_porcentagem", type="integer", nullable=false)
     */
    private $nu_porcentagem;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    /**
     * @var Entidade $id_entidaderesponsavel
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidaderesponsavel", referencedColumnName="id_entidade")
     */
    private $id_entidaderesponsavel;

    /**
     * @var Venda $id_venda
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     */
    private $id_venda;


    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    public function setId_contratoresponsavel($id_contratoresponsavel)
    {
        $this->id_contratoresponsavel = $id_contratoresponsavel;
    }

    public function getId_contratoresponsavel()
    {
        return $this->id_contratoresponsavel;
    }

    public function setId_entidaderesponsavel($id_entidaderesponsavel)
    {
        $this->id_entidaderesponsavel = $id_entidaderesponsavel;
    }

    public function getId_entidaderesponsavel()
    {
        return $this->id_entidaderesponsavel;
    }

    public function setId_tipocontratoresponsavel($id_tipocontratoresponsavel)
    {
        $this->id_tipocontratoresponsavel = $id_tipocontratoresponsavel;
    }

    public function getId_tipocontratoresponsavel()
    {
        return $this->id_tipocontratoresponsavel;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    public function getId_venda()
    {
        return $this->id_venda;
    }

    public function setNu_porcentagem($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
    }

    public function getNu_porcentagem()
    {
        return $this->nu_porcentagem;
    }







}

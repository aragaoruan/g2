<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_entidaderelacao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EntidadeRelacao
{

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     */
    private $id_entidade;

    /**
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=false)
     * @Id
     */
    private $id_entidadepai;

    /**
     * @var EntidadeClasse $id_entidadeclasse
     * @ManyToOne(targetEntity="EntidadeClasse")
     * @JoinColumn(name="id_entidadeclasse", referencedColumnName="id_entidadeclasse")
     */
    private $id_entidadeclasse;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     * @Id
     */
    private $id_usuariocadastro;

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param $id_entidadepai
     * @return $this
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    /**
     * @return EntidadeClasse
     */
    public function getId_entidadeclasse()
    {
        return $this->id_entidadeclasse;
    }

    /**
     * @param $id_entidadeclasse
     * @return $this
     */
    public function setId_entidadeclasse($id_entidadeclasse)
    {
        $this->id_entidadeclasse = $id_entidadeclasse;
        return $this;
    }

    /**
     * @param $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }
}
<?php

namespace G2\Entity;

/**
 *
 * @Table(name="tb_declaracaoaluno")
 * @Entity
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since: 02/07/2015
 */
class DeclaracaoAluno
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_declaracao
     * @Column(name="id_declaracao", type="integer", nullable=false, length=4)
     */
    private $id_declaracao;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @return integer id_declaracao
     */
    public function getId_declaracao()
    {
        return $this->id_declaracao;
    }

    /**
     * @param id_declaracao
     */
    public function setId_declaracao($id_declaracao)
    {
        $this->id_declaracao = $id_declaracao;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }
}
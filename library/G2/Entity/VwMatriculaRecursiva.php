<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matricularecursiva")
 * @Entity
 * @EntityView
 * @author Ruan Aragao <ruan.aragao@unyleya.com.br>
 */

class VwMatriculaRecursiva extends G2Entity {
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_matriculaorigem
     * @Column(name="id_matriculaorigem", type="integer")
     * @Id
     */
    private $id_matriculaorigem;
    /**
     * @var integer $nu_matriculaorigem
     * @Column(name="nu_matriculaorigem", type="integer")
     * @Id
     */
    private $nu_matriculaorigem;
    /**
     * @var integer $bl_ativo
     * @Column(name="bl_ativo", type="integer")
     * @Id
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    /**
     * @param int $id_matriculaorigem
     */
    public function setId_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
    }

    /**
     * @return int
     */
    public function getNu_matriculaorigem()
    {
        return $this->nu_matriculaorigem;
    }

    /**
     * @param int $nu_matriculaorigem
     */
    public function setNu_matriculaorigem($nu_matriculaorigem)
    {
        $this->nu_matriculaorigem = $nu_matriculaorigem;
    }

    /**
     * @return int
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param int $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }


}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_cancelamento")
 * @Entity(repositoryClass="\G2\Repository\Cancelamento")
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @EntityLog
 */
class Cancelamento extends G2Entity
{
    /**
     * @Id
     * @var integer $id_cancelamento
     * @Column(name="id_cancelamento", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cancelamento;
    /**
     * @var datetime2 $dt_solicitacao
     * @Column(name="dt_solicitacao", type="datetime2", nullable=false, length=8)
     */
    private $dt_solicitacao;

    /**
     * @var datetime2 $dt_finalizacao
     * @Column(name="dt_finalizacao", type="datetime2", nullable=false, length=8)
     */
    private $dt_finalizacao;

    /**
     * @var Evolucao $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var integer $nu_diascancelamento
     * @Column(name="nu_diascancelamento", type="integer", nullable=false, length=4)
     */
    private $nu_diascancelamento;
    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string", nullable=true, length=255)
     */
    private $st_observacao;

    /**
     * @var string $st_observacaocalculo
     * @Column(name="st_observacaocalculo", type="string", nullable=true, length=255)
     */
    private $st_observacaocalculo;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true, length=4)
     */
    private $nu_cargahoraria;
    /**
     * @var integer $nu_cargahorariacursada
     * @Column(name="nu_cargahorariacursada", type="integer", nullable=true, length=4)
     */
    private $nu_cargahorariacursada;
    /**
     * @var decimal $nu_valorbruto
     * @Column(name="nu_valorbruto", type="decimal", nullable=true, length=9)
     */
    private $nu_valorbruto;
    /**
     * @var decimal $nu_valornegociado
     * @Column(name="nu_valornegociado", type="decimal", nullable=true, length=9)
     */
    private $nu_valornegociado;
    /**
     * @var decimal $nu_valordiferenca
     * @Column(name="nu_valordiferenca", type="decimal", nullable=true, length=9)
     */
    private $nu_valordiferenca;
    /**
     * @var decimal $nu_valorhoraaula
     * @Column(name="nu_valorhoraaula", type="decimal", nullable=true, length=9)
     */
    private $nu_valorhoraaula;
    /**
     * @var decimal $nu_valormaterial
     * @Column(name="nu_valormaterial", type="decimal", nullable=true, length=9)
     */
    private $nu_valormaterial;
    /**
     * @var decimal $nu_valorutilizadomaterial
     * @Column(name="nu_valorutilizadomaterial", type="decimal", nullable=true, length=9)
     */
    private $nu_valorutilizadomaterial;

    /**
     * @var decimal $nu_valortotal
     * @Column(name="nu_valortotal", type="decimal", nullable=true, length=9)
     */
    private $nu_valortotal;

    /**
     * @var decimal $nu_totalutilizado
     * @Column(name="nu_totalutilizado", type="decimal", nullable=true, length=9)
     */
    private $nu_totalutilizado;
    /**
     * @var decimal $nu_valordevolucao
     * @Column(name="nu_valordevolucao", type="decimal", nullable=true, length=9)
     */
    private $nu_valordevolucao;

    /**
     * @var boolean $bl_cartagerada
     * @Column(name="bl_cartagerada", type="boolean", nullable=true, length=1)
     */
    private $bl_cartagerada;

    /**
     * @var boolean $bl_cancelamentofinalizado
     * @Column(name="bl_cancelamentofinalizado", type="boolean", nullable=true, length=1)
     */
    private $bl_cancelamentofinalizado;

    /**
     * @var decimal $nu_valorcarta
     * @Column(name="nu_valorcarta", type="decimal", nullable=true, length=9)
     */
    private $nu_valorcarta;

    /**
     * @var decimal $nu_valormultadevolucao
     * @Column(name="nu_valormultadevolucao", type="decimal", nullable=true, length=9)
     */
    private $nu_valormultadevolucao;

    /**
     * @var decimal $nu_valormultacarta
     * @Column(name="nu_valormultacarta", type="decimal", nullable=true, length=5)
     */
    private $nu_valormultacarta;

    /**
     * @var decimal $nu_multaporcentagemcarta
     * @Column(name="nu_multaporcentagemcarta", type="decimal", nullable=true, length=5)
     */
    private $nu_multaporcentagemcarta;
    /**
     * @var decimal $nu_multaporcentagemdevolucao
     * @Column(name="nu_multaporcentagemdevolucao", type="decimal", nullable=true, length=9)
     */
    private $nu_multaporcentagemdevolucao;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, options="{default: GETDATE()}")
     */
    private $dt_cadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var Ocorrencia $id_ocorrencia
     * @ManyToOne(targetEntity="Ocorrencia")
     * @JoinColumn(name="id_ocorrencia", referencedColumnName="id_ocorrencia", nullable=true)
     */
    private $id_ocorrencia;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario", nullable=true)
     */
    private $id_usuario;

    /**
     * @var datetime2 $dt_referencia
     * @Column(name="dt_referencia", type="datetime", nullable=false, length=8)
     */
    private $dt_referencia;

    /**
     * @var datetime2 $dt_encaminhamentofinanceiro
     * @Column(name="dt_encaminhamentofinanceiro", type="datetime2", nullable=false, length=8)
     */
    private $dt_encaminhamentofinanceiro;

    /**
     * @var datetime2 $dt_desistiu
     * @column(name="dt_desistiu", type="datetime2", nullable=false, length=8)
     */
    private $dt_desistiu;

    /**
     * @var boolean $bl_concluir
     * @Column(name="bl_concluir", type="boolean", nullable=true)
     */
    private $bl_concluir;
    /**
     * @var boolean $bl_finalizar
     * @Column(name="bl_finalizar", type="boolean", nullable=true)
     */
    private $bl_finalizar;
    /**
     * @return datetime2
     */
    public function getDt_referencia()
    {
        return $this->dt_referencia;
    }

    /**
     * @param datetime2 $dt_referencia
     */
    public function setDt_referencia($dt_referencia)
    {
        $this->dt_referencia = $dt_referencia;
    }

    /**
     * @return datetime2
     */
    public function getDt_encaminhamentofinanceiro()
    {
        return $this->dt_encaminhamentofinanceiro;
    }

    /**
     * @param datetime2 $dt_encaminhamentofinanceiro
     */
    public function setDt_encaminhamentofinanceiro($dt_encaminhamentofinanceiro)
    {
        $this->dt_encaminhamentofinanceiro = $dt_encaminhamentofinanceiro;
    }

    /**
     * @return datetime dt_solicitacao
     */
    public function getDt_solicitacao()
    {
        return $this->dt_solicitacao;
    }

    /**
     * @param dt_solicitacao
     */
    public function setDt_solicitacao($dt_solicitacao)
    {
        $this->dt_solicitacao = $dt_solicitacao;
        return $this;
    }

    /**
     * @return integer id_cancelamento
     */
    public function getId_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param id_cancelamento
     */
    public function setId_cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
        return $this;
    }

    /**
     * @return Evolucao id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param $id_evolucao
     * @return $this
     */
    public function setId_evolucao(Evolucao $id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer nu_diascancelamento
     */
    public function getNu_diascancelamento()
    {
        return $this->nu_diascancelamento;
    }

    /**
     * @param nu_diascancelamento
     */
    public function setNu_diascancelamento($nu_diascancelamento)
    {
        $this->nu_diascancelamento = $nu_diascancelamento;
        return $this;
    }

    /**
     * @return string st_observacao
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @param st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
        return $this;
    }


    /**
     * @return integer nu_cargahoraria
     */
    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param $nu_cargahoraria
     * @return $this
     */
    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return integer nu_cargahorariacursada
     */
    public function getNu_cargahorariacursada()
    {
        return $this->nu_cargahorariacursada;
    }

    /**
     * @param nu_cargahorariacursada
     */
    public function setNu_cargahorariacursada($nu_cargahorariacursada)
    {
        $this->nu_cargahorariacursada = $nu_cargahorariacursada;
        return $this;
    }

    /**
     * @return decimal nu_valorbruto
     */
    public function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @param nu_valorbruto
     */
    public function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
        return $this;
    }

    /**
     * @return decimal nu_valornegociado
     */
    public function getNu_valornegociado()
    {
        return $this->nu_valornegociado;
    }

    /**
     * @param nu_valornegociado
     */
    public function setNu_valornegociado($nu_valornegociado)
    {
        $this->nu_valornegociado = $nu_valornegociado;
        return $this;
    }

    /**
     * @return decimal nu_valordiferenca
     */
    public function getNu_valordiferenca()
    {
        return $this->nu_valordiferenca;
    }

    /**
     * @param nu_valordiferenca
     */
    public function setNu_valordiferenca($nu_valordiferenca)
    {
        $this->nu_valordiferenca = $nu_valordiferenca;
        return $this;
    }

    /**
     * @return decimal nu_valorhoraaula
     */
    public function getNu_valorhoraaula()
    {
        return $this->nu_valorhoraaula;
    }

    /**
     * @param $nu_valorhoraaula
     * @return $this
     */
    public function setNu_valorhoraaula($nu_valorhoraaula)
    {
        $this->nu_valorhoraaula = $nu_valorhoraaula;
        return $this;
    }

    /**
     * @return decimal nu_valormaterial
     */
    public function getNu_valormaterial()
    {
        return $this->nu_valormaterial;
    }

    /**
     * @param nu_valormaterial
     */
    public function setNu_valormaterial($nu_valormaterial)
    {
        $this->nu_valormaterial = $nu_valormaterial;
        return $this;
    }

    /**
     * @return decimal nu_valorutilizadomaterial
     */
    public function getNu_valorutilizadomaterial()
    {
        return $this->nu_valorutilizadomaterial;
    }

    /**
     * @param nu_valorutilizadomaterial
     */
    public function setNu_valorutilizadomaterial($nu_valorutilizadomaterial)
    {
        $this->nu_valorutilizadomaterial = $nu_valorutilizadomaterial;
        return $this;
    }

    /**
     * @return decimal nu_valortotal
     */
    public function getNu_valortotal()
    {
        return $this->nu_valortotal;
    }

    /**
     * @param nu_valortotal
     */
    public function setNu_valortotal($nu_valortotal)
    {
        $this->nu_valortotal = $nu_valortotal;
        return $this;
    }


    /**
     * @return decimal nu_totalutilizado
     */
    public function getNu_totalutilizado()
    {
        return $this->nu_totalutilizado;
    }

    /**
     * @param nu_totalutilizado
     */
    public function setNu_totalutilizado($nu_totalutilizado)
    {
        $this->nu_totalutilizado = $nu_totalutilizado;
        return $this;
    }

    /**
     * @return decimal nu_valordevolucao
     */
    public function getNu_valordevolucao()
    {
        return $this->nu_valordevolucao;
    }

    /**
     * @param nu_valordevolucao
     */
    public function setNu_valordevolucao($nu_valordevolucao)
    {
        $this->nu_valordevolucao = $nu_valordevolucao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_observacaocalculo()
    {
        return $this->st_observacaocalculo;
    }

    /**
     * @param $st_observacaocalculo
     * @return $this
     */
    public function setSt_observacaocalculo($st_observacaocalculo)
    {
        $this->st_observacaocalculo = $st_observacaocalculo;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_cancelamentofinalizado()
    {
        return $this->bl_cancelamentofinalizado;
    }

    /**
     * @param boolean $bl_cancelamentofinalizado
     * @return $this
     */
    public function setBl_cancelamentofinalizado($bl_cancelamentofinalizado)
    {
        $this->bl_cancelamentofinalizado = $bl_cancelamentofinalizado;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_cartagerada()
    {
        return $this->bl_cartagerada;
    }

    /**
     * @param boolean $bl_cartagerada
     */
    public function setBl_cartagerada($bl_cartagerada)
    {
        $this->bl_cartagerada = $bl_cartagerada;
    }


    /**
     * @return decimal nu_multaporcentagemcarta
     */
    public function getNu_multaporcentagemcarta()
    {
        return $this->nu_multaporcentagemcarta;
    }

    /**
     * @param nu_multaporcentagemcarta
     */
    public function setNu_multaporcentagemcarta($nu_multaporcentagemcarta)
    {
        $this->nu_multaporcentagemcarta = $nu_multaporcentagemcarta;
        return $this;
    }

    /**
     * @return decimal nu_multaporcentagemdevolucao
     */
    public function getNu_multaporcentagemdevolucao()
    {
        return $this->nu_multaporcentagemdevolucao;
    }

    /**
     * @param nu_multaporcentagemdevolucao
     */
    public function setNu_multaporcentagemdevolucao($nu_multaporcentagemdevolucao)
    {
        $this->nu_multaporcentagemdevolucao = $nu_multaporcentagemdevolucao;
        return $this;
    }


    /**
     * @return decimal nu_valormultacarta
     */
    public function getNu_valormultacarta()
    {
        return $this->nu_valormultacarta;
    }

    /**
     * @param nu_valormultacarta
     */
    public function setNu_valormultacarta($nu_valormultacarta)
    {
        $this->nu_valormultacarta = $nu_valormultacarta;
        return $this;
    }

    /**
     * @return decimal nu_valormultadevolucao
     */
    public function getNu_valormultadevolucao()
    {
        return $this->nu_valormultadevolucao;
    }

    /**
     * @param nu_valormultadevolucao
     */
    public function setNu_valormultadevolucao($nu_valormultadevolucao)
    {
        $this->nu_valormultadevolucao = $nu_valormultadevolucao;
        return $this;
    }


    /**
     * @return decimal nu_valorcarta
     */
    public function getNu_valorcarta()
    {
        return $this->nu_valorcarta;
    }

    /**
     * @param nu_valorcarta
     */
    public function setNu_valorcarta($nu_valorcarta)
    {
        $this->nu_valorcarta = $nu_valorcarta;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_finalizacao()
    {
        return $this->dt_finalizacao;
    }

    /**
     * @param datetime2 $dt_finalizacao
     */
    public function setDt_finalizacao($dt_finalizacao)
    {
        $this->dt_finalizacao = $dt_finalizacao;
    }

    /**
     * @return \DateTime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     * @return Cancelamento
     */
    public function setDt_cadastro(\DateTime $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return Cancelamento
     */
    public function setId_usuariocadastro(Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return Ocorrencia
     */
    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param Ocorrencia $id_ocorrencia
     * @return Cancelamento
     */
    public function setId_ocorrencia(Ocorrencia $id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param Usuario $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return datetime2
     */
    public function getDt_desistiu()
    {
        return $this->dt_desistiu;
    }

    /**
     * @param datetime2 $dt_desistiu
     */
    public function setDt_desistiu($dt_desistiu)
    {
        $this->dt_desistiu = $dt_desistiu;
    }

    /**
     * @return bool
     */
    public function getBl_concluir()
    {
        return $this->bl_concluir;
    }

    /**
     * @param bool $bl_concluir
     */
    public function setBl_concluir($bl_concluir)
    {
        $this->bl_concluir = $bl_concluir;
    }

    /**
     * @return bool
     */
    public function getBl_finalizar()
    {
        return $this->bl_finalizar;
    }

    /**
     * @param bool $bl_finalizar
     */
    public function setBl_finalizar($bl_finalizar)
    {
        $this->bl_finalizar = $bl_finalizar;
    }




}

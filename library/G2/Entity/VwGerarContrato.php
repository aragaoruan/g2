<?php

namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="vw_gerarcontrato")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwGerarContrato
{

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_contrato;

    /**
     * @var date $dt_nascimentoresponsavel
     * @Column(name="dt_nascimentoresponsavel", type="date", nullable=true, length=3)
     */
    private $dt_nascimentoresponsavel;

    /**
     * @var date $dt_nascimentoaluno
     * @Column(name="dt_nascimentoaluno", type="date", nullable=true, length=3)
     */
    private $dt_nascimentoaluno;

    /**
     * @var datetime2 $dt_confirmacao_venda
     * @Column(name="dt_confirmacao_venda", type="datetime2", nullable=true, length=8)
     */
    private $dt_confirmacao_venda;

    /**
     * @var integer $id_usuarioresponsavel
     * @Column(name="id_usuarioresponsavel", type="integer", nullable=true, length=4)
     */
    private $id_usuarioresponsavel;

    /**
     * @var integer $id_usuarioaluno
     * @Column(name="id_usuarioaluno", type="integer", nullable=false, length=4)
     */
    private $id_usuarioaluno;

    /**
     * @var integer $st_numerotitulo
     * @Column(name="st_numerotitulo", type="integer", nullable=true, length=4)
     */
    private $st_numerotitulo;

    /**
     * @var integer $st_areatitulo
     * @Column(name="st_areatitulo", type="integer", nullable=true, length=4)
     */
    private $st_areatitulo;

    /**
     * @var integer $st_zonatitulo
     * @Column(name="st_zonatitulo", type="integer", nullable=true, length=4)
     */
    private $st_zonatitulo;

    /**
     * @var integer $nu_parcelas_venda
     * @Column(name="nu_parcelas_venda", type="integer", nullable=true, length=4)
     */
    private $nu_parcelas_venda;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_diamensalidade
     * @Column(name="nu_diamensalidade", type="integer", nullable=true, length=4)
     */
    private $nu_diamensalidade;

    /**
     * @var integer $st_etnia
     * @Column(name="st_etnia", type="integer", nullable=true, length=4)
     */
    private $st_etnia;

    /**
     * @var integer $st_tiposanguineo
     * @Column(name="st_tiposanguineo", type="integer", nullable=true, length=4)
     */
    private $st_tiposanguineo;

    /**
     * @var integer $st_alergias
     * @Column(name="st_alergias", type="integer", nullable=true, length=4)
     */
    private $st_alergias;

    /**
     * @var integer $st_glicemia
     * @Column(name="st_glicemia", type="integer", nullable=true, length=4)
     */
    private $st_glicemia;

    /**
     * @var integer $id_tipocontratoresponsavel_financeiro
     * @Column(name="id_tipocontratoresponsavel_financeiro", type="integer", nullable=false, length=4)
     */
    private $id_tipocontratoresponsavel_financeiro;

    /**
     * @var integer $st_ufcompletoaluno
     * @Column(name="st_ufcompletoaluno", type="integer", nullable=true, length=4)
     */
    private $st_ufcompletoaluno;

    /**
     * @var integer $st_nacionalidadealuno
     * @Column(name="st_nacionalidadealuno", type="integer", nullable=true, length=4)
     */
    private $st_nacionalidadealuno;

    /**
     * @var integer $nu_dia
     * @Column(name="nu_dia", type="integer", nullable=true, length=4)
     */
    private $nu_dia;

    /**
     * @var integer $nu_mes
     * @Column(name="nu_mes", type="integer", nullable=true, length=4)
     */
    private $nu_mes;

    /**
     * @var integer $nu_ano
     * @Column(name="nu_ano", type="integer", nullable=true, length=4)
     */
    private $nu_ano;

    /**
     * @var integer $st_numeroreservita
     * @Column(name="st_numeroreservita", type="integer", nullable=true, length=4)
     */
    private $st_numeroreservita;

    /**
     * @var integer $st_numerocertidao
     * @Column(name="st_numerocertidao", type="integer", nullable=true, length=4)
     */
    private $st_numerocertidao;

    /**
     * @var integer $st_livrocertidao
     * @Column(name="st_livrocertidao", type="integer", nullable=true, length=4)
     */
    private $st_livrocertidao;

    /**
     * @var integer $st_folhascertidao
     * @Column(name="st_folhascertidao", type="integer", nullable=true, length=4)
     */
    private $st_folhascertidao;

    /**
     * @var integer $st_descricaocartorio
     * @Column(name="st_descricaocartorio", type="integer", nullable=true, length=4)
     */
    private $st_descricaocartorio;

    /**
     * @var integer $st_cargo
     * @Column(name="st_cargo", type="integer", nullable=true, length=4)
     */
    private $st_cargo;

    /**
     * @var decimal $nu_valorliquido_venda
     * @Column(name="nu_valorliquido_venda", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido_venda;

    /**
     * @var string $st_nomecompletoaluno
     * @Column(name="st_nomecompletoaluno", type="string", nullable=false, length=300)
     */
    private $st_nomecompletoaluno;

    /**
     * @var string $st_cpfaluno
     * @Column(name="st_cpfaluno", type="string", nullable=true, length=11)
     */
    private $st_cpfaluno;

    /**
     * @var string $st_rgaluno
     * @Column(name="st_rgaluno", type="string", nullable=true, length=20)
     */
    private $st_rgaluno;

    /**
     * @var string $st_estadocivil
     * @Column(name="st_estadocivil", type="string", nullable=true, length=255)
     */
    private $st_estadocivil;

    /**
     * @var string $st_orgaoexpedidor
     * @Column(name="st_orgaoexpedidor", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpedidor;

    /**
     * @var string $st_rgresp
     * @Column(name="st_rgresp", type="string", nullable=true, length=20)
     */
    private $st_rgresp;

    /**
     * @var string $st_secretarioescolar
     * @Column(name="st_secretarioescolar", type="string", nullable=true, length=300)
     */
    private $st_secretarioescolar;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @var string $st_inicio
     * @Column(name="st_inicio", type="string", nullable=true, length=1000)
     */
    private $st_inicio;

    /**
     * @var string $st_turno
     * @Column(name="st_turno", type="string", nullable=true, length=1000)
     */
    private $st_turno;

    /**
     * @var string $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="string", nullable=true, length=1000)
     */
    private $nu_cargahoraria;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=1000)
     */
    private $st_turma;

    /**
     * @var string $st_razaosocial
     * @Column(name="st_razaosocial", type="string", nullable=false, length=800)
     */
    private $st_razaosocial;

    /**
     * @var string $st_cnpj
     * @Column(name="st_cnpj", type="string", nullable=true, length=15)
     */
    private $st_cnpj;

    /**
     * @var string $st_urlimglogo
     * @Column(name="st_urlimglogo", type="string", nullable=true, length=1000)
     */
    private $st_urlimglogo;

    /**
     * @var string $st_urlsite
     * @Column(name="st_urlsite", type="string", nullable=true, length=200)
     */
    private $st_urlsite;

    /**
     * @var string $st_polo
     * @Column(name="st_polo", type="string", nullable=true, length=150)
     */
    private $st_polo;

    /**
     * @var string $st_diretor
     * @Column(name="st_diretor", type="string", nullable=true, length=300)
     */
    private $st_diretor;

    /**
     * @var string $nu_telefonealuno
     * @Column(name="nu_telefonealuno", type="string", nullable=true, length=15)
     */
    private $nu_telefonealuno;

    /**
     * @var string $st_nomemae
     * @Column(name="st_nomemae", type="string", nullable=true, length=100)
     */
    private $st_nomemae;

    /**
     * @var string $st_nomepai
     * @Column(name="st_nomepai", type="string", nullable=true, length=255)
     */
    private $st_nomepai;

    /**
     * @var string $st_sexoaluno
     * @Column(name="st_sexoaluno", type="string", nullable=false, length=13)
     */
    private $st_sexoaluno;

    /**
     * @var string $st_projeto
     * @Column(name="st_projeto", type="string", nullable=true, length=1000)
     */
    private $st_projeto;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_bairroaluno
     * @Column(name="st_bairroaluno", type="string", nullable=true, length=255)
     */
    private $st_bairroaluno;

    /**
     * @var string $st_cepaluno
     * @Column(name="st_cepaluno", type="string", nullable=true, length=12)
     */
    private $st_cepaluno;

    /**
     * @var string $st_municipioaluno
     * @Column(name="st_municipioaluno", type="string", nullable=true, length=255)
     */
    private $st_municipioaluno;

    /**
     * @var string $st_cidadealuno
     * @Column(name="st_cidadealuno", type="string", nullable=true, length=255)
     */
    private $st_cidadealuno;

    /**
     * @var string $st_paisaluno
     * @Column(name="st_paisaluno", type="string", nullable=true, length=100)
     */
    private $st_paisaluno;

    /**
     * @var string $nu_dddaluno
     * @Column(name="nu_dddaluno", type="string", nullable=true, length=3)
     */
    private $nu_dddaluno;

    /**
     * @var string $st_enderecoresponsavel
     * @Column(name="st_enderecoresponsavel", type="string", nullable=true, length=500)
     */
    private $st_enderecoresponsavel;

    /**
     * @var string $st_complementoresponsavel
     * @Column(name="st_complementoresponsavel", type="string", nullable=true, length=500)
     */
    private $st_complementoresponsavel;

    /**
     * @var string $st_cepresponsavel
     * @Column(name="st_cepresponsavel", type="string", nullable=true, length=12)
     */
    private $st_cepresponsavel;

    /**
     * @var string $st_cidaderesponsavel
     * @Column(name="st_cidaderesponsavel", type="string", nullable=true, length=255)
     */
    private $st_cidaderesponsavel;

    /**
     * @var string $st_bairroresponsavel
     * @Column(name="st_bairroresponsavel", type="string", nullable=true, length=255)
     */
    private $st_bairroresponsavel;

    /**
     * @var string $st_enderecoaluno
     * @Column(name="st_enderecoaluno", type="string", nullable=true, length=500)
     */
    private $st_enderecoaluno;

    /**
     * @var string $st_orgaoexpedidorresp
     * @Column(name="st_orgaoexpedidorresp", type="string", nullable=true, length=80)
     */
    private $st_orgaoexpedidorresp;

    /**
     * @var string $nu_telefoneresp
     * @Column(name="nu_telefoneresp", type="string", nullable=true, length=15)
     */
    private $nu_telefoneresp;

    /**
     * @var string $st_nomecompletoresponsavel
     * @Column(name="st_nomecompletoresponsavel", type="string", nullable=true, length=300)
     */
    private $st_nomecompletoresponsavel;

    /**
     * @var string $st_cpfresponsavel
     * @Column(name="st_cpfresponsavel", type="string", nullable=true, length=11)
     */
    private $st_cpfresponsavel;

    /**
     * @var string $st_tipocontratoresponsavel_financeiro
     * @Column(name="st_tipocontratoresponsavel_financeiro", type="string", nullable=false, length=10)
     */
    private $st_tipocontratoresponsavel_financeiro;

    /**
     * @var string $nu_porcentagem
     * @Column(name="nu_porcentagem", type="string", nullable=false, length=4)
     */
    private $nu_porcentagem;

    /**
     * @var string $sg_ufresponsavel
     * @Column(name="sg_ufresponsavel", type="string", nullable=true, length=2)
     */
    private $sg_ufresponsavel;

    /**
     * @var string $st_ufaluno
     * @Column(name="st_ufaluno", type="string", nullable=true, length=2)
     */
    private $st_ufaluno;

    /**
     * @var string $st_dataexpedicaoaluno
     * @Column(name="st_dataexpedicaoaluno", type="string", nullable=true, length=60)
     */
    private $st_dataexpedicaoaluno;

    /**
     * @var string $st_dataexpedicaoresp
     * @Column(name="st_dataexpedicaoresp", type="string", nullable=true, length=60)
     */
    private $st_dataexpedicaoresp;

    /**
     * @var string $st_datahora
     * @Column(name="st_datahora", type="string", nullable=true, length=60)
     */
    private $st_datahora;

    /**
     * @var string $dt_aceite
     * @Column(name="dt_aceite", type="string", nullable=true, length=60)
     */
    private $dt_aceite;

    /**
     * @var string $st_dataexpedicaotitulo
     * @Column(name="st_dataexpedicaotitulo", type="string", nullable=true, length=60)
     */
    private $st_dataexpedicaotitulo;


    /**
     * @var integer $id_contratomodelo
     * @Column(name="id_contratomodelo", type="integer", nullable=true)
     */
    private $id_contratomodelo;

    /**
     * @var string $st_campanhapontualidade
     * @Column(name="st_campanhapontualidade", type="string")
     */
    private $st_campanhapontualidade;

    /**
     * @var string $st_aceita
     * @Column(name="st_aceita", type="string")
     */
    private $st_aceita;

    /**
     * @var $dt_transferencia
     * @Column(name="dt_transferencia", type="string")
     */
    private $dt_transferencia;

    /**
     * @var integer $id_matriculaorigem
     * @Column(name="id_matriculaorigem", type="integer", nullable=true, length=4)
     */
    private $id_matriculaorigem;

    /**
     * @var string $st_projetoorigem
     * @Column(name="st_projetoorigem", type="string")
     */
    private $st_projetoorigem;

    /**
     * @var integer $id_matriculadestino
     * @Column(name="id_matriculadestino", type="integer", nullable=true, length=4)
     */
    private $id_matriculadestino;

    /**
     * @var string $st_projetodestino
     * @Column(name="st_projetodestino", type="string")
     */
    private $st_projetodestino;

    /**
     * @var string $nu_cargahorariadestino
     * @Column(name="nu_cargahorariadestino", type="string", nullable=false, length=4)
     */
    private $nu_cargahorariadestino;

    /**
     * @var string $nu_valorliquidotransferencia
     * @Column(name="nu_valorliquidotransferencia", type="string", nullable=false, length=4)
     */
    private $nu_valorliquidotransferencia;


    /**
     * @var integer $id_vendaaditivo
     * @Column(name="id_vendaaditivo", type="integer", nullable=true, length=4)
     */
    private $id_vendaaditivo;

    /**
     * @var string $st_porcentagemcampanhapontualidade
     * @Column(name="st_porcentagemcampanhapontualidade", type="string", nullable=true, length=20)
     */
    private $st_porcentagemcampanhapontualidade;

    /**
     * @var integer $id_matricula
     * @Column(type="integer", name="id_matricula")
     */
    private $id_matricula;


    /**
     * @return string
     */
    public function getSt_campanhapontualidade()
    {
        return $this->st_campanhapontualidade;
    }

    /**
     * @param string $st_campanhapontualidade
     * @return VwGerarContrato
     */
    public function setSt_campanhapontualidade($st_campanhapontualidade)
    {
        $this->st_campanhapontualidade = $st_campanhapontualidade;
        return $this;
    }


    public function getId_contratomodelo()
    {
        return $this->id_contratomodelo;
    }

    public function setId_contratomodelo($id_contratomodelo)
    {
        $this->id_contratomodelo = $id_contratomodelo;
        return $this;
    }

    public function getDt_nascimentoresponsavel()
    {
        return $this->dt_nascimentoresponsavel;
    }

    public function getDt_nascimentoaluno()
    {
        return $this->dt_nascimentoaluno;
    }

    public function getDt_confirmacao_venda()
    {
        return $this->dt_confirmacao_venda;
    }

    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    public function getId_usuarioresponsavel()
    {
        return $this->id_usuarioresponsavel;
    }

    public function getId_usuarioaluno()
    {
        return $this->id_usuarioaluno;
    }

    public function getSt_numerotitulo()
    {
        return $this->st_numerotitulo;
    }

    public function getSt_areatitulo()
    {
        return $this->st_areatitulo;
    }

    public function getSt_zonatitulo()
    {
        return $this->st_zonatitulo;
    }

    public function getNu_parcelas_venda()
    {
        return $this->nu_parcelas_venda;
    }

    public function getId_venda()
    {
        return $this->id_venda;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getNu_diamensalidade()
    {
        return $this->nu_diamensalidade;
    }

    public function getSt_etnia()
    {
        return $this->st_etnia;
    }

    public function getSt_tiposanguineo()
    {
        return $this->st_tiposanguineo;
    }

    public function getSt_alergias()
    {
        return $this->st_alergias;
    }

    public function getSt_glicemia()
    {
        return $this->st_glicemia;
    }

    public function getId_tipocontratoresponsavel_financeiro()
    {
        return $this->id_tipocontratoresponsavel_financeiro;
    }

    public function getSt_ufcompletoaluno()
    {
        return $this->st_ufcompletoaluno;
    }

    public function getSt_nacionalidadealuno()
    {
        return $this->st_nacionalidadealuno;
    }

    public function getNu_dia()
    {
        return $this->nu_dia;
    }

    public function getNu_mes()
    {
        return $this->nu_mes;
    }

    public function getNu_ano()
    {
        return $this->nu_ano;
    }

    public function getSt_numeroreservita()
    {
        return $this->st_numeroreservita;
    }

    public function getSt_numerocertidao()
    {
        return $this->st_numerocertidao;
    }

    public function getSt_livrocertidao()
    {
        return $this->st_livrocertidao;
    }

    public function getSt_folhascertidao()
    {
        return $this->st_folhascertidao;
    }

    public function getSt_descricaocartorio()
    {
        return $this->st_descricaocartorio;
    }

    public function getSt_cargo()
    {
        return $this->st_cargo;
    }

    public function getNu_valorliquido_venda()
    {
        return $this->nu_valorliquido_venda;
    }

    public function getSt_nomecompletoaluno()
    {
        return $this->st_nomecompletoaluno;
    }

    public function getSt_cpfaluno()
    {
        return $this->st_cpfaluno;
    }

    public function getSt_rgaluno()
    {
        return $this->st_rgaluno;
    }

    public function getSt_estadocivil()
    {
        return $this->st_estadocivil;
    }

    public function getSt_orgaoexpedidor()
    {
        return $this->st_orgaoexpedidor;
    }

    public function getSt_rgresp()
    {
        return $this->st_rgresp;
    }

    public function getSt_secretarioescolar()
    {
        return $this->st_secretarioescolar;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function getSt_inicio()
    {
        return $this->st_inicio;
    }

    public function getSt_turno()
    {
        return $this->st_turno;
    }

    public function getNu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    public function getSt_turma()
    {
        return $this->st_turma;
    }

    public function getSt_razaosocial()
    {
        return $this->st_razaosocial;
    }

    public function getSt_cnpj()
    {
        return $this->st_cnpj;
    }

    public function getSt_urlimglogo()
    {
        return $this->st_urlimglogo;
    }

    public function getSt_urlsite()
    {
        return $this->st_urlsite;
    }

    public function getSt_polo()
    {
        return $this->st_polo;
    }

    public function getSt_diretor()
    {
        return $this->st_diretor;
    }

    public function getNu_telefonealuno()
    {
        return $this->nu_telefonealuno;
    }

    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    public function getSt_sexoaluno()
    {
        return $this->st_sexoaluno;
    }

    public function getSt_projeto()
    {
        return $this->st_projeto;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function getSt_bairroaluno()
    {
        return $this->st_bairroaluno;
    }

    public function getSt_cepaluno()
    {
        return $this->st_cepaluno;
    }

    public function getSt_municipioaluno()
    {
        return $this->st_municipioaluno;
    }

    public function getSt_cidadealuno()
    {
        return $this->st_cidadealuno;
    }

    public function getSt_paisaluno()
    {
        return $this->st_paisaluno;
    }

    public function getNu_dddaluno()
    {
        return $this->nu_dddaluno;
    }

    public function getSt_enderecoresponsavel()
    {
        return $this->st_enderecoresponsavel;
    }

    public function getSt_complementoresponsavel()
    {
        return $this->st_complementoresponsavel;
    }

    public function getSt_cepresponsavel()
    {
        return $this->st_cepresponsavel;
    }

    public function getSt_cidaderesponsavel()
    {
        return $this->st_cidaderesponsavel;
    }

    public function getSt_bairroresponsavel()
    {
        return $this->st_bairroresponsavel;
    }

    public function getSt_enderecoaluno()
    {
        return $this->st_enderecoaluno;
    }

    public function getSt_orgaoexpedidorresp()
    {
        return $this->st_orgaoexpedidorresp;
    }

    public function getNu_telefoneresp()
    {
        return $this->nu_telefoneresp;
    }

    public function getSt_nomecompletoresponsavel()
    {
        return $this->st_nomecompletoresponsavel;
    }

    public function getSt_cpfresponsavel()
    {
        return $this->st_cpfresponsavel;
    }

    public function getSt_tipocontratoresponsavel_financeiro()
    {
        return $this->st_tipocontratoresponsavel_financeiro;
    }

    public function getNu_porcentagem()
    {
        return $this->nu_porcentagem;
    }

    public function getSg_ufresponsavel()
    {
        return $this->sg_ufresponsavel;
    }

    public function getSt_ufaluno()
    {
        return $this->st_ufaluno;
    }

    public function getSt_dataexpedicaoaluno()
    {
        return $this->st_dataexpedicaoaluno;
    }

    public function getSt_dataexpedicaoresp()
    {
        return $this->st_dataexpedicaoresp;
    }

    public function getSt_datahora()
    {
        return $this->st_datahora;
    }

    public function getDt_aceite()
    {
        return $this->dt_aceite;
    }

    public function getSt_dataexpedicaotitulo()
    {
        return $this->st_dataexpedicaotitulo;
    }

    public function setDt_nascimentoresponsavel($dt_nascimentoresponsavel)
    {
        $this->dt_nascimentoresponsavel = $dt_nascimentoresponsavel;
        return $this;
    }

    public function setDt_nascimentoaluno($dt_nascimentoaluno)
    {
        $this->dt_nascimentoaluno = $dt_nascimentoaluno;
        return $this;
    }

    public function setDt_confirmacao_venda(datetime2 $dt_confirmacao_venda)
    {
        $this->dt_confirmacao_venda = $dt_confirmacao_venda;
        return $this;
    }

    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    public function setId_usuarioresponsavel($id_usuarioresponsavel)
    {
        $this->id_usuarioresponsavel = $id_usuarioresponsavel;
        return $this;
    }

    public function setId_usuarioaluno($id_usuarioaluno)
    {
        $this->id_usuarioaluno = $id_usuarioaluno;
        return $this;
    }

    public function setSt_numerotitulo($st_numerotitulo)
    {
        $this->st_numerotitulo = $st_numerotitulo;
        return $this;
    }

    public function setSt_areatitulo($st_areatitulo)
    {
        $this->st_areatitulo = $st_areatitulo;
        return $this;
    }

    public function setSt_zonatitulo($st_zonatitulo)
    {
        $this->st_zonatitulo = $st_zonatitulo;
        return $this;
    }

    public function setNu_parcelas_venda($nu_parcelas_venda)
    {
        $this->nu_parcelas_venda = $nu_parcelas_venda;
        return $this;
    }

    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setNu_diamensalidade($nu_diamensalidade)
    {
        $this->nu_diamensalidade = $nu_diamensalidade;
        return $this;
    }

    public function setSt_etnia($st_etnia)
    {
        $this->st_etnia = $st_etnia;
        return $this;
    }

    public function setSt_tiposanguineo($st_tiposanguineo)
    {
        $this->st_tiposanguineo = $st_tiposanguineo;
        return $this;
    }

    public function setSt_alergias($st_alergias)
    {
        $this->st_alergias = $st_alergias;
        return $this;
    }

    public function setSt_glicemia($st_glicemia)
    {
        $this->st_glicemia = $st_glicemia;
        return $this;
    }

    public function setId_tipocontratoresponsavel_financeiro($id_tipocontratoresponsavel_financeiro)
    {
        $this->id_tipocontratoresponsavel_financeiro = $id_tipocontratoresponsavel_financeiro;
        return $this;
    }

    public function setSt_ufcompletoaluno($st_ufcompletoaluno)
    {
        $this->st_ufcompletoaluno = $st_ufcompletoaluno;
        return $this;
    }

    public function setSt_nacionalidadealuno($st_nacionalidadealuno)
    {
        $this->st_nacionalidadealuno = $st_nacionalidadealuno;
        return $this;
    }

    public function setNu_dia($nu_dia)
    {
        $this->nu_dia = $nu_dia;
        return $this;
    }

    public function setNu_mes($nu_mes)
    {
        $this->nu_mes = $nu_mes;
        return $this;
    }

    public function setNu_ano($nu_ano)
    {
        $this->nu_ano = $nu_ano;
        return $this;
    }

    public function setSt_numeroreservita($st_numeroreservita)
    {
        $this->st_numeroreservita = $st_numeroreservita;
        return $this;
    }

    public function setSt_numerocertidao($st_numerocertidao)
    {
        $this->st_numerocertidao = $st_numerocertidao;
        return $this;
    }

    public function setSt_livrocertidao($st_livrocertidao)
    {
        $this->st_livrocertidao = $st_livrocertidao;
        return $this;
    }

    public function setSt_folhascertidao($st_folhascertidao)
    {
        $this->st_folhascertidao = $st_folhascertidao;
        return $this;
    }

    public function setSt_descricaocartorio($st_descricaocartorio)
    {
        $this->st_descricaocartorio = $st_descricaocartorio;
        return $this;
    }

    public function setSt_cargo($st_cargo)
    {
        $this->st_cargo = $st_cargo;
        return $this;
    }

    public function setNu_valorliquido_venda($nu_valorliquido_venda)
    {
        $this->nu_valorliquido_venda = $nu_valorliquido_venda;
        return $this;
    }

    public function setSt_nomecompletoaluno($st_nomecompletoaluno)
    {
        $this->st_nomecompletoaluno = $st_nomecompletoaluno;
        return $this;
    }

    public function setSt_cpfaluno($st_cpfaluno)
    {
        $this->st_cpfaluno = $st_cpfaluno;
        return $this;
    }

    public function setSt_rgaluno($st_rgaluno)
    {
        $this->st_rgaluno = $st_rgaluno;
        return $this;
    }

    public function setSt_estadocivil($st_estadocivil)
    {
        $this->st_estadocivil = $st_estadocivil;
        return $this;
    }

    public function setSt_orgaoexpedidor($st_orgaoexpedidor)
    {
        $this->st_orgaoexpedidor = $st_orgaoexpedidor;
        return $this;
    }

    public function setSt_rgresp($st_rgresp)
    {
        $this->st_rgresp = $st_rgresp;
        return $this;
    }

    public function setSt_secretarioescolar($st_secretarioescolar)
    {
        $this->st_secretarioescolar = $st_secretarioescolar;
        return $this;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    public function setSt_inicio($st_inicio)
    {
        $this->st_inicio = $st_inicio;
        return $this;
    }

    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }

    public function setNu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    public function setSt_razaosocial($st_razaosocial)
    {
        $this->st_razaosocial = $st_razaosocial;
        return $this;
    }

    public function setSt_cnpj($st_cnpj)
    {
        $this->st_cnpj = $st_cnpj;
        return $this;
    }

    public function setSt_urlimglogo($st_urlimglogo)
    {
        $this->st_urlimglogo = $st_urlimglogo;
        return $this;
    }

    public function setSt_urlsite($st_urlsite)
    {
        $this->st_urlsite = $st_urlsite;
        return $this;
    }

    public function setSt_polo($st_polo)
    {
        $this->st_polo = $st_polo;
        return $this;
    }

    public function setSt_diretor($st_diretor)
    {
        $this->st_diretor = $st_diretor;
        return $this;
    }

    public function setNu_telefonealuno($nu_telefonealuno)
    {
        $this->nu_telefonealuno = $nu_telefonealuno;
        return $this;
    }

    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
        return $this;
    }

    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
        return $this;
    }

    public function setSt_sexoaluno($st_sexoaluno)
    {
        $this->st_sexoaluno = $st_sexoaluno;
        return $this;
    }

    public function setSt_projeto($st_projeto)
    {
        $this->st_projeto = $st_projeto;
        return $this;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function setSt_bairroaluno($st_bairroaluno)
    {
        $this->st_bairroaluno = $st_bairroaluno;
        return $this;
    }

    public function setSt_cepaluno($st_cepaluno)
    {
        $this->st_cepaluno = $st_cepaluno;
        return $this;
    }

    public function setSt_municipioaluno($st_municipioaluno)
    {
        $this->st_municipioaluno = $st_municipioaluno;
        return $this;
    }

    public function setSt_cidadealuno($st_cidadealuno)
    {
        $this->st_cidadealuno = $st_cidadealuno;
        return $this;
    }

    public function setSt_paisaluno($st_paisaluno)
    {
        $this->st_paisaluno = $st_paisaluno;
        return $this;
    }

    public function setNu_dddaluno($nu_dddaluno)
    {
        $this->nu_dddaluno = $nu_dddaluno;
        return $this;
    }

    public function setSt_enderecoresponsavel($st_enderecoresponsavel)
    {
        $this->st_enderecoresponsavel = $st_enderecoresponsavel;
        return $this;
    }

    public function setSt_complementoresponsavel($st_complementoresponsavel)
    {
        $this->st_complementoresponsavel = $st_complementoresponsavel;
        return $this;
    }

    public function setSt_cepresponsavel($st_cepresponsavel)
    {
        $this->st_cepresponsavel = $st_cepresponsavel;
        return $this;
    }

    public function setSt_cidaderesponsavel($st_cidaderesponsavel)
    {
        $this->st_cidaderesponsavel = $st_cidaderesponsavel;
        return $this;
    }

    public function setSt_bairroresponsavel($st_bairroresponsavel)
    {
        $this->st_bairroresponsavel = $st_bairroresponsavel;
        return $this;
    }

    public function setSt_enderecoaluno($st_enderecoaluno)
    {
        $this->st_enderecoaluno = $st_enderecoaluno;
        return $this;
    }

    public function setSt_orgaoexpedidorresp($st_orgaoexpedidorresp)
    {
        $this->st_orgaoexpedidorresp = $st_orgaoexpedidorresp;
        return $this;
    }

    public function setNu_telefoneresp($nu_telefoneresp)
    {
        $this->nu_telefoneresp = $nu_telefoneresp;
        return $this;
    }

    public function setSt_nomecompletoresponsavel($st_nomecompletoresponsavel)
    {
        $this->st_nomecompletoresponsavel = $st_nomecompletoresponsavel;
        return $this;
    }

    public function setSt_cpfresponsavel($st_cpfresponsavel)
    {
        $this->st_cpfresponsavel = $st_cpfresponsavel;
        return $this;
    }

    public function setSt_tipocontratoresponsavel_financeiro($st_tipocontratoresponsavel_financeiro)
    {
        $this->st_tipocontratoresponsavel_financeiro = $st_tipocontratoresponsavel_financeiro;
        return $this;
    }

    public function setNu_porcentagem($nu_porcentagem)
    {
        $this->nu_porcentagem = $nu_porcentagem;
        return $this;
    }

    public function setSg_ufresponsavel($sg_ufresponsavel)
    {
        $this->sg_ufresponsavel = $sg_ufresponsavel;
        return $this;
    }

    public function setSt_ufaluno($st_ufaluno)
    {
        $this->st_ufaluno = $st_ufaluno;
        return $this;
    }

    public function setSt_dataexpedicaoaluno($st_dataexpedicaoaluno)
    {
        $this->st_dataexpedicaoaluno = $st_dataexpedicaoaluno;
        return $this;
    }

    public function setSt_dataexpedicaoresp($st_dataexpedicaoresp)
    {
        $this->st_dataexpedicaoresp = $st_dataexpedicaoresp;
        return $this;
    }

    public function setSt_datahora($st_datahora)
    {
        $this->st_datahora = $st_datahora;
        return $this;
    }

    public function setDt_aceite($dt_aceite)
    {
        $this->dt_aceite = $dt_aceite;
        return $this;
    }

    public function setSt_dataexpedicaotitulo($st_dataexpedicaotitulo)
    {
        $this->st_dataexpedicaotitulo = $st_dataexpedicaotitulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_porcentagemcampanhapontualidade()
    {
        return $this->st_porcentagemcampanhapontualidade;
    }

    /**
     * @param string $st_porcentagemcampanhapontualidade
     * @return $this
     */
    public function setSt_porcentagemcampanhapontualidade($st_porcentagemcampanhapontualidade)
    {
        $this->st_porcentagemcampanhapontualidade = $st_porcentagemcampanhapontualidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwGerarContrato
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getSt_aceita()
    {
        return $this->st_aceita;
    }

    public function setSt_aceita($st_aceita)
    {
        $this->st_aceita = $st_aceita;
        return $this;
    }

    public function getDt_transferencia()
    {
        return $this->dt_transferencia;
    }

    public function setDt_transferencia($dt_transferencia)
    {
        $this->dt_transferencia = $dt_transferencia;
        return $this;
    }

    public function getId_matriculaorigem()
    {
        return $this->id_matriculaorigem;
    }

    public function setId_matriculaorigem($id_matriculaorigem)
    {
        $this->id_matriculaorigem = $id_matriculaorigem;
        return $this;
    }

    public function getSt_projetoorigem()
    {
        return $this->st_projetoorigem;
    }

    public function setSt_projetoorigem($st_projetoorigem)
    {
        $this->st_projetoorigem = $st_projetoorigem;
        return $this;
    }

    public function getId_matriculadestino()
    {
        return $this->id_matriculadestino;
    }

    public function setId_matriculadestino($id_matriculadestino)
    {
        $this->id_matriculadestino = $id_matriculadestino;
        return $this;
    }

    public function getSt_projetodestino()
    {
        return $this->st_projetodestino;
    }

    public function setSt_projetodestino($st_projetodestino)
    {
        $this->st_projetodestino = $st_projetodestino;
        return $this;
    }

    public function getNu_cargahorariadestino()
    {
        return $this->nu_cargahorariadestino;
    }

    public function setNu_cargahorariadestino($nu_cargahorariadestino)
    {
        $this->nu_cargahorariadestino = $nu_cargahorariadestino;
        return $this;
    }

    public function getNu_valorliquidotransferencia()
    {
        return $this->nu_valorliquidotransferencia;
    }

    public function setNu_valorliquidotransferencia($nu_valorliquidotransferencia)
    {
        $this->nu_valorliquidotransferencia = $nu_valorliquidotransferencia;
        return $this;
    }

    public function getId_vendaaditivo()
    {
        return $this->id_vendaaditivo;
    }

    public function setId_vendaaditivo($id_vendaaditivo)
    {
        $this->id_vendaaditivo = $id_vendaaditivo;
        return $this;
    }



}

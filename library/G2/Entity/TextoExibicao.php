<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_textoexibicao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TextoExibicao
{

    /**
     *
     * @var integer $id_textoexibicao
     * @Column(name="id_textoexibicao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_textoexibicao;

    /**
     * @var string $st_textoexibicao
     * @Column(name="st_textoexibicao",type="string", nullable=false, length=200)
     */
    private $st_textoexibicao;

    /**
     * @var string $st_descricao
     * @Column(name="st_descricao",type="string", nullable=false, length=300)
     */
    private $st_descricao;

    /**
     * @ManyToMany(targetEntity="TextoCategoria", mappedBy="textoExibicao")
     */
    private $textoCategoria;

    public function __construct ()
    {
        $this->textoCategoria = new ArrayCollection();
    }

    public function getTextoCategoria ()
    {
        return $this->textoCategoria;
    }

    public function getId_textoexibicao ()
    {
        return $this->id_textoexibicao;
    }

    public function setId_textoexibicao ($id_textoexibicao)
    {
        $this->id_textoexibicao = $id_textoexibicao;
        return $this;
    }

    public function getSt_textoexibicao ()
    {
        return $this->st_textoexibicao;
    }

    public function setSt_textoexibicao ($st_textoexibicao)
    {
        $this->st_textoexibicao = $st_textoexibicao;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}

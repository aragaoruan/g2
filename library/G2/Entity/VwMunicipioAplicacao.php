<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_municipioaplicacao")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwMunicipioAplicacao {

    /**
     *
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=false)
     * @Id
     */
    private $id_municipio;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     * @var string $st_nomemunicipio
     * @Column(name="st_nomemunicipio", type="string", nullable=true, length=255)
     */
    private $st_nomemunicipio;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;
    
    public function getId_municipio() {
        return $this->id_municipio;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getSt_nomemunicipio() {
        return $this->st_nomemunicipio;
    }

    public function getSg_uf() {
        return $this->sg_uf;
    }

    public function setId_municipio($id_municipio) {
        $this->id_municipio = $id_municipio;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setSt_nomemunicipio($st_nomemunicipio) {
        $this->st_nomemunicipio = $st_nomemunicipio;
    }

    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }



}

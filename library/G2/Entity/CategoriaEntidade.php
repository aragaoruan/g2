<?php

/*
 * Entity CategoriaEntidade
 * @author: Caio Eduardo <paulo.silva@unyleya.com.br>
 * @since: 2014-11-05
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_categoriaentidade")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class CategoriaEntidade
{

    /**
     *
     * @var integer $id_categoria
     * @Column(name="id_categoria", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_categoria;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_entidade;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @Column(name="dt_cadastro", type="datetime2",nullable=false)
     * @var string $dt_cadastro
     */
    private $dt_cadastro;

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return int
     */
    public function getId_categoria()
    {
        return $this->id_categoria;
    }

    /**
     * @param int $id_categoria
     */
    public function setId_categoria($id_categoria)
    {
        $this->id_categoria = $id_categoria;
    }

    /**
     * @return mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }


}

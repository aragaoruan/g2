<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 *
 * @Table(name="tb_tiposelecao")
 * @Entity
 * @author elcioguimaraes@gmail.com
 * @EntityLog
 */
class TipoSelecao extends G2Entity
{

    const PAD = 1;
    const PDS = 2;
    const ENEM = 3;
    const VESTIBULAR = 4;
    const TRANSFERENCIA = 5;
    const MATRICULA_COM_ISENCAO_DE_DISCIPLINAS = 6;


    /**
     *
     * @var integer $id_tiposelecao
     * @Column(name="id_tiposelecao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tiposelecao;

    /**
     * @Column(name="st_tiposelecao", type="string",length=200,nullable=false)
     * @var string
     */
    private $st_tiposelecao;

    /**
     * @var boolean
     * @Column(type="boolean", nullable=false, options="{default: 1}")
     */
    private $bl_ativo;

    /**
     * @var mixed
     * @Column(type="datetime2", nullable=false,  options="{default: GETDATE()}")
     */
    private $dt_cadastro;


    /**
     * @var Usuario $id_categoriapergunta
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var string
     * @Column(type="string", nullable=true)
     */
    private $st_descricao;

    /**
     * @return int
     */
    public function getId_tiposelecao()
    {
        return $this->id_tiposelecao;
    }

    /**
     * @param int $id_tiposelecao
     * @return TipoSelecao
     */
    public function setId_tiposelecao($id_tiposelecao)
    {
        $this->id_tiposelecao = $id_tiposelecao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tiposelecao()
    {
        return $this->st_tiposelecao;
    }

    /**
     * @param string $st_tiposelecao
     * @return TipoSelecao
     */
    public function setSt_tiposelecao($st_tiposelecao)
    {
        $this->st_tiposelecao = $st_tiposelecao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return TipoSelecao
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     * @return TipoSelecao
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return TipoSelecao
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     * @return TipoSelecao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }


}

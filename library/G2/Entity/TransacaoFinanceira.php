<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 02/07/14
 * Time: 15:07
 */

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_transacaofinanceira")
 * @Entity(repositoryClass="\G2\Repository\TransacaoFinanceira")
 */
class TransacaoFinanceira extends G2Entity{

    /**
     *
     * @var integer $id_transacaofinanceira
     * @Column(name="id_transacaofinanceira ", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_transacaofinanceira;

    /**
     * @var string $un_transacaofinanceira
     * @Column(name="un_transacaofinanceira", type="guid")
     */
    private $un_transacaofinanceira;

    /**
     * @var Usuario
     *
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumns({
     *      @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     * })
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_codtransacaogateway
     * @Column(name="st_codtransacaogateway", type="string", nullable=false, length=100)
     */
    private $st_codtransacaogateway;

    /**
     * @var string $st_statuspagarme
     * @Column(name="st_statuspagarme", type="string", nullable=false, length=100)
     */
    private $st_statuspagarme;

    /**
     * @var string $st_codtransacaooperadora
     * @Column(name="st_codtransacaooperadora", type="string", nullable=false, length=100)
     */
    private $st_codtransacaooperadora;

    /**
     * @var integer $nu_status
     * @Column(name="nu_status", type="integer", nullable=true)
     */
    private $nu_status;

    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private  $dt_cadastro;

    /**
     * @var Venda
     *
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumns({
     *      @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     * })
     */
    private $id_venda;

    /**
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=true)
     */
    private $id_cartaobandeira;

    /**
     * @var integer $id_cartaooperadora
     * @Column(name="id_cartaooperadora", type="integer", nullable=true)
     */
    private $id_cartaooperadora;

    /**
     * @var integer $nu_verificacao
     * @Column(name="nu_verificacao", type="integer", nullable=true)
     */
    private $nu_verificacao;

    /**
     * @var string $un_orderid
     * @Column(name="un_orderid", type="guid")
     */
    private $un_orderid;

    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false, length=8000)
     */
    private $st_mensagem;

    /**
     * @var integer $id_situacaointegracao
     * @Column(name="id_situacaointegracao", type="integer", nullable=true)
     */
    private $id_situacaointegracao;

    /**
     * @var string st_titularcartao
     * @Column(name="st_titularcartao", type="string", nullable=true, length=255)
     */
    private $st_titularcartao;

    /**
     * @var string $st_ultimosdigitos
     * @Column(name="st_ultimosdigitos", type="string", nullable=true, length=4)
     */
    private $st_ultimosdigitos;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer")
     */
    private $nu_parcelas;

    /**
     * @var mixed $nu_valorcielo
     * @Column(name="nu_valorcielo", type="decimal", precision=10, scale=2)
     */
    private $nu_valorcielo;

    /**
     * @var Situacao
     *
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumns({
     *      @JoinColumn(name="id_statustransacao", referencedColumnName="id_situacao")
     * })
     */
    private $id_statustransacao;

    /**
     * @var string $st_codigoautorizacao
     * @Column(name="st_codigoautorizacao", type="string", length=100)
     */
    private $st_codigoautorizacao;

    /**
     * @var mixed $nu_valorenviado
     * @Column(name="nu_valorenviado", type="decimal", precision=10, scale=2)
     */
    private $nu_valorenviado;

    /**
     * @var mixed $nu_valorautorizado
     * @Column(name="nu_valorautorizado", type="decimal", precision=10, scale=2)
     */
    private $nu_valorautorizado;

    /**
     * @var mixed $nu_valorrecusado
     * @Column(name="nu_valorrecusado", type="decimal", precision=10, scale=2)
     */
    private $nu_valorrecusado;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", length=50)
     */
    private $st_meiopagamento;

    /**
     * @var string $st_urlboleto
     * @Column(name="st_urlboleto", type="string", length=250)
     */
    private $st_urlboleto;

    /**
     * @var string $st_boletocoditobarras
     * @Column(name="st_boletocoditobarras", type="string", length=250)
     */
    private $st_boletocoditobarras;

    /**
     * @var string $st_urlpostback
     * @Column(name="st_urlpostback", type="string", length=250)
     */
    private $st_urlpostback;

    /**
     * @var datetime $dt_vencimento
     * @Column(name="dt_vencimento", type="datetime", nullable=false)
     */
    private  $dt_vencimento;

    /**
     * @var string $st_nsu
     * @Column(name="st_nsu", type="string", length=100)
     */
    private $st_nsu;

    /**
     * @var string $st_idtransacaoexterna
     * @Column(name="id_transacaoexterna", type="string", length=150)
     */
    private $st_idtransacaoexterna;

    /**
     * @var integer $id_mensagem
     * @Column(name="id_mensagem", type="integer", length=150)
     */
    private $id_mensagem;

    /**
     * @return string
     */
    public function getst_statuspagarme()
    {
        return $this->st_statuspagarme;
    }

    /**
     * @param string $st_statuspagarme
     * @return TransacaoFinanceira
     */
    public function setStStatuspagarme($st_statuspagarme)
    {
        $this->st_statuspagarme = $st_statuspagarme;
        return $this;
    }

    /**
     * @return string
     */
    public function getstStatuspagarme()
    {
        return $this->st_statuspagarme;
    }

    /**
     * @param string $nu_parcelas
     * @deprecated
     */
    public function setNuParcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @param $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas) {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getNuParcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @return string
     */
    public function getNu_parcelas() {
        return $this->nu_parcelas;
    }

    /**
     * @param string $nu_valorcielo
     * @deprecated
     */
    public function setNuValorcielo($nu_valorcielo)
    {
        $this->nu_valorcielo = $nu_valorcielo;
    }

    /**
     * @param $nu_valorcielo
     */
    public function setNu_valorcielo($nu_valorcielo) {
        $this->nu_valorcielo = $nu_valorcielo;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getNuValorcielo()
    {
        return $this->nu_valorcielo;
    }

    /**
     * @return string
     */
    public function getNu_valorcielo() {
        return $this->nu_valorcielo;
    }

    /**
     * @param string $st_titularcartao
     * @deprecated
     */
    public function setStTitularcartao($st_titularcartao)
    {
        $this->st_titularcartao = $st_titularcartao;
    }

    /**
     * @param $st_titularcartao
     */
    public function setSt_titularcartao($st_titularcartao) {
        $this->st_titularcartao = $st_titularcartao;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStTitularcartao()
    {
        return $this->st_titularcartao;
    }

    /**
     * @return string
     */
    public function getSt_titularcartao() {
        return$this->st_titularcartao;
    }

    /**
     * @param string $st_ultimosdigitos
     * @deprecated
     */
    public function setStUltimosdigitos($st_ultimosdigitos)
    {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @param $st_ultimosdigitos
     */
    public function setSt_ultimosdigitos($st_ultimosdigitos) {
        $this->st_ultimosdigitos = $st_ultimosdigitos;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStUltimosdigitos()
    {
        return $this->st_ultimosdigitos;
    }

    /**
     * @return string
     */
    public function getSt_ultimosdigitos() {
        return $this->st_ultimosdigitos;
    }


    /**
     * @param datetime $dt_cadastro
     * @deprecated
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return datetime
     * @deprecated
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return datetime
     */
    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $id_cartaobandeira
     * @deprecated
     */
    public function setIdCartaobandeira($id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
    }

    /**
     * @param $id_cartaobandeira
     */
    public function setId_cartaobandeira($id_cartaobandeira) {
        $this->id_cartaobandeira = $id_cartaobandeira;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getIdCartaobandeira()
    {
        return $this->id_cartaobandeira;
    }

    /**
     * @return int
     */
    public function getId_cartaobandeira() {
        return $this->id_cartaobandeira;
    }

    /**
     * @param int $id_cartaooperadora
     * @deprecated
     */
    public function setIdCartaooperadora($id_cartaooperadora)
    {
        $this->id_cartaooperadora = $id_cartaooperadora;
    }

    /**
     * @param $id_cartaooperadora
     */
    public function setId_cartaooperadora($id_cartaooperadora) {
        $this->id_cartaooperadora = $id_cartaooperadora;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdCartaooperadora()
    {
        return $this->id_cartaooperadora;
    }

    /**
     * @return int
     */
    public function getId_cartaooperadora() {
        return $this->id_cartaooperadora;
    }

    /**
     * @param int $id_situacaointegracao
     * @deprecated
     */
    public function setIdSituacaointegracao($id_situacaointegracao)
    {
        $this->id_situacaointegracao = $id_situacaointegracao;
    }

    /**
     * @param $id_situacaointegracao
     */
    public function setId_situacaointegracao($id_situacaointegracao) {
        $this->id_situacaointegracao = $id_situacaointegracao;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdSituacaointegracao()
    {
        return $this->id_situacaointegracao;
    }

    /**
     * @return int
     */
    public function getId_situacaointegracao() {
        return $this->id_situacaointegracao;
    }

    /**
     * @param int $id_transacaofinanceira
     * @deprecated
     */
    public function setIdTransacaofinanceira($id_transacaofinanceira)
    {
        $this->id_transacaofinanceira = $id_transacaofinanceira;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getIdTransacaofinanceira()
    {
        return $this->id_transacaofinanceira;
    }

    /**
     * @return int
     */
    public function getId_transacaofinanceira() {
        return $this->id_transacaofinanceira;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @deprecated
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @return \G2\Entity\Usuario
     * @deprecated
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    /**
     * @param mixed $id_venda
     * @deprecated
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @param $id_venda
     */
    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @return Venda
     */
    public function getId_venda() {
        return $this->id_venda;
    }

    /**
     * @param int $nu_status
     * @deprecated
     */
    public function setNuStatus($nu_status)
    {
        $this->nu_status = $nu_status;
    }

    /**
     * @param $nu_status
     */
    public function setNu_status($nu_status) {
        $this->nu_status = $nu_status;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuStatus()
    {
        return $this->nu_status;
    }

    /**
     * @return int
     */
    public function getNu_status() {
        return $this->nu_status;
    }

    /**
     * @param int $nu_verificacao
     * @deprecated
     */
    public function setNuVerificacao($nu_verificacao)
    {
        $this->nu_verificacao = $nu_verificacao;
    }

    /**
     * @param $nu_verificacao
     */
    public function setNu_verificacao($nu_verificacao) {
        $this->nu_verificacao = $nu_verificacao;
    }

    /**
     * @return int
     * @deprecated
     */
    public function getNuVerificacao()
    {
        return $this->nu_verificacao;
    }

    /**
     * @return int
     */
    public function getNu_verificacao() {
        return $this->nu_verificacao;
    }

    /**
     * @param string $st_codtransacaogateway
     * @deprecated
     */
    public function setStCodtransacaogateway($st_codtransacaogateway)
    {
        $this->st_codtransacaogateway = $st_codtransacaogateway;
    }

    /**
     * @param $st_codtransacaogateway
     */
    public function setSt_codtransacaogateway($st_codtransacaogateway) {
        $this->st_codtransacaogateway = $st_codtransacaogateway;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStCodtransacaogateway()
    {
        return $this->st_codtransacaogateway;
    }

    /**
     * @return string
     */
    public function getSt_codtransacaogateway() {
        return $this->st_codtransacaogateway;
    }

    /**
     * @param mixed $st_codtransacaooperadora
     * @deprecated
     */
    public function setStCodtransacaooperadora($st_codtransacaooperadora)
    {
        $this->st_codtransacaooperadora = $st_codtransacaooperadora;
    }

    /**
     * @param $st_codtrasacaooperadora
     */
    public function setSt_codtransacaooperadora($st_codtrasacaooperadora) {
        $this->st_codtransacaooperadora = $st_codtrasacaooperadora;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getStCodtransacaooperadora()
    {
        return $this->st_codtransacaooperadora;
    }

    /**
     * @return string
     */
    public function getSt_codtransacaooperadora() {
        return $this->st_codtransacaooperadora;
    }
    /**
     * @param string $st_mensagem
     * @deprecated
     */
    public function setStMensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @param $st_mensagem
     */
    public function setSt_mensagem($st_mensagem) {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getStMensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @return string
     */
    public function getSt_mensagem() {
        return $this->st_mensagem;
    }

    /**
     * @param string $un_orderid
     * @deprecated
     */
    public function setUnOrderid($un_orderid)
    {
        $this->un_orderid = $un_orderid;
    }

    /**
     * @param $un_orderid
     */
    public function setUn_orderid($un_orderid) {
        $this->un_orderid = $un_orderid;
    }

    /**
     * @return string
     * @deprecated
     */
    public function getUnOrderid()
    {
        return $this->un_orderid;
    }

    /**
     * @return string
     */
    public function getUn_orderid() {
        return $this->un_orderid;
    }

    /**
     * @param mixed $un_transacaofinanceira
     * @deprecated
     */
    public function setUnTransacaofinanceira($un_transacaofinanceira)
    {
        $this->un_transacaofinanceira = $un_transacaofinanceira;
    }

    /**
     * @param $un_transacaofinanceira
     */
    public function setUn_transacaofinanceira($un_transacaofinanceira) {
        $this->un_transacaofinanceira = $un_transacaofinanceira;
    }

    /**
     * @return mixed
     * @deprecated
     */
    public function getUnTransacaofinanceira()
    {
        return $this->un_transacaofinanceira;
    }

    /**
     * @return string
     */
    public function getUn_transacaofinanceira() {
        return $this->un_transacaofinanceira;
    }

    /**
     * @return Situacao
     */
    public function getId_statustransacao()
    {
        return $this->id_statustransacao;
    }


    /**
     * @param Situacao $id_statustransacao
     */
    public function setId_statustransacao($id_statustransacao)
    {
        $this->id_statustransacao = $id_statustransacao;
    }

    /**
     * @return string
     */
    public function getSt_codigoautorizacao()
    {
        return $this->st_codigoautorizacao;
    }

    /**
     * @param string $st_codigoautorizacao
     */
    public function setSt_codigoautorizacao($st_codigoautorizacao)
    {
        $this->st_codigoautorizacao = $st_codigoautorizacao;
    }

    /**
     * @return mixed
     */
    public function getNu_valorenviado()
    {
        return $this->nu_valorenviado;
    }

    /**
     * @param mixed $nu_valorenviado
     */
    public function setNu_valorenviado($nu_valorenviado)
    {
        $this->nu_valorenviado = $nu_valorenviado;
    }

    /**
     * @return mixed
     */
    public function getNu_valorautorizado()
    {
        return $this->nu_valorautorizado;
    }

    /**
     * @param mixed $nu_valorautorizado
     */
    public function setNu_valorautorizado($nu_valorautorizado)
    {
        $this->nu_valorautorizado = $nu_valorautorizado;
    }

    /**
     * @return mixed
     */
    public function getNu_valorrecusado()
    {
        return $this->nu_valorrecusado;
    }

    /**
     * @param mixed $nu_valorrecusado
     */
    public function setNu_valorrecusado($nu_valorrecusado)
    {
        $this->nu_valorrecusado = $nu_valorrecusado;
    }

    /**
     * @return string
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param string $st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @return string
     */
    public function getSt_urlboleto()
    {
        return $this->st_urlboleto;
    }

    /**
     * @param string $st_urlboleto
     */
    public function setSt_urlboleto($st_urlboleto)
    {
        $this->st_urlboleto = $st_urlboleto;
    }

    /**
     * @return string
     */
    public function getSt_boletocoditobarras()
    {
        return $this->st_boletocoditobarras;
    }

    /**
     * @param string $st_boletocoditobarras
     */
    public function setSt_boletocoditobarras($st_boletocoditobarras)
    {
        $this->st_boletocoditobarras = $st_boletocoditobarras;
    }

    /**
     * @return string
     */
    public function getSt_urlpostback()
    {
        return $this->st_urlpostback;
    }

    /**
     * @param string $st_urlpostback
     */
    public function setSt_urlpostback($st_urlpostback)
    {
        $this->st_urlpostback = $st_urlpostback;
    }

    /**
     * @return datetime
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param mixed $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @return string
     */
    public function getSt_nsu()
    {
        return $this->st_nsu;
    }

    /**
     * @param string $st_nsu
     */
    public function setSt_nsu($st_nsu)
    {
        $this->st_nsu = $st_nsu;
    }

    /**
     * @return string
     */
    public function getSt_idtransacaoexterna()
    {
        return $this->st_idtransacaoexterna;
    }

    /**
     * @param string $st_idtransacaoexterna
     */
    public function setSt_idtransacaoexterna($st_idtransacaoexterna)
    {
        $this->st_idtransacaoexterna = $st_idtransacaoexterna;
    }

    /**
     * @return int
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @param int $id_mensagem
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
    }

}

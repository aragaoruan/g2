<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_produtocarreira")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimarães@gmail.com> 
 */

use \G2\G2Entity;

class ProdutoCarreira extends G2Entity
{

    /**
     * @var integer $id_produtocarreira
     * @Column(name="id_produtocarreira", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produtocarreira;

    /**
     *
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    
    /**
     *
     * @var integer $id_carreira
     * @ManyToOne(targetEntity="Carreira")
     * @JoinColumn(name="id_carreira", nullable=false, referencedColumnName="id_carreira")
     */
    private $id_carreira;
    
    
	/**
	 * @return the $id_produtocarreira
	 */
	public function getId_produtocarreira() {
		return $this->id_produtocarreira;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_carreira
	 */
	public function getId_carreira() {
		return $this->id_carreira;
	}

	/**
	 * @param number $id_produtocarreira
	 */
	public function setId_produtocarreira($id_produtocarreira) {
		$this->id_produtocarreira = $id_produtocarreira;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param number $id_carreira
	 */
	public function setId_carreira($id_carreira) {
		$this->id_carreira = $id_carreira;
		return $this;
	}

    
    

}
<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_pessoaendereco")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class PessoaEndereco extends G2Entity
{

    /**
     * @var Usuario $id_usuario 
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(type="integer", unique=false, nullable=false, name="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade 
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", referencedColumnName="id_entidade", nullable=false)
     */
    private $id_entidade;

    /**
     * @var Endereco $id_endereco 
     * @ManyToOne(targetEntity="Endereco")
     * @JoinColumn(name="id_endereco", referencedColumnName="id_endereco", nullable=false)
     */
    private $id_endereco;

    /**
     * @var boolean $bl_padrao
     * @Column(type="boolean", nullable=true, name="bl_padrao") 
     */
    private $bl_padrao;

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * 
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\PessoaEndereco
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_endereco ()
    {
        return $this->id_endereco;
    }

    /**
     * 
     * @param \G2\Entity\Endereco $id_endereco
     * @return \G2\Entity\PessoaEndereco
     */
    public function setId_endereco (Endereco $id_endereco)
    {
        $this->id_endereco = $id_endereco;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

}
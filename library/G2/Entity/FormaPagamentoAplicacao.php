<?php

/*
 * Entity FormaPagamentoAplicacao
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-29
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formapagamentoaplicacao")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class FormaPagamentoAplicacao {

    /**
     *
     * @var integer $id_formapagamentoaplicacao
     * @Column(name="id_formapagamentoaplicacao", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_formapagamentoaplicacao;

    /**
     * @var string $st_formapagamentoaplicacao
     * @Column(name="st_formapagamentoaplicacao", type="string", nullable=false, length=255)
     */
    private $st_formapagamentoaplicacao;


    public function getId_formapagamentoaplicacao()
    {
        return $this->id_formapagamentoaplicacao;
    }

    public function getSt_formapagamentoaplicacao()
    {
        return $this->st_formapagamentoaplicacao;
    }

    public function setId_formapagamentoaplicacao($id_formapagamentoaplicacao)
    {
        $this->id_formapagamentoaplicacao = $id_formapagamentoaplicacao;
    }

    public function setSt_formapagamentoaplicacao($st_formapagamentoaplicacao)
    {
        $this->st_formapagamentoaplicacao = $st_formapagamentoaplicacao;
    }

}

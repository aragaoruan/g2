<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_contratomatricula")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ContratoMatricula extends G2Entity
{

    /**
     * @var integer $id_contrato
     * @Column(type="integer", nullable=false, name="id_contrato")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_contrato;

    /**
     * @var integer $id_matricula
     * @Column(type="integer", nullable=false, name="id_matricula")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=false, name="bl_ativo", options={"default":true})
     */
    private $bl_ativo;

    /**
     * @return integer
     */
    public function getId_contrato ()
    {
        return $this->id_contrato;
    }

    /**
     *
     * @return integer
     */
    public function getId_matricula ()
    {
        return $this->id_matricula;
    }

    /**
     *
     * @return boolean
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     *
     * @param integer $id_contrato
     * @return \G2\Entity\ContratoMatricula
     */
    public function setId_contrato ($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    /**
     *
     * @param integer $id_matricula
     * @return \G2\Entity\ContratoMatricula
     */
    public function setId_matricula ($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     *
     * @param boolean $bl_ativo
     * @return \G2\Entity\ContratoMatricula
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @Table(name="tb_censo")
 * @Entity
 *
 * @author Stéfan Amaral <stefan.amaral@unyleya.com.br>
 */
class Censo extends G2Entity
{

    /**
     * id da matricula, chave primária
     *
     * @Id
     * @var integer - Id do Usuario
     * @Column(name="id_matricula", type="integer",nullable=false)
     */
    private $id_matricula;

    /**
     *  Tipo de registro - cabeçalho
     *
     * FIXO
     *
     * @Column(name="tp_registrocabecalho", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registrocabecalho;

    /**
     *  ID da IES no INEP
     *
     * VARIÁVEL
     *
     * @Column(name="id_iesinep", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $id_iesinep;

    /**
     *  Tipo de arquivo - cabeçalho
     *
     * FIXO
     *
     * @Column(name="tp_arquivocabecalho", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $tp_arquivocabecalho;

    /**
     *  Tipo de registro - aluno
     *
     * FIXO
     *
     * @Column(name="tp_registroaluno", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registroaluno;

    /**
     *  ID do aluno no INEP
     *
     * FIXO
     *
     * @Column(name="id_alunoinep", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $id_alunoinep;

    /**
     * Nome
     *
     * VARIÁVEL
     *
     * @Column(name="st_nome", type="string",length=120,nullable=false, length=120)
     * @var string
     */
    private $st_nome;

    /**
     * CPF
     *
     * FIXO
     *
     * @Column(name="st_cpf", type="string",length=11,nullable=false, length=11)
     * @var string
     */
    private $st_cpf;

    /**
     * Documento de estrangeiro ou passaporte
     *
     * VARIÁVEL
     *
     * @Column(name="st_docuestrangeiropassaporte", type="string",length=20,nullable=true, length=20)
     * @var string
     */
    private $st_docuestrangeiropassaporte;

    /**
     * Data de Nascimento
     *
     * FIXO
     *
     * @Column(name="dt_nascimento",  type="string",length=10,nullable=false, length=8)
     * @var string
     */
    private $dt_nascimento;

    /**
     * Sexo
     *
     * FIXO
     *
     * @Column(name="nu_sexo", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_sexo;

    /**
     * Cor/Raça
     *
     * FIXO
     *
     * @Column(name="nu_corraca", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_corraca;

    /**
     * Nome completo da mãe
     *
     * VARIÁVEL
     *
     * @Column(name="st_nomecompletomae", type="string",length=120,nullable=true, length=120)
     * @var string
     */
    private $st_nomecompletomae;

    /**
     * Nacionalidade
     *
     * FIXO
     *
     * @Column(name="nu_nacionalidade", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_nacionalidade;

    /**
     * UF de nascimento
     *
     * FIXO
     *
     * @Column(name="nu_ufnascimento", type="integer",nullable=true, length=2)
     * @var integer
     */
    private $nu_ufnascimento;

    /**
     * Município de nascimento
     *
     * FIXO
     *
     * @Column(name="nu_municipionascimento", type="integer",nullable=true, length=7)
     * @var integer
     */
    private $nu_municipionascimento;

    /**
     * País de origem
     *
     * FIXO
     *
     * @Column(name="st_paisorigem", type="string",length=3,nullable=false, length=3)
     * @var string
     */
    private $st_paisorigem;

    /**
     * Aluno com deficiência, transtorno global do desenvolvimento ou altas habilidades/superdotação
     *
     * FIXO
     *
     * @Column(name="nu_alunodeftranstsuperdotacao", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_alunodeftranstsuperdotacao;

    /**
     * Tipo de deficiência - Cegueira
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciacegueira", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciacegueira;

    /**
     * Tipo de deficiência - Baixa visão
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciabaixavisao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciabaixavisao;

    /**
     * Tipo de deficiência - Surdez
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasurdez", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasurdez;

    /**
     * Tipo de deficiência - auditiva
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaauditiva", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaauditiva;

    /**
     * Tipo de deficiência - auditiva
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciafisica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciafisica;

    /**
     * Tipo de deficiência - Surdocegueira
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasurdocegueira", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasurdocegueira;

    /**
     * Tipo de deficiência - Múltipla
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciamultipla", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciamultipla;

    /**
     * Tipo de deficiência - Intelectual
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaintelectcual", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaintelectcual;

    /**
     *  Tipo de deficiência - Autismo(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaautismo", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaautismo;

    /**
     *  Tipo de deficiência - Síndrome de Asperger(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaasperger", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaasperger;

    /**
     *  Tipo de deficiência - Síndrome de RETT(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciarett", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciarett;

    /**
     * Tipo de deficiência - Transtorno desintegrativo da infância(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciatranstdesinfancia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciatranstdesinfancia;

    /**
     * Tipo de deficiência - Altas habilidades/superdotação
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasuperdotacao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasuperdotacao;

    /**
     * Tipo de registro - aluno > curso
     *
     * FIXO
     *
     * @Column(name="tp_registroalunocurso", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registroalunocurso;

    /**
     * Semestre de referência
     *
     * FIXO
     *
     * @Column(name="nu_semestrereferencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_semestrereferencia;

    /**
     * Código do curso
     *
     * VARIÁVEL
     *
     * @Column(name="nu_codigocurso", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $nu_codigocurso;

    /**
     * Código do pólo do curso a distância
     *
     * VARIÁVEL
     *
     * @Column(name="nu_codigopolocursodistancia", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $nu_codigopolocursodistancia;

    /**
     * ID na IES - Identificação única do aluno na IES
     *
     * VARIÁVEL
     *
     * @Column(name="st_idnaies", type="string",length=20,nullable=true, length=20)
     * @var string
     */
    private $st_idnaies;

    /**
     * Turno do aluno
     *
     * FIXO
     *
     * @Column(name="nu_turnoaluno", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_turnoaluno;

    /**
     * Situação de vínculo do aluno ao curso
     *
     * FIXO
     *
     * @Column(name="nu_situacaovinculoalunocurso", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_situacaovinculoalunocurso;

    /**
     * Curso de origem
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cursoorigem", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $nu_cursoorigem;

    /**
     * Semestre de conclusão do curso
     *
     * FIXO
     *
     * @Column(name="nu_semestreconclusao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_semestreconclusao;

    /**
     * Aluno PARFOR
     *
     * FIXO
     *
     * @Column(name="nu_alunoparfor", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_alunoparfor;

    /**
     * Semestre de ingresso no curso
     *
     * FIXO
     *
     * @Column(name="nu_semestreingcurso", type="string",nullable=true, length=6)
     * @var integer
     */
    private $nu_semestreingcurso;

    /**
     * Tipo de escola que concluiu o Ensino Médio
     *
     * FIXO
     *
     * @Column(name="nu_tipoescolaconclusaoensinomedio", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_tipoescolaconclusaoensinomedio;

    /**
     * Forma de ingresso/seleção - Vestibular
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovestibular", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovestibular;

    /**
     * Forma de ingresso/seleção - Enem
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoenem", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoenem;

    /**
     * Forma de ingresso/seleção - Avaliação Seriada
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoavaliacaoseriada", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoavaliacaoseriada;

    /**
     * Forma de ingresso/seleção - Seleção Simplificada
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoselecaosimplificada", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoselecaosimplificada;

    /**
     * Forma de ingresso/seleção - Egresso BI/LI
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoegressobili", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoegressobili;

    /**
     * Forma de ingresso/seleção - PEC-G
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoegressopecg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_formaingselecaoegressopecg;

    /**
     * Forma de ingresso/seleção - Transferência Ex Officio
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaotrasnfexofficio", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaotrasnfexofficio;

    /**
     * Forma de ingresso/seleção - Decisão judicial
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaodecisaojudicial", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaodecisaojudicial;

    /**
     * Forma de ingresso/seleção - Seleção para Vagas Remanescentes
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovagasremanescentes", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovagasremanescentes;

    /**
     * Forma de ingresso/seleção - Seleção para Vagas de Programas Especiais
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovagasprogespeciais", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovagasprogespeciais;

    /**
     * Mobilidade acadêmica
     *
     * FIXO
     *
     * @Column(name="nu_mobilidadeacademica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_mobilidadeacademica;

    /**
     * Tipo de mobilidade acadêmica
     *
     * FIXO
     *
     * @Column(name="nu_tipomodalidadeacademica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipomodalidadeacademica;

    /**
     * IES destino
     *
     * VARIÁVEL
     *
     * @Column(name="st_iesdestino", type="string",length=12,nullable=true, length=12)
     * @var string
     */
    private $st_iesdestino;

    /**
     * Tipo de mobilidade acadêmica internacional
     *
     * FIXO
     *
     * @Column(name="nu_tipomodalidadeacademicainternacional", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipomodalidadeacademicainternacional;

    /**
     * País destino
     *
     * FIXO
     *
     * @Column(name="st_paisdestino", type="string",length=3,nullable=true, length=3)
     * @var string
     */
    private $st_paisdestino;

    /**
     * Programa de reserva de vagas
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagas", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_programareservavagas;

    /**
     * Programa de reserva de vagas/açoes afirmativas - Etnico
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasetnico", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasetnico;

    /**
     * Programa de reserva de vagas/ações afirmativas - Pessoa com deficiência
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagaspessoadeficiencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagaspessoadeficiencia;

    /**
     * Programa de reserva de vagas - Estudante procedente de escola pública
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasestudanteescolapublica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasestudanteescolapublica;

    /**
     * Programa de reserva de vagas/ações afirmativas - Social/renda familiar
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagassocialrendafamiliar", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagassocialrendafamiliar;

    /**
     * Programa de reserva de vagas/ações afirmativas - Outros
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasoutros", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasoutros;

    /**
     * Financiamento estudantil
     *
     * FIXO
     *
     * @Column(name="nu_finestud", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestud;

    /**
     * Financiamento Estudantil Reembolsável - FIES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembfies", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembfies;

    /**
     * Financiamento Estudantil Reembolsável - Governo Estadual
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovest", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovest;

    /**
     * Financiamento Estudantil Reembolsável - Governo Municipal
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovmun", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovmun;

    /**
     * Financiamento Estudantil Reembolsável - IES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembies", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembies;

    /**
     * Financiamento Estudantil Reembolsável - Entidades externas
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembentidadesexternas", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembentidadesexternas;

    /**
     * Financiamento Estudantil Reembolsável - ProUni integral
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembprouniintegral", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembprouniintegral;

    /**
     * Financiamento Estudantil Reembolsável - ProUni parcial
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembprouniparcial", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembprouniparcial;

    /**
     * Tipo de financiamento não reembolsável - Entidades externas
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembentidadesexternas_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembentidadesexternas_2;

    /**
     * Financiamento Estudantil Reembolsável - Governo Estadual
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovest_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovest_2;

    /**
     * Financiamento Estudantil Reembolsável - IES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembies_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembies_2;

    /**
     * Financiamento Estudantil Reembolsável - Governo Municipal
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovmun_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovmun_2;

    /**
     * Apoio Social
     *
     * @Column(name="nu_apoiosocial", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_apoiosocial;

    /**
     * Tipo de apoio social - Alimentação
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialalimentacao", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialalimentacao;

    /**
     * Tipo de apoio social - Moradia
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialmoradia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialmoradia;

    /**
     * Tipo de apoio social - Transporte
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialtransporte", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialtransporte;

    /**
     * Tipo de apoio social - Material didático
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialmaterialdidatico", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialmaterialdidatico;

    /**
     * Tipo de apoio social - Bolsa trabalho
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialbolsatrabalho", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialbolsatrabalho;

    /**
     * Tipo de apoio social - Bolsa permanência
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialbolsapermanencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialbolsapermanencia;

    /**
     * Atividade extra curricular
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurr", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_atvextcurr;

    /**
     * Atividade extra curricular - pesquisa
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrpesquisa", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrpesquisa;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Pesquisa
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrpesquisa", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrpesquisa;

    /**
     * Atividade extracurricular - Extensão
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrextensao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrextensao;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Extensão
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrextensao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrextensao;

    /**
     * Atividade extracurricular - Monitoria
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrmonitoria", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrmonitoria;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Monitoria
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrmonitoria", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrmonitoria;

    /**
     * Atividade extracurricular - Estágio não obrigatório
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrestnaoobg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrestnaoobg;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Estágio não obrigatório
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrestnaoobg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrestnaoobg;

    /**
     * Carga horária total do curso por aluno
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cargahorariototalporaluno", type="integer",nullable=false, length=5)
     * @var integer
     */
    private $nu_cargahorariototalporaluno;

    /**
     * Carga horária integralizada pelo aluno
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cargahorariointegralizadapeloaluno", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $nu_cargahorariointegralizadapeloaluno;

    /**
     * ano do CENSOs
     *
     * @Column(name="nu_anocenso", type="integer",nullable=false)
     * @var integer
     */
    private $nu_anocenso;

    /**
     * ano de matricula do aluno
     *
     * @Column(name="nu_anomatricula", type="integer",nullable=false)
     * @var integer
     */
    private $nu_anomatricula;

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function gettp_registrocabecalho()
    {
        return $this->tp_registrocabecalho;
    }

    public function settp_registrocabecalho($tp_registrocabecalho)
    {
        $this->tp_registrocabecalho = $tp_registrocabecalho;
    }

    /**
     * @return int
     */
    public function getid_iesinep()
    {
        return $this->id_iesinep;
    }

    public function setid_iesinep($id_iesinep)
    {
        $this->id_iesinep = $id_iesinep;
    }

    /**
     * @return int
     */
    public function gettp_arquivocabecalho()
    {
        return $this->tp_arquivocabecalho;
    }

    public function settp_arquivocabecalho($tp_arquivocabecalho)
    {
        $this->tp_arquivocabecalho = $tp_arquivocabecalho;
    }

    /**
     * @return int
     */
    public function gettp_registroaluno()
    {
        return $this->tp_registroaluno;
    }

    public function settp_registroaluno($tp_registroaluno)
    {
        $this->tp_registroaluno = $tp_registroaluno;
    }

    /**
     * @return int
     */
    public function getid_alunoinep()
    {
        return $this->id_alunoinep;
    }

    public function setid_alunoinep($id_alunoinep)
    {
        $this->id_alunoinep = $id_alunoinep;
    }

    /**
     * @return string
     */
    public function getst_nome()
    {
        return $this->st_nome;
    }

    public function setst_nome($st_nome)
    {
        $this->st_nome = $st_nome;
    }

    /**
     * @return string
     */
    public function getst_cpf()
    {
        return $this->st_cpf;
    }

    public function setst_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getst_docuestrangeiropassaporte()
    {
        return $this->st_docuestrangeiropassaporte;
    }

    public function setst_docuestrangeiropassaporte($st_docuestrangeiropassaporte)
    {
        $this->st_docuestrangeiropassaporte = $st_docuestrangeiropassaporte;
    }

    /**
     * @return string
     */
    public function getdt_nascimento()
    {
        return $this->dt_nascimento;
    }

    public function setdt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @return int
     */
    public function getnu_sexo()
    {
        return $this->nu_sexo;
    }

    public function setnu_sexo($nu_sexo)
    {
        $this->nu_sexo = $nu_sexo;
    }

    /**
     * @return int
     */
    public function getnu_corraca()
    {
        return $this->nu_corraca;
    }

    public function setnu_corraca($nu_corraca)
    {
        $this->nu_corraca = $nu_corraca;
    }

    /**
     * @return string
     */
    public function getst_nomecompletomae()
    {
        return $this->st_nomecompletomae;
    }

    public function setst_nomecompletomae($st_nomecompletomae)
    {
        $this->st_nomecompletomae = $st_nomecompletomae;
    }

    /**
     * @return int
     */
    public function getnu_nacionalidade()
    {
        return $this->nu_nacionalidade;
    }

    public function setnu_nacionalidade($nu_nacionalidade)
    {
        $this->nu_nacionalidade = $nu_nacionalidade;
    }

    /**
     * @return int
     */
    public function getnu_ufnascimento()
    {
        return $this->nu_ufnascimento;
    }

    public function setnu_ufnascimento($nu_ufnascimento)
    {
        $this->nu_ufnascimento = $nu_ufnascimento;
    }

    /**
     * @return int
     */
    public function getnu_municipionascimento()
    {
        return $this->nu_municipionascimento;
    }

    public function setnu_municipionascimento($nu_municipionascimento)
    {
        $this->nu_municipionascimento = $nu_municipionascimento;
    }

    /**
     * @return string
     */
    public function getst_paisorigem()
    {
        return $this->st_paisorigem;
    }

    public function setst_paisorigem($st_paisorigem)
    {
        $this->st_paisorigem = $st_paisorigem;
    }

    /**
     * @return int
     */
    public function getnu_alunodeftranstsuperdotacao()
    {
        return $this->nu_alunodeftranstsuperdotacao;
    }

    public function setnu_alunodeftranstsuperdotacao($nu_alunodeftranstsuperdotacao)
    {
        $this->nu_alunodeftranstsuperdotacao = $nu_alunodeftranstsuperdotacao;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciacegueira()
    {
        return $this->nu_tipodeficienciacegueira;
    }

    public function setnu_tipodeficienciacegueira($nu_tipodeficienciacegueira)
    {
        $this->nu_tipodeficienciacegueira = $nu_tipodeficienciacegueira;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciabaixavisao()
    {
        return $this->nu_tipodeficienciabaixavisao;
    }

    public function setnu_tipodeficienciabaixavisao($nu_tipodeficienciabaixavisao)
    {
        $this->nu_tipodeficienciabaixavisao = $nu_tipodeficienciabaixavisao;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciasurdez()
    {
        return $this->nu_tipodeficienciasurdez;
    }

    public function setnu_tipodeficienciasurdez($nu_tipodeficienciasurdez)
    {
        $this->nu_tipodeficienciasurdez = $nu_tipodeficienciasurdez;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciaauditiva()
    {
        return $this->nu_tipodeficienciaauditiva;
    }

    public function setnu_tipodeficienciaauditiva($nu_tipodeficienciaauditiva)
    {
        $this->nu_tipodeficienciaauditiva = $nu_tipodeficienciaauditiva;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciafisica()
    {
        return $this->nu_tipodeficienciafisica;
    }

    public function setnu_tipodeficienciafisica($nu_tipodeficienciafisica)
    {
        $this->nu_tipodeficienciafisica = $nu_tipodeficienciafisica;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciasurdocegueira()
    {
        return $this->nu_tipodeficienciasurdocegueira;
    }

    public function setnu_tipodeficienciasurdocegueira($nu_tipodeficienciasurdocegueira)
    {
        $this->nu_tipodeficienciasurdocegueira = $nu_tipodeficienciasurdocegueira;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciamultipla()
    {
        return $this->nu_tipodeficienciamultipla;
    }

    public function setnu_tipodeficienciamultipla($nu_tipodeficienciamultipla)
    {
        $this->nu_tipodeficienciamultipla = $nu_tipodeficienciamultipla;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciaintelectcual()
    {
        return $this->nu_tipodeficienciaintelectcual;
    }

    public function setnu_tipodeficienciaintelectcual($nu_tipodeficienciaintelectcual)
    {
        $this->nu_tipodeficienciaintelectcual = $nu_tipodeficienciaintelectcual;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciaautismo()
    {
        return $this->nu_tipodeficienciaautismo;
    }

    public function setnu_tipodeficienciaautismo($nu_tipodeficienciaautismo)
    {
        $this->nu_tipodeficienciaautismo = $nu_tipodeficienciaautismo;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciaasperger()
    {
        return $this->nu_tipodeficienciaasperger;
    }

    public function setnu_tipodeficienciaasperger($nu_tipodeficienciaasperger)
    {
        $this->nu_tipodeficienciaasperger = $nu_tipodeficienciaasperger;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciarett()
    {
        return $this->nu_tipodeficienciarett;
    }

    public function setnu_tipodeficienciarett($nu_tipodeficienciarett)
    {
        $this->nu_tipodeficienciarett = $nu_tipodeficienciarett;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciatranstdesinfancia()
    {
        return $this->nu_tipodeficienciatranstdesinfancia;
    }

    public function setnu_tipodeficienciatranstdesinfancia($nu_tipodeficienciatranstdesinfancia)
    {
        $this->nu_tipodeficienciatranstdesinfancia = $nu_tipodeficienciatranstdesinfancia;
    }

    /**
     * @return int
     */
    public function getnu_tipodeficienciasuperdotacao()
    {
        return $this->nu_tipodeficienciasuperdotacao;
    }

    public function setnu_tipodeficienciasuperdotacao($nu_tipodeficienciasuperdotacao)
    {
        $this->nu_tipodeficienciasuperdotacao = $nu_tipodeficienciasuperdotacao;
    }

    /**
     * @return int
     */
    public function gettp_registroalunocurso()
    {
        return $this->tp_registroalunocurso;
    }

    public function settp_registroalunocurso($tp_registroalunocurso)
    {
        $this->tp_registroalunocurso = $tp_registroalunocurso;
    }

    /**
     * @return int
     */
    public function getnu_semestrereferencia()
    {
        return $this->nu_semestrereferencia;
    }

    public function setnu_semestrereferencia($nu_semestrereferencia)
    {
        $this->nu_semestrereferencia = $nu_semestrereferencia;
    }

    /**
     * @return int
     */
    public function getnu_codigocurso()
    {
        return $this->nu_codigocurso;
    }

    public function setnu_codigocurso($nu_codigocurso)
    {
        $this->nu_codigocurso = $nu_codigocurso;
    }

    /**
     * @return int
     */
    public function getnu_codigopolocursodistancia()
    {
        return $this->nu_codigopolocursodistancia;
    }

    public function setnu_codigopolocursodistancia($nu_codigopolocursodistancia)
    {
        $this->nu_codigopolocursodistancia = $nu_codigopolocursodistancia;
    }

    /**
     * @return string
     */
    public function getst_idnaies()
    {
        return $this->st_idnaies;
    }

    public function setst_idnaies($st_idnaies)
    {
        $this->st_idnaies = $st_idnaies;
    }

    /**
     * @return int
     */
    public function getnu_turnoaluno()
    {
        return $this->nu_turnoaluno;
    }

    public function setnu_turnoaluno($nu_turnoaluno)
    {
        $this->nu_turnoaluno = $nu_turnoaluno;
    }

    /**
     * @return int
     */
    public function getnu_situacaovinculoalunocurso()
    {
        return $this->nu_situacaovinculoalunocurso;
    }

    public function setnu_situacaovinculoalunocurso($nu_situacaovinculoalunocurso)
    {
        $this->nu_situacaovinculoalunocurso = $nu_situacaovinculoalunocurso;
    }

    /**
     * @return int
     */
    public function getnu_cursoorigem()
    {
        return $this->nu_cursoorigem;
    }

    public function setnu_cursoorigem($nu_cursoorigem)
    {
        $this->nu_cursoorigem = $nu_cursoorigem;
    }

    /**
     * @return int
     */
    public function getnu_semestreconclusao()
    {
        return $this->nu_semestreconclusao;
    }

    public function setnu_semestreconclusao($nu_semestreconclusao)
    {
        $this->nu_semestreconclusao = $nu_semestreconclusao;
    }

    /**
     * @return int
     */
    public function getnu_alunoparfor()
    {
        return $this->nu_alunoparfor;
    }

    public function setnu_alunoparfor($nu_alunoparfor)
    {
        $this->nu_alunoparfor = $nu_alunoparfor;
    }

    /**
     * @return int
     */
    public function getnu_semestreingcurso()
    {
        return $this->nu_semestreingcurso;
    }

    public function setnu_semestreingcurso($nu_semestreingcurso)
    {
        $this->nu_semestreingcurso = $nu_semestreingcurso;
    }

    /**
     * @return int
     */
    public function getnu_tipoescolaconclusaoensinomedio()
    {
        return $this->nu_tipoescolaconclusaoensinomedio;
    }

    public function setnu_tipoescolaconclusaoensinomedio($nu_tipoescolaconclusaoensinomedio)
    {
        $this->nu_tipoescolaconclusaoensinomedio = $nu_tipoescolaconclusaoensinomedio;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaovestibular()
    {
        return $this->nu_formaingselecaovestibular;
    }

    public function setnu_formaingselecaovestibular($nu_formaingselecaovestibular)
    {
        $this->nu_formaingselecaovestibular = $nu_formaingselecaovestibular;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaoenem()
    {
        return $this->nu_formaingselecaoenem;
    }

    public function setnu_formaingselecaoenem($nu_formaingselecaoenem)
    {
        $this->nu_formaingselecaoenem = $nu_formaingselecaoenem;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaoavaliacaoseriada()
    {
        return $this->nu_formaingselecaoavaliacaoseriada;
    }

    public function setnu_formaingselecaoavaliacaoseriada($nu_formaingselecaoavaliacaoseriada)
    {
        $this->nu_formaingselecaoavaliacaoseriada = $nu_formaingselecaoavaliacaoseriada;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaoselecaosimplificada()
    {
        return $this->nu_formaingselecaoselecaosimplificada;
    }

    public function setnu_formaingselecaoselecaosimplificada($nu_formaingselecaoselecaosimplificada)
    {
        $this->nu_formaingselecaoselecaosimplificada = $nu_formaingselecaoselecaosimplificada;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaoegressobili()
    {
        return $this->nu_formaingselecaoegressobili;
    }

    public function setnu_formaingselecaoegressobili($nu_formaingselecaoegressobili)
    {
        $this->nu_formaingselecaoegressobili = $nu_formaingselecaoegressobili;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaoegressopecg()
    {
        return $this->nu_formaingselecaoegressopecg;
    }

    public function setnu_formaingselecaoegressopecg($nu_formaingselecaoegressopecg)
    {
        $this->nu_formaingselecaoegressopecg = $nu_formaingselecaoegressopecg;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaotrasnfexofficio()
    {
        return $this->nu_formaingselecaotrasnfexofficio;
    }

    public function setnu_formaingselecaotrasnfexofficio($nu_formaingselecaotrasnfexofficio)
    {
        $this->nu_formaingselecaotrasnfexofficio = $nu_formaingselecaotrasnfexofficio;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaodecisaojudicial()
    {
        return $this->nu_formaingselecaodecisaojudicial;
    }

    public function setnu_formaingselecaodecisaojudicial($nu_formaingselecaodecisaojudicial)
    {
        $this->nu_formaingselecaodecisaojudicial = $nu_formaingselecaodecisaojudicial;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaovagasremanescentes()
    {
        return $this->nu_formaingselecaovagasremanescentes;
    }

    public function setnu_formaingselecaovagasremanescentes($nu_formaingselecaovagasremanescentes)
    {
        $this->nu_formaingselecaovagasremanescentes = $nu_formaingselecaovagasremanescentes;
    }

    /**
     * @return int
     */
    public function getnu_formaingselecaovagasprogespeciais()
    {
        return $this->nu_formaingselecaovagasprogespeciais;
    }

    public function setnu_formaingselecaovagasprogespeciais($nu_formaingselecaovagasprogespeciais)
    {
        $this->nu_formaingselecaovagasprogespeciais = $nu_formaingselecaovagasprogespeciais;
    }

    /**
     * @return int
     */
    public function getnu_mobilidadeacademica()
    {
        return $this->nu_mobilidadeacademica;
    }

    public function setnu_mobilidadeacademica($nu_mobilidadeacademica)
    {
        $this->nu_mobilidadeacademica = $nu_mobilidadeacademica;
    }

    /**
     * @return int
     */
    public function getnu_tipomodalidadeacademica()
    {
        return $this->nu_tipomodalidadeacademica;
    }

    public function setnu_tipomodalidadeacademica($nu_tipomodalidadeacademica)
    {
        $this->nu_tipomodalidadeacademica = $nu_tipomodalidadeacademica;
    }

    /**
     * @return string
     */
    public function getst_iesdestino()
    {
        return $this->st_iesdestino;
    }

    public function setst_iesdestino($st_iesdestino)
    {
        $this->st_iesdestino = $st_iesdestino;
    }

    /**
     * @return int
     */
    public function getnu_tipomodalidadeacademicainternacional()
    {
        return $this->nu_tipomodalidadeacademicainternacional;
    }

    public function setnu_tipomodalidadeacademicainternacional($nu_tipomodalidadeacademicainternacional)
    {
        $this->nu_tipomodalidadeacademicainternacional = $nu_tipomodalidadeacademicainternacional;
    }

    /**
     * @return string
     */
    public function getst_paisdestino()
    {
        return $this->st_paisdestino;
    }

    public function setst_paisdestino($st_paisdestino)
    {
        $this->st_paisdestino = $st_paisdestino;
    }

    /**
     * @return int
     */
    public function getnu_programareservavagas()
    {
        return $this->nu_programareservavagas;
    }

    public function setnu_programareservavagas($nu_programareservavagas)
    {
        $this->nu_programareservavagas = $nu_programareservavagas;
    }

    /**
     * @return int
     */
    public function getnu_programareservavagasetnico()
    {
        return $this->nu_programareservavagasetnico;
    }

    public function setnu_programareservavagasetnico($nu_programareservavagasetnico)
    {
        $this->nu_programareservavagasetnico = $nu_programareservavagasetnico;
    }

    /**
     * @return int
     */
    public function getnu_programareservavagaspessoadeficiencia()
    {
        return $this->nu_programareservavagaspessoadeficiencia;
    }

    public function setnu_programareservavagaspessoadeficiencia($nu_programareservavagaspessoadeficiencia)
    {
        $this->nu_programareservavagaspessoadeficiencia = $nu_programareservavagaspessoadeficiencia;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagasestudanteescolapublica()
    {
        return $this->nu_programareservavagasestudanteescolapublica;
    }

    public function setNu_programareservavagasestudanteescolapublica($nu_programareservavagasestudanteescolapublica)
    {
        $this->nu_programareservavagasestudanteescolapublica = $nu_programareservavagasestudanteescolapublica;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagassocialrendafamiliar()
    {
        return $this->nu_programareservavagassocialrendafamiliar;
    }

    public function setNu_programareservavagassocialrendafamiliar($nu_programareservavagassocialrendafamiliar)
    {
        $this->nu_programareservavagassocialrendafamiliar = $nu_programareservavagassocialrendafamiliar;
    }

    /**
     * @return int
     */
    public function getnu_programareservavagasoutros()
    {
        return $this->nu_programareservavagasoutros;
    }

    public function setnu_programareservavagasoutros($nu_programareservavagasoutros)
    {
        $this->nu_programareservavagasoutros = $nu_programareservavagasoutros;
    }

    /**
     * @return int
     */
    public function getnu_finestud()
    {
        return $this->nu_finestud;
    }

    public function setnu_finestud($nu_finestud)
    {
        $this->nu_finestud = $nu_finestud;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembfies()
    {
        return $this->nu_finestudreembfies;
    }

    public function setnu_finestudreembfies($nu_finestudreembfies)
    {
        $this->nu_finestudreembfies = $nu_finestudreembfies;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembgovest()
    {
        return $this->nu_finestudreembgovest;
    }

    public function setnu_finestudreembgovest($nu_finestudreembgovest)
    {
        $this->nu_finestudreembgovest = $nu_finestudreembgovest;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembgovmun()
    {
        return $this->nu_finestudreembgovmun;
    }

    public function setnu_finestudreembgovmun($nu_finestudreembgovmun)
    {
        $this->nu_finestudreembgovmun = $nu_finestudreembgovmun;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembies()
    {
        return $this->nu_finestudreembies;
    }

    public function setnu_finestudreembies($nu_finestudreembies)
    {
        $this->nu_finestudreembies = $nu_finestudreembies;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembentidadesexternas()
    {
        return $this->nu_finestudreembentidadesexternas;
    }

    public function setnu_finestudreembentidadesexternas($nu_finestudreembentidadesexternas)
    {
        $this->nu_finestudreembentidadesexternas = $nu_finestudreembentidadesexternas;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembprouniintegral()
    {
        return $this->nu_finestudreembprouniintegral;
    }

    public function setnu_finestudreembprouniintegral($nu_finestudreembprouniintegral)
    {
        $this->nu_finestudreembprouniintegral = $nu_finestudreembprouniintegral;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembprouniparcial()
    {
        return $this->nu_finestudreembprouniparcial;
    }

    public function setnu_finestudreembprouniparcial($nu_finestudreembprouniparcial)
    {
        $this->nu_finestudreembprouniparcial = $nu_finestudreembprouniparcial;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembentidadesexternas_2()
    {
        return $this->nu_finestudreembentidadesexternas_2;
    }

    public function setnu_finestudreembentidadesexternas_2($nu_finestudreembentidadesexternas_2)
    {
        $this->nu_finestudreembentidadesexternas_2 = $nu_finestudreembentidadesexternas_2;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembgovest_2()
    {
        return $this->nu_finestudreembgovest_2;
    }

    public function setnu_finestudreembgovest_2($nu_finestudreembgovest_2)
    {
        $this->nu_finestudreembgovest_2 = $nu_finestudreembgovest_2;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembies_2()
    {
        return $this->nu_finestudreembies_2;
    }

    public function setnu_finestudreembies_2($nu_finestudreembies_2)
    {
        $this->nu_finestudreembies_2 = $nu_finestudreembies_2;
    }

    /**
     * @return int
     */
    public function getnu_finestudreembgovmun_2()
    {
        return $this->nu_finestudreembgovmun_2;
    }

    public function setnu_finestudreembgovmun_2($nu_finestudreembgovmun_2)
    {
        $this->nu_finestudreembgovmun_2 = $nu_finestudreembgovmun_2;
    }

    /**
     * @return int
     */
    public function getnu_apoiosocial()
    {
        return $this->nu_apoiosocial;
    }

    public function setnu_apoiosocial($nu_apoiosocial)
    {
        $this->nu_apoiosocial = $nu_apoiosocial;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialalimentacao()
    {
        return $this->nu_tipoapoiosocialalimentacao;
    }

    public function setnu_tipoapoiosocialalimentacao($nu_tipoapoiosocialalimentacao)
    {
        $this->nu_tipoapoiosocialalimentacao = $nu_tipoapoiosocialalimentacao;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialmoradia()
    {
        return $this->nu_tipoapoiosocialmoradia;
    }

    public function setnu_tipoapoiosocialmoradia($nu_tipoapoiosocialmoradia)
    {
        $this->nu_tipoapoiosocialmoradia = $nu_tipoapoiosocialmoradia;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialtransporte()
    {
        return $this->nu_tipoapoiosocialtransporte;
    }

    public function setnu_tipoapoiosocialtransporte($nu_tipoapoiosocialtransporte)
    {
        $this->nu_tipoapoiosocialtransporte = $nu_tipoapoiosocialtransporte;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialmaterialdidatico()
    {
        return $this->nu_tipoapoiosocialmaterialdidatico;
    }

    public function setnu_tipoapoiosocialmaterialdidatico($nu_tipoapoiosocialmaterialdidatico)
    {
        $this->nu_tipoapoiosocialmaterialdidatico = $nu_tipoapoiosocialmaterialdidatico;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialbolsatrabalho()
    {
        return $this->nu_tipoapoiosocialbolsatrabalho;
    }

    public function setnu_tipoapoiosocialbolsatrabalho($nu_tipoapoiosocialbolsatrabalho)
    {
        $this->nu_tipoapoiosocialbolsatrabalho = $nu_tipoapoiosocialbolsatrabalho;
    }

    /**
     * @return int
     */
    public function getnu_tipoapoiosocialbolsapermanencia()
    {
        return $this->nu_tipoapoiosocialbolsapermanencia;
    }

    public function setnu_tipoapoiosocialbolsapermanencia($nu_tipoapoiosocialbolsapermanencia)
    {
        $this->nu_tipoapoiosocialbolsapermanencia = $nu_tipoapoiosocialbolsapermanencia;
    }

    /**
     * @return int
     */
    public function getnu_atvextcurr()
    {
        return $this->nu_atvextcurr;
    }

    public function setnu_atvextcurr($nu_atvextcurr)
    {
        $this->nu_atvextcurr = $nu_atvextcurr;
    }

    /**
     * @return int
     */
    public function getnu_atvextcurrpesquisa()
    {
        return $this->nu_atvextcurrpesquisa;
    }

    public function setnu_atvextcurrpesquisa($nu_atvextcurrpesquisa)
    {
        $this->nu_atvextcurrpesquisa = $nu_atvextcurrpesquisa;
    }

    /**
     * @return int
     */
    public function getnu_bolsaremuneracaoatvextcurrpesquisa()
    {
        return $this->nu_bolsaremuneracaoatvextcurrpesquisa;
    }

    public function setnu_bolsaremuneracaoatvextcurrpesquisa($nu_bolsaremuneracaoatvextcurrpesquisa)
    {
        $this->nu_bolsaremuneracaoatvextcurrpesquisa = $nu_bolsaremuneracaoatvextcurrpesquisa;
    }

    /**
     * @return int
     */
    public function getnu_atvextcurrextensao()
    {
        return $this->nu_atvextcurrextensao;
    }

    public function setnu_atvextcurrextensao($nu_atvextcurrextensao)
    {
        $this->nu_atvextcurrextensao = $nu_atvextcurrextensao;
    }

    /**
     * @return int
     */
    public function getnu_bolsaremuneracaoatvextcurrextensao()
    {
        return $this->nu_bolsaremuneracaoatvextcurrextensao;
    }

    public function setnu_bolsaremuneracaoatvextcurrextensao($nu_bolsaremuneracaoatvextcurrextensao)
    {
        $this->nu_bolsaremuneracaoatvextcurrextensao = $nu_bolsaremuneracaoatvextcurrextensao;
    }

    /**
     * @return int
     */
    public function getnu_atvextcurrmonitoria()
    {
        return $this->nu_atvextcurrmonitoria;
    }

    public function setnu_atvextcurrmonitoria($nu_atvextcurrmonitoria)
    {
        $this->nu_atvextcurrmonitoria = $nu_atvextcurrmonitoria;
    }

    /**
     * @return int
     */
    public function getnu_bolsaremuneracaoatvextcurrmonitoria()
    {
        return $this->nu_bolsaremuneracaoatvextcurrmonitoria;
    }

    public function setnu_bolsaremuneracaoatvextcurrmonitoria($nu_bolsaremuneracaoatvextcurrmonitoria)
    {
        $this->nu_bolsaremuneracaoatvextcurrmonitoria = $nu_bolsaremuneracaoatvextcurrmonitoria;
    }

    /**
     * @return int
     */
    public function getnu_atvextcurrestnaoobg()
    {
        return $this->nu_atvextcurrestnaoobg;
    }

    public function setnu_atvextcurrestnaoobg($nu_atvextcurrestnaoobg)
    {
        $this->nu_atvextcurrestnaoobg = $nu_atvextcurrestnaoobg;
    }

    /**
     * @return int
     */
    public function getnu_bolsaremuneracaoatvextcurrestnaoobg()
    {
        return $this->nu_bolsaremuneracaoatvextcurrestnaoobg;
    }

    public function setnu_bolsaremuneracaoatvextcurrestnaoobg($nu_bolsaremuneracaoatvextcurrestnaoobg)
    {
        $this->nu_bolsaremuneracaoatvextcurrestnaoobg = $nu_bolsaremuneracaoatvextcurrestnaoobg;
    }

    /**
     * @return int
     */
    public function getnu_cargahorariototalporaluno()
    {
        return $this->nu_cargahorariototalporaluno;
    }

    public function setnu_cargahorariototalporaluno($nu_cargahorariototalporaluno)
    {
        $this->nu_cargahorariototalporaluno = $nu_cargahorariototalporaluno;
    }

    /**
     * @return int
     */
    public function getnu_cargahorariointegralizadapeloaluno()
    {
        return $this->nu_cargahorariointegralizadapeloaluno;
    }

    public function setnu_cargahorariointegralizadapeloaluno($nu_cargahorariointegralizadapeloaluno)
    {
        $this->nu_cargahorariointegralizadapeloaluno = $nu_cargahorariointegralizadapeloaluno;
    }

    /**
     * @return int
     */
    public function getnu_anocenso()
    {
        return $this->nu_anocenso;
    }

    public function setnu_anocenso($nu_anocenso)
    {
        $this->nu_anocenso = $nu_anocenso;
    }

    /**
     * @return int
     */
    public function getnu_anomatricula()
    {
        return $this->nu_anomatricula;
    }

    public function setnu_anomatricula($nu_anomatricula)
    {
        $this->nu_anomatricula = $nu_anomatricula;
    }

}

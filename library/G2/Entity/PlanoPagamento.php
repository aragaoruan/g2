<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_planopagamento")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class PlanoPagamento
{

    /**
     *
     * @var integer $id_planopagamento
     * @Column(name="id_planopagamento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_planopagamento;

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    
    /**
     *
     * @var string $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=false) 
     */
    private $nu_parcelas;
    
    /**
     *
     * @var string $nu_valorparcela
     * @Column(name="nu_valorparcela", type="decimal", nullable=false) 
     */
    private $nu_valorparcela;
    
    
    /**
     *
     * @var string $nu_valorentrada
     * @Column(name="nu_valorentrada", type="decimal", nullable=false) 
     */
    private $nu_valorentrada;
    
    /**
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao",type="boolean", nullable=false)
     */
    private $bl_padrao;

    
    public function getId_planopagamento() {
        return $this->id_planopagamento;
    }

    public function setId_planopagamento($id_planopagamento) {
        $this->id_planopagamento = $id_planopagamento;
        return $this;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getNu_parcelas() {
        return $this->nu_parcelas;
    }

    public function setNu_parcelas($nu_parcelas) {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    public function getNu_valorparcela() {
        return $this->nu_valorparcela;
    }

    public function setNu_valorparcela($nu_valorparcela) {
        $this->nu_valorparcela = $nu_valorparcela;
        return $this;
    }

    public function getNu_valorentrada() {
        return $this->nu_valorentrada;
    }

    public function setNu_valorentrada($nu_valorentrada) {
        $this->nu_valorentrada = $nu_valorentrada;
        return $this;
    }

    public function getBl_padrao() {
        return $this->bl_padrao;
    }

    public function setBl_padrao($bl_padrao) {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }


}
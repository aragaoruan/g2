<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alunosreativarmoodle")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAlunosReativarMoodle
{

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_saladeaula;
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_alocacao;
    /**
     * @var integer $id_alocacaointegracao
     * @Column(name="id_alocacaointegracao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_alocacaointegracao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var boolean $bl_encerrado
     * @Column(name="bl_encerrado", type="boolean", nullable=false, length=1)
     */
    private $bl_encerrado;
    /**
     * @var string $st_codsistemacurso
     * @Column(name="st_codsistemacurso", type="string", nullable=false, length=30)
     */
    private $st_codsistemacurso;

    /**
     * @var integer $nu_perfilalunoencerrado
     * @Column(name="nu_perfilalunoencerrado", type="integer", nullable=true, length=4)
     */
    private $nu_perfilalunoencerrado;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_sistema;

    /**
     * @var integer $id_entidadeintegracao
     * @Column(name="id_entidadeintegracao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidadeintegracao;

    /**
     * @return int
     */
    public function getid_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwAlunosReativarMoodle
     */
    public function setid_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_alocacao
     * @return VwAlunosReativarMoodle
     */
    public function setid_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_alocacaointegracao()
    {
        return $this->id_alocacaointegracao;
    }

    /**
     * @param int $id_alocacaointegracao
     * @return VwAlunosReativarMoodle
     */
    public function setid_alocacaointegracao($id_alocacaointegracao)
    {
        $this->id_alocacaointegracao = $id_alocacaointegracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwAlunosReativarMoodle
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return VwAlunosReativarMoodle
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return VwAlunosReativarMoodle
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return bool
     */
    public function getbl_encerrado()
    {
        return $this->bl_encerrado;
    }

    /**
     * @param bool $bl_encerrado
     * @return VwAlunosReativarMoodle
     */
    public function setbl_encerrado($bl_encerrado)
    {
        $this->bl_encerrado = $bl_encerrado;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codsistemacurso()
    {
        return $this->st_codsistemacurso;
    }

    /**
     * @param string $st_codsistemacurso
     * @return VwAlunosReativarMoodle
     */
    public function setst_codsistemacurso($st_codsistemacurso)
    {
        $this->st_codsistemacurso = $st_codsistemacurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_perfilalunoencerrado()
    {
        return $this->nu_perfilalunoencerrado;
    }

    /**
     * @param int $nu_perfilalunoencerrado
     * @return VwAlunosReativarMoodle
     */
    public function setnu_perfilalunoencerrado($nu_perfilalunoencerrado)
    {
        $this->nu_perfilalunoencerrado = $nu_perfilalunoencerrado;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     * @return VwAlunosReativarMoodle
     */
    public function setid_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidadeintegracao()
    {
        return $this->id_entidadeintegracao;
    }

    /**
     * @param int $id_entidadeintegracao
     * @return VwAlunosReativarMoodle
     */
    public function setid_entidadeintegracao($id_entidadeintegracao)
    {
        $this->id_entidadeintegracao = $id_entidadeintegracao;
        return $this;
    }


}

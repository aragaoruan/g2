<?php

namespace G2\Entity;

/**
 * @Table(name="vw_censo")
 * @Entity
 * @EntityView
 */
class VwCenso
{

    /**
     * id da matricula, chave primária
     *
     * @Id
     * @var integer - Id do Usuario
     * @Column(name="id_matricula", type="integer",nullable=false)
     */
    private $id_matricula;

    /**
     *  Tipo de registro - cabeçalho
     *
     * FIXO
     *
     * @Column(name="tp_registrocabecalho", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registrocabecalho;

    /**
     *  ID da IES no INEP
     *
     * VARIÁVEL
     *
     * @Column(name="id_iesinep", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $id_iesinep;

    /**
     *  Tipo de arquivo - cabeçalho
     *
     * FIXO
     *
     * @Column(name="tp_arquivocabecalho", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $tp_arquivocabecalho;

    /**
     *  Tipo de registro - aluno
     *
     * FIXO
     *
     * @Column(name="tp_registroaluno", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registroaluno;

    /**
     *  ID do aluno no INEP
     *
     * FIXO
     *
     * @Column(name="id_alunoinep", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $id_alunoinep;

    /**
     * Nome
     *
     * VARIÁVEL
     *
     * @Column(name="st_nome", type="string",length=120,nullable=false, length=120)
     * @var string
     */
    private $st_nome;

    /**
     * CPF
     *
     * FIXO
     *
     * @Column(name="st_cpf", type="string",length=11,nullable=false, length=11)
     * @var string
     */
    private $st_cpf;

    /**
     * Documento de estrangeiro ou passaporte
     *
     * VARIÁVEL
     *
     * @Column(name="st_docuestrangeiropassaporte", type="string",length=20,nullable=true, length=20)
     * @var string
     */
    private $st_docuestrangeiropassaporte;

    /**
     * Data de Nascimento
     *
     * FIXO
     *
     * @Column(name="dt_nascimento",  type="string",length=10,nullable=false, length=8)
     * @var string
     */
    private $dt_nascimento;

    /**
     * Sexo
     *
     * FIXO
     *
     * @Column(name="nu_sexo", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_sexo;

    /**
     * Cor/Raça
     *
     * FIXO
     *
     * @Column(name="nu_corraca", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_corraca;

    /**
     * Nome completo da mãe
     *
     * VARIÁVEL
     *
     * @Column(name="st_nomecompletomae", type="string",length=120,nullable=true, length=120)
     * @var string
     */
    private $st_nomecompletomae;

    /**
     * Nacionalidade
     *
     * FIXO
     *
     * @Column(name="nu_nacionalidade", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_nacionalidade;

    /**
     * UF de nascimento
     *
     * FIXO
     *
     * @Column(name="nu_ufnascimento", type="integer",nullable=true, length=2)
     * @var integer
     */
    private $nu_ufnascimento;

    /**
     * Município de nascimento
     *
     * FIXO
     *
     * @Column(name="nu_municipionascimento", type="integer",nullable=true, length=7)
     * @var integer
     */
    private $nu_municipionascimento;

    /**
     * País de origem
     *
     * FIXO
     *
     * @Column(name="st_paisorigem", type="string",length=3,nullable=false, length=3)
     * @var string
     */
    private $st_paisorigem;

    /**
     * Aluno com deficiência, transtorno global do desenvolvimento ou altas habilidades/superdotação
     *
     * FIXO
     *
     * @Column(name="nu_alunodeftranstsuperdotacao", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_alunodeftranstsuperdotacao;

    /**
     * Tipo de deficiência - Cegueira
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciacegueira", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciacegueira;

    /**
     * Tipo de deficiência - Baixa visão
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciabaixavisao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciabaixavisao;

    /**
     * Tipo de deficiência - Surdez
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasurdez", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasurdez;

    /**
     * Tipo de deficiência - auditiva
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaauditiva", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaauditiva;

    /**
     * Tipo de deficiência - auditiva
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciafisica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciafisica;

    /**
     * Tipo de deficiência - Surdocegueira
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasurdocegueira", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasurdocegueira;

    /**
     * Tipo de deficiência - Múltipla
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciamultipla", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciamultipla;

    /**
     * Tipo de deficiência - Intelectual
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaintelectcual", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaintelectcual;

    /**
     *  Tipo de deficiência - Autismo(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaautismo", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaautismo;

    /**
     *  Tipo de deficiência - Síndrome de Asperger(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciaasperger", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciaasperger;

    /**
     *  Tipo de deficiência - Síndrome de RETT(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciarett", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciarett;

    /**
     * Tipo de deficiência - Transtorno desintegrativo da infância(Transtorno global do desenvolvimento)
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciatranstdesinfancia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciatranstdesinfancia;

    /**
     * Tipo de deficiência - Altas habilidades/superdotação
     *
     * FIXO
     *
     * @Column(name="nu_tipodeficienciasuperdotacao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipodeficienciasuperdotacao;

    /**
     * Tipo de registro - aluno > curso
     *
     * FIXO
     *
     * @Column(name="tp_registroalunocurso", type="integer",nullable=false, length=2)
     * @var integer
     */
    private $tp_registroalunocurso;

    /**
     * Semestre de referência
     *
     * FIXO
     *
     * @Column(name="nu_semestrereferencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_semestrereferencia;

    /**
     * Código do curso
     *
     * VARIÁVEL
     *
     * @Column(name="nu_codigocurso", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $nu_codigocurso;

    /**
     * Código do pólo do curso a distância
     *
     * VARIÁVEL
     *
     * @Column(name="nu_codigopolocursodistancia", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $nu_codigopolocursodistancia;

    /**
     * ID na IES - Identificação única do aluno na IES
     *
     * VARIÁVEL
     *
     * @Column(name="st_idnaies", type="string",length=20,nullable=true, length=20)
     * @var string
     */
    private $st_idnaies;

    /**
     * Turno do aluno
     *
     * FIXO
     *
     * @Column(name="nu_turnoaluno", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_turnoaluno;

    /**
     * Situação de vínculo do aluno ao curso
     *
     * FIXO
     *
     * @Column(name="nu_situacaovinculoalunocurso", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_situacaovinculoalunocurso;

    /**
     * Curso de origem
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cursoorigem", type="integer",nullable=true, length=12)
     * @var integer
     */
    private $nu_cursoorigem;

    /**
     * Semestre de conclusão do curso
     *
     * FIXO
     *
     * @Column(name="nu_semestreconclusao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_semestreconclusao;

    /**
     * Aluno PARFOR
     *
     * FIXO
     *
     * @Column(name="nu_alunoparfor", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_alunoparfor;

    /**
     * Semestre de ingresso no curso
     *
     * FIXO
     *
     * @Column(name="nu_semestreingcurso", type="string",nullable=true, length=6)
     * @var integer
     */
    private $nu_semestreingcurso;

    /**
     * Tipo de escola que concluiu o Ensino Médio
     *
     * FIXO
     *
     * @Column(name="nu_tipoescolaconclusaoensinomedio", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_tipoescolaconclusaoensinomedio;

    /**
     * Forma de ingresso/seleção - Vestibular
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovestibular", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovestibular;

    /**
     * Forma de ingresso/seleção - Enem
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoenem", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoenem;

    /**
     * Forma de ingresso/seleção - Avaliação Seriada
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoavaliacaoseriada", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoavaliacaoseriada;

    /**
     * Forma de ingresso/seleção - Seleção Simplificada
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoselecaosimplificada", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoselecaosimplificada;

    /**
     * Forma de ingresso/seleção - Egresso BI/LI
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoegressobili", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaoegressobili;

    /**
     * Forma de ingresso/seleção - PEC-G
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaoegressopecg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_formaingselecaoegressopecg;

    /**
     * Forma de ingresso/seleção - Transferência Ex Officio
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaotrasnfexofficio", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaotrasnfexofficio;

    /**
     * Forma de ingresso/seleção - Decisão judicial
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaodecisaojudicial", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaodecisaojudicial;

    /**
     * Forma de ingresso/seleção - Seleção para Vagas Remanescentes
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovagasremanescentes", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovagasremanescentes;

    /**
     * Forma de ingresso/seleção - Seleção para Vagas de Programas Especiais
     *
     * FIXO
     *
     * @Column(name="nu_formaingselecaovagasprogespeciais", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_formaingselecaovagasprogespeciais;

    /**
     * Mobilidade acadêmica
     *
     * FIXO
     *
     * @Column(name="nu_mobilidadeacademica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_mobilidadeacademica;

    /**
     * Tipo de mobilidade acadêmica
     *
     * FIXO
     *
     * @Column(name="nu_tipomodalidadeacademica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipomodalidadeacademica;

    /**
     * IES destino
     *
     * VARIÁVEL
     *
     * @Column(name="st_iesdestino", type="string",length=12,nullable=true, length=12)
     * @var string
     */
    private $st_iesdestino;

    /**
     * Tipo de mobilidade acadêmica internacional
     *
     * FIXO
     *
     * @Column(name="nu_tipomodalidadeacademicainternacional", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipomodalidadeacademicainternacional;

    /**
     * País destino
     *
     * FIXO
     *
     * @Column(name="st_paisdestino", type="string",length=3,nullable=true, length=3)
     * @var string
     */
    private $st_paisdestino;

    /**
     * Programa de reserva de vagas
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagas", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_programareservavagas;

    /**
     * Programa de reserva de vagas/açoes afirmativas - Etnico
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasetnico", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasetnico;

    /**
     * Programa de reserva de vagas/ações afirmativas - Pessoa com deficiência
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagaspessoadeficiencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagaspessoadeficiencia;

    /**
     * Programa de reserva de vagas - Estudante procedente de escola pública
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasestudanteescolapublica", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasestudanteescolapublica;

    /**
     * Programa de reserva de vagas/ações afirmativas - Social/renda familiar
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagassocialrendafamiliar", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagassocialrendafamiliar;

    /**
     * Programa de reserva de vagas/ações afirmativas - Outros
     *
     * FIXO
     *
     * @Column(name="nu_programareservavagasoutros", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_programareservavagasoutros;

    /**
     * Financiamento estudantil
     *
     * FIXO
     *
     * @Column(name="nu_finestud", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestud;

    /**
     * Financiamento Estudantil Reembolsável - FIES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembfies", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembfies;

    /**
     * Financiamento Estudantil Reembolsável - Governo Estadual
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovest", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovest;

    /**
     * Financiamento Estudantil Reembolsável - Governo Municipal
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovmun", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovmun;

    /**
     * Financiamento Estudantil Reembolsável - IES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembies", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembies;

    /**
     * Financiamento Estudantil Reembolsável - Entidades externas
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembentidadesexternas", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembentidadesexternas;

    /**
     * Financiamento Estudantil Reembolsável - ProUni integral
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembprouniintegral", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembprouniintegral;

    /**
     * Financiamento Estudantil Reembolsável - ProUni parcial
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembprouniparcial", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembprouniparcial;

    /**
     * Tipo de financiamento não reembolsável - Entidades externas
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembentidadesexternas_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembentidadesexternas_2;

    /**
     * Financiamento Estudantil Reembolsável - Governo Estadual
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovest_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovest_2;

    /**
     * Financiamento Estudantil Reembolsável - IES
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembies_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembies_2;

    /**
     * Financiamento Estudantil Reembolsável - Governo Municipal
     *
     * FIXO
     *
     * @Column(name="nu_finestudreembgovmun_2", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_finestudreembgovmun_2;

    /**
     * Apoio Social
     *
     * @Column(name="nu_apoiosocial", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_apoiosocial;

    /**
     * Tipo de apoio social - Alimentação
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialalimentacao", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialalimentacao;

    /**
     * Tipo de apoio social - Moradia
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialmoradia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialmoradia;

    /**
     * Tipo de apoio social - Transporte
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialtransporte", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialtransporte;

    /**
     * Tipo de apoio social - Material didático
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialmaterialdidatico", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialmaterialdidatico;

    /**
     * Tipo de apoio social - Bolsa trabalho
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialbolsatrabalho", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialbolsatrabalho;

    /**
     * Tipo de apoio social - Bolsa permanência
     *
     * FIXO
     *
     * @Column(name="nu_tipoapoiosocialbolsapermanencia", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_tipoapoiosocialbolsapermanencia;

    /**
     * Atividade extra curricular
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurr", type="integer",nullable=false, length=1)
     * @var integer
     */
    private $nu_atvextcurr;

    /**
     * Atividade extra curricular - pesquisa
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrpesquisa", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrpesquisa;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Pesquisa
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrpesquisa", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrpesquisa;

    /**
     * Atividade extracurricular - Extensão
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrextensao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrextensao;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Extensão
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrextensao", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrextensao;

    /**
     * Atividade extracurricular - Monitoria
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrmonitoria", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrmonitoria;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Monitoria
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrmonitoria", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrmonitoria;

    /**
     * Atividade extracurricular - Estágio não obrigatório
     *
     * FIXO
     *
     * @Column(name="nu_atvextcurrestnaoobg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_atvextcurrestnaoobg;

    /**
     * Bolsa/remuneração referente à atividade extracurricular - Estágio não obrigatório
     *
     * FIXO
     *
     * @Column(name="nu_bolsaremuneracaoatvextcurrestnaoobg", type="integer",nullable=true, length=1)
     * @var integer
     */
    private $nu_bolsaremuneracaoatvextcurrestnaoobg;

    /**
     * Carga horária total do curso por aluno
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cargahorariototalporaluno", type="integer",nullable=false, length=5)
     * @var integer
     */
    private $nu_cargahorariototalporaluno;

    /**
     * Carga horária integralizada pelo aluno
     *
     * VARIÁVEL
     *
     * @Column(name="nu_cargahorariointegralizadapeloaluno", type="integer",nullable=false, length=12)
     * @var integer
     */
    private $nu_cargahorariointegralizadapeloaluno;

    /**
     * ano de matricula do aluno
     *
     * @Column(name="nu_anomatricula", type="integer",nullable=false)
     * @var integer
     */
    private $nu_anomatricula;

    /**
     * ano de referência do censo
     *
     * @Column(name="nu_anocenso", type="integer", nullable=false)
     * @var integer
     */
    private $nu_anocenso;

    /**
     * @Column(name="id_entidade", type="integer", nullable=false)
     * @var integer
     */
    private $id_entidade;

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getTp_registrocabecalho()
    {
        return $this->tp_registrocabecalho;
    }

    /**
     * @param int $tp_registrocabecalho
     * @return $this
     */
    public function setTp_registrocabecalho($tp_registrocabecalho)
    {
        $this->tp_registrocabecalho = $tp_registrocabecalho;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_iesinep()
    {
        return $this->id_iesinep;
    }

    /**
     * @param int $id_iesinep
     * @return $this
     */
    public function setId_iesinep($id_iesinep)
    {
        $this->id_iesinep = $id_iesinep;
        return $this;
    }

    /**
     * @return int
     */
    public function getTp_arquivocabecalho()
    {
        return $this->tp_arquivocabecalho;
    }

    /**
     * @param int $tp_arquivocabecalho
     * @return $this
     */
    public function setTp_arquivocabecalho($tp_arquivocabecalho)
    {
        $this->tp_arquivocabecalho = $tp_arquivocabecalho;
        return $this;
    }

    /**
     * @return int
     */
    public function getTp_registroaluno()
    {
        return $this->tp_registroaluno;
    }

    /**
     * @param int $tp_registroaluno
     * @return $this
     */
    public function setTp_registroaluno($tp_registroaluno)
    {
        $this->tp_registroaluno = $tp_registroaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_alunoinep()
    {
        return $this->id_alunoinep;
    }

    /**
     * @param int $id_alunoinep
     * @return $this
     */
    public function setId_alunoinep($id_alunoinep)
    {
        $this->id_alunoinep = $id_alunoinep;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nome()
    {
        return $this->st_nome;
    }

    /**
     * @param string $st_nome
     * @return $this
     */
    public function setSt_nome($st_nome)
    {
        $this->st_nome = $st_nome;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     * @return $this
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_docuestrangeiropassaporte()
    {
        return $this->st_docuestrangeiropassaporte;
    }

    /**
     * @param string $st_docuestrangeiropassaporte
     * @return $this
     */
    public function setSt_docuestrangeiropassaporte($st_docuestrangeiropassaporte)
    {
        $this->st_docuestrangeiropassaporte = $st_docuestrangeiropassaporte;
        return $this;
    }

    /**
     * @return string
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @param string $dt_nascimento
     * @return $this
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_sexo()
    {
        return $this->nu_sexo;
    }

    /**
     * @param int $nu_sexo
     * @return $this
     */
    public function setNu_sexo($nu_sexo)
    {
        $this->nu_sexo = $nu_sexo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_corraca()
    {
        return $this->nu_corraca;
    }

    /**
     * @param int $nu_corraca
     * @return $this
     */
    public function setNu_corraca($nu_corraca)
    {
        $this->nu_corraca = $nu_corraca;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomecompletomae()
    {
        return $this->st_nomecompletomae;
    }

    /**
     * @param string $st_nomecompletomae
     * @return $this
     */
    public function setSt_nomecompletomae($st_nomecompletomae)
    {
        $this->st_nomecompletomae = $st_nomecompletomae;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_nacionalidade()
    {
        return $this->nu_nacionalidade;
    }

    /**
     * @param int $nu_nacionalidade
     * @return $this
     */
    public function setNu_nacionalidade($nu_nacionalidade)
    {
        $this->nu_nacionalidade = $nu_nacionalidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_ufnascimento()
    {
        return $this->nu_ufnascimento;
    }

    /**
     * @param int $nu_ufnascimento
     * @return $this
     */
    public function setNu_ufnascimento($nu_ufnascimento)
    {
        $this->nu_ufnascimento = $nu_ufnascimento;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_municipionascimento()
    {
        return $this->nu_municipionascimento;
    }

    /**
     * @param int $nu_municipionascimento
     * @return $this
     */
    public function setNu_municipionascimento($nu_municipionascimento)
    {
        $this->nu_municipionascimento = $nu_municipionascimento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_paisorigem()
    {
        return $this->st_paisorigem;
    }

    /**
     * @param string $st_paisorigem
     * @return $this
     */
    public function setSt_paisorigem($st_paisorigem)
    {
        $this->st_paisorigem = $st_paisorigem;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_alunodeftranstsuperdotacao()
    {
        return $this->nu_alunodeftranstsuperdotacao;
    }

    /**
     * @param int $nu_alunodeftranstsuperdotacao
     * @return $this
     */
    public function setNu_alunodeftranstsuperdotacao($nu_alunodeftranstsuperdotacao)
    {
        $this->nu_alunodeftranstsuperdotacao = $nu_alunodeftranstsuperdotacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciacegueira()
    {
        return $this->nu_tipodeficienciacegueira;
    }

    /**
     * @param int $nu_tipodeficienciacegueira
     * @return $this
     */
    public function setNu_tipodeficienciacegueira($nu_tipodeficienciacegueira)
    {
        $this->nu_tipodeficienciacegueira = $nu_tipodeficienciacegueira;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciabaixavisao()
    {
        return $this->nu_tipodeficienciabaixavisao;
    }

    /**
     * @param int $nu_tipodeficienciabaixavisao
     * @return $this
     */
    public function setNu_tipodeficienciabaixavisao($nu_tipodeficienciabaixavisao)
    {
        $this->nu_tipodeficienciabaixavisao = $nu_tipodeficienciabaixavisao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciasurdez()
    {
        return $this->nu_tipodeficienciasurdez;
    }

    /**
     * @param int $nu_tipodeficienciasurdez
     * @return $this
     */
    public function setNu_tipodeficienciasurdez($nu_tipodeficienciasurdez)
    {
        $this->nu_tipodeficienciasurdez = $nu_tipodeficienciasurdez;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciaauditiva()
    {
        return $this->nu_tipodeficienciaauditiva;
    }

    /**
     * @param int $nu_tipodeficienciaauditiva
     * @return $this
     */
    public function setNu_tipodeficienciaauditiva($nu_tipodeficienciaauditiva)
    {
        $this->nu_tipodeficienciaauditiva = $nu_tipodeficienciaauditiva;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciafisica()
    {
        return $this->nu_tipodeficienciafisica;
    }

    /**
     * @param int $nu_tipodeficienciafisica
     * @return $this
     */
    public function setNu_tipodeficienciafisica($nu_tipodeficienciafisica)
    {
        $this->nu_tipodeficienciafisica = $nu_tipodeficienciafisica;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciasurdocegueira()
    {
        return $this->nu_tipodeficienciasurdocegueira;
    }

    /**
     * @param int $nu_tipodeficienciasurdocegueira
     * @return $this
     */
    public function setNu_tipodeficienciasurdocegueira($nu_tipodeficienciasurdocegueira)
    {
        $this->nu_tipodeficienciasurdocegueira = $nu_tipodeficienciasurdocegueira;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciamultipla()
    {
        return $this->nu_tipodeficienciamultipla;
    }

    /**
     * @param int $nu_tipodeficienciamultipla
     * @return $this
     */
    public function setNu_tipodeficienciamultipla($nu_tipodeficienciamultipla)
    {
        $this->nu_tipodeficienciamultipla = $nu_tipodeficienciamultipla;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciaintelectcual()
    {
        return $this->nu_tipodeficienciaintelectcual;
    }

    /**
     * @param int $nu_tipodeficienciaintelectcual
     * @return $this
     */
    public function setNu_tipodeficienciaintelectcual($nu_tipodeficienciaintelectcual)
    {
        $this->nu_tipodeficienciaintelectcual = $nu_tipodeficienciaintelectcual;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciaautismo()
    {
        return $this->nu_tipodeficienciaautismo;
    }

    /**
     * @param int $nu_tipodeficienciaautismo
     * @return $this
     */
    public function setNu_tipodeficienciaautismo($nu_tipodeficienciaautismo)
    {
        $this->nu_tipodeficienciaautismo = $nu_tipodeficienciaautismo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciaasperger()
    {
        return $this->nu_tipodeficienciaasperger;
    }

    /**
     * @param int $nu_tipodeficienciaasperger
     * @return $this
     */
    public function setNu_tipodeficienciaasperger($nu_tipodeficienciaasperger)
    {
        $this->nu_tipodeficienciaasperger = $nu_tipodeficienciaasperger;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciarett()
    {
        return $this->nu_tipodeficienciarett;
    }

    /**
     * @param int $nu_tipodeficienciarett
     * @return $this
     */
    public function setNu_tipodeficienciarett($nu_tipodeficienciarett)
    {
        $this->nu_tipodeficienciarett = $nu_tipodeficienciarett;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciatranstdesinfancia()
    {
        return $this->nu_tipodeficienciatranstdesinfancia;
    }

    /**
     * @param int $nu_tipodeficienciatranstdesinfancia
     * @return $this
     */
    public function setNu_tipodeficienciatranstdesinfancia($nu_tipodeficienciatranstdesinfancia)
    {
        $this->nu_tipodeficienciatranstdesinfancia = $nu_tipodeficienciatranstdesinfancia;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipodeficienciasuperdotacao()
    {
        return $this->nu_tipodeficienciasuperdotacao;
    }

    /**
     * @param int $nu_tipodeficienciasuperdotacao
     * @return $this
     */
    public function setNu_tipodeficienciasuperdotacao($nu_tipodeficienciasuperdotacao)
    {
        $this->nu_tipodeficienciasuperdotacao = $nu_tipodeficienciasuperdotacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getTp_registroalunocurso()
    {
        return $this->tp_registroalunocurso;
    }

    /**
     * @param int $tp_registroalunocurso
     * @return $this
     */
    public function setTp_registroalunocurso($tp_registroalunocurso)
    {
        $this->tp_registroalunocurso = $tp_registroalunocurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_semestrereferencia()
    {
        return $this->nu_semestrereferencia;
    }

    /**
     * @param int $nu_semestrereferencia
     * @return $this
     */
    public function setNu_semestrereferencia($nu_semestrereferencia)
    {
        $this->nu_semestrereferencia = $nu_semestrereferencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_codigocurso()
    {
        return $this->nu_codigocurso;
    }

    /**
     * @param int $nu_codigocurso
     * @return $this
     */
    public function setNu_codigocurso($nu_codigocurso)
    {
        $this->nu_codigocurso = $nu_codigocurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_codigopolocursodistancia()
    {
        return $this->nu_codigopolocursodistancia;
    }

    /**
     * @param int $nu_codigopolocursodistancia
     * @return $this
     */
    public function setNu_codigopolocursodistancia($nu_codigopolocursodistancia)
    {
        $this->nu_codigopolocursodistancia = $nu_codigopolocursodistancia;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_idnaies()
    {
        return $this->st_idnaies;
    }

    /**
     * @param string $st_idnaies
     * @return $this
     */
    public function setSt_idnaies($st_idnaies)
    {
        $this->st_idnaies = $st_idnaies;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_turnoaluno()
    {
        return $this->nu_turnoaluno;
    }

    /**
     * @param int $nu_turnoaluno
     * @return $this
     */
    public function setNu_turnoaluno($nu_turnoaluno)
    {
        $this->nu_turnoaluno = $nu_turnoaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_situacaovinculoalunocurso()
    {
        return $this->nu_situacaovinculoalunocurso;
    }

    /**
     * @param int $nu_situacaovinculoalunocurso
     * @return $this
     */
    public function setNu_situacaovinculoalunocurso($nu_situacaovinculoalunocurso)
    {
        $this->nu_situacaovinculoalunocurso = $nu_situacaovinculoalunocurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cursoorigem()
    {
        return $this->nu_cursoorigem;
    }

    /**
     * @param int $nu_cursoorigem
     * @return $this
     */
    public function setNu_cursoorigem($nu_cursoorigem)
    {
        $this->nu_cursoorigem = $nu_cursoorigem;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_semestreconclusao()
    {
        return $this->nu_semestreconclusao;
    }

    /**
     * @param int $nu_semestreconclusao
     * @return $this
     */
    public function setNu_semestreconclusao($nu_semestreconclusao)
    {
        $this->nu_semestreconclusao = $nu_semestreconclusao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_alunoparfor()
    {
        return $this->nu_alunoparfor;
    }

    /**
     * @param int $nu_alunoparfor
     * @return $this
     */
    public function setNu_alunoparfor($nu_alunoparfor)
    {
        $this->nu_alunoparfor = $nu_alunoparfor;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_semestreingcurso()
    {
        return $this->nu_semestreingcurso;
    }

    /**
     * @param int $nu_semestreingcurso
     * @return $this
     */
    public function setNu_semestreingcurso($nu_semestreingcurso)
    {
        $this->nu_semestreingcurso = $nu_semestreingcurso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoescolaconclusaoensinomedio()
    {
        return $this->nu_tipoescolaconclusaoensinomedio;
    }

    /**
     * @param int $nu_tipoescolaconclusaoensinomedio
     * @return $this
     */
    public function setNu_tipoescolaconclusaoensinomedio($nu_tipoescolaconclusaoensinomedio)
    {
        $this->nu_tipoescolaconclusaoensinomedio = $nu_tipoescolaconclusaoensinomedio;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaovestibular()
    {
        return $this->nu_formaingselecaovestibular;
    }

    /**
     * @param int $nu_formaingselecaovestibular
     * @return $this
     */
    public function setNu_formaingselecaovestibular($nu_formaingselecaovestibular)
    {
        $this->nu_formaingselecaovestibular = $nu_formaingselecaovestibular;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaoenem()
    {
        return $this->nu_formaingselecaoenem;
    }

    /**
     * @param int $nu_formaingselecaoenem
     * @return $this
     */
    public function setNu_formaingselecaoenem($nu_formaingselecaoenem)
    {
        $this->nu_formaingselecaoenem = $nu_formaingselecaoenem;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaoavaliacaoseriada()
    {
        return $this->nu_formaingselecaoavaliacaoseriada;
    }

    /**
     * @param int $nu_formaingselecaoavaliacaoseriada
     * @return $this
     */
    public function setNu_formaingselecaoavaliacaoseriada($nu_formaingselecaoavaliacaoseriada)
    {
        $this->nu_formaingselecaoavaliacaoseriada = $nu_formaingselecaoavaliacaoseriada;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaoselecaosimplificada()
    {
        return $this->nu_formaingselecaoselecaosimplificada;
    }

    /**
     * @param int $nu_formaingselecaoselecaosimplificada
     * @return $this
     */
    public function setNu_formaingselecaoselecaosimplificada($nu_formaingselecaoselecaosimplificada)
    {
        $this->nu_formaingselecaoselecaosimplificada = $nu_formaingselecaoselecaosimplificada;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaoegressobili()
    {
        return $this->nu_formaingselecaoegressobili;
    }

    /**
     * @param int $nu_formaingselecaoegressobili
     * @return $this
     */
    public function setNu_formaingselecaoegressobili($nu_formaingselecaoegressobili)
    {
        $this->nu_formaingselecaoegressobili = $nu_formaingselecaoegressobili;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaoegressopecg()
    {
        return $this->nu_formaingselecaoegressopecg;
    }

    /**
     * @param int $nu_formaingselecaoegressopecg
     * @return $this
     */
    public function setNu_formaingselecaoegressopecg($nu_formaingselecaoegressopecg)
    {
        $this->nu_formaingselecaoegressopecg = $nu_formaingselecaoegressopecg;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaotrasnfexofficio()
    {
        return $this->nu_formaingselecaotrasnfexofficio;
    }

    /**
     * @param int $nu_formaingselecaotrasnfexofficio
     * @return $this
     */
    public function setNu_formaingselecaotrasnfexofficio($nu_formaingselecaotrasnfexofficio)
    {
        $this->nu_formaingselecaotrasnfexofficio = $nu_formaingselecaotrasnfexofficio;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaodecisaojudicial()
    {
        return $this->nu_formaingselecaodecisaojudicial;
    }

    /**
     * @param int $nu_formaingselecaodecisaojudicial
     * @return $this
     */
    public function setNu_formaingselecaodecisaojudicial($nu_formaingselecaodecisaojudicial)
    {
        $this->nu_formaingselecaodecisaojudicial = $nu_formaingselecaodecisaojudicial;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaovagasremanescentes()
    {
        return $this->nu_formaingselecaovagasremanescentes;
    }

    /**
     * @param int $nu_formaingselecaovagasremanescentes
     * @return $this
     */
    public function setNu_formaingselecaovagasremanescentes($nu_formaingselecaovagasremanescentes)
    {
        $this->nu_formaingselecaovagasremanescentes = $nu_formaingselecaovagasremanescentes;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_formaingselecaovagasprogespeciais()
    {
        return $this->nu_formaingselecaovagasprogespeciais;
    }

    /**
     * @param int $nu_formaingselecaovagasprogespeciais
     * @return $this
     */
    public function setNu_formaingselecaovagasprogespeciais($nu_formaingselecaovagasprogespeciais)
    {
        $this->nu_formaingselecaovagasprogespeciais = $nu_formaingselecaovagasprogespeciais;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_mobilidadeacademica()
    {
        return $this->nu_mobilidadeacademica;
    }

    /**
     * @param int $nu_mobilidadeacademica
     * @return $this
     */
    public function setNu_mobilidadeacademica($nu_mobilidadeacademica)
    {
        $this->nu_mobilidadeacademica = $nu_mobilidadeacademica;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipomodalidadeacademica()
    {
        return $this->nu_tipomodalidadeacademica;
    }

    /**
     * @param int $nu_tipomodalidadeacademica
     * @return $this
     */
    public function setNu_tipomodalidadeacademica($nu_tipomodalidadeacademica)
    {
        $this->nu_tipomodalidadeacademica = $nu_tipomodalidadeacademica;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_iesdestino()
    {
        return $this->st_iesdestino;
    }

    /**
     * @param string $st_iesdestino
     * @return $this
     */
    public function setSt_iesdestino($st_iesdestino)
    {
        $this->st_iesdestino = $st_iesdestino;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipomodalidadeacademicainternacional()
    {
        return $this->nu_tipomodalidadeacademicainternacional;
    }

    /**
     * @param int $nu_tipomodalidadeacademicainternacional
     * @return $this
     */
    public function setNu_tipomodalidadeacademicainternacional($nu_tipomodalidadeacademicainternacional)
    {
        $this->nu_tipomodalidadeacademicainternacional = $nu_tipomodalidadeacademicainternacional;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_paisdestino()
    {
        return $this->st_paisdestino;
    }

    /**
     * @param string $st_paisdestino
     * @return $this
     */
    public function setSt_paisdestino($st_paisdestino)
    {
        $this->st_paisdestino = $st_paisdestino;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagas()
    {
        return $this->nu_programareservavagas;
    }

    /**
     * @param int $nu_programareservavagas
     * @return $this
     */
    public function setNu_programareservavagas($nu_programareservavagas)
    {
        $this->nu_programareservavagas = $nu_programareservavagas;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagasetnico()
    {
        return $this->nu_programareservavagasetnico;
    }

    /**
     * @param int $nu_programareservavagasetnico
     * @return $this
     */
    public function setNu_programareservavagasetnico($nu_programareservavagasetnico)
    {
        $this->nu_programareservavagasetnico = $nu_programareservavagasetnico;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagaspessoadeficiencia()
    {
        return $this->nu_programareservavagaspessoadeficiencia;
    }

    /**
     * @param int $nu_programareservavagaspessoadeficiencia
     * @return $this
     */
    public function setNu_programareservavagaspessoadeficiencia($nu_programareservavagaspessoadeficiencia)
    {
        $this->nu_programareservavagaspessoadeficiencia = $nu_programareservavagaspessoadeficiencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagasestudanteescolapublica()
    {
        return $this->nu_programareservavagasestudanteescolapublica;
    }

    /**
     * @param int $nu_programareservavagasestudanteescolapublica
     * @return $this
     */
    public function setNu_programareservavagasestudanteescolapublica($nu_programareservavagasestudanteescolapublica)
    {
        $this->nu_programareservavagasestudanteescolapublica = $nu_programareservavagasestudanteescolapublica;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagassocialrendafamiliar()
    {
        return $this->nu_programareservavagassocialrendafamiliar;
    }

    /**
     * @param int $nu_programareservavagassocialrendafamiliar
     * @return $this
     */
    public function setNu_programareservavagassocialrendafamiliar($nu_programareservavagassocialrendafamiliar)
    {
        $this->nu_programareservavagassocialrendafamiliar = $nu_programareservavagassocialrendafamiliar;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_programareservavagasoutros()
    {
        return $this->nu_programareservavagasoutros;
    }

    /**
     * @param int $nu_programareservavagasoutros
     * @return $this
     */
    public function setNu_programareservavagasoutros($nu_programareservavagasoutros)
    {
        $this->nu_programareservavagasoutros = $nu_programareservavagasoutros;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestud()
    {
        return $this->nu_finestud;
    }

    /**
     * @param int $nu_finestud
     * @return $this
     */
    public function setNu_finestud($nu_finestud)
    {
        $this->nu_finestud = $nu_finestud;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembfies()
    {
        return $this->nu_finestudreembfies;
    }

    /**
     * @param int $nu_finestudreembfies
     * @return $this
     */
    public function setNu_finestudreembfies($nu_finestudreembfies)
    {
        $this->nu_finestudreembfies = $nu_finestudreembfies;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembgovest()
    {
        return $this->nu_finestudreembgovest;
    }

    /**
     * @param int $nu_finestudreembgovest
     * @return $this
     */
    public function setNu_finestudreembgovest($nu_finestudreembgovest)
    {
        $this->nu_finestudreembgovest = $nu_finestudreembgovest;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembgovmun()
    {
        return $this->nu_finestudreembgovmun;
    }

    /**
     * @param int $nu_finestudreembgovmun
     * @return $this
     */
    public function setNu_finestudreembgovmun($nu_finestudreembgovmun)
    {
        $this->nu_finestudreembgovmun = $nu_finestudreembgovmun;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembies()
    {
        return $this->nu_finestudreembies;
    }

    /**
     * @param int $nu_finestudreembies
     * @return $this
     */
    public function setNu_finestudreembies($nu_finestudreembies)
    {
        $this->nu_finestudreembies = $nu_finestudreembies;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembentidadesexternas()
    {
        return $this->nu_finestudreembentidadesexternas;
    }

    /**
     * @param int $nu_finestudreembentidadesexternas
     * @return $this
     */
    public function setNu_finestudreembentidadesexternas($nu_finestudreembentidadesexternas)
    {
        $this->nu_finestudreembentidadesexternas = $nu_finestudreembentidadesexternas;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembprouniintegral()
    {
        return $this->nu_finestudreembprouniintegral;
    }

    /**
     * @param int $nu_finestudreembprouniintegral
     * @return $this
     */
    public function setNu_finestudreembprouniintegral($nu_finestudreembprouniintegral)
    {
        $this->nu_finestudreembprouniintegral = $nu_finestudreembprouniintegral;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembprouniparcial()
    {
        return $this->nu_finestudreembprouniparcial;
    }

    /**
     * @param int $nu_finestudreembprouniparcial
     * @return $this
     */
    public function setNu_finestudreembprouniparcial($nu_finestudreembprouniparcial)
    {
        $this->nu_finestudreembprouniparcial = $nu_finestudreembprouniparcial;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembentidadesexternas_2()
    {
        return $this->nu_finestudreembentidadesexternas_2;
    }

    /**
     * @param int $nu_finestudreembentidadesexternas_2
     * @return $this
     */
    public function setNu_finestudreembentidadesexternas_2($nu_finestudreembentidadesexternas_2)
    {
        $this->nu_finestudreembentidadesexternas_2 = $nu_finestudreembentidadesexternas_2;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembgovest_2()
    {
        return $this->nu_finestudreembgovest_2;
    }

    /**
     * @param int $nu_finestudreembgovest_2
     * @return $this
     */
    public function setNu_finestudreembgovest_2($nu_finestudreembgovest_2)
    {
        $this->nu_finestudreembgovest_2 = $nu_finestudreembgovest_2;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembies_2()
    {
        return $this->nu_finestudreembies_2;
    }

    /**
     * @param int $nu_finestudreembies_2
     * @return $this
     */
    public function setNu_finestudreembies_2($nu_finestudreembies_2)
    {
        $this->nu_finestudreembies_2 = $nu_finestudreembies_2;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_finestudreembgovmun_2()
    {
        return $this->nu_finestudreembgovmun_2;
    }

    /**
     * @param int $nu_finestudreembgovmun_2
     * @return $this
     */
    public function setNu_finestudreembgovmun_2($nu_finestudreembgovmun_2)
    {
        $this->nu_finestudreembgovmun_2 = $nu_finestudreembgovmun_2;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_apoiosocial()
    {
        return $this->nu_apoiosocial;
    }

    /**
     * @param int $nu_apoiosocial
     * @return $this
     */
    public function setNu_apoiosocial($nu_apoiosocial)
    {
        $this->nu_apoiosocial = $nu_apoiosocial;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialalimentacao()
    {
        return $this->nu_tipoapoiosocialalimentacao;
    }

    /**
     * @param int $nu_tipoapoiosocialalimentacao
     * @return $this
     */
    public function setNu_tipoapoiosocialalimentacao($nu_tipoapoiosocialalimentacao)
    {
        $this->nu_tipoapoiosocialalimentacao = $nu_tipoapoiosocialalimentacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialmoradia()
    {
        return $this->nu_tipoapoiosocialmoradia;
    }

    /**
     * @param int $nu_tipoapoiosocialmoradia
     * @return $this
     */
    public function setNu_tipoapoiosocialmoradia($nu_tipoapoiosocialmoradia)
    {
        $this->nu_tipoapoiosocialmoradia = $nu_tipoapoiosocialmoradia;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialtransporte()
    {
        return $this->nu_tipoapoiosocialtransporte;
    }

    /**
     * @param int $nu_tipoapoiosocialtransporte
     * @return $this
     */
    public function setNu_tipoapoiosocialtransporte($nu_tipoapoiosocialtransporte)
    {
        $this->nu_tipoapoiosocialtransporte = $nu_tipoapoiosocialtransporte;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialmaterialdidatico()
    {
        return $this->nu_tipoapoiosocialmaterialdidatico;
    }

    /**
     * @param int $nu_tipoapoiosocialmaterialdidatico
     * @return $this
     */
    public function setNu_tipoapoiosocialmaterialdidatico($nu_tipoapoiosocialmaterialdidatico)
    {
        $this->nu_tipoapoiosocialmaterialdidatico = $nu_tipoapoiosocialmaterialdidatico;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialbolsatrabalho()
    {
        return $this->nu_tipoapoiosocialbolsatrabalho;
    }

    /**
     * @param int $nu_tipoapoiosocialbolsatrabalho
     * @return $this
     */
    public function setNu_tipoapoiosocialbolsatrabalho($nu_tipoapoiosocialbolsatrabalho)
    {
        $this->nu_tipoapoiosocialbolsatrabalho = $nu_tipoapoiosocialbolsatrabalho;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_tipoapoiosocialbolsapermanencia()
    {
        return $this->nu_tipoapoiosocialbolsapermanencia;
    }

    /**
     * @param int $nu_tipoapoiosocialbolsapermanencia
     * @return $this
     */
    public function setNu_tipoapoiosocialbolsapermanencia($nu_tipoapoiosocialbolsapermanencia)
    {
        $this->nu_tipoapoiosocialbolsapermanencia = $nu_tipoapoiosocialbolsapermanencia;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_atvextcurr()
    {
        return $this->nu_atvextcurr;
    }

    /**
     * @param int $nu_atvextcurr
     * @return $this
     */
    public function setNu_atvextcurr($nu_atvextcurr)
    {
        $this->nu_atvextcurr = $nu_atvextcurr;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_atvextcurrpesquisa()
    {
        return $this->nu_atvextcurrpesquisa;
    }

    /**
     * @param int $nu_atvextcurrpesquisa
     * @return $this
     */
    public function setNu_atvextcurrpesquisa($nu_atvextcurrpesquisa)
    {
        $this->nu_atvextcurrpesquisa = $nu_atvextcurrpesquisa;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_bolsaremuneracaoatvextcurrpesquisa()
    {
        return $this->nu_bolsaremuneracaoatvextcurrpesquisa;
    }

    /**
     * @param int $nu_bolsaremuneracaoatvextcurrpesquisa
     * @return $this
     */
    public function setNu_bolsaremuneracaoatvextcurrpesquisa($nu_bolsaremuneracaoatvextcurrpesquisa)
    {
        $this->nu_bolsaremuneracaoatvextcurrpesquisa = $nu_bolsaremuneracaoatvextcurrpesquisa;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_atvextcurrextensao()
    {
        return $this->nu_atvextcurrextensao;
    }

    /**
     * @param int $nu_atvextcurrextensao
     * @return $this
     */
    public function setNu_atvextcurrextensao($nu_atvextcurrextensao)
    {
        $this->nu_atvextcurrextensao = $nu_atvextcurrextensao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_bolsaremuneracaoatvextcurrextensao()
    {
        return $this->nu_bolsaremuneracaoatvextcurrextensao;
    }

    /**
     * @param int $nu_bolsaremuneracaoatvextcurrextensao
     * @return $this
     */
    public function setNu_bolsaremuneracaoatvextcurrextensao($nu_bolsaremuneracaoatvextcurrextensao)
    {
        $this->nu_bolsaremuneracaoatvextcurrextensao = $nu_bolsaremuneracaoatvextcurrextensao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_atvextcurrmonitoria()
    {
        return $this->nu_atvextcurrmonitoria;
    }

    /**
     * @param int $nu_atvextcurrmonitoria
     * @return $this
     */
    public function setNu_atvextcurrmonitoria($nu_atvextcurrmonitoria)
    {
        $this->nu_atvextcurrmonitoria = $nu_atvextcurrmonitoria;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_bolsaremuneracaoatvextcurrmonitoria()
    {
        return $this->nu_bolsaremuneracaoatvextcurrmonitoria;
    }

    /**
     * @param int $nu_bolsaremuneracaoatvextcurrmonitoria
     * @return $this
     */
    public function setNu_bolsaremuneracaoatvextcurrmonitoria($nu_bolsaremuneracaoatvextcurrmonitoria)
    {
        $this->nu_bolsaremuneracaoatvextcurrmonitoria = $nu_bolsaremuneracaoatvextcurrmonitoria;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_atvextcurrestnaoobg()
    {
        return $this->nu_atvextcurrestnaoobg;
    }

    /**
     * @param int $nu_atvextcurrestnaoobg
     * @return $this
     */
    public function setNu_atvextcurrestnaoobg($nu_atvextcurrestnaoobg)
    {
        $this->nu_atvextcurrestnaoobg = $nu_atvextcurrestnaoobg;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_bolsaremuneracaoatvextcurrestnaoobg()
    {
        return $this->nu_bolsaremuneracaoatvextcurrestnaoobg;
    }

    /**
     * @param int $nu_bolsaremuneracaoatvextcurrestnaoobg
     * @return $this
     */
    public function setNu_bolsaremuneracaoatvextcurrestnaoobg($nu_bolsaremuneracaoatvextcurrestnaoobg)
    {
        $this->nu_bolsaremuneracaoatvextcurrestnaoobg = $nu_bolsaremuneracaoatvextcurrestnaoobg;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahorariototalporaluno()
    {
        return $this->nu_cargahorariototalporaluno;
    }

    /**
     * @param int $nu_cargahorariototalporaluno
     * @return $this
     */
    public function setNu_cargahorariototalporaluno($nu_cargahorariototalporaluno)
    {
        $this->nu_cargahorariototalporaluno = $nu_cargahorariototalporaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_cargahorariointegralizadapeloaluno()
    {
        return $this->nu_cargahorariointegralizadapeloaluno;
    }

    /**
     * @param int $nu_cargahorariointegralizadapeloaluno
     * @return $this
     */
    public function setNu_cargahorariointegralizadapeloaluno($nu_cargahorariointegralizadapeloaluno)
    {
        $this->nu_cargahorariointegralizadapeloaluno = $nu_cargahorariointegralizadapeloaluno;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_anocenso()
    {
        return $this->nu_anocenso;
    }

    /**
     * @param int $nu_anocenso
     * @return $this
     */
    public function setNu_anocenso($nu_anocenso)
    {
        $this->nu_anocenso = $nu_anocenso;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_anomatricula()
    {
        return $this->nu_anomatricula;
    }

    /**
     * @param int $nu_anomatricula
     * @return $this
     */
    public function setNu_anomatricula($nu_anomatricula)
    {
        $this->nu_anomatricula = $nu_anomatricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return $this
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

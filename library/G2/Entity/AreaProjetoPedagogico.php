<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_areaprojetopedagogico")
 * @Entity
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class AreaProjetoPedagogico
{

    /**
     * @Id
     * @ManyToOne(targetEntity="AreaConhecimento")
     * @JoinColumn(name="id_areaconhecimento", referencedColumnName="id_areaconhecimento")
     */
    private $id_areaconhecimento;

    /**
     * @Id
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

}

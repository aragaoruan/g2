<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_perfisentidade")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPerfisEntidade
{

    /**
     *
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_perfil;

    /**
     *
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=false, length=100) 
     */
    private $st_nomeperfil;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", type="boolean", nullable=false) 
     */
    private $bl_padrao;

    /**
     *
     * @var integer $id_perfilpedagogico
     * @Column(name="id_perfilpedagogico", type="integer", nullable=true) 
     */
    private $id_perfilpedagogico;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false) 
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255) 
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false) 
     */
    private $id_sistema;

    /**
     *
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=false, length=255) 
     */
    private $st_sistema;

    /**
     *
     * @var string $st_perfilpedagogico
     * @Column(name="st_perfilpedagogico", type="string", nullable=true, length=255) 
     */
    private $st_perfilpedagogico;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=false) 
     */
    private $id_entidadecadastro;

    public function getId_perfil ()
    {
        return $this->id_perfil;
    }

    public function setId_perfil ($id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    public function getSt_nomeperfil ()
    {
        return $this->st_nomeperfil;
    }

    public function setSt_nomeperfil ($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getId_perfilpedagogico ()
    {
        return $this->id_perfilpedagogico;
    }

    public function setId_perfilpedagogico ($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_sistema ()
    {
        return $this->id_sistema;
    }

    public function setId_sistema ($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    public function getSt_sistema ()
    {
        return $this->st_sistema;
    }

    public function setSt_sistema ($st_sistema)
    {
        $this->st_sistema = $st_sistema;
        return $this;
    }

    public function getSt_perfilpedagogico ()
    {
        return $this->st_perfilpedagogico;
    }

    public function setSt_perfilpedagogico ($st_perfilpedagogico)
    {
        $this->st_perfilpedagogico = $st_perfilpedagogico;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

}
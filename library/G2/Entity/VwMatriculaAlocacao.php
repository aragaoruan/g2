<?php

namespace G2\Entity;

/**
 * @Table (name="vw_matriculaalocacao")
 * @Entity(repositoryClass="\G2\Repository\VwMatriculaAlocacao")
 * @EntityView
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class VwMatriculaAlocacao
{

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer", nullable=false, length=4)
     */
    private $id_entidadeatendimento;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     */
    private $id_alocacao;
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     */
    private $id_saladeaula;
    /**
     * @var datetime $dt_ultimoacesso
     * @Column(name="dt_ultimoacesso", type="datetime", nullable=true, length=8)
     */
    private $dt_ultimoacesso;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="datetime", nullable=true)
     */
    private $dt_inicio;
    /**
     * @var date $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var date $dt_terminomatricula
     * @Column(name="dt_terminomatricula", type="datetime", nullable=true)
     */
    private $dt_terminomatricula;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=300)
     */
    private $st_disciplina;

    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", type="integer", nullable=false, length=4)
     */
    private $id_situacaoagendamento;

    /**
     * @return date dt_encerramento
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidadeatendimento
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_alocacao
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return datetime dt_ultimoacesso
     */
    public function getDt_ultimoacesso()
    {
        return $this->dt_ultimoacesso;
    }

    /**
     * @param dt_ultimoacesso
     */
    public function setDt_ultimoacesso($dt_ultimoacesso)
    {
        $this->dt_ultimoacesso = $dt_ultimoacesso;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }


    /**
     * @return string dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return string dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return string dt_terminomatricula
     */
    public function getDt_terminomatricula()
    {
        return $this->dt_terminomatricula;
    }

    /**
     * @param dt_terminomatricula
     */
    public function setDt_terminomatricula($dt_terminomatricula)
    {
        $this->terminomatricula = $dt_terminomatricula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     * @return $this
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param int $id_situacaoagendamento
     * @return $this
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_avaliacaoaplicacao")
 * @Entity
 * @EntityView
 * @authora Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAvaliacaoAplicacao
{

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false)
     * @Id
     */
    private $id_avaliacaoaplicacao;
    /**
     * @var integer $nu_maxaplicacao
     * @Column(name="nu_maxaplicacao", type="integer", nullable=true)
     */
    private $nu_maxaplicacao;
    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true)
     */
    private $nu_numero;
    /**
     * @var integer $nu_vagasrestantes
     * @Column(name="nu_vagasrestantes", type="integer", nullable=true)
     */
    private $nu_vagasrestantes;
    /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer", nullable=true)
     */
    private $id_horarioaula;
    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true)
     */
    private $id_aplicadorprova;
    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=true)
     */
    private $st_aplicadorprova;
    /**
     * @var integer $id_endereco
     * @Column(name="id_endereco", type="integer", nullable=true)
     */
    private $id_endereco;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;
    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", type="date", nullable=true)
     */
    private $dt_aplicacao;
    /**
     * @var boolean $bl_unica
     * @Column(name="bl_unica", type="boolean", nullable=true)
     */
    private $bl_unica;
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;
    /**
     * @var string $dt_alteracaolimite
     * @Column(name="dt_alteracaolimite", type="date", nullable=true)
     */
    private $dt_alteracaolimite;
    /**
     * @var string $dt_antecedenciaminima
     * @Column(name="dt_antecedenciaminima", type="date", nullable=true)
     */
    private $dt_antecedenciaminima;
    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true)
     */
    private $st_endereco;
    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true)
     */
    private $st_complemento;
    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true)
     */
    private $sg_uf;
    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true)
     */
    private $st_cidade;
    /**
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=true)
     */
    private $id_municipio;
    /**
     * @var datetime $hr_inicioprova
     * @Column(name="hr_inicioprova", type="datetime2", nullable=true)
     */
    private $hr_inicioprova;
    /**
     * @var datetime $hr_fimprova
     * @Column(name="hr_fimprova", type="datetime2", nullable=true)
     */
    private $hr_fimprova;
    /**
     * @var string $st_horarioaula
     * @Column(name="st_horarioaula", type="string", nullable=true)
     */
    private $st_horarioaula;

    /**
     * @var integer $bl_dataativa
     * @Column(name="bl_dataativa", type="integer", nullable=true)
     */
    private $bl_dataativa;
    /**
     * @Column(name="bl_limitealunos",type="boolean",nullable=false)
     * @var boolean $bl_limitealunos
     */
    private $bl_limitealunos;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true)
     */
    private $st_bairro;

    /**
     * @var integer $nu_anoaplicacao
     * @Column(name="nu_anoaplicacao", type="integer", nullable=true)
     */
    private $nu_anoaplicacao;

    /**
     * @var integer $nu_mesaplicacao
     * @Column(name="nu_mesaplicacao", type="integer", nullable=true)
     */
    private $nu_mesaplicacao;

    /**
     * @var integer $nu_diaaplicacao
     * @Column(name="nu_diaaplicacao", type="integer", nullable=true)
     */
    private $nu_diaaplicacao;

    /**
     * @var integer $nu_relatoriosagendamento
     * @Column(name="nu_relatoriosagendamento", type="integer")
     */
    private $nu_relatoriosagendamento;


    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    public function getNu_maxaplicacao()
    {
        return $this->nu_maxaplicacao;
    }

    public function getNu_numero()
    {
        return $this->nu_maxaplicacao;
    }

    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    public function getId_endereco()
    {
        return $this->id_endereco;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getDt_aplicacao()
    {
        return $this->dt_aplicacao;
    }

    public function getBl_unica()
    {
        return $this->bl_unica;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getDt_alteracaolimite()
    {
        return $this->dt_alteracaolimite;
    }

    public function getDt_antecedenciaminima()
    {
        return $this->dt_antecedenciaminima;
    }

    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function getId_municipio()
    {
        return $this->id_municipio;
    }

    public function getHr_inicioprova()
    {
        return $this->hr_inicioprova;
    }

    public function getHr_fimprova()
    {
        return $this->hr_fimprova;
    }

    public function getNu_relatoriosagendamento()
    {
        return $this->nu_relatoriosagendamento;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    public function setNu_maxaplicacao($nu_maxaplicacao)
    {
        $this->nu_maxaplicacao = $nu_maxaplicacao;
    }

    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
    }

    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
    }

    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    public function setId_endereco($id_endereco)
    {
        $this->id_endereco = $id_endereco;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setDt_aplicacao(date $dt_aplicacao)
    {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    public function setBl_unica($bl_unica)
    {
        $this->bl_unica = $bl_unica;
    }

    public function setDt_cadastro(datetime2 $dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function setDt_alteracaolimite($dt_alteracaolimite)
    {
        $this->dt_alteracaolimite = $dt_alteracaolimite;
    }

    public function setDt_antecedenciaminima($dt_antecedenciaminima)
    {
        $this->dt_antecedenciaminima = $dt_antecedenciaminima;
    }

    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
    }

    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    public function setId_municipio($id_municipio)
    {
        $this->id_municipio = $id_municipio;
    }

    public function setHr_inicioprova($hr_inicioprova)
    {
        $this->hr_inicioprova = $hr_inicioprova;
    }

    public function setHr_fimprova($hr_fimprova)
    {
        $this->hr_fimprova = $hr_fimprova;
    }


    public function getSt_horarioaula()
    {
        return $this->st_horarioaula;
    }

    public function setSt_horarioaula($st_horarioaula)
    {
        $this->st_horarioaula = $st_horarioaula;
    }

    public function getBl_dataativa()
    {
        return $this->bl_dataativa;
    }

    public function setBl_dataativa($bl_dataativa)
    {
        $this->bl_dataativa = $bl_dataativa;
    }

    public function getBl_limitealunos()
    {
        return $this->bl_limitealunos;
    }

    public function setBl_limitealunos($bl_limitealunos)
    {
        $this->bl_limitealunos = $bl_limitealunos;
    }

    /**
     * @param string $st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @return string
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
    }

    /**
     * @return int
     */
    public function getNu_vagasrestantes()
    {
        return $this->nu_vagasrestantes;
    }

    /**
     * @param int $nu_vagasrestantes
     */
    public function setNu_vagasrestantes($nu_vagasrestantes)
    {
        $this->nu_vagasrestantes = $nu_vagasrestantes;
    }

    /**
     * @return int
     */
    public function getNu_anoaplicacao()
    {
        return $this->nu_anoaplicacao;
    }

    /**
     * @param int $nu_anoaplicacao
     * @return $this
     */
    public function setNu_anoaplicacao($nu_anoaplicacao)
    {
        $this->nu_anoaplicacao = $nu_anoaplicacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_mesaplicacao()
    {
        return $this->nu_mesaplicacao;
    }

    /**
     * @param int $nu_mesaplicacao
     * @return $this
     */
    public function setNu_mesaplicacao($nu_mesaplicacao)
    {
        $this->nu_mesaplicacao = $nu_mesaplicacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_diaaplicacao()
    {
        return $this->nu_diaaplicacao;
    }

    /**
     * @param int $nu_diaaplicacao
     * @return $this
     */
    public function setNu_diaaplicacao($nu_diaaplicacao)
    {
        $this->nu_diaaplicacao = $nu_diaaplicacao;
        return $this;
    }

    public function setNu_relatoriosagendamento($nu_relatoriosagendamento)
    {
        $this->nu_relatoriosagendamento = $nu_relatoriosagendamento;
        return $this;
    }

}

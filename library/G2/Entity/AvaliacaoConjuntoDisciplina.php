<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_avaliacaoconjuntodisciplina")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class AvaliacaoConjuntoDisciplina {


    /**
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_avaliacaoconjunto;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_tipodisciplina;


    public function setId_Avaliacaoconjunto($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    public function getId_Avaliacaoconjunto()
    {
        return $this->id_avaliacaoconjunto;
    }

    public function setId_Tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }


    public function getId_Tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_servidoremail")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ServidorEmail
{

    /**
     *
     * @var integer $id_servidoremail
     * @Column(name="id_servidoremail", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_servidoremail;

    /**
     *
     * @var string $st_servidoremail
     * @Column(name="st_servidoremail", type="string", nullable=false, length=20) 
     */
    private $st_servidoremail;

    public function getId_servidoremail ()
    {
        return $this->id_servidoremail;
    }

    public function setId_servidoremail ($id_servidoremail)
    {
        $this->id_servidoremail = $id_servidoremail;
        return $this;
    }

    public function getSt_servidoremail ()
    {
        return $this->st_servidoremail;
    }

    public function setSt_servidoremail ($st_servidoremail)
    {
        $this->st_servidoremail = $st_servidoremail;
        return $this;
    }

}
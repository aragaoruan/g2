<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @SWG\Definition(@SWG\Xml(name="VwGradeNota"))
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gradenota")
 * @Entity(repositoryClass="\G2\Repository\VwGradeNota")
 * @EntityView
 */
class VwGradeNota extends G2Entity{

    /**
     * @SWG\Property(format="integer")
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;

    /**
     * @SWG\Property(format="date")
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;
    /**
     * @SWG\Property(format="date")
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime", nullable=true, length=3)
     */
    private $dt_encerramento;
    /**
     * @SWG\Property(format="date")
     * @var date $dt_encerramentoextensao
     * @Column(name="dt_encerramentoextensao", type="datetime", nullable=true, length=3)
     */
    private $dt_encerramentoextensao;
    /**
     * @SWG\Property(format="date")
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=true, length=3)
     */
    private $dt_inicio;
    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tiponotatcc
     * @Column(name="id_tiponotatcc", type="integer", nullable=true, length=4)
     */
    private $id_tiponotatcc;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tiponotaead
     * @Column(name="id_tiponotaead", type="integer", nullable=true, length=4)
     */
    private $id_tiponotaead;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tiponotaatividade
     * @Column(name="id_tiponotaatividade", type="integer", nullable=true, length=4)
     */
    private $id_tiponotaatividade;

    /**
     * @SWG\Property(format="integer")
     * @var integer $st_notafinal
     * @Column(name="st_notafinal", type="integer", nullable=true, length=4)
     */
    private $st_notafinal;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=true, length=4)
     */
    private $id_modulo;

    /**
     * @SWG\Property(format="integer")
     * @var integer $bl_mostratcc
     * @Column(name="bl_mostratcc", type="integer", nullable=true, length=4)
     */
    private $bl_mostratcc;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_notatotal
     * @Column(name="nu_notatotal", type="integer", nullable=true, length=4)
     */
    private $nu_notatotal;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_percentualaprovacao
     * @Column(name="nu_percentualaprovacao", type="integer", nullable=true, length=4)
     */
    private $nu_percentualaprovacao;

    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true, length=4)
     */
    private $id_categoriasala;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_usuarioaproveitamento
     * @Column(name="id_usuarioaproveitamento", type="integer", nullable=true, length=4)
     */
    private $id_usuarioaproveitamento;

    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_encerramentosala
     * @Column(name="id_encerramentosala", type="integer", nullable=true, length=4)
     */
    private $id_encerramentosala;

    /**
     * @SWG\Property(format="integer")
     * @var integer $bl_status
     * @Column(name="bl_status", type="integer", nullable=false, length=4)
     */
    private $bl_status;

    /**
     * @SWG\Property(format="integer")
     * @var integer $bl_complementar
     * @Column(name="bl_complementar", type="integer", nullable=false, length=4)
     */
    private $bl_complementar;

    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tiponotarecuperacao
     * @Column(name="id_tiponotarecuperacao", type="integer", nullable=true, length=4)
     */
    private $id_tiponotarecuperacao;

    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_aproveitamento
     * @Column(name="id_aproveitamento", type="integer", nullable=true, length=4)
     */
    private $id_aproveitamento;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_notafinal
     * @Column(name="nu_notafinal", type="integer", nullable=true, length=4)
     */
    private $nu_notafinal;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=false, length=4)
     */
    private $nu_cargahoraria;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tiponotafinal
     * @Column(name="id_tiponotafinal", type="integer", nullable=true, length=4)
     */
    private $id_tiponotafinal;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_avaliacaoatividade
     * @Column(name="id_avaliacaoatividade", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoatividade;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @Id
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @SWG\Property(format="integer")
     * @id
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @SWG\Property(format="date-time")
     * @var datetime $dt_cadastroaproveitamento
     * @Column(name="dt_cadastroaproveitamento", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastroaproveitamento;

    /**
     * @SWG\Property(format="float")
     * @var float $nu_notafaltante
     * @Column(name="nu_notafaltante", type="float", nullable=true, length=8)
     */
    private $nu_notafaltante;

    /**
     * @SWG\Property(format="decimal")
     * @var decimal $st_notatcc
     * @Column(name="st_notatcc", type="decimal", nullable=true, length=5)
     */
    private $st_notatcc;

    /**
     * @SWG\Property(format="decimal")
     * @var decimal $st_notaead
     * @Column(name="st_notaead", type="decimal", nullable=true, length=5)
     */
    private $st_notaead;

    /**
     * @SWG\Property(format="decimal")
     * @var decimal $st_notaatividade
     * @Column(name="st_notaatividade", type="decimal", nullable=true, length=5)
     */
    private $st_notaatividade;

    /**
     * @SWG\Property(format="decimal")
     * @var decimal $st_notarecuperacao
     * @Column(name="st_notarecuperacao", type="decimal", nullable=true, length=5)
     */
    private $st_notarecuperacao;

    /**
     * @SWG\Property(format="decimal")
     * @var decimal $nu_notamaxima
     * @Column(name="nu_notamaxima", type="decimal", nullable=true, length=9)
     */
    private $nu_notamaxima;

    /**
     * @SWG\Property(format="string")
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @SWG\Property(format="string")
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;
    /**
     * @SWG\Property(format="string")
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @SWG\Property(format="string")
     * @var string $st_tituloexibicaodisciplina
     * @Column(name="st_tituloexibicaodisciplina", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicaodisciplina;
    /**
     * @SWG\Property(format="string")
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @SWG\Property(format="string")
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @SWG\Property(format="string")
     * @var string $st_avaliacaofinal
     * @Column(name="st_avaliacaofinal", type="string", nullable=true, length=100)
     */
    private $st_avaliacaofinal;
    /**
     * @SWG\Property(format="string")
     * @var string $st_avaliacaorecuperacao
     * @Column(name="st_avaliacaorecuperacao", type="string", nullable=true, length=100)
     */
    private $st_avaliacaorecuperacao;
    /**
     * @SWG\Property(format="string")
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=true, length=255)
     */
    private $st_modulo;
    /**
     * @SWG\Property(format="string")
     * @var string $st_nomeusuarioaproveitamento
     * @Column(name="st_nomeusuarioaproveitamento", type="string", nullable=true, length=300)
     */
    private $st_nomeusuarioaproveitamento;
    /**
     * @SWG\Property(format="string")
     * @var string $st_disciplinaoriginal
     * @Column(name="st_disciplinaoriginal", type="string", nullable=true, length=255)
     */
    private $st_disciplinaoriginal;
    /**
     * @SWG\Property(format="string")
     * @var string $st_status
     * @Column(name="st_status", type="string", nullable=false, length=255)
     */
    private $st_status;
    /**
     * @SWG\Property(format="string")
     * @var string $st_statusdisciplina
     * @Column(name="st_statusdisciplina", type="string", nullable=false, length=12)
     */
    private $st_statusdisciplina;
    /**
     * @SWG\Property(format="string")
     * @var string $st_avaliacaoatividade
     * @Column(name="st_avaliacaoatividade", type="string", nullable=true, length=100)
     */
    private $st_avaliacaoatividade;
    /**
     * @SWG\Property(format="string")
     * @var string $st_avaliacaoead
     * @Column(name="st_avaliacaoead", type="string", nullable=true, length=100)
     */
    private $st_avaliacaoead;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     */
    private $id_alocacao;

    /**
     * @SWG\Property(format="date")
     * @var date $dt_defesatcc
     * @Column(name="dt_defesatcc", type="datetime", nullable=true, length=3)
     */
    private $dt_defesatcc;

    /**
     * @SWG\Property(format="boolean")
     * @var boolean $bl_coeficienterendimento
     * @Column(name="bl_coeficienterendimento", type="boolean", nullable=true, length=1)
     */
    private $bl_coeficienterendimento;

    /**
     * @return date dt_abertura
     */
    public function getDt_abertura() {
        return $this->dt_abertura;
    }

    /**
     * @param dt_abertura
     */
    public function setDt_abertura($dt_abertura) {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return date dt_encerramento
     */
    public function getDt_encerramento() {
        return $this->dt_encerramento;
    }

    /**
     * @param dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento) {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return date dt_encerramentoextensao
     */
    public function getDt_encerramentoextensao() {
        return $this->dt_encerramentoextensao;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_tiponotatcc
     */
    public function getId_tiponotatcc() {
        return $this->id_tiponotatcc;
    }

    /**
     * @param id_tiponotatcc
     */
    public function setId_tiponotatcc($id_tiponotatcc) {
        $this->id_tiponotatcc = $id_tiponotatcc;
        return $this;
    }

    /**
     * @return integer id_tiponotaead
     */
    public function getId_tiponotaead() {
        return $this->id_tiponotaead;
    }

    /**
     * @param id_tiponotaead
     */
    public function setId_tiponotaead($id_tiponotaead) {
        $this->id_tiponotaead = $id_tiponotaead;
        return $this;
    }

    /**
     * @return integer id_tiponotaatividade
     */
    public function getId_tiponotaatividade() {
        return $this->id_tiponotaatividade;
    }

    /**
     * @param id_tiponotaatividade
     */
    public function setId_tiponotaatividade($id_tiponotaatividade) {
        $this->id_tiponotaatividade = $id_tiponotaatividade;
        return $this;
    }

    /**
     * @return integer st_notafinal
     */
    public function getSt_notafinal() {
        return $this->st_notafinal;
    }

    /**
     * @param st_notafinal
     */
    public function setSt_notafinal($st_notafinal) {
        $this->st_notafinal = $st_notafinal;
        return $this;
    }

    /**
     * @return integer id_modulo
     */
    public function getId_modulo() {
        return $this->id_modulo;
    }

    /**
     * @param id_modulo
     */
    public function setId_modulo($id_modulo) {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return integer bl_mostratcc
     */
    public function getBl_mostratcc() {
        return $this->bl_mostratcc;
    }

    /**
     * @param bl_mostratcc
     */
    public function setBl_mostratcc($bl_mostratcc) {
        $this->bl_mostratcc = $bl_mostratcc;
        return $this;
    }

    /**
     * @return integer nu_notatotal
     */
    public function getNu_notatotal() {
        return $this->nu_notatotal;
    }

    /**
     * @param nu_notatotal
     */
    public function setNu_notatotal($nu_notatotal) {
        $this->nu_notatotal = $nu_notatotal;
        return $this;
    }

    /**
     * @return integer nu_percentualaprovacao
     */
    public function getNu_percentualaprovacao() {
        return $this->nu_percentualaprovacao;
    }

    /**
     * @param nu_notatotal
     */
    public function setNssu_percentualaprovacao($nu_percentualaprovacao) {
        $this->nu_percentualaprovacao = $nu_percentualaprovacao;
        return $this;
    }

    /**
     * @return integer id_categoriasala
     */
    public function getId_categoriasala() {
        return $this->id_categoriasala;
    }

    /**
     * @param id_categoriasala
     */
    public function setId_categoriasala($id_categoriasala) {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    /**
     * @return integer id_usuarioaproveitamento
     */
    public function getId_usuarioaproveitamento() {
        return $this->id_usuarioaproveitamento;
    }

    /**
     * @param id_usuarioaproveitamento
     */
    public function setId_usuarioaproveitamento($id_usuarioaproveitamento) {
        $this->id_usuarioaproveitamento = $id_usuarioaproveitamento;
        return $this;
    }

    /**
     * @return integer id_encerramentosala
     */
    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    /**
     * @param id_encerramentosala
     */
    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
        return $this;
    }

    /**
     * @return integer bl_status
     */
    public function getBl_status() {
        return $this->bl_status;
    }

    /**
     * @param bl_status
     */
    public function setBl_status($bl_status) {
        $this->bl_status = $bl_status;
        return $this;
    }

    /**
     * @return integer bl_complementar
     */
    public function getBl_complementar() {
        return $this->bl_complementar;
    }

    /**
     * @param bl_complementar
     */
    public function setBl_complementar($bl_complementar) {
        $this->bl_complementar = $bl_complementar;
        return $this;
    }

    /**
     * @return integer id_tiponotarecuperacao
     */
    public function getId_tiponotarecuperacao() {
        return $this->id_tiponotarecuperacao;
    }

    /**
     * @param id_tiponotarecuperacao
     */
    public function setId_tiponotarecuperacao($id_tiponotarecuperacao) {
        $this->id_tiponotarecuperacao = $id_tiponotarecuperacao;
        return $this;
    }

    /**
     * @return integer id_aproveitamento
     */
    public function getId_aproveitamento() {
        return $this->id_aproveitamento;
    }

    /**
     * @param id_aproveitamento
     */
    public function setId_aproveitamento($id_aproveitamento) {
        $this->id_aproveitamento = $id_aproveitamento;
        return $this;
    }

    /**
     * @return integer id_avaliacaoconjuntoreferencia
     */
    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    /**
     * @param id_avaliacaoconjuntoreferencia
     */
    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    /**
     * @return integer nu_notafinal
     */
    public function getNu_notafinal() {
        return $this->nu_notafinal;
    }

    /**
     * @param nu_notafinal
     */
    public function setNu_notafinal($nu_notafinal) {
        $this->nu_notafinal = $nu_notafinal;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao() {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer nu_cargahoraria
     */
    public function getNu_cargahoraria() {
        return $this->nu_cargahoraria;
    }

    /**
     * @param nu_cargahoraria
     */
    public function setNu_cargahoraria($nu_cargahoraria) {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return integer id_tiponotafinal
     */
    public function getId_tiponotafinal() {
        return $this->id_tiponotafinal;
    }

    /**
     * @param id_tiponotafinal
     */
    public function setId_tiponotafinal($id_tiponotafinal) {
        $this->id_tiponotafinal = $id_tiponotafinal;
        return $this;
    }

    /**
     * @return integer id_avaliacaoatividade
     */
    public function getId_avaliacaoatividade() {
        return $this->id_avaliacaoatividade;
    }

    /**
     * @param id_avaliacaoatividade
     */
    public function setId_avaliacaoatividade($id_avaliacaoatividade) {
        $this->id_avaliacaoatividade = $id_avaliacaoatividade;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_tipodisciplina
     */
    public function getId_tipodisciplina() {
        return $this->id_tipodisciplina;
    }

    /**
     * @param id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina) {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return datetime dt_cadastroaproveitamento
     */
    public function getDt_cadastroaproveitamento() {
        return $this->dt_cadastroaproveitamento;
    }

    /**
     * @param dt_cadastroaproveitamento
     */
    public function setDt_cadastroaproveitamento($dt_cadastroaproveitamento) {
        $this->dt_cadastroaproveitamento = $dt_cadastroaproveitamento;
        return $this;
    }

    /**
     * @return float nu_notafaltante
     */
    public function getNu_notafaltante() {
        return $this->nu_notafaltante;
    }

    /**
     * @param nu_notafaltante
     */
    public function setNu_notafaltante($nu_notafaltante) {
        $this->nu_notafaltante = $nu_notafaltante;
        return $this;
    }

    /**
     * @return decimal st_notatcc
     */
    public function getSt_notatcc() {
        return $this->st_notatcc;
    }

    /**
     * @param st_notatcc
     */
    public function setSt_notatcc($st_notatcc) {
        $this->st_notatcc = $st_notatcc;
        return $this;
    }

    /**
     * @return decimal st_notaead
     */
    public function getSt_notaead() {
        return $this->st_notaead;
    }

    /**
     * @param st_notaead
     */
    public function setSt_notaead($st_notaead) {
        $this->st_notaead = $st_notaead;
        return $this;
    }

    /**
     * @return decimal st_notaatividade
     */
    public function getSt_notaatividade() {
        return $this->st_notaatividade;
    }

    /**
     * @param st_notaatividade
     */
    public function setSt_notaatividade($st_notaatividade) {
        $this->st_notaatividade = $st_notaatividade;
        return $this;
    }

    /**
     * @return decimal st_notarecuperacao
     */
    public function getSt_notarecuperacao() {
        return $this->st_notarecuperacao;
    }

    /**
     * @param st_notarecuperacao
     */
    public function setSt_notarecuperacao($st_notarecuperacao) {
        $this->st_notarecuperacao = $st_notarecuperacao;
        return $this;
    }

    /**
     * @return decimal nu_notamaxima
     */
    public function getNu_notamaxima() {
        return $this->nu_notamaxima;
    }

    /**
     * @param nu_notamaxima
     */
    public function setNu_notamaxima($nu_notamaxima) {
        $this->nu_notamaxima = $nu_notamaxima;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_tituloexibicaodisciplina
     */
    public function getSt_tituloexibicaodisciplina() {
        return $this->st_tituloexibicaodisciplina;
    }

    /**
     * @param st_tituloexibicaodisciplina
     */
    public function setSt_tituloexibicaodisciplina($st_tituloexibicaodisciplina) {
        $this->st_tituloexibicaodisciplina = $st_tituloexibicaodisciplina;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao() {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_avaliacaofinal
     */
    public function getSt_avaliacaofinal() {
        return $this->st_avaliacaofinal;
    }

    /**
     * @param st_avaliacaofinal
     */
    public function setSt_avaliacaofinal($st_avaliacaofinal) {
        $this->st_avaliacaofinal = $st_avaliacaofinal;
        return $this;
    }

    /**
     * @return string st_avaliacaorecuperacao
     */
    public function getSt_avaliacaorecuperacao() {
        return $this->st_avaliacaorecuperacao;
    }

    /**
     * @param st_avaliacaorecuperacao
     */
    public function setSt_avaliacaorecuperacao($st_avaliacaorecuperacao) {
        $this->st_avaliacaorecuperacao = $st_avaliacaorecuperacao;
        return $this;
    }

    /**
     * @return string st_modulo
     */
    public function getSt_modulo() {
        return $this->st_modulo;
    }

    /**
     * @param st_modulo
     */
    public function setSt_modulo($st_modulo) {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string st_nomeusuarioaproveitamento
     */
    public function getSt_nomeusuarioaproveitamento() {
        return $this->st_nomeusuarioaproveitamento;
    }

    /**
     * @param st_nomeusuarioaproveitamento
     */
    public function setSt_nomeusuarioaproveitamento($st_nomeusuarioaproveitamento) {
        $this->st_nomeusuarioaproveitamento = $st_nomeusuarioaproveitamento;
        return $this;
    }

    /**
     * @return string st_disciplinaoriginal
     */
    public function getSt_disciplinaoriginal() {
        return $this->st_disciplinaoriginal;
    }

    /**
     * @param st_disciplinaoriginal
     */
    public function setSt_disciplinaoriginal($st_disciplinaoriginal) {
        $this->st_disciplinaoriginal = $st_disciplinaoriginal;
        return $this;
    }

    /**
     * @return string st_status
     */
    public function getSt_status() {
        return $this->st_status;
    }

    /**
     * @param st_status
     */
    public function setSt_status($st_status) {
        $this->st_status = $st_status;
        return $this;
    }

    /**
     * @return string st_statusdisciplina
     */
    public function getSt_statusdisciplina() {
        return $this->st_statusdisciplina;
    }

    /**
     * @param st_statusdisciplina
     */
    public function setSt_statusdisciplina($st_statusdisciplina) {
        $this->st_statusdisciplina = $st_statusdisciplina;
        return $this;
    }

    /**
     * @return string st_avaliacaoatividade
     */
    public function getSt_avaliacaoatividade() {
        return $this->st_avaliacaoatividade;
    }

    /**
     * @param st_avaliacaoatividade
     */
    public function setSt_avaliacaoatividade($st_avaliacaoatividade) {
        $this->st_avaliacaoatividade = $st_avaliacaoatividade;
        return $this;
    }

    /**
     * @return string st_avaliacaoead
     */
    public function getSt_avaliacaoead() {
        return $this->st_avaliacaoead;
    }

    /**
     * @param st_avaliacaoead
     */
    public function setSt_avaliacaoead($st_avaliacaoead) {
        $this->st_avaliacaoead = $st_avaliacaoead;
        return $this;
    }

    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
    }

    /**
     * @return date
     */
    public function getDt_defesatcc()
    {
        return $this->dt_defesatcc;
    }

    /**
     * @param date $dt_defesatcc
     */
    public function setDt_defesatcc($dt_defesatcc)
    {
        $this->dt_defesatcc = $dt_defesatcc;
    }

    /**
     * @return bool
     */
    public function getBl_coeficienterendimento()
    {
        return $this->bl_coeficienterendimento;
    }

    /**
     * @param bool $bl_coeficienterendimento
     * @return $this
     */
    public function setBl_coeficienterendimento($bl_coeficienterendimento)
    {
        $this->bl_coeficienterendimento = $bl_coeficienterendimento;
        return $this;
    }

}

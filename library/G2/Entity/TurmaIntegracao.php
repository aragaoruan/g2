<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmaintegracao")
 * @Entity
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class TurmaIntegracao
{

    /**
     * @Id
     * @var integer $id_turmaintegracao
     * @Column(name="id_turmaintegracao", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_turmaintegracao;

    /**
     * @var integer $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", referencedColumnName="id_turma", nullable=false)
     */
    private $id_turma;

    /**
     * @var string $st_turmasistema
     * @Column(name="st_turmasistema", type="string", nullable=true)
     */
    private $st_turmasistema;

    /**
     * @var integer $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     * @return int
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_turmaintegracao()
    {
        return $this->id_turmaintegracao;
    }

    /**
     * @param int $id_turmaintegracao
     */
    public function setId_turmaintegracao($id_turmaintegracao)
    {
        $this->id_turmaintegracao = $id_turmaintegracao;
    }

    /**
     * @return int
     */
    public function getst_turmasistema()
    {
        return $this->st_turmasistema;
    }

    /**
     * @param int $st_turmasistema
     */
    public function setst_turmasistema($st_turmasistema)
    {
        $this->st_turmasistema = $st_turmasistema;
    }
}
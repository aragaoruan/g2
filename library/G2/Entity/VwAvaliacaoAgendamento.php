<?php

namespace G2\Entity;

use DateTime;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoagendamento")
 * @Entity(repositoryClass="\G2\Repository\AvaliacaoAgendamento")
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @update Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAvaliacaoAgendamento extends G2Entity
{

    /**
     *
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false)
     * @Id
     */
    private $id_avaliacaoagendamento;

    /**
     * @Column(name="dt_agendamento", type="date", nullable=true)
     */
    private $dt_agendamento;

    /**
     * @Column(name="dt_aplicacao", type="datetime", nullable=true)
     */
    private $dt_aplicacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true)
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true)
     */
    private $id_usuario;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true )
     */
    private $id_situacao;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true )
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_primeirachamada
     * @Column(name="id_primeirachamada", type="integer", nullable=true )
     */
    private $id_primeirachamada;

    /**
     * @var integer $id_ultimachamada
     * @Column(name="id_ultimachamada", type="integer", nullable=true )
     */
    private $id_ultimachamada;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true )
     */
    private $id_evolucao;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true )
     */
    private $bl_provaglobal;

    /**
     * @var integer $id_mensagens
     * @Column(name="id_mensagens", type="integer", nullable=true )
     */
    private $id_mensagens;

    /**
     * @var integer $id_provasrealizadas
     * @Column(name="id_provasrealizadas", type="integer", nullable=true )
     */
    private $id_provasrealizadas;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true )
     */
    private $id_entidade;

    /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer", nullable=true )
     */
    private $id_horarioaula;

    /**
     * @var integer $id_avaliacao
     * @Column(name="id_avaliacao", type="integer", nullable=true )
     */
    private $id_avaliacao;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true )
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer")
     */
    private $id_tipoavaliacao;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer")
     */
    private $id_tipodeavaliacao;

    /**
     * @var integer $id_situacaopresenca
     * @Column(name="id_situacaopresenca", type="integer", nullable=true )
     */
    private $id_situacaopresenca;

    /**
     * @var integer $id_chamada
     * @Column(name="id_chamada", type="integer", nullable=true )
     */
    private $id_chamada;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true)
     */
    private $bl_ativo;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true,  length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true,  length=300)
     */
    private $st_cpf;

    /**
     * @var string $st_avaliacao
     * @Column(name="st_avaliacao", type="string", nullable=true,  length=100)
     */
    private $st_avaliacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_tipoprova
     * @Column(name="st_tipoprova", type="string", nullable=true, length=255)
     */
    private $st_tipoprova;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var string $st_horarioaula
     * @Column(name="st_horarioaula", type="string", nullable=true, length=255)
     */
    private $st_horarioaula;

    /**
     * @var string $st_mensagens
     * @Column(name="st_mensagens", type="string", nullable=true, length=37)
     */
    private $st_mensagens;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=true)
     */
    private $st_codsistema;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=true, length=200)
     */
    private $st_aplicadorprova;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;

    /**
     * @var string $st_situacaopresenca
     * @Column(name="st_situacaopresenca", type="string", nullable=true, length=42)
     */
    private $st_situacaopresenca;

    /**
     * @var integer $nu_primeirapresenca
     * @Column(name="nu_primeirapresenca", type="integer", nullable=true )
     */
    private $nu_primeirapresenca;

    /**
     * @var integer $nu_ultimapresenca
     * @Column(name="nu_ultimapresenca", type="integer", nullable=true )
     */
    private $nu_ultimapresenca;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true )
     */
    private $id_aplicadorprova;

    /**
     * @var integer $nu_presenca
     * @Column(name="nu_presenca", type="integer", nullable=true)
     */
    private $nu_presenca;

    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true)
     */
    private $id_usuariolancamento;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_usuariolancamento
     * @Column(name="st_usuariolancamento", type="string", nullable=true)
     */
    private $st_usuariolancamento;

    /**
     * @var integer $bl_temnotafinal
     * @Column(name="bl_temnotafinal", type="integer", nullable=true)
     */
    private $bl_temnotafinal;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true)
     */
    private $st_email;

    /**
     * @var string $nu_telefone
     * @Column(name="nu_telefone", type="string", nullable=true)
     */
    private $nu_telefone;

    /**
     * @var string $nu_ddd
     * @Column(name="nu_ddd", type="string", nullable=true)
     */
    private $nu_ddd;

    /**
     * @var integer $nu_ddi
     * @Column(name="nu_ddi", type="integer", nullable=true)
     */
    private $nu_ddi;

    /**
     * @var string $nu_telefonealternativo
     * @Column(name="nu_telefonealternativo", type="string", nullable=true)
     */
    private $nu_telefonealternativo;

    /**
     * @var string $nu_dddalternativo
     * @Column(name="nu_dddalternativo", type="string", nullable=true)
     */
    private $nu_dddalternativo;

    /**
     * @var integer $nu_ddialternativo
     * @Column(name="nu_ddialternativo", type="integer", nullable=true)
     */
    private $nu_ddialternativo;

    /**
     * @var datetime2 $hr_inicio
     * @Column(name="hr_inicio", type="datetime2", nullable=true)
     */
    private $hr_inicio;

    /**
     * @var datetime2 $hr_fim
     * @Column(name="hr_fim", type="datetime2", nullable=true)
     */
    private $hr_fim;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true)
     */
    private $sg_uf;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true)
     */
    private $nu_numero;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true)
     */
    private $st_bairro;

    /**
     * @var string $st_telefoneaplicador
     * @Column(name="st_telefoneaplicador", type="string", nullable=true)
     */
    private $st_telefoneaplicador;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true)
     */
    private $st_disciplina;

    /**
     * @var integer $bl_automatico
     * @Column(name="bl_automatico", type="integer", nullable=true )
     */
    private $bl_automatico;

    /**
     * @var integer $bl_possuiprova
     * @Column(name="bl_possuiprova", type="integer", nullable=true )
     */
    private $bl_possuiprova;

    /**
     * @var integer $st_possuiprova
     * @Column(name="st_possuiprova", type="string", nullable=true )
     */
    private $st_possuiprova;

    /**
     * @var integer $cod_prova
     * @Column(name="cod_prova", type="integer", nullable=true)
     */
    private $cod_prova;

    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @Column(name="dt_envioaviso", type="date", nullable=true)
     */
    private $dt_envioaviso;

    /**
     * @var integer $bl_temprovaintegrada
     * @Column(name="bl_temprovaintegrada", type="integer", nullable=false)
     */
    private $bl_temprovaintegrada;

    /**
     * @var bool $bl_sincronizado
     * @Column(name="bl_sincronizado", type="integer", nullable=true)
     */
    private $bl_sincronizado;

    public function getId_avaliacaoagendamento()
    {
        return $this->id_avaliacaoagendamento;
    }

    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    public function getDt_aplicacao()
    {
        return $this->dt_aplicacao;
    }

    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_primeirachamada()
    {
        return $this->id_primeirachamada;
    }

    public function getId_ultimachamada()
    {
        return $this->id_ultimachamada;
    }

    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    public function getBl_provaglobal()
    {
        return $this->bl_provaglobal;
    }

    public function getId_mensagens()
    {
        return $this->id_mensagens;
    }

    public function getId_provasrealizadas()
    {
        return $this->id_provasrealizadas;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_horarioaula()
    {
        return $this->id_horarioaula;
    }

    public function getId_avaliacao()
    {
        return $this->id_avaliacao;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function getId_tipoavaliacao()
    {
        return $this->id_tipoavaliacao;
    }

    public function getId_tipodeavaliacao()
    {
        return $this->id_tipodeavaliacao;
    }

    public function getId_situacaopresenca()
    {
        return $this->id_situacaopresenca;
    }

    public function getId_chamada()
    {
        return $this->id_chamada;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    public function getSt_avaliacao()
    {
        return $this->st_avaliacao;
    }

    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    public function getSt_tipoprova()
    {
        return $this->st_tipoprova;
    }

    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    public function getSt_horarioaula()
    {
        return $this->st_horarioaula;
    }

    public function getSt_mensagens()
    {
        return $this->st_mensagens;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function getSt_codsistema()
    {
        return $this->st_codsistema;
    }

    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    public function getSt_situacaopresenca()
    {
        return $this->st_situacaopresenca;
    }

    public function getNu_primeirapresenca()
    {
        return $this->nu_primeirapresenca;
    }

    public function getNu_ultimapresenca()
    {
        return $this->nu_ultimapresenca;
    }

    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }

    public function getNu_presenca()
    {
        return $this->nu_presenca;
    }

    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getSt_usuariolancamento()
    {
        return $this->st_usuariolancamento;
    }

    public function getBl_temnotafinal()
    {
        return $this->bl_temnotafinal;
    }

    public function getSt_email()
    {
        return $this->st_email;
    }

    public function getNu_telefone()
    {
        return $this->nu_telefone;
    }

    public function getNu_ddd()
    {
        return $this->nu_ddd;
    }

    public function getNu_ddi()
    {
        return $this->nu_ddi;
    }

    public function getNu_telefonealternativo()
    {
        return $this->nu_telefonealternativo;
    }

    public function getNu_dddalternativo()
    {
        return $this->nu_dddalternativo;
    }

    public function getNu_ddialternativo()
    {
        return $this->nu_ddialternativo;
    }

    public function getHr_inicio()
    {
        return $this->hr_inicio;
    }

    public function getHr_fim()
    {
        return $this->hr_fim;
    }

    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    public function getSt_telefoneaplicador()
    {
        return $this->st_telefoneaplicador;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function getBl_automatico()
    {
        return $this->bl_automatico;
    }

    public function getBl_possuiprova()
    {
        return $this->bl_possuiprova;
    }

    public function getSt_possuiprova()
    {
        return $this->st_possuiprova;
    }

    public function getCod_prova()
    {
        return $this->cod_prova;
    }

    public function getId_avaliacaoconjuntoreferencia()
    {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getDt_envioaviso()
    {
        return $this->dt_envioaviso;
    }

    public function getBl_temprovaintegrada()
    {
        return $this->bl_temprovaintegrada;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento)
    {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
        return $this;
    }

    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
        return $this;
    }

    public function setDt_aplicacao($dt_aplicacao)
    {
        $this->dt_aplicacao = $dt_aplicacao;
        return $this;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
        return $this;
    }

    public function setId_primeirachamada($id_primeirachamada)
    {
        $this->id_primeirachamada = $id_primeirachamada;
        return $this;
    }

    public function setId_ultimachamada($id_ultimachamada)
    {
        $this->id_ultimachamada = $id_ultimachamada;
        return $this;
    }

    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function setBl_provaglobal($bl_provaglobal)
    {
        $this->bl_provaglobal = $bl_provaglobal;
        return $this;
    }

    public function setId_mensagens($id_mensagens)
    {
        $this->id_mensagens = $id_mensagens;
        return $this;
    }

    public function setId_provasrealizadas($id_provasrealizadas)
    {
        $this->id_provasrealizadas = $id_provasrealizadas;
        return $this;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function setId_horarioaula($id_horarioaula)
    {
        $this->id_horarioaula = $id_horarioaula;
        return $this;
    }

    public function setId_avaliacao($id_avaliacao)
    {
        $this->id_avaliacao = $id_avaliacao;
        return $this;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function setId_tipoavaliacao($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao)
    {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
        return $this;
    }

    public function setId_situacaopresenca($id_situacaopresenca)
    {
        $this->id_situacaopresenca = $id_situacaopresenca;
        return $this;
    }

    public function setId_chamada($id_chamada)
    {
        $this->id_chamada = $id_chamada;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    public function setSt_avaliacao($st_avaliacao)
    {
        $this->st_avaliacao = $st_avaliacao;
        return $this;
    }

    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function setSt_tipoprova($st_tipoprova)
    {
        $this->st_tipoprova = $st_tipoprova;
        return $this;
    }

    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    public function setSt_horarioaula($st_horarioaula)
    {
        $this->st_horarioaula = $st_horarioaula;
        return $this;
    }

    public function setSt_mensagens($st_mensagens)
    {
        $this->st_mensagens = $st_mensagens;
        return $this;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function setSt_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
        return $this;
    }

    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
        return $this;
    }

    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    public function setSt_situacaopresenca($st_situacaopresenca)
    {
        $this->st_situacaopresenca = $st_situacaopresenca;
        return $this;
    }

    public function setNu_primeirapresenca($nu_primeirapresenca)
    {
        $this->nu_primeirapresenca = $nu_primeirapresenca;
        return $this;
    }

    public function setNu_ultimapresenca($nu_ultimapresenca)
    {
        $this->nu_ultimapresenca = $nu_ultimapresenca;
        return $this;
    }

    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
        return $this;
    }

    public function setNu_presenca($nu_presenca)
    {
        $this->nu_presenca = $nu_presenca;
        return $this;
    }

    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setSt_usuariolancamento($st_usuariolancamento)
    {
        $this->st_usuariolancamento = $st_usuariolancamento;
        return $this;
    }

    public function setBl_temnotafinal($bl_temnotafinal)
    {
        $this->bl_temnotafinal = $bl_temnotafinal;
        return $this;
    }

    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
        return $this;
    }

    public function setNu_telefone($nu_telefone)
    {
        $this->nu_telefone = $nu_telefone;
        return $this;
    }

    public function setNu_ddd($nu_ddd)
    {
        $this->nu_ddd = $nu_ddd;
        return $this;
    }

    public function setNu_ddi($nu_ddi)
    {
        $this->nu_ddi = $nu_ddi;
        return $this;
    }

    public function setNu_telefonealternativo($nu_telefonealternativo)
    {
        $this->nu_telefonealternativo = $nu_telefonealternativo;
        return $this;
    }

    public function setNu_dddalternativo($nu_dddalternativo)
    {
        $this->nu_dddalternativo = $nu_dddalternativo;
        return $this;
    }

    public function setNu_ddialternativo($nu_ddialternativo)
    {
        $this->nu_ddialternativo = $nu_ddialternativo;
        return $this;
    }

    public function setHr_inicio($hr_inicio)
    {
        $this->hr_inicio = $hr_inicio;
        return $this;
    }

    public function setHr_fim($hr_fim)
    {
        $this->hr_fim = $hr_fim;
        return $this;
    }

    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
        return $this;
    }

    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
        return $this;
    }

    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
        return $this;
    }

    public function setSt_telefoneaplicador($st_telefoneaplicador)
    {
        $this->st_telefoneaplicador = $st_telefoneaplicador;
        return $this;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function setBl_automatico($bl_automatico)
    {
        $this->bl_automatico = $bl_automatico;
        return $this;
    }

    public function setBl_possuiprova($bl_possuiprova)
    {
        $this->bl_possuiprova = $bl_possuiprova;
        return $this;
    }

    public function setSt_possuiprova($st_possuiprova)
    {
        $this->st_possuiprova = $st_possuiprova;
        return $this;
    }

    public function setCod_prova($cod_prova)
    {
        $this->cod_prova = $cod_prova;
        return $this;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia)
    {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
        return $this;
    }

    public function setDt_envioaviso($dt_envioaviso)
    {
        $this->dt_envioaviso = $dt_envioaviso;
        return $this;
    }

    public function setBl_temprovaintegrada($bl_temprovaintegrada)
    {
        $this->bl_temprovaintegrada = $bl_temprovaintegrada;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_sincronizado()
    {
        return $this->bl_sincronizado;
    }

    /**
     * @param bool $bl_sincronizado
     * @return VwAvaliacaoAgendamento
     */
    public function setBl_sincronizado($bl_sincronizado)
    {
        $this->bl_sincronizado = $bl_sincronizado;
        return $this;
    }


}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_produto")
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-04-22
 * @Entity(repositoryClass="\G2\Repository\Produto")
 * @EntityView
 */
class VwProduto
{

    /**
     *
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_produto;

    /**
     *
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=true, length=250)
     */
    private $st_produto;

    /**
     * @var TipoProduto $id_tipoproduto
     * @ManyToOne(targetEntity="TipoProduto")
     * @JoinColumn(name="id_tipoproduto", nullable=false, referencedColumnName="id_tipoproduto")
     */
    private $id_tipoproduto;

    /**
     * @var ModeloVenda $id_modelovenda
     * @ManyToOne(targetEntity="ModeloVenda")
     * @JoinColumn(name="id_modelovenda", nullable=false, referencedColumnName="id_modelovenda")
     */
    private $id_modelovenda;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false)
     */
    private $bl_todasformas;

    /**
     *
     * @var boolean $bl_todascampanhas
     * @Column(name="bl_todascampanhas", type="boolean", nullable=false)
     */
    private $bl_todascampanhas;

    /**
     *
     * @var integer $nu_gratuito
     * @Column(name="nu_gratuito", type="integer", nullable=false)
     */
    private $nu_gratuito;

    /**
     *
     * @var integer $id_produtoimagempadrao
     * @Column(name="id_produtoimagempadrao", type="integer", nullable=true)
     */
    private $id_produtoimagempadrao;

    /**
     *
     * @var boolean $bl_unico
     * @Column(name="bl_unico", type="boolean", nullable=false)
     */
    private $bl_unico;

    /**
     *
     * @var string $st_tipoproduto
     * @Column(name="st_tipoproduto", type="string", nullable=true, length=250)
     */
    private $st_tipoproduto;

    /**
     *
     * @var integer $id_produtovalor
     * @Column(name="id_produtovalor", type="integer", nullable=false)
     */
    private $id_produtovalor;

    /**
     *
     * @var string $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false)
     */
    private $nu_valor;

    /**
     *
     * @var string $nu_valormensal
     * @Column(name="nu_valormensal", type="decimal", nullable=false)
     */
    private $nu_valormensal;

    /**
     *
     * @var integer $id_tipoprodutovalor
     * @Column(name="id_tipoprodutovalor", type="integer", nullable=false)
     */
    private $id_tipoprodutovalor;

    /**
     *
     * @var string $st_tipoprodutovalor
     * @Column(name="st_tipoprodutovalor", type="string", nullable=true, length=250)
     */
    private $st_tipoprodutovalor;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_planopagamento
     * @Column(name="id_planopagamento", type="integer", nullable=false)
     */
    private $id_planopagamento;

    /**
     *
     * @var string $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=false)
     */
    private $nu_parcelas;

    /**
     *
     * @var string $nu_valorentrada
     * @Column(name="nu_valorentrada", type="decimal", nullable=false)
     */
    private $nu_valorentrada;

    /**
     *
     * @var string $nu_valorparcela
     * @Column(name="nu_valorparcela", type="decimal", nullable=false)
     */
    private $nu_valorparcela;

    /**
     *
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", nullable=false)
     */
    private $st_nomeentidade;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", nullable=false)
     */
    private $st_descricao;

    /**
     *
     * @var string $st_observacoes
     * @Column(name="st_observacoes", nullable=false)
     */
    private $st_observacoes;

    /**
     *
     * @var boolean $bl_destaque
     * @Column(name="bl_destaque", type="boolean", nullable=false)
     */
    private $bl_destaque;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var string $st_subtitulo
     * @Column(type="string", length=300, nullable=true, name="st_subtitulo")
     */
    private $st_subtitulo;

    /**
     * @var string $st_slug
     * @Column(type="string", length=200, nullable=true, name="st_slug")
     */
    private $st_slug;

    /**
     * @var datetime2 $dt_atualizado
     * @Column(type="datetime2", nullable=false, name="dt_atualizado")
     */
    private $dt_atualizado;

    /**
     * @var datetime2 $dt_iniciopontosprom
     * @Column(type="datetime2", nullable=false, name="dt_iniciopontosprom")
     */
    private $dt_iniciopontosprom;

    /**
     * @var datetime2 $dt_fimpontosprom
     * @Column(type="datetime2", nullable=false, name="dt_fimpontosprom")
     */
    private $dt_fimpontosprom;

    /**
     * @var integer $nu_pontos
     * @Column(type="integer", nullable=true, name="nu_pontos")
     */
    private $nu_pontos;

    /**
     * @var integer $nu_pontospromocional
     * @Column(type="integer", nullable=true, name="nu_pontospromocional")
     */
    private $nu_pontospromocional;

    /**
     * @var integer $nu_estoque
     * @Column(type="integer", nullable=true, name="nu_estoque")
     */
    private $nu_estoque;

    /**
     *
     * @var string $st_informacoesadicionais
     * @Column(type="string", nullable=true, name="st_informacoesadicionais")
     */
    private $st_informacoesadicionais;

    /**
     *
     * @var string $st_estruturacurricular
     * @Column(type="string", nullable=true, name="st_estruturacurricular")
     */
    private $st_estruturacurricular;


    /**
     *
     * @var integer $id_produtovalorvenda
     * @Column(name="id_produtovalorvenda", type="integer", nullable=false)
     */
    private $id_produtovalorvenda;


    /**
     *
     * @var string $nu_valorvenda
     * @Column(name="nu_valorvenda", type="decimal", nullable=false)
     */
    private $nu_valorvenda;

    /**
     *
     * @var string $nu_valorpromocional
     * @Column(name="nu_valorpromocional", type="decimal", nullable=false)
     */
    private $nu_valorpromocional;


    /**
     * @var Turma $id_turma
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", nullable=false, referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var Upload $id_upload
     * @Column(name="id_upload", type="integer", nullable=false)
     */
    private $id_upload;

    /**
     * @var Upload $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=false)
     */
    private $id_textosistema;

    /**
     * @var Forma Disponibilização $id_formadisponibilizacao
     * @Column(name="id_formadisponibilizacao", type="string", nullable=false)
     */
    private $id_formadisponibilizacao;

    /**
     * @var Forma Disponibilização $st_formadisponibilizacao
     * @Column(name="st_formadisponibilizacao", type="string", nullable=false)
     */
    private $st_formadisponibilizacao;

    /**
     * @var Produto Texto Sistema $id_produtotextosistema
     * @Column(name="id_produtotextosistema", type="integer", nullable=false)
     */
    private $id_produtotextosistema;

    /**
     * @var Upload $st_upload
     * @Column(name="st_upload", type="string", nullable=false)
     */
    private $st_upload;

    /**
     * @return the $id_modelovenda
     */
    public function getId_modelovenda()
    {
        return $this->id_modelovenda;
    }

    /**
     * @param \G2\Entity\ModeloVenda $id_modelovenda
     */
    public function setId_modelovenda($id_modelovenda)
    {
        $this->id_modelovenda = $id_modelovenda;
        return $this;
    }

    /**
     * @return the $id_produtovalorvenda
     */
    public function getId_produtovalorvenda()
    {
        return $this->id_produtovalorvenda;
    }

    /**
     * @return the $nu_valorvenda
     */
    public function getNu_valorvenda()
    {
        return $this->nu_valorvenda;
    }

    /**
     * @return the $nu_valorpromocional
     */
    public function getNu_valorpromocional()
    {
        return $this->nu_valorpromocional;
    }

    /**
     * @return the $id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }


    /**
     * @param number $id_produtovalorvenda
     */
    public function setId_produtovalorvenda($id_produtovalorvenda)
    {
        $this->id_produtovalorvenda = $id_produtovalorvenda;
        return $this;
    }

    /**
     * @param string $nu_valorvenda
     */
    public function setNu_valorvenda($nu_valorvenda)
    {
        $this->nu_valorvenda = $nu_valorvenda;
        return $this;
    }

    /**
     * @param string $nu_valorpromocional
     */
    public function setNu_valorpromocional($nu_valorpromocional)
    {
        $this->nu_valorpromocional = $nu_valorpromocional;
        return $this;
    }

    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return string st_subtitulo
     */
    public function getSt_subtitulo()
    {
        return $this->st_subtitulo;
    }

    /**
     * @return string st_slug
     */
    public function getSt_slug()
    {
        return $this->st_slug;
    }

    /**
     * @return datetime2 dt_atualizado
     */
    public function getDt_atualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @return datetime2 dt_iniciopontosprom
     */
    public function getDt_iniciopontosprom()
    {
        return $this->dt_iniciopontosprom;
    }

    /**
     * @return datetime2 dt_fimpontosprom
     */
    public function getDt_fimpontosprom()
    {
        return $this->dt_fimpontosprom;
    }

    /**
     * @return integer nu_pontos
     */
    public function getNu_pontos()
    {
        return $this->nu_pontos;
    }

    /**
     * @return integer nu_pontospromocional
     */
    public function getNu_pontospromocional()
    {
        return $this->nu_pontospromocional;
    }

    /**
     * @return integer nu_estoque
     */
    public function getNu_estoque()
    {
        return $this->nu_estoque;
    }

    /**
     * @return string st_informacoesadicionais
     */
    public function getSt_informacoesadicionais()
    {
        return $this->st_informacoesadicionais;
    }

    /**
     * @return string st_estruturacurricular
     */
    public function getSt_estruturacurricular()
    {
        return $this->st_estruturacurricular;
    }

    /**
     * @param string $st_subtitulo
     * @return \G2\Entity\VwProduto
     */
    public function setSt_subtitulo($st_subtitulo)
    {
        $this->st_subtitulo = $st_subtitulo;
        return $this;
    }

    /**
     *
     * @param string $st_slug
     * @return \G2\Entity\VwProduto
     */
    public function setSt_slug($st_slug)
    {
        $this->st_slug = $st_slug;
        return $this;
    }

    /**
     *
     * @param datetime2 $dt_atualizado
     * @return \G2\Entity\VwProduto
     */
    public function setDt_atualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
        return $this;
    }

    /**
     *
     * @param datetime2 $dt_iniciopontosprom
     * @return \G2\Entity\VwProduto
     */
    public function setDt_iniciopontosprom($dt_iniciopontosprom)
    {
        $this->dt_iniciopontosprom = $dt_iniciopontosprom;
        return $this;
    }

    /**
     *
     * @param datetime2 $dt_fimpontosprom
     * @return \G2\Entity\VwProduto
     */
    public function setDt_fimpontosprom($dt_fimpontosprom)
    {
        $this->dt_fimpontosprom = $dt_fimpontosprom;
        return $this;
    }

    /**
     *
     * @param integer $nu_pontos
     * @return \G2\Entity\VwProduto
     */
    public function setNu_pontos($nu_pontos)
    {
        $this->nu_pontos = $nu_pontos;
        return $this;
    }

    /**
     *
     * @param integer $nu_pontospromocional
     * @return \G2\Entity\VwProduto
     */
    public function setNu_pontospromocional($nu_pontospromocional)
    {
        $this->nu_pontospromocional = $nu_pontospromocional;
        return $this;
    }

    /**
     *
     * @param integer $nu_estoque
     * @return \G2\Entity\VwProduto
     */
    public function setNu_estoque($nu_estoque)
    {
        $this->nu_estoque = $nu_estoque;
        return $this;
    }

    /**
     *
     * @param string $st_informacoesadicionais
     * @return \G2\Entity\VwProduto
     */
    public function setSt_informacoesadicionais($st_informacoesadicionais)
    {
        $this->st_informacoesadicionais = $st_informacoesadicionais;
        return $this;
    }

    /**
     *
     * @param string $st_estruturacurricular
     * @return \G2\Entity\VwProduto
     */
    public function setSt_estruturacurricular($st_estruturacurricular)
    {
        $this->st_estruturacurricular = $st_estruturacurricular;
        return $this;
    }

    /**
     * @return the $id_produto
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @return the $st_produto
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @return the $id_tipoproduto
     */
    public function getId_tipoproduto()
    {
        return $this->id_tipoproduto;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return the $id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return the $id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return the $bl_todasformas
     */
    public function getBl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @return the $bl_todascampanhas
     */
    public function getBl_todascampanhas()
    {
        return $this->bl_todascampanhas;
    }

    /**
     * @return the $nu_gratuito
     */
    public function getNu_gratuito()
    {
        return $this->nu_gratuito;
    }

    /**
     * @return the $id_produtoimagempadrao
     */
    public function getId_produtoimagempadrao()
    {
        return $this->id_produtoimagempadrao;
    }

    /**
     * @return the $bl_unico
     */
    public function getBl_unico()
    {
        return $this->bl_unico;
    }

    /**
     * @return the $st_tipoproduto
     */
    public function getSt_tipoproduto()
    {
        return $this->st_tipoproduto;
    }

    /**
     * @return the $id_produtovalor
     */
    public function getId_produtovalor()
    {
        return $this->id_produtovalor;
    }

    /**
     * @return the $nu_valor
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @return the $nu_valormensal
     */
    public function getNu_valormensal()
    {
        return $this->nu_valormensal;
    }

    /**
     * @return the $id_tipoprodutovalor
     */
    public function getId_tipoprodutovalor()
    {
        return $this->id_tipoprodutovalor;
    }

    /**
     * @return the $st_tipoprodutovalor
     */
    public function getSt_tipoprodutovalor()
    {
        return $this->st_tipoprodutovalor;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @return the $id_planopagamento
     */
    public function getId_planopagamento()
    {
        return $this->id_planopagamento;
    }

    /**
     * @return the $nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @return the $nu_valorentrada
     */
    public function getNu_valorentrada()
    {
        return $this->nu_valorentrada;
    }

    /**
     * @return the $nu_valorparcela
     */
    public function getNu_valorparcela()
    {
        return $this->nu_valorparcela;
    }

    /**
     * @param number $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @param string $st_produto
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
        return $this;
    }

    /**
     * @param \G2\Entity\TipoProduto $id_tipoproduto
     */
    public function setId_tipoproduto($id_tipoproduto)
    {
        $this->id_tipoproduto = $id_tipoproduto;
        return $this;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param boolean $bl_todasformas
     */
    public function setBl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;
    }

    /**
     * @param boolean $bl_todascampanhas
     */
    public function setBl_todascampanhas($bl_todascampanhas)
    {
        $this->bl_todascampanhas = $bl_todascampanhas;
        return $this;
    }

    /**
     * @param number $nu_gratuito
     */
    public function setNu_gratuito($nu_gratuito)
    {
        $this->nu_gratuito = $nu_gratuito;
        return $this;
    }

    /**
     * @param number $id_produtoimagempadrao
     */
    public function setId_produtoimagempadrao($id_produtoimagempadrao)
    {
        $this->id_produtoimagempadrao = $id_produtoimagempadrao;
        return $this;
    }

    /**
     * @param boolean $bl_unico
     */
    public function setBl_unico($bl_unico)
    {
        $this->bl_unico = $bl_unico;
        return $this;
    }

    /**
     * @param string $st_tipoproduto
     */
    public function setSt_tipoproduto($st_tipoproduto)
    {
        $this->st_tipoproduto = $st_tipoproduto;
        return $this;
    }

    /**
     * @param number $id_produtovalor
     */
    public function setId_produtovalor($id_produtovalor)
    {
        $this->id_produtovalor = $id_produtovalor;
        return $this;
    }

    /**
     * @param string $nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @param string $nu_valormensal
     */
    public function setNu_valormensal($nu_valormensal)
    {
        $this->nu_valormensal = $nu_valormensal;
        return $this;
    }

    /**
     * @param number $id_tipoprodutovalor
     */
    public function setId_tipoprodutovalor($id_tipoprodutovalor)
    {
        $this->id_tipoprodutovalor = $id_tipoprodutovalor;
        return $this;
    }

    /**
     * @param string $st_tipoprodutovalor
     */
    public function setSt_tipoprodutovalor($st_tipoprodutovalor)
    {
        $this->st_tipoprodutovalor = $st_tipoprodutovalor;
        return $this;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @param number $id_planopagamento
     */
    public function setId_planopagamento($id_planopagamento)
    {
        $this->id_planopagamento = $id_planopagamento;
        return $this;
    }

    /**
     * @param string $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @param string $nu_valorentrada
     */
    public function setNu_valorentrada($nu_valorentrada)
    {
        $this->nu_valorentrada = $nu_valorentrada;
        return $this;
    }

    /**
     * @param string $nu_valorparcela
     */
    public function setNu_valorparcela($nu_valorparcela)
    {
        $this->nu_valorparcela = $nu_valorparcela;
        return $this;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return string
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_observacoes
     */
    public function setSt_observacoes($st_observacoes)
    {
        $this->st_observacoes = $st_observacoes;
    }

    /**
     * @return string
     */
    public function getSt_observacoes()
    {
        return $this->st_observacoes;
    }

    /**
     * @param boolean $bl_destaque
     */
    public function setBl_destaque($bl_destaque)
    {
        $this->bl_destaque = $bl_destaque;
    }

    /**
     * @return boolean
     */
    public function getBl_destaque()
    {
        return $this->bl_destaque;
    }

    /**
     * @param \G2\Entity\datetime2 $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return \G2\Entity\datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param number id_upload
     */
    public function setId_upload($id_upload)
    {
        $this->id_upload = $id_upload;
    }

    /**
     * @return $id_upload
     */
    public function getId_upload()
    {
        return $this->id_upload;
    }

    /**
     * @param number id_textosistema
     */
    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
    }

    /**
     * @return $id_upload
     */
    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    /**
     * @param number id_produtotextosistema
     */
    public function setId_produtotextosistema($id_produtotextosistema)
    {
        $this->id_produtotextosistema = $id_produtotextosistema;
    }

    /**
     * @return $id_upload
     */
    public function getId_produtotextosistema()
    {
        return $this->id_produtotextosistema;
    }

    /**
     * @param number id_formadisponibilizacao
     */
    public function setId_formadisponibilizacao($id_formadisponibilizacao)
    {
        $this->id_formadisponibilizacao = $id_formadisponibilizacao;
    }

    /**
     * @return $id_upload
     */
    public function getId_formadisponibilizacao()
    {
        return $this->id_formadisponibilizacao;
    }

    /**
     * @param number st_formadisponibilizacao
     */
    public function setSt_formadisponibilizacao($st_formadisponibilizacao)
    {
        $this->st_formadisponibilizacao = $st_formadisponibilizacao;
    }

    /**
     * @return $id_upload
     */
    public function getSt_formadisponibilizacao()
    {
        return $this->st_formadisponibilizacao;
    }

    /**
     * @param number st_upload
     */
    public function setSt_upload($st_upload)
    {
        $this->st_upload = $st_upload;
    }

    /**
     * @return $id_upload
     */
    public function getSt_upload()
    {
        return $this->st_upload;
    }

}

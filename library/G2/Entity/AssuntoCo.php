<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_assuntoco")
 * @Entity
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @EntityLog
 */
class AssuntoCo extends G2Entity {

    /**
     * @Id
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer",  nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_assuntoco;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2",  nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var AssuntoCo $id_assuntocopai
     * @ManyToOne(targetEntity="AssuntoCo")
     * @JoinColumn(name="id_assuntocopai", referencedColumnName="id_assuntoco")
     */
    private $id_assuntocopai;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var TipoOcorrencia $id_tipoocorrencia
     * @ManyToOne(targetEntity="TipoOcorrencia")
     * @JoinColumn(name="id_tipoocorrencia", referencedColumnName="id_tipoocorrencia")
     */
    private $id_tipoocorrencia;

    /**
     * @var TextoSistema $id_textosistema
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_textosistema", referencedColumnName="id_textosistema")
     */
    private $id_textosistema; //

    /**
     * @var boolean $bl_autodistribuicao
     * @Column(name="bl_autodistribuicao", type="boolean",  nullable=false, length=1)
     */
    private $bl_autodistribuicao;

    /**
     * @var boolean $bl_abertura
     * @Column(name="bl_abertura", type="boolean",  nullable=false, length=1)
     */
    private $bl_abertura;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean",  nullable=true, length=1)
     */
    private $bl_cancelamento;

    /**
     * @var boolean $bl_trancamento
     * @Column(name="bl_trancamento", type="boolean",  nullable=true, length=1)
     */
    private $bl_trancamento;

    /**
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string",  nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     * @var integer $id_assuntocategoria
     * @Column(name="id_assuntocategoria", type="integer", nullable=false, length=4)
     */
    private $id_assuntocategoria;

    /**
     * @var boolean $bl_interno
     * @Column(name="bl_interno", type="boolean",  nullable=true, length=1)
     */
    private $bl_interno;

    /**
     * @var boolean $bl_recuperacao_resgate
     * @Column(name="bl_recuperacao_resgate", type="boolean",  nullable=true, length=1)
     */
    private $bl_recuperacao_resgate;

    public function getBl_interno()
    {
        return $this->bl_interno;
    }


    public function setBl_interno($bl_interno)
    {
        $this->bl_interno = $bl_interno;
        return $this;
    }

    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_assuntocopai()
    {
        return $this->id_assuntocopai;
    }

    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    public function getBl_autodistribuicao()
    {
        return $this->bl_autodistribuicao;
    }

    public function getBl_abertura()
    {
        return $this->bl_abertura;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    public function getBl_trancamento()
    {
        return $this->bl_trancamento;
    }

    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
        return $this;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setId_assuntocopai($id_assuntocopai)
    {
        $this->id_assuntocopai = $id_assuntocopai;
        return $this;
    }

    public function setId_entidadecadastro($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
        return $this;
    }

    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function setBl_autodistribuicao($bl_autodistribuicao)
    {
        $this->bl_autodistribuicao = $bl_autodistribuicao;
        return $this;
    }

    public function setBl_abertura($bl_abertura)
    {
        $this->bl_abertura = $bl_abertura;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
        return $this;
    }

    public function setBl_trancamento($bl_trancamento)
    {
        $this->bl_trancamento = $bl_trancamento;
        return $this;
    }

    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_assuntocategoria()
    {
        return $this->id_assuntocategoria;
    }

    /**
     * @param int $id_assuntocategoria
     */
    public function setId_assuntocategoria($id_assuntocategoria)
    {
        $this->id_assuntocategoria = $id_assuntocategoria;
    }
    /**
     * @return mixed
     */
    public function getBl_recuperacao_resgate()
    {
        return $this->bl_recuperacao_resgate;
    }

    /**
     * @param mixed $bl_recuperacao_resgate
     */
    public function setBl_recuperacao_resgate($bl_recuperacao_resgate)
    {
        $this->bl_recuperacao_resgate = $bl_recuperacao_resgate;
    }

}

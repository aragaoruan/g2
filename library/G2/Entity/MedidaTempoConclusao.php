<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_medidatempoconclusao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */

use G2\G2Entity;

class MedidaTempoConclusao extends G2Entity
{

    /**
     * @var integer $id_medidatempoconclusao
     * @Column(type="integer", nullable=false, name="id_medidatempoconclusao")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_medidatempoconclusao;
    /**
     * @var string $st_medidatempoconclusao
     * @Column(type="string", length=50, nullable=true, name="st_medidatempoconclusao")
     */
    private $st_medidatempoconclusao;


    public function setId_medidatempoconclusao($id_medidatempoconclusao)
    {
        $this->id_medidatempoconclusao = $id_medidatempoconclusao;
    }

    public function getId_medidatempoconclusao()
    {
        return $this->id_medidatempoconclusao;
    }

    public function setSt_medidatempoconclusao($st_medidatempoconclusao)
    {
        $this->st_medidatempoconclusao = $st_medidatempoconclusao;
    }

    public function getSt_medidatempoconclusao()
    {
        return $this->st_medidatempoconclusao;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_categoriasala")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class CategoriaSala
{

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
     private $id_categoriasala;

    /**
     * @Column(name="st_categoriasala", type="string",length=100,nullable=false)
     * @var string $st_categoriasala
     */
     private $st_categoriasala;


    public function setId_categoriasala($id_categoriasala)
    {
        $this->id_categoriasala = $id_categoriasala;
    }

    public function getId_categoriasala()
    {
        return $this->id_categoriasala;
    }

    public function setSt_categoriasala($st_categoriasala)
    {
        $this->st_categoriasala = $st_categoriasala;
    }

    public function getSt_categoriasala()
    {
        return $this->st_categoriasala;
    }

}
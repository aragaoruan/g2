<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_relatoriosinteticocoordenador")
 * @Entity
 * @EntityView
 * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
 */
class VwRelatorioSinteticoCoordenador
{
    /**
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @Id
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var integer $bloqueado
     * @Column(name="bloqueado", type="integer", nullable=true, length=4)
     */

    private $bloqueado;

    /**
     * @var integer $cancelado
     * @Column(name="cancelado", type="integer", nullable=true, length=4)
     */
    private $cancelado;

    /**
     * @var integer $certificado
     * @Column(name="certificado", type="integer", nullable=true, length=4)
     */
    private $certificado;

    /**
     * @var integer $concluinte
     * @Column(name="concluinte", type="integer", nullable=true, length=4)
     */
    private $concluinte;

    /**
     * @var integer $cursando
     * @Column(name="cursando", type="integer", nullable=true, length=4)
     */
    private $cursando;

    /**
     * @var integer $decurso_de_prazo
     * @Column(name="decurso_de_prazo", type="integer", nullable=true, length=4)
     */
    private $decurso_de_prazo;

    /**
     * @var integer $trancado
     * @Column(name="trancado", type="integer", nullable=true, length=4)
     */
    private $trancado;

    /**
     * @var integer $transferido
     * @Column(name="transferido", type="integer", nullable=true, length=4)
     */
    private $transferido;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /** *
     * @return integer bloqueado
     */
    public function getBloqueado()
    {
        return $this->bloqueado;
    }

    /** *
     * @param bloqueado
     */
    public function setBloqueado($bloqueado)
    {
        $this->bloqueado = $bloqueado;
        return $this;
    }

    /**
     * @return integer cancelado
     */
    public function getCancelado()
    {
        return $this->cancelado;
    }

    /**
     * @param cancelado
     */
    public function setCancelado($cancelado)
    {
        $this->cancelado = $cancelado;
        return $this;
    }

    /**
     * @return integer certificado
     */
    public function getCertificado()
    {
        return $this->certificado;
    }

    /**
     * @param certificado
     */
    public function setCertificado($certificado)
    {
        $this->certificado = $certificado;
        return $this;
    }

    /**
     * @return integer concluinte
     */
    public function getConcluinte()
    {
        return $this->concluinte;
    }

    /**
     * @param concluinte
     */
    public function setConcluinte($concluinte)
    {
        $this->concluinte = $concluinte;
        return $this;
    }

    /**
     * @return integer cursando
     */
    public function getCursando()
    {
        return $this->cursando;
    }

    /**
     * @param cursando
     */
    public function setCursando($cursando)
    {
        $this->cursando = $cursando;
        return $this;
    }

    /**
     * @return integer decurso_de_prazo
     */
    public function getDecurso_de_prazo()
    {
        return $this->decurso_de_prazo;
    }

    /**
     * @param decurso_de_prazo
     */
    public function setDecurso_de_prazo($decurso_de_prazo)
    {
        $this->decurso_de_prazo = $decurso_de_prazo;
        return $this;
    }

    /**
     * @return integer trancado
     */
    public function getTrancado()
    {
        return $this->trancado;
    }

    /**
     * @param trancado
     */
    public function setTrancado($trancado)
    {
        $this->trancado = $trancado;
        return $this;
    }

    /**
     * @return integer transferido
     */
    public function getTransferido()
    {
        return $this->transferido;
    }

    /**
     * @param transferido
     */
    public function setTransferido($transferido)
    {
        $this->transferido = $transferido;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }


    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /** * @param st_nomeentidade */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_mensagempadrao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class MensagemPadrao
{

    /**
     *
     * @var integer $id_mensagempadrao
     * @Column(name="id_mensagempadrao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_mensagempadrao;

    /**
     * @var TipoEnvio $id_tipoenvio
     * @ManyToOne(targetEntity="TipoEnvio")
     * @JoinColumn(name="id_tipoenvio", nullable=true, referencedColumnName="id_tipoenvio")
     */
    private $id_tipoenvio;

    /**
     * @var TextoCategoria $id_textocategoria
     * @ManyToOne(targetEntity="TextoCategoria")
     * @JoinColumn(name="id_textocategoria", nullable=true, referencedColumnName="id_textocategoria")
     */
    private $id_textocategoria;

    /**
     *
     * @var string $st_mensagempadrao
     * @Column(name="st_mensagempadrao", type="string", nullable=false, length=150)
     */
    private $st_mensagempadrao;

    /**
     *
     * @var text $st_default
     * @Column(name="st_default", type="text", nullable=true)
     */
    private $st_default;

    public function getId_mensagempadrao ()
    {
        return $this->id_mensagempadrao;
    }

    public function setId_mensagempadrao ($id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
        return $this;
    }

    public function getId_tipoenvio ()
    {
        return $this->id_tipoenvio;
    }

    public function setId_tipoenvio (TipoEnvio $id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria (TextoCategoria $id_textocategoria = null)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getSt_mensagempadrao ()
    {
        return $this->st_mensagempadrao;
    }

    public function setSt_mensagempadrao ($st_mensagempadrao)
    {
        $this->st_mensagempadrao = $st_mensagempadrao;
        return $this;
    }

    public function getSt_default ()
    {
        return $this->st_default;
    }

    public function setSt_default ($st_default)
    {
        $this->st_default = $st_default;
        return $this;
    }

}
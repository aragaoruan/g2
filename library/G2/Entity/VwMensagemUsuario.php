<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_mensagemusuario")
 * @Entity
 * @EntityView
 * @author Débora Castro
 */
class VwMensagemUsuario
{

    /**
     * @var datetime2 $dt_cadastromsg
     * @Column(name="dt_cadastromsg", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastromsg;
    /**
     * @var datetime2 $dt_envio
     * @Column(name="dt_envio", type="datetime2", nullable=true, length=8)
     */
    private $dt_envio;
    /**
     * @var datetime $dt_enviar
     * @Column(name="dt_enviar", type="datetime", nullable=false, length=6)
     */
    private $dt_enviar;
    /**
     * @var integer $id_mensagem
     * @Column(name="id_mensagem", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_mensagem;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;
    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;
    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_evolucaodestinatario
     * @Column(name="id_evolucaodestinatario", type="integer", nullable=false, length=4)
     */
    private $id_evolucaodestinatario;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;
    /**
     * @var boolean $bl_importante
     * @Column(name="bl_importante", type="boolean", nullable=false, length=1)
     */
    private $bl_importante;
    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false, length=500)
     */
    private $st_mensagem;
    /**
     * @var string $st_texto
     * @Column(name="st_texto", type="string", nullable=false, length=350)
     */
    private $st_texto;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @var integer $id_enviomensagem
     * @Column(name="id_enviomensagem", type="integer", nullable=true, length=4)
     */
    private $id_enviomensagem;

    /**
     * @var integer $id_enviodestinatario
     * @Column(name="id_enviodestinatario", type="integer", nullable=true, length=4)
     */
    private $id_enviodestinatario;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=false)
     */
    private $st_nomeentidade;


    /**
     * @var integer $id_tipoenvio
     * @Column(name="id_tipoenvio", type="integer", nullable=true, length=4)
     */
    private $id_tipoenvio;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=false)
     */
    private $st_endereco;

    /**
     * @return int
     */
    public function getid_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    /**
     * @param int $id_tipoenvio
     * @return VwMensagemUsuario
     */
    public function setIdTipoenvio($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_endereco
     * @return VwMensagemUsuario
     */
    public function setStEndereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastromsg
     */
    public function getDt_cadastromsg()
    {
        return $this->dt_cadastromsg;
    }

    /**
     * @param dt_cadastromsg
     */
    public function setDt_cadastromsg($dt_cadastromsg)
    {
        $this->dt_cadastromsg = $dt_cadastromsg;
        return $this;
    }

    /**
     * @return datetime2 dt_envio
     */
    public function getDt_envio()
    {
        return $this->dt_envio;
    }

    /**
     * @param dt_envio
     */
    public function setDt_envio($dt_envio)
    {
        $this->dt_envio = $dt_envio;
        return $this;
    }

    /**
     * @return  dt_enviar
     */
    public function getDt_enviar()
    {
        return $this->dt_enviar;
    }

    /**
     * @param dt_enviar
     */
    public function setDt_enviar($dt_enviar)
    {
        $this->dt_enviar = $dt_enviar;
        return $this;
    }

    /**
     * @return integer id_mensagem
     */
    public function getId_mensagem()
    {
        return $this->id_mensagem;
    }

    /**
     * @param id_mensagem
     */
    public function setId_mensagem($id_mensagem)
    {
        $this->id_mensagem = $id_mensagem;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_areaconhecimento
     */
    public function getId_areaconhecimento()
    {
        return $this->id_areaconhecimento;
    }

    /**
     * @param id_areaconhecimento
     */
    public function setId_areaconhecimento($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    /**
     * @return integer id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return integer id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return boolean bl_importante
     */
    public function getBl_importante()
    {
        return $this->bl_importante;
    }

    /**
     * @param bl_importante
     */
    public function setBl_importante($bl_importante)
    {
        $this->bl_importante = $bl_importante;
        return $this;
    }

    /**
     * @return string st_mensagem
     */
    public function getSt_mensagem()
    {
        return $this->st_mensagem;
    }

    /**
     * @param st_mensagem
     */
    public function setSt_mensagem($st_mensagem)
    {
        $this->st_mensagem = $st_mensagem;
        return $this;
    }

    /**
     * @return string st_texto
     */
    public function getSt_texto()
    {
        return $this->st_texto;
    }

    /**
     * @param st_texto
     */
    public function setSt_texto($st_texto)
    {
        $this->st_texto = $st_texto;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId_enviomensagem()
    {
        return $this->id_enviomensagem;
    }

    /**
     * @param mixed $id_enviomensagem
     */
    public function setId_enviomensagem($id_enviomensagem)
    {
        $this->id_enviomensagem = $id_enviomensagem;
    }

    /**
     * @return int
     */
    public function getId_enviodestinatario()
    {
        return $this->id_enviodestinatario;
    }

    /**
     * @param int $id_enviodestinatario
     */
    public function setId_enviodestinatario($id_enviodestinatario)
    {
        $this->id_enviodestinatario = $id_enviodestinatario;
    }

    /**
     * @return int
     */
    public function getId_evolucaodestinatario()
    {
        return $this->id_evolucaodestinatario;
    }

    /**
     * @param int $id_evolucaodestinatario
     * @return $this
     */
    public function setId_evolucaodestinatario($id_evolucaodestinatario)
    {
        $this->id_evolucaodestinatario = $id_evolucaodestinatario;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }
}

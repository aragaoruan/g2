<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_salaofertaturma")
 * @Entity
 * @EntityView
 */

class VwSalaOfertaTurma
{

    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     */
    private $id_saladeaula;
    /**
     * @var \Datetime $dt_inicioinscricao
     * @Column(name="dt_inicioinscricao", type="datetime", nullable=false)
     */
    private $dt_inicioinscricao;
    /**
     * @var \Datetime $dt_fiminscricao
     * @Column(name="dt_fiminscricao", type="datetime", nullable=false)
     */
    private $dt_fiminscricao;
    /**
     * @var \Datetime $dt_abertura
     * @Column(name="dt_abertura", type="datetime", nullable=false)
     */
    private $dt_abertura;
    /**
     * @var \Datetime $dt_encerramento
     * @Column(name="dt_encerramento", type="datetime", nullable=false, length=8)
     */
    private $dt_encerramento;
    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false, length=4)
     */
    private $id_turma;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=true)
     */
    private $nu_creditos;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_modulo
     * @Column(name="st_modulo", type="string", nullable=false, length=255)
     */
    private $st_modulo;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;
    /**
     * @var integer $id_periodoletivo
     * @Column(name="id_periodoletivo", type="integer", nullable=false, length=4)
     */
    private $id_periodoletivo;
    /**
     * @var string $st_periodoletivo
     * @Column(name="st_periodoletivo", type="string", nullable=false, length=255)
     */
    private $st_periodoletivo;
    /**
     * @var boolean
     * @Column(type="boolean", nullable=false, name="bl_ofertaexcepcional")
     */
    private $bl_ofertaexcepcional;

    /**
     * @return \Datetime
     */
    public function getDt_inicioinscricao()
    {
        return $this->dt_inicioinscricao;
    }

    /**
     * @param \Datetime $dt_inicioinscricao
     * @return VwSalaOfertaTurma
     */
    public function setDt_inicioinscricao($dt_inicioinscricao)
    {
        $this->dt_inicioinscricao = $dt_inicioinscricao;
        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getDt_fiminscricao()
    {
        return $this->dt_fiminscricao;
    }

    /**
     * @param \Datetime $dt_fiminscricao
     * @return VwSalaOfertaTurma
     */
    public function setDt_fiminscricao($dt_fiminscricao)
    {
        $this->dt_fiminscricao = $dt_fiminscricao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param \DateTime $dt_abertura
     * @return VwSalaOfertaTurma
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param \DateTime $dt_encerramento
     * @return VwSalaOfertaTurma
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     * @return VwSalaOfertaTurma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwSalaOfertaTurma
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param int $id_projetopedagogico
     * @return VwSalaOfertaTurma
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    /**
     * @param int $id_modulo
     * @return VwSalaOfertaTurma
     */
    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param int $id_saladeaula
     * @return VwSalaOfertaTurma
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    /**
     * @param int $nu_maxalunos
     * @return VwSalaOfertaTurma
     */
    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    /**
     * @param int $id_trilha
     * @return VwSalaOfertaTurma
     */
    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwSalaOfertaTurma
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     * @return VwSalaOfertaTurma
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_modulo()
    {
        return $this->st_modulo;
    }

    /**
     * @param string $st_modulo
     * @return VwSalaOfertaTurma
     */
    public function setSt_modulo($st_modulo)
    {
        $this->st_modulo = $st_modulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param string $st_saladeaula
     * @return VwSalaOfertaTurma
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_periodoletivo()
    {
        return $this->id_periodoletivo;
    }

    /**
     * @param int $id_periodoletivo
     * @return VwSalaOfertaTurma
     */
    public function setId_periodoletivo($id_periodoletivo)
    {
        $this->id_periodoletivo = $id_periodoletivo;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_periodoletivo()
    {
        return $this->st_periodoletivo;
    }

    /**
     * @param string $st_periodoletivo
     * @return VwSalaOfertaTurma
     */
    public function setSt_periodoletivo($st_periodoletivo)
    {
        $this->st_periodoletivo = $st_periodoletivo;
        return $this;
    }

    /**
     * @return int
     */
    public function getNu_creditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @param int $nu_creditos
     * @return VwSalaOfertaTurma
     */
    public function setNu_creditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ofertaexcepcional()
    {
        return $this->bl_ofertaexcepcional;
    }

    /**
     * @param boolean $bl_ofertaexcepcional
     * @return VwSalaOfertaTurma
     */
    public function setBl_ofertaexcepcional($bl_ofertaexcepcional)
    {
        $this->bl_ofertaexcepcional = $bl_ofertaexcepcional;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param string $st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }



}
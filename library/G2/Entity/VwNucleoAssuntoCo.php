<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_nucleoassuntoco")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwNucleoAssuntoCo extends G2Entity
{

    /**
     *
     * @var integer $id_assuntoco
     * @Column(name="id_assuntoco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_assuntoco;

    /**
     *
     * @var string $st_assuntoco
     * @Column(name="st_assuntoco", type="string", nullable=false, length=200)
     */
    private $st_assuntoco;

    /**
     *
     * @var boolean $bl_abertura
     * @Column(name="bl_abertura", type="boolean", nullable=false)
     */
    private $bl_abertura;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=200)
     */
    private $st_situacao;

    /**
     * @var boolean $bl_autodistribuicao
     * @Column(name="bl_autodistribuicao", type="boolean", nullable=false)
     */
    private $bl_autodistribuicao;

    /**
     *
     * @var integer $id_tipoocorrencia
     * @Column(name="id_tipoocorrencia", type="integer", nullable=false)
     */
    private $id_tipoocorrencia;

    /**
     *
     * @var string $st_tipoocorrencia
     * @Column(name="st_tipoocorrencia", type="string", nullable=false, length=200)
     */
    private $st_tipoocorrencia;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_textonotificacao
     * @Column(name="id_textonotificacao", type="integer", nullable=true)
     */
    private $id_textonotificacao;

    /**
     * @var integer $id_nucleoassuntoco
     * @Column(name="id_nucleoassuntoco", type="integer", nullable=true)
     */
    private $id_nucleoassuntoco;

    /**
     *
     * @var integer $id_nucleoco
     * @Column(name="id_nucleoco", type="integer", nullable=false)
     */
    private $id_nucleoco;

    /**
     *
     * @var string $st_assuntopai
     * @Column(name="st_assuntopai", type="string", nullable=true, length=200)
     */
    private $st_assuntopai;


    /**
     * @var boolean $bl_notificaatendente
     * @Column(name="bl_notificaatendente", type="boolean", nullable=false)
     */
    private $bl_notificaatendente;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;


    /**
     * @var boolean $bl_cancelamento
     * @Column(name="bl_cancelamento", type="boolean")
     */
    private $bl_cancelamento;

    /**
     * @param boolean $bl_abertura
     */
    public function setBl_abertura($bl_abertura)
    {
        $this->bl_abertura = $bl_abertura;
    }

    /**
     * @return boolean
     */
    public function getBl_abertura()
    {
        return $this->bl_abertura;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_autodistribuicao
     */
    public function setBl_autodistribuicao($bl_autodistribuicao)
    {
        $this->bl_autodistribuicao = $bl_autodistribuicao;
    }

    /**
     * @return mixed
     */
    public function getBl_autodistribuicao()
    {
        return $this->bl_autodistribuicao;
    }

    /**
     * @param boolean $bl_notificaatendente
     */
    public function setBl_notificaatendente($bl_notificaatendente)
    {
        $this->bl_notificaatendente = $bl_notificaatendente;
    }

    /**
     * @return boolean
     */
    public function getBl_notificaatendente()
    {
        return $this->bl_notificaatendente;
    }

    /**
     * @param int $id_assuntoco
     */
    public function setId_assuntoco($id_assuntoco)
    {
        $this->id_assuntoco = $id_assuntoco;
    }

    /**
     * @return int
     */
    public function getId_assuntoco()
    {
        return $this->id_assuntoco;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_nucleoassuntoco
     */
    public function setId_nucleoassuntoco($id_nucleoassuntoco)
    {
        $this->id_nucleoassuntoco = $id_nucleoassuntoco;
    }

    /**
     * @return int
     */
    public function getId_nucleoassuntoco()
    {
        return $this->id_nucleoassuntoco;
    }

    /**
     * @param int $id_nucleoco
     */
    public function setId_nucleoco($id_nucleoco)
    {
        $this->id_nucleoco = $id_nucleoco;
    }

    /**
     * @return int
     */
    public function getId_nucleoco()
    {
        return $this->id_nucleoco;
    }

    /**
     * @param int $id_textonotificacao
     */
    public function setId_textonotificacao($id_textonotificacao)
    {
        $this->id_textonotificacao = $id_textonotificacao;
    }

    /**
     * @return int
     */
    public function getId_textonotificacao()
    {
        return $this->id_textonotificacao;
    }

    /**
     * @param mixed $id_tipoocorrencia
     */
    public function setId_tipoocorrencia($id_tipoocorrencia)
    {
        $this->id_tipoocorrencia = $id_tipoocorrencia;
    }

    /**
     * @return mixed
     */
    public function getId_tipoocorrencia()
    {
        return $this->id_tipoocorrencia;
    }

    /**
     * @param string $st_assuntoco
     */
    public function setSt_assuntoco($st_assuntoco)
    {
        $this->st_assuntoco = $st_assuntoco;
    }

    /**
     * @return string
     */
    public function getSt_assuntoco()
    {
        return $this->st_assuntoco;
    }

    /**
     * @param string $st_assuntocopai
     */
    public function setSt_assuntopai($st_assuntopai)
    {
        $this->st_assuntopai = $st_assuntopai;
    }

    /**
     * @return string
     */
    public function getSt_assuntopai()
    {
        return $this->st_assuntopai;
    }

    /**
     * @param string $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return string
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_tipoocorrencia
     */
    public function setSt_tipoocorrencia($st_tipoocorrencia)
    {
        $this->st_tipoocorrencia = $st_tipoocorrencia;
    }

    /**
     * @return string
     */
    public function getSt_tipoocorrencia()
    {
        return $this->st_tipoocorrencia;
    }

    /**
     * @return integer $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return bool
     */
    public function getBl_cancelamento()
    {
        return $this->bl_cancelamento;
    }

    /**
     * @param bool $bl_cancelamento
     * @return VwNucleoAssuntoCo
     */
    public function setBl_cancelamento($bl_cancelamento)
    {
        $this->bl_cancelamento = $bl_cancelamento;
        return $this;
    }


}

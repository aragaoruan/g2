<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_matriculacolacao")
 * @Entity
 * @EntityLog
 */
class MatriculaColacao extends G2Entity
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_matriculacolacao
     * @Column(name="id_matriculacolacao", type="integer", nullable=false, length=4)
     */
    private $id_matriculacolacao;


    /**
     * @var Matricula $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_colacao
     * @Column(name="dt_colacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_colacao;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_presenca
     * @Column(name="bl_presenca", type="boolean", nullable=false, length=1, options={"default":false})
     */
    private $bl_presenca;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1, options={"default":true})
     */
    private $bl_ativo;

    /**
     * @return int
     */
    public function getId_matriculacolacao()
    {
        return $this->id_matriculacolacao;
    }

    /**
     * @param int $id_matriculacolacao
     * @return $this
     */
    public function setId_matriculacolacao($id_matriculacolacao)
    {
        $this->id_matriculacolacao = $id_matriculacolacao;
        return $this;
    }

    /**
     * @return Matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param Matricula $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return datetime2
     */
    public function getDt_colacao()
    {
        return $this->dt_colacao;
    }

    /**
     * @param datetime2 $dt_colacao
     * @return $this
     */
    public function setDt_colacao($dt_colacao)
    {
        $this->dt_colacao = $dt_colacao;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param Usuario $id_usuariocadastro
     * @return $this
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_presenca()
    {
        return $this->bl_presenca;
    }

    /**
     * @param boolean $bl_presenca
     * @return $this
     */
    public function setBl_presenca($bl_presenca)
    {
        $this->bl_presenca = $bl_presenca;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

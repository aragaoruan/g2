<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_matriculalancamento")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwMatriculaLancamento
{

    /**
     * @var \DateTime $dt_quitado
     * @Column(name="dt_quitado", type="datetime2", nullable=true, length=3)
     */
    private $dt_quitado;
    /**
     * @var \DateTime $dt_vencimento
     * @Column(name="dt_vencimento", type="datetime2", nullable=true, length=3)
     */
    private $dt_vencimento;
    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var \DateTime $dt_emissao
     * @Column(name="dt_emissao", type="datetime2", nullable=true, length=8)
     */
    private $dt_emissao;
    /**
     * @var \DateTime $dt_prevquitado
     * @Column(name="dt_prevquitado", type="datetime2", nullable=true, length=8)
     */
    private $dt_prevquitado;
    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_matricula;
    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer", nullable=false, length=4)
     */
    private $id_lancamento;
    /**
     * @var integer $id_meiopagamento
     * @Column(name="id_meiopagamento", type="integer", nullable=false, length=4)
     */
    private $id_meiopagamento;
    /**
     * @var integer $id_tipolancamento
     * @Column(name="id_tipolancamento", type="integer", nullable=false, length=4)
     */
    private $id_tipolancamento;
    /**
     * @var integer $id_usuariolancamento
     * @Column(name="id_usuariolancamento", type="integer", nullable=true, length=4)
     */
    private $id_usuariolancamento;
    /**
     * @var boolean $bl_quitado
     * @Column(name="bl_quitado", type="boolean", nullable=false, length=1)
     */
    private $bl_quitado;
    /**
     * @var boolean $bl_original
     * @Column(name="bl_original", type="boolean", nullable=false, length=1)
     */
    private $bl_original;
    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="decimal", nullable=false, length=17)
     */
    private $nu_valor;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=false, length=255)
     */
    private $st_meiopagamento;
    /**
     * @var string $st_usuariolancamento
     * @Column(name="st_usuariolancamento", type="string", nullable=true, length=300)
     */
    private $st_usuariolancamento;


    /**
     * @return \DateTime dt_quitado
     */
    public function getDt_quitado()
    {
        return $this->dt_quitado;
    }

    /**
     * @param dt_quitado
     */
    public function setDt_quitado($dt_quitado)
    {
        $this->dt_quitado = $dt_quitado;
        return $this;
    }

    /**
     * @return \DateTime dt_vencimento
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
        return $this;
    }

    /**
     * @return \DateTime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return \DateTime dt_emissao
     */
    public function getDt_emissao()
    {
        return $this->dt_emissao;
    }

    /**
     * @param dt_emissao
     */
    public function setDt_emissao($dt_emissao)
    {
        $this->dt_emissao = $dt_emissao;
        return $this;
    }

    /**
     * @return \DateTime dt_prevquitado
     */
    public function getDt_prevquitado()
    {
        return $this->dt_prevquitado;
    }

    /**
     * @param dt_prevquitado
     */
    public function setDt_prevquitado($dt_prevquitado)
    {
        $this->dt_prevquitado = $dt_prevquitado;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_lancamento
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param id_lancamento
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
        return $this;
    }

    /**
     * @return integer id_meiopagamento
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
        return $this;
    }

    /**
     * @return integer id_tipolancamento
     */
    public function getId_tipolancamento()
    {
        return $this->id_tipolancamento;
    }

    /**
     * @param id_tipolancamento
     */
    public function setId_tipolancamento($id_tipolancamento)
    {
        $this->id_tipolancamento = $id_tipolancamento;
        return $this;
    }

    /**
     * @return integer id_usuariolancamento
     */
    public function getId_usuariolancamento()
    {
        return $this->id_usuariolancamento;
    }

    /**
     * @param id_usuariolancamento
     */
    public function setId_usuariolancamento($id_usuariolancamento)
    {
        $this->id_usuariolancamento = $id_usuariolancamento;
        return $this;
    }

    /**
     * @return boolean bl_quitado
     */
    public function getBl_quitado()
    {
        return $this->bl_quitado;
    }

    /**
     * @param bl_quitado
     */
    public function setBl_quitado($bl_quitado)
    {
        $this->bl_quitado = $bl_quitado;
        return $this;
    }

    /**
     * @return boolean bl_original
     */
    public function getBl_original()
    {
        return $this->bl_original;
    }

    /**
     * @param bl_original
     */
    public function setBl_original($bl_original)
    {
        $this->bl_original = $bl_original;
        return $this;
    }

    /**
     * @return decimal nu_valor
     */
    public function getNu_valor()
    {
        return $this->nu_valor;
    }

    /**
     * @param nu_valor
     */
    public function setNu_valor($nu_valor)
    {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_meiopagamento
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
        return $this;
    }

    /**
     * @return string st_usuariolancamento
     */
    public function getSt_usuariolancamento()
    {
        return $this->st_usuariolancamento;
    }

    /**
     * @param st_usuariolancamento
     */
    public function setSt_usuariolancamento($st_usuariolancamento)
    {
        $this->st_usuariolancamento = $st_usuariolancamento;
        return $this;
    }

}
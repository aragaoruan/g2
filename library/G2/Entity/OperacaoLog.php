<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * Entity para tabela fria
 * @Entity
 * @Table(name="tb_operacaolog")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class OperacaoLog extends G2Entity
{

    /**
     * @var integer $id_operacaolog
     * @Column(type="integer", nullable=false, name="id_operacaolog")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_operacaolog;

    /**
     * @var string $st_nomeoperacaolog
     * @Column(type="string", nullable=false, name="st_nomeoperacaolog")
     */
    private $st_nomeoperacaolog;

    /**
     * @var string $st_descricaoperacaolog
     * @Column(type="string", nullable=true, name="st_descricaooperacaolog")
     */
    private $st_descricaoperacaolog;


    /**
     * @return int
     */
    public function getId_operacaolog()
    {
        return $this->id_operacaolog;
    }

    /**
     * @return string
     */
    public function getSt_nomeoperacaolog()
    {
        return $this->st_nomeoperacaolog;
    }

    /**
     * @return string
     */
    public function getSt_descricaoperacaolog()
    {
        return $this->st_descricaoperacaolog;
    }

    /**
     * @param $id_operacaolog
     * @return $this
     */
    public function setId_operacaolog($id_operacaolog)
    {
        $this->id_operacaolog = $id_operacaolog;
        return $this;
    }

    /**
     * @param $st_nomeoperacaolog
     * @return $this
     */
    public function setSt_nomeoperacaolog($st_nomeoperacaolog)
    {
        $this->st_nomeoperacaolog = $st_nomeoperacaolog;
        return $this;
    }

    /**
     * @param $st_descricaoperacaolog
     * @return $this
     */
    public function setSt_descricaoperacaolog($st_descricaoperacaolog)
    {
        $this->st_descricaoperacaolog = $st_descricaoperacaolog;
        return $this;
    }

}

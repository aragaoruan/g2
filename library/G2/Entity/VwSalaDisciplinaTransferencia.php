<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladisciplinatransferencia")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwSalaDisciplinaTransferencia
{

    /**
     * @var integer $id_saladeaula
     * @Column(type="integer", nullable=false, name="id_saladeaula")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;

    /**
     * @var string $st_saladeaula
     * @Column(type="string", length=255, nullable=false, name="st_saladeaula")
     */
    private $st_saladeaula;


    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true)
     */
    private $nu_maxalunos;



    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=true, name="id_projetopedagogico")
     */
    private $id_projetopedagogico;


    /**
     * @var integer $id_trilha
     * @Column(type="integer", nullable=true, name="id_trilha")
     */
    private $id_trilha;

    /**
     * @var integer $id_turma
     * @Column(type="integer", nullable=true, name="id_turma")
     */
    private $id_turma;

    /**
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     */
    private $id_disciplina;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $nu_alocados
     * @Column(name="nu_alocados", type="integer", nullable=true)
     */
    private $nu_alocados;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true)
     */
    private $dt_abertura;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true)
     */
    private $dt_encerramento;

    /**
     * @var string $st_disciplina;
     * @Column(type="string", length=255, nullable=false, name="st_disciplina")
     */
    private $st_disciplina;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true)
     */
    private $id_tipodisciplina;

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
    }

    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    public function getId_turma()
    {
        return $this->id_turma;
    }

    public function setNu_alocados($nu_alocados)
    {
        $this->nu_alocados = $nu_alocados;
    }

    public function getNu_alocados()
    {
        return $this->nu_alocados;
    }

    public function setNu_maxalunos($nu_maxalunos)
    {
        $this->nu_maxalunos = $nu_maxalunos;
    }

    public function getNu_maxalunos()
    {
        return $this->nu_maxalunos;
    }

    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }

    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }

    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }

    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }


    /**
     * @return array Array for attributes of the entity
     */
    public function _toArray ()
    {
        return array(
            'id_saladeaula' => $this->id_saladeaula,
            'st_saladeaula'=> $this->st_saladeaula,
            'nu_maxalunos'=> $this->nu_maxalunos,
            'id_projetopedagogico'=> $this->id_projetopedagogico,
            'id_trilha'=> $this->id_trilha,
            'id_turma'=> $this->id_turma,
            'id_disciplina'=> $this->id_disciplina,
            'id_entidade'=> $this->id_entidade,
            'nu_alocados'=> $this->nu_alocados,
            'dt_abertura' => (is_object($this->dt_abertura) && $this->dt_abertura instanceof \DateTime ? $this->dt_abertura->format("d/m/Y"):'' ),
            'dt_encerramento' => (is_object($this->dt_encerramento) && $this->dt_encerramento instanceof \DateTime ? $this->dt_encerramento->format("d/m/Y"):'' ),
            'st_disciplina' => $this->st_disciplina

        );
    }

}
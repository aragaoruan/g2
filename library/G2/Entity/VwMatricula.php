<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_matricula")
 * @Entity(repositoryClass="\G2\Repository\Matricula")
 * @EntityView
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwMatricula extends G2Entity
{

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer")
     */
    private $id_usuario;

    /**
     * @var integer $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string")
     */
    private $st_nomecompleto;

    /**
     * @var integer $st_cpf
     * @Column(name="st_cpf", type="string")
     */
    private $st_cpf;

    /**
     * @var \DateTime $dt_nascimento
     * @Column(name="dt_nascimento", type="datetime2")
     */
    private $dt_nascimento;

    /**
     * @var integer $st_nomepai
     * @Column(name="st_nomepai", type="string")
     */
    private $st_nomepai;

    /**
     * @var string $st_nomemae
     * @Column(name="st_nomemae", type="string")
     */
    private $st_nomemae;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string")
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer")
     */
    private $id_situacao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string")
     */
    private $st_situacao;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer")
     */
    private $id_evolucao;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string")
     */
    private $st_evolucao;

    /**
     * @var \DateTime $dt_concluinte
     * @Column(name="dt_concluinte", type="datetime2")
     */
    private $dt_concluinte;

    /**
     * @var \DateTime $dt_colacao
     * @Column(name="dt_colacao", type="datetime2")
     */
    private $dt_colacao;

    /**
     * @var integer $id_entidadematriz
     * @Column(name="id_entidadematriz", type="integer")
     */
    private $id_entidadematriz;

    /**
     * @var string $st_entidadematriz
     * @Column(name="st_entidadematriz", type="string")
     */
    private $st_entidadematriz;

    /**
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer")
     */
    private $id_entidadematricula;

    /**
     * @var string $st_entidadematricula
     * @Column(name="st_entidadematricula", type="string")
     */
    private $st_entidadematricula;

    /**
     * @var integer $id_entidadeatendimento
     * @Column(name="id_entidadeatendimento", type="integer")
     */
    private $id_entidadeatendimento;

    /**
     * @var string $st_entidadeatendimento
     * @Column(name="st_entidadeatendimento", type="string")
     */
    private $st_entidadeatendimento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean",  nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer")
     */
    private $id_contrato;

    /**
     * @var integer $nu_semestreatual
     * @Column(type="integer", nullable=true, name="nu_semestreatual")
     */
    private $nu_semestreatual;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer")
     */
    private $id_projetopedagogico;

    /**
     * @var \DateTime $dt_termino
     * @Column(name="dt_termino", type="datetime2")
     */
    private $dt_termino;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2")
     */
    private $dt_cadastro;

    /**
     * @var \DateTime $dt_inicio
     * @Column(name="dt_inicio", type="datetime2")
     */
    private $dt_inicio;

    /**
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer")
     */
    private $id_turma;

    /**
     * @var integer $id_turmaorigem
     * @Column(name="id_turmaorigem", type="integer")
     */
    private $id_turmaorigem;

    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string")
     */
    private $st_turma;

    /**
     * @var \DateTime $dt_inicioturma
     * @Column(name="dt_inicioturma", type="datetime2")
     */
    private $dt_inicioturma;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string")
     */
    private $st_email;

    /**
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string")
     */
    private $st_telefone;

    /**
     * @var boolean $bl_institucional
     * @Column(name="bl_institucional", type="boolean", length=1)
     */
    private $bl_institucional;

    /**
     * @var \DateTime $dt_terminoturma
     * @Column(name="dt_terminoturma", type="datetime2")
     */
    private $dt_terminoturma;

    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", type="integer")
     */
    private $id_situacaoagendamento;

    /**
     * @var integer $id_vendaproduto
     * @Column(type="integer", nullable=false, name="id_vendaproduto")
     */
    private $id_vendaproduto;

    /**
     * @var integer $id_produto
     * @Column(type="integer", nullable=false, name="id_produto")
     */
    private $id_produto;

    /**
     * @var integer $id_venda
     * @Column(type="integer", nullable=false, name="id_venda")
     */
    private $id_venda;

    /**
     * @var date $dt_terminomatricula
     * @Column(type="date", nullable=true, name="dt_terminomatricula")
     */
    private $dt_terminomatricula;

    /**
     * @var $st_urlacesso
     * @Column(name="st_urlacesso", type="string")
     */
    private $st_urlacesso;

    /**
     * @var boolean $bl_documentacao
     * @Column(name="bl_documentacao", type="boolean", length=1)
     */
    private $bl_documentacao;

    /**
     * @var $st_urlportal
     * @Column(name="st_urlportal", type="string")
     */
    private $st_urlportal;

    /**
     * @var $st_urlportalmatricula
     * @Column(name="st_urlportalmatricula", type="string")
     */
    private $st_urlportalmatricula;

    /**
     * @var string $st_identificacao
     * @Column(name="st_identificacao", type="string")
     */
    private $st_identificacao;

    /**
     * @var string $st_codcertificacao
     * @Column(name="st_codcertificacao", type="string", nullable=true, length=200)
     */
    private $st_codcertificacao;

    /**
     * @var string $st_urlavatar
     * @Column(name="st_urlavatar", type="string", nullable=true)
     */
    private $st_urlavatar;
    /**
     * @var integer $id_indicecertificado
     * @Column(name="id_indicecertificado", type="integer", nullable=true, length=4)
     */
    private $id_indicecertificado;

    /**
     * @var string $st_telefonealternativo
     * @Column(name="st_telefonealternativo", type="string", nullable=true, length=61)
     */
    private $st_telefonealternativo;

    /**
     * @var string $st_dtcadastrocertificacao
     * @Column(name="st_dtcadastrocertificacao", type="string", nullable=true, length=30)
     */
    private $st_dtcadastrocertificacao;

    /**
     * @var string $st_dtenviocertificado
     * @Column(name="st_dtenviocertificado", type="string", nullable=true, length=30)
     */
    private $st_dtenviocertificado;

    /**
     * @var string $st_dtretornocertificadora
     * @Column(name="st_dtretornocertificadora", type="string", nullable=true, length=30)
     */
    private $st_dtretornocertificadora;

    /**
     * @var string $st_dtenvioaluno
     * @Column(name="st_dtenvioaluno", type="string", nullable=true, length=30)
     */
    private $st_dtenvioaluno;

    /**
     * @var string $st_codigoacompanhamento
     * @Column(name="st_codigoacompanhamento", type="string", nullable=true, length=13)
     */
    private $st_codigoacompanhamento;

    /**
     * @var boolean $bl_disciplinacomplementar
     * @Column(name="bl_disciplinacomplementar", type="boolean",  nullable=false, length=1)
     */
    private $bl_disciplinacomplementar;

    /**
     * @var integer $id_matriculavinculada
     * @Column(name="id_matriculavinculada", type="integer", nullable=true, length=4)
     */
    private $id_matriculavinculada;

    /**
     * @var integer $id_tiporegracontrato
     * @Column(name="id_tiporegracontrato", type="integer", nullable=true, length=4)
     */
    private $id_tiporegracontrato;

    /**
     * @var boolean $bl_academico
     * @Column(name="bl_academico", type="boolean", length=1)
     */
    private $bl_academico;

    /**
     * @var \DateTime $dt_cadastrocertificacao
     * @Column(name="dt_cadastrocertificacao", type="datetime")
     */
    private $dt_cadastrocertificacao;

    /**
     * @var \DateTime $dt_enviocertificado
     * @Column(name="dt_enviocertificado", type="datetime")
     */
    private $dt_enviocertificado;

    /**
     * @var \DateTime $dt_retornocertificadora
     * @Column(name="dt_retornocertificadora", type="datetime")
     */
    private $dt_retornocertificadora;

    /**
     * @var \DateTime $dt_envioaluno
     * @Column(name="dt_envioaluno", type="datetime")
     */
    private $dt_envioaluno;

    /**
     * @var string $st_uploadcontrato
     * @Column(name="st_uploadcontrato", type="string", nullable=true)
     */
    private $st_uploadcontrato;

    /**
     * @var integer $id_contratoregra
     * @Column(name="id_contratoregra", type="integer", nullable=true, length=4)
     */
    private $id_contratoregra;

    /**
     * @var integer $id_aproveitamento
     * @Column(name="id_aproveitamento", type="integer", nullable=true, length=4)
     */
    private $id_aproveitamento;

    /**
     * @var integer $st_codigo
     * @Column(name="st_codigo", type="string", nullable=true)
     */
    private $st_codigo;

    /**
     * @var integer $st_descricao
     * @Column(name="st_descricao", type="string", nullable=true)
     */
    private $st_descricao;

    /**
     * @var integer $st_imagem
     * @Column(name="st_imagem", type="string", nullable=true)
     */
    private $st_imagem;


    /**
     * @var integer $id_situacaoagendamentoparcial
     * @Column(name="id_situacaoagendamentoparcial", type="integer", nullable=true)
     */
    private $id_situacaoagendamentoparcial;

    /**
     * @var boolean $bl_novoportal
     * @Column(name="bl_novoportal", type="boolean", length=1)
     */
    private $bl_novoportal;

    /**
     * @var string $nu_anomatricula
     * @Column(name="nu_anomatricula", type="string", nullable=true, length=4)
     */
    private $nu_anomatricula;

    /**
     * @var integer $id_esquemaconfiguracao
     * @Column(name="id_esquemaconfiguracao", type="integer", nullable=false, length=4)
     */
    private $id_esquemaconfiguracao;
    /**
     * @var integer $id_cancelamento
     * @Column(name="id_cancelamento", type="integer", nullable=false, length=4)
     */
    private $id_cancelamento;
    /**
     * @var $st_urlnovoportal
     * @Column(name="st_urlnovoportal", type="string")
     */
    private $st_urlnovoportal;

    /**
     * @var \DateTime $dt_aceitecontrato
     * @Column(name="dt_aceitecontrato", type="datetime2")
     */
    private $dt_aceitecontrato;

    /**
     * @var \DateTime $dt_ultimaoferta
     * @Column(name="dt_ultimaoferta", type="datetime2")
     */
    private $dt_ultimaoferta;

    /**
     * @var \DateTime $dt_ultimoacesso
     * @Column(name="dt_ultimoacesso", type="datetime2")
     */
    private $dt_ultimoacesso;

    /**
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string")
     */
    private $st_tituloexibicao;


    /**
     * @param $dt_aceitecontrato
     * @return $this
     */
    public function setDt_aceitecontrato($dt_aceitecontrato)
    {
        $this->dt_aceitecontrato = $dt_aceitecontrato;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_aceitecontrato()
    {
        return $this->dt_aceitecontrato;
    }


    /**
     * @return mixed
     */
    public function getst_urlportalmatricula()
    {
        return $this->st_urlportalmatricula;
    }

    /**
     * @param mixed $st_urlportalmatricula
     */
    public function setst_urlportalmatricula($st_urlportalmatricula)
    {
        $this->st_urlportalmatricula = $st_urlportalmatricula;
        return $this;

    }

    /**
     * @return date
     */
    public function getDt_terminomatricula()
    {
        return $this->dt_terminomatricula;
    }

    /**
     * @param date $dt_terminomatricula
     */
    public function setDt_terminomatricula($dt_terminomatricula)
    {
        $this->dt_terminomatricula = $dt_terminomatricula;
    }

    /**
     * @return integer
     */
    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param integer $id_vendaproduto
     * @return \G2\Entity\VwMatricula
     */
    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
    }

    /**
     * @return integer
     */
    public function getId_situacaoagendamento()
    {
        return $this->id_situacaoagendamento;
    }

    /**
     * @param $id_situacaoagendamento integer
     */
    public function setId_situacaoagendamento($id_situacaoagendamento)
    {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
    }

    public function getSt_Email()
    {
        return $this->st_email;
    }

    public function setSt_Email($st_email)
    {
        $this->st_email = $st_email;
    }

    public function getDt_Terminoturma()
    {
        return $this->dt_terminoturma;
    }

    public function setDt_Terminoturma($dt_terminoturma)
    {
        $this->dt_terminoturma = $dt_terminoturma;
    }

    public function getSt_Telefone()
    {
        return $this->st_telefone;
    }

    public function setSt_Telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    public function getBl_Institucional()
    {
        return $this->bl_institucional;
    }

    public function setBl_Institucional($bl_institucional)
    {
        $this->bl_institucional = $bl_institucional;
    }

    /**
     * @return the $id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param field_type $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return the $st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param field_type $st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    /**
     * @return the $st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param field_type $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return the $dt_nascimento
     */
    public function getDt_nascimento()
    {
        return $this->dt_nascimento;
    }

    /**
     * @param field_type $dt_nascimento
     */
    public function setDt_nascimento($dt_nascimento)
    {
        $this->dt_nascimento = $dt_nascimento;
    }

    /**
     * @return the $st_nomepai
     */
    public function getSt_nomepai()
    {
        return $this->st_nomepai;
    }

    /**
     * @param field_type $st_nomepai
     */
    public function setSt_nomepai($st_nomepai)
    {
        $this->st_nomepai = $st_nomepai;
    }

    /**
     * @return the $st_nomemae
     */
    public function getSt_nomemae()
    {
        return $this->st_nomemae;
    }

    /**
     * @param field_type $st_nomemae
     */
    public function setSt_nomemae($st_nomemae)
    {
        $this->st_nomemae = $st_nomemae;
    }

    /**
     * @return the $id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param field_type $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return the $st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param field_type $st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    /**
     * @return the $id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param field_type $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @return the $st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param field_type $st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
    }

    /**
     * @return the $id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param field_type $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @return the $st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param field_type $st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
    }

    /**
     * @return the $dt_concluinte
     */
    public function getDt_concluinte()
    {
        return $this->dt_concluinte;
    }

    /**
     * @param field_type $dt_concluinte
     */
    public function setDt_concluinte($dt_concluinte)
    {
        $this->dt_concluinte = $dt_concluinte;
    }

    /**
     * @return \DateTime
     */
    public function getDt_colacao()
    {
        return $this->dt_colacao;
    }

    /**
     * @param \DateTime $dt_colacao
     * @return $this
     */
    public function setDt_colacao($dt_colacao)
    {
        $this->dt_colacao = $dt_colacao;
        return $this;
    }

    /**
     * @return the $id_entidadematriz
     */
    public function getId_entidadematriz()
    {
        return $this->id_entidadematriz;
    }

    /**
     * @param field_type $id_entidadematriz
     */
    public function setId_entidadematriz($id_entidadematriz)
    {
        $this->id_entidadematriz = $id_entidadematriz;
    }

    /**
     * @return the $st_entidadematriz
     */
    public function getSt_entidadematriz()
    {
        return $this->st_entidadematriz;
    }

    /**
     * @param field_type $st_entidadematriz
     */
    public function setSt_entidadematriz($st_entidadematriz)
    {
        $this->st_entidadematriz = $st_entidadematriz;
    }

    /**
     * @return the $id_entidadematricula
     */
    public function getId_entidadematricula()
    {
        return $this->id_entidadematricula;
    }

    /**
     * @param field_type $id_entidadematricula
     */
    public function setId_entidadematricula($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
    }

    /**
     * @return the $st_entidadematricula
     */
    public function getSt_entidadematricula()
    {
        return $this->st_entidadematricula;
    }

    /**
     * @param field_type $st_entidadematricula
     */
    public function setSt_entidadematricula($st_entidadematricula)
    {
        $this->st_entidadematricula = $st_entidadematricula;
    }

    /**
     * @return the $id_entidadeatendimento
     */
    public function getId_entidadeatendimento()
    {
        return $this->id_entidadeatendimento;
    }

    /**
     * @param field_type $id_entidadeatendimento
     */
    public function setId_entidadeatendimento($id_entidadeatendimento)
    {
        $this->id_entidadeatendimento = $id_entidadeatendimento;
    }

    /**
     * @return the $st_entidadeatendimento
     */
    public function getSt_entidadeatendimento()
    {
        return $this->st_entidadeatendimento;
    }

    /**
     * @param field_type $st_entidadeatendimento
     */
    public function setSt_entidadeatendimento($st_entidadeatendimento)
    {
        $this->st_entidadeatendimento = $st_entidadeatendimento;
    }

    /**
     * @return the $bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param field_type $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return the $id_contrato
     */
    public function getId_contrato()
    {
        return $this->id_contrato;
    }

    /**
     * @param field_type $id_contrato
     */
    public function setId_contrato($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @return the $id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param field_type $id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    /**
     * @return the $dt_termino
     */
    public function getDt_termino()
    {
        return $this->dt_termino;
    }

    /**
     * @param field_type $dt_termino
     */
    public function setDt_termino($dt_termino)
    {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return the $dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param field_type $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return the $dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param field_type $dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return the $id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param field_type $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return the $st_turma
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param field_type $st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
    }

    /**
     * @return the $dt_inicioturma
     */
    public function getDt_inicioturma()
    {
        return $this->dt_inicioturma;
    }

    /**
     * @param field_type $dt_inicioturma
     */
    public function setDt_inicioturma($dt_inicioturma)
    {
        $this->dt_inicioturma = $dt_inicioturma;
    }

    /**
     * @return mixed
     */
    public function getSt_urlacesso()
    {
        return $this->st_urlacesso;
    }

    /**
     * @param mixed $st_urlacesso
     */
    public function setSt_urlacesso($st_urlacesso)
    {
        $this->st_urlacesso = $st_urlacesso;
    }

    function getId_venda()
    {
        return $this->id_venda;
    }

    function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    function getSt_urlportal()
    {
        return $this->st_urlportal;
    }

    function setSt_urlportal($st_urlportal)
    {
        $this->st_urlportal = $st_urlportal;
    }

    public function getBl_documentacao()
    {
        return $this->bl_documentacao;
    }

    public function setBl_documentacao($bl_documentacao)
    {
        $this->bl_documentacao = $bl_documentacao;
    }

    /**
     * @return string st_codcertificacao
     */
    public function getSt_codcertificacao()
    {
        return $this->st_codcertificacao;
    }

    /**
     * @param $st_codcertificacao
     * @return $this
     */
    public function setSt_codcertificacao($st_codcertificacao)
    {
        $this->st_codcertificacao = $st_codcertificacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_identificacao()
    {
        return $this->st_identificacao;
    }

    /**
     * @param $st_identificacao
     * @return $this
     */
    public function setSt_identificacao($st_identificacao)
    {
        $this->st_identificacao = $st_identificacao;
        return $this;
    }

    /**
     * @return integer id_indicecertificado
     */
    public function getId_indicecertificado()
    {
        return $this->id_indicecertificado;
    }

    /**
     * @param id_indicecertificado
     */
    public function setId_indicecertificado($id_indicecertificado)
    {
        $this->id_indicecertificado = $id_indicecertificado;
        return $this;
    }

    /**
     * @return string st_telefonealternativo
     */
    public function getSt_telefonealternativo()
    {
        return $this->st_telefonealternativo;
    }

    /**
     * @param st_telefonealternativo
     */
    public function setSt_telefonealternativo($st_telefonealternativo)
    {
        $this->st_telefonealternativo = $st_telefonealternativo;
        return $this;
    }

    /**
     * @return string st_dtcadastrocertificacao
     */
    public function getSt_dtcadastrocertificacao()
    {
        return $this->st_dtcadastrocertificacao;
    }

    /**
     * @param st_dtcadastrocertificacao
     */
    public function setSt_dtcadastrocertificacao($st_dtcadastrocertificacao)
    {
        $this->st_dtcadastrocertificacao = $st_dtcadastrocertificacao;
        return $this;
    }

    /**
     * @return string st_dtenviocertificado
     */
    public function getSt_dtenviocertificado()
    {
        return $this->st_dtenviocertificado;
    }

    /**
     * @param st_dtenviocertificado
     */
    public function setSt_dtenviocertificado($st_dtenviocertificado)
    {
        $this->st_dtenviocertificado = $st_dtenviocertificado;
        return $this;
    }

    /**
     * @return string st_dtretornocertificadora
     */
    public function getSt_dtretornocertificadora()
    {
        return $this->st_dtretornocertificadora;
    }

    /**
     * @param st_dtretornocertificadora
     */
    public function setSt_dtretornocertificadora($st_dtretornocertificadora)
    {
        $this->st_dtretornocertificadora = $st_dtretornocertificadora;
        return $this;
    }

    /**
     * @return string st_dtenvioaluno
     */
    public function getSt_dtenvioaluno()
    {
        return $this->st_dtenvioaluno;
    }

    /**
     * @param st_dtenvioaluno
     */
    public function setSt_dtenvioaluno($st_dtenvioaluno)
    {
        $this->st_dtenvioaluno = $st_dtenvioaluno;
        return $this;
    }

    /**
     * @return string st_codigoacompanhamento
     */
    public function getSt_codigoacompanhamento()
    {
        return $this->st_codigoacompanhamento;
    }

    /**
     * @param st_codigoacompanhamento
     */
    public function setSt_codigoacompanhamento($st_codigoacompanhamento)
    {
        $this->st_codigoacompanhamento = $st_codigoacompanhamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_urlavatar()
    {
        return $this->st_urlavatar;
    }

    /**
     * @param $st_urlavatar
     * @return $this
     */
    public function setSt_urlavatar($st_urlavatar)
    {
        $this->st_urlavatar = $st_urlavatar;
        return $this;
    }

    public function getBl_disciplinacomplementar()
    {
        return $this->bl_disciplinacomplementar;
    }

    public function setBl_disciplinacomplementar($bl_disciplinacomplementar)
    {
        $this->bl_disciplinacomplementar = $bl_disciplinacomplementar;
    }

    public function getId_matriculavinculada()
    {
        return $this->id_matriculavinculada;
    }

    public function setId_matriculavinculada($id_matriculavinculada)
    {
        $this->id_matriculavinculada = $id_matriculavinculada;
    }

    /**
     * @return int
     */
    public function getId_tiporegracontrato()
    {
        return $this->id_tiporegracontrato;
    }

    /**
     * @param int $id_tiporegracontrato
     */
    public function setId_tiporegracontrato($id_tiporegracontrato)
    {
        $this->id_tiporegracontrato = $id_tiporegracontrato;
    }

    public function getBl_academico()
    {
        return $this->bl_academico;
    }

    public function setBl_academico($bl_academico)
    {
        $this->bl_academico = $bl_academico;
    }

    public function getDt_cadastrocertificacao()
    {
        return $this->dt_cadastrocertificacao;
    }

    public function setDt_cadastrocertificacao($dt_cadastrocertificacao)
    {
        $this->dt_cadastrocertificacao = $dt_cadastrocertificacao;
    }

    public function getDt_enviocertificado()
    {
        return $this->dt_enviocertificado;
    }

    public function setDt_enviocertificado($dt_enviocertificado)
    {
        $this->dt_enviocertificado = $dt_enviocertificado;
    }

    public function getDt_retornocertificadora()
    {
        return $this->dt_retornocertificadora;
    }

    public function setDt_retornocertificadora($dt_retornocertificadora)
    {
        $this->dt_retornocertificadora = $dt_retornocertificadora;
    }

    public function getDt_envioaluno()
    {
        return $this->dt_envioaluno;
    }

    public function setDt_envioaluno($dt_envioaluno)
    {
        $this->dt_envioaluno = $dt_envioaluno;
    }

    public function getSt_uploadcontrato()
    {
        return $this->st_uploadcontrato;
    }

    public function setSt_uploadcontrato($st_uploadcontrato)
    {
        $this->st_uploadcontrato = $st_uploadcontrato;
    }

    /**
     * @return int
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param int $id_contratoregra
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }


    /**
     * @return int
     */
    public function getId_aproveitamento()
    {
        return $this->id_aproveitamento;
    }

    /**
     * @param int $id_aproveitamento
     */
    public function setId_aproveitamento($id_aproveitamento)
    {
        $this->id_aproveitamento = $id_aproveitamento;
    }

    /**
     * @return int
     */
    public function getSt_codigo()
    {
        return $this->st_codigo;
    }

    /**
     * @param int $st_codigo
     */
    public function setSt_codigo($st_codigo)
    {
        $this->st_codigo = $st_codigo;
    }

    /**
     * @return int
     */
    public function getSt_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param int $st_descricao
     */
    public function setSt_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
    }

    /**
     * @return int
     */
    public function getSt_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param int $st_imagem
     */
    public function setSt_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
    }

    /**
     * @return int
     */
    public function getId_situacaoagendamentoparcial()
    {
        return $this->id_situacaoagendamentoparcial;
    }

    /**
     * @param int $id_situacaoagendamentoparcial
     */
    public function setId_situacaoagendamentoparcial($id_situacaoagendamentoparcial)
    {
        $this->id_situacaoagendamentoparcial = $id_situacaoagendamentoparcial;
    }

    /**
     * @return int
     */
    public function getId_turmaorigem()
    {
        return $this->id_turmaorigem;
    }

    /**
     * @param int $id_turmaorigem
     * @return $this
     */
    public function setId_turmaorigem($id_turmaorigem)
    {
        $this->id_turmaorigem = $id_turmaorigem;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_novoportal()
    {
        return $this->bl_novoportal;
    }

    /**
     * @param bool $bl_novoportal
     */
    public function setBl_novoportal($bl_novoportal)
    {
        $this->bl_novoportal = $bl_novoportal;
    }

    /**
     * @return string
     */
    public function getNu_anomatricula()
    {
        return $this->nu_anomatricula;
    }

    public function setNu_anomatricula($nu_anomatricula)
    {
        $this->nu_anomatricula = $nu_anomatricula;
    }
    /**
     * @return int
     */
    public function getId_esquemaconfiguracao()
    {
        return $this->id_esquemaconfiguracao;
    }

    public function setId_esquemaconfiguracao($id_esquemaconfiguracao)
    {
        $this->id_esquemaconfiguracao = $id_esquemaconfiguracao;
    }

    /**
     * @return string
     */
    public function getSt_urlnovoportal()
    {
        return $this->st_urlnovoportal;
    }

    /**
     * @param string $st_urlnovoportal
     */
    public function setSt_urlnovoportal($st_urlnovoportal)
    {
        $this->st_urlnovoportal = $st_urlnovoportal;
    }

    /**
     * @return int
     */
    public function getNu_semestreatual()
    {
        return $this->nu_semestreatual;
    }
    /**
     * @return int
     */
    public function getId_cancelamento()
    {
        return $this->id_cancelamento;
    }

    /**
     * @param int $id_cancelamento
     */
    public function setId_cancelamento($id_cancelamento)
    {
        $this->id_cancelamento = $id_cancelamento;
    }



    /**
     * @param int $nu_semestreatual
     * @return $this
     */
    public function setNu_semestreatual($nu_semestreatual)
    {
        $this->nu_semestreatual = $nu_semestreatual;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param \DateTime $dt_ultimaoferta
     * @return $this
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ultimoacesso()
    {
        return $this->dt_ultimoacesso;
    }

    /**
     * @param \DateTime $dt_ultimoacesso
     * @return $this
     */
    public function setDt_ultimoacesso($dt_ultimoacesso)
    {
        $this->dt_ultimoacesso = $dt_ultimoacesso;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_tituloexibicao()
    {
        return $this->st_tituloexibicao;
    }

    /**
     * @param $st_tituloexibicao
     * @return $this
     */
    public function setSt_tituloexibicao($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }



}

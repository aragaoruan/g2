<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_contaentidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class ContaEntidade
{

    /**
     *
     * @var integer $id_contaentidade
     * @Column(name="id_contaentidade", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_contaentidade;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var TipoConta $id_tipodeconta
     * @ManyToOne(targetEntity="TipoConta")
     * @JoinColumn(name="id_tipodeconta", nullable=false, referencedColumnName="id_tipodeconta")
     */
    private $id_tipodeconta;

    /**
     * @var Banco $st_banco
     * @ManyToOne(targetEntity="Banco")
     * @JoinColumn(name="st_banco", nullable=true, referencedColumnName="st_banco")
     */
    private $st_banco;

    /**
     * @var string $st_digitoagencia
     * @Column(name="st_digitoagencia", nullable=true, type="string", length=2)
     */
    private $st_digitoagencia;

    /**
     * @var string $st_agencia
     * @Column(name="st_agencia", nullable=true, type="string", length=20)
     */
    private $st_agencia;

    /**
     *
     * @var string $st_conta
     * @Column(name="st_conta", nullable=true, type="string", length=10)
     */
    private $st_conta;

    /**
     *
     * @var boolean $bl_padrao
     * @Column(name="bl_padrao", nullable=false, type="boolean")
     */
    private $bl_padrao;

    /**
     *
     * @var string $st_digitoconta
     * @Column(name="st_digitoconta", nullable=true, type="string", length=2)
     */
    private $st_digitoconta;

    public function getId_contaentidade ()
    {
        return $this->id_contaentidade;
    }

    public function setId_contaentidade ($id_contaentidade)
    {
        $this->id_contaentidade = $id_contaentidade;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_tipodeconta ()
    {
        return $this->id_tipodeconta;
    }

    public function setId_tipodeconta (TipoConta $id_tipodeconta)
    {
        $this->id_tipodeconta = $id_tipodeconta;
        return $this;
    }

    public function getSt_banco ()
    {
        return $this->st_banco;
    }

    public function setSt_banco (Banco $st_banco)
    {
        $this->st_banco = $st_banco;
        return $this;
    }

    public function getSt_digitoagencia ()
    {
        return $this->st_digitoagencia;
    }

    public function setSt_digitoagencia ($st_digitoagencia)
    {
        $this->st_digitoagencia = $st_digitoagencia;
        return $this;
    }

    public function getSt_agencia ()
    {
        return $this->st_agencia;
    }

    public function setSt_agencia ($st_agencia)
    {
        $this->st_agencia = $st_agencia;
        return $this;
    }

    public function getSt_conta ()
    {
        return $this->st_conta;
    }

    public function setSt_conta ($st_conta)
    {
        $this->st_conta = $st_conta;
        return $this;
    }

    public function getBl_padrao ()
    {
        return $this->bl_padrao;
    }

    public function setBl_padrao ($bl_padrao)
    {
        $this->bl_padrao = $bl_padrao;
        return $this;
    }

    public function getSt_digitoconta ()
    {
        return $this->st_digitoconta;
    }

    public function setSt_digitoconta ($st_digitoconta)
    {
        $this->st_digitoconta = $st_digitoconta;
        return $this;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

}

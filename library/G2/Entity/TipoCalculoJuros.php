<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipocalculojuros")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoCalculoJuros
{

    /**
     * @var integer $id_tipocalculojuros
     * @Column(name="id_tipocalculojuros", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipocalculojuros;

    /**
     * @var string $st_tipocalculojuros
     * @Column(name="st_tipocalculojuros", type="string", nullable=false, length=100)
     */
    private $st_tipocalculojuros;

    public function getId_tipocalculojuros ()
    {
        return $this->id_tipocalculojuros;
    }

    public function setId_tipocalculojuros ($id_tipocalculojuros)
    {
        $this->id_tipocalculojuros = $id_tipocalculojuros;
    }

    public function getSt_tipocalculojuros ()
    {
        return $this->st_tipocalculojuros;
    }

    public function setSt_tipocalculojuros ($st_tipocalculojuros)
    {
        $this->st_tipocalculojuros = $st_tipocalculojuros;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_disciplina")
 * @Entity(repositoryClass="\G2\Repository\Disciplina")
 * @EntityLog
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> - 2014-12-18
 */
use Doctrine\Common\Collections\ArrayCollection;
use G2\G2Entity;

class Disciplina extends G2Entity
{

    /**
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(type="string", length=255, nullable=false, name="st_disciplina")
     */
    private $st_disciplina;

    /**
     * @var text $st_descricao
     * @Column(type="text", nullable=true, name="st_descricao")
     */
    private $st_descricao;

    /**
     * @var string $nu_identificador
     * @Column(type="string",length=255, nullable=true, name="nu_identificador")
     */
    private $nu_identificador;

    /**
     * @var decimal $nu_cargahoraria
     * @Column(type="decimal", nullable=false, name="nu_cargahoraria")
     */
    private $nu_cargahoraria;

    /**
     * @var decimal $nu_creditos
     * @Column(type="decimal", nullable=true, name="nu_creditos")
     */
    private $nu_creditos;

    /**
     * @var string $nu_codigoparceiro
     * @Column(type="string", length=255, nullable=true, name="nu_codigoparceiro")
     */
    private $nu_codigoparceiro;

    /**
     * @var boolean $bl_ativa
     * @Column(type="boolean",nullable=false, name="bl_ativa")
     */
    private $bl_ativa;

    /**
     * @var boolean $bl_compartilhargrupo
     * @Column(type="boolean", nullable=false, name="bl_compartilhargrupo")
     */
    private $bl_compartilhargrupo;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao", nullable=false)
     */
    private $id_situacao;

    /**
     * @var string $st_tituloexibicao
     * @Column(type="string", length=255, nullable=true, name="st_tituloexibicao")
     */
    private $st_tituloexibicao;

    /**
     * @var integer $id_tipodisciplina
     * @Column(type="integer", nullable=false, name="id_tipodisciplina")
     */
    private $id_tipodisciplina;

    /**
     * @var integer $st_apelido
     * @Column(type="string", length=15, nullable=true, name="st_apelido")
     */
    private $st_apelido;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuariocadastro
     * @Column(type="integer", nullable=false, name="id_usuariocadastro")
     */
    private $id_usuariocadastro;

    /**
     * @var string $st_identificador
     * @Column(type="string",length=30, nullable=true, name="st_identificador")
     */
    private $st_identificador;


    /**
     * @var string $st_imagem
     * @Column(type="string",length=200, nullable=true, name="st_imagem")
     */
    private $st_imagem;

    /**
     * @var text $st_ementacertificado
     * @Column(type="text",nullable=true, name="st_ementacertificado")
     */
    private $st_ementacertificado;

    /**
     * @var boolean $bl_provamontada
     * @Column(type="boolean",nullable=true, name="bl_provamontada")
     */
    private $bl_provamontada;

    /**
     * @var integer $nu_repeticao
     * @Column(type="integer", nullable=true, name="nu_repeticao")
     */
    private $nu_repeticao;

    /**
     *
     * @var Uf $sg_uf
     * @ManyToOne(targetEntity="Uf")
     * @JoinColumn(name="sg_uf", nullable=true, referencedColumnName="sg_uf")
     */
    private $sg_uf;

    /**
     * @var $salaDisciplina
     * @ManyToMany(targetEntity="SalaDeAula", mappedBy="salaDeAulaDisciplina")
     */
    private $salaDisciplina;

    /**
     * @var GrupoDisciplina $id_grupodisciplina
     * @ManyToOne(targetEntity="GrupoDisciplina")
     * @JoinColumn(name="id_grupodisciplina", referencedColumnName="id_grupodisciplina", nullable=true)
     */
    private $id_grupodisciplina;

    /**
     * @var string $st_codprovaintegracao
     * @Column(type="string",length=30, nullable=true, name="st_codprovaintegracao")
     */
    private $st_codprovaintegracao;

    /**
     * @var string $st_codprovarecuperacaointegracao
     * @Column(type="string",length=30, nullable=true, name="st_codprovarecuperacaointegracao")
     */
    private $st_codprovarecuperacaointegracao;

    /**
     * @var string $st_provaintegracao
     * @Column(type="string",length=255, nullable=true, name="st_provaintegracao")
     */
    private $st_provaintegracao;

    /**
     * @var string $st_provarecuperacaointegracao
     * @Column(type="string",length=255, nullable=true, name="st_provarecuperacaointegracao")
     */
    private $st_provarecuperacaointegracao;

    /**
     * @var boolean $bl_disciplinasemestre
     * @Column(type="boolean",nullable=false, name="bl_disciplinasemestre")
     */
    private $bl_disciplinasemestre;

    /**
     * @var boolean $bl_coeficienterendimento
     * @Column(name="bl_coeficienterendimento", type="boolean", nullable=true)
     */
    private $bl_coeficienterendimento;

    /**
     * @var boolean $bl_cargahorariaintegralizada
     * @Column(name="bl_cargahorariaintegralizada", type="boolean", nullable=true)
     */
    private $bl_cargahorariaintegralizada;

    public function __construct ()
    {
        $this->salaDisciplina = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getst_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param string $st_imagem
     */
    public function setst_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
        return $this;

    }



    /**
     *
     * @return ArrayCollection salaDisciplina
     */
    public function getSalaDisciplina ()
    {
        return $this->salaDisciplina;
    }

    /**
     *
     *
     * @return integer id_disciplina
     */
    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    /**
     *
     * @return string st_disciplina
     */
    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    /**
     *
     * @return string st_descricao
     */
    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    /**
     *
     * @return integer nu_identificador
     */
    public function getNu_identificador ()
    {
        return $this->nu_identificador;
    }

    /**
     *
     * @return integer nu_cargahoraria
     */
    public function getNu_cargahoraria ()
    {
        return $this->nu_cargahoraria;
    }

    /**
     *
     * @return integer nu_creditos
     */
    public function getNu_creditos ()
    {
        return $this->nu_creditos;
    }

    /**
     *
     * @return integer nu_codigoparceiro
     */
    public function getNu_codigoparceiro ()
    {
        return $this->nu_codigoparceiro;
    }

    /**
     *
     * @return boolean bl_ativa
     */
    public function getBl_ativa ()
    {
        return $this->bl_ativa;
    }

    /**
     *
     * @return boolean bl_compartilhargrupo
     */
    public function getBl_compartilhargrupo ()
    {
        return $this->bl_compartilhargrupo;
    }

    /**
     *
     * @return \G2\Entity\Situacao id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     *
     * @return string st_tituloexibicao
     */
    public function getSt_tituloexibicao ()
    {
        return $this->st_tituloexibicao;
    }

    /**
     *
     * @return integer id_tipodisciplina
     */
    public function getId_tipodisciplina ()
    {
        return $this->id_tipodisciplina;
    }

    /**
     *
     * @return integer
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     *
     * @return datetime2
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     *
     * @return integer
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     *
     * @return string st_identificador
     */
    public function getSt_identificador ()
    {
        return $this->st_identificador;
    }

    /**
     *
     * @return string st_identificador
     */
    public function getSt_ementacertificado ()
    {
        return $this->st_ementacertificado;
    }

    /**
     *
     * @return boolean bl_provamontada
     */
    public function getBl_provamontada ()
    {
        return $this->bl_provamontada;
    }

    /**
     *
     * @return integer nu_repeticao
     */
    public function getNu_repeticao ()
    {
        return $this->nu_repeticao;
    }

    /**
     *
     * @return \G2\Entity\Uf sg_uf
     */
    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    /**
     *
     * @return \G2\Entity\Disciplina
     */
    public function getSt_apelido ()
    {
        return $this->st_apelido;
    }

    /**
     *
     * @param string $id_disciplina
     * @return \G2\Entity\Disciplina
     */
    public function setSt_apelido ($st_apelido)
    {
        $this->st_apelido = $st_apelido;
        return $this;
    }

    /**
     *
     * @param string $id_disciplina
     * @return \G2\Entity\Disciplina
     */
    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     *
     * @param string $st_disciplina
     * @return \G2\Entity\Disciplina
     */
    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     *
     * @param string $st_descricao
     * @return \G2\Entity\Disciplina
     */
    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    /**
     *
     * @param integer $nu_identificador
     * @return \G2\Entity\Disciplina
     */
    public function setNu_identificador ($nu_identificador)
    {
        $this->nu_identificador = $nu_identificador;
        return $this;
    }

    /**
     *
     * @param integer $nu_cargahoraria
     * @return \G2\Entity\Disciplina
     */
    public function setNu_cargahoraria ($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     *
     * @param integer $nu_creditos
     * @return \G2\Entity\Disciplina
     */
    public function setNu_creditos ($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
        return $this;
    }

    /**
     *
     * @param integer $nu_codigoparceiro
     * @return \G2\Entity\Disciplina
     */
    public function setNu_codigoparceiro ($nu_codigoparceiro)
    {
        $this->nu_codigoparceiro = $nu_codigoparceiro;
        return $this;
    }

    /**
     *
     * @param boolean $bl_ativa
     * @return \G2\Entity\Disciplina
     */
    public function setBl_ativa ($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    /**
     *
     * @param boolean $bl_compartilhargrupo
     * @return \G2\Entity\Disciplina
     */
    public function setBl_compartilhargrupo ($bl_compartilhargrupo)
    {
        $this->bl_compartilhargrupo = $bl_compartilhargrupo;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\Disciplina
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function setSt_tituloexibicao ($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    /**
     *
     * @param integer $id_tipodisciplina
     * @return \G2\Entity\Disciplina
     */
    public function setId_tipodisciplina ($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
        return $this;
    }

    /**
     *
     * @param integer $id_entidade
     * @return \G2\Entity\Disciplina
     */
    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     *
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\Disciplina
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     *
     * @param integer $id_usuariocadastro
     * @return \G2\Entity\Disciplina
     */
    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     *
     * @param string $st_identificador
     * @return \G2\Entity\Disciplina
     */
    public function setSt_identificador ($st_identificador)
    {
        $this->st_identificador = $st_identificador;
        return $this;
    }

    /**
     *
     * @param string $st_ementacertificado
     * @return \G2\Entity\Disciplina
     */
    public function setSt_ementacertificado ($st_ementacertificado)
    {
        $this->st_ementacertificado = $st_ementacertificado;
        return $this;
    }

    /**
     *
     * @param boolean $bl_provamontada
     * @return \G2\Entity\Disciplina
     */
    public function setBl_provamontada ($bl_provamontada)
    {
        $this->bl_provamontada = $bl_provamontada;
        return $this;
    }

    /**
     *
     * @param integer $nu_repeticao
     * @return \G2\Entity\Disciplina
     */
    public function setNu_repeticao ($nu_repeticao)
    {
        $this->nu_repeticao = $nu_repeticao;
        return $this;
    }

    /**
     *
     * @param \G2\Entity\Uf $sg_uf
     * @return \G2\Entity\Disciplina
     */
    public function setSg_uf (Uf $sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return \G2\Entity\GrupoDisciplina
     */
    public function getid_grupodisciplina()
    {
        return $this->id_grupodisciplina;
    }

    /**
     * @param \G2\Entity\GrupoDisciplina $id_grupodisciplina
     * @return Disciplina
     */
    public function setid_grupodisciplina($id_grupodisciplina)
    {
        $this->id_grupodisciplina = $id_grupodisciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_codprovaintegracao()
    {
        return $this->st_codprovaintegracao;
    }

    /**
     * @param string $st_codprovaintegracao
     */
    public function setSt_codprovaintegracao($st_codprovaintegracao)
    {
        $this->st_codprovaintegracao = $st_codprovaintegracao;
    }

    /**
     * @return string
     */
    public function getSt_codprovarecuperacaointegracao()
    {
        return $this->st_codprovarecuperacaointegracao;
    }

    /**
     * @param string $st_codprovarecuperacaointegracao
     */
    public function setSt_codprovarecuperacaointegracao($st_codprovarecuperacaointegracao)
    {
        $this->st_codprovarecuperacaointegracao = $st_codprovarecuperacaointegracao;
    }

    /**
     * @return string
     */
    public function getSt_provaintegracao()
    {
        return $this->st_provaintegracao;
    }

    /**
     * @param string $st_provaintegracao
     */
    public function setSt_provaintegracao($st_provaintegracao)
    {
        $this->st_provaintegracao = $st_provaintegracao;
    }

    /**
     * @return string
     */
    public function getSt_provarecuperacaointegracao()
    {
        return $this->st_provarecuperacaointegracao;
    }

    /**
     * @param string $st_provarecuperacaointegracao
     */
    public function setSt_provarecuperacaointegracao($st_provarecuperacaointegracao)
    {
        $this->st_provarecuperacaointegracao = $st_provarecuperacaointegracao;
    }

    /**
     * @return bool
     */
    public function getBl_disciplinasemestre()
    {
        return $this->bl_disciplinasemestre;
    }

    public function setBl_disciplinasemestre($bl_disciplinasemestre)
    {
        $this->bl_disciplinasemestre = $bl_disciplinasemestre;
    }

    /**
     * @return bool
     */
    public function getBl_coeficienterendimento()
    {
        return $this->bl_coeficienterendimento;
    }

    /**
     * @param bool $bl_coeficienterendimento
     */
    public function setBl_coeficienterendimento($bl_coeficienterendimento)
    {
        $this->bl_coeficienterendimento = $bl_coeficienterendimento;
    }

    /**
     * @return bool
     */
    public function getBl_cargahorariaintegralizada()
    {
        return $this->bl_cargahorariaintegralizada;
    }

    /**
     * @param bool $bl_cargahorariaintegralizada
     */
    public function setBl_cargahorariaintegralizada($bl_cargahorariaintegralizada)
    {
        $this->bl_cargahorariaintegralizada = $bl_cargahorariaintegralizada;
    }

}

<?php

namespace G2\Entity;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Entity(repositoryClass="\G2\Repository\ComissaoLancamento")
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_comissaoreceber")
 */
class VwComissaoReceber
{
    /**
     * @var datetime2 $dt_confirmacao
     * @Column(name="dt_confirmacao", type="datetime2", nullable=true, length=8)
     */
    private $dt_confirmacao;
    /**
     * @GeneratedValue(strategy="NONE")
     * @Id
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @GeneratedValue(strategy="NONE")
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_funcao
     * @Column(name="id_funcao", type="integer", nullable=false, length=4)
     */
    private $id_funcao;
    /**
     * @GeneratedValue(strategy="NONE")
     * @Id
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @GeneratedValue(strategy="NONE")
     * @Id
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=false, length=4)
     */
    private $id_vendaproduto;
    /**
     * @GeneratedValue(strategy="NONE")
     * @Id
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var decimal $nu_comissaoreceber
     * @Column(name="nu_comissaoreceber", type="decimal", nullable=true, length=9)
     */
    private $nu_comissaoreceber;
    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_funcao
     * @Column(name="st_funcao", type="string", nullable=false, length=5)
     */
    private $st_funcao;
    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;


    /**
     * @return datetime2 dt_confirmacao
     */
    public function getDt_confirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @param dt_confirmacao
     */
    public function setDt_confirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_funcao
     */
    public function getId_funcao()
    {
        return $this->id_funcao;
    }

    /**
     * @param id_funcao
     */
    public function setId_funcao($id_funcao)
    {
        $this->id_funcao = $id_funcao;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_vendaproduto
     */
    public function getId_vendaproduto()
    {
        return $this->id_vendaproduto;
    }

    /**
     * @param id_vendaproduto
     */
    public function setId_vendaproduto($id_vendaproduto)
    {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return decimal nu_comissaoreceber
     */
    public function getNu_comissaoreceber()
    {
        return $this->nu_comissaoreceber;
    }

    /**
     * @param nu_comissaoreceber
     */
    public function setNu_comissaoreceber($nu_comissaoreceber)
    {
        $this->nu_comissaoreceber = $nu_comissaoreceber;
        return $this;
    }

    /**
     * @return decimal nu_valorliquido
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_funcao
     */
    public function getSt_funcao()
    {
        return $this->st_funcao;
    }

    /**
     * @param st_funcao
     */
    public function setSt_funcao($st_funcao)
    {
        $this->st_funcao = $st_funcao;
        return $this;
    }

    /**
     * @return string st_disciplina
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @param st_disciplina
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }


}

<?php

namespace G2\Entity;

use Doctrine\ORM\Mapping\ManyToOne;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_holdingfiliada")
 * @Entity(repositoryClass="\G2\Repository\Holding")
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class HoldingFiliada
{
    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_holdingfiliada
     * @Column(name="id_holdingfiliada", type="integer", nullable=false, length=4)
     */
    private $id_holdingfiliada;
    /**
     * @ManyToOne(targetEntity="Holding")
     * @JoinColumn(name="id_holding", referencedColumnName="id_holding")
     */
    private $id_holding;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false, length=8)
     */
    private $dt_cadastro;
    /**
     * @var boolean $id_situacao
     * @Column(name="id_situacao", type="boolean", nullable=false, length=1)
     */
    private $id_situacao;
    /**
     * @var boolean $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="boolean", nullable=false, length=1)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_emiterelatorioholding
     * @Column(name="bl_emiterelatorioholding", type="boolean",nullable=true, length=1)
     */
    private $bl_emiterelatorioholding;

    /**
     * @return integer id_holdingfiliada
     */
    public function getId_holdingfiliada()
    {
        return $this->id_holdingfiliada;
    }

    /**
     * @param id_holdingfiliada
     */
    public function setId_holdingfiliada($id_holdingfiliada)
    {
        $this->id_holdingfiliada = $id_holdingfiliada;
        return $this;
    }

    /**
     * @return integer id_holding
     */
    public function getId_holding()
    {
        return $this->id_holding;
    }

    /**
     * @param id_holding
     */
    public function setId_holding($id_holding)
    {
        $this->id_holding = $id_holding;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }
    /**
     * @return boolean id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean bl_emiterelatorioholding
     */
    public function getBl_emiterelatorioholding()
    {
        return $this->bl_emiterelatorioholding;
    }
    /**
     * @param $bl_emiterelatorioholding
     */
    public function setBl_emiterelatorioholding($bl_emiterelatorioholding)
    {
        $this->bl_emiterelatorioholding = $bl_emiterelatorioholding;
    }

}
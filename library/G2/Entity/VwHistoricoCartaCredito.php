<?php
namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_historicocartacredito")
 * @Entity
 * @EntityView
 * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
 */

class VwHistoricoCartaCredito
{
    /**
     * @var integer id_cartacredito
     * @Column(name="id_cartacredito", type="integer", nullable=false)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_cartacredito;

    /**
     * @var integer id_venda
     * @Id
     * @Column(name="id_venda",type="integer",nullable=false)
     */
    private $id_venda;

    /**
     * @var integer id_produto
     * @Column(name="id_produto",type="integer", nullable=false)
     */
    private $id_produto;

    /**
     * @var string st_produto
     * @Column(name="st_produto",type="string",nullable=false)
     */
    private $st_produto;

    /**
     * @var string dt_utilizacao
     * @Column(name="dt_utilizacao",type="string",nullable=false)
     */
    private $dt_utilizacao;

    /**
     * @var float nu_valororiginal
     * @Column(name="nu_valororiginal",type="float",nullable=false)
     */
    private $nu_valororiginal;

    /**
     * @var float nu_valorutilizado
     * @Column(name="nu_valorutilizado",type="float",nullable=false)
     */
    private $nu_valorutilizado;

    /**
     * @var float nu_valordisponivel
     * @Column(name="nu_valordisponivel",type="float", nullable=false)
     */
    private $nu_valordisponivel;

    /**
     * @var integer id_evolucao
     * @Column(name="id_evolucao",type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     * @var string st_evolucao
     * @Column(name="st_evolucao",type="integer",nullable=false)
     */
    private $st_evolucao;

    /**
     * @return int
     */
    public function getId_cartacredito()
    {
        return $this->id_cartacredito;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @return string
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @return string
     */
    public function getDt_utilizacao()
    {
        return $this->dt_utilizacao;
    }

    /**
     * @return float
     */
    public function getNu_valororiginal()
    {
        return $this->nu_valororiginal;
    }

    /**
     * @return float
     */
    public function getNu_valorutilizado()
    {
        return $this->nu_valorutilizado;
    }

    /**
     * @return float
     */
    public function getNu_valordisponivel()
    {
        return $this->nu_valordisponivel;
    }

    /**
     * @return int
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return string
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }
}
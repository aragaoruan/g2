<?php


namespace G2\Entity;

/**
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="vw_vendausuario")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */

class VwVendaUsuario
{

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_venda;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;


    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_prevenda
     * @Column(name="id_prevenda", type="integer", nullable=true, length=4)
     */
    private $id_prevenda;

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=true, length=4)
     */
    private $id_formapagamento;

    /**
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=true, length=4)
     */
    private $id_campanhacomercial;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_atendente
     * @Column(name="id_atendente", type="integer", nullable=false, length=4)
     */
    private $id_atendente;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false, length=4)
     */
    private $id_usuariocadastro;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=true, length=4)
     */
    private $nu_parcelas;

    /**
     * @var integer $id_categoriacampanha
     * @Column(name="id_categoriacampanha", type="integer", nullable=true, length=4)
     */
    private $id_categoriacampanha;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false, length=1)
     */
    private $bl_ativo;

    /**
     * @var boolean $bl_contrato
     * @Column(name="bl_contrato", type="boolean", nullable=false, length=1)
     */
    private $bl_contrato;

    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido;

    /**
     * @var decimal $nu_valorbruto
     * @Column(name="nu_valorbruto", type="decimal", nullable=false, length=17)
     */
    private $nu_valorbruto;

    /**
     * @var decimal $nu_descontoporcentagem
     * @Column(name="nu_descontoporcentagem", type="decimal", nullable=true, length=9)
     */
    private $nu_descontoporcentagem;

    /**
     * @var decimal $nu_descontovalor
     * @Column(name="nu_descontovalor", type="decimal", nullable=true, length=9)
     */
    private $nu_descontovalor;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", nullable=true, length=9)
     */
    private $nu_juros;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_formapagamento
     * @Column(name="st_formapagamento", type="string", nullable=true, length=255)
     */
    private $st_formapagamento;

    /**
     * @var string $st_campanhacomercial
     * @Column(name="st_campanhacomercial", type="string", nullable=true, length=255)
     */
    private $st_campanhacomercial;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_nomecompletocadastrador
     * @Column(name="st_nomecompletocadastrador", type="string", nullable=false, length=300)
     */
    private $st_nomecompletocadastrador;

    /**
     * @var string $st_linkloja
     * @Column(name="st_linkloja", type="string", nullable=true)
     */
    private $st_linkloja;

    /**
     * @var string $st_categoriacampanha
     * @Column(name="st_categoriacampanha", type="string", nullable=true, length=50)
     */
    private $st_categoriacampanha;

    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    function getId_usuario()
    {
        return $this->id_usuario;
    }

    function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    function getId_venda()
    {
        return $this->id_venda;
    }

    function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    function getId_entidade()
    {
        return $this->id_entidade;
    }

    function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    function getId_prevenda()
    {
        return $this->id_prevenda;
    }

    function setId_prevenda($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
        return $this;
    }

    function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    function getId_situacao()
    {
        return $this->id_situacao;
    }

    function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    function getId_atendente()
    {
        return $this->id_atendente;
    }

    function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
        return $this;
    }

    function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    function getId_categoriacampanha()
    {
        return $this->id_categoriacampanha;
    }

    function setId_categoriacampanha($id_categoriacampanha)
    {
        $this->id_categoriacampanha = $id_categoriacampanha;
        return $this;
    }

    function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    function getBl_contrato()
    {
        return $this->bl_contrato;
    }

    function setBl_contrato($bl_contrato)
    {
        $this->bl_contrato = $bl_contrato;
        return $this;
    }

    function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
        return $this;
    }

    function getNu_descontoporcentagem()
    {
        return $this->nu_descontoporcentagem;
    }

    function setNu_descontoporcentagem($nu_descontoporcentagem)
    {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
        return $this;
    }

    function getNu_descontovalor()
    {
        return $this->nu_descontovalor;
    }

    function setNu_descontovalor($nu_descontovalor)
    {
        $this->nu_descontovalor = $nu_descontovalor;
        return $this;
    }

    function getNu_juros()
    {
        return $this->nu_juros;
    }

    function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
        return $this;
    }

    function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    function getSt_cpf()
    {
        return $this->st_cpf;
    }

    function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    function getSt_formapagamento()
    {
        return $this->st_formapagamento;
    }

    function setSt_formapagamento($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
        return $this;
    }

    function getSt_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    function setSt_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    function getSt_situacao()
    {
        return $this->st_situacao;
    }

    function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    function getSt_nomecompletocadastrador()
    {
        return $this->st_nomecompletocadastrador;
    }

    function setSt_nomecompletocadastrador($st_nomecompletocadastrador)
    {
        $this->st_nomecompletocadastrador = $st_nomecompletocadastrador;
        return $this;
    }

    function getSt_linkloja()
    {
        return $this->st_linkloja;
    }

    function setSt_linkloja($st_linkloja)
    {
        $this->st_linkloja = $st_linkloja;
        return $this;
    }

    function getSt_categoriacampanha()
    {
        return $this->st_categoriacampanha;
    }

    function setSt_categoriacampanha($st_categoriacampanha)
    {
        $this->st_categoriacampanha = $st_categoriacampanha;
        return $this;
    }

    function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

}

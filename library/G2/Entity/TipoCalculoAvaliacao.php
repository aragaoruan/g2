<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipocalculoavaliacao")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class TipoCalculoAvaliacao {

    /**
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao",type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipocalculoavaliacao;
        /**
     * @Column(name="st_tipocalculoavaliacao", type="string", nullable=false, length=100)
     * @var string $st_tipocalculoavaliacao
     */
    private $st_tipocalculoavaliacao;
    
    public function getId_tipocalculoavaliacao() {
        return $this->id_tipocalculoavaliacao;
    }

    public function getSt_tipocalculoavaliacao() {
        return $this->st_tipocalculoavaliacao;
    }

    public function setId_tipocalculoavaliacao($id_tipocalculoavaliacao) {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
    }

    public function setSt_tipocalculoavaliacao($st_tipocalculoavaliacao) {
        $this->st_tipocalculoavaliacao = $st_tipocalculoavaliacao;
    }



}

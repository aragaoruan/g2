<?php

namespace G2\Entity;

/**
* @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
* @Table (name="vw_saladisciplinaturma")
* @Entity
* @EntityView
* @author Caio Eduardo <caio.teixeira@unyleya.com.br>
*/
class VwSalaDisciplinaTurma
{
    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;
    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;
    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;
    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;
    /**
     * @var integer $id_matricula
     * @Id
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     */
    private $id_disciplina;
    /**
     * @var integer $id_matriculadisciplina
     * @Id
     * @Column(name="id_matriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_matriculadisciplina;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     */
    private $id_alocacao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=true, length=4)
     */
    private $id_tipodisciplina;
    /**
     * @var integer $nu_alocados
     * @Column(name="nu_alocados", type="integer", nullable=true, length=4)
     */
    private $nu_alocados;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;


    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer nu_maxalunos
     */
    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    /**
     * @param nu_maxalunos
     */
    public function setNu_maxalunos($nu_maxalunos) {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    /**
     * @return integer id_projetopedagogico
     */
    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    /**
     * @param id_projetopedagogico
     */
    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return integer id_trilha
     */
    public function getId_trilha() {
        return $this->id_trilha;
    }

    /**
     * @param id_trilha
     */
    public function setId_trilha($id_trilha) {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula() {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return integer id_matriculadisciplina
     */
    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_alocacao
     */
    public function getId_alocacao() {
        return $this->id_alocacao;
    }

    /**
     * @param id_alocacao
     */
    public function setId_alocacao($id_alocacao) {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer nu_alocados
     */
    public function getNu_alocados() {
        return $this->nu_alocados;
    }

    /**
     * @param nu_alocados
     */
    public function setNu_alocados($nu_alocados) {
        $this->nu_alocados = $nu_alocados;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setid_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }



}
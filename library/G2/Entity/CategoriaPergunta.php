<?php

namespace G2\Entity;

/**
 * @Entity(repositoryClass="\G2\Repository\PerguntasFrequentes")
 * @Table(name="tb_categoriapergunta")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class CategoriaPergunta
{

    /**
     * @var integer $id_categoriapergunta
     * @Column(type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_categoriapergunta;

    /**
     * @var string $st_categoriapergunta
     * @Column(type="string", nullable=false)
     */
    private $st_categoriapergunta;

    /**
     * @var boolean $st_categoriapergunta
     * @Column(type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var EsquemaPergunta $id_esquemapergunta
     * @ManyToOne(targetEntity="EsquemaPergunta")
     * @JoinColumn(name="id_esquemapergunta", referencedColumnName="id_esquemapergunta")
     */
    private $id_esquemapergunta;

    /**
     * @var Entidade $id_entidadecadastro
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidadecadastro", referencedColumnName="id_entidade")
     */
    private $id_entidadecadastro;

    /**
     * @return int
     */
    public function getId_categoriapergunta()
    {
        return $this->id_categoriapergunta;
    }

    /**
     * @return string
     */
    public function getSt_categoriapergunta()
    {
        return $this->st_categoriapergunta;
    }

    /**
     * @return bool
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return bool
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return EsquemaPergunta
     */
    public function getId_esquemapergunta()
    {
        return $this->id_esquemapergunta;
    }

    /**
     * @param $id_categoriapergunta
     * @return $this
     */
    public function setId_categoriapergunta($id_categoriapergunta)
    {
        $this->id_categoriapergunta = $id_categoriapergunta;
        return $this;
    }

    /**
     * @param $st_categoriapergunta
     * @return $this
     */
    public function setSt_categoriapergunta($st_categoriapergunta)
    {
        $this->st_categoriapergunta = $st_categoriapergunta;
        return $this;
    }

    /**
     * @param bool $bl_ativo
     * @return $this
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @param $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param EsquemaPergunta $id_esquemapergunta
     * @return $this
     */
    public function setId_esquemapergunta(EsquemaPergunta $id_esquemapergunta)
    {
        $this->id_esquemapergunta = $id_esquemapergunta;
        return $this;
    }

    /**
     * @return Entidade
     */
    public function getId_entidadecadastro()
    {
        return $this->id_entidadecadastro;
    }

    /**
     * @param Entidade $id_entidadecadastro
     * @return $this
     */
    public function setId_entidadecadastro(Entidade $id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }
}

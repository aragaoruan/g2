<?php
/**
 * Created by PhpStorm.
 * User: Elcio Guimarães
 * Date: 09/10/2017
 * Time: 14:38
 */

namespace G2\Entity;

/**
 *
 * @SWG\Definition(@SWG\Xml(name="VwCampanhaComercial"))
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_campanhacomercial")
 * @Entity(repositoryClass="\G2\Repository\VwCampanhaComercial")
 * @EntityView
 * @author Elcio Guimarães <elcioguimaraes@gmail.com>
 */
class VwCampanhaComercial extends \G2\G2Entity
{

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_campanhacomercial
     * @Column(name="id_campanhacomercial", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_campanhacomercial;

    /**
     * @SWG\Property()
     * @var string $st_campanhacomercial
     * @Column(type="string", length=255, nullable=false, name="st_campanhacomercial")
     */
    private $st_campanhacomercial;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_aplicardesconto
     * @Column(name="bl_aplicardesconto", type="boolean", nullable=false)
     */
    private $bl_aplicardesconto;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_disponibilidade
     * @Column(name="bl_disponibilidade", type="boolean", nullable=false)
     */
    private $bl_disponibilidade;

    /**
     * @SWG\Property()
     * @var date $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @SWG\Property()
     * @var date $dt_fim
     * @Column(name="dt_fim", type="datetime", nullable=true, length=8)
     */
    private $dt_fim;

    /**
     * @SWG\Property()
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="datetime", nullable=true, length=8)
     */
    private $dt_inicio;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var string $st_nomeentidade
     * @Column(type="string", length=255, nullable=false, name="st_nomeentidade")
     */
    private $st_nomeentidade;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     * @SWG\Property()
     * @var string $st_situacao
     * @Column(type="string", length=255, nullable=false, name="st_situacao")
     */
    private $st_situacao;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tipodesconto
     * @Column(name="id_tipodesconto", type="integer", nullable=false)
     */
    private $id_tipodesconto;

    /**
     * @SWG\Property()
     * @var string $st_tipodesconto
     * @Column(type="string", length=255, nullable=false, name="st_tipodesconto")
     */
    private $st_tipodesconto;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_tipocampanha
     * @Column(name="id_tipocampanha", type="integer", nullable=false)
     */
    private $id_tipocampanha;

    /**
     * @SWG\Property()
     * @var string $st_tipocampanha
     * @Column(type="string", length=255, nullable=false, name="st_tipocampanha")
     */
    private $st_tipocampanha;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_disponibilidade
     * @Column(name="nu_disponibilidade", type="integer", nullable=false, length=4)
     */
    private $nu_disponibilidade;

    /**
     * @SWG\Property()
     * @var string $st_descricao
     * @Column(type="string", length=255, nullable=false, name="st_descricao")
     */
    private $st_descricao;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_todosprodutos
     * @Column(name="bl_todosprodutos", type="boolean", nullable=false)
     */
    private $bl_todosprodutos;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_todasformas
     * @Column(name="bl_todasformas", type="boolean", nullable=false)
     */
    private $bl_todasformas;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_categoriacampanha
     * @Column(name="id_categoriacampanha", type="integer", nullable=false)
     */
    private $id_categoriacampanha;

    /**
     * @SWG\Property()
     * @var string $st_categoriacampanha
     * @Column(type="string", length=255, nullable=false, name="st_categoriacampanha")
     */
    private $st_categoriacampanha;

    /**
     * @SWG\Property()
     * @var integer $nu_valordesconto
     * @Column(name="nu_valordesconto", type="decimal", nullable=true, length=17)
     */
    private $nu_valordesconto;

    /**
     * @SWG\Property(format="integer")
     * @var integer $nu_diasprazo
     * @Column(name="nu_diasprazo", type="integer", nullable=false, length=4)
     */
    private $nu_diasprazo;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_premio
     * @Column(name="id_premio", type="integer", nullable=false)
     */
    private $id_premio;

    /**
     * @SWG\Property()
     * @var string $st_prefixocupompremio
     * @Column(type="string", length=255, nullable=false, name="st_prefixocupompremio")
     */
    private $st_prefixocupompremio;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_finalidadecampanha
     * @Column(name="id_finalidadecampanha", type="integer", nullable=false)
     */
    private $id_finalidadecampanha;

    /**
     * @SWG\Property()
     * @var string $st_finalidadecampanha
     * @Column(type="string", length=255, nullable=false, name="st_finalidadecampanha")
     */
    private $st_finalidadecampanha;

    /**
     * @SWG\Property()
     * @var string $st_link
     * @Column(type="string", length=255, nullable=false, name="st_link")
     */
    private $st_link;

    /**
     * @SWG\Property()
     * @var string $st_imagem
     * @Column(type="string", length=255, nullable=false, name="st_imagem")
     */
    private $st_imagem;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_portaldoaluno
     * @Column(name="bl_portaldoaluno", type="boolean", nullable=false)
     */
    private $bl_portaldoaluno;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_mobile
     * @Column(name="bl_mobile", type="boolean", nullable=false)
     */
    private $bl_mobile;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_vigente
     * @Column(name="bl_vigente", type="boolean", nullable=false)
     */
    private $bl_vigente;

    /**
     * @SWG\Property()
     *
     * @var boolean $bl_visualizado
     * @Column(name="bl_visualizado", type="boolean", nullable=false)
     */
    private $bl_visualizado;

    /**
     * @SWG\Property(format="integer")
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;


    /**
     * @SWG\Property(format="integer")
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     * @return int
     */
    public function getid_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setid_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setid_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;

    }

    /**
     * @return bool
     */
    public function getBl_visualizado()
    {
        return $this->bl_visualizado;
    }

    /**
     * @param bool $bl_visualizado
     */
    public function setBl_visualizado($bl_visualizado)
    {
        $this->bl_visualizado = $bl_visualizado;
    }

    /**
     * @return int
     */
    public function getid_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param int $id_campanhacomercial
     */
    public function setid_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_campanhacomercial()
    {
        return $this->st_campanhacomercial;
    }

    /**
     * @param string $st_campanhacomercial
     */
    public function setst_campanhacomercial($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_aplicardesconto()
    {
        return $this->bl_aplicardesconto;
    }

    /**
     * @param mixed $bl_aplicardesconto
     */
    public function setbl_aplicardesconto($bl_aplicardesconto)
    {
        $this->bl_aplicardesconto = $bl_aplicardesconto;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param mixed $bl_ativo
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_disponibilidade()
    {
        return $this->bl_disponibilidade;
    }

    /**
     * @param bool $bl_disponibilidade
     */
    public function setbl_disponibilidade($bl_disponibilidade)
    {
        $this->bl_disponibilidade = $bl_disponibilidade;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param mixed $dt_cadastro
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getdt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param mixed $dt_fim
     */
    public function setdt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;

    }

    /**
     * @return date
     */
    public function getdt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param date $dt_inicio
     */
    public function setdt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param int $id_situacao
     */
    public function setid_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param string $st_situacao
     */
    public function setst_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipodesconto()
    {
        return $this->id_tipodesconto;
    }

    /**
     * @param int $id_tipodesconto
     */
    public function setid_tipodesconto($id_tipodesconto)
    {
        $this->id_tipodesconto = $id_tipodesconto;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipodesconto()
    {
        return $this->st_tipodesconto;
    }

    /**
     * @param string $st_tipodesconto
     */
    public function setst_tipodesconto($st_tipodesconto)
    {
        $this->st_tipodesconto = $st_tipodesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @param int $id_tipocampanha
     */
    public function setid_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_tipocampanha()
    {
        return $this->st_tipocampanha;
    }

    /**
     * @param string $st_tipocampanha
     */
    public function setst_tipocampanha($st_tipocampanha)
    {
        $this->st_tipocampanha = $st_tipocampanha;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_disponibilidade()
    {
        return $this->nu_disponibilidade;
    }

    /**
     * @param int $nu_disponibilidade
     */
    public function setnu_disponibilidade($nu_disponibilidade)
    {
        $this->nu_disponibilidade = $nu_disponibilidade;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_descricao()
    {
        return $this->st_descricao;
    }

    /**
     * @param string $st_descricao
     */
    public function setst_descricao($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_todosprodutos()
    {
        return $this->bl_todosprodutos;
    }

    /**
     * @param mixed $bl_todosprodutos
     */
    public function setbl_todosprodutos($bl_todosprodutos)
    {
        $this->bl_todosprodutos = $bl_todosprodutos;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_todasformas()
    {
        return $this->bl_todasformas;
    }

    /**
     * @param mixed $bl_todasformas
     */
    public function setbl_todasformas($bl_todasformas)
    {
        $this->bl_todasformas = $bl_todasformas;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getid_categoriacampanha()
    {
        return $this->id_categoriacampanha;
    }

    /**
     * @param mixed $id_categoriacampanha
     */
    public function setid_categoriacampanha($id_categoriacampanha)
    {
        $this->id_categoriacampanha = $id_categoriacampanha;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getst_categoriacampanha()
    {
        return $this->st_categoriacampanha;
    }

    /**
     * @param mixed $st_categoriacampanha
     */
    public function setst_categoriacampanha($st_categoriacampanha)
    {
        $this->st_categoriacampanha = $st_categoriacampanha;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_valordesconto()
    {
        return $this->nu_valordesconto;
    }

    /**
     * @param int $nu_valordesconto
     */
    public function setnu_valordesconto($nu_valordesconto)
    {
        $this->nu_valordesconto = $nu_valordesconto;
        return $this;

    }

    /**
     * @return int
     */
    public function getnu_diasprazo()
    {
        return $this->nu_diasprazo;
    }

    /**
     * @param int $nu_diasprazo
     */
    public function setnu_diasprazo($nu_diasprazo)
    {
        $this->nu_diasprazo = $nu_diasprazo;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_premio()
    {
        return $this->id_premio;
    }

    /**
     * @param int $id_premio
     */
    public function setid_premio($id_premio)
    {
        $this->id_premio = $id_premio;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_prefixocupompremio()
    {
        return $this->st_prefixocupompremio;
    }

    /**
     * @param string $st_prefixocupompremio
     */
    public function setst_prefixocupompremio($st_prefixocupompremio)
    {
        $this->st_prefixocupompremio = $st_prefixocupompremio;
        return $this;

    }

    /**
     * @return int
     */
    public function getid_finalidadecampanha()
    {
        return $this->id_finalidadecampanha;
    }

    /**
     * @param int $id_finalidadecampanha
     */
    public function setid_finalidadecampanha($id_finalidadecampanha)
    {
        $this->id_finalidadecampanha = $id_finalidadecampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_finalidadecampanha()
    {
        return $this->st_finalidadecampanha;
    }

    /**
     * @param string $st_finalidadecampanha
     */
    public function setst_finalidadecampanha($st_finalidadecampanha)
    {
        $this->st_finalidadecampanha = $st_finalidadecampanha;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_link()
    {
        return $this->st_link;
    }

    /**
     * @param string $st_link
     */
    public function setst_link($st_link)
    {
        $this->st_link = $st_link;
        return $this;

    }

    /**
     * @return string
     */
    public function getst_imagem()
    {
        return $this->st_imagem;
    }

    /**
     * @param string $st_imagem
     */
    public function setst_imagem($st_imagem)
    {
        $this->st_imagem = $st_imagem;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_portaldoaluno()
    {
        return $this->bl_portaldoaluno;
    }

    /**
     * @param mixed $bl_portaldoaluno
     */
    public function setbl_portaldoaluno($bl_portaldoaluno)
    {
        $this->bl_portaldoaluno = $bl_portaldoaluno;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getbl_mobile()
    {
        return $this->bl_mobile;
    }

    /**
     * @param mixed $bl_mobile
     */
    public function setbl_mobile($bl_mobile)
    {
        $this->bl_mobile = $bl_mobile;
        return $this;

    }

    /**
     * @return bool
     */
    public function getbl_vigente()
    {
        return $this->bl_vigente;
    }

    /**
     * @param bool $bl_vigente
     */
    public function setbl_vigente($bl_vigente)
    {
        $this->bl_vigente = $bl_vigente;
        return $this;

    }


}

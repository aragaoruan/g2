<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_areaconhecimentonivelserie")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwAreaConhecimentoNivelSerie
{

    /**
     *
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     */
    private $st_areaconhecimento;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_nivelensino
     * @Column(name="st_nivelensino", type="string", nullable=true, length=255)
     */
    private $st_nivelensino;

    /**
     *
     * @var integer $id_nivelensino
     * @Column(name="id_nivelensino", type="integer", nullable=true)
     */
    private $id_nivelensino;

    /**
     *
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=true)
     */
    private $id_serie;

    /**
     *
     * @var integer $id_serieanterior
     * @Column(name="id_serieanterior", type="integer", nullable=true)
     */
    private $id_serieanterior;

    /**
     *
     * @var string $st_serie
     * @Column(name="st_serie", type="string", nullable=true, length=255)
     */
    private $st_serie;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_areaconhecimentopai
     * @Column(name="id_areaconhecimentopai", type="integer", nullable=true)
     */
    private $id_areaconhecimentopai;

    public function getId_areaconhecimento ()
    {
        return $this->id_areaconhecimento;
    }

    public function setId_areaconhecimento ($id_areaconhecimento)
    {
        $this->id_areaconhecimento = $id_areaconhecimento;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_areaconhecimento ()
    {
        return $this->st_areaconhecimento;
    }

    public function setSt_areaconhecimento ($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_nivelensino ()
    {
        return $this->st_nivelensino;
    }

    public function setSt_nivelensino ($st_nivelensino)
    {
        $this->st_nivelensino = $st_nivelensino;
        return $this;
    }

    public function getId_nivelensino ()
    {
        return $this->id_nivelensino;
    }

    public function setId_nivelensino ($id_nivelensino)
    {
        $this->id_nivelensino = $id_nivelensino;
        return $this;
    }

    public function getId_serie ()
    {
        return $this->id_serie;
    }

    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    public function getId_serieanterior ()
    {
        return $this->id_serieanterior;
    }

    public function setId_serieanterior ($id_serieanterior)
    {
        $this->id_serieanterior = $id_serieanterior;
        return $this;
    }

    public function getSt_serie ()
    {
        return $this->st_serie;
    }

    public function setSt_serie ($st_serie)
    {
        $this->st_serie = $st_serie;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_areaconhecimentopai ()
    {
        return $this->id_areaconhecimentopai;
    }

    public function setId_areaconhecimentopai ($id_areaconhecimentopai)
    {
        $this->id_areaconhecimentopai = $id_areaconhecimentopai;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_projetoentidade")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ProjetoEntidade
{

    /**
     * @var integer $id_projetoentidade
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @Column(name="id_projetoentidade", type="integer", nullable=false, length=4)
     */
    private $id_projetoentidade;

    /**
     * @var ProjetoPedagogico $id_projetopedagogico
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", nullable=false, referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var Usuario $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var date $dt_inicio
     * @Column(type="date",nullable=false, name="dt_inicio")
     */
    private $dt_inicio;

    /**
     * @return integer id_projetoentidade
     */
    public function getId_projetoentidade ()
    {
        return $this->id_projetoentidade;
    }

    /**
     * @param integer $id_projetoentidade
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setId_projetoentidade ($id_projetoentidade)
    {
        $this->id_projetoentidade = $id_projetoentidade;
        return $this;
    }

    /**
     * @return \G2\Entity\ProjetoPedagogico id_projetopedagogico
     */
    public function getId_projetopedagogico ()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @param \G2\Entity\ProjetoPedagogico $id_projetopedagogico
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setId_projetopedagogico (ProjetoPedagogico $id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @return \G2\Entity\Entidade id_entidade
     */
    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    /**
     * @param \G2\Entity\Entidade $id_entidade
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setId_entidade (Entidade $id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return \G2\Entity\Situacao id_situacao
     */
    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setId_situacao (Situacao $id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return \G2\Entity\Usuario id_usuariocadastro
     */
    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuariocadastro
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setId_usuariocadastro (Usuario $id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return datetime2 dt_cadastro
     */
    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    /**
     * @param date $dt_inicio
     * @return \G2\Entity\ProjetoEntidade
     */
    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_aproveitamento")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwAproveitamento
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;

    /**
     *
     * @var string $st_instituicao
     * @Column(name="st_instituicao", type="string", nullable=true, length=200)
     */
    private $st_instituicao;

    /**
     *
     * @var string $st_curso
     * @Column(name="st_curso", type="string", nullable=true, length=200)
     */
    private $st_curso;

    /**
     *
     * @var string $st_cargahoraria
     * @Column(name="st_cargahoraria", type="string", nullable=true, length=10)
     */
    private $st_cargahoraria;

    /**
     *
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=200)
     */
    private $st_disciplina;

    /**
     *
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_municipio
     * @Column(name="id_municipio", type="integer", nullable=true)
     */
    private $id_municipio;

    /**
     *
     * @var integer $id_aproveitamentodisciplina
     * @Column(name="id_aproveitamentodisciplina", type="integer", nullable=true)
     */
    private $id_aproveitamentodisciplina;

    /**
     *
     * @var datetime $dt_conclusao
     * @Column(name="dt_conclusao", type="datetime", nullable=true)
     */
    private $dt_conclusao;

    /**
     *
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false)
     */
    private $id_matricula;

    /**
     *
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true)
     */
    private $id_disciplina;

    /**
     *
     * @var decimal $nu_aprovafinal
     * @Column(name="nu_aprovafinal", type="decimal", nullable=true)
     */
    private $nu_aprovafinal;

    /**
     *
     * @var integer $id_entidadematricula
     * @Column(name="id_entidadematricula", type="integer", nullable=true)
     */
    private $id_entidadematricula;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var string $st_disciplinaaproveitada
     * @Column(name="st_disciplinaaproveitada", type="string", nullable=true, length=255)
     */
    private $st_disciplinaaproveitada;

    /**
     *
     * @var string $st_cargahorariadisciplina
     * @Column(name="st_cargahorariadisciplina", type="string", nullable=true, length=10)
     */
    private $st_cargahorariadisciplina;

    /**
     *
     * @var integer $nu_anoconclusao
     * @Column(name="nu_anoconclusao", type="integer", nullable=true)
     */
    private $nu_anoconclusao;

    /**
     *
     * @var string $st_notaoriginal
     * @Column(name="st_notaoriginal", type="string", nullable=true, length=10)
     */
    private $st_notaoriginal;

    /**
     *
     * @var integer $id_serie
     * @Column(name="id_serie", type="integer", nullable=true)
     */
    private $id_serie;

    /**
     *
     * @var string $st_disciplinaoriginal
     * @Column(name="st_disciplinaoriginal", type="string", nullable=false, length=200)
     */
    private $st_disciplinaoriginal;

    /**
     *
     * @var integer $id_apmatriculadisciplina
     * @Column(name="id_apmatriculadisciplina", type="integer", nullable=false)
     */
    private $id_apmatriculadisciplina;

    /**
     *
     * @var string $st_tituloexibicao
     * @Column(name="st_tituloexibicao", type="string", nullable=true, length=255)
     */
    private $st_tituloexibicao;

    /**
     *
     * @var decimal $nu_aproveitamento
     * @Column(name="nu_aproveitamento", type="decimal", nullable=false)
     */
    private $nu_aproveitamento;

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getSt_instituicao ()
    {
        return $this->st_instituicao;
    }

    public function setSt_instituicao ($st_instituicao)
    {
        $this->st_instituicao = $st_instituicao;
        return $this;
    }

    public function getSt_curso ()
    {
        return $this->st_curso;
    }

    public function setSt_curso ($st_curso)
    {
        $this->st_curso = $st_curso;
        return $this;
    }

    public function getSt_cargahoraria ()
    {
        return $this->st_cargahoraria;
    }

    public function setSt_cargahoraria ($st_cargahoraria)
    {
        $this->st_cargahoraria = $st_cargahoraria;
        return $this;
    }

    public function getSt_disciplina ()
    {
        return $this->st_disciplina;
    }

    public function setSt_disciplina ($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function getSg_uf ()
    {
        return $this->sg_uf;
    }

    public function setSg_uf ($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_municipio ()
    {
        return $this->id_municipio;
    }

    public function setId_municipio ($id_municipio)
    {
        $this->id_municipio = $id_municipio;
        return $this;
    }

    public function getId_aproveitamentodisciplina ()
    {
        return $this->id_aproveitamentodisciplina;
    }

    public function setId_aproveitamentodisciplina ($id_aproveitamentodisciplina)
    {
        $this->id_aproveitamentodisciplina = $id_aproveitamentodisciplina;
        return $this;
    }

    public function getDt_conclusao ()
    {
        return $this->dt_conclusao;
    }

    public function setDt_conclusao ($dt_conclusao)
    {
        $this->dt_conclusao = $dt_conclusao;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getId_matricula ()
    {
        return $this->id_matricula;
    }

    public function setId_matricula ($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getId_disciplina ()
    {
        return $this->id_disciplina;
    }

    public function setId_disciplina ($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getNu_aprovafinal ()
    {
        return $this->nu_aprovafinal;
    }

    public function setNu_aprovafinal ($nu_aprovafinal)
    {
        $this->nu_aprovafinal = $nu_aprovafinal;
        return $this;
    }

    public function getId_entidadematricula ()
    {
        return $this->id_entidadematricula;
    }

    public function setId_entidadematricula ($id_entidadematricula)
    {
        $this->id_entidadematricula = $id_entidadematricula;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getSt_disciplinaaproveitada ()
    {
        return $this->st_disciplinaaproveitada;
    }

    public function setSt_disciplinaaproveitada ($st_disciplinaaproveitada)
    {
        $this->st_disciplinaaproveitada = $st_disciplinaaproveitada;
        return $this;
    }

    public function getSt_cargahorariadisciplina ()
    {
        return $this->st_cargahorariadisciplina;
    }

    public function setSt_cargahorariadisciplina ($st_cargahorariadisciplina)
    {
        $this->st_cargahorariadisciplina = $st_cargahorariadisciplina;
        return $this;
    }

    public function getNu_anoconclusao ()
    {
        return $this->nu_anoconclusao;
    }

    public function setNu_anoconclusao ($nu_anoconclusao)
    {
        $this->nu_anoconclusao = $nu_anoconclusao;
        return $this;
    }

    public function getSt_notaoriginal ()
    {
        return $this->st_notaoriginal;
    }

    public function setSt_notaoriginal ($st_notaoriginal)
    {
        $this->st_notaoriginal = $st_notaoriginal;
        return $this;
    }

    public function getId_serie ()
    {
        return $this->id_serie;
    }

    public function setId_serie ($id_serie)
    {
        $this->id_serie = $id_serie;
        return $this;
    }

    public function getSt_disciplinaoriginal ()
    {
        return $this->st_disciplinaoriginal;
    }

    public function setSt_disciplinaoriginal ($st_disciplinaoriginal)
    {
        $this->st_disciplinaoriginal = $st_disciplinaoriginal;
        return $this;
    }

    public function getId_apmatriculadisciplina ()
    {
        return $this->id_apmatriculadisciplina;
    }

    public function setId_apmatriculadisciplina ($id_apmatriculadisciplina)
    {
        $this->id_apmatriculadisciplina = $id_apmatriculadisciplina;
        return $this;
    }

    public function getSt_tituloexibicao ()
    {
        return $this->st_tituloexibicao;
    }

    public function setSt_tituloexibicao ($st_tituloexibicao)
    {
        $this->st_tituloexibicao = $st_tituloexibicao;
        return $this;
    }

    public function getNu_aproveitamento ()
    {
        return $this->nu_aproveitamento;
    }

    public function setNu_aproveitamento ($nu_aproveitamento)
    {
        $this->nu_aproveitamento = $nu_aproveitamento;
        return $this;
    }

}

<?php

/**
 * Entity e para o Sys.Views
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="sys.views")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-11-12
 */
class View {

    /**
     * @var integer $object_id
     * @Column(name="object_id", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $object_id;

    /**
     * @Column(type="string",length=30,nullable=false, name="name")
     * @var string
     */
    private $st_view;

    /**
     * @var integer $schema_id
     * @Column(name="schema_id", type="integer", nullable=false)
     */
    private $schema_id;



    /**
     * @return int
     */
    public function getObject_id()
    {
        return $this->object_id;
    }

    /**
     * @param int $object_id
     */
    public function setObject_id($object_id)
    {
        $this->object_id = $object_id;
    }


    /**
     * @return string
     */
    public function getSt_view()
    {
        return $this->st_view;
    }

    /**
     * @param string $st_view
     */
    public function setSt_view($st_view)
    {
        $this->st_view = $st_view;
    }

    /**
     * @return int
     */
    public function getSchema_id()
    {
        return $this->schema_id;
    }

    /**
     * @param int $schema_id
     */
    public function setSchema_id($schema_id)
    {
        $this->schema_id = $schema_id;
    }



}

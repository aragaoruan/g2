<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_pesquisarlivro")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarLivro
{

    /**
     *
     * @var integer $id_livro
     * @Column(name="id_livro", nullable=false, type="integer")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_livro;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true) 
     */
    private $id_situacao;

    /**
     *
     * @var integer $id_entidadecadastro
     * @Column(name="id_entidadecadastro", type="integer", nullable=true) 
     */
    private $id_entidadecadastro;

    /**
     *
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true) 
     */
    private $id_usuariocadastro;

    /**
     *
     * @var integer $id_tipolivro
     * @Column(name="id_tipolivro", type="integer", nullable=true) 
     */
    private $id_tipolivro;

    /**
     *
     * @var integer $id_livrocolecao
     * @Column(name="id_livrocolecao", type="integer", nullable=true) 
     */
    private $id_livrocolecao;

    /**
     *
     * @var string $st_livro
     * @Column(name="st_livro", type="string", nullable=false, length=250) 
     */
    private $st_livro;

    /**
     *
     * @var string $st_isbn
     * @Column(name="st_isbn", type="string", nullable=true, length=50) 
     */
    private $st_isbn;

    /**
     *
     * @var string $st_codigocontrole
     * @Column(name="st_codigocontrole", type="string", nullable=true, length=50) 
     */
    private $st_codigocontrole;

    /**
     *
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false) 
     */
    private $bl_ativo;

    /**
     *
     * @var string $st_livrocolecao
     * @Column(name="st_livrocolecao", type="string", nullable=false, length=200) 
     */
    private $st_livrocolecao;

    /**
     *
     * @var string $st_tipolivro
     * @Column(name="st_tipolivro", type="string", nullable=false, length=20) 
     */
    private $st_tipolivro;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255) 
     */
    private $st_situacao;

    public function getId_livro ()
    {
        return $this->id_livro;
    }

    public function setId_livro ($id_livro)
    {
        $this->id_livro = $id_livro;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getId_entidadecadastro ()
    {
        return $this->id_entidadecadastro;
    }

    public function setId_entidadecadastro ($id_entidadecadastro)
    {
        $this->id_entidadecadastro = $id_entidadecadastro;
        return $this;
    }

    public function getId_usuariocadastro ()
    {
        return $this->id_usuariocadastro;
    }

    public function setId_usuariocadastro ($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    public function getId_tipolivro ()
    {
        return $this->id_tipolivro;
    }

    public function setId_tipolivro ($id_tipolivro)
    {
        $this->id_tipolivro = $id_tipolivro;
        return $this;
    }

    public function getId_livrocolecao ()
    {
        return $this->id_livrocolecao;
    }

    public function setId_livrocolecao ($id_livrocolecao)
    {
        $this->id_livrocolecao = $id_livrocolecao;
        return $this;
    }

    public function getSt_livro ()
    {
        return $this->st_livro;
    }

    public function setSt_livro ($st_livro)
    {
        $this->st_livro = $st_livro;
        return $this;
    }

    public function getSt_isbn ()
    {
        return $this->st_isbn;
    }

    public function setSt_isbn ($st_isbn)
    {
        $this->st_isbn = $st_isbn;
        return $this;
    }

    public function getSt_codigocontrole ()
    {
        return $this->st_codigocontrole;
    }

    public function setSt_codigocontrole ($st_codigocontrole)
    {
        $this->st_codigocontrole = $st_codigocontrole;
        return $this;
    }

    public function getBl_ativo ()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo ($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    public function getSt_livrocolecao ()
    {
        return $this->st_livrocolecao;
    }

    public function setSt_livrocolecao ($st_livrocolecao)
    {
        $this->st_livrocolecao = $st_livrocolecao;
        return $this;
    }

    public function getSt_tipolivro ()
    {
        return $this->st_tipolivro;
    }

    public function setSt_tipolivro ($st_tipolivro)
    {
        $this->st_tipolivro = $st_tipolivro;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

}
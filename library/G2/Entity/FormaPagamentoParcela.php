<?php

/*
 * Entity FormaPagamentoParcela
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-30
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_formapagamentoparcela")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class FormaPagamentoParcela {

    /**
     * @var integer $id_formapagamentoparcela
     * @Column(name="id_formapagamentoparcela", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_formapagamentoparcela;

    /**
     * @var integer $id_formapagamento
     * @Column(name="id_formapagamento", type="integer", nullable=false)
     */
    private $id_formapagamento;


    /**
     * @var MeioPagamento $id_meiopagamento
     * @ManyToOne(targetEntity="MeioPagamento")
     * @JoinColumn(name="id_meiopagamento", referencedColumnName="id_meiopagamento")
     */
    private $id_meiopagamento;

    /**
     * @var integer $nu_parcelaquantidademin
     * @Column(name="nu_parcelaquantidademin", type="integer", nullable=false)
     */
    private $nu_parcelaquantidademin;

    /**
     * @var integer $nu_parcelaquantidademax
     * @Column(name="nu_parcelaquantidademax", type="integer", nullable=false)
     */
    private $nu_parcelaquantidademax;

    /**
     * @var decimal $nu_juros
     * @Column(name="nu_juros", type="decimal", nullable=true)
     */
    private $nu_juros;

    /**
     * @var decimal $nu_valormin
     * @Column(name="nu_valormin", type="decimal", nullable=true)
     */
    private $nu_valormin;

    /**
     * @param int $id_formapagamento
     */
    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    /**
     * @return int
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @param int $id_formapagamentoparcela
     */
    public function setId_formapagamentoparcela($id_formapagamentoparcela)
    {
        $this->id_formapagamentoparcela = $id_formapagamentoparcela;
    }

    /**
     * @return int
     */
    public function getId_formapagamentoparcela()
    {
        return $this->id_formapagamentoparcela;
    }

    /**
     * @param int $id_meiopagamento
     */
    public function setId_meiopagamento($id_meiopagamento)
    {
        $this->id_meiopagamento = $id_meiopagamento;
    }

    /**
     * @return int
     */
    public function getId_meiopagamento()
    {
        return $this->id_meiopagamento;
    }

    /**
     * @param \G2\Entity\decimal $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @param int $nu_parcelaquantidademax
     */
    public function setNu_parcelaquantidademax($nu_parcelaquantidademax)
    {
        $this->nu_parcelaquantidademax = $nu_parcelaquantidademax;
    }

    /**
     * @return int
     */
    public function getNu_parcelaquantidademax()
    {
        return $this->nu_parcelaquantidademax;
    }

    /**
     * @param int $nu_parcelaquantidademin
     */
    public function setNu_parcelaquantidademin($nu_parcelaquantidademin)
    {
        $this->nu_parcelaquantidademin = $nu_parcelaquantidademin;
    }

    /**
     * @return int
     */
    public function getNu_parcelaquantidademin()
    {
        return $this->nu_parcelaquantidademin;
    }

    /**
     * @param \G2\Entity\decimal $nu_valormin
     */
    public function setNu_valormin($nu_valormin)
    {
        $this->nu_valormin = $nu_valormin;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_valormin()
    {
        return $this->nu_valormin;
    }




}

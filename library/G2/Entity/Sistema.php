<?php

namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_sistema")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Sistema extends G2Entity
{

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_sistema;

    /**
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=false, length=255)
     */
    private $st_sistema;

    /**
     * @var string $st_conexao
     * @Column(name="st_conexao", type="string", nullable=false, length=2000)
     */
    private $st_conexao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     *
     * @var $entidades
     * @ManyToMany(targetEntity="Entidade", inversedBy="sistemas")
     * @JoinTable(name="tb_entidadesistema",
     *   joinColumns={@JoinColumn(name="id_sistema", referencedColumnName="id_sistema")},
     *   inverseJoinColumns={@JoinColumn(name="id_entidade", referencedColumnName="id_entidade")}
     * )
     */
    private $entidades;

    /**
     * @var string $st_chaveacesso
     * @Column(name="st_chaveacesso", type="string", nullable=false, length=300)
     */
    private $st_chaveacesso;

    public function __construct()
    {
        $this->entidades = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getst_conexao()
    {
        return $this->st_conexao;
    }

    /**
     * @param string $st_conexao
     */
    public function setst_conexao($st_conexao)
    {
        $this->st_conexao = $st_conexao;
        return $this;

    }

    public function getEntidades()
    {
        return $this->entidades;
    }

    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
        return $this;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_chaveacesso()
    {
        return $this->st_chaveacesso;
    }

    /**
     * @param string $st_chaveacesso
     */
    public function setst_chaveacesso($st_chaveacesso)
    {
        $this->st_chaveacesso = $st_chaveacesso;
        return $this;

    }

}
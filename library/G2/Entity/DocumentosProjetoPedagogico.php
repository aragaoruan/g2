<?php

/*
 * Entity DocumentosProjetoPedagogico
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-10-01
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentosprojetopedagogico")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DocumentosProjetoPedagogico {

    /**
     *
     * @var integer $id_documentos
     * @Column(name="id_documentos", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_documentos;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;


    public function getId_documentos() {
        return $this->id_documentos;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function setId_documentos($id_documentos) {
        $this->id_documentos = $id_documentos;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

}

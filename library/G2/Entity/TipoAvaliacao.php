<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoavaliacao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoAvaliacao
{

    /**
     *
     * @var integer $id_tipoavaliacao
     * @Column(name="id_tipoavaliacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoavaliacao;

    /**
     *
     * @var string $st_tipoavaliacao
     * @Column(name="st_tipoavaliacao", type="string", nullable=false, length=200) 
     */
    private $st_tipoavaliacao;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=300) 
     */
    private $st_descricao;

    public function getId_tipoavaliacao ()
    {
        return $this->id_tipoavaliacao;
    }

    public function setId_tipoavaliacao ($id_tipoavaliacao)
    {
        $this->id_tipoavaliacao = $id_tipoavaliacao;
        return $this;
    }

    public function getSt_tipoavaliacao ()
    {
        return $this->st_tipoavaliacao;
    }

    public function setSt_tipoavaliacao ($st_tipoavaliacao)
    {
        $this->st_tipoavaliacao = $st_tipoavaliacao;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_professorsaladeaulalote")
 * @Entity(repositoryClass="\G2\Repository\VwProfessorSalaDeAulaLote")
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwProfessorSalaDeAulaLote
{

    /**
     * @var integer $id_saladeaula
     * @Column(type="integer", name="id_saladeaula", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;


    /**
     * @var integer $id_usuario
     * @Column(type="integer", name="id_usuario", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", name="id_entidade", nullable=true)
     */
    private $id_entidade;


    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;


    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;

    /**
     * @var integer $id_disciplina
     * @Column(type="integer", name="id_disciplina", nullable=true)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true)
     */
    private $dt_abertura;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_perfilreferencia
     * @Column(type="integer", name="id_perfilreferencia", nullable=true)
     */
    private $id_perfilreferencia;

    /**
     * @var integer $id_perfilpedagogico
     * @Column(type="integer", name="id_perfilpedagogico", nullable=true)
     */
    private $id_perfilpedagogico;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=4)
     */
    private $bl_ativo;

    /**
     * @var integer $id_perfil
     * @Column(type="integer", name="id_perfil", nullable=true)
     */
    private $id_perfil;

    /**
     * @var string $st_nomeperfil
     * @Column(name="st_nomeperfil", type="string", nullable=true, length=100)
     */
    private $st_nomeperfil;

    /**
     * @var boolean $bl_titular
     * @Column(name="bl_titular", type="boolean", nullable=true, length=4)
     */
    private $bl_titular;

    /**
     * @var string $st_titular
     * @Column(name="st_titular", type="string", nullable=true, length=255)
     */
    private $st_titular;

    /**
     * @var string $st_dtabertura
     * @Column(name="st_dtabertura", type="string", nullable=true, length=255)
     */
    private $st_dtabertura;

    /**
     * @var string $st_dtencerramento
     * @Column(name="st_dtencerramento", type="string", nullable=true, length=255)
     */
    private $st_dtencerramento;

    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }


    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }


    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }


    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }


    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
    }


    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }


    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }


    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }


    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }


    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }


    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }


    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }


    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
    }


    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }


    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
    }


    public function getId_perfilreferencia()
    {
        return $this->id_perfilreferencia;
    }


    public function setId_perfilreferencia($id_perfilreferencia)
    {
        $this->id_perfilreferencia = $id_perfilreferencia;
    }


    public function getId_perfilpedagogico()
    {
        return $this->id_perfilpedagogico;
    }


    public function setId_perfilpedagogico($id_perfilpedagogico)
    {
        $this->id_perfilpedagogico = $id_perfilpedagogico;
    }


    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
    }


    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }


    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    public function getId_perfil()
    {
        return $this->id_perfil;
    }


    public function setId_perfil($id_perfil)
    {
        $this->id_perfil = $id_perfil;
    }


    public function getSt_nomeperfil()
    {
        return $this->st_nomeperfil;
    }

    public function setSt_nomeperfil($st_nomeperfil)
    {
        $this->st_nomeperfil = $st_nomeperfil;
    }


    public function getBl_titular()
    {
        return $this->bl_titular;
    }

    public function setBl_titular($bl_titular)
    {
        $this->bl_titular = $bl_titular;
    }

    public function getSt_titular()
    {
        return $this->st_titular;
    }


    public function setSt_titular($st_titular)
    {
        $this->st_titular = $st_titular;
    }


    public function getSt_dtabertura()
    {
        return $this->st_dtabertura;
    }

    public function setSt_dtabertura($st_dtabertura)
    {
        $this->st_dtabertura = $st_dtabertura;
    }

    public function getSt_dtencerramento()
    {
        return $this->st_dtencerramento;
    }


    public function setSt_dtencerramento($st_dtencerramento)
    {
        $this->st_dtencerramento = $st_dtencerramento;
    }









}
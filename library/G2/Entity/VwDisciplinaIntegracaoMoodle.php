<?php
/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 16/10/2017
 * Time: 17:29
 */

namespace G2\Entity;


use G2\G2Entity;


/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_disciplinaintegracaomoodle")
 * @Entity
 * @EntityView
 */
class VwDisciplinaIntegracaoMoodle extends G2Entity
{
    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var integer $id_disciplinaintegracao
     * @Column(name="id_disciplinaintegracao", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplinaintegracao;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_sistema;
    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_usuariocadastro;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var integer $id_moodleintegracao
     * @Column(name="id_moodleintegracao", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_moodleintegracao;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;
    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=false, length=30)
     */
    private $st_codsistema;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_salareferencia
     * @Column(name="st_salareferencia", type="string", nullable=true, length=255)
     */
    private $st_salareferencia;
    /**
     * @var string $st_moodleintegracao
     * @Column(name="st_moodleintegracao", type="string", nullable=true, length=140)
     */
    private $st_moodleintegracao;

    /**
     * @return datetime2
     */
    public function getdt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param datetime2 $dt_cadastro
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setdt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_disciplinaintegracao()
    {
        return $this->id_disciplinaintegracao;
    }

    /**
     * @param int $id_disciplinaintegracao
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_disciplinaintegracao($id_disciplinaintegracao)
    {
        $this->id_disciplinaintegracao = $id_disciplinaintegracao;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param int $id_sistema
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return int
     */
    public function getid_moodleintegracao()
    {
        return $this->id_moodleintegracao;
    }

    /**
     * @param int $id_moodleintegracao
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setid_moodleintegracao($id_moodleintegracao)
    {
        $this->id_moodleintegracao = $id_moodleintegracao;
        return $this;
    }

    /**
     * @return bool
     */
    public function isbl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bool $bl_ativo
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setbl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_codsistema()
    {
        return $this->st_codsistema;
    }

    /**
     * @param string $st_codsistema
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setst_codsistema($st_codsistema)
    {
        $this->st_codsistema = $st_codsistema;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setst_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_salareferencia()
    {
        return $this->st_salareferencia;
    }

    /**
     * @param string $st_salareferencia
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setst_salareferencia($st_salareferencia)
    {
        $this->st_salareferencia = $st_salareferencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_moodleintegracao()
    {
        return $this->st_moodleintegracao;
    }

    /**
     * @param string $st_moodleintegracao
     * @return VwDisciplinaIntegracaoMoodle
     */
    public function setst_moodleintegracao($st_moodleintegracao)
    {
        $this->st_moodleintegracao = $st_moodleintegracao;
        return $this;
    }


}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_textosistema")
 * @Entity
 * @Entity(repositoryClass="\G2\Repository\TextoSistema")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TextoSistema
{

    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_textosistema;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Entidade $id_entidade
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=true, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var TextoExibicao $id_textoexibicao
     * @ManyToOne(targetEntity="TextoExibicao")
     * @JoinColumn(name="id_textoexibicao", nullable=true, referencedColumnName="id_textoexibicao")
     */
    private $id_textoexibicao;

    /**
     * @var TextoCategoria $id_textocategoria
     * @ManyToOne(targetEntity="TextoCategoria")
     * @JoinColumn(name="id_textocategoria", nullable=true, referencedColumnName="id_textocategoria")
     */
    private $id_textocategoria;

    /**
     * @var Situacao $id_situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", nullable=false, referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=false, length=200)
     */
    private $st_textosistema;

    /**
     * @var text $st_texto
     * @Column(name="st_texto", type="text", nullable=false)
     */
    private $st_texto;

    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=false)
     */
    private $dt_fim;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false, options={"default":NULL})
     */
    private $dt_cadastro;

    /**
     * @var OrientacaoTexto $id_orientacaotexto
     * @ManyToOne(targetEntity="OrientacaoTexto")
     * @JoinColumn(name="id_orientacaotexto", nullable=false, referencedColumnName="id_orientacaotexto")
     */
    private $id_orientacaotexto;

    /**
     * @var boolean $bl_cabecalho
     * @Column(name="bl_cabecalho", type="boolean", nullable=true)
     */
    private $bl_cabecalho;

    /**
     * @var TextoSistema $id_cabecalho
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_cabecalho", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_cabecalho;

    /**
     * @var TextoSistema $id_rodape
     * @ManyToOne(targetEntity="TextoSistema")
     * @JoinColumn(name="id_rodape", nullable=false, referencedColumnName="id_textosistema")
     */
    private $id_rodape;

    /**
     * @var boolean $bl_edicao
     * @Column(name="bl_edicao", type="boolean", nullable=false)
     */
    private $bl_edicao = 1;

    public function getId_textosistema ()
    {
        return $this->id_textosistema;
    }

    public function setId_textosistema ($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
        return $this;
    }

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario (Usuario $id_usuario = null)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade (Entidade $id_entidade = null)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_textoexibicao ()
    {
        return $this->id_textoexibicao;
    }

    public function setId_textoexibicao (TextoExibicao $id_textoexibicao = null)
    {
        $this->id_textoexibicao = $id_textoexibicao;
        return $this;
    }

    public function getId_textocategoria ()
    {
        return $this->id_textocategoria;
    }

    public function setId_textocategoria (TextoCategoria $id_textocategoria = null)
    {
        $this->id_textocategoria = $id_textocategoria;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao (Situacao $id_situacao = null)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_textosistema ()
    {
        return $this->st_textosistema;
    }

    public function setSt_textosistema ($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
        return $this;
    }

    public function getSt_texto ()
    {
        return $this->st_texto;
    }

    public function setSt_texto ($st_texto)
    {
        $this->st_texto = $st_texto;
        return $this;
    }

    public function getDt_inicio ()
    {
        return $this->dt_inicio;
    }

    public function setDt_inicio ($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    public function getDt_fim ()
    {
        return $this->dt_fim;
    }

    public function setDt_fim ($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getId_orientacaotexto ()
    {
        return $this->id_orientacaotexto;
    }

    public function setId_orientacaotexto (OrientacaoTexto $id_orientacaotexto = null)
    {
        $this->id_orientacaotexto = $id_orientacaotexto;
        return $this;
    }

    public function getBl_cabecalho() {
        return $this->bl_cabecalho;
    }

    public function setBl_cabecalho($bl_cabecalho) {
        $this->bl_cabecalho = $bl_cabecalho;
        return $this;
    }

    public function getId_cabecalho()
    {
        return $this->id_cabecalho;
    }

    public function setId_cabecalho($id_cabecalho)
    {
        $this->id_cabecalho = $id_cabecalho;
        return $this;
    }

    public function getId_rodape()
    {
        return $this->id_rodape;
    }

    public function setId_rodape($id_rodape)
    {
        $this->id_rodape = $id_rodape;
        return $this;
    }

    /**
     * @return string
     */
    public function getBl_edicao()
    {
        return $this->bl_edicao;
    }

    /**
     * @param string $bl_edicao
     */
    public function setBl_edicao($bl_edicao)
    {
        $this->bl_edicao = $bl_edicao;
    }

}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_alunoslancamentonota")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAlunosLancamentoNota {

    /**
     * @var integer $id_avaliacaoagendamento
     * @Column(name="id_avaliacaoagendamento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoagendamento;

    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", type="date", nullable=true, length=3)
     */
    private $dt_aplicacao;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=true, length=4)
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=true, length=4)
     */
    private $id_areaconhecimento;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;

    /**
     * @var integer $id_situacaoagendamento
     * @Column(name="id_situacaoagendamento", type="integer", nullable=true, length=4)
     */
    private $id_situacaoagendamento;

    /**
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=true, length=4)
     */
    private $id_evolucao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=true, length=4)
     */
    private $id_aplicadorprova;

    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=true, length=300)
     */
    private $st_nomecompleto;

    /**
     * @var string $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true, length=255)
     */
    private $st_areaconhecimento;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=true, length=255)
     */
    private $st_evolucao;

    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=true, length=200)
     */
    private $st_aplicadorprova;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true, length=500)
     */
    private $st_endereco;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true, length=255)
     */
    private $st_email;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;

    /**
     * @var integer $id_tipodeavaliacao
     * @Column(name="id_tipodeavaliacao", type="integer", nullable=true, length=4)
     */
    private $id_tipodeavaliacao;

    /**
     * @var integer $bl_provaglobal
     * @Column(name="bl_provaglobal", type="integer", nullable=true, length=4)
     */
    private $bl_provaglobal;
    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="integer", nullable=true, length=3)
     */
    private $nu_valor;

    public function getId_avaliacaoagendamento() {
        return $this->id_avaliacaoagendamento;
    }

    public function getDt_aplicacao() {
        return $this->dt_aplicacao;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function getId_avaliacaoaplicacao() {
        return $this->id_avaliacaoaplicacao;
    }

    public function getId_areaconhecimento() {
        return $this->id_areaconhecimento;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_situacao() {
        return $this->id_situacao;
    }

    public function getId_situacaoagendamento() {
        return $this->id_situacaoagendamento;
    }

    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function getId_aplicadorprova() {
        return $this->id_aplicadorprova;
    }

    public function getSt_nomecompleto() {
        return $this->st_nomecompleto;
    }

    public function getSt_areaconhecimento() {
        return $this->st_areaconhecimento;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    public function getSt_situacao() {
        return $this->st_situacao;
    }

    public function getSt_aplicadorprova() {
        return $this->st_aplicadorprova;
    }

    public function getSt_endereco() {
        return $this->st_endereco;
    }

    public function getSt_cpf() {
        return $this->st_cpf;
    }

    public function getSt_email() {
        return $this->st_email;
    }

    public function getSg_uf() {
        return $this->sg_uf;
    }

    public function setId_avaliacaoagendamento($id_avaliacaoagendamento) {
        $this->id_avaliacaoagendamento = $id_avaliacaoagendamento;
    }

    public function setDt_aplicacao($dt_aplicacao) {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
    }

    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao) {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    public function setId_areaconhecimento($id_areaconhecimento) {
        $this->id_areaconhecimento = $id_areaconhecimento;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

    public function setId_situacaoagendamento($id_situacaoagendamento) {
        $this->id_situacaoagendamento = $id_situacaoagendamento;
    }

    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
    }

    public function setId_aplicadorprova($id_aplicadorprova) {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    public function setSt_nomecompleto($st_nomecompleto) {
        $this->st_nomecompleto = $st_nomecompleto;
    }

    public function setSt_areaconhecimento($st_areaconhecimento) {
        $this->st_areaconhecimento = $st_areaconhecimento;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
    }

    public function setSt_situacao($st_situacao) {
        $this->st_situacao = $st_situacao;
    }

    public function setSt_aplicadorprova($st_aplicadorprova) {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    public function setSt_endereco($st_endereco) {
        $this->st_endereco = $st_endereco;
    }

    public function setSt_cpf($st_cpf) {
        $this->st_cpf = $st_cpf;
    }

    public function setSt_email($st_email) {
        $this->st_email = $st_email;
    }

    public function setSg_uf($sg_uf) {
        $this->sg_uf = $sg_uf;
    }
    
    public function getId_tipodeavaliacao() {
        return $this->id_tipodeavaliacao;
    }

    public function getBl_provaglobal() {
        return $this->bl_provaglobal;
    }

    public function setId_tipodeavaliacao($id_tipodeavaliacao) {
        $this->id_tipodeavaliacao = $id_tipodeavaliacao;
    }

    public function setBl_provaglobal($bl_provaglobal) {
        $this->bl_provaglobal = $bl_provaglobal;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function setNu_nota($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

}

<?php

/*
 * Entity HorarioDiaSemana
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2013-09-11
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_horariodiasemana")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class HorarioDiaSemana {

    /**
     *
     * @var integer $id_diasemana
     * @Column(name="id_diasemana", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_diasemana;

    /**
     * @var integer $id_horarioaula
     * @Column(name="id_horarioaula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_horarioaula;


    public function getId_diasemana() {
        return $this->id_diasemana;
    }

    public function getId_horarioaula() {
        return $this->id_horarioaula;
    }

    public function setId_diasemana($id_diasemana) {
        $this->id_diasemana = $id_diasemana;
    }

    public function setId_horarioaula($id_horarioaula) {
        $this->id_horarioaula = $id_horarioaula;
    }

}

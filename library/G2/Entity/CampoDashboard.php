<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_campodashboard")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-20
 */
class CampoDashboard {

    /**
     *
     * @var integer $id_campodashboard
     * @Column(name="id_campodashboard", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_campodashboard;
    /**
     * @var integer $id_dashboard
     * @Column(name="id_dashboard", type="integer", nullable=false, length=4)
     */
    private $id_dashboard;
    /**
     * @var string $st_campodashboard
     * @Column(name="st_campodashboard", type="string", nullable=false, length=250)
     */
    private $st_campodashboard;
    /**
     * @var string $st_titulocampo
     * @Column(name="st_titulocampo", type="string", nullable=false, length=250)
     */
    private $st_titulocampo;

    /**
     * @return int
     */
    public function getId_campodashboard()
    {
        return $this->id_campodashboard;
    }

    /**
     * @param int $id_campodashboard
     */
    public function setId_campodashboard($id_campodashboard)
    {
        $this->id_campodashboard = $id_campodashboard;
    }

    /**
     * @return int
     */
    public function getId_dashboard()
    {
        return $this->id_dashboard;
    }

    /**
     * @param int $id_dashboard
     */
    public function setId_dashboard($id_dashboard)
    {
        $this->id_dashboard = $id_dashboard;
    }

    /**
     * @return string
     */
    public function getSt_campodashboard()
    {
        return $this->st_campodashboard;
    }

    /**
     * @param string $st_campodashboard
     */
    public function setSt_campodashboard($st_campodashboard)
    {
        $this->st_campodashboard = $st_campodashboard;
    }

    /**
     * @return string
     */
    public function getSt_titulocampo()
    {
        return $this->st_titulocampo;
    }

    /**
     * @param string $st_titulocampo
     */
    public function setSt_titulocampo($st_titulocampo)
    {
        $this->st_titulocampo = $st_titulocampo;
    }



}

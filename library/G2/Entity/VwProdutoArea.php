<?php

namespace G2\Entity;

use G2\G2Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_produtoarea")
 * @Entity
 * @EntityView
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class VwProdutoArea extends G2Entity
{

    /**
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento",type="integer",nullable=false)
     */
    private $id_areaconhecimento;

    /**
     * @Column(name="st_areaconhecimento", type="string", nullable=false, length=255)
     * @var string $st_areaconhecimento
     */
    private $st_areaconhecimento;

   /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;
    
    /**
     * @var int $id_situacao
     * @Column(name="id_situacao",type="integer",nullable=false)
     */
    private $id_situacao;
    
    /**
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     * @var string $st_situacao
     */
    private $st_situacao;
    
   /**
     * @var integer $id_produtoarea
     * @Column(name="id_produtoarea", type="integer", nullable=true)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_produtoarea;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=true)
     */
    private $id_produto;
    
    
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $st_areaconhecimento
	 */
	public function getSt_areaconhecimento() {
		return $this->st_areaconhecimento;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $id_situacao
	 */
	public function getId_situacao() {
		return $this->id_situacao;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $id_produtoarea
	 */
	public function getId_produtoarea() {
		return $this->id_produtoarea;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param number $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
		return $this;
	}

	/**
	 * @param string $st_areaconhecimento
	 */
	public function setSt_areaconhecimento($st_areaconhecimento) {
		$this->st_areaconhecimento = $st_areaconhecimento;
		return $this;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @param number $id_situacao
	 */
	public function setId_situacao($id_situacao) {
		$this->id_situacao = $id_situacao;
		return $this;
	}

	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
		return $this;
	}

	/**
	 * @param number $id_produtoarea
	 */
	public function setId_produtoarea($id_produtoarea) {
		$this->id_produtoarea = $id_produtoarea;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}


    

}
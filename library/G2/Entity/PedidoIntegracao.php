<?php

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_pedidointegracao")
 * @Entity
 */
class PedidoIntegracao extends G2Entity {

    /**
     *
     * @var integer $id_pedidointegracao
     * @Column(name="id_pedidointegracao ", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_pedidointegracao;

    /**
     * @var Venda
     *
     * @ManyToOne(targetEntity="Venda")
     * @JoinColumns({
     *      @JoinColumn(name="id_venda", referencedColumnName="id_venda")
     * })
     */
    private $id_venda;

    /**
     * @var Sistema
     *
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumns({
     *      @JoinColumn(name="id_sistema", referencedColumnName="id_sistema")
     * })
     */
    private $id_sistema;

    /**
     * @var integer $id_situacaointegracao
     * @Column(name="id_situacaointegracao", type="integer", nullable=true)
     */
    private $id_situacaointegracao;

    /**
     * @var datetime $dt_envio
     * @Column(name="dt_envio", type="datetime", nullable=false)
     */
    private $dt_envio;

    /**
     * @var datetime $dt_inicio
     * @Column(name="dt_inicio", type="datetime", nullable=false)
     */
    private $dt_inicio;

    /**
     * @var datetime $dt_termino
     * @Column(name="dt_termino", type="datetime", nullable=false)
     */
    private $dt_termino;

    /**
     * @var datetime $dt_proximopagamento
     * @Column(name="dt_proximopagamento", type="datetime", nullable=false)
     */
    private $dt_proximopagamento;

    /**
     * @var datetime $dt_vencimentocartao
     * @Column(name="dt_vencimentocartao", type="datetime", nullable=false)
     */
    private $dt_vencimentocartao;

    /**
     * @var datetime $dt_proximatentativa
     * @Column(name="dt_proximatentativa", type="datetime", nullable=false)
     */
    private $dt_proximatentativa;

    /**
     * @var integer $nu_intervalo
     * @Column(name="nu_intervalo", type="integer", nullable=true)
     */
    private $nu_intervalo;

    /**
     * @var integer $nu_diavencimento
     * @Column(name="nu_diavencimento", type="integer", nullable=true)
     */
    private $nu_diavencimento;

    /**
     * @var integer $nu_tentativas
     * @Column(name="nu_tentativas", type="integer", nullable=true)
     */
    private $nu_tentativas;

    /**
     * @var integer $nu_tentativasfeitas
     * @Column(name="nu_tentativasfeitas", type="integer", nullable=true)
     */
    private $nu_tentativasfeitas;

    /**
     * @var integer $nu_valor
     * @Column(name="nu_valor", type="integer", nullable=true)
     */
    private $nu_valor;

    /**
     * @var integer $nu_bandeiracartao
     * @Column(name="nu_bandeiracartao", type="integer", nullable=true)
     */
    private $nu_bandeiracartao;

    /**
     * @var integer $st_bandeiracartao
     * @Column(name="st_bandeiracartao", type="string", nullable=true)
     */
    private $st_bandeiracartao;

    /**
     * @var string $st_mensagem
     * @Column(name="st_mensagem", type="string", nullable=false, length=255)
     */
    private $st_mensagem;

    /**
     * @var string $st_orderid
     * @Column(name="st_orderid", type="string", nullable=false, length=50)
     */
    private $st_orderid;

    /**
     * @var datetime $dt_mudancavencimento
     * @Column(name="dt_mudancavencimento", type="datetime", nullable=false)
     */
    private $dt_mudancavencimento;

    /**
     * @return int
     */
    public function getst_bandeiracartao()
    {
        return $this->st_bandeiracartao;
    }

    /**
     * @param int $st_bandeiracartao
     * @return PedidoIntegracao
     */
    public function setStBandeiracartao($st_bandeiracartao)
    {
        $this->st_bandeiracartao = $st_bandeiracartao;
        return $this;
    }


    /**
     * @param mixed $dt_envio
     */
    public function setDtEnvio($dt_envio) {
        $this->dt_envio = $dt_envio;
    }

    /**
     * @return mixed
     */
    public function getDtEnvio() {
        return $this->dt_envio;
    }

    /**
     * @param \G2\Entity\datetime $dt_inicio
     */
    public function setDtInicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtInicio() {
        return $this->dt_inicio;
    }

    /**
     * @param \G2\Entity\datetime $dt_proximatentativa
     */
    public function setDtProximatentativa($dt_proximatentativa) {
        $this->dt_proximatentativa = $dt_proximatentativa;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtProximatentativa() {
        return $this->dt_proximatentativa;
    }

    /**
     * @param \G2\Entity\datetime $dt_proximopagamento
     */
    public function setDtProximopagamento($dt_proximopagamento) {
        $this->dt_proximopagamento = $dt_proximopagamento;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtProximopagamento() {
        return $this->dt_proximopagamento;
    }

    /**
     * @param \G2\Entity\datetime $dt_termino
     */
    public function setDtTermino($dt_termino) {
        $this->dt_termino = $dt_termino;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDtTermino() {
        return $this->dt_termino;
    }

    /**
     * @param int $id_pedidointegracao
     */
    public function setIdPedidointegracao($id_pedidointegracao) {
        $this->id_pedidointegracao = $id_pedidointegracao;
    }

    /**
     * @return int
     */
    public function getIdPedidointegracao() {
        return $this->id_pedidointegracao;
    }

    /**
     * @param \G2\Entity\Sistema $id_sistema
     */
    public function setIdSistema($id_sistema) {
        $this->id_sistema = $id_sistema;
    }

    /**
     * @return \G2\Entity\Sistema
     */
    public function getIdSistema() {
        return $this->id_sistema;
    }

    /**
     * @param \G2\Entity\SituacaoIntegracao $id_situacaointegracao
     */
    public function setIdSituacaointegracao($id_situacaointegracao) {
        $this->id_situacaointegracao = $id_situacaointegracao;
    }

    /**
     * @return \G2\Entity\SituacaoIntegracao
     */
    public function getIdSituacaointegracao() {
        return $this->id_situacaointegracao;
    }

    /**
     * @param \G2\Entity\Venda $id_venda
     */
    public function setIdVenda($id_venda) {
        $this->id_venda = $id_venda;
    }

    /**
     * @return \G2\Entity\Venda
     */
    public function getIdVenda() {
        return $this->id_venda;
    }

    /**
     * @param int $nu_bandeiracartao
     */
    public function setNuBandeiracartao($nu_bandeiracartao) {
        $this->nu_bandeiracartao = $nu_bandeiracartao;
    }

    /**
     * @return int
     */
    public function getNuBandeiracartao() {
        return $this->nu_bandeiracartao;
    }

    /**
     * @param int $nu_diavencimento
     */
    public function setNuDiavencimento($nu_diavencimento) {
        $this->nu_diavencimento = $nu_diavencimento;
    }

    /**
     * @return int
     */
    public function getNuDiavencimento() {
        return $this->nu_diavencimento;
    }

    /**
     * @param int $nu_intervalo
     */
    public function setNuIntervalo($nu_intervalo) {
        $this->nu_intervalo = $nu_intervalo;
    }

    /**
     * @return int
     */
    public function getNuIntervalo() {
        return $this->nu_intervalo;
    }

    /**
     * @param int $nu_tentativas
     */
    public function setNuTentativas($nu_tentativas) {
        $this->nu_tentativas = $nu_tentativas;
    }

    /**
     * @return int
     */
    public function getNuTentativas() {
        return $this->nu_tentativas;
    }

    /**
     * @param int $nu_tentativasfeitas
     */
    public function setNuTentativasfeitas($nu_tentativasfeitas) {
        $this->nu_tentativasfeitas = $nu_tentativasfeitas;
    }

    /**
     * @return int
     */
    public function getNuTentativasfeitas() {
        return $this->nu_tentativasfeitas;
    }

    /**
     * @param int $nu_valor
     */
    public function setNuValor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    /**
     * @return int
     */
    public function getNuValor() {
        return $this->nu_valor;
    }

    /**
     * @param string $st_mensagem
     */
    public function setStMensagem($st_mensagem) {
        $this->st_mensagem = $st_mensagem;
    }

    /**
     * @return string
     */
    public function getStMensagem() {
        return $this->st_mensagem;
    }

    /**
     * @param string $st_orderid
     */
    public function setStOrderid($st_orderid) {
        $this->st_orderid = $st_orderid;
    }

    /**
     * @return string
     */
    public function getStOrderid() {
        return $this->st_orderid;
    }

    public function getId_pedidointegracao() {
        return $this->id_pedidointegracao;
    }

    public function getId_venda() {
        return $this->id_venda;
    }

    public function getId_sistema() {
        return $this->id_sistema;
    }

    public function getId_situacaointegracao() {
        return $this->id_situacaointegracao;
    }

    public function getDt_envio() {
        return $this->dt_envio;
    }

    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function getDt_termino() {
        return $this->dt_termino;
    }

    public function getDt_proximopagamento() {
        return $this->dt_proximopagamento;
    }

    public function getDt_vencimentocartao() {
        return $this->dt_vencimentocartao;
    }

    public function getDt_proximatentativa() {
        return $this->dt_proximatentativa;
    }

    public function getNu_intervalo() {
        return $this->nu_intervalo;
    }

    public function getNu_diavencimento() {
        return $this->nu_diavencimento;
    }

    public function getNu_tentativas() {
        return $this->nu_tentativas;
    }

    public function getNu_tentativasfeitas() {
        return $this->nu_tentativasfeitas;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function getNu_bandeiracartao() {
        return $this->nu_bandeiracartao;
    }

    public function getSt_mensagem() {
        return $this->st_mensagem;
    }

    public function getSt_orderid() {
        return $this->st_orderid;
    }

    public function setId_pedidointegracao($id_pedidointegracao) {
        $this->id_pedidointegracao = $id_pedidointegracao;
    }

    public function setId_venda(Venda $id_venda) {
        $this->id_venda = $id_venda;
    }

    public function setId_sistema(Sistema $id_sistema) {
        $this->id_sistema = $id_sistema;
    }

    public function setId_situacaointegracao($id_situacaointegracao) {
        $this->id_situacaointegracao = $id_situacaointegracao;
    }

    public function setDt_envio(datetime $dt_envio) {
        $this->dt_envio = $dt_envio;
    }

    public function setDt_inicio(datetime $dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_termino(datetime $dt_termino) {
        $this->dt_termino = $dt_termino;
    }

    public function setDt_proximopagamento(datetime $dt_proximopagamento) {
        $this->dt_proximopagamento = $dt_proximopagamento;
    }

    public function setDt_vencimentocartao($dt_vencimentocartao) {
        $this->dt_vencimentocartao = $dt_vencimentocartao;
    }

    public function setDt_proximatentativa(datetime $dt_proximatentativa) {
        $this->dt_proximatentativa = $dt_proximatentativa;
    }

    public function setNu_intervalo($nu_intervalo) {
        $this->nu_intervalo = $nu_intervalo;
    }

    public function setNu_diavencimento($nu_diavencimento) {
        $this->nu_diavencimento = $nu_diavencimento;
    }

    public function setNu_tentativas($nu_tentativas) {
        $this->nu_tentativas = $nu_tentativas;
    }

    public function setNu_tentativasfeitas($nu_tentativasfeitas) {
        $this->nu_tentativasfeitas = $nu_tentativasfeitas;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
    }

    public function setNu_bandeiracartao($nu_bandeiracartao) {
        $this->nu_bandeiracartao = $nu_bandeiracartao;
    }

    public function setSt_mensagem($st_mensagem) {
        $this->st_mensagem = $st_mensagem;
    }

    public function setSt_orderid($st_orderid) {
        $this->st_orderid = $st_orderid;
    }

    /**
     * @param \G2\Entity\datetime $dt_mudancavencimento
     */
    public function setDt_mudancavencimento($dt_mudancavencimento)
    {
        $this->dt_mudancavencimento = $dt_mudancavencimento;
    }

    /**
     * @return \G2\Entity\datetime
     */
    public function getDt_mudancavencimento()
    {
        return $this->dt_mudancavencimento;
    }

}

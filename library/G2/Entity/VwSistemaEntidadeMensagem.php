<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_sistemaentidademensagem")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwSistemaEntidadeMensagem
{
    /**
     * @var integer $id_mensagempadrao
     * @Column(name="id_mensagempadrao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_mensagempadrao;

    /**
     * @var integer $id_tipoenvio
     * @Column(name="id_tipoenvio", type="integer", nullable=true)
     */
    private $id_tipoenvio;

    /**
     * @var string $st_mensagempadrao
     * @Column(name="st_mensagempadrao", type="string", nullable=false, length=150)
     */
    private $st_mensagempadrao;

    /**
     * @var string $st_default
     * @Column(name="st_default", type="string", nullable=true, length=8000)
     */
    private $st_default;


    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;


    /**
     * @var integer $id_sistemaentidademensagem
     * @Column(name="id_sistemaentidademensagem", type="integer", nullable=true)
     */
    private $id_sistemaentidademensagem;


    /**
     * @var integer $id_textosistema
     * @Column(name="id_textosistema", type="integer", nullable=true)
     */
    private $id_textosistema;


    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=true, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(type="boolean", nullable=true, name="bl_ativo")
     */
    private $bl_ativo;


    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=true)
     */
    private $id_usuariocadastro;


    /**
     * @var string $st_textosistema
     * @Column(name="st_textosistema", type="string", nullable=true, length=200)
     */
    private $st_textosistema;



    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function setId_mensagempadrao($id_mensagempadrao)
    {
        $this->id_mensagempadrao = $id_mensagempadrao;
    }

    public function getId_mensagempadrao()
    {
        return $this->id_mensagempadrao;
    }

    public function setId_sistemaentidademensagem($id_sistemaentidademensagem)
    {
        $this->id_sistemaentidademensagem = $id_sistemaentidademensagem;
    }

    public function getId_sistemaentidademensagem()
    {
        return $this->id_sistemaentidademensagem;
    }

    public function setId_textosistema($id_textosistema)
    {
        $this->id_textosistema = $id_textosistema;
    }

    public function getId_textosistema()
    {
        return $this->id_textosistema;
    }

    public function setId_tipoenvio($id_tipoenvio)
    {
        $this->id_tipoenvio = $id_tipoenvio;
    }

    public function getId_tipoenvio()
    {
        return $this->id_tipoenvio;
    }

    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    public function setSt_default($st_default)
    {
        $this->st_default = $st_default;
    }

    public function getSt_default()
    {
        return $this->st_default;
    }

    public function setSt_mensagempadrao($st_mensagempadrao)
    {
        $this->st_mensagempadrao = $st_mensagempadrao;
    }

    public function getSt_mensagempadrao()
    {
        return $this->st_mensagempadrao;
    }

    public function setSt_textosistema($st_textosistema)
    {
        $this->st_textosistema = $st_textosistema;
    }

    public function getSt_textosistema()
    {
        return $this->st_textosistema;
    }




}
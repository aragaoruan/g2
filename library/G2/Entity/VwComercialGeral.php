<?php
namespace G2\Entity;
/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="rel.vw_comercialgeral")
 * @Entity
 * @EntityView
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class VwComercialGeral
{
    /**
     * @var date $dt_matricula
     * @Column(name="dt_matricula", type="date", nullable=true)
     */
    private $dt_matricula;
    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=true)
     */
    private $dt_inicio;
    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true)
     */
    private $dt_fim;
    /**
     * @var date $dt_vencimentoultima
     * @Column(name="dt_vencimentoultima", type="date", nullable=true)
     */
    private $dt_vencimentoultima;
    /**
     * @Id
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;
    /**
     * @Id
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;
    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=true, length=4)
     */
    private $nu_parcelas;
    /**
     * @Id
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false, length=4)
     */
    private $id_venda;
    /**
     * @Id
     * @var integer $id_produtocombo
     * @Column(name="id_produtocombo", type="integer", nullable=true, length=4)
     */
    private $id_produtocombo;
    /**
     * @Id
     * @var integer $id_entidadepai
     * @Column(name="id_entidadepai", type="integer", nullable=false, length=4)
     */
    private $id_entidadepai;
    /**
     * @Id
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=false, length=4)
     */
    private $id_produto;
    /**
     * @var decimal $nu_valorboleto
     * @Column(name="nu_valorboleto", type="decimal", nullable=true, length=17)
     */
    private $nu_valorboleto;
    /**
     * @var decimal $nu_valorcartaoc
     * @Column(name="nu_valorcartaoc", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcartaoc;
    /**
     * @var decimal $nu_valorcartaod
     * @Column(name="nu_valorcartaod", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcartaod;
    /**
     * @var decimal $nu_valorcartaor
     * @Column(name="nu_valorcartaor", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcartaor;
    /**
     * @var decimal $nu_valorcheque
     * @Column(name="nu_valorcheque", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcheque;
    /**
     * @var decimal $nu_valoroutros
     * @Column(name="nu_valoroutros", type="decimal", nullable=true, length=17)
     */
    private $nu_valoroutros;
    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", nullable=false, length=17)
     */
    private $nu_valorliquido;
    /**
     * @var decimal $nu_valorcarta
     * @Column(name="nu_valorcarta", type="decimal", nullable=true, length=17)
     */
    private $nu_valorcarta;
    /**
     * @var decimal $nu_valorbolsa
     * @Column(name="nu_valorbolsa", type="decimal", nullable=true, length=17)
     */
    private $nu_valorbolsa;
    /**
     * @var string $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true, length=150)
     */
    private $st_nomeentidade;
    /**
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=300)
     */
    private $st_nomecompleto;
    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true, length=11)
     */
    private $st_cpf;
    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true, length=255)
     */
    private $st_cidade;
    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;
    /**
     * @Id
     * @var string $id_turma
     * @Column(name="id_turma", type="integer", nullable=true, length=4)
     */
    private $id_turma;
    /**
     * @var string $st_turma
     * @Column(name="st_turma", type="string", nullable=true, length=150)
     */
    private $st_turma;
    /**
     * @var string $st_representante
     * @Column(name="st_representante", type="string", nullable=true, length=300)
     */
    private $st_representante;
    /**
     * @var string $st_atendente
     * @Column(name="st_atendente", type="string", nullable=true, length=300)
     */
    private $st_atendente;
    /**
     * @Id
     * @var string $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false, length=4)
     */
    private $id_evolucao;
    /**
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_combo
     * @Column(name="st_combo", type="string", nullable=true, length=250)
     */
    private $st_combo;
    /**
     * @var string $st_nomeentidaderec
     * @Column(name="st_nomeentidaderec", type="string", nullable=true, length=150)
     */
    private $st_nomeentidaderec;
    /**
     * @var char $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true, length=2)
     */
    private $sg_uf;
    /**
     * @var nvarchar $st_areaconhecimento
     * @Column(name="st_areaconhecimento", type="string", nullable=true)
     */
    private $st_areaconhecimento;


    /**
     * @return date dt_matricula
     */
    public function getDt_matricula()
    {
        return $this->dt_matricula;
    }

    /**
     * @param dt_matricula
     */
    public function setDt_matricula($dt_matricula)
    {
        $this->dt_matricula = $dt_matricula;
        return $this;
    }

    /**
     * @return date dt_inicio
     */
    public function getDt_inicio()
    {
        return $this->dt_inicio;
    }

    /**
     * @param dt_inicio
     */
    public function setDt_inicio($dt_inicio)
    {
        $this->dt_inicio = $dt_inicio;
        return $this;
    }

    /**
     * @return date dt_fim
     */
    public function getDt_fim()
    {
        return $this->dt_fim;
    }

    /**
     * @param dt_fim
     */
    public function setDt_fim($dt_fim)
    {
        $this->dt_fim = $dt_fim;
        return $this;
    }

    /**
     * @return date dt_vencimentoultima
     */
    public function getDt_vencimentoultima()
    {
        return $this->dt_vencimentoultima;
    }

    /**
     * @param dt_vencimentoultima
     */
    public function setDt_vencimentoultima($dt_vencimentoultima)
    {
        $this->dt_vencimentoultima = $dt_vencimentoultima;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_matricula
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @return integer nu_parcelas
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
        return $this;
    }

    /**
     * @return integer id_venda
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
        return $this;
    }

    /**
     * @return integer id_produtocombo
     */
    public function getId_produtocombo()
    {
        return $this->id_produtocombo;
    }

    /**
     * @param id_produtocombo
     */
    public function setId_produtocombo($id_produtocombo)
    {
        $this->id_produtocombo = $id_produtocombo;
        return $this;
    }

    /**
     * @return integer id_entidadepai
     */
    public function getId_entidadepai()
    {
        return $this->id_entidadepai;
    }

    /**
     * @param id_entidadepai
     */
    public function setId_entidadepai($id_entidadepai)
    {
        $this->id_entidadepai = $id_entidadepai;
        return $this;
    }

    /**
     * @return integer id_produto
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
        return $this;
    }

    /**
     * @return decimal nu_valorboleto
     */
    public function getNu_valorboleto()
    {
        return $this->nu_valorboleto;
    }

    /**
     * @param nu_valorboleto
     */
    public function setNu_valorboleto($nu_valorboleto)
    {
        $this->nu_valorboleto = $nu_valorboleto;
        return $this;
    }

    /**
     * @return decimal nu_valorcartaoc
     */
    public function getNu_valorcartaoc()
    {
        return $this->nu_valorcartaoc;
    }

    /**
     * @param nu_valorcartaoc
     */
    public function setNu_valorcartaoc($nu_valorcartaoc)
    {
        $this->nu_valorcartaoc = $nu_valorcartaoc;
        return $this;
    }

    /**
     * @return decimal nu_valorcartaod
     */
    public function getNu_valorcartaod()
    {
        return $this->nu_valorcartaod;
    }

    /**
     * @param nu_valorcartaod
     */
    public function setNu_valorcartaod($nu_valorcartaod)
    {
        $this->nu_valorcartaod = $nu_valorcartaod;
        return $this;
    }

    /**
     * @return decimal nu_valorcartaor
     */
    public function getNu_valorcartaor()
    {
        return $this->nu_valorcartaor;
    }

    /**
     * @param nu_valorcartaor
     */
    public function setNu_valorcartaor($nu_valorcartaor)
    {
        $this->nu_valorcartaor = $nu_valorcartaor;
        return $this;
    }

    /**
     * @return decimal nu_valorcheque
     */
    public function getNu_valorcheque()
    {
        return $this->nu_valorcheque;
    }

    /**
     * @param nu_valorcheque
     */
    public function setNu_valorcheque($nu_valorcheque)
    {
        $this->nu_valorcheque = $nu_valorcheque;
        return $this;
    }

    /**
     * @return decimal nu_valoroutros
     */
    public function getNu_valoroutros()
    {
        return $this->nu_valoroutros;
    }

    /**
     * @param nu_valoroutros
     */
    public function setNu_valoroutros($nu_valoroutros)
    {
        $this->nu_valoroutros = $nu_valoroutros;
        return $this;
    }

    /**
     * @return decimal nu_valorliquido
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
        return $this;
    }

    /**
     * @return decimal nu_valorcarta
     */
    public function getNu_valorcarta()
    {
        return $this->nu_valorcarta;
    }

    /**
     * @param nu_valorcarta
     */
    public function setNu_valorcarta($nu_valorcarta)
    {
        $this->nu_valorcarta = $nu_valorcarta;
        return $this;
    }

    /**
     * @return decimal nu_valorbolsa
     */
    public function getNu_valorbolsa()
    {
        return $this->nu_valorbolsa;
    }

    /**
     * @param nu_valorbolsa
     */
    public function setNu_valorbolsa($nu_valorbolsa)
    {
        $this->nu_valorbolsa = $nu_valorbolsa;
        return $this;
    }

    /**
     * @return string st_nomeentidade
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
        return $this;
    }

    /**
     * @return string st_nomecompleto
     */
    public function getSt_nomecompleto()
    {
        return $this->st_nomecompleto;
    }

    /**
     * @param st_nomecompleto
     */
    public function setSt_nomecompleto($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    /**
     * @return string st_cpf
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
        return $this;
    }

    /**
     * @return string st_cidade
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param st_cidade
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
        return $this;
    }

    /**
     * @return string st_projetopedagogico
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param st_projetopedagogico
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return string id_turma
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @return string st_turma
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @param st_turma
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @return string st_representante
     */
    public function getSt_representante()
    {
        return $this->st_representante;
    }

    /**
     * @param st_representante
     */
    public function setSt_representante($st_representante)
    {
        $this->st_representante = $st_representante;
        return $this;
    }

    /**
     * @return string st_atendente
     */
    public function getSt_atendente()
    {
        return $this->st_atendente;
    }

    /**
     * @param st_atendente
     */
    public function setSt_atendente($st_atendente)
    {
        $this->st_atendente = $st_atendente;
        return $this;
    }

    /**
     * @return string id_evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @param id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    /**
     * @return string st_evolucao
     */
    public function getSt_evolucao()
    {
        return $this->st_evolucao;
    }

    /**
     * @param st_evolucao
     */
    public function setSt_evolucao($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_combo
     */
    public function getSt_combo()
    {
        return $this->st_combo;
    }

    /**
     * @param st_combo
     */
    public function setSt_combo($st_combo)
    {
        $this->st_combo = $st_combo;
        return $this;
    }

    /**
     * @return string st_nomeentidaderec
     */
    public function getSt_nomeentidaderec()
    {
        return $this->st_nomeentidaderec;
    }

    /**
     * @param st_nomeentidaderec
     */
    public function setSt_nomeentidaderec($st_nomeentidaderec)
    {
        $this->st_nomeentidaderec = $st_nomeentidaderec;
        return $this;
    }

    /**
     * @return char sg_uf
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
        return $this;
    }

    /**
     * @return nvarchar st_areaconhecimento
     */
    public function getSt_areaconhecimento()
    {
        return $this->st_areaconhecimento;
    }

    /**
     * @param st_areaconhecimento
     */
    public function setSt_areaconhecimento($st_areaconhecimento)
    {
        $this->st_areaconhecimento = $st_areaconhecimento;
        return $this;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: Débora Castro
 * Date: 28/10/14
 * Time: 12:50
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tramiteliberatcc")
 * @Entity
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class TramiteLiberaTcc {
    /**
     * @var integer $id_tramiteliberatcc
     * @Column(name="id_tramiteliberatcc", type="integer", nullable=false, length=4)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tramiteliberatcc;
    /**
     * @var integer $id_tramite
     * @Column(name="id_tramite", type="integer", nullable=false, length=4)
     */
    private $id_tramite;
    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=false, length=4)
     */
    private $id_alocacao;
    /**
     * @var integer $id_situacaotcc
     * @Column(name="id_situacaotcc", type="integer", nullable=false, length=4)
     */
    private $id_situacaotcc;

    /**
     * @param int $id_alocacao
     */
    public function setId_alocacao($id_alocacao)
    {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    /**
     * @return int
     */
    public function getId_alocacao()
    {
        return $this->id_alocacao;
    }

    /**
     * @param int $id_situacaotcc
     */
    public function setId_situacaotcc($id_situacaotcc)
    {
        $this->id_situacaotcc = $id_situacaotcc;
        return $this;

    }

    /**
     * @return int
     */
    public function getId_situacaotcc()
    {
        return $this->id_situacaotcc;
    }

    /**
     * @param int $id_tramite
     */
    public function setId_tramite($id_tramite)
    {
        $this->id_tramite = $id_tramite;
        return $this;

    }

    /**
     * @return int
     */
    public function getId_tramite()
    {
        return $this->id_tramite;
    }

    /**
     * @param mixed $id_tramiteliberatcc
     */
    public function setId_tramiteliberatcc($id_tramiteliberatcc)
    {
        $this->id_tramiteliberatcc = $id_tramiteliberatcc;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getId_tramiteliberatcc()
    {
        return $this->id_tramiteliberatcc;
    }


} 
<?php
namespace G2\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_meiopagamentointegracao")
 * @Entity
 * @EntityView
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */

class VwMeioPagamentoIntegracao extends G2Entity{

    /**
     * @var integer $id_meiopagamento
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @Column(name="id_meiopagamento", type="integer", nullable=false, length=4)
     */
    private $id_meiopagamento;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=false, length=255)
     */
    private $st_meiopagamento;

    /**
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    /**
     * @var integer $id_cartaobandeira
     * @Column(name="id_cartaobandeira", type="integer", nullable=true, length=4)
     */
    private $id_cartaobandeira;

    /**
     * @var string $st_cartaobandeira
     * @Column(name="st_cartaobandeira", type="string", nullable=true, length=150)
     */
    private $st_cartaobandeira;

    /**
     * @var integer $id_meiopagamentointegracao
     * @Column(name="id_meiopagamentointegracao", type="integer", nullable=true, length=4)
     */
    private $id_meiopagamentointegracao;

    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var string $st_codsistema
     * @Column(name="st_codsistema", type="string", nullable=true, length=30)
     */
    private $st_codsistema;

    /**
     * @var string $st_codcontacaixa
     * @Column(name="st_codcontacaixa", type="string", nullable=true, length=10)
     */
    private $st_codcontacaixa;

}

?>
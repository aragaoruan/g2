<?php

/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 18/11/13
 * Time: 11:25
 */

namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="tb_venda")
 * @Entity(repositoryClass="\G2\Repository\Venda")
 * @EntityLog
 */
class Venda extends G2Entity
{

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=false)
     * @GeneratedValue(strategy="IDENTITY")
     * @Id
     */
    private $id_venda;

    /**
     * @var FormaPagamento
     * @ManyToOne(targetEntity="FormaPagamento")
     * @JoinColumn(name="id_formapagamento", referencedColumnName="id_formapagamento")
     */
    private $id_formapagamento;

    /**
     * @var \DateTime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true)
     */
    private $dt_cadastro;

    /**
     * @var \DateTime $dt_atualizado
     * @Column(name="dt_atualizado", type="datetime", nullable=true)
     */
    private $dt_atualizado;

    /**
     * @var decimal $nu_descontoporcentagem
     * @Column(name="nu_descontoporcentagem", type="decimal", precision=10, scale=5, nullable=false)
     */
    private $nu_descontoporcentagem;

    /**
     * @var numeric $nu_descontovalor
     * @Column(name="nu_descontovalor", type="decimal", precision=10, scale=5, nullable=false)
     */
    private $nu_descontovalor;

    /**
     * @var numeric $nu_juros
     * @Column(name="nu_juros", type="decimal", precision=10, scale=5, nullable=false)
     */
    private $nu_juros;

    /**
     * @var CampanhaComercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhacomercial;

    /**
     * @var CampanhaComercial
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercialpremio", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhacomercialpremio;

    /**
     * @var Evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_evolucao;

    /**
     * @var Situacao
     * @ManyToOne(targetEntity="Situacao")
     * @JoinColumn(name="id_situacao", referencedColumnName="id_situacao")
     */
    private $id_situacao;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var integer $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_transferidoentidade
     * @Column(name="bl_transferidoentidade", type="boolean", nullable=false)
     */
    private $bl_transferidoentidade = 0;

    /**
     * @var decimal $nu_valorliquido
     * @Column(name="nu_valorliquido", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valorliquido;

    /**
     * @var decimal $nu_valorbruto
     * @Column(name="nu_valorbruto", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valorbruto;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_operador", referencedColumnName="id_usuario")
     */
    private $id_operador;

    /**
     * @var Usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_atendente", referencedColumnName="id_usuario")
     */
    private $id_atendente;

    /**
     * @var TipoCampanha
     * @ManyToOne(targetEntity="TipoCampanha")
     * @JoinColumn(name="id_tipocampanha", referencedColumnName="id_tipocampanha")
     */
    private $id_tipocampanha;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=false)
     */
    private $nu_parcelas;

    /**
     * @var boolean $bl_contrato
     * @Column(name="bl_contrato", type="boolean", nullable=false)
     */
    private $bl_contrato;

    /**
     * @var integer $nu_diamensalidade
     * @Column(name="nu_diamensalidade", type="integer", nullable=true)
     */
    private $nu_diamensalidade;

    /**
     * @var OrigemVenda
     * @ManyToOne(targetEntity="OrigemVenda")
     * @JoinColumn(name="id_origemvenda", referencedColumnName="id_origemvenda")
     */
    private $id_origemvenda;

    /**
     * @var Prevenda
     * @ManyToOne(targetEntity="Prevenda")
     * @JoinColumn(name="id_prevenda", referencedColumnName="id_prevenda")
     */
    private $id_prevenda;

    /**
     * @var Protocolo
     * @ManyToOne(targetEntity="Protocolo")
     * @JoinColumn(name="id_protocolo", referencedColumnName="id_protocolo")
     */
    private $id_protocolo;

    /**
     * @var date $dt_agendamento
     * @Column(name="dt_agendamento", type="date", nullable=true)
     */
    private $dt_agendamento;

    /**
     * @var \DateTime $dt_confirmacao
     * @Column(name="dt_confirmacao", type="datetime", nullable=true)
     */
    private $dt_confirmacao;

    /**
     * @var integer $id_contratoafiliado
     * @Column(name="id_contratoafiliado", type="integer", nullable=true)
     */
    private $id_contratoafiliado;

    /**
     * @var string $st_observacao
     * @Column(name="st_observacao", type="string", nullable=true)
     */
    private $st_observacao;

    /**
     * @var Endereco
     * @ManyToOne(targetEntity="Endereco")
     * @JoinColumn(name="id_enderecoentrega", referencedColumnName="id_endereco")
     */
    private $id_enderecoentrega;

    /**
     * @var decimal $nu_valoratualizado
     * @Column(name="nu_valoratualizado", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valoratualizado;

    /**
     * @var Ocorrencia
     * @ManyToOne(targetEntity="Ocorrencia")
     * @JoinColumn(name="id_ocorrencia", referencedColumnName="id_ocorrencia")
     */
    private $id_ocorrencia;

    /**
     * @var string $recorrete_orderid
     * @Column(name="recorrente_orderid", type="string")
     */
    private $recorrete_orderid;

    /**
     * @var Upload $id_uploadcontrato
     * @ManyToOne(targetEntity="Upload")
     * @JoinColumn(name="id_uploadcontrato", referencedColumnName="id_upload")
     */
    private $id_uploadcontrato;
    /**
     * @var Cupom $id_cupom
     * @ManyToOne(targetEntity="Cupom")
     * @JoinColumn(name="id_cupom", referencedColumnName="id_cupom")
     */
    private $id_cupom;

    /**
     * @var decimal $nu_valorcartacredito
     * @Column(name="nu_valorcartacredito", type="decimal", nullable=true)
     */
    private $nu_valorcartacredito;

    /**
     * @var CampanhaComercial $id_campanhapontualidade
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhapontualidade", referencedColumnName="id_campanhacomercial")
     */
    private $id_campanhapontualidade;

    /**
     * @var integer $nu_viascontrato
     * @Column(name="nu_viascontrato", type="integer", nullable=false)
     */
    private $nu_viascontrato;

    /**
     * @var integer $nu_creditos
     * @Column(name="nu_creditos", type="integer", nullable=false)
     */
    private $nu_creditos;

    /**
     * @var Ocorrencia $id_ocorrencia
     * @ManyToOne(targetEntity="Ocorrencia")
     * @JoinColumn(name="id_ocorrenciaaproveitamento", referencedColumnName="id_ocorrencia")
     */
    private $id_ocorrenciaaproveitamento;

    /**
     * @var \DateTime $dt_limiterenovacao
     * @Column(name="dt_limiterenovacao", type="datetime", nullable=true)
     */
    private $dt_limiterenovacao;


    /**
     * @var boolean $bl_renovacao
     * @Column(name="bl_renovacao", type="boolean", nullable=false)
     */
    private $bl_renovacao = 0;


    /**
     * @var integer $nu_semestre
     * @Column(name="nu_semestre", type="integer", nullable=true)
     */
    private $nu_semestre;


    /**
     * @var datetime2 $dt_tentativarenovacao
     * @Column(name="dt_tentativarenovacao", type="datetime2", nullable=true)
     */
    private $dt_tentativarenovacao;

    /**
     * @var Afiliado
     * @ManyToOne(targetEntity="Afiliado")
     * @JoinColumn(name="id_afiliado", referencedColumnName="id_afiliado")
     */
    private $id_afiliado;

    /**
     * @var NucleoTelemarketing
     * @ManyToOne(targetEntity="NucleoTelemarketing")
     * @JoinColumn(name="id_nucleotelemarketing", referencedColumnName="id_nucleotelemarketing")
     */
    private $id_nucleotelemarketing;

    /**
     * @var Vendedor
     * @ManyToOne(targetEntity="Vendedor")
     * @JoinColumn(name="id_vendedor", referencedColumnName="id_vendedor")
     */
    private $id_vendedor;

    /**
     * @var string $st_siteorigem
     * @Column(name="st_siteorigem", type="string", nullable=true)
     */
    private $st_siteorigem;

    /**
     * @var boolean $bl_processamento
     * @Column(name="bl_processamento", type="boolean", nullable=false)
     */
    private $bl_processamento = 0;

    /**
     * @var boolean
     * @Column(name="bl_semsalarenovacao", type="boolean", nullable=true, options={"default":0})
     */
    private $bl_semsalarenovacao;

    /**
     * @var boolean $bl_contratocorrigido
     * @Column(name="bl_contratocorrigido", type="boolean", nullable=false)
     */
    private $bl_contratocorrigido;

    /**
     * @var \DateTime $dt_ultimaoferta
     * @Column(name="dt_ultimaoferta", type="datetime", nullable=true)
     */
    private $dt_ultimaoferta;

    /**
     * @var string $st_errorenovacao
     * @Column(name="st_errorenovacao", type="string", nullable=true)
     */
    private $st_errorenovacao;

    /**
     * @var string $utm_source
     * @Column(name="utm_source", type="string", nullable=true)
     */
    private $utm_source;

    /**
     * @var string $utm_medium
     * @Column(name="utm_medium", type="string", nullable=true)
     */
    private $utm_medium;

    /**
     * @var string $utm_campaign
     * @Column(name="utm_campaign", type="string", nullable=true)
     */
    private $utm_campaign;

    /**
     * @var string $utm_term
     * @Column(name="utm_term", type="string", nullable=true)
     */
    private $utm_term;

    /**
     * @var string $utm_content
     * @Column(name="utm_content", type="string", nullable=true)
     */
    private $utm_content;

    /**
     * @var string $origem_organica
     * @Column(name="origem_organica", type="string", nullable=true)
     */
    private $origem_organica;

    /**
     * @return Upload
     */
    public function getIdUploadcontrato()
    {
        return $this->id_uploadcontrato;
    }

    /**
     * @return Upload
     */
    public function getId_uploadcontrato()
    {
        return $this->id_uploadcontrato;
    }

    /**
     * @deprecated
     * @param Upload $id_uploadcontrato
     */
    public function setIdUploadcontrato($id_uploadcontrato)
    {
        $this->id_uploadcontrato = $id_uploadcontrato;
    }

    /**
     * @param Upload $id_uploadcontrato
     */
    public function setId_uploadcontrato($id_uploadcontrato)
    {
        $this->id_uploadcontrato = $id_uploadcontrato;
    }

    /**
     * @deprecated
     * @return CampanhaComercial
     */
    public function getIdCampanhacomercialpremio()
    {
        return $this->id_campanhacomercialpremio;
    }

    /**
     * @return CampanhaComercial
     */
    public function getId_campanhacomercialpremio()
    {
        return $this->id_campanhacomercialpremio;
    }

    /**
     * @deprecated
     * @param CampanhaComercial $id_campanhacomercialpremio
     * @return $this
     */
    public function setIdCampanhacomercialpremio($id_campanhacomercialpremio)
    {
        $this->id_campanhacomercialpremio = $id_campanhacomercialpremio;
        return $this;
    }

    /**
     * @param CampanhaComercial $id_campanhacomercialpremio
     * @return $this
     */
    public function setId_campanhacomercialpremio($id_campanhacomercialpremio)
    {
        $this->id_campanhacomercialpremio = $id_campanhacomercialpremio;
        return $this;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Usuario
     */
    public function getIdAtendente()
    {
        return $this->id_atendente;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @deprecated
     * @param \G2\Entity\Usuario $id_atendente
     * @return $this
     */
    public function setIdAtendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
        return $this;
    }

    /**
     * @param \G2\Entity\Usuario $id_atendente
     * @return $this
     */
    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
        return $this;
    }

    /**
     * @deprecated
     * @return Cupom
     */
    public function getIdCupom()
    {
        return $this->id_cupom;
    }

    /**
     * @return Cupom
     */
    public function getId_cupom()
    {
        return $this->id_cupom;
    }

    /**
     * @deprecated
     * @param Cupom $id_cupom
     * @return $this
     */
    public function setIdCupom(Cupom $id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @param Cupom $id_cupom
     * @return $this
     */
    public function setId_cupom(Cupom $id_cupom)
    {
        $this->id_cupom = $id_cupom;
        return $this;
    }

    /**
     * @deprecated
     * @return boolean
     */
    public function getBlAtivo()
    {
        return $this->bl_ativo;
    }

    /**
     * @return boolean
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @deprecated
     * @param boolean $bl_ativo
     */
    public function setBlAtivo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @param boolean $bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
    }

    /**
     * @return bool
     */
    public function getBl_transferidoentidade()
    {
        return $this->bl_transferidoentidade;
    }

    /**
     * @param bool $bl_transferidoentidade
     * @return $this
     */
    public function setBl_transferidoentidade($bl_transferidoentidade)
    {
        $this->bl_transferidoentidade = $bl_transferidoentidade;
        return $this;
    }

    /**
     * @deprecated
     * @return boolean
     */
    public function getBlContrato()
    {
        return $this->bl_contrato;
    }

    /**
     * @return boolean
     */
    public function getBl_contrato()
    {
        return $this->bl_contrato;
    }

    /**
     * @deprecated
     * @param boolean $bl_contrato
     */
    public function setBlContrato($bl_contrato)
    {
        $this->bl_contrato = $bl_contrato;
    }

    /**
     * @param boolean $bl_contrato
     */
    public function setBl_contrato($bl_contrato)
    {
        $this->bl_contrato = $bl_contrato;
    }

    /**
     * @deprecated
     * @return date
     */
    public function getDtAgendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @return \G2\Entity\date
     */
    public function getDt_agendamento()
    {
        return $this->dt_agendamento;
    }

    /**
     * @deprecated
     * @param date $dt_agendamento
     */
    public function setDtAgendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    /**
     * @param \G2\Entity\date $dt_agendamento
     */
    public function setDt_agendamento($dt_agendamento)
    {
        $this->dt_agendamento = $dt_agendamento;
    }

    /**
     * @deprecated
     * @return \DateTime
     */
    public function getDtCadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return \DateTime
     * @todo não deveria retornar um \DateTime ? (Elcio)
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @deprecated
     * @param \DateTime $dt_cadastro
     */
    public function setDtCadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @param \DateTime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @deprecated
     * @return \DateTime
     */
    public function getDtConfirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @return \DateTime
     */
    public function getDt_confirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @deprecated
     * @param \DateTime $dt_confirmacao
     */
    public function setDtConfirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
    }

    /**
     * @param \DateTime $dt_confirmacao
     */
    public function setDt_confirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
    }

    /**
     * @deprecated
     * @return \G2\Entity\CampanhaComercial
     */
    public function getIdCampanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @return \G2\Entity\CampanhaComercial
     */
    public function getId_campanhacomercial()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @deprecated
     * @param \G2\Entity\CampanhaComercial $id_campanhacomercial
     */
    public function setIdCampanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @param \G2\Entity\CampanhaComercial $id_campanhacomercial
     */
    public function setId_campanhacomercial($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getIdContratoafiliado()
    {
        return $this->id_contratoafiliado;
    }

    /**
     * @return mixed
     */
    public function getId_contratoafiliado()
    {
        return $this->id_contratoafiliado;
    }

    /**
     * @deprecated
     * @param mixed $id_contratoafiliado
     */
    public function setIdContratoafiliado($id_contratoafiliado)
    {
        $this->id_contratoafiliado = $id_contratoafiliado;
    }

    /**
     * @param mixed $id_contratoafiliado
     */
    public function setId_contratoafiliado($id_contratoafiliado)
    {
        $this->id_contratoafiliado = $id_contratoafiliado;
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getIdEnderecoentrega()
    {
        return $this->id_enderecoentrega;
    }

    /**
     * @return Endereco
     */
    public function getId_enderecoentrega()
    {
        return $this->id_enderecoentrega;
    }

    /**
     * @deprecated
     * @param mixed $id_enderecoentrega
     */
    public function setIdEnderecoentrega($id_enderecoentrega)
    {
        $this->id_enderecoentrega = $id_enderecoentrega;
    }

    /**
     * @param mixed $id_enderecoentrega
     */
    public function setId_enderecoentrega($id_enderecoentrega)
    {
        $this->id_enderecoentrega = $id_enderecoentrega;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @deprecated
     * @param int $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Evolucao
     */
    public function getIdEvolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @return \G2\Entity\Evolucao
     */
    public function getId_evolucao()
    {
        return $this->id_evolucao;
    }

    /**
     * @deprecated
     * @param \G2\Entity\Evolucao $id_evolucao
     */
    public function setIdEvolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @param \G2\Entity\Evolucao $id_evolucao
     */
    public function setId_evolucao($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
    }

    /**
     * @deprecated
     * @return \G2\Entity\FormaPagamento
     */
    public function getIdFormapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @return \G2\Entity\FormaPagamento
     */
    public function getId_formapagamento()
    {
        return $this->id_formapagamento;
    }

    /**
     * @deprecated
     * @param \G2\Entity\FormaPagamento $id_formapagamento
     */
    public function setIdFormapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    /**
     * @param \G2\Entity\FormaPagamento $id_formapagamento
     */
    public function setId_formapagamento($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getIdOrigemvenda()
    {
        return $this->id_origemvenda;
    }

    /**
     * @return int
     */
    public function getId_origemvenda()
    {
        return $this->id_origemvenda;
    }

    /**
     * @deprecated
     * @param int $id_origemvenda
     */
    public function setIdOrigemvenda($id_origemvenda)
    {
        $this->id_origemvenda = $id_origemvenda;
    }

    /**
     * @param int $id_origemvenda
     */
    public function setId_origemvenda($id_origemvenda)
    {
        $this->id_origemvenda = $id_origemvenda;
    }

    /**
     * @deprecated
     * @return mixed
     */
    public function getIdPrevenda()
    {
        return $this->id_prevenda;
    }

    /**
     * @return mixed
     */
    public function getId_prevenda()
    {
        return $this->id_prevenda;
    }

    /**
     * @deprecated
     * @param mixed $id_prevenda
     */
    public function setIdPrevenda($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
    }

    /**
     * @param mixed $id_prevenda
     */
    public function setId_prevenda($id_prevenda)
    {
        $this->id_prevenda = $id_prevenda;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Protocolo
     */
    public function getIdProtocolo()
    {
        return $this->id_protocolo;
    }

    /**
     * @return \G2\Entity\Protocolo
     */
    public function getId_protocolo()
    {
        return $this->id_protocolo;
    }

    /**
     * @deprecated
     * @param \G2\Entity\Protocolo $id_protocolo
     */
    public function setIdProtocolo($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
    }

    /**
     * @param \G2\Entity\Protocolo $id_protocolo
     */
    public function setId_protocolo($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Situacao
     */
    public function getIdSituacao()
    {
        return $this->id_situacao;
    }

    /**
     * @return \G2\Entity\Situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @deprecated
     * @param \G2\Entity\Situacao $id_situacao
     */
    public function setIdSituacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @param \G2\Entity\Situacao $id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    /**
     * @deprecated
     * @return \G2\Entity\TipoCampanha
     */
    public function getIdTipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @return \G2\Entity\TipoCampanha
     */
    public function getId_tipocampanha()
    {
        return $this->id_tipocampanha;
    }

    /**
     * @deprecated
     * @param \G2\Entity\TipoCampanha $id_tipocampanha
     */
    public function setIdTipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
    }

    /**
     * @param \G2\Entity\TipoCampanha $id_tipocampanha
     */
    public function setId_tipocampanha($id_tipocampanha)
    {
        $this->id_tipocampanha = $id_tipocampanha;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Usuario
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @return \G2\Entity\Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @deprecated
     * @param \G2\Entity\Usuario $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @param \G2\Entity\Usuario $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getIdUsuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @return int
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /*
     * Metodos usando o padrão de nomenclatura
     * */

    /**
     * @deprecated
     * @param int $id_usuariocadastro
     */
    public function setIdUsuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @param int $id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @deprecated
     * @param int $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @deprecated
     * @return integer
     */
    public function getNuDescontoporcentagem()
    {
        return $this->nu_descontoporcentagem;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_descontoporcentagem()
    {
        return $this->nu_descontoporcentagem;
    }

    /**
     * @deprecated
     * @param integer $nu_descontoporcentagem
     */
    public function setNuDescontoporcentagem($nu_descontoporcentagem)
    {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
    }

    /**
     * @param \G2\Entity\decimal $nu_descontoporcentagem
     */
    public function setNu_descontoporcentagem($nu_descontoporcentagem)
    {
        $this->nu_descontoporcentagem = $nu_descontoporcentagem;
    }

    /**
     * @deprecated
     * @return number
     */
    public function getNuDescontovalor()
    {
        return $this->nu_descontovalor;
    }

    /**
     * @return \G2\Entity\numeric
     */
    public function getNu_descontovalor()
    {
        return $this->nu_descontovalor;
    }

    /**
     * @deprecated
     * @param $nu_descontovalor
     */
    public function setNuDescontovalor($nu_descontovalor)
    {
        $this->nu_descontovalor = $nu_descontovalor;
    }

    /**
     * @param \G2\Entity\numeric $nu_descontovalor
     */
    public function setNu_descontovalor($nu_descontovalor)
    {
        $this->nu_descontovalor = $nu_descontovalor;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getNuDiamensalidade()
    {
        return $this->nu_diamensalidade;
    }

    /**
     * @return int
     */
    public function getNu_diamensalidade()
    {
        return $this->nu_diamensalidade;
    }

    /**
     * @deprecated
     * @param int $nu_diamensalidade
     */
    public function setNuDiamensalidade($nu_diamensalidade)
    {
        $this->nu_diamensalidade = $nu_diamensalidade;
    }

    /**
     * @param int $nu_diamensalidade
     */
    public function setNu_diamensalidade($nu_diamensalidade)
    {
        $this->nu_diamensalidade = $nu_diamensalidade;
    }

    /**
     * @deprecated
     * @return number
     */
    public function getNuJuros()
    {
        return $this->nu_juros;
    }

    /**
     * @return \G2\Entity\numeric
     */
    public function getNu_juros()
    {
        return $this->nu_juros;
    }

    /**
     * @deprecated
     * @param number $nu_juros
     */
    public function setNuJuros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @param \G2\Entity\numeric $nu_juros
     */
    public function setNu_juros($nu_juros)
    {
        $this->nu_juros = $nu_juros;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getNuParcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @return int
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @deprecated
     * @param int $nu_parcelas
     */
    public function setNuParcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @param int $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @deprecated
     * @return integer
     */
    public function getNuValoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_valoratualizado()
    {
        return $this->nu_valoratualizado;
    }

    /**
     * @deprecated
     * @param integer $nu_valoratualizado
     */
    public function setNuValoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
    }

    /**
     * @param \G2\Entity\decimal $nu_valoratualizado
     */
    public function setNu_valoratualizado($nu_valoratualizado)
    {
        $this->nu_valoratualizado = $nu_valoratualizado;
    }

    /**
     * @deprecated
     * @return number
     */
    public function getNuValorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @deprecated
     * @param number $nu_valorbruto
     */
    public function setNuValorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @param \G2\Entity\decimal $nu_valorbruto
     */
    public function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @deprecated
     * @return number
     */
    public function getNuValorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @return \G2\Entity\decimal
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @deprecated
     * @param number $nu_valorliquido
     */
    public function setNuValorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @param \G2\Entity\decimal $nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @deprecated
     * @return string
     */
    public function getStObservacao()
    {
        return $this->st_observacao;
    }

    /**
     * @return string
     */
    public function getSt_observacao()
    {
        return $this->st_observacao;
    }

    /**
     * @deprecated
     * @param string $st_observacao
     */
    public function setStObservacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
    }

    /**
     * @param string $st_observacao
     */
    public function setSt_observacao($st_observacao)
    {
        $this->st_observacao = $st_observacao;
    }

    /**
     * @deprecated
     * @return integer
     */
    public function getIdOcorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @return string
     */
    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @deprecated
     * @param integer $id_ocorrencia
     */
    public function setIdOcorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @param string $id_ocorrencia
     */
    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecorreteOrderid()
    {
        return $this->recorrete_orderid;
    }

    /**
     * @deprecated
     * @param string $recorrete_orderid
     */
    public function setRecorreteOrderid($recorrete_orderid)
    {
        $this->recorrete_orderid = $recorrete_orderid;
    }

    /**
     * @deprecated
     * @return number
     */
    public function getNuValorCartaCredito()
    {
        return $this->nu_valorcartacredito;
    }

    /**
     * @return decimal
     */
    public function getNu_valorcartacredito()
    {
        return $this->nu_valorcartacredito;
    }

    /**
     * @deprecated
     * @param $nu_valorcartacredito
     * @return $this
     */
    public function setNuValorCartaCredito($nu_valorcartacredito)
    {
        $this->nu_valorcartacredito = $nu_valorcartacredito;
        return $this;
    }

    /**
     * @param $nu_valorcartacredito
     * @return $this
     */
    public function setNu_valorcartacredito($nu_valorcartacredito)
    {
        $this->nu_valorcartacredito = $nu_valorcartacredito;
        return $this;
    }

    /**
     * @deprecated
     * @return \G2\Entity\CampanhaComercial
     */
    public function getIdCampanhaPontualidade()
    {
        return $this->id_campanhapontualidade;
    }

    /**
     * @return \G2\Entity\CampanhaComercial
     */
    public function getId_campanhapontualidade()
    {
        return $this->id_campanhapontualidade;
    }

    /**
     * @deprecated
     * @param \G2\Entity\CampanhaComercial $id_campanhapontualidade
     * @return \G2\Entity\Venda
     */
    public function setIdCampanhaPontualidade(CampanhaComercial $id_campanhapontualidade)
    {
        $this->id_campanhapontualidade = $id_campanhapontualidade;
        return $this;
    }

    /**
     * @param int|\G2\Entity\CampanhaComercial $id_campanhapontualidade
     * @return $this
     */
    public function setId_campanhapontualidade($id_campanhapontualidade)
    {
        $this->id_campanhapontualidade = $id_campanhapontualidade;
        return $this;
    }

    /**
     * @deprecated
     * @return \G2\Entity\Operador
     */
    public function getIdOperador()
    {
        return $this->id_operador;
    }

    /**
     * @return Operador
     */
    public function getId_operador()
    {
        return $this->id_operador;
    }

    /**
     * @deprecated
     * @param Operador $id_operador
     */
    public function setIdOperador($id_operador)
    {
        $this->id_operador = $id_operador;
    }

    /**
     * @param Operador $id_operador
     */
    public function setId_operador($id_operador)
    {
        $this->id_operador = $id_operador;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getNuViasContrato()
    {
        return $this->nu_viascontrato;
    }

    /**
     * @return int
     */
    public function getNu_viascontrato()
    {
        return $this->nu_viascontrato;
    }

    /**
     * @deprecated
     * @param int $nu_viascontrato
     */
    public function setNuViasContrato($nu_viascontrato)
    {
        $this->nu_viascontrato = $nu_viascontrato;
    }

    /**
     * @param int $nu_viascontrato
     */
    public function setNu_viascontrato($nu_viascontrato)
    {
        $this->nu_viascontrato = $nu_viascontrato;
    }

    /**
     * @deprecated
     * @return int
     */
    public function getNuCreditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @return int
     */
    public function getNu_creditos()
    {
        return $this->nu_creditos;
    }

    /**
     * @deprecated
     * @param int $nu_creditos
     */
    public function setNuCreditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
    }

    /**
     * @param int $nu_creditos
     */
    public function setNu_creditos($nu_creditos)
    {
        $this->nu_creditos = $nu_creditos;
    }

    /**
     * @deprecated
     * @return Ocorrencia
     */
    public function getIdOcorrenciaAproveitamento()
    {
        return $this->id_ocorrenciaaproveitamento;
    }

    /**
     * @return Ocorrencia
     */
    public function getId_ocorrenciaaproveitamento()
    {
        return $this->id_ocorrenciaaproveitamento;
    }

    /**
     * @deprecated
     * @param Ocorrencia $id_ocorrenciaaproveitamento
     */
    public function setIdOcorrenciaAproveitamento($id_ocorrenciaaproveitamento)
    {
        $this->id_ocorrenciaaproveitamento = $id_ocorrenciaaproveitamento;
    }

    /**
     * @param Ocorrencia $id_ocorrenciaaproveitamento
     */
    public function setId_ocorrenciaaproveitamento($id_ocorrenciaaproveitamento)
    {
        $this->id_ocorrenciaaproveitamento = $id_ocorrenciaaproveitamento;
    }

    /**
     * @deprecated
     * @return \DateTime
     */
    public function getDtAtualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @return \DateTime
     */
    public function getDt_atualizado()
    {
        return $this->dt_atualizado;
    }

    /**
     * @deprecated
     * @param \DateTime $dt_atualizado
     */
    public function setDtAtualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @param \DateTime $dt_atualizado
     */
    public function setDt_atualizado($dt_atualizado)
    {
        $this->dt_atualizado = $dt_atualizado;
    }

    /**
     * @deprecated
     * @return \DateTime
     */
    public function getDtLimiterenovacao()
    {
        return $this->dt_limiterenovacao;
    }

    public function getDt_limiterenovacao()
    {
        return $this->dt_limiterenovacao;
    }

    /**
     * @deprecated
     * @param $dt_limiterenovacao
     * @return $this
     */
    public function setDtLimiterenovacao($dt_limiterenovacao)
    {
        $this->dt_limiterenovacao = $dt_limiterenovacao;
        return $this;
    }

    public function setDt_limiterenovacao($dt_limiterenovacao)
    {
        $this->dt_limiterenovacao = $dt_limiterenovacao;
        return $this;
    }

    public function getBl_renovacao()
    {
        return $this->bl_renovacao;
    }

    public function setBl_renovacao($bl_renovacao)
    {
        $this->bl_renovacao = $bl_renovacao;
    }


    public function getNu_semestre()
    {
        return $this->nu_semestre;
    }

    public function setNu_semestre($nu_semestre)
    {
        $this->nu_semestre = $nu_semestre;
    }

    public function getDt_tentativarenovacao()
    {
        return $this->dt_tentativarenovacao;
    }

    public function setDt_tentativarenovacao($dt_tentativarenovacao)
    {
        $this->dt_tentativarenovacao = $dt_tentativarenovacao;
    }

    /**
     * @return Afiliado
     */
    public function getId_afiliado()
    {
        return $this->id_afiliado;
    }

    /**
     * @param Afiliado $id_afiliado
     * @return $this
     */
    public function setId_afiliado($id_afiliado)
    {
        $this->id_afiliado = $id_afiliado;
        return $this;
    }

    /**
     * @return NucleoTelemarketing
     */
    public function getId_nucleotelemarketing()
    {
        return $this->id_nucleotelemarketing;
    }

    /**
     * @param NucleoTelemarketing $id_nucleotelemarketing
     * @return $this
     */
    public function setId_nucleotelemarketing($id_nucleotelemarketing)
    {
        $this->id_nucleotelemarketing = $id_nucleotelemarketing;
        return $this;
    }

    /**
     * @return Vendedor
     */
    public function getId_vendedor()
    {
        return $this->id_vendedor;
    }

    /**
     * @param Vendedor $id_vendedor
     * @return $this
     */
    public function setId_vendedor($id_vendedor)
    {
        $this->id_vendedor = $id_vendedor;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_siteorigem()
    {
        return $this->st_siteorigem;
    }

    /**
     * @param string $st_siteorigem
     * @return $this
     */
    public function setSt_siteorigem($st_siteorigem)
    {
        $this->st_siteorigem = $st_siteorigem;
        return $this;
    }

    /**
     * @return bool
     */
    public function getBl_processamento()
    {
        return $this->bl_processamento;
    }

    /**
     * @param bool $bl_processamento
     */
    public function setBl_processamento($bl_processamento)
    {
        $this->bl_processamento = $bl_processamento;
    }

    /**
     * @return bool
     */
    public function getBl_contratocorrigido()
    {
        return $this->bl_contratocorrigido;
    }

    /**
     * @param bool $bl_contratocorrigido
     */
    public function setBl_contratocorrigido($bl_contratocorrigido)
    {
        $this->bl_contratocorrigido = $bl_contratocorrigido;
    }

    /**
     * @return bool
     */
    public function getBl_semsalarenovacao()
    {
        return $this->bl_semsalarenovacao;
    }

    /**
     * @param bool $bl_semsalarenovacao
     * @return Venda
     */
    public function setBl_semsalarenovacao($bl_semsalarenovacao)
    {
        $this->bl_semsalarenovacao = $bl_semsalarenovacao;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDt_ultimaoferta()
    {
        return $this->dt_ultimaoferta;
    }

    /**
     * @param \DateTime $dt_ultimaoferta
     * @return $this
     */
    public function setDt_ultimaoferta($dt_ultimaoferta)
    {
        $this->dt_ultimaoferta = $dt_ultimaoferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getSt_errorenovacao()
    {
        return $this->st_errorenovacao;
    }

    /**
     * @param string $st_errorenovacao
     * @return $this
     */
    public function setSt_errorenovacao($st_errorenovacao)
    {
        $this->st_errorenovacao = $st_errorenovacao;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtm_source()
    {
        return $this->utm_source;
    }

    /**
     * @param string $utm_source
     * @return Venda
     */
    public function setUtm_source($utm_source)
    {
        $this->utm_source = $utm_source;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtm_medium()
    {
        return $this->utm_medium;
    }

    /**
     * @param string $utm_medium
     * @return Venda
     */
    public function setUtm_medium($utm_medium)
    {
        $this->utm_medium = $utm_medium;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtm_campaign()
    {
        return $this->utm_campaign;
    }

    /**
     * @param string $utm_campaign
     * @return Venda
     */
    public function setUtm_campaign($utm_campaign)
    {
        $this->utm_campaign = $utm_campaign;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtm_term()
    {
        return $this->utm_term;
    }

    /**
     * @param string $utm_term
     * @return Venda
     */
    public function setUtm_term($utm_term)
    {
        $this->utm_term = $utm_term;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtm_content()
    {
        return $this->utm_content;
    }

    /**
     * @param string $utm_content
     * @return Venda
     */
    public function setUtm_content($utm_content)
    {
        $this->utm_content = $utm_content;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigem_organica()
    {
        return $this->origem_organica;
    }

    /**
     * @param string $origem_organica
     * @return Venda
     */
    public function setOrigem_organica($origem_organica)
    {
        $this->origem_organica = $origem_organica;
        return $this;
    }


}

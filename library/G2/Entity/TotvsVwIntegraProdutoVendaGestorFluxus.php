<?php

namespace G2\Entity;

use \Doctrine\Mapping as ORM;

/**
 * @Entity(repositoryClass="G2\Repository\TotvsVwIntegraProdutoVendaGestorFluxs")
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table( name="totvs.vw_Integra_Produto_Venda_GestorFluxus")
 */
class TotvsVwIntegraProdutoVendaGestorFluxus
{
    /**
     * @var smallint $codcoligada_fcfo
     * @Id
     * @Column(name="CODCOLIGADA_FCFO", type="smallint", length=2)
     */
    private $codcoligada_fcfo;

    /**
     * @var integer $idmov_titmov
     * @Column(name="IDMOV_TITMOV", type="integer")
     *
     */
    private $idmov_titmov;

    /**
     * @var integer $nseqitmov_titmmov
     * @Column(name="NSEQITMOV_TITMOV", type="integer")
     *
     */
    private $nseqitmov_titmmov;

    /**
     * @var integer $numerosequencial_titmmov
     * @Column(name="NUMEROSEQUENCIAL_TITMOV", type="integer")
     *
     */
    private $numerosequencial_titmmov;

    /**
     * @var integer $idprd_titmmov
     * @Column(name="IDPRD_TITMOV", type="integer")
     *
     */
    private $idprd_titmmov;

    /**
     * @var string $quantidade_titmmov
     * @Column(name="QUANTIDADE_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $quantidade_titmmov;

    /**
     * @var string $precounitario_titmmov
     * @Column(name="PRECOUNITARIO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $precounitario_titmmov;

    /**
     * @var string $precotabela_titmov
     * @Column(name="PRECOTABELA_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $precotabela_titmov;

    /**
     * @var datetime $dataemissao_titmmov
     * @Column(name="DATAEMISSAO_TITMOV", type="string", length=40)
     *
     */
    private $dataemissao_titmmov;

    /**
     * @var string $codund_titmmov
     * @Column(name="CODUND_TITMOV ", type="string", length=5)
     *
     */
    private $codund_titmmov;

    /**
     * @var string $quantidadeareceber_titmov
     * @Column(name="QUANTIDADEARECEBER_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $quantidadeareceber_titmov;

    /**
     * @var string $valorunitario_titmov
     * @Column(name="VALORUNITARIO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorunitario_titmov;

    /**
     * @var string $pquantidadeareceber_titmov
     * @Column(name="VALORFINANCEIRO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorfinanceiro_titmov;

    /**
     * @var string $aliqordenacao_titmov
     * @Column(name="ALIQORDENACAO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $aliqordenacao_titmov;

    /**
     * @var string $quantidadeoriginal_titmov
     * @Column(name="QUANTIDADEORIGINAL_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $quantidadeoriginal_titmov;

    /**
     * @var integer $idnat_titmmov
     * @Column(name="IDNAT_TITMOV", type="integer")
     *
     */
    private $idnat_titmmov;

    /**
     * @var integer $flag_titmmov
     * @Column(name="FLAG_TITMOV", type="integer")
     *
     */
    private $flag_titmmov;

    /**
     * @var string $fatorconvund_titmov
     * @Column(name="FATORCONVUND_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $fatorconvund_titmov;

    /**
     * @var string $valortotalitem_titmov
     * @Column(name="VALORTOTALITEM_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valortotalitem_titmov;

    /**
     * @var integer $codfilial_titmmov
     * @Column(name="CODFILIAL_TITMOV", type="integer")
     *
     */
    private $codfilial_titmmov;

    /**
     * @var string $quantidadeseparada_titmov
     * @Column(name="QUANTIDADESEPARADA_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $quantidadeseparada_titmov;

    /**
     * @var string $percentcomissao
     * @Column(name="PERCENTCOMISSAO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $percentcomissao;

    /**
     * @var string $comissaorepres_titmov
     * @Column(name="COMISSAOREPRES_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $comissaorepres_titmov;


    /**
     * @var string $valorescrituracao_titmov
     * @Column(name="VALORESCRITURACAO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorescrituracao_titmov;

    /**
     * @var string $valorfinpedido_titmov
     * @Column(name="VALORFINPEDIDO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorfinpedido_titmov;

    /**
     * @var string $valorpfrm1_titmov
     * @Column(name="VALORPFRM1_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorpfrm1_titmov;

    /**
     * @var string $valorpfrm2_titmov
     * @Column(name="VALORPFRM2_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorpfrm2_titmov;

    /**
     * @var string $precoeditado_titmov
     * @Column(name="PRECOEDITADO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $precoeditado_titmov;

    /**
     * @var integer $qtdevolumeunitario
     * @Column(name="QTDEVOLUMEUNITARIO_TITMOV", type="integer")
     *
     */
    private $qtdevolumeunitario_titmmov;

    /**
     * @var string $cst_titmmov
     * @Column(name="CST_TITMOV ", type="string", length=3)
     *
     */
    private $cst_titmmov;

    /**
     * @var string $valordesccondiconalitm_titmov
     * @Column(name="VALORDESCCONDICONALITM_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valordesccondiconalitm_titmov;

    /**
     * @var string $valordespcondicionalitm_titmov
     * @Column(name="VALORDESPCONDICIONALITM_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valordespcondicionalitm_titmov;

    /**
     * @var datetime $dataorcamento_titmmov
     * @Column(name="DATAORCAMENTO_TITMOV", type="string", length=40)
     *
     */
    private $dataorcamento_titmmov;

    /**
     * @var string $codtbtorcamento_titmmov
     * @Column(name="CODTBORCAMENTO_TITMOV ", type="string", length=40)
     *
     */
    private $codtbtorcamento_titmmov;

    /**
     * @var string $valoruntorcamento_titmov
     * @Column(name="VALORUNTORCAMENTO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valoruntorcamento_titmov;

    /**
     * @var string $valserviconfe_titmov
     * @Column(name="VALSERVICONFE_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valserviconfe_titmov;

    /**
     * @var string $codloc_titmmov
     * @Column(name="CODLOC_TITMOV ", type="string", length=15)
     *
     */
    private $codloc_titmmov;

    /**
     * @var string $valorbem_titmov
     * @Column(name="VALORBEM_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorbem_titmov;

    /**
     * @var string $valorliquido_titmov
     * @Column(name="VALORLIQUIDO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorliquido_titmov;

    /**
     * @var string $codloc_titmmov
     * @Column(name="CODMUNSERVICO_TITMOV ", type="string", length=20)
     *
     */
    private $codmunservico_titmmov;

    /**
     * @var string $codetdmunserv_titmmov
     * @Column(name="CODETDMUNSERV_TITMOV ", type="string", length=2)
     *
     */
    private $codetdmunserv_titmmov;

    /**
     * @var string $rateioccustodepto_titmov
     * @Column(name="RATEIOCCUSTODEPTO_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $rateioccustodepto_titmov;

    /**
     * @var string $valorbrutoitem_titmov
     * @Column(name="VALORBRUTOITEM_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorbrutoitem_titmov;

    /**
     * @var string $valorbrutoitemorig_titmov
     * @Column(name="VALORBRUTOITEMORIG_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $valorbrutoitemorig_titmov;

    /**
     * @var string $codcoltborcamento_titmmov
     * @Column(name="CODCOLTBORCAMENTO_TITMOV ", type="string", length=2)
     *
     */
    private $codcoltborcamento_titmmov;

    /**
     * @var string $quantidadetotal_titmov
     * @Column(name="QUANTIDADETOTAL_TITMOV", type="decimal", precision=10, scale=2)
     *
     */
    private $quantidadetotal_titmov;

    /**
     * @var integer $produtosubstituto_titmmov
     * @Column(name="PRODUTOSUBSTITUTO_TITMOV", type="integer")
     *
     */
    private $produtosubstituto_titmmov;

    /**
     * @var integer $precounitarioselec_titmmov
     * @Column(name="PRECOUNITARIOSELEC_TITMOV", type="integer")
     *
     */
    private $precounitarioselec_titmmov;

    /**
     * @var string $historicocurto
     * @Column(name="HISTORICOCURTO", type="string", length=255)
     *
     */
    private $historicocurto;

    /**
     * @var string $valor
     * @Column(name="VALOR", type="decimal", precision=10, scale=2)
     *
     */
    private $valor;

    /**
     * @var string $percentual
     * @Column(name="PERCENTUAL", type="decimal", precision=10, scale=2)
     *
     */
    private $percentual;

    /**
     * @var string $codccusto_titmmov
     * @Column(name="CODCCUSTO", type="string")
     */
    private $codccusto_titmmov;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer")
     */
    private $id_venda;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer")
     */
    private $id_produto;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer")
     */
    private $id_entidade;

    /**
     * @return mixed
     */
    public function getIdEntidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param mixed $id_entidade
     */
    public function setIdEntidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getIdVenda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setIdVenda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return int
     */
    public function getIdProduto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     */
    public function setIdProduto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return smallint
     */
    public function getCodcoligadaFcfo()
    {
        return $this->codcoligada_fcfo;
    }

    /**
     * @param smallint $codcoligada_fcfo
     */
    public function setCodcoligadaFcfo($codcoligada_fcfo)
    {
        $this->codcoligada_fcfo = $codcoligada_fcfo;
    }

    /**
     * @return int
     */
    public function getIdmovTitmov()
    {
        return $this->idmov_titmov;
    }

    /**
     * @param int $idmov_titmov
     */
    public function setIdmovTitmov($idmov_titmov)
    {
        $this->idmov_titmov = $idmov_titmov;
    }

    /**
     * @return int
     */
    public function getNseqitmovTitmmov()
    {
        return $this->nseqitmov_titmmov;
    }

    /**
     * @param int $nseqitmov_titmmov
     */
    public function setNseqitmovTitmmov($nseqitmov_titmmov)
    {
        $this->nseqitmov_titmmov = $nseqitmov_titmmov;
    }

    /**
     * @return int
     */
    public function getNumerosequencialTitmmov()
    {
        return $this->numerosequencial_titmmov;
    }

    /**
     * @param int $numerosequencial_titmmov
     */
    public function setNumerosequencialTitmmov($numerosequencial_titmmov)
    {
        $this->numerosequencial_titmmov = $numerosequencial_titmmov;
    }

    /**
     * @return int
     */
    public function getIdprdTitmmov()
    {
        return $this->idprd_titmmov;
    }

    /**
     * @param int $idprd_titmmov
     */
    public function setIdprdTitmmov($idprd_titmmov)
    {
        $this->idprd_titmmov = $idprd_titmmov;
    }

    /**
     * @return string
     */
    public function getQuantidadeTitmmov()
    {
        return $this->quantidade_titmmov;
    }

    /**
     * @param string $quantidade_titmmov
     */
    public function setQuantidadeTitmmov($quantidade_titmmov)
    {
        $this->quantidade_titmmov = $quantidade_titmmov;
    }

    /**
     * @return string
     */
    public function getPrecounitarioTitmmov()
    {
        return $this->precounitario_titmmov;
    }

    /**
     * @param string $precounitario_titmmov
     */
    public function setPrecounitarioTitmmov($precounitario_titmmov)
    {
        $this->precounitario_titmmov = $precounitario_titmmov;
    }

    /**
     * @return string
     */
    public function getPrecotabelaTitmov()
    {
        return $this->precotabela_titmov;
    }

    /**
     * @param string $precotabela_titmov
     */
    public function setPrecotabelaTitmov($precotabela_titmov)
    {
        $this->precotabela_titmov = $precotabela_titmov;
    }

    /**
     * @return datetime
     */
    public function getDataemissaoTitmmov()
    {
        return $this->dataemissao_titmmov;
    }

    /**
     * @param datetime $dataemissao_titmmov
     */
    public function setDataemissaoTitmmov($dataemissao_titmmov)
    {
        $this->dataemissao_titmmov = $dataemissao_titmmov;
    }

    /**
     * @return string
     */
    public function getCodundTitmmov()
    {
        return $this->codund_titmmov;
    }

    /**
     * @param string $codund_titmmov
     */
    public function setCodundTitmmov($codund_titmmov)
    {
        $this->codund_titmmov = $codund_titmmov;
    }

    /**
     * @return string
     */
    public function getQuantidadeareceberTitmov()
    {
        return $this->quantidadeareceber_titmov;
    }

    /**
     * @param string $quantidadeareceber_titmov
     */
    public function setQuantidadeareceberTitmov($quantidadeareceber_titmov)
    {
        $this->quantidadeareceber_titmov = $quantidadeareceber_titmov;
    }

    /**
     * @return string
     */
    public function getValorunitarioTitmov()
    {
        return $this->valorunitario_titmov;
    }

    /**
     * @param string $valorunitario_titmov
     */
    public function setValorunitarioTitmov($valorunitario_titmov)
    {
        $this->valorunitario_titmov = $valorunitario_titmov;
    }

    /**
     * @return string
     */
    public function getValorfinanceiroTitmov()
    {
        return $this->valorfinanceiro_titmov;
    }

    /**
     * @param string $valorfinanceiro_titmov
     */
    public function setValorfinanceiroTitmov($valorfinanceiro_titmov)
    {
        $this->valorfinanceiro_titmov = $valorfinanceiro_titmov;
    }

    /**
     * @return string
     */
    public function getAliqordenacaoTitmov()
    {
        return $this->aliqordenacao_titmov;
    }

    /**
     * @param string $aliqordenacao_titmov
     */
    public function setAliqordenacaoTitmov($aliqordenacao_titmov)
    {
        $this->aliqordenacao_titmov = $aliqordenacao_titmov;
    }

    /**
     * @return string
     */
    public function getQuantidadeoriginalTitmov()
    {
        return $this->quantidadeoriginal_titmov;
    }

    /**
     * @param string $quantidadeoriginal_titmov
     */
    public function setQuantidadeoriginalTitmov($quantidadeoriginal_titmov)
    {
        $this->quantidadeoriginal_titmov = $quantidadeoriginal_titmov;
    }

    /**
     * @return int
     */
    public function getIdnatTitmmov()
    {
        return $this->idnat_titmmov;
    }

    /**
     * @param int $idnat_titmmov
     */
    public function setIdnatTitmmov($idnat_titmmov)
    {
        $this->idnat_titmmov = $idnat_titmmov;
    }

    /**
     * @return int
     */
    public function getFlagTitmmov()
    {
        return $this->flag_titmmov;
    }

    /**
     * @param int $flag_titmmov
     */
    public function setFlagTitmmov($flag_titmmov)
    {
        $this->flag_titmmov = $flag_titmmov;
    }

    /**
     * @return string
     */
    public function getFatorconvundTitmov()
    {
        return $this->fatorconvund_titmov;
    }

    /**
     * @param string $fatorconvund_titmov
     */
    public function setFatorconvundTitmov($fatorconvund_titmov)
    {
        $this->fatorconvund_titmov = $fatorconvund_titmov;
    }

    /**
     * @return string
     */
    public function getValortotalitemTitmov()
    {
        return $this->valortotalitem_titmov;
    }

    /**
     * @param string $valortotalitem_titmov
     */
    public function setValortotalitemTitmov($valortotalitem_titmov)
    {
        $this->valortotalitem_titmov = $valortotalitem_titmov;
    }

    /**
     * @return int
     */
    public function getCodfilialTitmmov()
    {
        return $this->codfilial_titmmov;
    }

    /**
     * @param int $codfilial_titmmov
     */
    public function setCodfilialTitmmov($codfilial_titmmov)
    {
        $this->codfilial_titmmov = $codfilial_titmmov;
    }

    /**
     * @return string
     */
    public function getQuantidadeseparadaTitmov()
    {
        return $this->quantidadeseparada_titmov;
    }

    /**
     * @param string $quantidadeseparada_titmov
     */
    public function setQuantidadeseparadaTitmov($quantidadeseparada_titmov)
    {
        $this->quantidadeseparada_titmov = $quantidadeseparada_titmov;
    }

    /**
     * @return string
     */
    public function getPercentcomissao()
    {
        return $this->percentcomissao;
    }

    /**
     * @param string $percentcomissao
     */
    public function setPercentcomissao($percentcomissao)
    {
        $this->percentcomissao = $percentcomissao;
    }

    /**
     * @return string
     */
    public function getComissaorepresTitmov()
    {
        return $this->comissaorepres_titmov;
    }

    /**
     * @param string $comissaorepres_titmov
     */
    public function setComissaorepresTitmov($comissaorepres_titmov)
    {
        $this->comissaorepres_titmov = $comissaorepres_titmov;
    }

    /**
     * @return string
     */
    public function getValorescrituracaoTitmov()
    {
        return $this->valorescrituracao_titmov;
    }

    /**
     * @param string $valorescrituracao_titmov
     */
    public function setValorescrituracaoTitmov($valorescrituracao_titmov)
    {
        $this->valorescrituracao_titmov = $valorescrituracao_titmov;
    }

    /**
     * @return string
     */
    public function getValorfinpedidoTitmov()
    {
        return $this->valorfinpedido_titmov;
    }

    /**
     * @param string $valorfinpedido_titmov
     */
    public function setValorfinpedidoTitmov($valorfinpedido_titmov)
    {
        $this->valorfinpedido_titmov = $valorfinpedido_titmov;
    }

    /**
     * @return string
     */
    public function getValorpfrm1Titmov()
    {
        return $this->valorpfrm1_titmov;
    }

    /**
     * @param string $valorpfrm1_titmov
     */
    public function setValorpfrm1Titmov($valorpfrm1_titmov)
    {
        $this->valorpfrm1_titmov = $valorpfrm1_titmov;
    }

    /**
     * @return string
     */
    public function getValorpfrm2Titmov()
    {
        return $this->valorpfrm2_titmov;
    }

    /**
     * @param string $valorpfrm2_titmov
     */
    public function setValorpfrm2Titmov($valorpfrm2_titmov)
    {
        $this->valorpfrm2_titmov = $valorpfrm2_titmov;
    }

    /**
     * @return string
     */
    public function getPrecoeditadoTitmov()
    {
        return $this->precoeditado_titmov;
    }

    /**
     * @param string $precoeditado_titmov
     */
    public function setPrecoeditadoTitmov($precoeditado_titmov)
    {
        $this->precoeditado_titmov = $precoeditado_titmov;
    }

    /**
     * @return int
     */
    public function getQtdevolumeunitarioTitmmov()
    {
        return $this->qtdevolumeunitario_titmmov;
    }

    /**
     * @param int $qtdevolumeunitario_titmmov
     */
    public function setQtdevolumeunitarioTitmmov($qtdevolumeunitario_titmmov)
    {
        $this->qtdevolumeunitario_titmmov = $qtdevolumeunitario_titmmov;
    }

    /**
     * @return string
     */
    public function getCstTitmmov()
    {
        return $this->cst_titmmov;
    }

    /**
     * @param string $cst_titmmov
     */
    public function setCstTitmmov($cst_titmmov)
    {
        $this->cst_titmmov = $cst_titmmov;
    }

    /**
     * @return string
     */
    public function getValordesccondiconalitmTitmov()
    {
        return $this->valordesccondiconalitm_titmov;
    }

    /**
     * @param string $valordesccondiconalitm_titmov
     */
    public function setValordesccondiconalitmTitmov($valordesccondiconalitm_titmov)
    {
        $this->valordesccondiconalitm_titmov = $valordesccondiconalitm_titmov;
    }

    /**
     * @return string
     */
    public function getValordespcondicionalitmTitmov()
    {
        return $this->valordespcondicionalitm_titmov;
    }

    /**
     * @param string $valordespcondicionalitm_titmov
     */
    public function setValordespcondicionalitmTitmov($valordespcondicionalitm_titmov)
    {
        $this->valordespcondicionalitm_titmov = $valordespcondicionalitm_titmov;
    }

    /**
     * @return datetime
     */
    public function getDataorcamentoTitmmov()
    {
        return $this->dataorcamento_titmmov;
    }

    /**
     * @param datetime $dataorcamento_titmmov
     */
    public function setDataorcamentoTitmmov($dataorcamento_titmmov)
    {
        $this->dataorcamento_titmmov = $dataorcamento_titmmov;
    }

    /**
     * @return string
     */
    public function getCodtbtorcamentoTitmmov()
    {
        return $this->codtbtorcamento_titmmov;
    }

    /**
     * @param string $codtbtorcamento_titmmov
     */
    public function setCodtbtorcamentoTitmmov($codtbtorcamento_titmmov)
    {
        $this->codtbtorcamento_titmmov = $codtbtorcamento_titmmov;
    }

    /**
     * @return string
     */
    public function getValoruntorcamentoTitmov()
    {
        return $this->valoruntorcamento_titmov;
    }

    /**
     * @param string $valoruntorcamento_titmov
     */
    public function setValoruntorcamentoTitmov($valoruntorcamento_titmov)
    {
        $this->valoruntorcamento_titmov = $valoruntorcamento_titmov;
    }

    /**
     * @return string
     */
    public function getValserviconfeTitmov()
    {
        return $this->valserviconfe_titmov;
    }

    /**
     * @param string $valserviconfe_titmov
     */
    public function setValserviconfeTitmov($valserviconfe_titmov)
    {
        $this->valserviconfe_titmov = $valserviconfe_titmov;
    }

    /**
     * @return string
     */
    public function getCodlocTitmmov()
    {
        return $this->codloc_titmmov;
    }

    /**
     * @param string $codloc_titmmov
     */
    public function setCodlocTitmmov($codloc_titmmov)
    {
        $this->codloc_titmmov = $codloc_titmmov;
    }

    /**
     * @return string
     */
    public function getValorbemTitmov()
    {
        return $this->valorbem_titmov;
    }

    /**
     * @param string $valorbem_titmov
     */
    public function setValorbemTitmov($valorbem_titmov)
    {
        $this->valorbem_titmov = $valorbem_titmov;
    }

    /**
     * @return string
     */
    public function getValorliquidoTitmov()
    {
        return $this->valorliquido_titmov;
    }

    /**
     * @param string $valorliquido_titmov
     */
    public function setValorliquidoTitmov($valorliquido_titmov)
    {
        $this->valorliquido_titmov = $valorliquido_titmov;
    }

    /**
     * @return string
     */
    public function getCodmunservicoTitmmov()
    {
        return $this->codmunservico_titmmov;
    }

    /**
     * @param string $codmunservico_titmmov
     */
    public function setCodmunservicoTitmmov($codmunservico_titmmov)
    {
        $this->codmunservico_titmmov = $codmunservico_titmmov;
    }

    /**
     * @return string
     */
    public function getCodetdmunservTitmmov()
    {
        return $this->codetdmunserv_titmmov;
    }

    /**
     * @param string $codetdmunserv_titmmov
     */
    public function setCodetdmunservTitmmov($codetdmunserv_titmmov)
    {
        $this->codetdmunserv_titmmov = $codetdmunserv_titmmov;
    }

    /**
     * @return string
     */
    public function getRateioccustodeptoTitmov()
    {
        return $this->rateioccustodepto_titmov;
    }

    /**
     * @param string $rateioccustodepto_titmov
     */
    public function setRateioccustodeptoTitmov($rateioccustodepto_titmov)
    {
        $this->rateioccustodepto_titmov = $rateioccustodepto_titmov;
    }

    /**
     * @return string
     */
    public function getValorbrutoitemTitmov()
    {
        return $this->valorbrutoitem_titmov;
    }

    /**
     * @param string $valorbrutoitem_titmov
     */
    public function setValorbrutoitemTitmov($valorbrutoitem_titmov)
    {
        $this->valorbrutoitem_titmov = $valorbrutoitem_titmov;
    }

    /**
     * @return string
     */
    public function getValorbrutoitemorigTitmov()
    {
        return $this->valorbrutoitemorig_titmov;
    }

    /**
     * @param string $valorbrutoitemorig_titmov
     */
    public function setValorbrutoitemorigTitmov($valorbrutoitemorig_titmov)
    {
        $this->valorbrutoitemorig_titmov = $valorbrutoitemorig_titmov;
    }

    /**
     * @return string
     */
    public function getCodcoltborcamentoTitmmov()
    {
        return $this->codcoltborcamento_titmmov;
    }

    /**
     * @param string $codcoltborcamento_titmmov
     */
    public function setCodcoltborcamentoTitmmov($codcoltborcamento_titmmov)
    {
        $this->codcoltborcamento_titmmov = $codcoltborcamento_titmmov;
    }

    /**
     * @return string
     */
    public function getQuantidadetotalTitmov()
    {
        return $this->quantidadetotal_titmov;
    }

    /**
     * @param string $quantidadetotal_titmov
     */
    public function setQuantidadetotalTitmov($quantidadetotal_titmov)
    {
        $this->quantidadetotal_titmov = $quantidadetotal_titmov;
    }

    /**
     * @return int
     */
    public function getProdutosubstitutoTitmmov()
    {
        return $this->produtosubstituto_titmmov;
    }

    /**
     * @param int $produtosubstituto_titmmov
     */
    public function setProdutosubstitutoTitmmov($produtosubstituto_titmmov)
    {
        $this->produtosubstituto_titmmov = $produtosubstituto_titmmov;
    }

    /**
     * @return int
     */
    public function getPrecounitarioselecTitmmov()
    {
        return $this->precounitarioselec_titmmov;
    }

    /**
     * @param int $precounitarioselec_titmmov
     */
    public function setPrecounitarioselecTitmmov($precounitarioselec_titmmov)
    {
        $this->precounitarioselec_titmmov = $precounitarioselec_titmmov;
    }

    /**
     * @return string
     */
    public function getHistoricocurto()
    {
        return $this->historicocurto;
    }

    /**
     * @param string $historicocurto
     */
    public function setHistoricocurto($historicocurto)
    {
        $this->historicocurto = $historicocurto;
    }

    /**
     * @return string
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getPercentual()
    {
        return $this->percentual;
    }

    /**
     * @param string $percentual
     */
    public function setPercentual($percentual)
    {
        $this->percentual = $percentual;
    }

    /**
     * @return string
     */
    public function getCodccustoTitmmov()
    {
        return $this->codccusto_titmmov;
    }

    /**
     * @param string $codccusto_titmmov
     */
    public function setCodccustoTitmmov($codccusto_titmmov)
    {
        $this->codccusto_titmmov = $codccusto_titmmov;
    }
}
<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_encerramentoturmamatricula_pontosoft")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwEncerramentoTurmaMatriculaPontoSoft
{

    /**
     *
     * @var integer $id_matriculaintegracao
     * @Column(name="id_matriculaintegracao", type="integer", nullable=false)
     * @Id
     */
    private $id_matriculaintegracao;

    /**
     *
     * @var integer $id_usuariointegracao
     * @Column(name="id_usuariointegracao", type="integer", nullable=false)
     * @Id
     */
    private $id_usuariointegracao;

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     */
    private $id_usuario;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_turma
     * @Column(name="id_turma", type="integer", nullable=false)
     */
    private $id_turma;

    /**
     *
     * @var string $st_codusuario
     * @Column(name="st_codusuario", type="string", nullable=false)
     */
    private $st_codusuario;

    /**
     *
     * @var boolean $bl_usuario
     * @Column(name="bl_usuario", type="boolean", nullable=false)
     */
    private $bl_usuario;

    /**
     *
     * @var string $st_turmasistema
     * @Column(name="st_turmasistema", type="string", nullable=false)
     */
    private $st_turmasistema;

    /**
     * @return int
     */
    public function getId_matriculaintegracao()
    {
        return $this->id_matriculaintegracao;
    }

    /**
     * @param int $id_matriculaintegracao
     */
    public function setId_matriculaintegracao($id_matriculaintegracao)
    {
        $this->id_matriculaintegracao = $id_matriculaintegracao;
    }

    /**
     * @return int
     */
    public function getId_usuariointegracao()
    {
        return $this->id_usuariointegracao;
    }

    /**
     * @param int $id_usuariointegracao
     */
    public function setId_usuariointegracao($id_usuariointegracao)
    {
        $this->id_usuariointegracao = $id_usuariointegracao;
    }

    /**
     * @return boolean
     */
    public function getBl_usuario()
    {
        return $this->bl_usuario;
    }

    /**
     * @param boolean $bl_usuario
     */
    public function setBl_usuario($bl_usuario)
    {
        $this->bl_usuario = $bl_usuario;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return string
     */
    public function getSt_codusuario()
    {
        return $this->st_codusuario;
    }

    /**
     * @param string $st_codusuario
     */
    public function setSt_codusuario($st_codusuario)
    {
        $this->st_codusuario = $st_codusuario;
    }

    /**
     * @return string
     */
    public function getst_Turmasistema()
    {
        return $this->st_turmasistema;
    }

    /**
     * @param string $st_turmasistema
     */
    public function setst_Turmasistema($st_turmasistema)
    {
        $this->st_turmasistema = $st_turmasistema;
    }
}
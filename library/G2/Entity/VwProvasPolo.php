<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_provaspolo")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwProvasPolo {

    /**
     *
     * @var integer $id_avaliacaoaplicacao
     * @Column(name="id_avaliacaoaplicacao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoaplicacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     * @var integer $id_aplicadorprova
     * @Column(name="id_aplicadorprova", type="integer", nullable=false)
     */
    private $id_aplicadorprova;

    /**
     * @var string $st_aplicadorprova
     * @Column(name="st_aplicadorprova", type="string", nullable=false, length=200)
     */
    private $st_aplicadorprova;

    /**
     * @var string $st_dtaplicacao
     * @Column(name="st_dtaplicacao", type="string", nullable=true, length=30)
     */
    private $st_dtaplicacao;


    /**
     * @var date $dt_aplicacao
     * @Column(name="dt_aplicacao", type="date", nullable=true)
     */
    private $dt_aplicacao;


    /**
     * @var string $hr_inicio
     * @Column(name="hr_inicio", type="string", nullable=true, length=30)
     */
    private $hr_inicio;


    /**
     * @var string $hr_fim
     * @Column(name="hr_fim", type="string", nullable=true, length=30)
     */
    private $hr_fim;


    /**
     * @var integer $nu_vagasexistentes
     * @Column(name="nu_vagasexistentes", nullable=true, type="integer")
     */
    private $nu_vagasexistentes;


    /**
     * @var integer $total_agendamentos
     * @Column(name="total_agendamentos", nullable=true, type="integer")
     */
    private $total_agendamentos;


    /**
     * @var integer $nu_provafinal
     * @Column(name="nu_provafinal", nullable=true, type="integer")
     */
    private $nu_provafinal;


    /**
     * @var integer $nu_provarecuperacao
     * @Column(name="nu_provarecuperacao", nullable=true, type="integer")
     */
    private $nu_provarecuperacao;


    /**
     * @var integer $nu_restantes
     * @Column(name="nu_restantes", nullable=true, type="integer")
     */
    private $nu_restantes;

    /**
     * @param \G2\Entity\date $dt_aplicacao
     */
    public function setDt_aplicacao($dt_aplicacao)
    {
        $this->dt_aplicacao = $dt_aplicacao;
    }

    /**
     * @return \G2\Entity\date
     */
    public function getDt_aplicacao()
    {
        return $this->dt_aplicacao;
    }

    /**
     * @param string $hr_fim
     */
    public function setHr_fim($hr_fim)
    {
        $this->hr_fim = $hr_fim;
    }

    /**
     * @return string
     */
    public function getHr_fim()
    {
        return $this->hr_fim;
    }

    /**
     * @param string $hr_inicio
     */
    public function setHr_inicio($hr_inicio)
    {
        $this->hr_inicio = $hr_inicio;
    }

    /**
     * @return string
     */
    public function getHr_inicio()
    {
        return $this->hr_inicio;
    }

    /**
     * @param int $id_avaliacaoaplicacao
     */
    public function setId_avaliacaoaplicacao($id_avaliacaoaplicacao)
    {
        $this->id_avaliacaoaplicacao = $id_avaliacaoaplicacao;
    }

    /**
     * @return int
     */
    public function getId_avaliacaoaplicacao()
    {
        return $this->id_avaliacaoaplicacao;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $nu_provafinal
     */
    public function setNu_provafinal($nu_provafinal)
    {
        $this->nu_provafinal = $nu_provafinal;
    }

    /**
     * @return int
     */
    public function getNu_provafinal()
    {
        return $this->nu_provafinal;
    }

    /**
     * @param int $nu_provarecuperacao
     */
    public function setNu_provarecuperacao($nu_provarecuperacao)
    {
        $this->nu_provarecuperacao = $nu_provarecuperacao;
    }

    /**
     * @return int
     */
    public function getNu_provarecuperacao()
    {
        return $this->nu_provarecuperacao;
    }

    /**
     * @param int $nu_restantes
     */
    public function setNu_restantes($nu_restantes)
    {
        $this->nu_restantes = $nu_restantes;
    }

    /**
     * @return int
     */
    public function getNu_restantes()
    {
        return $this->nu_restantes;
    }

    /**
     * @param int $nu_vagasexistentes
     */
    public function setNu_vagasexistentes($nu_vagasexistentes)
    {
        $this->nu_vagasexistentes = $nu_vagasexistentes;
    }

    /**
     * @return int
     */
    public function getNu_vagasexistentes()
    {
        return $this->nu_vagasexistentes;
    }

    /**
     * @param string $st_aplicadorprova
     */
    public function setSt_aplicadorprova($st_aplicadorprova)
    {
        $this->st_aplicadorprova = $st_aplicadorprova;
    }

    /**
     * @return string
     */
    public function getSt_aplicadorprova()
    {
        return $this->st_aplicadorprova;
    }

    /**
     * @param string $st_dtaplicacao
     */
    public function setSt_dtaplicacao($st_dtaplicacao)
    {
        $this->st_dtaplicacao = $st_dtaplicacao;
    }

    /**
     * @return string
     */
    public function getSt_dtaplicacao()
    {
        return $this->st_dtaplicacao;
    }

    /**
     * @param int $total_agendamentos
     */
    public function setTotal_agendamentos($total_agendamentos)
    {
        $this->total_agendamentos = $total_agendamentos;
    }

    /**
     * @return int
     */
    public function getTotal_agendamentos()
    {
        return $this->total_agendamentos;
    }

    /**
     * @param int $id_aplicadorprova
     */
    public function setId_aplicadorprova($id_aplicadorprova)
    {
        $this->id_aplicadorprova = $id_aplicadorprova;
    }

    /**
     * @return int
     */
    public function getId_aplicadorprova()
    {
        return $this->id_aplicadorprova;
    }




}

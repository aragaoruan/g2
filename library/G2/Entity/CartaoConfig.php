<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_cartaoconfig")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @EntityLog
 */
class CartaoConfig extends G2Entity
{

    /**
     *
     * @var integer $id_cartaoconfig
     * @Column(name="id_cartaoconfig", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_cartaoconfig;

    /**
     * @var CartaoBandeira $id_cartaobandeira
     * @ManyToOne(targetEntity="CartaoBandeira")
     * @JoinColumn(name="id_cartaobandeira", nullable=false, referencedColumnName="id_cartaobandeira")
     */
    private $id_cartaobandeira;

    /**
     * @var CartaoOperadora $id_cartaooperadora
     * @ManyToOne(targetEntity="CartaoOperadora")
     * @JoinColumn(name="id_cartaooperadora", nullable=false, referencedColumnName="id_cartaooperadora")
     */
    private $id_cartaooperadora;

    /**
     * @var integer $id_contaentidade
     * @Column(name="id_contaentidade", type="integer", nullable=false)
     */
    private $id_contaentidade;

    /**
     * @var string $st_gateway
     * @Column(name="st_gateway", type="string", nullable=true, length=50)
     */
    private $st_gateway;

    /**
     * @var string $st_contratooperadora
     * @Column(name="st_contratooperadora", type="string", nullable=true, length=50)
     */
    private $st_contratooperadora;

    /**
     * @var Sistema $id_sistema
     * @ManyToOne(targetEntity="Sistema")
     * @JoinColumn(name="id_sistema", nullable=false, referencedColumnName="id_sistema")
     */
    private $id_sistema;

    /**
     *
     * @var $entidadeFinaceira
     * @ManyToMany(targetEntity="EntidadeFinanceiro", mappedBy="cartaoConfig")
     */
    private $entidadeFinaceira;

    public function getId_cartaoconfig ()
    {
        return $this->id_cartaoconfig;
    }

    public function setId_cartaoconfig ($id_cartaoconfig)
    {
        $this->id_cartaoconfig = $id_cartaoconfig;
        return $this;
    }

    public function getId_cartaobandeira ()
    {
        return $this->id_cartaobandeira;
    }

    public function setId_cartaobandeira (CartaoBandeira $id_cartaobandeira)
    {
        $this->id_cartaobandeira = $id_cartaobandeira;
        return $this;
    }

    public function getId_cartaooperadora ()
    {
        return $this->id_cartaooperadora;
    }

    public function setId_cartaooperadora (CartaoOperadora $id_cartaooperadora)
    {
        $this->id_cartaooperadora = $id_cartaooperadora;
        return $this;
    }

    public function getId_contaentidade ()
    {
        return $this->id_contaentidade;
    }

    public function setId_contaentidade ($id_contaentidade)
    {
        $this->id_contaentidade = $id_contaentidade;
        return $this;
    }

    public function getSt_gateway ()
    {
        return $this->st_gateway;
    }

    public function setSt_gateway ($st_gateway)
    {
        $this->st_gateway = $st_gateway;
        return $this;
    }

    public function getSt_contratooperadora ()
    {
        return $this->st_contratooperadora;
    }

    public function setSt_contratooperadora ($st_contratooperadora)
    {
        $this->st_contratooperadora = $st_contratooperadora;
        return $this;
    }

    public function getId_sistema ()
    {
        return $this->id_sistema;
    }

    public function setId_sistema (Sistema $id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

}

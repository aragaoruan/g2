<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_projetocontratoduracaotipo")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class ProjetoContratoDuracaoTipo
{

    /**
     *
     * @var integer $id_projetocontratoduracaotipo
     * @Column(name="id_projetocontratoduracaotipo", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_projetocontratoduracaotipo;

    /**
     *
     * @var string $st_projetocontratoduracaotipo
     * @Column(name="st_projetocontratoduracaotipo", type="string", nullable=false, length=255)
     */
    private $st_projetocontratoduracaotipo;

    /**
     *
     * @var string $st_descricao
     * @Column(name="st_descricao", type="string", nullable=false, length=2500)
     */
    private $st_descricao;

    public function getId_projetocontratoduracaotipo ()
    {
        return $this->id_projetocontratoduracaotipo;
    }

    public function setId_projetocontratoduracaotipo ($id_projetocontratoduracaotipo)
    {
        $this->id_projetocontratoduracaotipo = $id_projetocontratoduracaotipo;
        return $this;
    }

    public function getSt_projetocontratoduracaotipo ()
    {
        return $this->st_projetocontratoduracaotipo;
    }

    public function setSt_projetocontratoduracaotipo ($st_projetocontratoduracaotipo)
    {
        $this->st_projetocontratoduracaotipo = $st_projetocontratoduracaotipo;
        return $this;
    }

    public function getSt_descricao ()
    {
        return $this->st_descricao;
    }

    public function setSt_descricao ($st_descricao)
    {
        $this->st_descricao = $st_descricao;
        return $this;
    }

}
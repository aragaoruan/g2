<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_ocorrenciaresponsavel")
 * @Entity
 * @EntityLog
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class OcorrenciaResponsavel extends G2Entity {

    /**
     * @Id
     * @var integer $id_ocorrenciaresponsavel
     * @Column(name="id_ocorrenciaresponsavel", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_ocorrenciaresponsavel;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
     */
    private $dt_cadastro;

    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="integer", nullable=true, length=4)
     */
    private $id_ocorrencia;

    /**
     * @var Usuario $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;


    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    public function getId_ocorrenciaresponsavel()
    {
        return $this->id_ocorrenciaresponsavel;
    }

    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @return Usuario
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function setId_ocorrenciaresponsavel($id_ocorrenciaresponsavel)
    {
        $this->id_ocorrenciaresponsavel = $id_ocorrenciaresponsavel;
        return $this;
    }

    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}

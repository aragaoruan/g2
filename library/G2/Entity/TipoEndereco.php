<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_tipoendereco")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TipoEndereco
{

    /**
     *
     * @var integer $id_tipoendereco
     * @Column(name="id_tipoendereco", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipoendereco;

    /**
     *
     * @var string $st_tipoendereco
     * @Column(name="st_tipoendereco", type="string", nullable=false, length=255) 
     */
    private $st_tipoendereco;

    /**
     *
     * @var CategoriaEndereco $id_categoriaendereco
     * @ManyToOne(targetEntity="CategoriaEndereco")
     * @JoinColumn(name="id_categoriaendereco", nullable=false, referencedColumnName="id_categoriaendereco")
     */
    private $id_categoriaendereco;

    public function getId_tipoendereco ()
    {
        return $this->id_tipoendereco;
    }

    public function setId_tipoendereco ($id_tipoendereco)
    {
        $this->id_tipoendereco = $id_tipoendereco;
        return $this;
    }

    public function getSt_tipoendereco ()
    {
        return $this->st_tipoendereco;
    }

    public function setSt_tipoendereco ($st_tipoendereco)
    {
        $this->st_tipoendereco = $st_tipoendereco;
        return $this;
    }

    public function getId_categoriaendereco ()
    {
        return $this->id_categoriaendereco;
    }

    public function setId_categoriaendereco (CategoriaEndereco $id_categoriaendereco)
    {
        $this->id_categoriaendereco = $id_categoriaendereco;
        return $this;
    }

}
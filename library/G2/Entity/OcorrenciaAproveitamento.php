<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_ocorrenciaaproveitamento")
 * @Entity
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */

use G2\G2Entity;

class OcorrenciaAproveitamento extends G2Entity
{

    /**
     * @Id
     * @var integer $id_ocorrenciaaproveitamento
     * @Column(name="id_ocorrenciaaproveitamento", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_ocorrenciaaproveitamento;
    /**
     * @var integer $id_ocorrencia
     * @ManyToOne(targetEntity="Ocorrencia")
     * @JoinColumn(name="id_ocorrencia", referencedColumnName="id_ocorrencia")
     */
    private $id_ocorrencia;
    /**
     * @var integer $id_aproveitamento
     * @ManyToOne(targetEntity="Aproveitamento")
     * @JoinColumn(name="id_aproveitamento", referencedColumnName="id_aproveitamento")
     */
    private $id_aproveitamento;
    /**
     * @var integer $id_usuariocadastro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocadastro", referencedColumnName="id_usuario")
     */
    private $id_usuariocadastro;
    /**
     * @var datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=true, length=8)
     */
    private $dt_cadastro;
    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
     */
    private $bl_ativo;


    /**
     * @return integer id_ocorrenciaaproveitamento
     */
    public function getId_ocorrenciaaproveitamento()
    {
        return $this->id_ocorrenciaaproveitamento;
    }

    /**
     * @param id_ocorrenciaaproveitamento
     */
    public function setId_ocorrenciaaproveitamento($id_ocorrenciaaproveitamento)
    {
        $this->id_ocorrenciaaproveitamento = $id_ocorrenciaaproveitamento;
        return $this;
    }

    /**
     * @return integer id_ocorrencia
     */
    public function getId_ocorrencia()
    {
        return $this->id_ocorrencia;
    }

    /**
     * @param id_ocorrencia
     */
    public function setId_ocorrencia($id_ocorrencia)
    {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

    /**
     * @return integer id_aproveitamento
     */
    public function getId_aproveitamento()
    {
        return $this->id_aproveitamento;
    }

    /**
     * @param id_aproveitamento
     */
    public function setId_aproveitamento($id_aproveitamento)
    {
        $this->id_aproveitamento = $id_aproveitamento;
        return $this;
    }

    /**
     * @return integer id_usuariocadastro
     */
    public function getId_usuariocadastro()
    {
        return $this->id_usuariocadastro;
    }

    /**
     * @param id_usuariocadastro
     */
    public function setId_usuariocadastro($id_usuariocadastro)
    {
        $this->id_usuariocadastro = $id_usuariocadastro;
        return $this;
    }

    /**
     * @return datetime dt_cadastro
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @return boolean bl_ativo
     */
    public function getBl_ativo()
    {
        return $this->bl_ativo;
    }

    /**
     * @param bl_ativo
     */
    public function setBl_ativo($bl_ativo)
    {
        $this->bl_ativo = $bl_ativo;
        return $this;
    }

}
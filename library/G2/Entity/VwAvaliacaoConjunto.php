<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoconjunto")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwAvaliacaoConjunto
{

    /**
     *
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoconjunto;

    /**
     *
     * @var string $st_avaliacaoconjunto
     * @Column(name="st_avaliacaoconjunto", type="string", nullable=false, length=100)
     */
    private $st_avaliacaoconjunto;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var integer $id_tipoprova
     * @Column(name="id_tipoprova", type="integer", nullable=true)
     */
    private $id_tipoprova;

    /**
     *
     * @var string $st_tipoprova
     * @Column(name="st_tipoprova", type="string", nullable=false, length=255)
     */
    private $st_tipoprova;

    /**
     *
     * @var integer $id_tipocalculoavaliacao
     * @Column(name="id_tipocalculoavaliacao", type="integer", nullable=true)
     */
    private $id_tipocalculoavaliacao;

    /**
     *
     * @var string $st_tipocalculoavaliacao
     * @Column(name="st_tipocalculoavaliacao", type="string", nullable=false, length=100)
     */
    private $st_tipocalculoavaliacao;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true)
     */
    private $id_entidade;

    public function getId_avaliacaoconjunto ()
    {
        return $this->id_avaliacaoconjunto;
    }

    public function setId_avaliacaoconjunto ($id_avaliacaoconjunto)
    {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
        return $this;
    }

    public function getSt_avaliacaoconjunto ()
    {
        return $this->st_avaliacaoconjunto;
    }

    public function setSt_avaliacaoconjunto ($st_avaliacaoconjunto)
    {
        $this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getId_tipoprova ()
    {
        return $this->id_tipoprova;
    }

    public function setId_tipoprova ($id_tipoprova)
    {
        $this->id_tipoprova = $id_tipoprova;
        return $this;
    }

    public function getSt_tipoprova ()
    {
        return $this->st_tipoprova;
    }

    public function setSt_tipoprova ($st_tipoprova)
    {
        $this->st_tipoprova = $st_tipoprova;
        return $this;
    }

    public function getId_tipocalculoavaliacao ()
    {
        return $this->id_tipocalculoavaliacao;
    }

    public function setId_tipocalculoavaliacao ($id_tipocalculoavaliacao)
    {
        $this->id_tipocalculoavaliacao = $id_tipocalculoavaliacao;
        return $this;
    }

    public function getSt_tipocalculoavaliacao ()
    {
        return $this->st_tipocalculoavaliacao;
    }

    public function setSt_tipocalculoavaliacao ($st_tipocalculoavaliacao)
    {
        $this->st_tipocalculoavaliacao = $st_tipocalculoavaliacao;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}
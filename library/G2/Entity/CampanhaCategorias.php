<?php

namespace G2\Entity;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_campanhacategorias")
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class CampanhaCategorias
{

    /**
     * @var integer $id_campanhacategoris 
     * @Column(type="integer", nullable=false, name="id_campanhacategoris")
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_campanhacategoris;

    /**
     * @var Categoria $id_categoria 
     * @ManyToOne(targetEntity="Categoria")
     * @JoinColumn(name="id_categoria", referencedColumnName="id_categoria", nullable=false)
     */
    private $id_categoria;

    /**
     * @var CampanhaComercial $id_campanhacomercial 
     * @ManyToOne(targetEntity="CampanhaComercial")
     * @JoinColumn(name="id_campanhacomercial", referencedColumnName="id_campanhacomercial", nullable=false)
     */
    private $id_campanhacomercial;

    /**
     * @return integer
     */
    public function getId_campanhacategoris ()
    {
        return $this->id_campanhacategoris;
    }

    /**
     * @return \G2\Entity\Categoria
     */
    public function getId_categoria ()
    {
        return $this->id_categoria;
    }

    /**
     * @return \G2\Entity\CampanhaComercial
     */
    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    /**
     * @param integer $id_campanhacategoris
     * @return \G2\Entity\CampanhaCategorias
     */
    public function setId_campanhacategoris ($id_campanhacategoris)
    {
        $this->id_campanhacategoris = $id_campanhacategoris;
        return $this;
    }

    /**
     * @param \G2\Entity\Categoria $id_categoria
     * @return \G2\Entity\CampanhaCategorias
     */
    public function setId_categoria (Categoria $id_categoria)
    {
        $this->id_categoria = $id_categoria;
        return $this;
    }

    /**
     * @param \G2\Entity\CampanhaComercial $id_campanhacomercial
     * @return \G2\Entity\CampanhaCategorias
     */
    public function setId_campanhacomercial (CampanhaComercial $id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

}

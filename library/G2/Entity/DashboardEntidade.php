<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_dashboardentidade")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-20
 */
class DashboardEntidade extends G2Entity {

    /**
     *
     * @var integer $id_dashboardentidade
     * @Column(name="id_dashboardentidade", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_dashboardentidade;
    /**
     * @var integer $id_dashboard
     * @Column(name="id_dashboard", type="integer", nullable=false, length=4)
     */
    private $id_dashboard;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @return mixed
     */
    public function getId_dashboard()
    {
        return $this->id_dashboard;
    }

    /**
     * @param mixed $id_dashboard
     */
    public function setId_dashboard($id_dashboard)
    {
        $this->id_dashboard = $id_dashboard;
    }

    /**
     * @return int
     */
    public function getId_dashboardentidade()
    {
        return $this->id_dashboardentidade;
    }

    /**
     * @param int $id_dashboardentidade
     */
    public function setId_dashboardentidade($id_dashboardentidade)
    {
        $this->id_dashboardentidade = $id_dashboardentidade;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }




}

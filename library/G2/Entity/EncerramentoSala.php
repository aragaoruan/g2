<?php
/**
 * User: Denise Xavier
 * Date: 09/07/14
 * Time: 11:40
 */
namespace G2\Entity;

use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_encerramentosala")
 * @Entity
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @EntityLog
 */
class EncerramentoSala extends G2Entity
{
    /**
     * @var integer $id_encerramentosala
     * @Column(name="id_encerramentosala", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_encerramentosala;

    /**
     * @var SaladeAula $id_saladeaula
     * @ManyToOne(targetEntity="SalaDeAula")
     * @JoinColumn(name="id_saladeaula", nullable=true, referencedColumnName="id_saladeaula")
     */
    private $id_saladeaula;

    /**
     * @var Usuario $id_usuariocoordenador
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariocoordenador", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariocoordenador;

    /**
     * @var Usuario $id_usuariopedagogico
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariopedagogico", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariopedagogico;

    /**
     * @var Usuario $id_usuarioprofessor
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuarioprofessor", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuarioprofessor;


    /**
     * @var Usuario $id_usuariofinanceiro
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariofinanceiro", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariofinanceiro;

    /**
     * @var datetime2 $dt_encerramentoprofessor
     * @Column(name="dt_encerramentoprofessor", type="datetime2", nullable=true)
     */
    private $dt_encerramentoprofessor;

    /**
     * @var datetime2 $dt_encerramentocoordenador
     * @Column(name="dt_encerramentocoordenador", type="datetime2", nullable=true)
     */
    private $dt_encerramentocoordenador;

    /**
     * @var datetime2 $dt_encerramentopedagogico
     * @Column(name="dt_encerramentopedagogico", type="datetime2", nullable=true)
     */
    private $dt_encerramentopedagogico;

    /**
     * @var datetime2 $dt_encerramentofinanceiro
     * @Column(name="dt_encerramentofinanceiro", type="datetime2", nullable=true)
     */
    private $dt_encerramentofinanceiro;

    /**
     * @var datetime2 $dt_recusa
     * @Column(name="dt_recusa", type="datetime2", nullable=true)
     */
    private $dt_recusa;

    /**
     * @var Usuario $id_usuariorecusa
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuariorecusa", nullable=true, referencedColumnName="id_usuario")
     */
    private $id_usuariorecusa;

    /**
     * @var string $st_motivorecusa
     * @Column(name="st_motivorecusa", type="string", nullable=true, length=8000)
     */
    private $st_motivorecusa;

    public function getId_encerramentosala() {
        return $this->id_encerramentosala;
    }

    public function setId_encerramentosala($id_encerramentosala) {
        $this->id_encerramentosala = $id_encerramentosala;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function getId_usuarioprofessor() {
        return $this->id_usuarioprofessor;
    }

    public function setId_usuarioprofessor($id_usuarioprofessor) {
        $this->id_usuarioprofessor = $id_usuarioprofessor;
    }

    public function getId_usuariocoordenador() {
        return $this->id_usuariocoordenador;
    }

    public function setId_usuariocoordenador($id_usuariocoordenador) {
        $this->id_usuariocoordenador = $id_usuariocoordenador;
        return $this;
    }

    public function getId_usuariopedagogico() {
        return $this->id_usuariopedagogico;
    }

    public function setId_usuariopedagogico($id_usuariopedagogico) {
        $this->id_usuariopedagogico = $id_usuariopedagogico;
        return $this;
    }

    public function getId_usuariofinanceiro() {
        return $this->id_usuariofinanceiro;
    }

    public function setId_usuariofinanceiro($id_usuariofinanceiro) {
        $this->id_usuariofinanceiro = $id_usuariofinanceiro;
    }

    public function getDt_encerramentoprofessor() {
        return $this->dt_encerramentoprofessor;
    }

    public function setDt_encerramentoprofessor($dt_encerramentoprofessor) {
        $this->dt_encerramentoprofessor = $dt_encerramentoprofessor;
    }

    public function getDt_encerramentocoordenador() {
        return $this->dt_encerramentocoordenador;
    }

    public function setDt_encerramentocoordenador($dt_encerramentocoordenador) {
        $this->dt_encerramentocoordenador = $dt_encerramentocoordenador;
        return $this;
    }

    public function getDt_encerramentopedagogico() {
        return $this->dt_encerramentopedagogico;
    }

    public function setDt_encerramentopedagogico($dt_encerramentopedagogico) {
        $this->dt_encerramentopedagogico = $dt_encerramentopedagogico;
        return $this;
    }

    public function getDt_encerramentofinanceiro() {
        return $this->dt_encerramentofinanceiro;
    }

    public function setDt_encerramentofinanceiro($dt_encerramentofinanceiro) {
        $this->dt_encerramentofinanceiro = $dt_encerramentofinanceiro;
    }

    public function setDt_recusa($dt_recusa)
    {
        $this->dt_recusa = $dt_recusa;
    }

    public function getDt_recusa()
    {
        return $this->dt_recusa;
    }

    public function setId_usuariorecusa($id_usuariorecusa)
    {
        $this->id_usuariorecusa = $id_usuariorecusa;
    }

    public function getId_usuariorecusa()
    {
        return $this->id_usuariorecusa;
    }

    public function setSt_motivorecusa($st_motivorecusa)
    {
        $this->st_motivorecusa = $st_motivorecusa;
    }

    public function getSt_motivorecusa()
    {
        return $this->st_motivorecusa;
    }
}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_itemativacaoentidade")
 * @Entity
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2015-01-20
 */
class ItemAtivacaoEntidade {

    /**
     *
     * @var integer $id_itemativacaoentidade
     * @Column(name="id_itemativacaoentidade", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_itemativacaoentidade;

    /**
     * @var integer $id_itemativacao
     * @Column(name="id_itemativacao", type="integer", nullable=false, length=4)
     */
    private $id_itemativacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true)
     */
    private $id_situacao;

    /**
     * @var integer $id_usuarioatualizacao
     * @Column(name="id_usuarioatualizacao", type="integer", nullable=true)
     */
    private $id_usuarioatualizacao;

    /**
     * @Column(name="dt_atualizacao", type="datetime2", nullable=false)
     * @var datetime2 $dt_atualizacao
     */
    private $dt_atualizacao;


    /**
     * @return Entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    public function getId_usuarioatualizacao()
    {
        return $this->id_usuarioatualizacao;
    }

    public function getDt_atualizacao()
    {
        return $this->dt_atualizacao;
    }

    /**
     * @param Entidade $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
    }

    public function setId_usuarioatualizacao($id_usuarioatualizacao)
    {
        $this->id_usuarioatualizacao = $id_usuarioatualizacao;
    }

    public function setDt_atualizacao($dt_atualizacao)
    {
        $this->dt_atualizacao = $dt_atualizacao;
    }

    /**
     * @return ItemAtivacao
     */
    public function getId_itemativacao()
    {
        return $this->id_itemativacao;
    }

    /**
     * @param ItemAtivacao $id_itemativacao
     */
    public function setId_itemativacao($id_itemativacao)
    {
        $this->id_itemativacao = $id_itemativacao;
    }

    /**
     * @return int
     */
    public function getId_itemativacaoentidade()
    {
        return $this->id_itemativacaoentidade;
    }

    /**
     * @param int $id_itemativacaoentidade
     */
    public function setId_itemativacaoentidade($id_itemativacaoentidade)
    {
        $this->id_itemativacaoentidade = $id_itemativacaoentidade;
    }



}

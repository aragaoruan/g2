<?php

/*
 * Entity DocumentosObrigatoriedade
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-09-26
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_documentosobrigatoriedade")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class DocumentosObrigatoriedade {

    /**
     *
     * @var integer $id_documentosobrigatoriedade
     * @Column(name="id_documentosobrigatoriedade", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_documentosobrigatoriedade;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_documentosobrigatoriedade")
     * @var string
     */
    private $st_documentosobrigatoriedade;

    /**
     * @var boolean $id_usuariocadastro
     * @Column(name="id_usuariocadastro", type="integer", nullable=false)
     */
    private $id_usuariocadastro;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false)
     */
    private $dt_cadastro;


    public function getId_documentosobrigatoriedade() {
        return $this->id_documentosobrigatoriedade;
    }

    public function getSt_documentosobrigatoriedade() {
        return $this->st_documentosobrigatoriedade;
    }

    public function getId_usuariocadastro() {
        return $this->id_usuariocadastro;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    public function setId_documentosobrigatoriedade($id_documentosobrigatoriedade) {
        $this->id_documentosobrigatoriedade = $id_documentosobrigatoriedade;
    }

    public function setSt_documentosobrigatoriedade($st_documentosobrigatoriedade) {
        $this->st_documentosobrigatoriedade = $st_documentosobrigatoriedade;
    }

    public function setId_usuariocadastro($id_usuariocadastro) {
        $this->id_usuariocadastro = $id_usuariocadastro;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }

    public function setDt_cadastro($dt_cadastro) {
        $this->dt_cadastro = $dt_cadastro;
    }

}

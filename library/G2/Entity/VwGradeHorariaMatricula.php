<?php

namespace G2\Entity;

/**
 * Description of VwGerarDeclaracao
 *
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_gradehorariamatricula")
 * @Entity(repositoryClass="\G2\Repository\GradeHoraria")
 * @EntityView
 */
class VwGradeHorariaMatricula
{

    /**
     * @var integer $id_gradehoraria
     * @Column(type="integer", nullable=false, name="id_gradehoraria")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_gradehoraria;

    /**
     * @var integer $id_itemgradehoraria
     * @Column(type="integer", nullable=false, name="id_itemgradehoraria")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_itemgradehoraria;

    /**
     * @var integer $id_unidade
     * @Column(type="integer", nullable=false, name="id_unidade")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_unidade;

    /**
     * @var integer $id_turno
     * @Column(type="integer", nullable=false, name="id_turno")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_turno;

    /**
     * @var integer $id_diasemana
     * @Column(type="integer", nullable=false, name="id_diasemana")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_diasemana;

    /**
     * @var integer $id_disciplina
     * @Column(type="integer", nullable=false, name="id_disciplina")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_disciplina;

    /**
     * @var integer $id_professor
     * @Column(type="integer", nullable=false, name="id_professor")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_professor;

    /**
     * @var integer $id_turma
     * @Column(type="integer", nullable=false, name="id_turma")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_turma;

    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", nullable=false, name="id_projetopedagogico")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_matricula
     * @Column(type="integer", nullable=false, name="id_matricula")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_matricula;

    /**
     * @var integer $nu_totalencontros
     * @Column(type="integer", nullable=false, name="nu_totalencontros")
     */
    private $nu_totalencontros;

    /**
     * @var integer $nu_encontros
     * @Column(type="integer", nullable=false, name="nu_encontros")
     */
    private $nu_encontros;

    /**
     * @var datetime2 $dt_cadastro
     * @Column(type="datetime2", nullable=false, name="dt_cadastro")
     */
    private $dt_cadastro;

    /**
     * @var datetime2 $dt_atualizacao
     * @Column(type="datetime2", nullable=false, name="dt_atualizacao")
     */
    private $dt_atualizacao;

    /**
     * @var datetime2 $dt_iniciogradehoraria
     * @Column(type="datetime2", nullable=false, name="dt_iniciogradehoraria")
     */
    private $dt_iniciogradehoraria;

    /**
     * @var datetime2 $dt_fimgradehoraria
     * @Column(type="datetime2", nullable=false, name="dt_fimgradehoraria")
     */
    private $dt_fimgradehoraria;

    /**
     * @var string $st_nomegradehoraria
     * @Column(type="string", nullable=false, name="st_nomegradehoraria")
     */
    private $st_nomegradehoraria;

    /**
     * @var string $st_nomeunidade
     * @Column(type="string", nullable=false, name="st_nomeunidade")
     */
    private $st_nomeunidade;

    /**
     * @var string $st_turno
     * @Column(type="string", nullable=false, name="st_turno")
     */
    private $st_turno;

    /**
     * @var string $st_diasemana
     * @Column(type="string", nullable=false, name="st_diasemana")
     */
    private $st_diasemana;

    /**
     * @var string $st_disciplina
     * @Column(type="string", nullable=false, name="st_disciplina")
     */
    private $st_disciplina;

    /**
     * @var string $st_nomeprofessor
     * @Column(type="string", nullable=false, name="st_nomeprofessor")
     */
    private $st_nomeprofessor;

    /**
     * @var string $st_turma
     * @Column(type="string", nullable=false, name="st_turma")
     */
    private $st_turma;

    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", nullable=false, name="st_projetopedagogico")
     */
    private $st_projetopedagogico;

    /**
     * @var date $dt_diasemana
     * @Column(type="date", nullable=false, name="dt_diasemana")
     */
    private $dt_diasemana;
    /**
     * @var date $nu_encontro
     * @Column(type="integer", nullable=false, name="nu_encontro")
     */
    private $nu_encontro;

    /**
     * @return date
     */
    public function getDt_diasemana()
    {
        return $this->dt_diasemana;
    }

    /**
     * @param $dt_diasemana
     * @return $this
     */
    public function setDt_diasemana($dt_diasemana)
    {
        $this->dt_diasemana = $dt_diasemana;
        return $this;
    }


    /**
     * @return int
     */
    public function getId_gradehoraria()
    {
        return $this->id_gradehoraria;
    }

    /**
     * @return int
     */
    public function getId_itemgradehoraria()
    {
        return $this->id_itemgradehoraria;
    }

    /**
     * @return int
     */
    public function getId_unidade()
    {
        return $this->id_unidade;
    }

    /**
     * @return int
     */
    public function getId_turno()
    {
        return $this->id_turno;
    }

    /**
     * @return int
     */
    public function getId_diasemana()
    {
        return $this->id_diasemana;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_professor()
    {
        return $this->id_professor;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @return int
     */
    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @return int
     */
    public function getNu_totalencontros()
    {
        return $this->nu_totalencontros;
    }

    /**
     * @return int
     */
    public function getNu_encontros()
    {
        return $this->nu_encontros;
    }

    /**
     * @return datetime2
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @return datetime2
     */
    public function getDt_atualizacao()
    {
        return $this->dt_atualizacao;
    }

    /**
     * @return datetime2
     */
    public function getDt_iniciogradehoraria()
    {
        return $this->dt_iniciogradehoraria;
    }

    /**
     * @return datetime2
     */
    public function getDt_fimgradehoraria()
    {
        return $this->dt_fimgradehoraria;
    }

    /**
     * @return string
     */
    public function getSt_nomegradehoraria()
    {
        return $this->st_nomegradehoraria;
    }

    /**
     * @return string
     */
    public function getSt_nomeunidade()
    {
        return $this->st_nomeunidade;
    }

    /**
     * @return string
     */
    public function getSt_turno()
    {
        return $this->st_turno;
    }

    /**
     * @return string
     */
    public function getSt_diasemana()
    {
        return $this->st_diasemana;
    }

    /**
     * @return string
     */
    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    /**
     * @return string
     */
    public function getSt_nomeprofessor()
    {
        return $this->st_nomeprofessor;
    }

    /**
     * @return string
     */
    public function getSt_turma()
    {
        return $this->st_turma;
    }

    /**
     * @return string
     */
    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    /**
     * @param $id_gradehoraria
     * @return $this
     */
    public function setId_gradehoraria($id_gradehoraria)
    {
        $this->id_gradehoraria = $id_gradehoraria;
        return $this;
    }

    /**
     * @param $id_itemgradehoraria
     * @return $this
     */
    public function setId_itemgradehoraria($id_itemgradehoraria)
    {
        $this->id_itemgradehoraria = $id_itemgradehoraria;
        return $this;
    }

    /**
     * @param $id_unidade
     * @return $this
     */
    public function setId_unidade($id_unidade)
    {
        $this->id_unidade = $id_unidade;
        return $this;
    }

    /**
     * @param $id_turno
     * @return $this
     */
    public function setId_turno($id_turno)
    {
        $this->id_turno = $id_turno;
        return $this;
    }

    /**
     * @param $id_diasemana
     * @return $this
     */
    public function setId_diasemana($id_diasemana)
    {
        $this->id_diasemana = $id_diasemana;
        return $this;
    }

    /**
     * @param $id_disciplina
     * @return $this
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    /**
     * @param $id_professor
     * @return $this
     */
    public function setId_professor($id_professor)
    {
        $this->id_professor = $id_professor;
        return $this;
    }

    /**
     * @param $id_turma
     * @return $this
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
        return $this;
    }

    /**
     * @param $id_projetopedagogico
     * @return $this
     */
    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    /**
     * @param $id_matricula
     * @return $this
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    /**
     * @param $nu_totalencontros
     * @return $this
     */
    public function setNu_totalencontros($nu_totalencontros)
    {
        $this->nu_totalencontros = $nu_totalencontros;
        return $this;
    }

    /**
     * @param $nu_encontros
     * @return $this
     */
    public function setNu_encontros($nu_encontros)
    {
        $this->nu_encontros = $nu_encontros;
        return $this;
    }

    /**
     * @param  $dt_cadastro
     * @return $this
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    /**
     * @param $dt_atualizacao
     * @return $this
     */
    public function setDt_atualizacao($dt_atualizacao)
    {
        $this->dt_atualizacao = $dt_atualizacao;
        return $this;
    }

    /**
     * @param $dt_iniciogradehoraria
     * @return $this
     */
    public function setDt_iniciogradehoraria($dt_iniciogradehoraria)
    {
        $this->dt_iniciogradehoraria = $dt_iniciogradehoraria;
        return $this;
    }

    /**
     * @param $dt_fimgradehoraria
     * @return $this
     */
    public function setDt_fimgradehoraria($dt_fimgradehoraria)
    {
        $this->dt_fimgradehoraria = $dt_fimgradehoraria;
        return $this;
    }

    /**
     * @param $st_nomegradehoraria
     * @return $this
     */
    public function setSt_nomegradehoraria($st_nomegradehoraria)
    {
        $this->st_nomegradehoraria = $st_nomegradehoraria;
        return $this;
    }

    /**
     * @param $st_nomeunidade
     * @return $this
     */
    public function setSt_nomeunidade($st_nomeunidade)
    {
        $this->st_nomeunidade = $st_nomeunidade;
        return $this;
    }

    /**
     * @param $st_turno
     * @return $this
     */
    public function setSt_turno($st_turno)
    {
        $this->st_turno = $st_turno;
        return $this;
    }

    /**
     * @param $st_diasemana
     * @return $this
     */
    public function setSt_diasemana($st_diasemana)
    {
        $this->st_diasemana = $st_diasemana;
        return $this;
    }

    /**
     * @param $st_disciplina
     * @return $this
     */
    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @param $st_nomeprofessor
     * @return $this
     */
    public function setSt_nomeprofessor($st_nomeprofessor)
    {
        $this->st_nomeprofessor = $st_nomeprofessor;
        return $this;
    }

    /**
     * @param $st_turma
     * @return $this
     */
    public function setSt_turma($st_turma)
    {
        $this->st_turma = $st_turma;
        return $this;
    }

    /**
     * @param $st_projetopedagogico
     * @return $this
     */
    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    /**
     * @return date
     */
    public function getNu_encontro()
    {
        return $this->nu_encontro;
    }

    /**
     * @param date $nu_encontro
     */
    public function setNu_encontro($nu_encontro)
    {
        $this->nu_encontro = $nu_encontro;
    }



}

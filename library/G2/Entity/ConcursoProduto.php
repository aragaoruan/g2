<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_concursoproduto")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimarães@gmail.com> 
 */

use G2\G2Entity;

class ConcursoProduto extends G2Entity
{

    /**
     * @var integer $id_concursoproduto
     * @Column(name="id_concursoproduto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_concursoproduto;

    /**
     *
     * @var integer $id_concurso
     * @ManyToOne(targetEntity="Concurso")
     * @JoinColumn(name="id_concurso", nullable=false, referencedColumnName="id_concurso")
     */
    private $id_concurso;
    
    
    /**
     *
     * @var integer $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;
    
    
	/**
	 * @return the $id_concursoproduto
	 */
	public function getId_concursoproduto() {
		return $this->id_concursoproduto;
	}

	/**
	 * @return the $id_concurso
	 */
	public function getId_concurso() {
		return $this->id_concurso;
	}

	/**
	 * @return the $id_produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @param number $id_concursoproduto
	 */
	public function setId_concursoproduto($id_concursoproduto) {
		$this->id_concursoproduto = $id_concursoproduto;
		return $this;
	}

	/**
	 * @param number $id_concurso
	 */
	public function setId_concurso($id_concurso) {
		$this->id_concurso = $id_concurso;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

    
    

}
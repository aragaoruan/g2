<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_turmahorario")
 * @Entity
 * @author Helder Fernandes <helder.silva@unyleya.com.br>
 */
class TurmaHorario
{
/**
* @Id 
* @var integer $id_turmahorario
* @Column(name="id_turmahorario", type="integer", nullable=false, length=4)
* @GeneratedValue(strategy="IDENTITY")
*/
 private $id_turmahorario;
    /**
* @var datetime2 $dt_cadastro
* @Column(name="dt_cadastro", type="datetime2", nullable=true, length=8)
*/
 private $dt_cadastro;
/**
* @var integer $id_turma
* @Column(name="id_turma", type="integer", nullable=false, length=4)
*/
 private $id_turma;
/**
* @var integer $id_horarioaula
* @Column(name="id_horarioaula", type="integer", nullable=false, length=4)
*/
 private $id_horarioaula;
/**
* @var integer $id_usuariocadastro
* @Column(name="id_usuariocadastro", type="integer", nullable=true, length=4)
*/
 private $id_usuariocadastro;
/**
* @var boolean $bl_ativo
* @Column(name="bl_ativo", type="boolean", nullable=true, length=1)
*/
 private $bl_ativo;
 
 
/**
* @return datetime2 dt_cadastro
*/
public function getDt_cadastro() {
return $this->dt_cadastro;
}

/**
* @param dt_cadastro
*/
public function setDt_cadastro($dt_cadastro) {
$this->dt_cadastro = $dt_cadastro;
return $this;
}

/**
* @return integer id_turmahorario
*/
public function getId_turmahorario() {
return $this->id_turmahorario;
}

/**
* @param id_turmahorario
*/
public function setId_turmahorario($id_turmahorario) {
$this->id_turmahorario = $id_turmahorario;
return $this;
}

/**
* @return integer id_turma
*/
public function getId_turma() {
return $this->id_turma;
}

/**
* @param id_turma
*/
public function setId_turma($id_turma) {
$this->id_turma = $id_turma;
return $this;
}

/**
* @return integer id_horarioaula
*/
public function getId_horarioaula() {
return $this->id_horarioaula;
}

/**
* @param id_horarioaula
*/
public function setId_horarioaula($id_horarioaula) {
$this->id_horarioaula = $id_horarioaula;
return $this;
}

/**
* @return integer id_usuariocadastro
*/
public function getId_usuariocadastro() {
return $this->id_usuariocadastro;
}

/**
* @param id_usuariocadastro
*/
public function setId_usuariocadastro($id_usuariocadastro) {
$this->id_usuariocadastro = $id_usuariocadastro;
return $this;
}

/**
* @return boolean bl_ativo
*/
public function getBl_ativo() {
return $this->bl_ativo;
}

/**
* @param bl_ativo
*/
public function setBl_ativo($bl_ativo) {
$this->bl_ativo = $bl_ativo;
return $this;
}

}


<?php

/*
 * Entity TipoDocumento
 * @author: Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since: 2014-09-26
 */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_tipodocumento")
 * @Entity
 * @author Caio Eduardio <caio.teixeira@unyleya.com.br>
 */
class TipoDocumento {

    /**
     *
     * @var integer $id_tipodocumento
     * @Column(name="id_tipodocumento", type="integer",nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_tipodocumento;


    /**
     * @Column(type="string",length=30,nullable=false, name="st_tipodocumento")
     * @var string
     */
    private $st_tipodocumento;

    /**
     * @var boolean $bl_ativo
     * @Column(name="bl_ativo", type="boolean", nullable=false)
     */
    private $bl_ativo;


    public function getId_tipodocumento() {
        return $this->id_tipodocumento;
    }

    public function getSt_tipodocumento() {
        return $this->st_tipodocumento;
    }

    public function getBl_ativo() {
        return $this->bl_ativo;
    }

    public function setId_tipodocumento($id_tipodocumento) {
        $this->id_tipodocumento = $id_tipodocumento;
    }

    public function setSt_tipodocumento($st_tipodocumento) {
        $this->st_tipodocumento = $st_tipodocumento;
    }

    public function setBl_ativo($bl_ativo) {
        $this->bl_ativo = $bl_ativo;
    }


}

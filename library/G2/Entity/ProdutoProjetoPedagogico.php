<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_produtoprojetopedagogico")
 * @Entity
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br> - 24/09/2014
 */
class ProdutoProjetoPedagogico extends G2Entity
{

    /**
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     * @var integer $id_produtoprojetopedagogico
     * @Column(name="id_produtoprojetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_produtoprojetopedagogico;

    /**
     * @ManyToOne(targetEntity="ProjetoPedagogico")
     * @JoinColumn(name="id_projetopedagogico", nullable=false, referencedColumnName="id_projetopedagogico")
     */
    private $id_projetopedagogico;

    /**
     * var Produto $id_produto
     * @ManyToOne(targetEntity="Produto")
     * @JoinColumn(name="id_produto", nullable=false, referencedColumnName="id_produto")
     */
    private $id_produto;

    /**
     * @ManyToOne(targetEntity="Entidade")
     * @JoinColumn(name="id_entidade", nullable=false, referencedColumnName="id_entidade")
     */
    private $id_entidade;

    /**
     * @var integer $nu_tempoacesso
     * @Column(name="nu_tempoacesso", type="integer", nullable=true, length=4)
     */
    private $nu_tempoacesso;

    /**
     * @ManyToOne(targetEntity="Turma")
     * @JoinColumn(name="id_turma", nullable=false, referencedColumnName="id_turma")
     */
    private $id_turma;

    /**
     * @var boolean  $bl_indeterminado
     * @Column(type="boolean", nullable=false, name="bl_indeterminado")
     */
    private $bl_indeterminado;

    /**
     * @var integer $nu_cargahoraria
     * @Column(name="nu_cargahoraria", type="integer", nullable=true, length=9)
     */
    private $nu_cargahoraria;


    /**
     *
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true)
     */
    private $st_disciplina;

    /**
     * var ContratoRegra $id_contratoregra
     * @ManyToOne(targetEntity="ContratoRegra")
     * @JoinColumn(name="id_contratoregra", nullable=false, referencedColumnName="id_contratoregra")
     */
    private $id_contratoregra;

    /**
     * @param string $st_disciplina
     */
    public function setst_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    /**
     * @return string
     */
    public function getst_disciplina()
    {
        return $this->st_disciplina;
    }


    /**
     * @param int $nu_cargahoraria
     */
    public function setnu_cargahoraria($nu_cargahoraria)
    {
        $this->nu_cargahoraria = $nu_cargahoraria;
        return $this;
    }

    /**
     * @return int
     */
    public function getnu_cargahoraria()
    {
        return $this->nu_cargahoraria;
    }

    /**
     * @param boolean $bl_indeterminado
     */
    public function setbl_indeterminado($bl_indeterminado)
    {
        $this->bl_indeterminado = $bl_indeterminado;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getbl_indeterminado()
    {
        return $this->bl_indeterminado;
    }
    
	/**
	 * @return ProjetoPedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return Produto
	 */
	public function getId_produto() {
		return $this->id_produto;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $nu_tempoacesso
	 */
	public function getNu_tempoacesso() {
		return $this->nu_tempoacesso;
	}

	/**
	 * @param number $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
		return $this;
	}

	/**
	 * @param number $id_produto
	 */
	public function setId_produto($id_produto) {
		$this->id_produto = $id_produto;
		return $this;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @param number $nu_tempoacesso
	 */
	public function setNu_tempoacesso($nu_tempoacesso) {
		$this->nu_tempoacesso = $nu_tempoacesso;
		return $this;
	}

    /**
     * @param int $id_produtoprojetopedagogico
     */
    public function setId_produtoprojetopedagogico($id_produtoprojetopedagogico)
    {
        $this->id_produtoprojetopedagogico = $id_produtoprojetopedagogico;
    }

    /**
     * @return int $id_produtoprojetopedagogico
     */
    public function getId_produtoprojetopedagogico()
    {
        return $this->id_produtoprojetopedagogico;
    }

    /**
     * @param int $id_turma
     */
    public function setId_turma($id_turma)
    {
        $this->id_turma = $id_turma;
    }

    /**
     * @return int
     */
    public function getId_turma()
    {
        return $this->id_turma;
    }

    /**
     * @return mixed
     */
    public function getId_contratoregra()
    {
        return $this->id_contratoregra;
    }

    /**
     * @param mixed $id_contratoregra
     */
    public function setId_contratoregra($id_contratoregra)
    {
        $this->id_contratoregra = $id_contratoregra;
    }



}

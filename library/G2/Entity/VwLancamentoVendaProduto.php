<?php

/* IMPORTANTE!

    Esta Vw foi criada especificamente para as demandas GII-7600 e GII-7601.
    Melhor não utilizá-la em situações não relacionadas aos relatórios
    "Lançamento da Venda Produto" e "Lançamento da Venda Produto por Atendente".

    Devido à incompatibilidade quanto a estrutura dos dados no banco e as exigências
    do cliente, alguns campos são concatenados em caso de múltiplos PRODUTOS por VENDA.

    São eles:
        st_produto
        id_produto
        nu_valorliquido
        nu_valorbruto
        nu_valorproduto

    Por exemplo, para um lançamento X com múltiplos produtos, uma busca pode retornar
    '1234 / 1247 / 1233' no campo id_produto. Isso significa que os campos acima citados
    são STRINGS, mesmo que prefixados com nu_ ou id_. */

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_lancamentovendaproduto")
 * @Entity
 * @EntityView
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 * @author @update Marcus Pereira <marcus.pereira@unyleya.com.br>
 */
class VwLancamentoVendaProduto
{

    /**
     * @var integer $id_atendente
     * @Column(name="id_atendente", type="integer", nullable=false)
     * @Id
     */
    private $id_atendente;

    /**
     * @var string $st_atendente
     * @Column(name="st_atendente", type="string", length=250, nullable=false)
     */
    private $st_atendente;

    /**
     * @var string $st_aluno
     * @Column(name="st_aluno", type="string", nullable=true)
     */
    private $st_aluno;

    /**
     * @var string $st_cpf
     * @Column(name="st_cpf", type="string", nullable=true)
     */
    private $st_cpf;

    /**
     *
     * @var string $st_rg
     * @Column(name="st_rg", type="string", nullable=true)
     */
    private $st_rg;

    /**
     * @var string $st_email
     * @Column(name="st_email", type="string", nullable=true)
     */
    private $st_email;

    /**
     * @var string $st_ddd
     * @Column(name="st_ddd", type="string", nullable=true)
     */
    private $st_ddd;

    /**
     * @var string $st_telefone
     * @Column(name="st_telefone", type="string", nullable=true)
     */
    private $st_telefone;

    /**
     * @var string $st_cep
     * @Column(name="st_cep", type="string", nullable=true)
     */
    private $st_cep;

    /**
     * @var string $st_endereco
     * @Column(name="st_endereco", type="string", nullable=true)
     */
    private $st_endereco;

    /**
     * @var string $st_bairro
     * @Column(name="st_bairro", type="string", nullable=true)
     */
    private $st_bairro;

    /**
     * @var string $st_complemento
     * @Column(name="st_complemento", type="string", nullable=true)
     */
    private $st_complemento;

    /**
     * @var string $nu_numero
     * @Column(name="nu_numero", type="string", nullable=true)
     */
    private $nu_numero;

    /**
     * @var string $st_cidade
     * @Column(name="st_cidade", type="string", nullable=true)
     */
    private $st_cidade;

    /**
     * @var string $sg_uf
     * @Column(name="sg_uf", type="string", nullable=true)
     */
    private $sg_uf;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="integer", nullable=true, length=4)
     */
    private $id_venda;

    /**
     * @var string $st_meiopagamento
     * @Column(name="st_meiopagamento", type="string", nullable=true)
     */
    private $st_meiopagamento;

    /**
     * @var string $nu_valorliquido
     * @Column(name="nu_valorliquido", type="string", nullable=false)
     */
    private $nu_valorliquido;

    /**
     * @var string $nu_valorbruto
     * @Column(name="nu_valorbruto", type="string", nullable=false)
     */
    private $nu_valorbruto;

    /**
     * @var integer $id_aluno
     * @Column(name="id_aluno", type="integer", nullable=true, length=4)
     */
    private $id_aluno;

    /**
     * @var \datetime $dt_confirmacao
     * @Column(name="dt_confirmacao", type="datetime", nullable=false)
     */
    private $dt_confirmacao;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="string", nullable=true)
     */
    private $id_produto;

    /**
     * @var string $st_produto
     * @Column(name="st_produto", type="string", nullable=true)
     */
    private $st_produto;

    /**
     * @var \datetime $dt_vencimento
     * @Column(name="dt_vencimento", type="datetime", nullable=false)
     */
    private $dt_vencimento;

    /**
     * @var \datetime $dt_baixa
     * @Column(name="dt_baixa", type="datetime", nullable=false)
     */
    private $dt_baixa;

    /**
     * @var decimal $nu_valorlancamento
     * @Column(name="nu_valorlancamento", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valorlancamento;

    /**
     * @var decimal $nu_valorlancamentopago
     * @Column(name="nu_valorlancamentopago", type="decimal", precision=30, scale=2, nullable=false)
     */
    private $nu_valorlancamentopago;

    /**
     * @var \datetime $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime", nullable=false)
     */
    private $dt_cadastro;

    /**
     * @var string $nu_valorproduto
     * @Column(name="nu_valorproduto", type="string", nullable=false)
     */
    private $nu_valorproduto;

    /**
     * @var integer $id_lancamento
     * @Column(name="id_lancamento", type="integer", nullable=true, length=4)
     */
    private $id_lancamento;

    /**
     * @var integer $nu_ordem
     * @Column(name="nu_ordem", type="integer", nullable=true)
     */
    private $nu_ordem;

    /**
     * @var integer $nu_parcelas
     * @Column(name="nu_parcelas", type="integer", nullable=true)
     */
    private $nu_parcelas;

    /**
     * @var integer $st_nomeentidade
     * @Column(name="st_nomeentidade", type="string", nullable=true)
     */
    private $st_nomeentidade;

    /**
     * @return int
     */
    public function getId_atendente()
    {
        return $this->id_atendente;
    }

    /**
     * @param int $id_atendente
     */
    public function setId_atendente($id_atendente)
    {
        $this->id_atendente = $id_atendente;
    }

    /**
     * @return string
     */
    public function getSt_atendente()
    {
        return $this->st_atendente;
    }

    /**
     * @param string $st_atendente
     */
    public function setSt_atendente($st_atendente)
    {
        $this->st_atendente = $st_atendente;
    }

    /**
     * @return string
     */
    public function getSt_aluno()
    {
        return $this->st_aluno;
    }

    /**
     * @param string $st_aluno
     */
    public function setSt_aluno($st_aluno)
    {
        $this->st_aluno = $st_aluno;
    }

    /**
     * @return string
     */
    public function getSt_cpf()
    {
        return $this->st_cpf;
    }

    /**
     * @param string $st_cpf
     */
    public function setSt_cpf($st_cpf)
    {
        $this->st_cpf = $st_cpf;
    }

    /**
     * @return string
     */
    public function getSt_rg()
    {
        return $this->st_rg;
    }

    /**
     * @param string $st_rg
     */
    public function setSt_rg($st_rg)
    {
        $this->st_rg = $st_rg;
    }

    /**
     * @return string
     */
    public function getSt_email()
    {
        return $this->st_email;
    }

    /**
     * @param string $st_email
     */
    public function setSt_email($st_email)
    {
        $this->st_email = $st_email;
    }

    /**
     * @return string
     */
    public function getSt_telefone()
    {
        return $this->st_telefone;
    }

    /**
     * @param string $st_telefone
     */
    public function setSt_telefone($st_telefone)
    {
        $this->st_telefone = $st_telefone;
    }

    /**
     * @return string
     */
    public function getSt_cep()
    {
        return $this->st_cep;
    }

    /**
     * @param string $st_cep
     */
    public function setSt_cep($st_cep)
    {
        $this->st_cep = $st_cep;
    }

    /**
     * @return string
     */
    public function getSt_endereco()
    {
        return $this->st_endereco;
    }

    /**
     * @param string $st_endereco
     */
    public function setSt_endereco($st_endereco)
    {
        $this->st_endereco = $st_endereco;
    }

    /**
     * @return string
     */
    public function getSt_bairro()
    {
        return $this->st_bairro;
    }

    /**
     * @param string $st_bairro
     */
    public function setSt_bairro($st_bairro)
    {
        $this->st_bairro = $st_bairro;
    }

    /**
     * @return string
     */
    public function getSt_complemento()
    {
        return $this->st_complemento;
    }

    /**
     * @param string $st_complemento
     */
    public function setSt_complemento($st_complemento)
    {
        $this->st_complemento = $st_complemento;
    }

    /**
     * @return string
     */
    public function getNu_numero()
    {
        return $this->nu_numero;
    }

    /**
     * @param string $nu_numero
     */
    public function setNu_numero($nu_numero)
    {
        $this->nu_numero = $nu_numero;
    }

    /**
     * @return string
     */
    public function getSt_cidade()
    {
        return $this->st_cidade;
    }

    /**
     * @param string $st_cidade
     */
    public function setSt_cidade($st_cidade)
    {
        $this->st_cidade = $st_cidade;
    }

    /**
     * @return string
     */
    public function getSg_uf()
    {
        return $this->sg_uf;
    }

    /**
     * @param string $sg_uf
     */
    public function setSg_uf($sg_uf)
    {
        $this->sg_uf = $sg_uf;
    }

    /**
     * @return int
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param int $id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
    }

    /**
     * @return int
     */
    public function getId_venda()
    {
        return $this->id_venda;
    }

    /**
     * @param int $id_venda
     */
    public function setId_venda($id_venda)
    {
        $this->id_venda = $id_venda;
    }

    /**
     * @return string
     */
    public function getSt_meiopagamento()
    {
        return $this->st_meiopagamento;
    }

    /**
     * @param string $st_meiopagamento
     */
    public function setSt_meiopagamento($st_meiopagamento)
    {
        $this->st_meiopagamento = $st_meiopagamento;
    }

    /**
     * @return float
     */
    public function getNu_valorliquido()
    {
        return $this->nu_valorliquido;
    }

    /**
     * @param float $nu_valorliquido
     */
    public function setNu_valorliquido($nu_valorliquido)
    {
        $this->nu_valorliquido = $nu_valorliquido;
    }

    /**
     * @return float
     */
    public function getNu_valorbruto()
    {
        return $this->nu_valorbruto;
    }

    /**
     * @param float $nu_valorbruto
     */
    public function setNu_valorbruto($nu_valorbruto)
    {
        $this->nu_valorbruto = $nu_valorbruto;
    }

    /**
     * @return int
     */
    public function getId_aluno()
    {
        return $this->id_aluno;
    }

    /**
     * @param int $id_aluno
     */
    public function setId_aluno($id_aluno)
    {
        $this->id_aluno = $id_aluno;
    }

    /**
     * @return \datetime
     */
    public function getDt_confirmacao()
    {
        return $this->dt_confirmacao;
    }

    /**
     * @param \datetime $dt_confirmacao
     */
    public function setDt_confirmacao($dt_confirmacao)
    {
        $this->dt_confirmacao = $dt_confirmacao;
    }

    /**
     * @return int
     */
    public function getId_produto()
    {
        return $this->id_produto;
    }

    /**
     * @param int $id_produto
     */
    public function setId_produto($id_produto)
    {
        $this->id_produto = $id_produto;
    }

    /**
     * @return string
     */
    public function getSt_produto()
    {
        return $this->st_produto;
    }

    /**
     * @param string $st_produto
     */
    public function setSt_produto($st_produto)
    {
        $this->st_produto = $st_produto;
    }

    /**
     * @return \datetime
     */
    public function getDt_vencimento()
    {
        return $this->dt_vencimento;
    }

    /**
     * @param \datetime $dt_vencimento
     */
    public function setDt_vencimento($dt_vencimento)
    {
        $this->dt_vencimento = $dt_vencimento;
    }

    /**
     * @return \datetime
     */
    public function getDt_baixa()
    {
        return $this->dt_baixa;
    }

    /**
     * @param \datetime $dt_baixa
     */
    public function setDt_baixa($dt_baixa)
    {
        $this->dt_baixa = $dt_baixa;
    }

    /**
     * @return float
     */
    public function getNu_valorlancamento()
    {
        return $this->nu_valorlancamento;
    }

    /**
     * @param float $nu_valorlancamento
     */
    public function setNu_valorlancamento($nu_valorlancamento)
    {
        $this->nu_valorlancamento = $nu_valorlancamento;
    }

    /**
     * @return float
     */
    public function getNu_valorlancamentopago()
    {
        return $this->nu_valorlancamentopago;
    }

    /**
     * @param float $nu_valorlancamentopago
     */
    public function setNu_valorlancamentopago($nu_valorlancamentopago)
    {
        $this->nu_valorlancamentopago = $nu_valorlancamentopago;
    }

    /**
     * @return \datetime
     */
    public function getDt_cadastro()
    {
        return $this->dt_cadastro;
    }

    /**
     * @param \datetime $dt_cadastro
     */
    public function setDt_cadastro($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
    }

    /**
     * @return float
     */
    public function getNu_valorproduto()
    {
        return $this->nu_valorproduto;
    }

    /**
     * @param float $nu_valorproduto
     */
    public function setNu_valorproduto($nu_valorproduto)
    {
        $this->nu_valorproduto = $nu_valorproduto;
    }

    /**
     * @return int
     */
    public function getId_lancamento()
    {
        return $this->id_lancamento;
    }

    /**
     * @param int $id_lancamento
     */
    public function setId_lancamento($id_lancamento)
    {
        $this->id_lancamento = $id_lancamento;
    }

    /**
     * @return int
     */
    public function getNu_ordem()
    {
        return $this->nu_ordem;
    }

    /**
     * @param int $nu_ordem
     */
    public function setNu_ordem($nu_ordem)
    {
        $this->nu_ordem = $nu_ordem;
    }

    /**
     * @return int
     */
    public function getNu_parcelas()
    {
        return $this->nu_parcelas;
    }

    /**
     * @param int $nu_parcelas
     */
    public function setNu_parcelas($nu_parcelas)
    {
        $this->nu_parcelas = $nu_parcelas;
    }

    /**
     * @return string
     */
    public function getSt_nomeentidade()
    {
        return $this->st_nomeentidade;
    }

    /**
     * @param string $st_nomeentidade
     */
    public function setSt_nomeentidade($st_nomeentidade)
    {
        $this->st_nomeentidade = $st_nomeentidade;
    }

    /**
     * @return string
     */
    public function getSt_ddd()
    {
        return $this->st_ddd;
    }

    /**
     * @param string $st_ddd
     */
    public function setSt_ddd($st_ddd)
    {
        $this->st_ddd = $st_ddd;
    }


}
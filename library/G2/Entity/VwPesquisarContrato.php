<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_pesquisarcontrato")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarContrato
{

    /**
     *
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_usuario;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=255)
     */
    private $st_nomecompleto;

    /**
     *
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false)
     */
    private $id_entidade;

    /**
     *
     * @var integer $id_contrato
     * @Column(name="id_contrato", type="integer", nullable=false)
     */
    private $id_contrato;

    /**
     *
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     */
    private $id_evolucao;

    /**
     *
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=false)
     */
    private $id_situacao;

    /**
     *
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=false, length=255)
     */
    private $st_situacao;

    /**
     *
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255)
     */
    private $st_evolucao;

    public function getId_usuario ()
    {
        return $this->id_usuario;
    }

    public function setId_usuario ($id_usuario)
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getId_contrato ()
    {
        return $this->id_contrato;
    }

    public function setId_contrato ($id_contrato)
    {
        $this->id_contrato = $id_contrato;
        return $this;
    }

    public function getId_evolucao ()
    {
        return $this->id_evolucao;
    }

    public function setId_evolucao ($id_evolucao)
    {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function getId_situacao ()
    {
        return $this->id_situacao;
    }

    public function setId_situacao ($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    public function getSt_situacao ()
    {
        return $this->st_situacao;
    }

    public function setSt_situacao ($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    public function getSt_evolucao ()
    {
        return $this->st_evolucao;
    }

    public function setSt_evolucao ($st_evolucao)
    {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

}
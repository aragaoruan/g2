<?php

namespace G2\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;

/**
 * @Entity
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_notificacaoentidadeperfil")
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class NotificacaoEntidadePerfil
{

    /**
     * @Id
     * @var integer $id_notificacaoentidadeperfil
     * @Column(name="id_notificacaoentidadeperfil", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_notificacaoentidadeperfil;

    /**
     * @var integer $id_notificacaoentidade
     * @ManyToOne(targetEntity="NotificacaoEntidade")
     * @JoinColumn(name="id_notificacaoentidade", referencedColumnName="id_notificacaoentidade")
     */
    private $id_notificacaoentidade;

    /**
     * @var integer $id_perfil
     * @ManyToOne(targetEntity="Perfil")
     * @JoinColumn(name="id_perfil", referencedColumnName="id_perfil")
     */
    private $id_perfil;

    /**
     * @return integer id_notificacaoentidadeperfil
     */
    public function getId_notificacaoentidadeperfil()
    {
        return $this->id_notificacaoentidadeperfil;
    }

    /**
     * @param id_notificacaoentidadeperfil
     */
    public function setId_notificacaoentidadeperfil($id_notificacaoentidadeperfil)
    {
        $this->id_notificacaoentidadeperfil = $id_notificacaoentidadeperfil;
        return $this;
    }

    /**
     * @return integer id_notificacaoentidade
     */
    public function getId_notificacaoentidade()
    {
        return $this->id_notificacaoentidade;
    }

    /**
     * @param id_notificacaoentidade
     */
    public function setId_notificacaoentidade(\G2\Entity\NotificacaoEntidade $id_notificacaoentidade)
    {
        $this->id_notificacaoentidade = $id_notificacaoentidade;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil()
    {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil(\G2\Entity\Perfil $id_perfil)
    {
        $this->id_perfil = $id_perfil;
        return $this;
    }

} 
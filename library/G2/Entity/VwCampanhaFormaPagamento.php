<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_campanhaformapagamento")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwCampanhaFormaPagamento
{

    /**
     * @var integer $id_campanhacomercial
     * @Column(type="integer", nullable=false, name="id_campanhacomercial") 
     */
    private $id_campanhacomercial;

    /**
     * @var string $st_campanhacomercial
     * @Column(type="string", length=255, nullable=false, name="st_campanhacomercial")
     */
    private $st_campanhacomercial;

    /**
     * @var integer $id_formapagamento
     * @Column(type="integer", nullable=false, name="id_formapagamento")
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_formapagamento;

    /**
     * @var string $st_formapagamento
     * @Column(type="string", length=255, nullable=false, name="st_formapagamento")
     */
    private $st_formapagamento;

    /**
     * @var integer $id_entidade
     * @Column(type="integer", nullable=false, name="id_entidade")
     */
    private $id_entidade;

    public function getId_campanhacomercial ()
    {
        return $this->id_campanhacomercial;
    }

    public function getSt_campanhacomercial ()
    {
        return $this->st_campanhacomercial;
    }

    public function getId_formapagamento ()
    {
        return $this->id_formapagamento;
    }

    public function getSt_formapagamento ()
    {
        return $this->st_formapagamento;
    }

    public function getId_entidade ()
    {
        return $this->id_entidade;
    }

    public function setId_campanhacomercial ($id_campanhacomercial)
    {
        $this->id_campanhacomercial = $id_campanhacomercial;
        return $this;
    }

    public function setSt_campanhacomercial ($st_campanhacomercial)
    {
        $this->st_campanhacomercial = $st_campanhacomercial;
        return $this;
    }

    public function setId_formapagamento ($id_formapagamento)
    {
        $this->id_formapagamento = $id_formapagamento;
        return $this;
    }

    public function setSt_formapagamento ($st_formapagamento)
    {
        $this->st_formapagamento = $st_formapagamento;
        return $this;
    }

    public function setId_entidade ($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

}

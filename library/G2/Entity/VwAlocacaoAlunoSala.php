<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_alocacaoalunosala")
 * @Entity
 * @EntityView
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class VwAlocacaoAlunoSala
{

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=false, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=false, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_disciplina;

    /**
     * @var integer $id_alocacaonormal
     * @Column(name="id_alocacaonormal", type="integer", nullable=true, length=4)
     */
    private $id_alocacaonormal;

    /**
     * @var integer $id_saladeaulanormal
     * @Column(name="id_saladeaulanormal", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaulanormal;

    /**
     * @var integer $id_alocacaoprr
     * @Column(name="id_alocacaoprr", type="integer", nullable=true, length=4)
     */
    private $id_alocacaoprr;

    /**
     * @var integer $id_saladeaulaprr
     * @Column(name="id_saladeaulaprr", type="integer", nullable=true, length=4)
     * @Id
     */
    private $id_saladeaulaprr;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=false, length=4)
     */
    private $id_modulo;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;

    /**
     * @var integer $id_matriculadisciplinanor
     * @Column(name="id_matriculadisciplinanor", type="integer", nullable=true, length=4)
     */
    private $id_matriculadisciplinanor;
    /**
     * @var integer $nu_diasacessoatualn
     * @Column(name="nu_diasacessoatualn", type="integer", nullable=true, length=4)
     */
    private $nu_diasacessoatualn;
    /**
     * @var integer $id_matriculadisciplinaprr
     * @Column(name="id_matriculadisciplinaprr", type="integer", nullable=true, length=4)
     */
    private $id_matriculadisciplinaprr;

    /**
     * @var integer $id_tipodisciplina
     * @Column(name="id_tipodisciplina", type="integer", nullable=false, length=4)
     */
    private $id_tipodisciplina;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=false, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=false, length=255)
     */
    private $st_disciplina;

    /**
     * @var string $st_saladeaulanormal
     * @Column(name="st_saladeaulanormal", type="string", nullable=true, length=255)
     */
    private $st_saladeaulanormal;

    /**
     * @var string $st_saladeaulaprr
     * @Column(name="st_saladeaulaprr", type="string", nullable=true, length=255)
     */
    private $st_saladeaulaprr;

    /**
     * @var date $dt_aberturanormal
     * @Column(name="dt_aberturanormal", type="date", nullable=true)
     */
    private $dt_aberturanormal;

    /**
     * @var date $dt_aberturaprr
     * @Column(name="dt_aberturaprr", type="date", nullable=true)
     */
    private $dt_aberturaprr;

    /**
     * @var date $dt_inicioalocacaonormal
     * @Column(name="dt_inicioalocacaonormal", type="date", nullable=true)
     */
    private $dt_inicioalocacaonormal;



    /**
     * @var integer $nu_diasacessoatualp
     * @Column(name="nu_diasacessoatualp", type="integer", nullable=true,length=4)
     */
    private $nu_diasacessoatualp;

    /**
     * @var integer $nu_diasextensaonormal
     * @Column(name="nu_diasextensaonormal", type="integer", nullable=true,length=4)
     */
    private $nu_diasextensaonormal;
    /**
     * @var integer $nu_diasalunonormal
     * @Column(name="nu_diasalunonormal", type="integer", nullable=true,length=4)
     */
    private $nu_diasalunonormal;
    /**
     * @var date $dt_inicioalocacaoprr
     * @Column(name="dt_inicioalocacaoprr", type="date", nullable=true)
     */
    private $dt_inicioalocacaoprr;
    /**
     * @var integer $nu_diasextensaoprr
     * @Column(name="nu_diasextensaoprr", type="integer", nullable=true,length=4)
     */
    private $nu_diasextensaoprr;
    /**
     * @var integer $nu_diasalunoprr
     * @Column(name="nu_diasalunoprr", type="integer", nullable=true,length=4)
     */
    private $nu_diasalunoprr;

    /**
     * @var integer $id_tiposalanormal
     * @Column(name="id_tiposalanormal", type="integer", nullable=false, length=4)
     */
    private $id_tiposalanormal;

    /**
     * @var integer $id_tiposalaprr
     * @Column(name="id_tiposalaprr", type="integer", nullable=false, length=4)
     */
    private $id_tiposalaprr;

    /**
     * @var integer $id_situacaomatriculadisciplina
     * @Column(name="id_situacaomatriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_situacaomatriculadisciplina;

    /**
     * @var integer $id_evolucaomatriculadisciplina
     * @Column(name="id_evolucaomatriculadisciplina", type="integer", nullable=false, length=4)
     */
    private $id_evolucaomatriculadisciplina;


    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    public function getId_projetopedagogico()
    {
        return $this->id_projetopedagogico;
    }

    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    public function getId_alocacaonormal()
    {
        return $this->id_alocacaonormal;
    }

    public function getId_saladeaulanormal()
    {
        return $this->id_saladeaulanormal;
    }

    public function getId_alocacaoprr()
    {
        return $this->id_alocacaoprr;
    }

    public function getId_saladeaulaprr()
    {
        return $this->id_saladeaulaprr;
    }

    public function getId_modulo()
    {
        return $this->id_modulo;
    }

    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    public function getId_trilha()
    {
        return $this->id_trilha;
    }

    public function getSt_projetopedagogico()
    {
        return $this->st_projetopedagogico;
    }

    public function getSt_disciplina()
    {
        return $this->st_disciplina;
    }

    public function getSt_saladeaulanormal()
    {
        return $this->st_saladeaulanormal;
    }

    public function getSt_saladeaulaprr()
    {
        return $this->st_saladeaulaprr;
    }

    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    public function setId_projetopedagogico($id_projetopedagogico)
    {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    public function setId_alocacaonormal($id_alocacaonormal)
    {
        $this->id_alocacaonormal = $id_alocacaonormal;
    }

    public function setId_saladeaulanormal($id_saladeaulanormal)
    {
        $this->id_saladeaulanormal = $id_saladeaulanormal;
    }

    public function setId_alocacaoprr($id_alocacaoprr)
    {
        $this->id_alocacaoprr = $id_alocacaoprr;
    }

    public function setId_saladeaulaprr($id_saladeaulaprr)
    {
        $this->id_saladeaulaprr = $id_saladeaulaprr;
    }

    public function setId_modulo($id_modulo)
    {
        $this->id_modulo = $id_modulo;
    }

    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    public function setId_trilha($id_trilha)
    {
        $this->id_trilha = $id_trilha;
    }

    public function setSt_projetopedagogico($st_projetopedagogico)
    {
        $this->st_projetopedagogico = $st_projetopedagogico;
    }

    public function setSt_disciplina($st_disciplina)
    {
        $this->st_disciplina = $st_disciplina;
    }

    public function setSt_saladeaulanormal($st_saladeaulanormal)
    {
        $this->st_saladeaulanormal = $st_saladeaulanormal;
    }

    public function setSt_saladeaulaprr($st_saladeaulaprr)
    {
        $this->st_saladeaulaprr = $st_saladeaulaprr;
    }

    public function getId_matriculadisciplinanor()
    {
        return $this->id_matriculadisciplinanor;
    }

    public function getId_matriculadisciplinaprr()
    {
        return $this->id_matriculadisciplinaprr;
    }

    public function setId_matriculadisciplinanor($id_matriculadisciplinanor)
    {
        $this->id_matriculadisciplinanor = $id_matriculadisciplinanor;
    }

    public function setId_matriculadisciplinaprr($id_matriculadisciplinaprr)
    {
        $this->id_matriculadisciplinaprr = $id_matriculadisciplinaprr;
    }

    public function getDt_aberturanormal()
    {
        return $this->dt_aberturanormal;
    }

    public function getDt_aberturaprr()
    {
        return $this->dt_aberturaprr;
    }

    public function setDt_aberturanormal(date $dt_aberturanormal)
    {
        $this->dt_aberturanormal = $dt_aberturanormal;
    }

    public function setDt_aberturaprr(date $dt_aberturaprr)
    {
        $this->dt_aberturaprr = $dt_aberturaprr;
    }

    /**
     * @param int $id_tipodisciplina
     */
    public function setId_tipodisciplina($id_tipodisciplina)
    {
        $this->id_tipodisciplina = $id_tipodisciplina;
    }

    /**
     * @return int
     */
    public function getId_tipodisciplina()
    {
        return $this->id_tipodisciplina;
    }

    /**
     * @param int $nu_diasextensaoprr
     */
    public function setNu_diasextensaoprr($nu_diasextensaoprr)
    {
        $this->nu_diasextensaoprr = $nu_diasextensaoprr;
    }

    /**
     * @return int
     */
    public function getNu_diasextensaoprr()
    {
        return $this->nu_diasextensaoprr;
    }

    /**
     * @param int $nu_diasextensaonormal
     */
    public function setNu_diasextensaonormal($nu_diasextensaonormal)
    {
        $this->nu_diasextensaonormal = $nu_diasextensaonormal;
    }

    /**
     * @return int
     */
    public function getNu_diasextensaonormal()
    {
        return $this->nu_diasextensaonormal;
    }

    /**
     * @param int $nu_diasalunoprr
     */
    public function setNu_diasalunoprr($nu_diasalunoprr)
    {
        $this->nu_diasalunoprr = $nu_diasalunoprr;
    }

    /**
     * @return int
     */
    public function getNu_diasalunoprr()
    {
        return $this->nu_diasalunoprr;
    }

    /**
     * @param int $nu_diasalunonormal
     */
    public function setNu_diasalunonormal($nu_diasalunonormal)
    {
        $this->nu_diasalunonormal = $nu_diasalunonormal;
    }

    /**
     * @return int
     */
    public function getNu_diasalunonormal()
    {
        return $this->nu_diasalunonormal;
    }

    /**
     * @param date $dt_inicioalocacaonormal
     */
    public function setDt_inicioalocacaonormal($dt_inicioalocacaonormal)
    {
        $this->dt_inicioalocacaonormal = $dt_inicioalocacaonormal;
    }

    /**
     * @return date
     */
    public function getDt_inicioalocacaonormal()
    {
        return $this->dt_inicioalocacaonormal;
    }

    /**
     * @param date $dt_inicioalocacaoprr
     */
    public function setDt_inicioalocacaoprr($dt_inicioalocacaoprr)
    {
        $this->dt_inicioalocacaoprr = $dt_inicioalocacaoprr;
    }

    /**
     * @return date
     */
    public function getDt_inicioalocacaoprr()
    {
        return $this->dt_inicioalocacaoprr;
    }

    /**
     * @param integer $nu_diasacessoatualp
     */
    public function setNu_diasacessoatualp($nu_diasacessoatualp)
    {
        $this->nu_diasacessoatualp = $nu_diasacessoatualp;
    }

    /**
     * @return integer
     */
    public function getNu_diasacessoatualp()
    {
        return $this->nu_diasacessoatualp;
    }

    /**
     * @param integer $nu_diasacessoatualn
     */
    public function setNu_diasacessoatualn($nu_diasacessoatualn)
    {
        $this->nu_diasacessoatualn = $nu_diasacessoatualn;
    }

    /**
     * @return integer
     */
    public function getNu_diasacessoatualn()
    {
        return $this->nu_diasacessoatualn;
    }

    /**
     * @param int $id_tiposalanormal
     */
    public function setId_tiposalanormal($id_tiposalanormal)
    {
        $this->id_tiposalanormal = $id_tiposalanormal;
    }

    /**
     * @return int
     */
    public function getId_tiposalanormal()
    {
        return $this->id_tiposalanormal;
    }

    /**
     * @param int $id_tiposalaprr
     */
    public function setId_tiposalaprr($id_tiposalaprr)
    {
        $this->id_tiposalaprr = $id_tiposalaprr;
    }

    /**
     * @return int
     */
    public function getId_tiposalaprr()
    {
        return $this->id_tiposalaprr;
    }

    /**
     * @param int $id_tiposalaprr
     */
    public function setId_situacaomatriculadisciplina($id_situacaomatriculadisciplina)
    {
        $this->id_situacaomatriculadisciplina = $id_situacaomatriculadisciplina;
    }

    /**
     * @return int
     */
    public function getId_situacaomatriculadisciplina()
    {
        return $this->id_situacaomatriculadisciplina;
    }

    /**
     * @return int
     */
    public function getId_evolucaomatriculadisciplina()
    {
        return $this->id_evolucaomatriculadisciplina;
    }

    /**
     * @param int $id_evolucaomatriculadisciplina
     */
    public function setId_evolucaomatriculadisciplina($id_evolucaomatriculadisciplina)
    {
        $this->id_evolucaomatriculadisciplina = $id_evolucaomatriculadisciplina;
    }



}

<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_pesquisarprotocolo")
 * @Entity
 * @EntityView
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class VwPesquisarProtocolo
{

    /**
     *
     * @var integer $id_protocolo
     * @Column(name="id_protocolo", type="integer", nullable=false) 
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_protocolo;

    /**
     *
     * @var datetime2 $dt_cadastro
     * @Column(name="dt_cadastro", type="datetime2", nullable=false) 
     */
    private $dt_cadastro;

    /**
     *
     * @var string $st_nomecompleto
     * @Column(name="st_nomecompleto", type="string", nullable=false, length=255) 
     */
    private $st_nomecompleto;

    public function getId_protocolo ()
    {
        return $this->id_protocolo;
    }

    public function setId_protocolo ($id_protocolo)
    {
        $this->id_protocolo = $id_protocolo;
        return $this;
    }

    public function getDt_cadastro ()
    {
        return $this->dt_cadastro;
    }

    public function setDt_cadastro ($dt_cadastro)
    {
        $this->dt_cadastro = $dt_cadastro;
        return $this;
    }

    public function getSt_nomecompleto ()
    {
        return $this->st_nomecompleto;
    }

    public function setSt_nomecompleto ($st_nomecompleto)
    {
        $this->st_nomecompleto = $st_nomecompleto;
        return $this;
    }

}
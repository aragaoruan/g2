<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_avaliacaoconjuntoreferencia")
 * @Entity
 * @EntityView
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwAvaliacaoConjuntoReferencia {

    /**
     *
     * @var integer $id_avaliacaoconjunto
     * @Column(name="id_avaliacaoconjunto", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_avaliacaoconjunto;

    /**
     * @var string $st_avaliacaoconjunto
     * @Column(name="st_avaliacaoconjunto", type="string", nullable=false, length=100)
     */
    private $st_avaliacaoconjunto;


    /**
     * @var string $st_tipoprova
     * @Column(name="st_tipoprova", type="string", nullable=false, length=255)
     */
    private $st_tipoprova;


    /**
     * @var integer $id_avaliacaoconjuntoreferencia
     * @Column(name="id_avaliacaoconjuntoreferencia", type="integer", nullable=true)
     */
    private $id_avaliacaoconjuntoreferencia;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true)
     */
    private $id_saladeaula;

    /**
     * @var integer $id_modulo
     * @Column(name="id_modulo", type="integer", nullable=true)
     */
    private $id_modulo;


    /**
     * @var date $dt_inicio
     * @Column(name="dt_inicio", type="date", nullable=true)
     */
    private $dt_inicio;


    /**
     * @var date $dt_fim
     * @Column(name="dt_fim", type="date", nullable=true)
     */
    private $dt_fim;


    public function getId_avaliacaoconjunto() {
        return $this->id_avaliacaoconjunto;
    }

    public function getSt_avaliacaoconjunto() {
        return $this->st_avaliacaoconjunto;
    }

    public function getSt_tipoprova() {
        return $this->st_tipoprova;
    }

    public function getId_avaliacaoconjuntoreferencia() {
        return $this->id_avaliacaoconjuntoreferencia;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function getId_modulo() {
        return $this->id_modulo;
    }

    public function getDt_inicio() {
        return $this->dt_inicio;
    }

    public function getDt_fim() {
        return $this->dt_fim;
    }

    public function setId_avaliacaoconjunto($id_avaliacaoconjunto) {
        $this->id_avaliacaoconjunto = $id_avaliacaoconjunto;
    }

    public function setSt_avaliacaoconjunto($st_avaliacaoconjunto) {
        $this->st_avaliacaoconjunto = $st_avaliacaoconjunto;
    }

    public function setSt_tipoprova($st_tipoprova) {
        $this->st_tipoprova = $st_tipoprova;
    }

    public function setId_avaliacaoconjuntoreferencia($id_avaliacaoconjuntoreferencia) {
        $this->id_avaliacaoconjuntoreferencia = $id_avaliacaoconjuntoreferencia;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
    }

    public function setId_modulo($id_modulo) {
        $this->id_modulo = $id_modulo;
    }

    public function setDt_inicio($dt_inicio) {
        $this->dt_inicio = $dt_inicio;
    }

    public function setDt_fim($dt_fim) {
        $this->dt_fim = $dt_fim;
    }


}

<?php
/**
 * Created by PhpStorm.
 * User: aragaoRuan
 * Date: 11/07/2018
 * Time: 16:49
 */

namespace G2\Entity;
use G2\G2Entity;

/**
 * Class Entity for VwResumoFinanceiro
 * @Entity
 * @EntityView
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_matriculanotavinculada")
 */
class VwMatriculaNotaVinculada extends G2Entity
{
    /**
     * @var boolean $bl_calcular
     * @Column(name="bl_calcular", type="boolean")
     */
    private $bl_calcular;
    /**
     * @var boolean $bl_aprovado
     * @Column(name="bl_aprovado", type="boolean")
     */
    private $bl_aprovado;
    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer")
     */
    private $id_matriculadisciplina;
    /**
     * @var string $st_notafinal
     * @Column(name="st_notafinal", type="string")
     */
    private $st_notafinal;
    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer")
     */
    private $id_disciplina;
    /**
     * @Id
     * @GeneratedValue(strategy="NONE")
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer")
     */
    private $id_matricula;
    /**
     * @var integer $nu_percentualfinal
     * @Column(name="nu_percentualfinal", type="integer")
     */
    private $nu_percentualfinal;
    /**
     * @return bool
     */
    public function getBl_calcular()
    {
        return $this->bl_calcular;
    }

    /**
     * @param bool $bl_calcular
     */
    public function setBl_calcular($bl_calcular)
    {
        $this->bl_calcular = $bl_calcular;
    }

    /**
     * @return bool
     */
    public function getBl_aprovado()
    {
        return $this->bl_aprovado;
    }

    /**
     * @param bool $bl_aprovado
     */
    public function setBl_aprovado($bl_aprovado)
    {
        $this->bl_aprovado = $bl_aprovado;
    }

    /**
     * @return int
     */
    public function getId_matriculadisciplina()
    {
        return $this->id_matriculadisciplina;
    }

    /**
     * @param int $id_matriculadisciplina
     */
    public function setId_matriculadisciplina($id_matriculadisciplina)
    {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
    }

    /**
     * @return string
     */
    public function getSt_notafinal()
    {
        return $this->st_notafinal;
    }

    /**
     * @param string $st_nota
     */
    public function setSt_notafinal($st_notafinal)
    {
        $this->st_nota = $st_notafinal;
    }

    /**
     * @return int
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param int $id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getNu_percentualfinal()
    {
        return $this->nu_percentualfinal;
    }

    /**
     * @param int $nu_percentualfinal
     */
    public function setNu_percentualfinal($nu_percentualfinal)
    {
        $this->nu_percentualfinal = $nu_percentualfinal;
    }


}
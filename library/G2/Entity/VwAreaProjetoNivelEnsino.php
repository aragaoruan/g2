<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_areaprojetonivelensino")
 * @Entity
 * @EntityView
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class VwAreaProjetoNivelEnsino
{

    /**
     *
     * @var integer $id_areaconhecimento
     * @Column(name="id_areaconhecimento", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="NONE")
     */
    private $id_areaconhecimento;
    
    /**
     * @var integer $id_projetopedagogico
     * @Column(type="integer", name="id_projetopedagogico", nullable=false)
     */
    private $id_projetopedagogico;
    
    /**
     * @var integer $id_entidade
     * @Column(type="integer", name="id_entidade", nullable=false)
     */
    private $id_entidade;
    
    /**
     * @var string $st_areaconhecimento
     * @Column(type="string", length=255, name="st_areaconhecimento", nullable=false)
     */
    
    
    /**
     * @var string $st_projetopedagogico
     * @Column(type="string", length=255, name="st_projetopedagogico", nullable=false)
     */
    private $st_projetopedagogico;
    
    /**
     * @var string $st_tituloexibicao
     * @Column(type="string", length=255, name="st_tituloexibicao", nullable=true)
     */
    private $st_tituloexibicao;
    
    
    /**
     * @var integer $id_nivelensino
     * @Column(type="integer", name="id_nivelensino", nullable=false)
     */
    private $id_nivelensino;
    
    /**
     * @var string $st_nivelensino
     * @Column(type="string", length=255, name="st_nivelensino", nullable=false)
     */
    private $st_nivelensino;

    /**
     * @var string $st_situacao
     * @Column(type="string", length=255, name="st_situacao", nullable=false)
     */
    private $st_situacao;
    
    /**
     * @var string $st_nomeentidade
     * @Column(type="string", length=2500, name="st_nomeentidade", nullable=true)
     */
    private $st_nomeentidade;
    
	/**
	 * @return the $id_areaconhecimento
	 */
	public function getId_areaconhecimento() {
		return $this->id_areaconhecimento;
	}

	/**
	 * @return the $id_projetopedagogico
	 */
	public function getId_projetopedagogico() {
		return $this->id_projetopedagogico;
	}

	/**
	 * @return the $id_entidade
	 */
	public function getId_entidade() {
		return $this->id_entidade;
	}

	/**
	 * @return the $st_projetopedagogico
	 */
	public function getSt_projetopedagogico() {
		return $this->st_projetopedagogico;
	}

	/**
	 * @return the $st_tituloexibicao
	 */
	public function getSt_tituloexibicao() {
		return $this->st_tituloexibicao;
	}

	/**
	 * @return the $id_nivelensino
	 */
	public function getId_nivelensino() {
		return $this->id_nivelensino;
	}

	/**
	 * @return the $st_nivelensino
	 */
	public function getSt_nivelensino() {
		return $this->st_nivelensino;
	}

	/**
	 * @return the $st_situacao
	 */
	public function getSt_situacao() {
		return $this->st_situacao;
	}

	/**
	 * @return the $st_nomeentidade
	 */
	public function getSt_nomeentidade() {
		return $this->st_nomeentidade;
	}

	/**
	 * @param number $id_areaconhecimento
	 */
	public function setId_areaconhecimento($id_areaconhecimento) {
		$this->id_areaconhecimento = $id_areaconhecimento;
		return $this;
	}

	/**
	 * @param number $id_projetopedagogico
	 */
	public function setId_projetopedagogico($id_projetopedagogico) {
		$this->id_projetopedagogico = $id_projetopedagogico;
		return $this;
	}

	/**
	 * @param number $id_entidade
	 */
	public function setId_entidade($id_entidade) {
		$this->id_entidade = $id_entidade;
		return $this;
	}

	/**
	 * @param string $st_projetopedagogico
	 */
	public function setSt_projetopedagogico($st_projetopedagogico) {
		$this->st_projetopedagogico = $st_projetopedagogico;
		return $this;
	}

	/**
	 * @param string $st_tituloexibicao
	 */
	public function setSt_tituloexibicao($st_tituloexibicao) {
		$this->st_tituloexibicao = $st_tituloexibicao;
		return $this;
	}

	/**
	 * @param number $id_nivelensino
	 */
	public function setId_nivelensino($id_nivelensino) {
		$this->id_nivelensino = $id_nivelensino;
		return $this;
	}

	/**
	 * @param string $st_nivelensino
	 */
	public function setSt_nivelensino($st_nivelensino) {
		$this->st_nivelensino = $st_nivelensino;
		return $this;
	}

	/**
	 * @param string $st_situacao
	 */
	public function setSt_situacao($st_situacao) {
		$this->st_situacao = $st_situacao;
		return $this;
	}

	/**
	 * @param string $st_nomeentidade
	 */
	public function setSt_nomeentidade($st_nomeentidade) {
		$this->st_nomeentidade = $st_nomeentidade;
		return $this;
	}

   

}
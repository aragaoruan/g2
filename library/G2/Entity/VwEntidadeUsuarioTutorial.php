<?php

namespace G2\Entity;

/**
 * Classe Entity para VwEntidadeUsuarioTutorial
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="vw_entidadeusuariotutorial")
 * @Entity
 * @EntityView
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class VwEntidadeUsuarioTutorial
{
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_entidade;
    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_usuario;
    /**
     * @var integer $id_perfil
     * @Column(name="id_perfil", type="integer", nullable=false, length=4)
     * @Id
     */
    private $id_perfil;
    /**
     * @var boolean $bl_tutorialaluno
     * @Column(name="bl_tutorialaluno", type="boolean", nullable=true, length=1)
     *
     */
    private $bl_tutorialaluno;
    /**
     * @var boolean $bl_tutorialentidade
     * @Column(name="bl_tutorialentidade", type="boolean", nullable=true, length=1)
     */
    private $bl_tutorialentidade;



    /**
     * @return integer id_entidade
     */
    public function getId_entidade() {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return integer id_usuario
     */
    public function getId_usuario() {
        return $this->id_usuario;
    }

    /**
     * @param id_usuario
     */
    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return integer id_perfil
     */
    public function getId_perfil() {
        return $this->id_perfil;
    }

    /**
     * @param id_perfil
     */
    public function setId_perfil($id_perfil) {
        $this->id_perfil = $id_perfil;
        return $this;
    }

    /**
     * @return boolean bl_tutorialaluno
     */
    public function getBl_tutorialaluno() {
        return $this->bl_tutorialaluno;
    }

    /**
     * @param bl_tutorialaluno
     */
    public function setBl_tutorialaluno($bl_tutorialaluno) {
        $this->bl_tutorialaluno = $bl_tutorialaluno;
        return $this;
    }

    /**
     * @return boolean bl_tutorialentidade
     */
    public function getBl_tutorialentidade() {
        return $this->bl_tutorialentidade;
    }

    /**
     * @param bl_tutorialentidade
     */
    public function setBl_tutorialentidade($bl_tutorialentidade) {
        $this->bl_tutorialentidade = $bl_tutorialentidade;
        return $this;
    }
}
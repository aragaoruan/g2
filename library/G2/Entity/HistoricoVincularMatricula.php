<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 29/05/18
 * Time: 16:22
 */

namespace G2\Entity;


use G2\G2Entity;

/**
 * @Table(name="tb_historicovincularmatricula")
 * @Entity
 * @EntityLog
 */


class HistoricoVincularMatricula extends G2Entity
{
    /**
     * @var integer $id_historicovincularmatricula
     * @Column(name="id_historicovincularmatricula", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_historicovincularmatricula;

    /**
     * @var integer $id_matriculavinculada
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matriculavinculada", nullable=false, referencedColumnName="id_matricula")
     */
    private $id_matriculavinculada;

    /**
     * @var integer $id_matricula
     * @ManyToOne(targetEntity="Matricula")
     * @JoinColumn(name="id_matricula", nullable=false, referencedColumnName="id_matricula")
     */
    private $id_matricula;

    /**
     * @var integer $id_usuario
     * @ManyToOne(targetEntity="Usuario")
     * @JoinColumn(name="id_usuario", nullable=false, referencedColumnName="id_usuario")
     */
    private $id_usuario;

    /**
     * @var datetime2 $dt_historicovincularmatricula
     * @Column(name="dt_historicovincularmatricula", type="datetime2",  nullable=false, length=8)
     */
    private $dt_historicovincularmatricula;

    /**
     * @var string $st_historicovincularmatricula
     * @Column(name="st_historicovincularmatricula", type="string", nullable=false, length=255)
     */
    private $st_historicovincularmatricula;

    /**
     * @var boolean $bl_vinculada
     * @Column(name="bl_vinculada", type="boolean", nullable=false)
     */
    private $bl_vinculada;

    /**
     * @return int
     */
    public function getId_historicovincularmatricula()
    {
        return $this->id_historicovincularmatricula;
    }

    /**
     * @param int $id_historicovincularmatricula
     */
    public function setId_historicovincularmatricula($id_historicovincularmatricula)
    {
        $this->id_historicovincularmatricula = $id_historicovincularmatricula;
    }

    /**
     * @return int
     */
    public function getId_matriculavinculada()
    {
        return $this->id_matriculavinculada;
    }

    /**
     * @param int $id_matriculavinculada
     */
    public function setId_matriculavinculada($id_matriculavinculada)
    {
        $this->id_matriculavinculada = $id_matriculavinculada;
    }

    /**
     * @return int
     */
    public function getId_matricula()
    {
        return $this->id_matricula;
    }

    /**
     * @param int $id_matricula
     */
    public function setId_matricula($id_matricula)
    {
        $this->id_matricula = $id_matricula;
    }

    /**
     * @return int
     */
    public function getId_usuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     */
    public function setId_usuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return datetime2
     */
    public function getDt_historicovincularmatricula()
    {
        return $this->dt_historicovincularmatricula;
    }

    /**
     * @param datetime2 $dt_historicovincularmatricula
     */
    public function setDt_historicovincularmatricula($dt_historicovincularmatricula)
    {
        $this->dt_historicovincularmatricula = $dt_historicovincularmatricula;
    }

    /**
     * @return string
     */
    public function getSt_historicovincularmatricula()
    {
        return $this->st_historicovincularmatricula;
    }

    /**
     * @param string $st_historicovincularmatricula
     */
    public function setSt_historicovincularmatricula($st_historicovincularmatricula)
    {
        $this->st_historicovincularmatricula = $st_historicovincularmatricula;
    }

    /**
     * @return int
     */
    public function getBl_vinculada()
    {
        return $this->bl_vinculada;
    }

    /**
     * @param int $bl_vinculada
     */
    public function setBl_vinculada($bl_vinculada)
    {
        $this->bl_vinculada = $bl_vinculada;
    }


}

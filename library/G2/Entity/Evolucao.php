<?php

namespace G2\Entity;
use G2\G2Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="tb_evolucao")
 * @Entity
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Evolucao extends G2Entity {

    /**
     *
     * @var integer $id_evolucao
     * @Column(name="id_evolucao", type="integer", nullable=false)
     * @Id
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_evolucao;

    /**
     *
     * @var string $st_evolucao
     * @Column(name="st_evolucao", type="string", nullable=false, length=255) 
     */
    private $st_evolucao;

    /**
     *
     * @var string $st_tabela
     * @Column(name="st_tabela", type="string", nullable=false, length=255) 
     */
    private $st_tabela;

    /**
     *
     * @var string $st_campo
     * @Column(name="st_campo", type="string", nullable=false, length=255) 
     */
    private $st_campo;

    /**
     * @var integer $id_evolucao
     * @ManyToOne(targetEntity="Evolucao")
     * @JoinColumn(name="id_evolucao", referencedColumnName="id_evolucao")
     */
    private $id_situacao;

    public function getId_evolucao() {
        return $this->id_evolucao;
    }

    public function setId_evolucao($id_evolucao) {
        $this->id_evolucao = $id_evolucao;
        return $this;
    }

    public function getSt_evolucao() {
        return $this->st_evolucao;
    }

    public function setSt_evolucao($st_evolucao) {
        $this->st_evolucao = $st_evolucao;
        return $this;
    }

    public function getSt_tabela() {
        return $this->st_tabela;
    }

    public function setSt_tabela($st_tabela) {
        $this->st_tabela = $st_tabela;
        return $this;
    }

    public function getSt_campo() {
        return $this->st_campo;
    }

    public function setSt_campo($st_campo) {
        $this->st_campo = $st_campo;
        return $this;
    }

    function getId_situacao() {
        return $this->id_situacao;
    }

    function setId_situacao($id_situacao) {
        $this->id_situacao = $id_situacao;
    }

}

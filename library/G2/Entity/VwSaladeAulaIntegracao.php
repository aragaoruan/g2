<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladeaulaintegracao")
 * @Entity
 * @EntityView
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class VwSaladeAulaIntegracao
{
    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=false, length=4)
     * @GeneratedValue(strategy="NONE")
     */
    private $id_saladeaula;
    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;
    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_situacao
     * @Column(name="id_situacao", type="integer", nullable=true, length=4)
     */
    private $id_situacao;
    /**
     * @var integer $id_modalidadesaladeaula
     * @Column(name="id_modalidadesaladeaula", type="integer", nullable=false, length=4)
     */
    private $id_modalidadesaladeaula;
    /**
     * @var integer $id_sistema
     * @Column(name="id_sistema", type="integer", nullable=true, length=4)
     */
    private $id_sistema;
    /**
     * @var integer $id_saladeaulaintegracao
     * @Column(name="id_saladeaulaintegracao", type="integer", nullable=true, length=4)
     */
    private $id_saladeaulaintegracao;
    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=false, length=4)
     */
    private $id_entidade;
    /**
     * @var boolean $bl_ativa
     * @Column(name="bl_ativa", type="boolean", nullable=false, length=1)
     */
    private $bl_ativa;
    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=false, length=255)
     */
    private $st_saladeaula;
    /**
     * @var string $st_situacao
     * @Column(name="st_situacao", type="string", nullable=true, length=255)
     */
    private $st_situacao;
    /**
     * @var string $st_modalidadesaladeaula
     * @Column(name="st_modalidadesaladeaula", type="string", nullable=false, length=255)
     */
    private $st_modalidadesaladeaula;
    /**
     * @var string $st_sistema
     * @Column(name="st_sistema", type="string", nullable=true, length=255)
     */
    private $st_sistema;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true)
     */
    private $id_disciplina;

    /**
     * @return date dt_abertura
     */
    public function getDt_abertura()
    {
        return $this->dt_abertura;
    }

    /**
     * @param dt_abertura
     */
    public function setDt_abertura($dt_abertura)
    {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    /**
     * @return date dt_encerramento
     */
    public function getDt_encerramento()
    {
        return $this->dt_encerramento;
    }

    /**
     * @param dt_encerramento
     */
    public function setDt_encerramento($dt_encerramento)
    {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    /**
     * @return integer id_saladeaula
     */
    public function getId_saladeaula()
    {
        return $this->id_saladeaula;
    }

    /**
     * @param id_saladeaula
     */
    public function setId_saladeaula($id_saladeaula)
    {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    /**
     * @return integer id_situacao
     */
    public function getId_situacao()
    {
        return $this->id_situacao;
    }

    /**
     * @param id_situacao
     */
    public function setId_situacao($id_situacao)
    {
        $this->id_situacao = $id_situacao;
        return $this;
    }

    /**
     * @return integer id_modalidadesaladeaula
     */
    public function getId_modalidadesaladeaula()
    {
        return $this->id_modalidadesaladeaula;
    }

    /**
     * @param id_modalidadesaladeaula
     */
    public function setId_modalidadesaladeaula($id_modalidadesaladeaula)
    {
        $this->id_modalidadesaladeaula = $id_modalidadesaladeaula;
        return $this;
    }

    /**
     * @return integer id_sistema
     */
    public function getId_sistema()
    {
        return $this->id_sistema;
    }

    /**
     * @param id_sistema
     */
    public function setId_sistema($id_sistema)
    {
        $this->id_sistema = $id_sistema;
        return $this;
    }

    /**
     * @return integer id_saladeaulaintegracao
     */
    public function getId_saladeaulaintegracao()
    {
        return $this->id_saladeaulaintegracao;
    }

    /**
     * @param id_saladeaulaintegracao
     */
    public function setId_saladeaulaintegracao($id_saladeaulaintegracao)
    {
        $this->id_saladeaulaintegracao = $id_saladeaulaintegracao;
        return $this;
    }

    /**
     * @return integer id_entidade
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * @param id_entidade
     */
    public function setId_entidade($id_entidade)
    {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    /**
     * @return boolean bl_ativa
     */
    public function getBl_ativa()
    {
        return $this->bl_ativa;
    }

    /**
     * @param bl_ativa
     */
    public function setBl_ativa($bl_ativa)
    {
        $this->bl_ativa = $bl_ativa;
        return $this;
    }

    /**
     * @return string st_saladeaula
     */
    public function getSt_saladeaula()
    {
        return $this->st_saladeaula;
    }

    /**
     * @param st_saladeaula
     */
    public function setSt_saladeaula($st_saladeaula)
    {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    /**
     * @return string st_situacao
     */
    public function getSt_situacao()
    {
        return $this->st_situacao;
    }

    /**
     * @param st_situacao
     */
    public function setSt_situacao($st_situacao)
    {
        $this->st_situacao = $st_situacao;
        return $this;
    }

    /**
     * @return string st_modalidadesaladeaula
     */
    public function getSt_modalidadesaladeaula()
    {
        return $this->st_modalidadesaladeaula;
    }

    /**
     * @param st_modalidadesaladeaula
     */
    public function setSt_modalidadesaladeaula($st_modalidadesaladeaula)
    {
        $this->st_modalidadesaladeaula = $st_modalidadesaladeaula;
        return $this;
    }

    /**
     * @return string st_sistema
     */
    public function getSt_sistema()
    {
        return $this->st_sistema;
    }

    /**
     * @param st_sistema
     */
    public function setSt_sistema($st_sistema)
    {
        $this->st_sistema = $st_sistema;
        return $this;
    }


    /**
     * @return integer id_disciplina
     */
    public function getId_disciplina()
    {
        return $this->id_disciplina;
    }

    /**
     * @param id_disciplina
     */
    public function setId_disciplina($id_disciplina)
    {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

}
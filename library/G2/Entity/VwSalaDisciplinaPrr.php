<?php

namespace G2\Entity;

/**
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table (name="vw_saladisciplinaprr")
 * @Entity
 * @EntityView
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class VwSalaDisciplinaPrr {

    /**
     * @Id
     * @var integer $id_saladeaula
     * @Column(name="id_saladeaula", type="integer", nullable=true, length=4)
     * @GeneratedValue(strategy="IDENTITY")
     */
    private $id_saladeaula;

    /**
     * @var string $st_saladeaula
     * @Column(name="st_saladeaula", type="string", nullable=true, length=255)
     */
    private $st_saladeaula;

    /**
     * @var integer $nu_maxalunos
     * @Column(name="nu_maxalunos", type="integer", nullable=true, length=4)
     */
    private $nu_maxalunos;

    /**
     * @var integer $id_projetopedagogico
     * @Column(name="id_projetopedagogico", type="integer", nullable=true, length=4)
     */
    private $id_projetopedagogico;

    /**
     * @var integer $id_trilha
     * @Column(name="id_trilha", type="integer", nullable=true, length=4)
     */
    private $id_trilha;

    /**
     * @var integer $id_matricula
     * @Column(name="id_matricula", type="integer", nullable=true, length=4)
     */
    private $id_matricula;

    /**
     * @var integer $id_disciplina
     * @Column(name="id_disciplina", type="integer", nullable=true, length=4)
     */
    private $id_disciplina;

    /**
     * @var string $st_disciplina
     * @Column(name="st_disciplina", type="string", nullable=true, length=255)
     */
    private $st_disciplina;

    /**
     * @var integer $id_matriculadisciplina
     * @Column(name="id_matriculadisciplina", type="integer", nullable=true, length=4)
     */
    private $id_matriculadisciplina;

    /**
     * @var integer $id_usuario
     * @Column(name="id_usuario", type="integer", nullable=true, length=4)
     */
    private $id_usuario;

    /**
     * @var integer $id_alocacao
     * @Column(name="id_alocacao", type="integer", nullable=true, length=4)
     */
    private $id_alocacao;

    /**
     * @var integer $id_entidade
     * @Column(name="id_entidade", type="integer", nullable=true, length=4)
     */
    private $id_entidade;

    /**
     * @var integer $nu_alocados
     * @Column(name="nu_alocados", type="integer", nullable=true, length=4)
     */
    private $nu_alocados;

    /**
     * @var date $dt_abertura
     * @Column(name="dt_abertura", type="date", nullable=true, length=3)
     */
    private $dt_abertura;

    /**
     * @var date $dt_encerramento
     * @Column(name="dt_encerramento", type="date", nullable=true, length=3)
     */
    private $dt_encerramento;

    /**
     * @var integer $id_categoriasala
     * @Column(name="id_categoriasala", type="integer", nullable=true, length=4)
     */
    private $id_categoriasala;

    /**
     * @var string $st_projetopedagogico
     * @Column(name="st_projetopedagogico", type="string", nullable=true, length=255)
     */
    private $st_projetopedagogico;

    /**
     * @var integer $id_produto
     * @Column(name="id_produto", type="integer", nullable=true, length=4)
     */
    private $id_produto;

    /**
     * @var float $nu_valor
     * @Column(name="nu_valor", type="float", nullable=true, length=9)
     */
    private $nu_valor;

    /**
     * @var integer $id_vendaproduto
     * @Column(name="id_vendaproduto", type="integer", nullable=true, length=9)
     */
    private $id_vendaproduto;

    /**
     * @var datetime2 $dt_entrega
     * @Column(name="dt_entrega", type="date", nullable=true, length=3)
     */
    private $dt_entrega;

    /**
     * @var integer $id_venda
     * @Column(name="id_venda", type="float", nullable=true, length=9)
     */
    private $id_venda;

    /**
     * @var integer $id_ocorrencia
     * @Column(name="id_ocorrencia", type="float", nullable=true, length=9)
     */
    private $id_ocorrencia;

    public function getId_saladeaula() {
        return $this->id_saladeaula;
    }

    public function setId_saladeaula($id_saladeaula) {
        $this->id_saladeaula = $id_saladeaula;
        return $this;
    }

    public function getSt_saladeaula() {
        return $this->st_saladeaula;
    }

    public function setSt_saladeaula($st_saladeaula) {
        $this->st_saladeaula = $st_saladeaula;
        return $this;
    }

    public function getNu_maxalunos() {
        return $this->nu_maxalunos;
    }

    public function setNu_maxalunos($nu_maxalunos) {
        $this->nu_maxalunos = $nu_maxalunos;
        return $this;
    }

    public function getId_projetopedagogico() {
        return $this->id_projetopedagogico;
    }

    public function setId_projetopedagogico($id_projetopedagogico) {
        $this->id_projetopedagogico = $id_projetopedagogico;
        return $this;
    }

    public function getId_trilha() {
        return $this->id_trilha;
    }

    public function setId_trilha($id_trilha) {
        $this->id_trilha = $id_trilha;
        return $this;
    }

    public function getId_matricula() {
        return $this->id_matricula;
    }

    public function setId_matricula($id_matricula) {
        $this->id_matricula = $id_matricula;
        return $this;
    }

    public function getId_disciplina() {
        return $this->id_disciplina;
    }

    public function setId_disciplina($id_disciplina) {
        $this->id_disciplina = $id_disciplina;
        return $this;
    }

    public function getSt_disciplina() {
        return $this->st_disciplina;
    }

    public function setSt_disciplina($st_disciplina) {
        $this->st_disciplina = $st_disciplina;
        return $this;
    }

    public function getId_matriculadisciplina() {
        return $this->id_matriculadisciplina;
    }

    public function setId_matriculadisciplina($id_matriculadisciplina) {
        $this->id_matriculadisciplina = $id_matriculadisciplina;
        return $this;
    }

    public function getId_usuario() {
        return $this->id_usuario;
    }

    public function setId_usuario($id_usuario) {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    public function getId_alocacao() {
        return $this->id_alocacao;
    }

    public function setId_alocacao($id_alocacao) {
        $this->id_alocacao = $id_alocacao;
        return $this;
    }

    public function getId_entidade() {
        return $this->id_entidade;
    }

    public function setId_entidade($id_entidade) {
        $this->id_entidade = $id_entidade;
        return $this;
    }

    public function getNu_alocados() {
        return $this->nu_alocados;
    }

    public function setNu_alocados($nu_alocados) {
        $this->nu_alocados = $nu_alocados;
        return $this;
    }

    public function getDt_abertura() {
        return $this->dt_abertura;
    }

    public function setDt_abertura(\DateTime $dt_abertura) {
        $this->dt_abertura = $dt_abertura;
        return $this;
    }

    public function getDt_encerramento() {
        return $this->dt_encerramento;
    }

    public function setDt_encerramento(\DateTime $dt_encerramento) {
        $this->dt_encerramento = $dt_encerramento;
        return $this;
    }

    public function getId_categoriasala() {
        return $this->id_categoriasala;
    }

    public function setId_categoriasala($id_categoriasala) {
        $this->id_categoriasala = $id_categoriasala;
        return $this;
    }

    public function getSt_projetopedagogico() {
        return $this->st_projetopedagogico;
    }

    public function setSt_projetopedagogico($st_projetopedagogico) {
        $this->st_projetopedagogico = $st_projetopedagogico;
        return $this;
    }

    public function getId_produto() {
        return $this->id_produto;
    }

    public function setId_produto($id_produto) {
        $this->id_produto = $id_produto;
        return $this;
    }

    public function getNu_valor() {
        return $this->nu_valor;
    }

    public function setNu_valor($nu_valor) {
        $this->nu_valor = $nu_valor;
        return $this;
    }

    public function getId_vendaproduto() {
        return $this->id_vendaproduto;
    }

    public function setId_vendaproduto($id_vendaproduto) {
        $this->id_vendaproduto = $id_vendaproduto;
        return $this;
    }

    public function getDt_entrega() {
        return $this->dt_entrega;
    }

    public function setDt_entrega(\DateTime $dt_entrega) {
        $this->dt_entrega = $dt_entrega;
        return $this;
    }

    public function getId_venda() {
        return $this->id_venda;
    }

    public function setId_venda($id_venda) {
        $this->id_venda = $id_venda;
        return $this;
    }

    public function getId_ocorrencia() {
        return $this->id_ocorrencia;
    }

    public function setId_ocorrencia($id_ocorrencia) {
        $this->id_ocorrencia = $id_ocorrencia;
        return $this;
    }

}
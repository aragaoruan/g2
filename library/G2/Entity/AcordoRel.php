<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/11/13
 * Time: 13:47
 */

namespace G2\Entity;

/**
 * @Entity
 * @HasLifecycleCallbacks
 * @ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 * @Table(name="tb_acordorel")
 */
class AcordoRel {

    /**
     * @var integer $id_acordo
     * @Column(name="IDACORDO", type="integer", nullable=false)
     * @Id
     */
    private $id_acordo;

    /**
     * @var integer $id_lan
     * @Column(name="IDLAN", type="integer", nullable=false)
     * @Id
     */
    private $id_lan;

    /**
     * @var integer $classificacao
     * @Column(name="CLASSIFICACAO", type="integer", nullable=false)
     */
    private $classificacao;

    /**
     * @var integer $codcoligada
     * @Column(name="CODCOLIGADA", type="integer", nullable=false)
     */
    private $codcoligada;

    /**
     * @param int $classificacao
     */
    public function setClassificacao($classificacao)
    {
        $this->classificacao = $classificacao;
    }

    /**
     * @return int
     */
    public function getClassificacao()
    {
        return $this->classificacao;
    }

    /**
     * @param mixed $codcoligada
     */
    public function setCodcoligada($codcoligada)
    {
        $this->codcoligada = $codcoligada;
    }

    /**
     * @return mixed
     */
    public function getCodcoligada()
    {
        return $this->codcoligada;
    }

    /**
     * @param int $id_acordo
     */
    public function setIdAcordo($id_acordo)
    {
        $this->id_acordo = $id_acordo;
    }

    /**
     * @return int
     */
    public function getIdAcordo()
    {
        return $this->id_acordo;
    }

    /**
     * @param int $id_lan
     */
    public function setIdLan($id_lan)
    {
        $this->id_lan = $id_lan;
    }

    /**
     * @return int
     */
    public function getIdLan()
    {
        return $this->id_lan;
    }


} 
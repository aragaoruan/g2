<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30/03/2015
 * Time: 11:49
 */

namespace G2\Negocio;

/**
 * Classe de negócio para Item de Material Presencial
 * @author Helder Silva<helder.silva@unyleya.com.br>;
 */

class ItemDeMaterialPresencial extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function retornaTurmaEntidade($params)
    {

        return $this->findBy('\G2\Entity\Turma', array('id_entidadecadastro' => $params['id'], 'bl_ativo' => true));

    }

    public function retornaDisciplinaTurma($params)
    {
        return $this->em->getRepository('\G2\Entity\VwTurmaDisciplina')->retornaDisciplinasInTurmas($params);
    }

    public function retornaProfessorDisciplina($params)
    {
        return $this->findBy('\G2\Entity\VwUsuarioPerfilEntidadeReferencia', array('id_disciplina' => $params['id_disciplina'], 'id_perfilpedagogico' => 1));
    }

    public function retornaEncontroDisciplina($params)
    {
        return $this->findBy('\G2\Entity\ItemGradeHoraria', array('id_disciplina' => $params['id_disciplina'], 'id_unidade' => $params['id_entidade']));
    }

    public function retornaTipoDeMaterial($params)
    {
        return $this->findBy('\G2\Entity\TipodeMaterial', array('id_entidade' => $params['id'], 'id_situacao' => \G2\Constante\Situacao::TB_TIPODEMATERIAL_ATIVO));
    }

    public function salvaMaterial($params)
    {

        try {
            $this->beginTransaction();

//            \Zend_Debug::dump($params);exit;

            $objetoMaterial = new \G2\Entity\ItemDeMaterial();
            if (array_key_exists('id_material', $params) && !empty($params['id_material'])) {
                $objetoMaterial = $this->find('\G2\Entity\ItemDeMaterial', $params['id_material']);
            }

            $objetoMaterial->setId_entidade($this->getReference('\G2\Entity\Entidade', $params['id_entidade']));
            $objetoMaterial->setNu_qtdepaginas($params['nu_paginas']);
            $objetoMaterial->setNu_qtdestoque($params['nu_qtdestoque']);
            $objetoMaterial->setNu_encontro($params['nu_encontro']);
            $objetoMaterial->setDt_cadastro(new \DateTime());
            $objetoMaterial->setNu_valor($params['nu_valor']);
            $objetoMaterial->setSt_itemdematerial($params['st_nomematerial']);
            $objetoMaterial->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objetoMaterial->setId_professor($this->getReference('\G2\Entity\Usuario', $params['id_professor']));
            $objetoMaterial->setId_tipodematerial($this->getReference('\G2\Entity\TipodeMaterial', $params['id_tipomaterial']));
            $objetoMaterial->setId_situacao($this->getReference('\G2\Entity\Situacao', $params['id_situacao']));
            $objetoMaterial->setBl_portal($params['bl_portal']);
            $objetoMaterial->setBl_ativo(true);
            $resultado = $this->save($objetoMaterial);

            $resultadoTurma = array();

            $todosItemTurma = $this->findBy('\G2\Entity\ItemDeMaterialTurma', array('id_material' => $resultado->getId_itemdematerial()));

            foreach ($todosItemTurma as $val) {
                $val->setBl_ativo(false);
                $this->save($val);
            }

            foreach ($params['id_turma'] as $key => $value) {
                $objetoTurmaMaterial = $this->findOneBy('\G2\Entity\ItemDeMaterialTurma', array('id_turma' => $value, 'id_material' => $resultado->getId_itemdematerial()));

                if (!$objetoTurmaMaterial)
                    $objetoTurmaMaterial = new \G2\Entity\ItemDeMaterialTurma();

                $objetoTurmaMaterial->setId_material($this->getReference('\G2\Entity\ItemDeMaterial', $resultado->getId_itemdematerial()));
                $objetoTurmaMaterial->setId_turma($this->getReference('\G2\Entity\Turma', $value));
                $objetoTurmaMaterial->setBl_ativo(true);
                $x = $this->save($objetoTurmaMaterial);
                array_push($resultadoTurma, $x->getId_turma()->getId_turma());
            }

            $objetoDisciplinaMaterial = $this->findOneBy('\G2\Entity\ItemDeMaterialDisciplina', array('id_disciplina' => $params['id_disciplina'], 'id_itemdematerial' => $resultado->getId_itemdematerial()));
            if (!$objetoDisciplinaMaterial)
                $objetoDisciplinaMaterial = new \G2\Entity\ItemDeMaterialDisciplina();

            $objetoDisciplinaMaterial->setId_itemdematerial($resultado->getId_itemdematerial());
            $objetoDisciplinaMaterial->setId_disciplina($this->getReference('\G2\Entity\Disciplina', $params['id_disciplina']));
            $resultadoDisciplina = $this->save($objetoDisciplinaMaterial);
            if ($resultado)
                $resultadoArquivo = $this->uploadArquivoMaterial($resultado->getId_itemdematerial());

            if (!$resultadoArquivo) {
                $this->rollback();
                return array('retorno' => false, 'message' => array('Erro ao salvar arquivo de material!'));
            }
            $retorno['id_itemdematerial'] = $resultado;
            $retorno['id_turma'] = $resultadoTurma;
            $retorno['id_disciplina'] = $resultadoDisciplina->getId_itemdematerialdisciplina();
            $this->commit();

            return $retorno;
        } catch (\Exception $e) {
            $this->rollback();
            return $e->getMessage();
        }
    }

    public function uploadArquivoMaterial($id_material)
    {
        try {
            if (isset($_FILES['attachment'])) {
                $arquivo = $_FILES['attachment'];
                $nome = $arquivo['name'];
                $tipo = pathinfo($nome, PATHINFO_EXTENSION);
                $novo_nome = $id_material . '.' . $tipo;
                $retorno = null;

                $objetoMaterial = $this->find('\G2\Entity\ItemDeMaterial', $id_material);
                if ($objetoMaterial->getId_upload() == NULL) {
                    $objetoUpload = new \G2\Entity\Upload();
                    $objetoUpload->setSt_upload($novo_nome);
                    $resultadoUpload = $this->save($objetoUpload);
                    $objetoMaterial->setId_upload($resultadoUpload);
                    $this->save($objetoMaterial);
                }
                $uploadMaterial = realpath(APPLICATION_PATH . "/../public/upload/materiaispresenciais/");

                if (move_uploaded_file($arquivo['tmp_name'], $uploadMaterial . '/' . $novo_nome)) {
                    chmod($uploadMaterial . '/' . $novo_nome, 0777);
                    return true;
                } else {
                    return false;
                }

            }
            return true;
        } catch (\Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function retornaPesquisaItemDeMaterialPresencial($params)
    {
        try {
            $where = array_filter($params);
            $resultado = $this->findBy('\G2\Entity\VwMaterialTurmaDisciplina', $where, array('id_itemdematerial' => 'ASC', 'id_turma' => 'ASC'));
            return $resultado;
        } catch (\Exception $e) {
            echo $e;
            return $e->getMessage();
        }
    }

    public function retornaNomeUpload($params)
    {
        try {
            $resultado = $this->find('\G2\Entity\Upload', array('id_upload' => $params));
            return $resultado->getSt_upload();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deleta($id)
    {
        try {
            $objetoMaterial = $this->find('\G2\Entity\ItemDeMaterial', $id);
            $objetoMaterial->setBl_ativo(false);
            return $this->save($objetoMaterial);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function retornaDadosCodigoBarras($params)
    {
        return $this->find('\G2\Entity\ItemDeMaterial', $params['id']);
    }

    public function retornaMatriculas($params)
    {
        return $this->findBy('\G2\Entity\VwMatricula', array('id_usuario' => $params, 'id_entidadematricula' => $this->sessao->id_entidade));
    }


    public function retornaItemMaterialEntrega($params)
    {
        return $this->em->getRepository('\G2\Entity\ItemDeMaterial')->retornaItemUsuario($params);

    }

    public function executaEntrega($params)
    {
        try {
            $this->beginTransaction();
            $objetoEntrega = new \G2\Entity\EntregaMaterial();


            if (array_key_exists('id_entregamaterial', $params) && $params['id_entregamaterial']) {
                $objetoEntrega = $this->find('\G2\Entity\EntregaMaterial', $params['id_entregamaterial']);
            }
            $objetoEntrega->setBl_ativo(true);
            $objetoEntrega->setId_situacao(\G2\Constante\Situacao::TB_ENTREGAMATERIAL_ENTREGUE);
            $objetoEntrega->setId_usuariocadastro($this->sessao->id_usuario);
            $objetoEntrega->setId_itemdematerial($params['id_itemdematerial']);
            $objetoEntrega->setDt_entrega(new \DateTime());
            $objetoEntrega->setId_matricula($params['id_matricula']);
            $objetoEntrega->setId_matriculadisciplina($params['id_matriculadisciplina']);
            $result = $this->save($objetoEntrega);
            if ($result) {

                $objetoEstoque = $this->find('\G2\Entity\ItemDeMaterial', $result->getId_itemdematerial());
                $objetoEstoque->setNu_qtdestoque($objetoEstoque->getNu_qtdestoque() - 1);
                $this->save($objetoEstoque);

                $objetoTramite = new \G2\Entity\Tramite();
                $objetoTramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', 4));
                $objetoTramite->setId_usuario($this->getReference('\G2\Entity\Usuario',$this->sessao->id_usuario));
                $objetoTramite->setId_entidade($this->getReference('\G2\Entity\Entidade',$this->sessao->id_entidade));
                $objetoTramite->setSt_tramite('Entrega de Material Presencial - '.$this->getReference('\G2\Entity\ItemDeMaterial',$result->getId_itemdematerial())->getSt_itemdematerial());
                $objetoTramite->setDt_cadastro(new \DateTime());
                $objetoTramite->setBl_visivel(true);
                $resultTramite = $this->save($objetoTramite);

                if($resultTramite){
                    $objetoTramiteEntregaMaterial = new \G2\Entity\TramiteEntregaMaterial();
                    $objetoTramiteEntregaMaterial->setId_tramite($resultTramite);
                    $objetoTramiteEntregaMaterial->setId_entregamaterial($result);
                    $resultTramiteEntregaMaterial = $this->save($objetoTramiteEntregaMaterial);

                }

                $this->commit();
                return $result;
            }
        } catch (\Exception $e) {
            $this->rollback();
            return $e->getMessage();
        }
    }

    public function devolverEntrega($params)
    {
        try{
            $this->beginTransaction();

            $objetoDevolucao = $this->find('\G2\Entity\EntregaMaterial', $params['id_entregamaterial']);
            $objetoDevolucao->setDt_devolucao(new \DateTime());
            $objetoDevolucao->setId_situacao(\G2\Constante\Situacao::TB_ENTREGAMATERIAL_DEVOLVIDO);
            $result =  $this->save($objetoDevolucao);
            if($result){

                $objetoItem = $this->getReference('\G2\Entity\ItemDeMaterial',$result->getId_itemdematerial());

                $objetoEstoque = $this->find('\G2\Entity\ItemDeMaterial', $objetoItem->getId_itemdematerial());
                $objetoEstoque ->setNu_qtdestoque($objetoItem->getNu_qtdestoque() + 1);
                $this->save($objetoEstoque);

                $objetoTramite = new \G2\Entity\Tramite();
                $objetoTramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', 4));
                $objetoTramite->setId_usuario($this->getReference('\G2\Entity\Usuario',$this->sessao->id_usuario));
                $objetoTramite->setId_entidade($this->getReference('\G2\Entity\Entidade',$this->sessao->id_entidade));
                $objetoTramite->setSt_tramite('Devolução de Material Presencial - '.$this->getReference('\G2\Entity\ItemDeMaterial',$result->getId_itemdematerial())->getSt_itemdematerial());
                $objetoTramite->setDt_cadastro(new \DateTime());
                $objetoTramite->setBl_visivel(true);
                $resultTramite = $this->save($objetoTramite);

                if($resultTramite){
                    $objetoTramiteEntregaMaterial = new \G2\Entity\TramiteEntregaMaterial();
                    $objetoTramiteEntregaMaterial->setId_tramite($resultTramite);
                    $objetoTramiteEntregaMaterial->setId_entregamaterial($result);
                    $resultTramiteEntregaMaterial = $this->save($objetoTramiteEntregaMaterial);
                }
            }
            $this->commit();
            return $result;
        }catch (\Exception $e){
            $this->rollback();
            \Zend_Debug::dump($e->getMessage());exit;
            return $e->getMessage();
        }

    }

    public function executaEntregaRapida($params)
    {
        $objetoItemUsuario = $this->em->getRepository('\G2\Entity\ItemDeMaterial')->retornaItemUsuario($params);

        if (count($objetoItemUsuario) == 1) {
            if ($objetoItemUsuario[0]['id_situacao'] == \G2\Constante\Situacao::TB_ENTREGAMATERIAL_ENTREGUE) {
                return 0;
            } else {
                try {
                    $this->beginTransaction();
                    $objetoEntrega = new \G2\Entity\EntregaMaterial();

                    if (array_key_exists('id_entregamaterial', $objetoItemUsuario[0]) && $objetoItemUsuario[0]['id_entregamaterial']) {
                        $objetoEntrega = $this->find('\G2\Entity\EntregaMaterial', $objetoItemUsuario[0]['id_entregamaterial']);
                    }
                    $objetoEntrega->setBl_ativo(true);
                    $objetoEntrega->setId_situacao(\G2\Constante\Situacao::TB_ENTREGAMATERIAL_ENTREGUE);
                    $objetoEntrega->setId_usuariocadastro($this->sessao->id_usuario);
                    $objetoEntrega->setId_itemdematerial($objetoItemUsuario[0]['id_itemdematerial']);
                    $objetoEntrega->setDt_entrega(new \DateTime());
                    $objetoEntrega->setId_matricula($objetoItemUsuario[0]['id_matricula']);
                    $objetoEntrega->setId_matriculadisciplina($objetoItemUsuario[0]['id_matriculadisciplina']);
                    $result = $this->save($objetoEntrega);
                    if ($result) {

                        $objetoEstoque = $this->find('\G2\Entity\ItemDeMaterial', $objetoItemUsuario[0]['id_itemdematerial']);
                        $objetoEstoque ->setNu_qtdestoque($objetoItemUsuario[0]['nu_qtdestoque'] - 1);
                        $this->save($objetoEstoque);

                        $objetoTramite = new \G2\Entity\Tramite();
                        $objetoTramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', 4));
                        $objetoTramite->setId_usuario($this->getReference('\G2\Entity\Usuario',$this->sessao->id_usuario));
                        $objetoTramite->setId_entidade($this->getReference('\G2\Entity\Entidade',$this->sessao->id_entidade));
                        $objetoTramite->setSt_tramite('Entrega de Material Presencial - '.$this->getReference('\G2\Entity\ItemDeMaterial',$result->getId_itemdematerial())->getSt_itemdematerial());
                        $objetoTramite->setDt_cadastro(new \DateTime());
                        $objetoTramite->setBl_visivel(true);
                        $resultTramite = $this->save($objetoTramite);

                        if($resultTramite){
                            $objetoTramiteEntregaMaterial = new \G2\Entity\TramiteEntregaMaterial();
                            $objetoTramiteEntregaMaterial->setId_tramite($resultTramite);
                            $objetoTramiteEntregaMaterial->setId_entregamaterial($result);
                            $resulttTramiteEntregaMaterial = $this->save($objetoTramiteEntregaMaterial);

                        }
                        $this->commit();
                        return 1;
                    }
                } catch (\Exception $e) {
                    $this->rollback();
                    return $e->getMessage();
                }
            }
        } elseif (count($objetoItemUsuario) > 1) {
            return $objetoItemUsuario;
        } else {
            return 2;
        }
    }

    public function  retornaTramite($params){
        return $this->em->getRepository('\G2\Entity\ItemDeMaterial')->retornaTramite($params);
    }

}
<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;

/**
 * Classe de Negocio para Prevenda
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-06
 */
class Prevenda extends Negocio
{

    private $repositoryName = 'G2\Entity\Prevenda';
    private $_preVendaProdutoNegocio;

    public function __construct ()
    {
        parent::__construct();
        $this->_preVendaProdutoNegocio = new PrevendaProduto();
    }

    protected function prepareParams(array $params = array())
    {

        $arrParams = array();
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                if (!empty($value) && $value != 'true' && $value != 'undefined') {
                    if (substr($key, 0, 3) == 'dt_') {
                        $value = $this->converterData($value);
                    }
                    $arrParams[$key] = $value;
                }
            }
        }

        $arrParams['id_entidade'] = !empty($arrParams['id_entidade']) ?
            $arrParams['id_entidade'] : $this->sessao->id_entidade;

        return $arrParams;
    }

    /**
     * Find Pre-Venda from array params
     * @param array $params Array com parâmetros para o filtro
     * @return object Doctrine Object
     */
    public function findContatosByFilter(array $params = array())
    {

        $return = $this->_preVendaProdutoNegocio->getPrevendaProdutoJoinPrevenda($this->prepareParams($params));

        $arrReturn = array();
        if (!empty($return)) {
            foreach ($return as $row) {
                $dt_cadastro = date_create($row->getId_prevenda()->getDt_cadastro());

                $nu_telefoneresidencial = '(' . $row->getId_prevenda()->getNu_dddresidencial() . ') '
                    . $row->getId_prevenda()->getNu_telefoneresidencial();

                $nu_celularresidencial = '(' . $row->getId_prevenda()->getNu_dddcelular() . ') '
                    . $row->getId_prevenda()->getNu_telefonecelular();

                $arrReturn[] = array(
                    'id' => $row->getId_prevendaproduto(),
                    'id_prevendaproduto' => $row->getId_prevendaproduto(),
                    'id_produto' => $row->getId_produto()->getId_produto(),
                    'st_produto' => $row->getId_produto()->getSt_produto(),
                    'id_prevenda' => $row->getId_prevenda()->getId_prevenda(),
                    'st_nomecompleto' => $row->getId_prevenda()->getSt_nomecompleto(),
                    'st_email' => $row->getId_prevenda()->getSt_email(),
                    'nu_telefoneresidencial' => $nu_telefoneresidencial,
                    'nu_telefonecelular' => $nu_celularresidencial,
                    'dt_cadastro' => $dt_cadastro->format('d/m/Y'),
                    'id_situacao' => $row->getId_prevenda()->getId_situacao()->getId_situacao(),
                    'st_situacao' => $row->getId_prevenda()->getId_situacao()->getSt_situacao()
                );
            }
        }

        return $arrReturn;
    }


    /**
     * Find Pre-Venda from array params
     * @param array $params Array com parâmetros para o filtro
     * @return object Doctrine Object
     */
    public function returnPreVendas(array $params = array())
    {

        $return = $this->_preVendaProdutoNegocio->getPreVendas($this->prepareParams($params));

        $arrReturn = array();
        if (!empty($return)) {
            foreach ($return as $row) {

                $dt_cadastro = $this->converterData($row['dt_cadastro'], 'datetime');

                $nu_telefoneresidencial = '(' . $row['nu_dddresidencial'] . ') '
                    . $row['nu_telefoneresidencial'];

                $nu_celularresidencial = '(' . $row['nu_dddcelular'] . ') '
                    . $row['nu_telefonecelular'];

                $dt_ultimainteracao = !empty($row['dt_ultimainteracao']) ?
                    date_create($row['dt_ultimainteracao']) : null;

                $arrReturn[] = array(
                    'id' => $row['id_prevendaproduto'],
                    'id_prevendaproduto' => $row['id_prevendaproduto'],
                    'id_produto' => $row['id_produto'],
                    'st_produto' => $row['st_produto'],
                    'id_prevenda' => $row['id_prevenda'],
                    'st_nomecompleto' => $row['st_nomecompleto'],
                    'st_email' => $row['st_email'],
                    'nu_telefoneresidencial' => $nu_telefoneresidencial,
                    'nu_telefonecelular' => $nu_celularresidencial,
                    'dt_cadastro' => $dt_cadastro->format('d/m/Y'),
                    'id_situacao' => $row['id_situacao'],
                    'st_situacao' => $row['st_situacao'],
                    'dt_ultimainteracao' => $dt_ultimainteracao ? $dt_ultimainteracao->format('d/m/Y') : ' - ',
                    'st_atendente' => $row['st_nomecompletoatendente'],
                );
            }
        }

        return $arrReturn;
    }

    /**
     * Salva os dados de uma prevenda informando o id_prevendaproduto
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function salvarPrevendaProduto(array $params)
    {
        try {
            $tramite = new Tramite();

            /** @var \G2\Entity\Situacao $situacao */
            $situacao = $this->getReference('\G2\Entity\Situacao', $params['id_situacao']);

            /** @var \G2\Entity\Usuario $usuario */
            $atendente = $this->getReference(
                'G2\Entity\Usuario',
                !empty($params['id_atendente']) ? $params['id_atendente'] : $this->sessao->id_usuario
            );
            /** @var \G2\Entity\PrevendaProduto $resultPrevendaProduto */
            $resultPrevendaProduto = $this->_preVendaProdutoNegocio->find(
                $this->_preVendaProdutoNegocio->repositoryName, $params['id_prevendaproduto']
            );

            if (!$resultPrevendaProduto) {
                throw new \Exception("Pre venda não encontrada");
            }

            /** @var \G2\Entity\Prevenda $resultPrevenda */
            $resultPrevenda = $this->find(
                $this->repositoryName, $resultPrevendaProduto->getId_prevenda()->getId_prevenda()
            );

            //Seta os valores de prevenda
            $resultPrevenda->setId_situacao($situacao);

            switch ($situacao->getId_situacao()) {
                case \G2\Constante\Situacao::TB_PREVENDA_DESCARTADA:
                    /** @var \G2\Entity\Usuario $usuarioDescarte */
                    $usuarioDescarte = $this->getReference(
                        'G2\Entity\Usuario', $this->sessao->id_usuario
                    );

                    $resultPrevenda->setId_usuariodescarte($usuarioDescarte);
                    $resultPrevenda->setDt_descarte(new \DateTime());

                    $mensagemTramite = 'Pré Venda descartada por: ' . $usuarioDescarte->getSt_nomecompleto() .
                        ' - motivo: ' . $params['st_descarte'];

                    break;
                case \G2\Constante\Situacao::TB_PREVENDA_DISTRIBUIDA:
                    $resultPrevenda
                        ->setId_usuarioatendimento($atendente)
                        ->setDt_atendimento(new \DateTime());

                    $mensagemTramite = 'Pré Venda: ' .
                        $resultPrevendaProduto->getId_prevenda()->getId_prevenda() .
                        ' - ' . $situacao->getSt_situacao() . ' - ' .
                        ' Atendente: ' . $atendente->getSt_nomecompleto();

                    break;
                default:
                    $mensagemTramite = $params['st_descarte'];

                    break;
            }

            $return = $this->save($resultPrevenda);
            if (empty($return)) {
                return new \Ead1_Mensageiro(
                    array('id_prevenda' => $resultPrevenda->getId_prevenda(),
                        'st_mensagem' => 'Situação da Pré Venda não pode ser alterada para: ' .
                            $situacao->getSt_situacao()
                    ), \Ead1_IMensageiro::ERRO
                );
            }

            $tramite->salvarTramite(
                array('st_tramite' => $mensagemTramite,
                    'id_entidade' => $this->sessao->id_entidade,
                    'id_usuario' => $this->sessao->id_usuario,
                    'id_campo' => $resultPrevendaProduto->getId_prevenda()->getId_prevenda(),
                    'id_categoriatramite' => 7
                )
            );

            return new \Ead1_Mensageiro(
                array('id_prevenda' => $resultPrevenda->getId_prevenda(),
                    'st_mensagem' => 'Situação da Pré Venda alterada para: ' .
                        $situacao->getSt_situacao()
                ), \Ead1_IMensageiro::SUCESSO
            );

        } catch (\Exception $error) {
            return new \Ead1_Mensageiro (
                array('id_prevenda' => $resultPrevenda->getId_prevenda() ? $resultPrevenda->getId_prevenda() : null,
                    'st_mensagem' => 'Situação da Pré Venda não pode ser alterada para: ' .
                        $situacao->getSt_situacao(),
                    'error' => $error->getMessage()
                ), \Ead1_IMensageiro::ERRO
            );
        }
    }

    /**
     * Salva Mensagens Lidas By array of IdPrevendaProduto
     * @param array $params
     * @param integer $idSituacao Id Situação to SET 209 = situação distribuida.
     * @param integer $idAtendente Id do atendente a ser distribuido a pré,
     *          caso não informado envia para o usuário logado
     * @return array Mixed array
     */
    public function salvarByIdPrevendaProduto(
        array $params, $idSituacao = \G2\Constante\Situacao::TB_PREVENDA_DISTRIBUIDA,
        $idAtendente = null, $st_descarte = null
    )
    {
        try {

            $arrReturnFail = array();
            if (empty($params)) {
                throw new \Exception('Nenhum parâmetro foi informado!');
            }

            /** @var \G2\Entity\Situacao $situacao */
            $situacao = $this->getReference('\G2\Entity\Situacao', $idSituacao);

            //percorre o array dos parametros
            foreach ($params as $id) {
                /** @var \Ead1_Mensageiro $return */
                $return = $this->salvarPrevendaProduto(
                    array(
                        'id_situacao' => $idSituacao,
                        'id_atendente' => $idAtendente,
                        'id_prevendaproduto' => $id,
                        'st_descarte' => $st_descarte
                    )
                );

                //busca a prevendaproduto
                if ($return->getTipo() == \Ead1_IMensageiro::ERRO) {
                    $result = $return->getFirstMensagem();
                    $arrReturnFail[] = $result['id_prevenda'];
                }
            }

            if ($arrReturnFail) {
                return array('type' => 'error',
                    'title' => 'Erro ao Atualizar!',
                    'text' => 'Falha ao atualizar o(s) registro(s), ' . implode(', ', $arrReturnFail) . '.'
                );
            }

            return array('type' => 'success',
                'title' => 'Registro Atualizado!',
                'text' => 'Pré-matrícula(s) ' . $situacao->getSt_situacao() . '(s) com sucesso! '
            );

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new Zend_Exception($e->getMessage());
        }
    }

    public function retornarTramitesPreVenda($idPrevenda)
    {
        if (empty($idPrevenda)) {
            return array();
        }

        $return = $this->_preVendaProdutoNegocio->getInteracoesPreVendas($idPrevenda);

        if ($return) {
            foreach ($return as &$row) {
                $dt_cadastro = date_create($row['dt_cadastro']);
                if ($dt_cadastro) {
                    $row['dt_cadastro'] = $dt_cadastro->format('d/m/Y H:i:s');
                }
            }
        }

        return $return;
    }
}

<?php

namespace G2\Negocio;

use \G2\Constante\Situacao as SituacaoConstantes;
use G2\Utils\Helper;

/**
 * Class AtividadeComplementar
 * @package G2\Negocio
 */
class AtividadeComplementar extends Negocio
{


    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Método para salvar e/ou editar o tipo de atividade complementar
     * @param array $dados
     * @return mixed
     * @throws \Exception
     */
    public function salvarTipoAtividade(array $dados)
    {
        try {
            $tipoAtividade = new \G2\Entity\TipoAtividadeComplementar();

            //verifica se foi passado o id do tipo de atividade
            if (isset($dados['id_tipoatividadecomplementar']) && !empty($dados['id_tipoatividadecomplementar'])) {
                $tipoAtividade = $this
                    ->find('\G2\Entity\TipoAtividadeComplementar', $dados['id_tipoatividadecomplementar']);

                if (!$tipoAtividade) {
                    throw new \Exception("Tipo de Atividade Complementar não encontrado.");
                }
            }

            //verifica se não tem o id da atividade complementar
            if (!$tipoAtividade->getId_tipoatividadecomplementar()) {
                $tipoAtividade->setBl_ativo(true)
                    ->setDt_cadastro(new \DateTime())
                    ->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario))
                    ->setId_entidadecadastro($this->getReference('\G2\Entity\Entidade', $this->getId_entidade()));
            }

            $tipoAtividade->setSt_tipoatividadecomplementar($dados['st_tipoatividadecomplementar']);

            return $this->save($tipoAtividade);

        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Calcula a quantidade de horas
     * @param $atividades
     * @param $agruparPor
     * @param $valorReferencia
     * @return string
     */
    public function calculaTotalHoras($atividades, $agruparPor, $valorReferencia)
    {
        if ($atividades) {
            $horas = 0;
            foreach ($atividades as $atividade) {
                if ($atividade["nu_horasconvalidada"] && $atividade[$agruparPor] == $valorReferencia) {
                    $horas += (float)$atividade["nu_horasconvalidada"];
                }
            }

            return $horas;
        }
    }


    /**
     * Salva os dados da atividade complementar
     * @param array $dados
     * @param array|null $anexo
     * @return \Doctrine\ORM\NoResultException|\Exception|\G2\Entity\AtividadeComplementar|mixed|null|object
     * @throws \Exception
     */
    public function salvarAtividade(array $dados, array $anexo = null)
    {
        try {
            $this->beginTransaction();
            $this->validarDadosAtividade($dados, $anexo);

            $atividadeEntity = new \G2\Entity\AtividadeComplementar();

            if (isset($dados['id_atividadecomplementar']) && !empty($dados['id_atividadecomplementar'])) {
                $atividadeEntity = $this
                    ->find(\G2\Entity\AtividadeComplementar::class, $dados['id_atividadecomplementar']);

                if (!$atividadeEntity) {
                    throw new \Exception("Atividade Complementar não encontrada.");
                }
            }

            //faz o upload do arquivo
            $resultUpload = $this->uploadComprovante($anexo, $dados['id_matricula']);

            //seta o id da entidade que está na sessão
            $id_entidade = $this->getId_entidade();

            //mas se achar a matrícula, seta a entidade da matrícula
            $matriculaEN = (new Matricula())->find(\G2\Entity\Matricula::class, $dados['id_matricula']);
            if ($matriculaEN) {
                $id_entidade = $matriculaEN->getId_entidadematricula();
            }

            //verifica se o id da tabela não existe e seta os valores padrões para o insert do registro
            if (!$atividadeEntity->getId_atividadecomplementar()) {
                $enviadoParaAnalise = SituacaoConstantes::TB_ATIVIDADECOMPLEMENTAR_ENVIADO_PARA_ANALISE;

                $id_entidadecadastro = empty($dados['id_entidadecadastro']) ? $this->getId_entidade() :
                    $dados['id_entidadecadastro'];

                $atividadeEntity->setBl_ativo(true)
                    ->setDt_cadastro(new \DateTime())
                    ->setId_situacao($this->getReference("\G2\Entity\Situacao", $enviadoParaAnalise))
                    ->setId_entidadecadastro($this->getReference("\G2\Entity\Entidade", $id_entidade))
                    ->setId_matricula($this->getReference("\G2\Entity\Matricula", $dados['id_matricula']));
            }


            //seta os valores que são dinamicos e passiveis de alteração por update
            $atividadeEntity->setSt_tituloatividade($dados['st_tituloatividade'])
                ->setSt_resumoatividade(empty($dados['st_resumoatividade']) ? null : $dados['st_resumoatividade'])
                ->setId_tipoatividade($this->getReference("\G2\Entity\TipoAtividadeComplementar", $dados['id_tipoatividade']))
                ->setId_upload($this->getReference("\G2\Entity\Upload", $resultUpload['id_upload']))
                ->setSt_caminhoarquivo($resultUpload['caminho_upload'] . "/" . $resultUpload['st_upload']);

            $atividadeEntity = $this->save($atividadeEntity);
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }

        return $atividadeEntity;
    }

    /**
     * Verifica se os principais parametros foram passados
     * @param array $dados
     * @param array $anexo
     * @return bool
     * @throws \Exception
     */
    private function validarDadosAtividade(array $dados, array $anexo)
    {
        if (!isset($dados['id_atividadecomplementar']) && !$anexo) {
            throw new \Exception("Não é possível salvar Atividade sem anexar o comprovante.");
        }

        if (!isset($dados['st_tituloatividade']) || empty($dados['st_tituloatividade'])) {
            throw new \Exception("Campo Título da Atividade é obrigatório.");
        }

        if (!isset($dados['id_tipoatividade']) || empty($dados['id_tipoatividade'])) {
            throw new \Exception("Campo Tipo de Atividade Complementar é obrigatório.");
        }

        if (!isset($dados['id_matricula']) || empty($dados['id_matricula'])) {
            throw new \Exception("Não é possível salvar a atividade sem informar a matrícula.");
        }

        return true;

    }


    /**
     * Prepara os dados e faz o upload do arquivo
     * @param array $anexo
     * @param $idMatricula
     * @return mixed
     * @throws \Exception
     */
    private function uploadComprovante(array $anexo, $idMatricula)
    {
        try {
            $uploadNegocio = new Upload();
            $permitidos = ['pdf', 'doc', 'docx', 'jpg', 'png', 'jpeg'];
            $destination = "upload/atividade-complementar/" . $idMatricula;
            $fileName = sha1($idMatricula . date("YmdHis") . uniqid());
            $fileSize = 10485760;//10mb

            if ($anexo["size"] > $fileSize) {
                throw new \Exception("O tamanho do anexo excede os 10mb permitido pelo sistema");
            }

            $resultUpload = $uploadNegocio->upload($anexo, $destination, true, $permitidos, $fileName, $fileSize);

            if (!($resultUpload instanceof \Ead1_Mensageiro) ||
                $resultUpload->getType() != \Ead1_IMensageiro::TYPE_SUCESSO
            ) {
                throw new \Exception("Erro ao tentar fazer upload do arquivo. " . $resultUpload->getText());
            }

            $upload = $resultUpload->getFirstMensagem();
            $upload["caminho_upload"] = $destination;

        } catch (\Exception $e) {
            throw  $e;
        }
        return $upload;
    }


    /**
     * Faz a exclusão lógica da atividade
     * @param $idAtividade
     * @return \Doctrine\ORM\NoResultException|\Exception|mixed|null|object
     * @throws \Exception
     */
    public function removerAtividade($idAtividade)
    {
        try {
            if (!$idAtividade) {
                throw new \Exception("Não é possível remover o registro sem informar o ID");
            }

            $atividadeEntity = $this->find(\G2\Entity\AtividadeComplementar::class, $idAtividade);

            if (!$atividadeEntity) {
                throw new \Exception("Atividade não encontrada.");
            }

            if ($atividadeEntity->getId_situacao()
                && $atividadeEntity->getId_situacao()->getId_situacao() != SituacaoConstantes::TB_ATIVIDADECOMPLEMENTAR_ENVIADO_PARA_ANALISE
            ) {
                throw new \Exception("A situação atual da Atividade não permite a sua exclusão.");
            }


            $atividadeEntity->setBl_ativo(false);
            $atividadeEntity = $this->save($atividadeEntity);

        } catch (\Exception $e) {
            throw $e;
        }
        return $atividadeEntity;

    }

    /**
     * @description Validar se o aluno possui o total de horas de atividades complementares que o curso solicita
     * @param int|\G2\Entity\Matricula|\G2\Entity\VwMatricula|\MatriculaTO|\VwMatriculaTO $matricula
     * @return bool
     * @throws \Exception
     */
    public function validarTotalHorasAluno($matricula)
    {
        /** @var \G2\Entity\ProjetoPedagogico|null $projetopedagogico */
        $projetopedagogico = null;

        if (
            !($matricula instanceof \G2\Entity\Matricula)
            && !($matricula instanceof \G2\Entity\VwMatricula)
            && !($matricula instanceof \MatriculaTO)
            && !($matricula instanceof \VwMatriculaTO)
        ) {
            $matricula = $this->find('\G2\Entity\Matricula', $matricula);
        }

        // Validar matrícula
        if (!($matricula instanceof \G2\Entity\Matricula)) {
            throw new \Exception('Matrícula não encontrada.');
        }

        // Buscar projeto
        $projetopedagogico = ($matricula->getId_projetopedagogico() instanceof \G2\Entity\ProjetoPedagogico)
            ? $matricula->getId_projetopedagogico()
            : $this->find('\G2\Entity\ProjetoPedagogico', $matricula->getId_projetopedagogico());

        // Validar projeto
        if (!($projetopedagogico instanceof \G2\Entity\ProjetoPedagogico)) {
            throw new \Exception('Projeto pedagógico não encontrado.');
        }

        // Total de horas necessárias
        $nu_horasnecessarias = (float)($projetopedagogico->getNu_horasatividades() ?: 0);

        // Buscar total de horas do aluno
        $nu_horasaluno = (float)$this->getRepository('\G2\Entity\AtividadeComplementar')
            ->createQueryBuilder('tb')
            ->select('SUM(tb.nu_horasconvalidada) as nu_horasaluno')
            ->where('tb.id_matricula = :id_matricula')
            ->andWhere('tb.id_situacao = :id_situacao')
            ->andWhere('tb.bl_ativo = :bl_ativo')
            ->setParameters(array(
                'id_matricula' => $matricula->getId_matricula(),
                'id_situacao' => \G2\Constante\Situacao::TB_ATIVIDADECOMPLEMENTAR_DEFERIDO,
                'bl_ativo' => true,
            ))
            ->getQuery()
            ->getSingleScalarResult();

        return $nu_horasaluno >= $nu_horasnecessarias;
    }

    public function salvarAvaliacaoAtividade(array $data)
    {
        try {

            //verifica se foi passado o id da atividade
            if (!isset($data["id_atividadecomplementar"])
                || (isset($data["id_atividadecomplementar"]) && empty($data["id_atividadecomplementar"]))
            ) {
                throw new \Exception("Id da Atividade Complementar não informado.");
            }

            /** Busca atividade no banco @var \G2\Entity\AtividadeComplementar $atividade */
            $atividade = $this->find(\G2\Entity\AtividadeComplementar::class, $data["id_atividadecomplementar"]);

            //verifica se encontrou
            if (!$atividade) {
                throw new \Exception("Atividade Complementar não encontrada.");
            }

            //valida os dados da atividade
            if ($this->validarDadosAtividade($data, array($data["st_caminhoarquivo"]))) {

                //pega o usuario da sessão para definir como usuario da analise
                $usuarioAnalise = $this->sessao->id_usuario;

                //verifica se foi passado via parametro o usuario da analise e substitui o valor
                if (isset($data['id_usuarioanalise']) && !empty($data['id_usuarioanalise'])) {
                    $usuarioAnalise = $data['id_usuarioanalise'];
                }

                //seta os valores na entity
                $atividade->setDt_analisado(new \DateTime())
                    ->setId_usuarioanalise($this->getReference(\G2\Entity\Usuario::class, $usuarioAnalise))
                    ->setId_situacao($this->getReference(\G2\Entity\Situacao::class, $data['id_situacao']))
                    ->setSt_tituloatividade($data["st_tituloatividade"])
                    ->setSt_observacaoavaliador($data["st_observacaoavaliador"])
                    ->setNu_horasconvalidada($data["nu_horasconvalidada"]);

                //salva
                $atividade = $this->save($atividade);
            }

        } catch (\Exception $e) {
            throw $e;
        }

        return $atividade;
    }


}

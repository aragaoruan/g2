<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 28/05/18
 * Time: 11:03
 */

namespace G2\Negocio;


use G2\Entity\HistoricoVincularMatricula;
use G2\Utils\Helper;

class VincularDisciplinaComplementar extends Negocio
{

    const REPOSITORY_MATRICULA = 'G2\Entity\Matricula';
    const REPOSITORY_MATRICULA_DISCIPLINA = 'G2\Entity\MatriculaDisciplina';
    const REPOSITORY_USUARIO = 'G2\Entity\Usuario';
    const REPOSITORY_PROJETO_PEDAGOGICO = 'G2\Entity\ProjetoPedagogico';
    const REPOSITORY_HISTORICO_VINCULAR_MATRICULA = 'G2\Entity\HistoricoVincularMatricula';


    private $historicoVincularMatricula;
    private $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
        $this->historicoVincularMatricula = new HistoricoVincularMatricula();
    }

    /** Função que retorna matriculas com a evolução cursando a partir de id_usuario
     * feature GII-9563
     * @param $idUsuario
     * @return array|\Ead1_Mensageiro
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculas($idUsuario)
    {
        try {
            $result = array();
            if (!is_null($idUsuario)) {
                $idEntidade = $this->sessao->id_entidade;
                $matriculas = $this->getRepository(self::REPOSITORY_MATRICULA)
                    ->retornarMatriculas($idUsuario, $idEntidade);

                if ($matriculas) {

                    foreach ($matriculas as $key => $matricula) {

                        $result[$key]['id_matricula'] = $matricula['id_matricula'];
                        $result[$key]['st_projetopedagogico'] = $matricula['id_matricula'] . ' - '
                            . $matricula['st_projetopedagogico'] . ' - '
                            . $matricula['st_evolucao'] . ' - ('
                            . $matricula['st_nomeentidade'] . ')';
                        $result[$key]['id_evolucao'] = $matricula['id_evolucao'];
                    }
                }

            }

            return $result;

        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro(
                "Erro ao buscar matrícula(s)." . $e->getMessage(),
                \Ead1_IMensageiro::ERRO
            );
        }
    }

    /** Função que retorna matricula complementar com evolução cursando e concluinte passando id_usuario.
     * feature GII-9563
     * @param $idUsuario
     * @return array|\Ead1_Mensageiro
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornaMatriculaComplementar($idUsuario)
    {
        try {
            $result = array();
            if (!is_null($idUsuario)) {
                $idEntidade = $this->sessao->id_entidade;
                $result = $this->getRepository(self::REPOSITORY_MATRICULA)
                    ->retornaMatriculaComplementar($idUsuario, $idEntidade);
                if ($result) {
                    return $result;
                }
            }

            return $result;

        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro(
                "Erro ao buscar matrícula(s)." . $e->getMessage(),
                \Ead1_IMensageiro::ERRO
            );
        }
    }

    /** Função que retorna matricula complementar vinculada passando id_matricula
     * feature GII-9563
     * @param $idMatricula
     * @return array|\Ead1_Mensageiro
     * @author Ruan Aragão <ruan.aragao@unyleya.com.br>
     */
    public function retornarMatriculaComplementarVinculada($idMatricula)
    {
        try {
            $result = array();
            if (!is_null($idMatricula)) {
                $idEntidade = $this->sessao->id_entidade;
                $result = $this->getRepository(self::REPOSITORY_MATRICULA)
                    ->retornarMatriculaComplementarVinculada($idMatricula, $idEntidade);
                if ($result) {
                    return $result;
                }
            }

            return $result;

        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro(
                "Erro ao buscar matrícula(s) complemetar(es)." . $e->getMessage(),
                \Ead1_IMensageiro::ERRO
            );
        }
    }

    /**Função que vincula uma ou varias matriculas complementares em uma matricula
     * @param array $param
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function vincularMatriculas($param = array())
    {

        try {
            $this->beginTransaction();
            if (array_key_exists('id_disciplinacomplementar', $param)) {

                foreach ($param['id_disciplinacomplementar'] as $id_disciplinacomplementar) {

                    $where = array(
                        'id_matricula' => $id_disciplinacomplementar
                    );

                    $matricula = $this->findOneBy(self::REPOSITORY_MATRICULA, $where);
                    $matriculaDisciplina = $this->findOneBy(self::REPOSITORY_MATRICULA_DISCIPLINA, $where);

                    if ($matricula && $matriculaDisciplina) {
                        $matricula->setDt_matriculavinculada(new \DateTime());
                        $matricula->setId_matriculavinculada(
                            $this->getReference(self::REPOSITORY_MATRICULA, (int)$param['id_matricula'])
                        );

                        $this->save($matricula);

                        $matriculaDisciplina->setId_matricula((int)$param['id_matricula']);
                        $matriculaDisciplina->setId_matriculaoriginal(
                            $this->getReference(self::REPOSITORY_MATRICULA, $id_disciplinacomplementar)
                        );
                        $this->save($matriculaDisciplina);

                        $id_matricula = $matriculaDisciplina->getId_matricula();
                        $id_disciplinacomplementar = $matriculaDisciplina->getId_matriculaoriginal()->getId_matricula();

                        if (!self::salvarHistoricoVincularMatricula($id_matricula, $id_disciplinacomplementar)) {
                            throw new Exception('Erro o salvar salvar historico vincular matricula ');
                        }
                    }

                }
            }
            $this->commit();

            return new \Ead1_Mensageiro(
                "Disciplina(s) complementar(es) vinculada(s) com sucesso",
                \Ead1_IMensageiro::SUCESSO
            );

        } catch (\Exception $e) {
            $this->rollback();
            throw new Exception($e->getMessage());
        }
    }

    /**Função para salvar o historico quando uma matricula é vinculada ou desvinculada
     * @param $idMatricula
     * @param $idMatriculaComplementar
     * @param bool $blVincular
     * @return bool
     */
    public function salvarHistoricoVincularMatricula($idMatricula, $idMatriculaComplementar, $blVincular = true)
    {

        try {

            $stProjetoPedagogico = $this->getRepository(self::REPOSITORY_PROJETO_PEDAGOGICO)
                ->retornarStProjeto($idMatriculaComplementar);

            // Criando $st_historicovincularmatricula
            $stHistoricovincularmatricula = $stProjetoPedagogico['st_projetopedagogico'];
            $stHistoricovincularmatricula .= $blVincular ? ' vinculado ' : ' desvinculado ';
            $stHistoricovincularmatricula .= 'a matrícula ';
            $stHistoricovincularmatricula .= $idMatricula;

            //Referencia matricula complementar
            $referenceMatriculaVinculada = $this->getReference(
                self::REPOSITORY_MATRICULA, $idMatriculaComplementar
            );

            //Referencia matricula
            $referenciaMatricula = $this->getReference(
                self::REPOSITORY_MATRICULA, $idMatricula
            );

            //Referencia Usuario
            $referenciaUsuario = $this->getReference(self::REPOSITORY_USUARIO, $this->sessao->id_usuario);

            //Salvando historico
            $historico = new HistoricoVincularMatricula();
            $historico->setId_matriculavinculada($referenceMatriculaVinculada);
            $historico->setId_matricula($referenciaMatricula);
            $historico->setId_usuario($referenciaUsuario);
            $historico->setDt_historicovincularmatricula(Helper::converterData(new \DateTime()));
            $historico->setSt_historicovincularmatricula($stHistoricovincularmatricula);
            $historico->setBl_vinculada((bool)$blVincular);

            if (!$this->save($historico)) {
                throw new Exception('Erro o salvar salvar historico vincular matricula ');
            }

            return true;

        } catch (\Exception $e) {
            throw new Exception($e->getMessage());

        }
    }

    /**Função desvincular uma matricula complementar de uma matricula
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function desvincularMatricula($params = array())
    {
        try {
            $this->beginTransaction();

            if (array_key_exists('id_matricula', $params) && array_key_exists('id_disciplinacomplementar', $params)) {

                $id_matricula = $params['id_matricula'];
                $id_disciplinacomplementar = $params['id_disciplinacomplementar'];

                $where = array(
                    'id_matricula' => $id_disciplinacomplementar
                );

                $wherematriculaDisciplina = array(
                    'id_matriculaoriginal' => $id_disciplinacomplementar
                );
                $matricula = $this->findOneBy(self::REPOSITORY_MATRICULA, $where);
                $matriculaDisciplina = $this->findOneBy(self::REPOSITORY_MATRICULA_DISCIPLINA, $wherematriculaDisciplina);

                if ($matricula && $matriculaDisciplina) {

                    $matricula->setDt_matriculavinculada(null);
                    $matricula->setId_matriculavinculada(null);
                    $this->save($matricula);

                    $matriculaDisciplina->setId_matricula((int)$params['id_disciplinacomplementar']);
                    $this->save($matriculaDisciplina);

                    if (!self::salvarHistoricoVincularMatricula($id_matricula, $id_disciplinacomplementar, false)) {
                        throw new Exception('Erro o salvar salvar historico vincular matricula ');
                    }
                }

            }

            $this->commit();

            return new \Ead1_Mensageiro(
                "Disciplina complementar desvinculada com sucesso",
                \Ead1_IMensageiro::SUCESSO
            );

        } catch (\Exception $e) {
            $this->rollback();
            throw new Exception($e->getMessage());
        }

    }

    /**Função que retorna historico das matriculas vinculadas e desvinculadas.
     * @param $idMatricula
     * @return array
     */
    public function retornarHistorico($idMatricula)
    {
        try {
            $where = array(
                'id_matricula' => $idMatricula
            );
            $order = array(
                'dt_historicovincularmatricula' => 'DESC'
            );
            $historicoVincularMatriculas = $this->findBy(self::REPOSITORY_HISTORICO_VINCULAR_MATRICULA, $where, $order);

            $result = array();

            if ($historicoVincularMatriculas) {

                foreach ($historicoVincularMatriculas as $key => $historicoVincularMatricula) {

                    $result[$key]['st_historicovincularmatricula'] = $historicoVincularMatricula->getSt_historicovincularmatricula();
                    $result[$key]['dt_historicovincularmatricula'] = Helper::converterData(
                        $historicoVincularMatricula->getDt_historicovincularmatricula(), 'd/m/Y'
                    );
                    $result[$key]['st_usuario'] = $historicoVincularMatricula->getId_usuario()->getSt_nomecompleto();
                }
            }

            return $result;

        } catch (\Exception $e) {

            throw new Exception($e->getMessage());
        }

    }
}

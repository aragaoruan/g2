<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Importação de Dados
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class ImportacaoDeDados extends Negocio
{
    private $_headers = [
        'st_cpf',
        'dt_nascimento',
        'st_nomecompleto',
        'st_sexo',
        'st_nomemae',
        'st_nomepai',
        'st_rg',
        'st_orgaoexpeditor',
        'dt_dataexpedicao',
        'st_cep',
        'st_endereco',
        'nu_numero',
        'st_bairro',
        'st_complemento',
        'sg_uf',
        'st_municipio',
        'nu_ddi_residencial',
        'nu_ddd_residencial',
        'nu_telefone_residencial',
        'nu_ddi_celular',
        'nu_ddd_celular',
        'nu_telefone_celular',
        'st_email'
    ];

    /**
     * @var BO PessoaBO
     */
    public $_pessoaBo;

    public function __construct()
    {
        parent::__construct();
        $this->_pessoaBo = new \PessoaBO();
    }

    /**
     * Retorna os municipios vindo do arquivo XLS
     * @return \Ead1_Mensageiro
     */
    public function retornarMunicipio($sg_uf, $st_nomemunicipio, $limit)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\Municipio');
            return $repo->findMunicipiosLike($sg_uf, $st_nomemunicipio, $limit);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function verificarArquivoEnviado($inputFileName)
    {
        /*
         * Verificando mimetype do arquivo carregado
         * */
        $check = new \Zend_Validate_File_MimeType(array(
            'application/vnd.ms-excel',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        ));
        if (1 == 1) {
            //Carregamento do arquivo inicial e load para a verificação
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);

            //Ultima Coluna do arquivo
            $lastColumn = $objPHPExcel->getActiveSheet()->getHighestColumn(1);

            //Ultima linha do arquivo
            $lastRow = $objPHPExcel->getActiveSheet()->getHighestRow();

            //Filtro para verificar o cabeçalho
            $filterHeader = new \G2\Utils\FiltroImportacao(\PHPExcel_Cell::columnIndexFromString('A'), \PHPExcel_Cell::columnIndexFromString($lastColumn), 1, 1);
            $objReader->setReadFilter($filterHeader);
            $objPHPExcel = $objReader->load($inputFileName);

            //Objeto final após o filtro
            $sheetHeaderData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);

            //Compara o cabeçalho do template com o setado na propriedade da classe
            $arrayDiff = array_diff($sheetHeaderData[1], $this->_headers);
            if (empty($arrayDiff)) {
                //Novo filtro para a leitura completa do arquivo
                $filterContent = new \G2\Utils\FiltroImportacao(\PHPExcel_Cell::columnIndexFromString('A'), \PHPExcel_Cell::columnIndexFromString($lastColumn), 1, $lastRow);
                $objReader->setReadFilter($filterContent);
                $objPHPExcel = $objReader->load($inputFileName);

                $maxCell = $objPHPExcel->getActiveSheet()->getHighestRowAndColumn();
                $sheetContentData = $objPHPExcel->getActiveSheet()->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
                // $sheetContentData = array_map('array_filter', $sheetContentData);
                //var_dump($sheetContentData);die;
                $sheetContentData = array_filter($sheetContentData);

                $arrayFinal = array();
                $cont = 0;
                unset($sheetContentData[0]);

                foreach ($sheetContentData as $key => $value) {
                    if ($cont == 0) {
                        foreach ($value as $k => $v) {
                            $cor = $objPHPExcel->getActiveSheet()->getStyle($k . '1')->getFill()->getStartColor()->getRGB();
                            if ($cor == 'E5B8B7') {
                                for ($row = 1; $row <= $lastRow; $row++) {
                                    $cell = $objPHPExcel->getActiveSheet()->getCell($k . $row);

                                    if ($cell->getValue() == '' || $cell->getValue() == null) {
                                        $arrayFinal[$k . $row]['cod'] = 0;
                                        $arrayFinal[$k . $row]['message'] = $cell->getColumn() . $cell->getRow();
                                    }
                                }
                            }
                        }

                        /*
                         * Retorna caso encontrar erro de células requeridas não preenchidas
                         * */
                        if (!empty($errorArray)) {
                            $arrayFinal['retorno'] = false;
                            return array_values($arrayFinal);
                        }
                    }
                    $cont++;

                    $cpfLimpo = \G2\Constante\Utils::limpaCpfCnpj($value['0']);

                    $arrayFinal[$key]['st_cpf'] = $cpfLimpo;
                    $arrayFinal[$key]['dt_nascimento'] = str_replace('-', '/', $value['1']);
                    $arrayFinal[$key]['st_nomecompleto'] = $value['2'];
                    $arrayFinal[$key]['st_sexo'] = $value['3'];
                    $arrayFinal[$key]['st_nomemae'] = $value['4'];
                    $arrayFinal[$key]['st_nomepai'] = $value['5'];
                    $arrayFinal[$key]['st_rg'] = $value['6'];
                    $arrayFinal[$key]['st_orgaoexpeditor'] = $value['7'];
                    $arrayFinal[$key]['dt_dataexpedicao'] = str_replace('-', '/', $value['8']);
                    $arrayFinal[$key]['st_cep'] = $value['9'];
                    $arrayFinal[$key]['st_endereco'] = $value['10'];
                    $arrayFinal[$key]['nu_numero'] = $value['11'];
                    $arrayFinal[$key]['st_bairro'] = $value['12'];
                    $arrayFinal[$key]['st_complemento'] = $value['13'];

                    $municipio = $this->retornarMunicipio($value['14'], $value['15'], 1);

                    $arrayFinal[$key]['id_municipio'] = 5300108;
                    $arrayFinal[$key]['st_municipio'] = 'Brasília';
                    $arrayFinal[$key]['sg_uf'] = 'DF';

                    if (!empty($municipio)) {
                        foreach ($municipio as $m) {
                            $arrayFinal[$key]['id_municipio'] = $m->getId_municipio();
                            $arrayFinal[$key]['st_municipio'] = $m->getSt_nomemunicipio();
                            $arrayFinal[$key]['sg_uf'] = $m->getSg_uf()->getSg_uf();
                        }
                    }

                    $arrayFinal[$key]['nu_ddi_residencial'] = $value['16'];
                    $arrayFinal[$key]['nu_ddd_residencial'] = $value['17'];
                    $arrayFinal[$key]['nu_telefone_residencial'] = $value['18'];
                    $arrayFinal[$key]['nu_ddi_celular'] = $value['19'];
                    $arrayFinal[$key]['nu_ddd_celular'] = $value['20'];
                    $arrayFinal[$key]['nu_telefone_celular'] = $value['21'];
                    $arrayFinal[$key]['st_email'] = $value['22'];
                    /*
                     * Verificando se já existe esse usuário cadastrado no sistema
                     * */
                    $negocioPessoa = new \G2\Negocio\Pessoa();
                    $existeCpf = $negocioPessoa->retornaVwPessoaOne(array('st_cpf' => $cpfLimpo));
                    $vwPessoa = $existeCpf->getMensagem();
                    if (sizeof($vwPessoa) == 0) {
                        $arrayFinal[$key]['bl_existe'] = 0;
                    } else {
                        $arrayFinal[$key]['bl_existe'] = 1;
                        $arrayFinal[$key]['id_usuario'] = $vwPessoa[0]->getId_usuario();
                    }
                    //valida o cpf
                    $validaCpf = $this->_pessoaBo->validaCPF($cpfLimpo);
                    if (!$validaCpf) {
                        $arrayFinal[$key]['bl_cpf'] = 1;
                        $arrayFinal[$key]['bl_existe'] = 1;
                    }
                    //verifica se o campo sexo foi preenchido
                    if (!isset($value['3'])) {
                        $arrayFinal[$key]['bl_sexo'] = 1;
                        $arrayFinal[$key]['bl_existe'] = 1;
                    }

                    //validando email.
                    if (!$this->_pessoaBo->validaEmail($value['22'])) {
                        $arrayFinal[$key]['bl_email'] = 1;
                        $arrayFinal[$key]['bl_existe'] = 1;
                    }
                }

                /*
                 * Retorna os dados da planilha caso esta não estiver com nenhuma célula requerida vazia
                 *
                 * */
                $arrayFinal = array_values($arrayFinal);
                $arrayFinal['retorno'] = true;

                return $arrayFinal;
            } else {
                return array(
                    'cod' => 2,
                    'message' => 'Por favor, verifique o arquivo carregado. Campos requeridos estão faltando',
                    'retorno' => false
                );
            }
        }

        return array(
            'cod' => 1,
            'message' => 'Por favor, verifique o arquivo carregado.',
            'retorno' => false
        );
    }
}
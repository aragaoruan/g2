<?php

namespace G2\Negocio;

/**
 * Classe de negócio para DocumentosUtilizacao (tb_documentosutilizacao)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DocumentosUtilizacao extends Negocio
{

    private $repositoryName = 'G2\Entity\DocumentosUtilizacao';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_documentosutilizacao' => $row->getId_documentosutilizacao(),
                    'st_documentosutilizacao' => $row->getSt_documentosutilizacao(),
                    'bl_ativo' => $row->getBl_ativo()
                );
            }

            return $arrReturn;
        }
    }

}
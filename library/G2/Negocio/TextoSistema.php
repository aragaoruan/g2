<?php

namespace G2\Negocio;

use G2\Entity\TextoSistema as TextoSistemaEntity;
use G2\Utils\Helper;

/**
 * Classe de negocio para Funcionalidade de Textos Sistema
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Update 2013-10-29 Kayo Silva <kayo.silva@unyleya.com.br>
 */
class TextoSistema extends Negocio
{

    private $repositoryName = 'G2\Entity\TextoSistema';
    private $situacaoNegocio;
    private $entidadeNegocio;

    public function __construct()
    {
        parent::__construct();
        $this->situacaoNegocio = new Situacao();
        $this->entidadeNegocio = new Entidade();
    }

    /**
     * Find By Entidade
     * @param integer $idTextoExibicao
     * @return object G2\Entity\TextoSistema
     */
    public function findByEntidadeSessao($params = array())
    {
        $params['id_entidade'] = $this->sessao->id_entidade;
        return $this->findBy($this->repositoryName, $params);
    }

    /**
     * Find By Entidade retornando em array
     * @param integer $idTextoExibicao
     * @return object G2\Entity\TextoSistema
     */
    public function findByTextoSistemaArray($params = array())
    {
        $params['id_entidade'] = $this->sessao->id_entidade;
        $result = $this->findBy($this->repositoryName, $params);

        $arrReturn = array();
        foreach ($result as $row) {
            $arrReturn[] = $this->toArrayEntity($row);
        }
        return $arrReturn;
    }

    /**
     *
     * @param integer $id
     * @return object G2\Entity\TextoSistema
     */
    public function findTextoSistema($id)
    {
        if (!$id)
            return false;
        return parent::find($this->repositoryName, $id);
    }

    /**
     * Salva Texto Sistema
     * @param array $data
     * @return Entity
     */
    public function salvarTextoSistema(array $data)
    {
        //pega as referencias dos relacionamentos
        $situacao = $this->getReference('\G2\Entity\Situacao', $data['id_sistuacao']);
        $entidade = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
        $usuario = $this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
        $orientacaoTexto = $this->getReference('G2\Entity\OrientacaoTexto', $data['id_orientacaotexto']);
        $textoCategoria = $this->getReference('G2\Entity\TextoCategoria', $data['id_textocategoria']);
        $textoExibicao = $this->getReference('G2\Entity\TextoExibicao', $data['id_textoexibicao']);

        //Verifica se o id do registro veio setado no array e define se é um novo ou é para atualizar
        if (isset($data['id']) && !empty($data['id'])) {
            //se existir o id, busca o registro
            $entity = $this->find($this->repositoryName, $data['id']);
        } else {
            //se não existir o id cria um novo registro
            $entity = new TextoSistemaEntity();
        }
        //seta os atributos
        $entity->setSt_textosistema($data['st_textosistema'])
            ->setId_situacao($situacao)
            ->setId_textoexibicao($textoExibicao)
            ->setId_textocategoria($textoCategoria)
            ->setId_orientacaotexto($orientacaoTexto)
            ->setDt_cadastro(new \DateTime())
            ->setId_usuario($usuario)
            ->setId_entidade($entidade)
            ->setSt_texto($data['st_texto'])
            ->setBl_edicao(true);
        //verifica se existe data e converte
        if (!empty($data['dt_inicio'])) {
            $entity->setDt_inicio($this->converteDataBanco($data['dt_inicio'], "datetime"));
        }
        if (!empty($data['dt_fim'])) {
            $entity->setDt_fim($this->converteDataBanco($data['dt_fim'], "datetime"));
        }
        if(!empty($data['id_cabecalho']))
            $entity->setId_cabecalho($this->getReference('\G2\Entity\TextoSistema',$data['id_cabecalho']));
        else
            $entity->setId_cabecalho(null);
        if(!empty($data['id_rodape']))
            $entity->setId_rodape($this->getReference('\G2\Entity\TextoSistema',$data['id_rodape']));
        else
            $entity->setId_rodape(null);
        //salva o registro
        return $this->save($entity);
    }

    /**
     * Retorna os dados da VwTextoSistema
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findVwTextoSistema(array $params)
    {

        try {
            //Textos de sistema da entidade logada
            $params['id_entidade'] = $this->sessao->id_entidade;
            $result = $this->em->getRepository('G2\Entity\VwTextoSistema')->findBy($params);

            //Textos de sistema da entidade pai
            $entidadepai = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

            $resultEntidadePai = array();
            if ($entidadepai instanceof \G2\Entity\Entidade) {
                $params['id_entidade'] = $entidadepai->getId_entidadecadastro()->getId_entidade();
                $resultEntidadePai = $this->em->getRepository('G2\Entity\VwTextoSistema')->findBy($params);
            }

            //união dos textos de sistema
            $resultado = array_merge($result, $resultEntidadePai);

            $arrReturn = array();
            foreach ($resultado as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }
            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    public function retornaTextoMensagemPadrao($idmensagempadrao, $identidade, $params)
    {
        $mensagempadrao = $this->findOneBy('\G2\Entity\MensagemPadrao', array('id_mensagempadrao' => $idmensagempadrao));
        $tipo = $mensagempadrao->getId_tipoenvio()->getId_tipoenvio();
        switch ($tipo) {
            case \G2\Constante\TipoEnvio::HTML:
                $textoSistema = $this->findOneBy('\G2\Entity\VwSistemaEntidadeMensagem', array('id_mensagempadrao' => $idmensagempadrao, 'id_entidade' => $identidade));
                if (!$textoSistema->getId_textosistema()) {
                    return "Matricula efetuada com sucesso. <br> Texto de contrato não definido, entre em contato com a sua entidade para imprimir seu contrato.";
                }

                $txtBO = new \TextoSistemaBO();
                $textoSistemaTO = new \TextoSistemaTO();
                $textoSistemaTO->setId_textosistema($textoSistema->getId_textosistema());
                $mensagem = $txtBO->gerarTexto($textoSistemaTO, $params);
                $texto = $mensagem->getMensagem();
                if ($mensagem->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                    $retorno = $texto[0]->getSt_texto();
                } else {
                    $retorno = $mensagem->getText();
                }
                break;
            default :
                $retorno = "Matricula efetuada com sucesso. <br> Texto de contrato não definido, entre em contato com a sua entidade para imprimir seu contrato.";
                break;
        }
        return $retorno;
    }

    public function salvarProdutoDeclaracao($params)
    {
        try {
            $this->beginTransaction();

            if ($params['id_produtotextosistema']) {
                $objetoProdutoTextoSistema = $this->find('\G2\Entity\ProdutoTextoSistema', array('id_produtotextosistema' => $params['id_produtotextosistema']));
            } else {
                $objetoProdutoTextoSistema = new \G2\Entity\ProdutoTextoSistema();
            }

            $objetoProdutoTextoSistema->setId_entidade($params['id_entidade']);
            $objetoProdutoTextoSistema->setId_produto($params['id_produto']);
            $objetoProdutoTextoSistema->setId_textosistema($params['id_textosistema']);
            $objetoProdutoTextoSistema->setId_formadisponibilizacao($params['id_formadisponibilizacao']);
            $resultado = $this->save($objetoProdutoTextoSistema);

            if (isset($_FILES["arquivo-exemplo"]['size']) && $_FILES["arquivo-exemplo"]['size'] != 0) {
                //ALTERA O NOME DO ARQUIVO
                $tipoArquivo = explode(".", $_FILES["arquivo-exemplo"]["name"]);
                unset($_FILES["arquivo-exemplo"]["name"]);

                //SETA O NOME DO ID DO PRODUTO TEXTO SISTEMA NO ARQUIVO
                $newNameFile = $resultado->getId_produtotextosistema() . '.' . $tipoArquivo[1];
                $_FILES["arquivo-exemplo"]["name"] = $newNameFile;

                //SETA O NOME DO ARQUIVO
                $diretorio = 'upload' . DIRECTORY_SEPARATOR . 'produtodeclaracao' . DIRECTORY_SEPARATOR;
                if (!is_dir('upload' . DIRECTORY_SEPARATOR . 'produtodeclaracao' . DIRECTORY_SEPARATOR)) {
                    mkdir('upload' . DIRECTORY_SEPARATOR . 'produtodeclaracao' . DIRECTORY_SEPARATOR, 0777);
                    throw new \Exception ('Não foi possível criar o diretório informado.');
                }

                chmod('upload' . DIRECTORY_SEPARATOR . 'produtodeclaracao' . DIRECTORY_SEPARATOR, 0777);
                //SALVA O ARQUIVO OU VERIFICA SE EXISTE E SUBSTITUI O ARQUIVO
                if ($resultado->getId_upload() == NULL) {
                    $objetoUpload = new \G2\Entity\Upload();
                    $objetoUpload->setSt_upload($newNameFile);
                    $resultadoUpload = $this->save($objetoUpload);
                    $objetoProdutoTextoSistema->setId_upload($resultadoUpload->getId_upload());
                    $this->save($objetoProdutoTextoSistema);
                } else {
                    $idUpload = $resultado->getId_upload();
                    $objetoUpload = $this->find('\G2\Entity\Upload', array('id_upload' => $idUpload));
                    $objetoUpload->setSt_upload($newNameFile);
                    $this->save($objetoUpload);
                }

                $adp = new \Zend_File_Transfer_Adapter_Http();
                $adp->setDestination($diretorio);
                $adp->receive();
            }

            $this->commit();
            $mensageiro = new \Ead1_Mensageiro("Operação executada com sucesso!", \Ead1_IMensageiro::SUCESSO);

            return $mensageiro;
        } catch (\Exception $e) {
            $this->rollback();
            return $e->getMessage();
        }
    }

    /**
     * Find tb_produtoTextoSistema em array
     * @param integer $idTextoExibicao
     * @return object G2\Entity\TextoSistema
     */
    public function findByProdutoTextoSistemaArray($params = array())
    {
        $result = $this->findBy('\G2\Entity\ProdutoTextoSistema', $params);
        if ($result) {
            $result = $this->toArrayEntity($result[0]);
            $result['st_upload'] = null;
            if ($result['id_upload']) {
                $result['st_upload'] = $this->find('\G2\Entity\Upload', $result['id_upload'])->getSt_upload();
            }
        }

        return $result;
    }

    /**
     *
     * Retorna o texto sistema baseado na mensagem padrao
     * @param integer $id_mensagempadrao
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarTextoSistemaByMensagemPadrao($id_mensagempadrao)
    {
        try {
            $where['id_mensagempadrao'] = $id_mensagempadrao;
            $where['id_entidade'] = $this->sessao->id_entidade;
            $where['id_tipoenvio'] = \G2\Constante\TipoEnvio::HTML;
            return $this->findOneBy('\G2\Entity\VwSistemaEntidadeMensagem', $where);
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao consultar dados de Texto Sistema. " . $ex->getMessage());
        }
    }

    /**
     * Busca texto sistema com join na tb_produtodeclaracao
     * @param integer $idTextoCadastro
     * @return object G2\Entity\TextoSistema
     */
    public function retornaTextoSistemaProdutoDeclaracao($params = array())
    {
        $params['id_entidade'] = $this->sessao->id_entidade;
        $result = $this->em->getRepository('G2\Entity\TextoSistema')->findTextoSistemaProdutoDeclaracao($params);
        $result = $this->findBy($this->repositoryName, $params);

        $arrReturn = array();
        foreach ($result as $row) {
            $arrReturn[] = $this->toArrayEntity($row);
        }
        return $arrReturn;
    }

    /**
     * Busca os dados de acordo com os parametros enviados.
     * Caso necessario que o retorno seja em array, envie true no segundo parametros
     *
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|null|object
     * @throws \Exception
     */
    public function retornaTextoSistema($dados, $array = false)
    {
        $result = $this->findOneBy($this->repositoryName, $dados);

        if (!is_null($result)) {
            if ($array) {
                $result = $this->toArrayEntity($result);
                return $result;
            }
            return $result;
        }
        return array();
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Unyleya06
 * Date: 18/10/2016
 * Time: 10:58
 */

namespace G2\Negocio;


class Curso extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDetalhesCoordenador($idUsuario, $idEntidade) {

        try {

            $arrData = array();

            $descricaoEntity = $this->findOneBy('\G2\Entity\Pessoa', array(
                'id_usuario' => $idUsuario,
                'id_entidade' => $idEntidade
            ));

            if($descricaoEntity instanceof \G2\Entity\Pessoa) {
                $arrData = array(
                    'st_descricaocoordenador' => $descricaoEntity->getSt_descricaocoordenador(),
                    'st_nomeexibicao' => $descricaoEntity->getSt_nomeexibicao(),
                    'st_urlavatar' => $descricaoEntity->getSt_urlavatar()
                );
            }

            return $arrData;

        } catch (\Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Db_Exception($e->getMessage());
        }
    }

    public function salvarDescricaoCoordenador($idUsuario, $idEntidade, $stDescricaoCoordenador) {

        try {

            if(!$idUsuario) {
                Throw new \Exception('ID do usuário inválido.');
            }

            if(!$idEntidade) {
                Throw new \Exception('ID da entidade inválido.');
            }

            if($stDescricaoCoordenador === null) {
                Throw new \Exception('Descrição do coordenador inválida.');
            }

            $this->beginTransaction();

            $descricaoEntity = $this->findOneBy('\G2\Entity\Pessoa', array(
                'id_usuario' => $idUsuario,
                'id_entidade' => $idEntidade
            ));


            if ($descricaoEntity instanceof \G2\Entity\Pessoa) {
                $descricaoEntity->setSt_descricaocoordenador($stDescricaoCoordenador);
            }

            $this->save($descricaoEntity);
            $this->commit();

            return new \Ead1_Mensageiro("Descrição do coordenador salva.", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao salvar descrição do coordenador.' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function getCoordenadorCurso($idProjetoPedagogico) {
        
        try {
            $arrData = array();

            if (!$idProjetoPedagogico) {
                throw new \Exception('Projeto pedagógico não foi definido.');
            }

          $result = $this->findOneBy('\G2\Entity\VwUsuarioPerfilEntidadeReferencia', array('id_projetopedagogico' =>  $idProjetoPedagogico, 'id_perfilpedagogico' => \G2\Constante\PerfilPedagogico::COORDENADOR_DE_PROJETO));

          $arrData = array();
          if(isset($result) && !empty($result)){
            $arrData = array('id_usuario' => $result->getId_usuario());
          }
          return $arrData;

        } catch (\Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Db_Exception($e->getMessage());
        }
    }

    public function getBoasVindasCurso($idProjetoPedagogico) {

        try {

            $arrData = array();

            $result = $this->findOneBy('\G2\Entity\ProjetoPedagogico', array(
                'id_projetopedagogico' => $idProjetoPedagogico
            ));

            if($result instanceof \G2\Entity\ProjetoPedagogico) {
                $arrData = array(
                    'st_boasvindascurso'=> $result->getSt_boasvindascurso(),
                    'st_valorapresentacao' => $result->getSt_valorapresentacao()
                );
            }

        } catch (\Exception $e ){
            $this->excecao = $e->getMessage();
            THROW new \Zend_Db_Exception($e->getMessage());
        }


        return $arrData;
    }
}
<?php

namespace G2\Negocio;

use G2\Constante\Sistema;

/**
 * Description of SalaDeAula
 * @author rafaelbruno
 */
class SalaDeAula extends Negocio
{

    /**
     * Repository Name Entity
     * @var string G2\Entity\SalaDeAula
     */
    private $repositoryName = '\G2\Entity\SalaDeAula';

    /**
     * Retorna Sala de Aula por id
     * @param $id Id da sala de aula
     * @return mixed | \G2\Entity\SalaDeAula
     * @throws \Exception
     */
    public function findSalaDeAula($id)
    {
        try {
            return $this->find($this->repositoryName, $id);
        } catch (\Exception $e) {
            throw new \Exception("Erro ao tentar consultar Sala de Aula" . $e->getMessage());
        }
    }


    public function retornaVwSalaDisciplinaAlocacao(array $where = null)
    {
        return $this->findBy('\G2\Entity\VwSalaDisciplinaAlocacao', $where);
    }

    public function retornaVwSalaDisciplinaPrr(array $where = null)
    {
        return $this->findBy('\G2\Entity\VwSalaDisciplinaPrr', $where);
    }

    /**
     * Método que retorna a grade e sala de aula do Aluno
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param VwAlunoGradeIntegracaoTO $to
     * @return \G2\Entity\VwAlunoGradeIntegracao $entity
     */
    public function retornarAlunoSalaDeAulaIntegracao(\VwAlunoGradeIntegracaoTO $to)
    {


        if (!$to->getId_usuario()) {
            $to->setId_usuario(\Ead1_Sessao::getSessaoUsuario()->id_usuario);
        }

        $where = array(
            'id_disciplina' => $to->getId_disciplina(),
            'id_usuario' => $to->getId_usuario(),
            'id_projetopedagogico' => $to->getId_projetopedagogico(),
            'id_matricula' => $to->getId_matricula(),
            'id_categoriasala' => $to->getId_categoriasala()
        );

        $entity = $this->findOneBy('\G2\Entity\VwAlunoGradeIntegracao', $where);

        if ($entity) {
            if ($entity->getSt_integracao() == null && $entity->getId_alocacao()) {
                $alocacaoTO = new \AlocacaoTO();
                $alocacaoTO->setId_alocacao($entity->getId_alocacao());
                $salaBO = new \SalaDeAulaBO();

                $mensageiroReintegrar = $salaBO->reintegrarAlocacaoAluno($alocacaoTO);

                if ($mensageiroReintegrar->getTipo() != Ead1_IMensageiro::SUCESSO) {
                    THROW new Zend_Validate_Exception("Erro ao reintegrar aluno a sala de aula!!" . $mensageiroReintegrar->getFirstMensagem());
                } else {
                    $entity = $this->findOneBy('\G2\Entity\VwAlunoGradeIntegracao', $where);
                }
            }
        }

        return $entity;
    }

    /**
     * @return array
     */
    public function retornaVwAlunosReativarMoodle(array $params = [])
    {
        return $this->findBy(\G2\Entity\VwAlunosReativarMoodle::class, $params, null, 100);
    }


    /**
     * @return array
     * Retorna vw com professores para serem desalocados do moodle
     * @see RoboController.php::removerPapelProfessorMoodleAction
     */
    public function retornaVwDesativarProfessoresMoodle()
    {
        $retorno = $this->findBy('\G2\Entity\VwDesativarProfessoresMoodle', array('bl_desativarmoodle' => 1), null, 20);
        return $retorno;
    }

    /**
     * Metodo responsavel por listar todas as alocacoes de uma sala, depois disso chamar o metodo que
     * inativa cada uma das alocacoes e em sequencia pega da alocacao o id_matricula disciplina
     * e seta a evolucoe para desalocado na tabela matricula disciplina.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $param
     * @return \G2\Negocio\GerenciaSalas::findSalaDeAula
     */
    public function cancelarSalaDeAula(array $param)
    {

        try {

            $this->beginTransaction();

            $salaDeAula = new \SalaDeAulaBO();

            $arrAlocacao = $this->findBy('\G2\Entity\Alocacao',
                array('id_saladeaula' => $param['id_saladeaula'], 'bl_ativo' => true));

            //Desativando alocacoes ativas da sala de aula
            foreach ($arrAlocacao as $key => $value) {

                $to = new \AlocacaoTO();
                $to->setId_alocacao($value->getId_alocacao());
                $to->setBl_ativo($value->getBl_ativo());
                $salaDeAula->inativarAlocacao($to);

                $matriculaDisciplina = $this->find('\G2\Entity\MatriculaDisciplina',
                    $value->getId_matriculadisciplina()->getId_matriculadisciplina());
                $matriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO);
                $matriculaDisciplina->setId_situacao(\G2\Constante\Situacao::TB_MATRICULADISCIPLINA_PENDENTE);
                $this->save($matriculaDisciplina);
            }

            //Desativando sala de aula
            $situacao = $this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_SALADEAULA_INATIVA);
            $usuarioCancelamento = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);

            $sala = $this->find('\G2\Entity\SalaDeAula', $param['id_saladeaula']);
            $sala->setId_situacao($situacao);
            $sala->setDt_cancelamento(new \DateTime());
            $sala->setId_usuariocancelamento($usuarioCancelamento);
            $this->save($sala);

            $this->commit();

            return $this->find('\G2\Entity\SalaDeAula', $param['id_saladeaula']);

        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Zend_Exception $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }

    }


    /**
     * Retorna vw_saladeaulaquantitativos
     *
     * @param array $params
     * @param array $order
     * @param null $limit
     * @param null $offset
     * @return array
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwSalaDeAulaQuantitativos(array $params = array(), array $order = array(), $limit = null, $offset = null
    )
    {
        return $this->findBy('\G2\Entity\VwSalaDeAulaQuantitativos', $params, $order, $limit, $offset);
    }


    /**
     * Retorna projetos ligados a uma sala de aula
     * @param $array
     * @return array|string
     */
    public function retornaProjetosSala($array)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAreaProjetoSala');
            $query = $repo->createQueryBuilder("vw")
                ->select(array('vw.id_areaprojetosala as id', 'vw.id_projetopedagogico', 'vw.st_projetopedagogico', 'vw.nu_diasacesso'))
                ->distinct(true)
                ->where('vw.id_saladeaula = :id_saladeaula')
                ->setParameter('id_saladeaula', $array['id_saladeaula']);

            $arrayRetorno = $query->getQuery()->getResult();
            if (is_array($arrayRetorno) && count($arrayRetorno)) {
                return new \Ead1_Mensageiro($arrayRetorno, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum Registro Encontrado!', \Ead1_IMensageiro::AVISO);
            }


        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function retornaSalaDeAulaProrrogaSalaAula($dados)
    {
        try {
            $result = $this->em->getRepository('\G2\Entity\SalaDeAula')->retonaSalaAulaProrrogacaoSala($dados);
            return $result;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Funcao necessaria para o webservice SalaWS
     * @param array $arr_id_entidades
     * @param array $arr_codcurso
     * @return array
     */
    public function retornaSalasMoodlePorEntidades($arr_id_entidades, $arr_codcurso = array())
    {
        $results = array();

        if ($arr_id_entidades) {
            $dql = 'SELECT DISTINCT s.id_saladeaula, IDENTITY(s.id_situacao) AS id_situacao, IDENTITY(s.id_tiposaladeaula) AS id_tiposaladeaula, si.st_codsistemacurso, s.dt_abertura, s.dt_encerramento, s.nu_diasextensao, si.id_saladeaulaintegracao' .
                ' FROM G2\Entity\SalaDeAula AS s ' .
                ' JOIN G2\Entity\SalaDeAulaIntegracao AS si WITH s.id_saladeaula = si.id_saladeaula' .
                ' WHERE si.id_sistema = ' . \G2\Constante\Sistema::MOODLE .
                ' AND s.id_entidade IN (' . implode(',', $arr_id_entidades) . ')';

            if ($arr_codcurso) {
                $str_codcurso = "'" . implode("','", $arr_codcurso) . "'";
                $dql .= " AND si.st_codsistemacurso IN ({$str_codcurso})";
            }

            $results = $this->em->createQuery($dql)->getResult();
        }

        return $results;
    }


    /**
     * Importa conteúdo para salas de aula do moodle de acordo com a sala de referência
     * FindAll Vw_ImportaSalasMoodle
     * @return \Ead1_Mensageiro
     * @author Denise Xavier
     */
    public function importarSalaDeAulaMoodle($maxexecute = false)
    {
        $array_retorno = array();
        $mensageiroRetorno = new \Ead1_Mensageiro();
        try {
            $array_salas = $this->findBy(\G2\Entity\VwImportaSalasMoodle::class, array(), array(), $maxexecute);
            if (!empty($array_salas)) {
                foreach ($array_salas as $key => $vw) {
                    if ($vw instanceof \G2\Entity\VwImportaSalasMoodle) {
                        $array_retorno[$key]['id_salamoodleTo'] = $vw->getSt_codsistemacurso();
                        $array_retorno[$key]['st_salamoodleTo'] = $vw->getSt_saladeaula();
                        $array_retorno[$key]['id_salamoodleFrom'] = $vw->getSt_codsistema();
                        $array_retorno[$key]['st_salamoodleFrom'] = $vw->getSt_salareferencia();

                        $moodleWs = new \MoodleCursosWebServices($vw->getId_entidade(), $vw->getid_entidadeintegracao());
                        $salaTo = new \SalaDeAulaTO(array('id_saladeaula' => $vw->getId_saladeaula()));

                        $mensageiroImportacao = $moodleWs->importarSalaCursoReferenciaMoodle($salaTo);
                        if ($mensageiroImportacao->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $retornoArr = $mensageiroImportacao->getFirstMensagem();
                            // se ele retorna uma mensagem e uma exception deu algum erro
                            $entityUpdate = $this->find('\G2\Entity\SalaDeAulaIntegracao',
                                (int)$vw->getId_saladeaulaintegracao());
                            if (is_array($retornoArr) && array_key_exists('message',
                                    $retornoArr) && !empty($retornoArr['message'])
                            ) {
                                $array_retorno[$key]['status'] = implode(" - ", $retornoArr);
                                $array_retorno[$key]['bl_erro'] = true;

                                //salva na tb_erro
                                $erro = new \G2\Entity\Erro();
                                $erro->setBlCorrigido(0);
                                $erro->setBlLido(0);
                                $erro->setDtCadastro(new \DateTime());
                                $erro->setIdEntidade($vw->getId_entidade());
                                $erro->setIdUsuario(1); // não existe usuário nessa parte, é feita pelo robô
                                $erro->setProcesso($this->find('\G2\Entity\Processo',
                                    (int)\ProcessoTO::IMPORTA_SALAS_MOODLE));
                                $erro->setSistema($this->find('\G2\Entity\Sistema', (int)Sistema::MOODLE));
                                $erro->setStCodigo($vw->getId_saladeaula());
                                $erro->setStOrigem('\G2\Negocio\SalaDeAula::importarSalaDeAulaMoodle');
                                $erro->setStMensagem(implode(" - ", $retornoArr));
                                $this->save($erro);

                                if ($entityUpdate instanceof \G2\Entity\SalaDeAulaIntegracao && $entityUpdate->getId_saladeaulaintegracao()) {
                                    $this->beginTransaction();
                                    $entityUpdate->setBl_conteudo(false);
                                    $entityUpdate->setBl_erroimportacao(true);
                                    if ($this->save($entityUpdate)) {
                                        $this->commit();
                                    } else {
                                        $this->rollback();
                                    }
                                }
                            } else {
                                //se não tiver retornado nenhum erro, salva o bl_conteudo como 1

                                if ($entityUpdate instanceof \G2\Entity\SalaDeAulaIntegracao && $entityUpdate->getId_saladeaulaintegracao()) {
                                    $this->beginTransaction();
                                    $entityUpdate->setBl_conteudo(true);
                                    $entityUpdate->setBl_erroimportacao(false);
                                    if ($this->save($entityUpdate)) {
                                        $this->commit();
                                    } else {
                                        $this->rollback();
                                    }
                                }
                                $array_retorno[$key]['status'] = 'Importação realizada com sucesso';
                                $array_retorno[$key]['bl_erro'] = false;
                            }
                        } else {
                            $array_retorno[$key]['status'] = $mensageiroImportacao->getFirstMensagem();
                            $array_retorno[$key]['bl_erro'] = true;
                        }

                    }
                }
                $mensageiroRetorno->setMensageiro($array_retorno, \Ead1_IMensageiro::SUCESSO);

            } else {
                $mensageiroRetorno->setMensageiro('Nenhuma sala para importar.', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $ex) {
            $mensageiroRetorno->setMensageiro('Erro ao importar salas: ' . $ex->getMessage(), \Ead1_IMensageiro::ERRO);

            $erro = new \G2\Entity\Erro();
            $erro->setBlCorrigido(0);
            $erro->setBlLido(0);
            $erro->setDtCadastro(new \DateTime());
            $erro->setIdEntidade(1);
            $erro->setIdUsuario(1); // não existe usuário nessa parte, é feita pelo robô
            $erro->setProcesso($this->getReference('\G2\Entity\Processo', \ProcessoTO::IMPORTA_SALAS_MOODLE));
            $erro->setSistema($this->getReference('\G2\Entity\Sistema', Sistema::MOODLE));
            $erro->setStCodigo($ex->getTraceAsString());
            $erro->setStOrigem($ex->getFile());
            $erro->setStMensagem($ex->getMessage());
            $this->save($erro);
        }
        return $mensageiroRetorno;
    }


    /**
     * @param $parametros
     * @return array
     */
    public function retornaProjetoSala($parametros)
    {

        $arrReturn = array();

        $retorno = $this->retornaProjetosSala(array('id_saladeaula' => $parametros['id_saladeaula']));

        if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $arrReturn = $retorno->getMensagem();

            $sala = $this->find('\G2\Entity\SalaDeAula', $parametros['id_saladeaula']);

            if ($sala && $sala->getBl_vincularturma()) {

                $turmasVinculadas = $this->findBy('\G2\Entity\TurmaSala',
                    array('id_saladeaula' => $parametros['id_saladeaula']));

                if ($turmasVinculadas) {
                    foreach ($turmasVinculadas as $key => $value) {
                        $array[] = $this->toArrayEntity($value);
                    }
                }

                foreach ($arrReturn as $key => $arr) {

                    $parametros['id_projetopedagogico'] = $arr['id_projetopedagogico'];
                    $retorno = $this->getRepository('\G2\Entity\VwTurmaProjetoPedagogico')->returnTurmaSalaByProjeto($parametros);
                    $arr = array();
                    if ($retorno) {
                        foreach ($retorno as $k => $ret) {
                            if ($ret['id_salaturma']) {
                                array_push($arr, $ret);
                            }
                        }
                        $arrReturn[$key]['turmas'] = $arr;
                    }
                }
            }

        }
        return $arrReturn;
    }

    /** Retorna salas em que o aluno está cursando pela matrícula
     * @param $id_matricula integer
     * @return array
     * */
    public function retornaSalaPorMatricula($id_matricula)
    {
        $query = $this->em->createQueryBuilder('')
        ->select('a.id_matricula,d.st_saladeaula,d.id_saladeaula')->from('\G2\Entity\MatriculaDisciplina', 'a')
        ->join('\G2\Entity\Disciplina', 'b', \Doctrine\ORM\Query\Expr\Join::WITH,
            'b.id_disciplina = a.id_disciplina AND b.id_tipodisciplina <> 3')
        ->join('\G2\Entity\Alocacao', 'c', \Doctrine\ORM\Query\Expr\Join::WITH,
            'c.id_matriculadisciplina = a.id_matriculadisciplina')
        ->join('\G2\Entity\SalaDeAula', 'd', \Doctrine\ORM\Query\Expr\Join::WITH,
            'd.id_saladeaula = c.id_saladeaula')
        ->where('a.id_matricula= :id_matricula');

        $query->setParameter('id_matricula', $id_matricula);

        $result = $query->getQuery()->getResult();
        return $result;

    }

}

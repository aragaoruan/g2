<?php
/**
 * Classe de negócio para VwConsultaDisciplina.php
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 *
 * @update 01/11/2017
 * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
 */
namespace G2\Negocio;


use G2\Entity\MatriculaDisciplina;
use G2\Entity\VwMatricula;
use G2\Utils\Helper;

class VwAlunosRecuperar extends Negocio
{

    private $repository = '\G2\Entity\VwAlunosRecuperar';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Parâmetros para Pesquisa:
     *  - A área de conhecimento é obrigatório
     *  - O projeto pedagógico é obrigatório
     * @param null $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaResultadoPesquisa($params = null)
    {
        try {

            if (!($params['id_areaconhecimento'])) {
                throw new \Exception("Voce precisa selecionar ao menos um Projeto Pedagógico!");
            }

            if (!($params['id_tipo_servico'])) {
                throw new \Exception("Voce precisa selecionar o tipo de serviço!");
            }

            if ($params['dt_inicioturma_min'] && $params['dt_inicioturma_max']) {
                $params['dt_inicioturma_min'] = Helper::converterData($params['dt_inicioturma_min'], 'Y-m-d');
                $params['dt_inicioturma_max'] = Helper::converterData($params['dt_inicioturma_max'], 'Y-m-d');
            }

            $results = $this->em->getRepository($this->repository)->pesquisaResultadoAlunosRecuperar($params,
                $this->sessao->id_entidade, $this->criarParametroOcorrencia());

            $ar_return = array();

            if (is_array($results) && $results) {
                foreach ($results as $key => $result) {
                    $result = $this->toArrayEntity($result);

                    // Retornar somente os campos necessarios para deixar o retorno MAIS LEVE
                    $ar_return[] = array(
                        'id_usuario' => $result['id_usuario'],
                        'st_nomecompleto' => $result['st_nomecompleto'],
                        'st_cpf' => $result['st_cpf'],
                        'st_email' => $result['st_email'],
                        'st_ddd' => $result['nu_ddd'] ? $result['nu_ddd'] : '-',
                        'st_telefone' => $result['nu_telefone'] ? $result['nu_telefone'] : '-',
                        'st_dddalternativo' => $result['nu_dddalternativo'] ? $result['nu_dddalternativo'] : '-',
                        'st_telefonealternativo' => $result['nu_telefonealternativo'] ? $result['nu_telefonealternativo'] : '-',
                        'st_areaconhecimento' => $result['st_areaconhecimento'],
                        'st_projetopedagogico' => $result['st_projetopedagogico'],
                        'st_evolucao' => $result['st_evolucao'],
                        'id_evolucao' => $result['id_evolucao'],
                        'dt_inicioturma' => $this->converterData($result['dt_inicioturma']['date'], 'd/m/Y'),
                        'id_disciplina' => $result['id_disciplina'],
                        'dt_aberturasala' => $this->converterData($result['dt_aberturasala']['date'], 'd/m/Y'),
                        'dt_encerramentosala' => $this->converterData($result['dt_encerramentosala']['date'], 'd/m/Y'),
                        'st_nota' => $result['st_notafinal'] === null ? "-" : $result['st_notafinal'],
                        'st_notaead' => $result['st_notaead'] === null ? "-" : $result['st_notaead'],
                        'st_notafinal' => $result['nu_notafinal'] === null ? "-" : $result['nu_notafinal'],
                        'tipo_servico' => $result['id_situacaotiposervico'] === 210 ? "resgate" : "recuperação",
                        'id_matricula' => $result['id_matricula'],
                        'id_saladeaula' => $result['id_saladeaula'] === null ? "-" : $result['id_saladeaula']

                    );
                }
            }

            return $ar_return;
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception("Erro ao Pesquisar." . $e->getMessage());
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

    /**
     * Esta função tem a responsabilidade de criar um novo parametro
     * No qual não retorna na pesquisa os alunos que já estiverem com
     * uma ocorrência aberta. Para ser utilizado como um filtro na função retornaResultadoPesquisa()
     *
     */
    public function criarParametroOcorrencia()
    {
        $whereAssunto = "";  //WHERE que será utilizado na pesquisa dos ids do subassuntos resgate e recuperação

        /**
         * Where que retorna apenas os id's do resgate e recuperação.
         */
        $whereNucleo = array(
            "id_assuntocoresgate" => "IS NOT NULL",
            "id_assuntorecuperacao" => "IS NOT NULL"
        );

        try {
            $nucleoCO = $this->findCustom('G2\Entity\NucleoCo', $whereNucleo);

            $whereAssunto .= " IN ( ";

            foreach ($nucleoCO as $value) {
                $value = $this->toArrayEntity($value);
                $whereAssunto .= $value["id_assuntocoresgate"] . "," . $value["id_assuntorecuperacao"] . ",";
            }

            $whereAssunto = substr($whereAssunto, 0, -1);
            $whereAssunto .= " )";

            /**
             * Este WHERE irá retornar apenas as ocorrencias dos assuntos de interesse
             */
            $finalWhere = array(
                "id_assuntoco" => $whereAssunto
            );

            $ocorrencia = $this->findCustom("\G2\Entity\Ocorrencia", $finalWhere);

            if ($ocorrencia) {
                $idUsuario = " ( ";

                foreach ($ocorrencia as $value) {
                    $id = $this->toArrayEntity($value);
                    $idUsuario .= $id["id_usuariointeressado"]["id_usuario"] . ",";
                }

                $idUsuario = substr($idUsuario, 0, -1);
                $idUsuario .= " )";

                return $idUsuario;
            }

            return [];  //Retorna vazio, para não dar erro na query de pesquisa.

        } catch (\Exception $e) {
            throw new \Exception("Erro, não foi possível encontrar o assunto.");
        }
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para métodos realcionados gerenciamento de alunos concluintes
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Conclusao extends Negocio
{

    var $mensageiro;

    public function __contruct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }


    public function findAllVwEntidadeClasse()
    {
        $id_entidade = $this->sessao->id_entidade;
        $repo = $this->em->getRepository('G2\Entity\VwEntidadeClasse');
        $array_search = array('vw.id_entidade', 'vw.st_nomeentidade');

        $query = $repo->createQueryBuilder("vw")
            ->select($array_search)
            ->where('1=1')
            ->orderBy('vw.st_nomeentidade');

        $where = array();
        $where[] = "vw.id_entidade =" . $id_entidade;
        $where[] = "vw.id_entidadepai =" . $id_entidade;
        $query->andWhere(implode(" OR ", $where));

        $result = $query->getQuery()->getResult();
        return $result;

    }

    public function findByVwProjetoEntidade(array $where, array $order = null)
    {
        return $this->findBy('\G2\Entity\VwProjetoEntidade', $where, array('st_projetopedagogico' => 'ASC'));
    }

    public function findByMatriculaCertificacao(array $where)
    {
        return $this->findBy('\G2\Entity\MatriculaCertificacao', $where);
    }

    public function findByVwMatricula(array $where, array $order = null)
    {
        $where['id_entidadeatendimento'] = $this->sessao->id_entidade;
        $where['bl_ativo'] = 1;
        return $this->findBy('\G2\Entity\VwMatricula', $where);
    }

    public function findByEntidade()
    {
        $where['id_entidade'] = $this->sessao->id_entidade;
        return $this->findBy('\G2\Entity\Entidade', $where);
    }


    /*
     * retorno com a pesquisa principal da tela de Conclusão
     * lista todos os alunos concluintes, de acordo com os filtros passados.
     */
    public function pesquisaAlunosConcluintes($where)
    {

        $toPesquisa = new \PesquisarAlunoTO();
        $toPesquisa->setSt_nomecompleto($where['st_nomecompleto']);
        $toPesquisa->setId_entidadematricula($where['id_entidade']);
        $toPesquisa->setDt_inicio($where['dt_concluinte_inicio']);
        $toPesquisa->setDt_termino($where['dt_concluinte_fim']);
        $toPesquisa->setId_projetopedagogico($where['id_projetopedagogico']);
        $toPesquisa->setBl_documentacao($where['bl_documentacao']);
        $toPesquisa->setId_evolucao(array_key_exists('id_evolucao', $where) ? $where['id_evolucao'] : NULL);
        $toPesquisa->setId_evolucaocertificacao($where["id_evolucaocertificacao"]);
        $toPesquisa->setBl_disciplinacomplementar((boolean)$where["bl_disciplinacomplementar"]);
        //datas para pesquisa de acordo com os combos
        $toPesquisa->setDt_iniciocertificadogerado($where['dt_certificado_inicio']);
        $toPesquisa->setDt_terminocertificadogerado($where['dt_certificado_fim']);
        $toPesquisa->setDt_inicioenviadoaluno($where['dt_envio-aluno_inicio']);
        $toPesquisa->setDt_terminoenviadoaluno($where['dt_envio-aluno_fim']);
        $toPesquisa->setDt_inicioenviadocertificadora($where['dt_envio_certificadora_inicio']);
        $toPesquisa->setDt_terminoenviadocertificadora($where['dt_envio_certificadora_fim']);
        $toPesquisa->setDt_inicioretornadocertificadora($where['dt_retorno_certificadora_inicio']);
        $toPesquisa->setDt_terminoretornadocertificadora($where['dt_retorno_certificadora_fim']);
        $toPesquisa->setBl_academico($where['bl_academico']);

        $bo = new \MatriculaBO();
        $retorno = $bo->retornarAlunosConluintes($toPesquisa);

        if ($retorno->getTipo() != \Ead1_IMensageiro::SUCESSO) {
            return false;
        }

        $array = array();
        foreach ($retorno->getMensagem() as $key => $tos) {
            $array[$key]['id'] = $tos->getId_matricula();
            $array[$key] = $tos->toArray();
        }

        return $array;
    }

    /*
     * Retorna o Status da documentadação do aluno de acordo com setores: Secretaria, Financeiro e Projeto Pedagógico
     */
    public function pesquisaStatusDocumentacaoAluno($dados)
    {
        $bo = new \SecretariaBO();
        $boPedagogico = new \ProjetoPedagogicoBO();
        $to = new \MatriculaTO();

        $to->setId_matricula($dados['id_matricula']);
        $to->setBl_ativo(1);

        $array = array();

        $array['secretaria'] = $bo->retornarStatusSecretaria($to)->getFirstMensagem();

        $array['pedagogico'] = $boPedagogico->retornarStatusPedagogico($to)->getFirstMensagem();

        $boFinanceiro = new \FinanceiroBO();
        $array['financeiro'] = $boFinanceiro->retornarStatusFinanceiro($to)->getFirstMensagem();


        return $array;
    }

    /*
     * Retorna todas os documentos pendentes com o setor: Secretaria
     */
    public function pesquisaDocumentacaoAlunoSecretaria($dados)
    {

        if (!isset($dados['id_matricula']) || $dados['id_matricula'] == '') {
            $mensageiro = new \Ead1_Mensageiro('Nenhum aluno informado', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }

        $bo = new \SecretariaBO();

        $to = new \MatriculaTO();
        $to->setId_matricula($dados['id_matricula']);
//            $to->setBl_ativo(1);

        $detalhamento = $bo->retornarDetalhamentoStatusSecretaria($to);
        if ($detalhamento->getTipo() != \Ead1_IMensageiro::SUCESSO) {
            return false;
        }

        $dados = $detalhamento->getMensagem();
        $dados = array_values(array_filter($dados));
        if (isset($dados[0]['dados'])) {
            foreach ($dados[0]['dados'] as $row) {
                $dados = $row['dados'];
            }
        }
        return $dados;

    }

    /*
     * Retorna todas os documentos pendentes com o setor: Financeiro
     */
    public function pesquisaDocumentacaoAlunoFinanceiro($dados)
    {

        if (!isset($dados['id_matricula']) || $dados['id_matricula'] == '') {
            $mensageiro = new \Ead1_Mensageiro('Nenhum aluno informado', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }

        $bo = new \FinanceiroBO();

        $to = new \MatriculaTO();
        $to->setId_matricula($dados['id_matricula']);
//            $to->setBl_ativo(1);

        $detalhamento = $bo->retornarStatusFinanceiro($to, true);
        if ($detalhamento->getTipo() != \Ead1_IMensageiro::SUCESSO) {
            return false;
        }

        $dados = $detalhamento->getMensagem();
        $dados = array_values(array_filter($dados));
        if (isset($dados[0]['dados'])) {
            foreach ($dados[0]['dados'] as $row) {
                $dados = $row['dados'];
            }
        }
        return $dados;

    }

    /*
     * Retorna todas os documentos pendentes com o setor: Pedagogico
     */
    public function pesquisaDocumentacaoAlunoPedagogico($dados)
    {

        if (!isset($dados['id_matricula']) || $dados['id_matricula'] == '') {
            $mensageiro = new \Ead1_Mensageiro('Nenhum aluno informado', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }

        $bo = new \ProjetoPedagogicoBO();

        $to = new \MatriculaTO();
        $to->setId_matricula($dados['id_matricula']);

        $detalhamento = $bo->retornarDetalhamentoStatusPedagogico($to, true);
        if ($detalhamento->getTipo() != \Ead1_IMensageiro::SUCESSO) {
            return false;
        }

        $dados = $detalhamento->getMensagem();
//        \Zend_Debug::dump($dados);

        $dados = array_values(array_filter($dados));
        if (isset($dados[0]['dados'])) {
            return $dados[0]['dados'];
        } else {
            return false;
        }
    }

    public function certificarAluno($dados)
    {
        if (!isset($dados['id_matricula']) || $dados['id_matricula'] == '') {
            $mensageiro = new \Ead1_Mensageiro('Nenhum aluno informado', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }

        $bo = new \MatriculaBO();
        $to = new \MatriculaTO();
        $to->setId_matricula($dados['id_matricula']);
        $to->setId_evolucaocertificacao($dados['id_evolucaocertificacao']);

        $codigo = $this->gerarCodigoCertificacao($dados['id_matricula']);
        if ($codigo) {
            $to->setSt_codcertificacao($codigo);
        }
        $retorno = $bo->certificarMatricula($to);

        if ($to->getId_evolucaocertificacao() != \MatriculaTO::EVOLUCAO_CERT_APTO_GERAR_CERTIFICADO) {
            $tramite = new \G2\Negocio\Tramite();
            $stTramite = "O certificado foi impresso";
            $tramite->salvarTramiteMatricula($stTramite, $to->getId_matricula());
        }

        return $retorno;

    }
    /**
     * Busca e retorna o certificado impresso do aluno caso exista.
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param int $id_matricula - ID da matrícula do aluno.
     * @return array
     */
    public function retornaCertificadoImpresso($id_matricula) {

        $repo = '\G2\Entity\ImpressaoCertificado';
        $certificadoAluno = $this->findOneBy($repo, array('id_matricula' => $id_matricula));

        $retorno = array();

        if ($certificadoAluno) {
            $retorno = array('id_impressaocertificado' => $certificadoAluno->getId_impressaocertificado(),
                'st_textocertificado'     => $certificadoAluno->getSt_textocertificado()
            );
        }
        return $retorno;
    }

    /**
     * Salva no BD o HTML do certificado do aluno.
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param $params - st_textocertificado, id_matricula e id_usuario.
     * @return \Ead1_Mensageiro
     */
    public function salvarImpressaoCertificado($params) {
        try {
            $entity = new \G2\Entity\ImpressaoCertificado();
            $entity->setSt_textocertificado($params['st_textocertificado']);
            $entity->setId_matricula($this->getReference('\G2\Entity\Matricula', $params['id_matricula']));
            $entity->setId_usuario($this->getReference('\G2\Entity\Usuario', $params['id_usuario']));
            $entity->setDt_cadastro(new \DateTime());
            $this->save($entity);
            return new \Ead1_Mensageiro('Certificado salvo com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar certificado.', \Ead1_IMensageiro::ERRO);
        }
    }

    public function gerarCodigoCertificacao($id_matricula)
    {

        $codCertificacao = null;

        $dados = $this->findByVwMatricula(array('id_matricula' => (int)$id_matricula));

        if (is_array($dados)) {
            foreach ($dados as $row) {
                $codCertificacao = $row->getSt_codcertificacao();
            }
        }

        if (is_null($codCertificacao)) {
            $codCertificacao = $this->insertMatriculaCertificacao($id_matricula);
        }
//        else { //caso seja necessário que seja criado um novo código para um aluno
//            $retornoDesativacao = $this->desativarMatriculaCertificado($id_matricula);
//            if ($retornoDesativacao === false) {
//                return $retornoDesativacao;
//            }
//            $codCertificacao = $this->insertMatriculaCertificacao($id_matricula);
//        }


        return $codCertificacao;

    }

    /*
     * Insere o registro referente a montagem de codigo de certificacao
     */
    public function insertMatriculaCertificacao($idMatricula)
    {

        $dadosEntidade = $this->findByEntidade();

        foreach ($dadosEntidade as $linha) {
            $siglaEntidade = $linha->getSt_siglaentidade() != '' ? $linha->getSt_siglaentidade() : 'DEFAULT';
        }

        $entidade = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);

        $entity = new \G2\Entity\MatriculaCertificacao();
        $entity->setId_matricula($idMatricula);
        $entity->setId_entidade($entidade);
        $entity->setSt_siglaentidade($siglaEntidade);
        $entity->setBl_ativo(1);
        $entity->setNu_ano(date('y'));
        $entity->setDt_cadastro(new \DateTime());

        $retorno = $this->save($entity);

        $codigo = $entity->getId_indicecertificado() . ' - ' . $idMatricula . ' / ' . $entity->getSt_siglaentidade() . '-' . $entity->getNu_ano();

        return $codigo;

    }

    /*
     * Desativa um Codigo de Certificação Matrícula
     * tb_matriculacertificacao
     */

    public function desativarMatriculaCertificado($idMat)
    {
        try {

            $dadosMatCertificado = $this->findByMatriculaCertificacao(array('id_matricula' => $idMat, 'bl_ativo' => 1));
            if (is_array($dadosMatCertificado) && !(is_null($dadosMatCertificado))) {
                foreach ($dadosMatCertificado as $entityMC) {
                    $entityMC->setBl_ativo(0);
                    $this->merge($entityMC);
                }
            }

            return true;

        } catch (\Zend_Exception $e) {
            return false;
        }
    }

    /**
     * Responsavel por receber os dados necessarios para salvar na tabela matricula certificacao,
     * obedecendo ao criterio de se a evolucao da certificacao informada for "65" a matricula
     * e convertida para certificada na tabela matricula
     *
     * @param array $matriculaCertificacao
     * @return \G2\Entity\MatriculaCertificacao
     */
    public function salvarProcessoMatriculaCertificacao(array $matriculaCertificacao)
    {

        if (array_key_exists('id_indicecertificado', $matriculaCertificacao) and $matriculaCertificacao['id_indicecertificado']) {

            try {
                $this->beginTransaction();

                /**
                 * Salvando informacoes da tabela matricula certificacao
                 */
                $entity = $this->find('\G2\Entity\MatriculaCertificacao', $matriculaCertificacao['id_indicecertificado']);

                $serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
                $entityUpdate = $serializer->arrayToEntity($matriculaCertificacao, $entity);

                if (is_null($entity->getId_usuariocadastro())) {
                    $usuarioCadastro = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);
                    $entityUpdate->setId_usuariocadastro($usuarioCadastro);
                }

                $return = $this->save($entityUpdate);

                $evolucaoCert = $this->getReference('\G2\Entity\Evolucao', $matriculaCertificacao['id_evolucaocertificacao']);

                $matricula = $this->find('\G2\Entity\Matricula', $entity->getId_matricula());
                $matricula->setId_evolucaocertificacao($evolucaoCert);

                //Certificando a matricula caso a evolucao da certificacao seja "65 - Enviado ao Aluno"
                if ($matricula->getId_evolucaocertificacao()->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULA_CERT_ENVIADO_ALUNO) {
                    $evolucao = $this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO);
                    $matricula->setId_evolucao($evolucao);

                    //Alterando evolucao das Disciplinas Complementares vinculadas.
                    $where = array(
                        "id_matriculavinculada" => $entity->getId_matricula()
                    );

                    $disciplinascomplemetares = $this->findBy('\G2\Entity\Matricula', $where);
                    foreach ($disciplinascomplemetares as $diciplinaComplemetar) {
                        $diciplinaComplemetar->setId_evolucao($evolucao);
                        $this->save($diciplinaComplemetar);
                    }
                    //Gerando tramite para operacao de tornar o aluno certificado
                    $tramite = new \G2\Negocio\Tramite();
                    $evolucao = \G2\Constante\Evolucao::getArrayMatricula();
                    $stTramite = "A Evolução da matricula foi alterada para " . $evolucao[\G2\Constante\Evolucao::TB_MATRICULA_CERTIFICADO];
                    $tramite->salvarTramiteMatricula($stTramite, $matricula->getId_matricula());

                    //enviando e-mail para aluno
                    if ($entityUpdate instanceof \G2\Entity\MatriculaCertificacao && $entityUpdate->getSt_codigoacompanhamento()) {
                        $vw = new \VwMatriculaTO();
                        $vw->setId_matricula($entityUpdate->getId_matricula());
                        $envioEmail = $this->enviarEmailCodigoRastreamento($vw);
                    }
                }

                //Gerando tramite para evolucao da certificacao
                $tramite = new \G2\Negocio\Tramite();
                $evolucao = \G2\Constante\Evolucao::getArrayCertificado();
                $stTramite = "A Evolução do certificado foi alterada para " . $evolucao[$matricula->getId_evolucaocertificacao()->getId_evolucao()];
                $tramite->salvarTramiteMatricula($stTramite, $matricula->getId_matricula());

                $this->save($matricula);

                $this->commit();

                return $return;

            } catch (\Doctrine\DBALException $de) {
                $this->rollback();
                \Zend_Debug::dump($de);
            }

        }
    }

    /**
     * Responsavel por receber uma lista de dados de matriculas para chamar o metodo que salva
     * individualmente cada processo de certificacao do aluno
     * @param array $arrayMatriculaCertificacao
     * @return array
     */
    public function salvarListProcessoMatriculaCertificacao(array $arrayMatriculaCertificacao)
    {
        $array = array();

        foreach ($arrayMatriculaCertificacao as $matriculaCertificacao) {
            $array[] = $this->salvarProcessoMatriculaCertificacao($matriculaCertificacao);
        }

        return $array;
    }

    /**
     * Metodo responsavel por salvar o processo de vinculacao de matriculas
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $arrayMatriculaComplementar
     * @return array
     * @throws \Zend_Exception
     */
    public function vincularListMatriculaComplementar(array $arrayMatriculaComplementar)
    {

        try {

            //Verificando se as informacoes passadas sao o bastante para execucao do processo
            if (!$arrayMatriculaComplementar) {
                throw new \Zend_Exception('Informações necessarias para o processo não foram recebidas.');
            }

            //O processo abaixo precisa de transacao por ser um processo encadeado em mais de uma tabela
            $this->beginTransaction();
            $array = array();

            /**
             * Salva informacoes de vinculos na tabela matricula
             */
            foreach ($arrayMatriculaComplementar as $matriculaComplementar) {

                $matriculaVinculada =
                    $this->getReference('\G2\Entity\Matricula', $matriculaComplementar['id_matriculavinculada']);

                $entityMatricula = $this->find('\G2\Entity\Matricula', $matriculaComplementar['id_matricula']);
                $entityMatricula
                    ->setId_matriculavinculada($matriculaVinculada)
                    ->setDt_matriculavinculada(new \DateTime());

                if (!$this->save($entityMatricula)) {
                    throw new \Zend_Db_Exception('Erro ao tentar salvar matriculas vinculadas: ' . $matriculaComplementar['id_matricula'], 500);
                } else {

                    //Busca relacionamento na tabela matricula disciplina referentes as matriculas vinculadas
                    $entityMatriculaDisciplina =
                        $this->findBy('\G2\Entity\MatriculaDisciplina',
                            array('id_matricula' => $matriculaComplementar['id_matricula']));

                    foreach ($entityMatriculaDisciplina as $matriculaDisciplina) {

                        $entity = $this->find('\G2\Entity\MatriculaDisciplina', $matriculaDisciplina->getId_matriculadisciplina());
                        $matriculaOriginal =
                            $this->getReference('\G2\Entity\Matricula', $entity->getId_matricula());

                        $entity
                            ->setId_matriculaoriginal($matriculaOriginal)
                            ->setId_matricula($matriculaComplementar['id_matriculavinculada']);

                        if (!$this->save($entity)) {
                            throw new \Zend_Db_Exception('Erro ao executar o subprocesso de alteracao na tb_matriculadisciplina: ' . $matriculaDisciplina->getId_matriculadisciplina(), 500);
                        }
                    }
                }
                $vincularDisciplinaComplementar = new VincularDisciplinaComplementar();
                $vincularDisciplinaComplementar->salvarHistoricoVincularMatricula(
                    $matriculaComplementar['id_matriculavinculada'], // Maricula principal
                    $matriculaComplementar['id_matricula'] // Matricula complementar
                );

            }

            //Comita os processos de alteracoes na tabela matricula e matricula disciplina
            $this->commit();

            //Preenche array com resultado da pesquisa em vw_matricula com resultados das alteracoes
            $array = $this->findBy('\G2\Entity\VwMatricula',
                array(
                    'id_entidadeatendimento' => $this->sessao->id_entidade,
                    'id_usuario' => $arrayMatriculaComplementar[0]['id_usuario'],
                    'id_projetopedagogico' => $arrayMatriculaComplementar[0]['id_projetopedagogico'],
                    'bl_ativo' => true
                ));

            return $array;

        } catch (\Zend_Db_Exception $zbe) {
            $this->rollback();
            throw new \Zend_Exception($zbe->getMessage());
        } catch (\Zend_Exception $ze) {
            throw new \Zend_Exception($ze->getMessage());
        }

    }

    /**
     * @param \MatriculaTO $mTO
     * @return bool
     * @throws \Exception
     * @throws \Zend_Exception
     */
    private function retornaStatusPedagogico(\MatriculaTO $mTO)
    {
        try {
            $matricula = $this->findOneBy('G2\Entity\Matricula', array('id_matricula' => $mTO->getId_matricula()));
            return ((($matricula instanceof \G2\Entity\Matricula) && ($matricula->getBl_academico() == 1)) ? 'Confirmado' : false);
        } catch (\Zend_Exception $e) {
            THROW $e;
        }
    }


    /**
     * Método que envia mensagem para o aluno ao cadastrar o codigo de rastreamento do envio do certificado
     * @param \VwMatriculaTO $to
     * @return \Ead1_Mensageiro
     */
    public function enviarEmailCodigoRastreamento(\VwMatriculaTO $to)
    {
        $mensageiroReturn = new \Ead1_Mensageiro();
        try {

            $to = $to->fetch(false, true, true);
            $mensagemBO = new \MensagemBO();
            $mensageiro = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::ENVIO_COD_RASTREAMENTO_CERTIFICADO, $to, \TipoEnvioTO::EMAIL, true);
            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Zend_Exception($mensageiro->getFirstMensagem());
            }
            $mensageiroReturn->setMensageiro('Email Enviado com Sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $mensageiroReturn->setMensageiro('Erro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $mensageiro->getFirstMensagem());
        }

        return $mensageiroReturn;
    }


    public function gerarXlsEtiquetas($arrayUsuario)
    {
        $fields = array('us.id_usuario', 'us.st_cpf', 'us.st_nomecompleto', 'pe.st_endereco', 'pe.nu_numero', 'pe.st_bairro', 'pe.st_complemento', 'pe.sg_uf', 'pe.st_nomemunicipio', 'pe.st_cep');
        $query = "SELECT " . implode(', ', $fields) . " FROM \G2\Entity\Usuario us LEFT JOIN \G2\Entity\VwPessoaEndereco pe WITH pe.id_usuario = us.id_usuario AND pe.id_tipoendereco = 5";

        $sql_in = array();

        foreach ($arrayUsuario as $usuario) {
            $sql_in[] = $usuario['id_usuario'];
        }

        $query .= ' WHERE us.id_usuario IN (' . implode(',', $sql_in) . ')';
        $query .= ' ORDER BY us.st_nomecompleto ASC';

        $query = $this->em->createQuery($query);
        $results = $query->execute();

        // CORRIGIR CODIFICACAO
        $results = json_encode($results, JSON_UNESCAPED_UNICODE);
        $results = htmlentities($results, ENT_NOQUOTES);
        $results = json_decode(utf8_encode($results), true);

        // CONFIGURACOES DO RELATORIO
        $cabecalho = array(
            'st_cpf' => 'CPF',
            'st_nomecompleto' => 'Nome',
            'st_endereco' => 'Endereço',
            'nu_numero' => 'Número',
            'st_bairro' => 'Bairro',
            'st_complemento' => 'Complemento',
            'sg_uf' => 'UF',
            'st_nomemunicipio' => 'Município',
            'st_cep' => 'CEP'
        );

        $xls = new \Ead1_GeradorXLS();
        $loginBO = new \LoginBO();
        $mensageiro = $loginBO->retornaDadosUsuario($this->sessao->id_usuario, $this->sessao->id_entidade, $this->sessao->id_perfil);
        if ($mensageiro->getCodigo() == \Ead1_IMensageiro::AVISO) {
            echo 'Erro ao gerar o documento. Tente novamente por favor';
            exit;
        }

        $arXlsHeaderTO = array();
        foreach ($cabecalho as $index => $colunaCabecalho) {
            $xlsHeaderTO = new \XLSHeaderTO();
            $xlsHeaderTO->setSt_header($colunaCabecalho);
            $xlsHeaderTO->setSt_par($index);
            $arXlsHeaderTO[] = $xlsHeaderTO;
        }

        $arTO = \Ead1_TO_Dinamico::encapsularTo($results, new \ParametroRelatorioTO(), false, false);

        $x = new \XLSConfigurationTO();
        $x->setFilename('relatorio_etiquetas');

        $xls->geraXLS($arXlsHeaderTO, $arTO, $x, true);
    }

}

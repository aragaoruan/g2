<?php

namespace G2\Negocio;

/**
 * Classe de negócio para métodos realcionados ao vinculo de Turmas a Sala
 * @author Helder Fernandes  <helder.silva@unyleya.com.br>
 */
class TurmaSala extends Negocio
{

    public function __contruct()
    {
        parent::__construct();
    }

   public function salvaArrayTurmaSala($dados, $id_sala){
        $resultados = $this->findBy('\G2\Entity\TurmaSala', array('id_saladeaula'=>$id_sala));
        if($resultados){
            foreach ($resultados as $resultado){
                $this->delete($resultado);
            }
        }

        foreach ($dados as $sala){
            foreach ($sala as $turma){
                $objTurmaSala = new \G2\Entity\TurmaSala();
                $objTurmaSala->setId_turma($this->getReference('\G2\Entity\Turma', $turma));
                $objTurmaSala->setId_saladeaula($this->getReference('\G2\Entity\SalaDeAula', $id_sala));
                $objTurmaSala->setDt_cadastro(new \DateTime());
                $this->save($objTurmaSala);
            }
        }
   }


}

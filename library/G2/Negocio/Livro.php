<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Livro
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-01-21
 */
class Livro extends Negocio
{

    private $repositoryLivro = 'G2\Entity\Livro';
    private $livroBO;

    public function __construct ()
    {
        parent::__construct();
        $this->livroBO = new \LivroBO();
    }

    /**
     * Retorna todas os Livros da Entidade
     * @return \Ead1_Mensageiro
     */
    public function findAllLivro ()
    {

        try {

            $result = $this->em->getRepository($this->repositoryLivro)
                    ->findBy(array('id_entidade' => $this->sessao->id_entidade));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o Livro pelo ID
     * @param type $id_livro
     * @return \Ead1_Mensageiro
     */
    public function findLivroById ($id_livro)
    {

        try {

            $result = $this->em->getRepository($this->repositoryLivro)->find($id_livro);
            if ($result) {
                $arrReturn = $this->toArrayEntity($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna todos os tipos de Livros 
     * @return \Ead1_Mensageiro
     */
    public function findAllTipoLivro ()
    {

        try {

            $result = $this->em->getRepository('G2\Entity\TipoLivro')
                    ->findBy(array(), array('st_tipolivro' => 'ASC'));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna todas as coleções
     * @return \Ead1_Mensageiro
     * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-24-01
     */
    public function findAllLivroColecao ()
    {
        try {
            $result = $this->findBy('G2\Entity\LivroColecao', array('bl_ativo' => true), array('st_livrocolecao' => 'ASC'));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Salva ou edita LivroColecao
     * @param array $data valores para persisitir
     * @return \Ead1_Mensageiro 
     */
    public function salvaLivroColecao (array $data)
    {
        try {
            //Instancia a Entity
            $entity = new \G2\Entity\LivroColecao();
            if (isset($data['id_livrocolecao'])) { // Verifica se id_livrocolecao foi passado
                $colecao = $this->find('\G2\Entity\LivroColecao', $data['id_livrocolecao']); //Find LivroColecao
                if ($colecao) {
                    $entity = $colecao;
                }
            }

            //Set atributos
            $entity->setDt_cadastro($entity->getDt_cadastro() ? $entity->getDt_cadastro() : new \DateTime())
                    ->setId_entidadecadastro($entity->getId_entidadecadastro() ? $entity->getId_entidadecadastro() : $this->sessao->id_entidade)
                    ->setId_usuariocadastro($entity->getId_usuariocadastro() ? $entity->getId_usuariocadastro() : $this->sessao->id_usuario)
                    ->setSt_livrocolecao($data['st_livrocolecao'])
                    ->setBl_ativo(isset($data['bl_ativo']) ? $data['bl_ativo'] : $entity->getBl_ativo());

            //Persiste
            $return = $this->save($entity);
            //Set Mensageiro
            $mensageiro = new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        //Return Mensageiro
        return $mensageiro;
    }

    /**
     * Deleta lógicamento LivroColecao
     * @param integer $id Identificador de Livro Colecao
     * @return \Ead1_Mensageiro
     */
    public function deleteLivroColecao ($id)
    {
        try {
            $colecao = $this->find('\G2\Entity\LivroColecao', $id); //Find LivroColecao
            if ($colecao) {
                $colecao->setBl_ativo(false);
                //Persiste
                $return = $this->save($colecao);
                //Set Mensageiro
                $mensageiro = new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        //Return Mensageiro
        return $mensageiro;
    }

    /**
     * Salva ou edita dados de livro
     * @param array $data Dados para serem postado em livro
     */
    public function salvaLivro (array $data)
    {
        try {
            $to = new \LivroTO();
            $to->montaToDinamico($data);

            if (!$to->getSt_livro()) {
                throw new \Zend_Exception("st_livro é de preenchimento obrigatório!");
            }
            return $this->livroBO->salvarLivro($to);
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna Livro Entidade by params
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function retornaLivroEntidade (array $params = array())
    {
        try {
            $result = $this->findBy('\G2\Entity\LivroEntidade', $params);
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro("Sem Resultado para exibição", \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * 
     * @param type $idLivro
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaLivroEntidade ($idLivro, array $data = array())
    {
        try {
            $livroTO = new \LivroTO();
            $livroTO->setId_livro($idLivro);

            $arrEntidade = array();
            if ($data) {
                foreach ($data as $row) {
                    $entTO = new \LivroEntidadeTO();
                    if ($row['id_entidade']) {
                        $entTO->setId_entidade($row['id_entidade']);
                        $arrEntidade[] = $entTO;
                    }
                }
            }
            return $this->livroBO->salvarArLivroEntidade($arrEntidade, $livroTO);
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function deletaLivro ($id)
    {
        try {
            $livro = $this->find($this->repositoryLivro, $id);
            $livro->setBl_ativo(false);
            $result = $this->save($livro);
            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna um perfil para autor
     * @param array $params
     * @return G2\Entity\Perfil
     */
    public function retornaIdPerfilAutor (array $params = array())
    {
        $params['id_entidade'] = (isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade); //id_entidade Sessão
        return $this->findOneBy('G2\Entity\Perfil', $params);
    }

}

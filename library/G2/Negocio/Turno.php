<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Turno (tb_turno)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Turno extends Negocio
{

    private $repositoryName = 'G2\Entity\Turno';

    public function __construct()
    {
        parent::__construct();
    }

    public function findById($id)
    {
        $result = parent::find($this->repositoryName, $id);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Retorna todos os turnos
     * @param null $repositoryName
     * @param bool $toArray
     * @return mixed|array
     */
    public function findAll($repositoryName = null, $toArray = true)
    {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            if ($toArray) {
                foreach ($result as $key => $row) {
                    $result[$key] = array(
                        'id_turno' => $row->getId_turno(),
                        'st_turno' => $row->getSt_turno()
                    );
                }
            }
            return $result;
        }
    }

    /**
     * Retorna os turnos
     * @param array $where
     * @param bool $toArray
     * @return mixed
     * @throws \Zend_Exception
     */
    public function findByTurno(array $where = array(), $toArray = false)
    {
        try {
            $result = $this->findBy($this->repositoryName, $where);
            if ($result) {
                if ($toArray) {
                    foreach ($result as $key => $row) {
                        $result[$key] = array(
                            'id_turno' => $row->getId_turno(),
                            'st_turno' => $row->getSt_turno()
                        );
                    }
                }
            }
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

}
<?php

namespace G2\Negocio;

use G2\Constante\Sistema;
use G2\Constante\TipoAvaliacao;
use G2\Entity\Tramite;
use G2\G2Entity;

/**
 * Description of Avaliacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Avaliacao extends Negocio
{

    const ENTITY_VW_AVALIACAALUNO = '\G2\Entity\VwAvaliacaoAluno';
    const ENTITY_VW_ENCERRAMENOALUNOS = '\G2\Entity\VwEncerramentoAlunos';
    const ENTITY_UPLOAD = '\G2\Entity\Upload';

    public function findVwAvaliacaoAluno(array $where = null, array $order = null)
    {
        $result = $this->findBy(self::ENTITY_VW_AVALIACAALUNO, $where, $order);
        return $result;
    }

    public function pesquisaVwAvaliacaoAluno(array $params = null)
    {
        $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoAluno');

        $array_search = array('vw.id_avaliacaoaluno', 'vw.id_matricula', 'vw.st_nomecompleto', 'vw.id_modulo', 'vw.st_tituloexibicaomodulo'
        , 'vw.id_disciplina', 'vw.st_tituloexibicaodisciplina', 'vw.st_nota', 'vw.id_avaliacaoagendamento'
        , 'vw.id_avaliacao', 'vw.st_avaliacao', 'vw.id_situacao', 'vw.st_situacao', 'vw.id_tipoprova'
        , 'vw.id_avaliacaoconjuntoreferencia', 'vw.bl_agendamento', 'vw.id_evolucao', 'vw.st_evolucao'
        , 'vw.nu_notamax', 'vw.id_tipocalculoavaliacao', 'vw.id_tipoavaliacao', 'vw.st_tipoavaliacao'
        , 'vw.id_avaliacaorecupera', 'vw.bl_provaglobal', 'vw.nu_notamaxima', 'vw.nu_percentualaprovacao'
        , 'vw.dt_agendamento', 'vw.bl_ativo', 'vw.dt_avaliacao', 'vw.id_saladeaula', 'vw.st_codusuario'
        , 'vw.id_avaliacaoconjunto', 'vw.st_projetopedagogico', 'vw.id_projetopedagogico', 'vw.dt_defesa'
        , 'vw.st_tituloavaliacao', 'vw.id_entidadeatendimento', 'vw.id_upload', 'vw.id_tipodisciplina'
        , 'vw.st_justificativa', 'vw.id_matriculadisciplina', 'vw.id_tiponota', 'vw.dt_cadastrosala'
        , 'vw.dt_encerramentosala'
            //        , 'vw.dt_cadastroavaliacao', 'vw.id_usuariocadavaliacao', 'vw.st_usuariocadavaliacao'
        );

        $query = $repo->createQueryBuilder("vw")
            ->select($array_search)
            ->where('1=1')
            ->orderBy('vw.st_nomecompleto');

        if (isset($params['id_matricula']) && !empty($params['id_matricula'])) {
            $query->andWhere('vw.id_matricula = :id_matricula')
                ->setParameter('id_matricula', $params['id_matricula']);
        }

        if (isset($params['id_avaliacao']) && !empty($params['id_avaliacao'])) {
            $query->andWhere('vw.id_avaliacao = :id_avaliacao')
                ->setParameter('id_avaliacao', $params['id_avaliacao']);
        }
        if (isset($params['id_avaliacaoconjuntoreferencia']) && !empty($params['id_avaliacao'])) {
            $query->andWhere('vw.id_avaliacaoconjuntoreferencia = :id_avaliacaoconjuntoreferencia')
                ->setParameter('id_avaliacaoconjuntoreferencia', $params['id_avaliacaoconjuntoreferencia']);
        }
        $query->andWhere('vw.id_avaliacaoaluno is not null');

        $result = $query->getQuery()->getResult();
//        return (count($result) > 0 ? $result : false);
        return $result;
    }

    /**
     * @param array|null $params
     * @return array
     * @throws \Exception
     */
    public function findVwMatriculaNota(array $params = null)
    {
        try {
            $result = $this->findBy('G2\Entity\VwMatriculaNota', $params);
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Verificar qual o tipo de Avaliação de acordo com o banco
     * @param $idTipoAvaliacao
     * @param $idDisciplina
     * @return array
     */
    private function defineArrDisciplina($idTipoAvaliacao, $idDisciplina)
    {
        $arrTipos = [
            \G2\Constante\TipoAvaliacao::ATIVIDADE_PRESENCIAL => "id_disciplinaatividade",//Atividades
            \G2\Constante\TipoAvaliacao::AVALIACAO_RECUPERACAO => "id_disciplinarecuperacao",//Recuperacao
            \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL => "id_disciplinafinal",//Final
            \G2\Constante\TipoAvaliacao::ATIVIDADE_EAD => "id_disciplinaead",//EAD
            \G2\Constante\TipoAvaliacao::TCC => "id_disciplinatcc",//TCC
        ];

        //Verificar qual o tipo de Avaliação de acordo com o banco
        if (array_key_exists($idTipoAvaliacao, $arrTipos)) {
            return [
                $arrTipos[$idTipoAvaliacao] => $idDisciplina
            ];
        }

        return [];
    }

    /**
     * @description Método responsável por processar a nota do aluno de acordo com a lógica presente na vw_matriculanota
     * @param \G2\Entity\AvaliacaoAluno|mixed $avaliacaoAluno
     * @return \Ead1_Mensageiro
     */
    public function processaNotaDisciplina($avaliacaoAluno, $verificarConcluite = true)
    {
        $mensageiro = new \Ead1_Mensageiro('Disciplina Processada com Sucesso!', \Ead1_IMensageiro::SUCESSO);

        try {
            $id_matricula = $avaliacaoAluno->getId_matricula();

            if ($avaliacaoAluno->getId_matricula() instanceof \G2\Entity\Matricula) {
                $id_matricula = $avaliacaoAluno->getId_matricula()->getId_matricula();
            }

            if (!$avaliacaoAluno->getId_avaliacaoconjuntoreferencia()) {
                throw new \Exception('O id_avaliacaoconjuntoreferencia é obrigatório e não foi passado!');
            }

            //recupera os dadas da disciplina
            $disciplina = $this->find(\G2\Entity\AvaliacaoConjuntoReferencia::class,
                $avaliacaoAluno->getId_avaliacaoconjuntoreferencia()
            );

            //se a disciplina for uma estancia da entidade "AvaliacaoConjuntoReferencia"...
            if ($disciplina instanceof \G2\Entity\AvaliacaoConjuntoReferencia) {

                //Array de condições para a pesquisa da sala
                $whereSala = [
                    'id_saladeaula' => $disciplina->getId_saladeaula()
                ];

                //recupera dados da sala
                $sala = $this->findOneBy(\G2\Entity\DisciplinaSalaDeAula::class, $whereSala);

                //se a disciplina for uma estancia da entidade "DisciplinaSalaDeAula"...
                if ($sala instanceof \G2\Entity\DisciplinaSalaDeAula) {

                    //para verificar quando a matricula é do tipo vinculada
                    $matriculaPrincipalVinculada = $this->findOneBy(\G2\Entity\Matricula::class,
                        array('id_matricula' => $id_matricula));

                    $idMatriculaPrincipal = false;
                    $idMatriculaComplementar = false;
                    if ($matriculaPrincipalVinculada) {
                        //verifica se a e matricula tem uma matricula principal
                        if ($matriculaPrincipalVinculada->getId_matriculavinculada()) {
                            $idMatriculaPrincipal = $matriculaPrincipalVinculada->getId_matriculavinculada()
                                ->getId_matricula();
                            $idMatriculaComplementar = true;
                        }
                    }

                    //Array de condições para a pesquisa da avaliacao do aluno simples
                    $WhereAas = [
                        'id_matricula' => $idMatriculaPrincipal
                            ? $idMatriculaPrincipal
                            : $id_matricula,
                        'id_saladeaula' => $disciplina->getId_saladeaula()
                    ];

                    //Recupera a avalicao do aluno de acordo com os dados da saula de aula
                    $aas = $this->findOneBy('G2\Entity\VwAvaliacaoAlunoSimples', $WhereAas);

                    if (!$aas) {
                        throw new \Exception('Avaliação Aluno não encontrado!');
                    }
                    /*
                    Comeca a criar o array para a pesquisa da vwmatriculanota
                    Uni o array da matricula e da disciplina
                    Recebe as avaliações do aluno
                    */

                    if ($idMatriculaComplementar) {

                        $where = array (
                            'id_matricula' => $id_matricula
                        );
                        $avaliacao = $this->findOneBy('G2\Entity\VwMatriculaNotaVinculada', $where);

                        if (!$avaliacao) {
                            throw new \Exception('Matricula invalida!');
                        }

                    } else {
                        $avaliacaoBy = $this->findVwMatriculaNota(array_merge(
                            ['id_matricula' => $idMatriculaPrincipal
                                ? $idMatriculaPrincipal
                                : $id_matricula],
                            $this->defineArrDisciplina($aas->getId_tipoavaliacao(), $sala->getId_disciplina())
                        ));

                        $avaliacao = [];
                        //Loop para verificar qual a maior nota percentual final, no final do loop restara só um item com a maior nota
                        foreach ($avaliacaoBy AS $key => $ava) {
                            //se a avaliação estiver vazio (primeira vez) recebe esse conjunto de dados que contém a nota maior...
                            if (empty($avaliacao)) {
                                $avaliacao = $ava;
                                //se não estiver vazio, verifica se a nota e maior e sobrescreve o conjunto de dados da avaliação que contém a nota maior
                            } elseif ($ava->getNu_percentualfinal() >= $avaliacao->getNu_percentualfinal()) {
                                $avaliacao = $ava;
                            }
                        }
                    }

                    // Verifica se a $avalição  é uma estancia da entidade "VwMatriculaNota"
                    if ($avaliacao instanceof \G2\Entity\VwMatriculaNota || $avaliacao instanceof \G2\Entity\VwMatriculaNotaVinculada) {

                        $matriculaDisciplina = $this->find('G2\Entity\MatriculaDisciplina', $avaliacao->getId_matriculadisciplina());

                        $matriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO);

                        // Calcular a nota de percentual final...
                        if ($avaliacao->getBl_calcular()) {
                            if ($matriculaDisciplina instanceof \G2\Entity\MatriculaDisciplina
                                && !($matriculaDisciplina->getId_matriculadisciplina())) {
                                throw new \Exception('Não foi encontrado registro para essa matricula disciplina!');
                            }

                            if ($avaliacao->getBl_aprovado()) {
                                $matriculaDisciplina
                                    ->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO);
                            } else {
                                $matriculaDisciplina
                                    ->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO);
                                $this->verificaRecuperacao($avaliacaoAluno);
                            }

                            //recebe a nota do percentual final e salva
                            $matriculaDisciplina->setNu_aprovafinal($avaliacao->getNu_percentualfinal());
                        }

                        //chama a função para atribuir o valor do tipo de serviço
                        $matriculaDisciplina->setId_SituacaoTipoServico($this->regraResgateRecuperacao($this->toArrayEntity($avaliacao), $this->toArrayEntity($matriculaDisciplina)));

                        $this->save($matriculaDisciplina);

                        // Verificar prova final (avaliacao unica) para tornar o aluno apto para o agendamento
                        $this->verificaProvaFinal($id_matricula);

                        if ($avaliacao->getBl_calcular() && $verificarConcluite) {
                            $matriculaDisciplinaConclusaoTO = new \G2\Entity\MatriculaDisciplina();
                            $matriculaDisciplinaConclusaoTO->setId_matricula($id_matricula);

                            $this->concluirMatricula($matriculaDisciplinaConclusaoTO);
                        }
                    }
                }

                $mensageiro->setText('Disciplina Processada com Sucesso!');
                return $mensageiro;
            } else {
                return $mensageiro->setMensageiro('Conjunto de avaliação não encontrado!', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Zend_Exception $e) {
            return $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO, $e);
        } catch (\Exception $e) {
            return $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Metodo que verifica as salas encerradas
     * @param $id_matriculadisciplina
     * @return array
     * @throws \Exception
     */
    private function verificarSalaEncerrada($id_matriculadisciplina)
    {
        try {
            $query = $this->em->createQueryBuilder()
                ->select('vw')
                ->from('\G2\Entity\VwGetDefesaTcc', 'vw')
                ->join('\G2\Entity\Alocacao',
                    'al',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'al.id_matriculadisciplina = vw.id_matriculadisciplina'
                )
                ->join('\G2\Entity\SalaDeAula',
                    'sa',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'al.id_saladeaula = sa.id_saladeaula')
                ->where('vw.id_matriculadisciplina IN (:id_matriculadisciplina)')
                ->setParameter('id_matriculadisciplina', $id_matriculadisciplina)
                ->andWhere('sa.dt_encerramento >= (:dt_encerramento)')
                ->setParameter('dt_encerramento', array(date('Y-m-d')))
                ->andWhere('sa.id_categoriasala = (:id_categoriasala)')
                ->setParameter('id_categoriasala', array(\G2\Constante\CategoriaSala::PRR));

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Método que verifica se todas as Matrículas disciplinas obrigat√≥rias estão com evolução concluída para a
     * Matrícula em questão, se estiverem marca a Matrícula como concluinte na evolução.
     * @param $matriculaDisciplinaTO
     * @throws \Exception
     * @throws \Zend_Exception
     */
    private function concluirMatricula($matriculaDisciplinaTO)
    {
        $matriculaDisciplinaTO->setBl_obrigatorio(true);

        /**
         * @var \G2\Entity\MatriculaDisciplina[] $matriculasDisciplinas
         */
        $matriculasDisciplinas = $this->findBy('G2\Entity\MatriculaDisciplina', array(
            'id_matricula' => $matriculaDisciplinaTO->getId_matricula(),
            'bl_obrigatorio' => $matriculaDisciplinaTO->getBl_obrigatorio()
        ));

        if ($matriculasDisciplinas) {
            $bl_concluir = true;

            $id_matriculadisciplina = array();
            foreach ($matriculasDisciplinas as $matriculaDisciplinaAtual) {
                // Verificar se possui alguma sala com data NÃO EXPIRADA
                $sala_naoencerrada = $this->getRepository(\G2\Entity\VwGradeNota::class)
                    ->verificaSalaNaoEncerrada(
                        $matriculaDisciplinaAtual->getId_matriculadisciplina(),
                        $matriculaDisciplinaAtual->getId_matricula()
                    );

                if ($sala_naoencerrada) {
                    $bl_concluir = false;
                }

                // Verificar se está aprovado
                if (
                    $matriculaDisciplinaAtual->getId_evolucao() !== \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO
                ) {
                    $bl_concluir = false;
                    break;
                }
                array_push($id_matriculadisciplina, $matriculaDisciplinaAtual->getId_matriculadisciplina());
            }

            //verifica sala encerrada
            if (self::verificarSalaEncerrada($id_matriculadisciplina)) {
                $bl_concluir = false;
            }

            $bl_matriculagraduacao = $this->getRepository('\G2\Entity\VwMatricula')
                ->isMatriculaGraduacao($matriculaDisciplinaTO->getId_matricula());

            // @history GII-4413
            // Se for graduação
            if ($bl_matriculagraduacao) {

                // @history GII-9257
                // Verificar TCC com data de defesa e APROVADO
                $tcc = $this->getRepository(\G2\Entity\VwAvaliacaoAluno::class)
                    ->verificarTccComDataDefesaeAprovado($matriculaDisciplinaTO->getId_matricula());

                if (!$tcc) {
                    $bl_concluir = false;
                }

                // Deve possuir as Horas de Atividades Complementares Obrigatórias atingidas de acordo com o curso;
                $ng_atividadecomp = new \G2\Negocio\AtividadeComplementar();

                if (!$ng_atividadecomp->validarTotalHorasAluno($matriculaDisciplinaTO->getId_matricula())) {
                    $bl_concluir = false;
                }
            }

            if ($bl_concluir) {
                $matricula = $this->find('G2\Entity\Matricula', $matriculaDisciplinaTO->getId_matricula());

                if (($matricula instanceof \G2\Entity\Matricula)
                    && ($matricula->getId_evolucao()->getId_evolucao() == \MatriculaTO::EVOLUCAO_MATRICULA_CURSANDO)) {
                    $matricula->setDt_concluinte(new \DateTime());
                    $matricula
                        ->setId_evolucao(
                            $this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE)
                        ); //15- concluinte

                    $matricula->setId_evolucaocertificacao(
                        $this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CERT_APTO_GERAR)
                    ); //61 - Apto gerar certificado


                    if (!$this->save($matricula)) {
                        THROW new \Zend_Exception("ERRO ao alterar a evolução da Matrícula!", 0);
                    }

                    //grava trâmite da conclusão
                    $entityTramite = new \G2\Entity\Tramite();
                    $entityTramite->setId_tipotramite(
                        $this->getReference('\G2\Entity\TipoTramite', \TramiteMatriculaTO::EVOLUCAO)
                    )
                        ->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade))
                        ->setSt_tramite('A evolução da matrícula foi alterada para Concluinte.')
                        ->setDt_cadastro(new \DateTime())
                        ->setBl_visivel(1);
                    $this->save($entityTramite);

                    if ($entityTramite instanceof Tramite && $entityTramite->getId_tramite()) {
                        $bo = new \TramiteBO();
                        $toTM = new \TramiteMatriculaTO();
                        $toTM->setId_matricula($matricula->getId_matricula());
                        $toTM->setId_tramite($entityTramite->getId_tramite());
                        $bo->cadastrarTramiteMatricula($toTM);
                    }

                }
            }
        }
    }

    /**
     * Verifica se cadastra ou nao mensagem de recuperacao do aluno
     * @param $avaliacaoAluno
     * @return boolean
     */
    public function verificaRecuperacao($avaliacaoAluno)
    {
        try {
            if ($avaliacaoAluno->getId_matricula() instanceof \G2\Entity\Matricula) {
                $matricula = $avaliacaoAluno->getId_matricula();
            } else {
                $matricula = $this->find('G2\Entity\Matricula', $avaliacaoAluno->getId_matricula());
            }

            if ($matricula instanceof \G2\Entity\Matricula && !empty($matricula)) {

                if (is_null($matricula->getId_mensagemrecuperacao())) {
                    $mensagemPortal = new \G2\Negocio\MensagemPortal();
                    $retorno = $mensagemPortal->salvaMensagemAgendamentoRecuperacao(array('id_matricula' => $matricula->getId_matricula()));

                    if (is_array($retorno) && $retorno['type'] == 'success' && array_key_exists('obj', $retorno)) {

                        $mensagem = $retorno['obj'];
                        $matricula->setId_mensagemrecuperacao($mensagem->getId_mensagem());

                        if (!$this->save($matricula)) {
                            THROW new \Zend_Exception("ERRO ao alterar a id_mensagemrecuperacao da Matrícula!", 0);
                        }
                    }
                }
            } else {
                THROW new \Zend_Exception("ERRO ao procurar matrícula na avaliação aluno!", 0);
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Metodo responsavel por cadastrar e tratar avaliacoes que serao
     * substituidas de um aluno com ou sem arquivo de TCC
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     * @param array $avaliacaoAluno
     * @param array $arquivo
     * @return array|null|object \G2\Entity\AvaliacaoAluno
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function cadastrarAvaliacaoAluno(array $avaliacaoAluno, $arquivo = null)
    {
        try {

            $avaliacaoBO = new \AvaliacaoBO();
            $avaliacaoAlunoTO = new \AvaliacaoAlunoTO();

            /* GII-8180
             * Mantem a data de postagem (cadastro) do TCC no caso de alteração de nota ou título.
             * Salva também trâmite com histórico de alteração de títulos do TCC.
             * */
            $idAvaliacao = $this->find('\G2\Entity\Avaliacao',
                array('id_avaliacao' => $avaliacaoAluno['id_avaliacao']));

            if ($idAvaliacao->getId_tipoavaliacao()->getId_tipoavaliacao() === TipoAvaliacao::TCC) {

                $avaliacaoAlunoAnterior = $this->find('\G2\Entity\AvaliacaoAluno',
                    array('id_avaliacaoaluno' => $avaliacaoAluno['id_avaliacaoaluno']));

                if ($avaliacaoAlunoAnterior) {
                    $avaliacaoAlunoTO->setDt_cadastro($avaliacaoAlunoAnterior->getDt_cadastro());
                    $avaliacaoAlunoTO->setDt_defesa($avaliacaoAlunoAnterior->getDt_defesa());

                    $tituloTCCAnteiror = trim($avaliacaoAlunoAnterior->getSt_tituloavaliacao());
                    $tituloTCCNovo = trim($avaliacaoAluno['st_tituloavaliacao']);

                    if ($tituloTCCAnteiror !== $tituloTCCNovo) {
                        try {
                            $tramiteMsg = "Título do TCC alterado de '" . $tituloTCCAnteiror . "' para: '" . $tituloTCCNovo . "'";
                            $tramiteNegocio = new \G2\Negocio\Tramite();
                            $tramiteNegocio->salvarTramiteMatricula($tramiteMsg, $avaliacaoAluno['id_matricula'],
                                \G2\Constante\TipoTramite::MATRICULA_TCC);
                        } catch (\Exception $exception) {
                            throw new \Exception('Erro ao salvar trâmite com o histórico de alterações no título do TCC.');
                        }
                    }
                }

                //Se o tipo da disciplina for igual a TCC, atualiza a situacao para AVALIADO PELO ORIENTADOR (206)
                if ($avaliacaoAluno['id_tipodisciplina'] == \G2\Constante\TipoDisciplina::TCC) {
                    $parametrosDeBusca = ['id_alocacao' => $avaliacaoAluno['id_alocacao']];
                    (new Alocacao)->atualizaSituacaoTcc(
                        $parametrosDeBusca,
                        \G2\Constante\Situacao::TB_ALOCACAO_AVALIADO_PELO_ORIENTADOR,
                        false
                    );
                }
            }


            $avaliacaoAlunoTO->setId_avaliacaoaluno($avaliacaoAluno['id_avaliacaoaluno']);
            $avaliacaoAlunoTO->setId_tiponota($avaliacaoAluno['id_tiponota']);
            $avaliacaoAlunoTO->setId_avaliacao(is_array($avaliacaoAluno['id_avaliacao']) && array_key_exists('id_avaliacao', $avaliacaoAluno['id_avaliacao']) ? $avaliacaoAluno['id_avaliacao']['id_avaliacao'] : $avaliacaoAluno['id_avaliacao']);
            $avaliacaoAlunoTO->setId_avaliacaoconjuntoreferencia($avaliacaoAluno['id_avaliacaoconjuntoreferencia']);
            $avaliacaoAlunoTO->setId_matricula(is_array($avaliacaoAluno['id_matricula']) && array_key_exists('id_matricula', $avaliacaoAluno['id_matricula']) ? $avaliacaoAluno['id_matricula']['id_matricula'] : $avaliacaoAluno['id_matricula']);
            $avaliacaoAlunoTO->setSt_nota(array_key_exists('st_nota', $avaliacaoAluno) ? $avaliacaoAluno['st_nota'] : null);
            $avaliacaoAlunoTO->setId_usuariocadastro($this->sessao->id_usuario);
            $avaliacaoAlunoTO->setDt_avaliacao($avaliacaoAluno['dt_avaliacao']);
            $avaliacaoAlunoTO->setId_situacao($avaliacaoAluno['id_situacao']);
            $avaliacaoAlunoTO->setSt_tituloavaliacao($avaliacaoAluno['st_tituloavaliacao']);
            $avaliacaoAlunoTO->setId_upload($avaliacaoAluno['id_upload']);
            $avaliacaoAlunoTO->setBl_ativo($avaliacaoAluno['bl_ativo']);
            $avaliacaoAlunoTO->setId_usuarioalteracao($this->sessao->id_usuario);
            $avaliacaoAlunoTO->setId_sistema(Sistema::GESTOR);
            $avaliacaoAlunoTO->setId_notaconceitual(array_key_exists('id_notaconceitual', $avaliacaoAluno) ? $avaliacaoAluno['id_notaconceitual'] : null);

            if ($avaliacaoAluno['id_tipoavaliacao'] != TipoAvaliacao::CONCEITUAL) {
                if ($avaliacaoAlunoTO->getSt_nota() > $avaliacaoAluno['nu_notamax']) {
                    throw new \Zend_Exception("Erro no cadastro da avaliacao, nota invalida.", 500);
                }
            }

            $mensageiroNota = $avaliacaoBO->salvarAvaliacaoAluno($avaliacaoAlunoTO, null, true, $avaliacaoAluno);

            if ($mensageiroNota->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                if ($arquivo) {
                    $tipo = pathinfo($arquivo['arquivo']['name'], PATHINFO_EXTENSION);

                    $zfile = new \Zend_File_Transfer_Adapter_Http();
                    $zfile->addFilter('Rename', $avaliacaoAlunoTO->getId_avaliacaoaluno() . '.' . $tipo);
                    $zfile->addValidator('ExcludeExtension', false, 'php,exe');
                    $zfile->setDestination('upload/avaliacao');
                    $zfile->receive();
                    $info = $zfile->getFileInfo();

                    if ($zfile->isReceived()) {
                        //Buscando avaliacao que foi enviada
                        $avaliacaoAlunoEntity = $this->find('\G2\Entity\AvaliacaoAluno', $avaliacaoAlunoTO->getId_avaliacaoaluno());

                        //inserindo informacao do arquivo na tabela upload
                        $upload = new \G2\Entity\Upload();
                        $upload->setSt_upload($info['arquivo']['name']);
                        $newEntityUp = $this->save($upload);

                        //Alterando tabela avaliacao aluno com informacao do novo arquivo
                        $avaliacaoAlunoEntity->setId_upload($newEntityUp->getId_upload());
                        if (!$this->save($avaliacaoAlunoEntity)) {
                            throw new \Exception('Erro ao fazer upload de arquivo');

                        }

                        return $avaliacaoAlunoEntity;
                    }
                } else {

                    $avaliacaoAlunoEntity = $this->findOneBy('\G2\Entity\AvaliacaoAluno', array(
                        'id_avaliacaoconjuntoreferencia' => $avaliacaoAluno['id_avaliacaoconjuntoreferencia'],
                        'id_matricula' => $avaliacaoAluno['id_matricula']
                    ), array('id_avaliacaoaluno' => 'DESC'));

                    return $avaliacaoAlunoEntity;
                }

            } else {
                throw new \Zend_Exception($mensageiroNota->getFirstMensagem());
            }


        } catch (\Doctrine\DBAL\DBALException $ex) {
            throw $ex;
        } catch (\Zend_Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Metodo responsavel por salvar dados de avaliacaoaluno
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $avaliacaoAluno
     * @return mixed
     * @throws \Exception
     */
    public function salvarAvaliacaoAluno($avaliacaoAluno)
    {
        try {

            if (empty($avaliacaoAluno['id_usuariocadastro'])) {
                if (!empty($this->sessao->id_usuario)) {
                    $avaliacaoAluno['id_usuariocadastro'] = $this->sessao->id_usuario;
                } else {
                    throw new \Exception("Usuário cadastro não encontrada.");
                }
            }
            $situacao = $this->getReference(\G2\Entity\Situacao::class, $avaliacaoAluno['id_situacao']);
            $usuarioReference = $this->getReference(\G2\Entity\Usuario::class, $avaliacaoAluno['id_usuariocadastro']);

            if (!$usuarioReference) {
                throw new \Exception("Usuário não encontrada.");
            }

            $entity = new \G2\Entity\AvaliacaoAluno();

            if (array_key_exists('id_avaliacaoaluno', $avaliacaoAluno)
                && $avaliacaoAluno['id_avaliacaoaluno']
            ) {
                //code update
                /** @var \G2\Entity\AvaliacaoAluno $entity */
                $entity = $this->find('\G2\Entity\AvaliacaoAluno', $avaliacaoAluno['id_avaliacaoaluno']);
                if (!$entity instanceof \G2\Entity\AvaliacaoAluno) {
                    throw new \Exception("Avaliação Aluno não encontrada.");
                }
                $entity->setId_usuarioalteracao($usuarioReference)
                    ->setDt_alteracao(new \DateTime());
            } else {
                $entity->setBl_ativo($avaliacaoAluno['bl_ativo'])
                    ->setId_situacao($situacao)
                    ->setId_matricula($this->getReference(\G2\Entity\Matricula::class, $avaliacaoAluno['id_matricula']))
                    ->setId_avaliacao($this->getReference(\G2\Entity\Avaliacao::class, $avaliacaoAluno['id_avaliacao']))
                    ->setId_avaliacaoconjuntoreferencia($avaliacaoAluno['id_avaliacaoconjuntoreferencia'])
                    ->setSt_nota($avaliacaoAluno['st_nota'])
                    ->setId_tiponota($avaliacaoAluno['id_tiponota'])
                    ->setId_usuariocadastro($usuarioReference)
                    ->setSt_tituloavaliacao($avaliacaoAluno['st_tituloavaliacao'])
                    ->setId_upload(array_key_exists('id_upload', $avaliacaoAluno)
                    && $avaliacaoAluno['id_upload']
                        ? $avaliacaoAluno['id_upload'] : null)
                    ->setId_notaconceitual($avaliacaoAluno['id_notaconceitual'] == null
                        ? null
                        : $this->getReference(\G2\Entity\NotaConceitual::class, $avaliacaoAluno['id_notaconceitual'])
                    );


                !empty($avaliacaoAluno['dt_cadastro'])
                    ? $entity->setDt_cadastro($avaliacaoAluno['dt_cadastro'])
                    : $entity->setDt_cadastro(new \DateTime());
            }

            if (!empty($avaliacaoAluno['dt_defesa'])) {
                $entity->setDt_defesa($avaliacaoAluno['dt_defesa']);
            }

            if (!empty($avaliacaoAluno['st_justificativa']))
                $entity->setSt_justificativa($avaliacaoAluno['st_justificativa']);

            if (!$entity->getDt_avaliacao()) {
                $entity->setDt_avaliacao(new \DateTime());
            }
            if (!$entity->getDt_cadastro()) {
                $entity->setDt_cadastro(new \DateTime());
            }

            $entity->setBl_ativo($avaliacaoAluno['bl_ativo'])
                ->setId_situacao($situacao);

            if (array_key_exists('id_sistema', $avaliacaoAluno) && $avaliacaoAluno['id_sistema']) {
                $entity->setId_sistema(
                    $this->getReference(\G2\Entity\Sistema::class, $avaliacaoAluno["id_sistema"]));
            }

            if (array_key_exists('id_sistema', $avaliacaoAluno) && !empty($avaliacaoAluno["id_sistema"])) {
                $entity->setId_sistema($this->getReference(\G2\Entity\Sistema::class, $avaliacaoAluno["id_sistema"]));
            }


            $entity = $this->save($entity);

            return $entity;

        } catch (\Exception $ei) {
            throw $ei;
        }
    }

    public function atualizarEvolucao($param, $conceitual)
    {

        $whereAvaliacaoaluno = array(
            'id_avaliacaoaluno' => $param->getId_avaliacaoaluno()
        );
        $vw_avaliacaoaluno = $this->findOneBy('\G2\Entity\VwAvaliacaoAluno', $whereAvaliacaoaluno);

        try {
            $where = array(
                'id_matriculadisciplina' => $vw_avaliacaoaluno->getid_matriculadisciplina()
            );
            $matriculaDisciplina = $this->find('\G2\Entity\MatriculaDisciplina', $where);

            if ($conceitual['id_notaconceitual'] == \G2\Constante\NotaConceitual::CONCLUIDO) {
                $matriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO);
            }
            if ($conceitual['id_notaconceitual'] == \G2\Constante\NotaConceitual::NAO_CONCLUIDO) {
                $matriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO);
            }

            $entity = $this->save($matriculaDisciplina);
            return $entity;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo responsavel por filtrar e retornar lista de avaliacoes simples registradas
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param type $where
     * @param array $order
     * @return Array VwAvaliacaoAlunoRegistrada
     */
    public function findVwAvaliacaoAlunoRegistrada($where, array $order = null)
    {
        return $this->findBy('\G2\Entity\VwAvaliacaoAlunoRegistrada', $where, $order);
    }

    /**
     * Verificar se o aluno (MATRICULA) esta com todas as notas lancadas (SATISFATORIAS) e salas de aulas encerradas (REPASSADAS PELO PROFESSOR)
     * @param int $id_matricula
     * @return bool
     * @deprecated use verificarStatusNota instead
     */
    public function alunoAptoAgendamentoPos($id_matricula)
    {
        $query = $this->em->getRepository('\G2\Entity\VwGradeTipo')
            ->createQueryBuilder("vw")
            ->where('vw.id_matricula = :id_matricula')
            ->andWhere('vw.id_tipoavaliacao != :id_tipoavaliacao')
            ->andWhere('vw.bl_status = :bl_status')
            ->setParameters(array(
                'id_matricula' => $id_matricula,
                'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL,
                'bl_status' => false
            ));

        // Verificar se existe registros de NOTAS que ainda NAO estao com bl_status = 1 (NAO ESTAO APTOS)
        $nao_aptos = $query->getQuery()->getResult();

        return $nao_aptos ? false : true;
    }

    /**
     * @description Método que verifica se o aluno está apto a prova final (avaliacao unica) ou não para a POS GRADUACAO
     *  - PÓS - Significa prova por matricula
     *  - GRA ou GRAD - Significa prova por disciplina
     * @param mixed $matricula
     * @return bool
     */
    public function verificaProvaFinal($matricula)
    {
        // Busca a entity caso o parametro for somente o id
        if (!$matricula instanceof \G2\Entity\Matricula) {
            $matricula = $this->find('\G2\Entity\Matricula', $matricula);
        }

        //Apenas verifica o agendamento se o aluno estiver cursando GII-7543
        if ($matricula->getId_evolucao()->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO) {
            $id_matricula = $matricula->getId_matricula();

            // Verificar se o aluno esta apto para o agendamento da prova presencial

            // ALTERACOES GII-962

            $en_confg = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
                'id_entidade' => $matricula->getId_entidadematricula(),
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::AVALIACAO_POR_DISCIPLINA,
                'st_valor' => true
            ));

            // Flag que defina se é prova por disciplinas (GRAD) ou por matricula (POS)
            $bl_provapordisciplina = $en_confg instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao;

            // Buscar o id_situacaoagendamento ATUAL
            $id_situacaoagendamento = $matricula->getId_situacaoagendamento() ? $matricula->getId_situacaoagendamento()->getId_situacao() : null;

            // Inicia a variavel como TRUE (aluno apto) para POS
            $aluno_apto = true;

            // Se for PROVA POR DISCIPLINA, então coloca como APTO em cada registro na tb_matriculadisciplina que estiver apto
            if ($bl_provapordisciplina) {
                $ng_gerenciaprova = new \G2\Negocio\GerenciaProva();
                $ng_gerenciaprova->tornarAlunosAptosAgendamentoPorDisciplina(array('id_matricula' => $id_matricula));
            } else {
                // Consulta as disciplinas que ele tem na tb_matriculadisciplna que tem bl_obrigatorio = 1 e que ele ainda nao ta aprovado nela
                $disciplinas = $this->findBy('\G2\Entity\MatriculaDisciplina', array(
                    'id_matricula' => $id_matricula,
                    'bl_obrigatorio' => true
                ));

                if (is_array($disciplinas)) {
                    // Se o Aluno ja possui presença em alguma avaliação, então ele deve manter o status APTO para poder liberar a Recuperação
                    $possui_presenca = $this->getRepository(\G2\Entity\AvaliacaoAgendamento::class)
                        ->verificarPossuiPresenca($id_matricula);

                    if (!$possui_presenca) {
                        foreach ($disciplinas as $disciplina) {
                            // Verificar se o status da nota é satisfatorio
                            $status = $this->verificarStatusNota($disciplina);

                            if (!$status) {
                                $aluno_apto = false;
                                // Nao é necessário continuar o for caso encontre alguma disciplina da PÓS que ele não esteja apto
                                break;
                            }
                        }
                    }
                }

                // Salvar tramite aluno apto/não apto
                $ng_tramite = new \G2\Negocio\Tramite();
                $ng_tramite->salvarTramiteAptoAgendamento(
                    $matricula,
                    $aluno_apto ? \G2\Constante\Situacao::TB_MATRICULA_APTO : \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO,
                    $this->sessao->id_usuario ? $this->sessao->id_usuario : $matricula->getId_usuario() ? $matricula->getId_usuario()->getId_usuario() : null,
                    $this->getId_entidade() ? $this->getId_entidade() : $matricula->getId_entidadematricula(),
                    !$aluno_apto ? \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO_NOTA : ($aluno_apto && !$matricula->getBl_documentacao() ? \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO_DOCUMENTOS : \G2\Constante\Situacao::TB_MATRICULA_APTO_PARCIAL)
                );

            }

            // Se for PROVA POR DISCIPLINA (GRA), entao reseta as referencias da tb_matricula, porque na GRA usa-se a tb_matriculadisciplina
            // OU se o aluno não estiver apto na POS
            if ($bl_provapordisciplina || !$aluno_apto) {
                $matricula->setId_situacaoagendamento($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO));
                $matricula->setId_situacaoagendamentoparcial($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO_NOTA));
                $matricula->setDt_aptoagendamento(null);

            } else if ($id_situacaoagendamento != \G2\Constante\Situacao::TB_MATRICULA_APTO || !$matricula->getDt_aptoagendamento()) {

                /**
                 * @history GII-7924
                 */

                $matricula->setId_situacaoagendamento($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_APTO));
                $matricula->setDt_aptoagendamento(date('Y-m-d H:i:s'));

                // Salvar mensagem de agendamento do PORTAL do ALUNO
                $ng_mensagemportal = new \G2\Negocio\MensagemPortal();
                $ng_mensagemportal->salvaMensagemAgendamento(array('id_matricula' => $matricula->getId_matricula()));

                /*
                //Se o aluno for da POS e tiver a marcação de documentação completa, ele recebe apto para o agendamento
                if(!$bl_provapordisciplina && $matricula->getBl_documentacao()){
                    $matricula->setId_situacaoagendamento($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_APTO));
                    $matricula->setId_situacaoagendamentoparcial($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_APTO_PARCIAL));
                    $matricula->setDt_aptoagendamento(date('Y-m-d H:i:s'));

                    // Salvar mensagem de agendamento do PORTAL do ALUNO
                    $ng_mensagemportal = new \G2\Negocio\MensagemPortal();
                    $ng_mensagemportal->salvaMensagemAgendamento(array('id_matricula' => $matricula->getId_matricula()));
                }else{
                    $matricula->setId_situacaoagendamentoparcial($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_NAO_APTO_DOCUMENTOS));
                }
                */
            }

            return $this->save($matricula);
        } else {
            return false;
        }

    }

    /**
     * @param \G2\Entity\MatriculaDisciplina $en_matriculadisciplina
     * @param $st_nota - Usado caso queira verificar baseado em uma nota específica, e não o valor salvo no banco
     * @return bool - True se for SATISFATORIO e false se for INSATISFATÓRIO
     * @throws \Zend_Exception
     */
    public function verificarStatusNota(\G2\Entity\MatriculaDisciplina $en_matriculadisciplina, $st_nota = null)
    {
        // Verificar se a evolução já é aprovado
        // Quando o parametro $st_nota existir, não se deve considerar esta condição
        if ($en_matriculadisciplina->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO && $st_nota === null) {
            return true;
        }

        // Verificar se tem nota de aproveitamento, se tiver, está apto
        $aproveitamento = $this->findOneBy('\G2\Entity\VwAvaliacaoAlunoSimples', array(
            'id_matriculadisciplina' => $en_matriculadisciplina->getId_matriculadisciplina(),
            'id_tiponota' => \G2\Constante\TipoNota::APROVEITAMENTO
        ));

        if ($aproveitamento instanceof \G2\Entity\VwAvaliacaoAlunoSimples) {
            return true;
        }

        // Buscar notas da disciplina para fazer o calculo se está Satisfatório
        $notas = $this->findBy('\G2\Entity\VwMatriculaNota', array(
            'id_matriculadisciplina' => $en_matriculadisciplina->getId_matriculadisciplina()
        ));

        // Verificar se possui alguma avaliacao com nota satisfatoria e sala encerrada
        $status_disciplinas = array();

        if (is_array($notas)) {
            /**
             * @var \G2\Entity\VwMatriculaNota $nota
             */
            foreach ($notas as $nota) {
                $id_matriculadisciplina = $nota->getId_matriculadisciplina();
                $nu_notafaltante = 0;
                $nu_notatotal = floatval($nota->getNu_notatotal());
                $nu_notaaprovacao = $nota->getNu_notaaprovacao();

                if ($st_nota !== null) {
                    $nu_notatotal = floatval($st_nota);
                }

                // Somatorio nota faltante
                //$nu_notafaltante = $nota->getNu_notamaxatividade() + $nota->getNu_notamaxead() + $nota->getNu_notamaxfinal() + $nota->getNu_notamaxtcc() + $nota->getNu_notamaxrecuperacao();
                $nu_notafaltante += is_null($nota->getSt_notafinal()) ? $nota->getNu_notamaxfinal() : 0;
                $nu_notafaltante += is_null($nota->getSt_notaead()) ? $nota->getNu_notamaxead() : 0;
                $nu_notafaltante += is_null($nota->getSt_notaatividade()) ? $nota->getNu_notamaxatividade() : 0;
                $nu_notafaltante += is_null($nota->getSt_notatcc()) ? $nota->getNu_notamaxtcc() : 0;
                $nu_notafaltante += is_null($nota->getSt_notarecuperacao()) ? $nota->getNu_notamaxrecuperacao() : 0;

                // Boolean - Status do aluno na avaliacao desta disciplina
                // É necessarário que a sala tenha ENCERRAMENTO e também a nota esteja satisfatória.
                // O cálculo da nota satisfatória é:
                // ∑ nota + ∑ nota max >= Média de aprovação
                $status = $nota->getId_encerramentosala() && (($nu_notatotal + $nu_notafaltante) >= $nu_notaaprovacao);

                // Cria o index da matriculadisciplina pra verificar se ele esta satisfatorio nela
                // Se o index ja existir, então define o novo valor caso o antigo seja FALSE, porque se for TRUE, ele ja esta aprovado na disciplina!
                if (!isset($status_disciplinas[$id_matriculadisciplina]) || !$status_disciplinas[$id_matriculadisciplina]) {
                    $status_disciplinas[$id_matriculadisciplina] = $status;
                }
            }
        }

        // Se algum valor no array FOR FALSE, quer dizer que a disciplina esta INSATISFATORIA ou NAO ENCERRADA em todas as avaliações feitas nela
        return !in_array(false, $status_disciplinas);
    }


    /**
     * Verifica se o aluno pode enviar TCC na ultima etapa do Moodle
     * Envio é feito dentro do portal
     * @param $params
     * @param $baseUrl
     * @return \Ead1_Mensageiro
     */
    public function verificaEnvioTccMoodle($params, $baseUrl = null)
    {
        try {
            $id_saladeaula = false;
            $id_matricula = false;
            $array_retorno = [];

            $msgRetorno = new \Ead1_Mensageiro();
            $moodleIntegracao = new \G2\Negocio\Moodle();
            /** @var \G2\Entity\VwAlunoGradeIntegracao $integracao */
            $integracao = $moodleIntegracao->verificaIntegracaoAlunoSalaMoodle($params);

            //se o aluno estiver integrado, retorna uma instancia da vw_alunogradeintegracao
            if ($integracao instanceof \G2\Entity\VwAlunoGradeIntegracao) {
                $id_saladeaula = $integracao->getId_saladeaula();
                $id_matricula = $integracao->getId_matricula();
                $array_retorno['dadosAluno'] = $integracao;
            }

            if (!($id_saladeaula) || !($id_matricula)) {
                $msgRetorno->setMensageiro('Você não tem perfil de aluno nessa sala ou não possui integração. Entre em contato com o suporte.', \Ead1_IMensageiro::AVISO);
            } else {
                //Consultando os dados da avaliacao do aluno
                /** @var \G2\Entity\VwAvaliacaoAluno $vwAvaliacaoAluno */
                $vwAvaliacaoAluno = $this->findOneBy(self::ENTITY_VW_AVALIACAALUNO,
                    array('id_matricula' => $id_matricula,
                        'id_saladeaula' => $id_saladeaula,
                        'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::TCC));
                $array_retorno['avaliacao'] = $vwAvaliacaoAluno;

                //Verificando se a avaliação não foi encerrada pelo professor
                /** @var \G2\Entity\VwEncerramentoAlunos $vwencerramento */
                $vwencerramento = $this->findOneBy(self::ENTITY_VW_ENCERRAMENOALUNOS,
                    array('id_matricula' => $id_matricula,
                        'id_saladeaula' => $id_saladeaula));

                $array_retorno['encerramento'] = $vwencerramento;
                $array_retorno['encerrada'] = false;

                if ($vwencerramento->getDt_encerramentoprofessor()) {
                    $array_retorno['encerrada'] = true;
                }
                $array_retorno['podePublicar'] = false;

                $array_pode_publicar = array(\G2\Constante\Situacao::TB_ALOCACAO_PENDENTE, \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO);
                $array_retorno['podePublicar'] = in_array($integracao->getId_situacaotcc(), $array_pode_publicar) ? true : false;

                $array_retorno['url_upload'] = $baseUrl . \Ead1_Ambiente::geral()->baseUrlUpload . '/avaliacao/';
                if ($vwAvaliacaoAluno->getId_upload()) {
                    $upload = $this->find(self::ENTITY_UPLOAD
                        , $vwAvaliacaoAluno->getId_upload());
                    $array_retorno['upload'] = $upload;
                }
                $msgRetorno->setMensageiro($array_retorno, \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $e) {
            $msgRetorno->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $msgRetorno;
    }

    /**
     * Essa função tem a responsabilidade de identificar se o aluno
     * Se enquadra na regra de resgate ou de recuperação
     *
     * @param $vwMatriculaNota
     * @return string
     */
    public function regraResgateRecuperacao($vwMatriculaNota, $matriculaDisciplina)
    {

        if ($matriculaDisciplina["id_evolucao"] !== \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO) {
            if ($vwMatriculaNota["st_notafinal"] && $vwMatriculaNota["st_notafinal"] < 30 && $vwMatriculaNota["st_notaead"] > 10) {
                return \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_RECUPERACAO;
            }

            return \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_RESGATE;
        }

        return null;
    }

}

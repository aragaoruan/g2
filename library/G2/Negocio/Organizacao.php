<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Organizacao
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>;
 */
class Organizacao extends Negocio
{

    /**
     * Metodo responsavel por criar ou editar informacoes de notificacao de uma entidade
     *
     * @param array $data
     * @throws \Zend_Exception
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function salvarDadosNotificacaoEntidade(array $data)
    {
        try {

            $where = array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_notificacao' => $data['id_notificacao']
            );

            //Verifica se a entidade ja tem informacoes para o tipo de notificacao repassado.
            $notificacaoEntidade = $this->findOneBy('\G2\Entity\NotificacaoEntidade', $where);

            //Caso nao tenha notificacao cadastrada para entidade, sera criada uma nova.
            if (!$notificacaoEntidade) {
                $notificacaoEntidade = new \G2\Entity\NotificacaoEntidade();

                $entidade = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
                $notificacao = $this->find('\G2\Entity\Notificacao', $data['id_notificacao']);

                $notificacaoEntidade->setId_entidade($entidade);
                $notificacaoEntidade->setId_notificacao($notificacao);
            }

            //Caso tenha sido informado ou alterado a informacao de envio de email
            if (array_key_exists('bl_enviaparaemail', $data)) {
                $notificacaoEntidade->setBl_enviaparaemail((boolean)$data['bl_enviaparaemail']);
            }

            if (array_key_exists('nu_percentuallimitealunosala', $data) && (int)$data['nu_percentuallimitealunosala']) {
                $notificacaoEntidade->setNu_percentuallimitealunosala($data['nu_percentuallimitealunosala']);
            }

            //Salvando informacoes da notificacao relacionada a entidade
            $idNotificacaoEntidade = $this->save($notificacaoEntidade);

            //Caso seja adicionado, excluido perfis para lista de recipientes de notificacoes
            if ($data['perfis']) {
                $this->salvarPerfisNotificacaoEntidade($data['perfis'], $notificacaoEntidade);
            }

            return $this->findBy('\G2\Entity\VwPerfisEntidadeNotificacao', $where);

        } catch (\Doctrine\ORM\Exception $e) {
            throw new \Zend_Exception($e->getMessage(), 500);
        }
    }

    /**
     * Metodo responsavel por salvar os perfis que receberam notificacoes de uma entidade.
     *
     * @param array $perfis
     * @param \G2\Entity\NotificacaoEntidade $notificacaoEntidade
     * @throws \Zend_Exception
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function salvarPerfisNotificacaoEntidade(array $perfis, \G2\Entity\NotificacaoEntidade $notificacaoEntidade)
    {

        try {

            //Executando varios processos de perfis a serem vinculados
            foreach ($perfis as $key => $value) {

                if (!$value['id_notificacaoentidadeperfil']) {

                    //Verificando se o perfil ja nao se encontra vinculado
                    if (!$this->find('\G2\Entity\NotificacaoEntidadePerfil', $value['id_notificacaoentidadeperfil'])) {

                        $notificacaoEntidade = $this->getReference('\G2\Entity\NotificacaoEntidade', $notificacaoEntidade->getId_notificacaoentidade());
                        $perfil = $this->getReference('\G2\Entity\Perfil', $value['id_perfil']);

                        $notificacaoEntidadePerfil = new \G2\Entity\NotificacaoEntidadePerfil();
                        $notificacaoEntidadePerfil->setId_perfil($perfil);
                        $notificacaoEntidadePerfil->setId_notificacaoentidade($notificacaoEntidade);

                        $this->save($notificacaoEntidadePerfil);
                    }
                } else {
                    //Removendo o perfil da lista, caso o usuario tenha desmarcado o vinculo do mesmo
                    //para a notificacao de uma entidade
                    $find = $this->find('\G2\Entity\NotificacaoEntidadePerfil', $value['id_notificacaoentidadeperfil']);
                    if ($find) {
                        $this->delete($find);
                    }
                }
            }

            return true;

        } catch (\Doctrine\ORM\Exception $e) {
            throw new \Zend_Exception($e->getMessage(), 500);
        }
    }

}

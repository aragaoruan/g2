<?php

namespace G2\Negocio;

use Doctrine\ORM\ORMException;
use G2\Entity\PerfilReferenciaIntegracao;
use G2\Entity\Sistema;
use G2\Entity\Turma;
use G2\Entity\TurmaSala;
use G2\Entity\Usuario;

/**
 * Classe de negócio para Gerenciar Salas
 * @author Debora Castro <debora.castro@unyleya.com.br>;
 */
class GerenciaSalas extends Negocio
{

    private $entidadeNegocio;

    public function __construct()
    {
        parent::__construct();
        $this->entidadeNegocio = new Entidade();
    }

    public function findByProjetoPedagogico($where = array())
    {
        $repositoryName = 'G2\Entity\VwProjetoEntidade';

        try {

            if (!isset($where['id_entidade']))
                $where['id_entidade'] = $this->sessao->id_entidade;

            if (isset($where['search']) && $where['search']) {
                $whereLikes = $where['search'];
                unset($where['search']);

                return $this->findBySearch($repositoryName, $whereLikes, $where, array('st_projetopedagogico' => 'ASC'));
            } else {
                return $this->em->getRepository($repositoryName)->findBy($where, array('st_projetopedagogico' => 'ASC'));
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findBySalaDeAula($params = null)
    {
        try {
            $repositoryName = 'G2\Entity\SalaDeAula';
            return $this->em->getRepository($repositoryName)->findBy($params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByVwAvaliacaoConjuntoReferencia($params = null)
    {
        try {
            $repositoryName = 'G2\Entity\VwAvaliacaoConjuntoReferencia';
            return $this->em->getRepository($repositoryName)->findBy($params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find By Periodo
     * @return object G2\Entity\PeriodoLetivo
     * Retorna dados para o combo de período na pesquisa de gerencia sala
     */
    public function findByPeriodo($params = array())
    {
        try {
            // O filtro de entidade foi RETIRADO na historia AC-26141
            // e READICIONADO em GRA-38
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\PeriodoLetivo';
            return $this->findBy($repositoryName, $params, array('st_periodoletivo' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find By Modalidade
     * @return object G2\Entity\ModalidadeSalaDeAula
     * Retorna dados para o combo de modalidade na tela de Gerencia Sala
     */
    public function findByModalidade()
    {
        try {
            $repositoryName = 'G2\Entity\ModalidadeSalaDeAula';
            return $this->em->getRepository($repositoryName)->findAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find By Tipo Sala de aula
     * @return object G2\Entity\TipoSaladeAula
     * Retorna dados para o combo de tipo na tela de Gerencia Sala
     */
    public function findByTipoSala()
    {
        try {
            $repositoryName = 'G2\Entity\TipoSaladeAula';
            return $this->em->getRepository($repositoryName)->findAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByAvaliacao()
    {
        try {
            $params1['id_tipoprova'] = 2;
            $params1['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwAvaliacaoConjunto';
            return $this->em->getRepository($repositoryName)->findBy($params1);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByProfessor()
    {
        try {
            $params2['bl_ativo'] = true;
            $params2['id_perfilpedagogico'] = 1;
            $params2['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwUsuarioPerfilEntidade';
            return $this->em->getRepository($repositoryName)->findBy($params2, array('st_nomecompleto' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByOrganizacaoDisciplina()
    {
        try {
            $params3['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\Entidade';
            return $this->em->getRepository($repositoryName)->findBY($params3);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByAreaDisciplina($id_entidade)
    {
        try {
            $parametro['id_entidade'] = $id_entidade;
            $repositoryName = 'G2\Entity\AreaConhecimento';
            return $this->em->getRepository($repositoryName)->findBy($parametro);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findBySalasParaCriarPrr($id_entidade)
    {
        try {
            $parametro['id_entidade'] = $id_entidade;
            $repositoryName = 'G2\Entity\VwSalasParaCriarPrr';
            return $this->em->getRepository($repositoryName)->finBy($parametro);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByPesquisaDisciplinaModal($parametros)
    {
        try {

            $repositoryName = 'G2\Entity\VwEntidadeProjetoDisciplina';

            // Montar DQL de buscar disciplinas, dependendo dos filtros passados
            $dql = "SELECT p FROM {$repositoryName} p WHERE p.bl_ativo = 1 ";
            if (isset($parametros['id_entidade']) && trim($parametros['id_entidade']) != '')
                $dql .= "AND p.id_entidade = {$parametros['id_entidade']} ";
            if (isset($parametros['id_serie']) && trim($parametros['id_serie']) != '')
                $dql .= "AND p.id_serie = {$parametros['id_serie']} ";
            if (isset($parametros['st_disciplina']) && trim($parametros['st_disciplina']) != '')
                $dql .= "AND p.st_disciplina LIKE '%{$parametros['st_disciplina']}%' ";
            $dql .= "ORDER BY p.st_disciplina ";

            $result = $this->em->createQuery($dql)->getResult();

            return $result;
            //return $this->em->getRepository($repositoryName)->findBy($params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método que salva várias salas de aula (utilizando método antigo)
     * Os dados foram setados de forma que correspondem que é esperado no método
     * cadastrarArraySalas na SalaDeAulaBO
     * Débora Castro <debora.castro@unyleya.com.br>
     * 29/10/2013
     * @param array $dados
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvaSalaDeAula($dados = array())
    {

        $arrGeral = array();
        $array_organizado = array();

        if (empty($dados['entidades'])) {
            return new \Ead1_Mensageiro('Selecione uma organização', \Ead1_IMensageiro::AVISO);
        }

        if (!$dados['salaDeAulaDisciplina']) {
            return new \Ead1_Mensageiro('Selecione uma Disciplina', \Ead1_IMensageiro::AVISO);
        }

        if (!$dados['salaDeAulaProjeto']) {
            return new \Ead1_Mensageiro('Selecione um Projeto Pedagógico', \Ead1_IMensageiro::AVISO);
        }

        if ($dados['bl_vincularturma'] && !$dados['salaDeAulaTurma']) {
            return new \Ead1_Mensageiro('Selecione uma Turma', \Ead1_IMensageiro::AVISO);
        }

        if (empty($dados['id_moodle'])) {
            return new \Ead1_Mensageiro('Selecione um Moodle', \Ead1_IMensageiro::AVISO);

        }

        $nu_linhas = (int)$dados['nu_linhas'];

        /**
         * history AC-27034
         */
        // Os indices nem sempre estarao em ordem, entao serao descobertos antes do for
        $array_ids = array();
        foreach ($dados as $key => $dado) {
            if (substr($key, 0, 13) == 'st_saladeaula')
                $array_ids[] = substr($key, 13);
        }

        $existeSalaComMesmoNome = array();
        $salaExistenteID = null;

        /* Pesquisa na Vw se existem salas de aula com o mesmo nome.
        Caso positivo, adiciona a Entity resultante em um elemento do array e
        atribui o id à variável $salaExistenteID */

        foreach ($array_ids as $cont => $id) {

            $existeSalaComMesmoNome[$id] = $this->findOneBy('\G2\Entity\VwPesquisaSalaDeAula', array(
                'st_saladeaula' => $dados['st_saladeaula' . $id],
                'id_entidade' => $this->entidadeNegocio->getId_entidade()
            ));

            if ($existeSalaComMesmoNome[$id])
                $salaExistenteID = $id;
        }

        /* Caso $existeSalaComMesmoNome exista, retorna array com dados para montagem do pNotify
        e impede o salvamento da sala. Caso contrário, salva normalmente. */

        if (isset ($salaExistenteID) && !is_null($salaExistenteID)) {
            return array(
                'existeSalaComMesmoNome' => true,
                'title' => 'Impossível salvar sala',
                'type' => 'error',
                'text' => 'Já existe uma sala de aula com o nome <strong>' . $existeSalaComMesmoNome[$salaExistenteID]->getSt_saladeaula() . '</strong>. Altere-o e tente novamente.'
            );
        } else {
            foreach ($array_ids as $cont => $id) {
                $saladeaula = new \SalaDeAulaTO();
                //setando os dados de sala de aula
                $saladeaula->setSt_saladeaula($dados['st_saladeaula' . $id]);
                $saladeaula->setDt_inicioinscricao($dados['dt_inicioinscricao' . $id]);
                $saladeaula->setDt_fiminscricao($dados['dt_fiminscricao' . $id]);
                $saladeaula->setDt_abertura($dados['dt_abertura' . $id]);
                $saladeaula->setDt_encerramento($dados['dt_encerramento' . $id]);


                $saladeaula->setId_modalidadesaladeaula($dados['id_modalidadesaladeaula']);
                $saladeaula->setNu_diasextensao($dados['nu_diasextensao']);
                $saladeaula->setId_tiposaladeaula($dados['id_tiposaladeaula']);
                $saladeaula->setId_periodoletivo($dados['id_periodoletivo']);
                $saladeaula->setBl_usardoperiodoletivo($dados['bl_usardoperiodoletivo']);
                $saladeaula->setId_situacao($dados['id_situacao']);
                $saladeaula->setNu_maxalunos($dados['nu_maxalunos']);
                $saladeaula->setNu_diasaluno($dados['nu_diasaluno']);
                $saladeaula->setId_categoriasala($dados['id_categoriasala']);
                $saladeaula->setId_entidadeintegracao((int)$dados['id_moodle']);

                $saladeaula->setBl_ofertaexcepcional(false);
                //verifica se é uma oferta excepcional
                if (array_key_exists('bl_ofertaexcepcional', $dados) && !empty($dados['bl_ofertaexcepcional'])) {
                    $saladeaula->setBl_ofertaexcepcional(true);
                }

                //Verifica se haverá vinculo com alguma turma
                $arr_turma = array();

                $saladeaula->setBl_vincularturma(false);
                if (array_key_exists('bl_vincularturma', $dados) && !empty($dados['bl_vincularturma'])) {
                    $saladeaula->setBl_vincularturma(true);
                    //setando TurmaSala
                    if (is_array($dados['salaDeAulaTurma'])) {
                        $turmasala = explode(',', $dados['salaDeAulaTurma'][$cont]);
                    } else {
                        $turmasala = explode(',', $dados['salaDeAulaTurma']);
                    }
                    array_push($arr_turma, $turmasala);
                }

//            $saladeaula->setNu_diasextensao(0);
                $saladeaula->setNu_diasencerramento($dados['nu_diasencerramento']);
                $saladeaula->setBl_semencerramento(false);
                if (is_array($dados['bl_semencerramento'])) {
                    $saladeaula->setBl_semencerramento(true);
                }

                $saladeaula->setBl_semdiasaluno(false);
                if (is_array($dados['bl_semdiasaluno'])) {
                    $saladeaula->setBl_semdiasaluno(true);
                }


                //Seta a sala de referencia para o retorno
                $salaIntegracao = new \SalaDeAulaIntegracaoTO();
                $salaIntegracao->setId_disciplinaintegracao((int)$dados['id_disciplinaintegracao' . $id]);

                //setando atributos de disciplina
                $disciplinaSala = new \DisciplinaSalaDeAulaTO();
                if (is_array($dados['salaDeAulaDisciplina'])) {
                    $dis = $dados['salaDeAulaDisciplina'][$cont];
                } else {
                    $dis = $dados['salaDeAulaDisciplina'];
                }
                $disciplinaSala->setId_disciplina($dis);

                //setando usuário referencia
                $usuarioPerfil = new \UsuarioPerfilEntidadeReferenciaTO();
                $usuarioPerfil->setBl_ativo(true);
                $usuarioPerfil->setId_entidade($this->sessao->id_entidade);
                $usuarioPerfil->setBl_titular(true);
                if (isset($dados['id_usuario' . $id]) && !empty($dados['id_usuario' . $id])) {
                    $usu = explode('#', $dados['id_usuario' . $id]);
                    if (is_array($usu)):
                        $usuarioPerfil->setId_perfil($usu[1]);
                        $usuarioPerfil->setId_usuario($usu[0]);
                    endif;
                };

                //setando avaliação
                $avaliacaoConjunto = new \AvaliacaoConjuntoReferenciaTO();
                $ava = $dados['id_avaliacaoconjunto' . $id];
                $avaliacaoConjunto->setId_avaliacaoconjunto($ava);
                $avaliacaoConjunto->setDt_inicio(date("Y-m-d H:i:s"));


                //setando AreaProjetoSala
                if (is_array($dados['salaDeAulaProjeto'])) {
                    $proj = explode(',', $dados['salaDeAulaProjeto'][$cont]);
                } else {
                    $proj = explode(',', $dados['salaDeAulaProjeto']);
                }


                for ($i = 0; $i < count($proj); $i++) {
                    $projetoSala = new \AreaProjetoSalaTO;
                    $projetoSala->setId_projetopedagogico($proj[$i]);
                    $arrProj[$i] = $projetoSala;
                }

                for ($j = 0; $j < count($dados['entidades']); $j++) {
                    $entidadeSala = new \SalaDeAulaEntidadeTO;
                    $entidadeSala->setId_entidade($dados['entidades'][$j]);
                    $arrEnt[$j] = $entidadeSala;
                }


                $arrGeral[$cont] = array();
                $arrGeral[$cont][0] = $saladeaula; //$sala[0] -> SalaDeAulaTO
                $arrGeral[$cont][1] = $disciplinaSala; //$sala[1] -> DisciplinaSalaDeAulaTO
                $arrGeral[$cont][2] = $arrProj; //$sala[2] -> Array de AreaProjetoSalaTO
                $arrGeral[$cont][3] = $usuarioPerfil; //$sala[3] -> UsuarioPerfilEntidadeReferenciaTO
                $arrGeral[$cont][4] = $avaliacaoConjunto; //$sala[4] -> AvaliacaoConjuntoReferenciaTO
                $arrGeral[$cont][5] = $arrEnt; //$sala[5] -> Array de SalaDeAulaEntidadeTO
                $arrGeral[$cont][6] = $salaIntegracao; //sala de aula integracao
                $arrGeral[$cont][7] = $arr_turma; //sala de aula integracao

            };

            $salaBO = new \SalaDeAulaBO;
            $retorno = $salaBO->cadastrarArraySalas($arrGeral);

            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $chave => $salaIn) {
                    $salaIn[0]->id_disciplinaintegracao = $salaIn[6]->getId_disciplinaintegracao();
                    $array_organizado[$chave] = $salaIn[0];
                }
                $mensageiro = new \Ead1_Mensageiro($array_organizado, \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setText('Sala(s) salva(s) com sucesso!');
            } else {
                $mensageiro = new \Ead1_Mensageiro($retorno->getMensagem(), \Ead1_IMensageiro::ERRO);
            }
//              \Zend::dump($mensageiro); die;
            return $mensageiro;
        }

    }

    /**
     * @param $param
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function sincronizarSalas($param)
    {

        try {
            $id_sistema = $param['id_sistema'];
            $array_disciplinaintegracao = $param['id_disciplinaintegracao'];
            $array_salas = array();
            $id_saladeaula = $param['id_saladeaula_sincronizar'];

            if (!$id_saladeaula) {
                throw  new \Exception("Nenhum id de sala de aula informado para sincronizar.");
            }
            $array_integracao = array();
            foreach ($id_saladeaula as $key => $sala) {
                $to = new \SalaDeAulaTO();
                $to->setId_saladeaula($sala);
                $to->fetch(true, true, true);
                $array_salas[] = $to;
                if ($array_disciplinaintegracao && $array_disciplinaintegracao[$key] != 0) {
                    $array_integracao[$sala] = $array_disciplinaintegracao[$key];
                }
            }
            $retorno = $this->integraSalas($array_salas, $id_sistema, $array_integracao);
            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Salas Integradas com Sucesso!', \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro($retorno->getMensagem(), \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return $mensageiro;
    }

    public function integraSalas($salas, $id_sistema, $array_integracao = false)
    {
        $salaBO = new \SalaDeAulaBO;
        $retorno = $salaBO->wsSalasDeAula($salas, (int)$id_sistema, $array_integracao);
        return $retorno;
    }

    /**
     * Método para criar salas de PRR automáticamente
     * Ativado pelo ROBO
     */
    public function criarSalasPRRLote($id_entidade)
    {

        $repositoryName = 'G2\Entity\VwSalasParaCriarPrr';
        $salas = $this->em->getRepository($repositoryName)->findAll();

        $arrGeral = array();
        $arrProj = array();
        $array_organizado = array();
        $i = 0;
        if ($salas) {
            foreach ($salas as $dados) {
                $saladeaula = new \SalaDeAulaTO();

                $saladeaula->setSt_saladeaula($dados->getSt_saladeaula());
                $saladeaula->setDt_inicioinscricao($dados->getDt_inicioinscricao());
                $saladeaula->setDt_fiminscricao($dados->getDt_fiminscricao());
                $saladeaula->setDt_abertura($dados->getDt_abertura());
                $saladeaula->setDt_encerramento($dados->getDt_encerramento());
                $saladeaula->setId_categoriasala($dados->getId_categoriasala());
                $saladeaula->setId_modalidadesaladeaula($dados->getId_modalidadesaladeaula());
                $saladeaula->setNu_diasextensao($dados->getNu_diasextensao());
                $saladeaula->setId_tiposaladeaula($dados->getId_tiposaladeaula());
                $saladeaula->setId_periodoletivo($dados->getId_periodoletivo());
                $saladeaula->setBl_usardoperiodoletivo($dados->getBl_usardoperiodoletivo());
                $saladeaula->setId_situacao($dados->getId_situacao());
                $saladeaula->setNu_maxalunos($dados->getNu_maxalunos());
                $saladeaula->setNu_diasaluno($dados->getNu_diasaluno());
                $saladeaula->setNu_diasencerramento($dados->getNu_diasencerramento());
                $saladeaula->setBl_semencerramento($dados->getBl_semencerramento());
                $saladeaula->setBl_semdiasaluno($dados->getBl_semdiasaluno());

                $disciplinaSala = new \DisciplinaSalaDeAulaTO();
                $disciplinaSala->setId_disciplina($dados->getId_disciplina());

                $usuarioPerfil = new \UsuarioPerfilEntidadeReferenciaTO();
                $usuarioPerfil->setBl_ativo(1);
                $usuarioPerfil->setId_entidade($id_entidade);
                $usuarioPerfil->setBl_titular(1);

                $projetoSala = new \AreaProjetoSalaTO;
                $projetoSala->setId_projetopedagogico($dados->getId_projetopedagogico());
                $arrProj[0] = $projetoSala;

                $entidadeSala = new \SalaDeAulaEntidadeTO;
                $entidadeSala->setId_entidade($id_entidade);
                $arrEnt[0] = $entidadeSala;

                $avaliacaoConjunto = new \AvaliacaoConjuntoReferenciaTO();
                $avaliacaoConjunto->setId_avaliacaoconjunto($dados->getId_avaliacaoconjunto());
                $avaliacaoConjunto->setDt_inicio(date("Y-m-d H:i:s"));

                $arrGeral[$i] = array();
                $arrGeral[$i][0] = $saladeaula; //$sala[0] -> SalaDeAulaTO
                $arrGeral[$i][1] = $disciplinaSala; //$sala[1] -> DisciplinaSalaDeAulaTO
                $arrGeral[$i][2] = $arrProj; //$sala[2] -> Array de AreaProjetoSalaTO
                $arrGeral[$i][3] = $usuarioPerfil; //$sala[3] -> UsuarioPerfilEntidadeReferenciaTO
                $arrGeral[$i][4] = $avaliacaoConjunto; //$sala[4] -> AvaliacaoConjuntoReferenciaTO
                $arrGeral[$i][5] = $arrEnt; //$sala[5] -> Array de SalaDeAulaEntidadeTO
                $i++;
            }
//                \Zend_Debug::dump($arrGeral);die;

            $salaBO = new \SalaDeAulaBO;
            $retorno = $salaBO->cadastrarArraySalas($arrGeral);

            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                foreach ($retorno->getMensagem() as $chave => $salaIn) {
                    $array_organizado[$chave] = $salaIn[0];
                }
                $retorno1 = $this->integraSalas($array_organizado, 6);

                if ($retorno1->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $mensageiro = new \Ead1_Mensageiro(count($array_organizado) . ' Salas Salvas e Integradas com Sucesso!', \Ead1_IMensageiro::SUCESSO);
                } else {
                    $mensageiro = new \Ead1_Mensageiro($retorno->getMensagem() . '-' . $retorno1->getMensagem(), \Ead1_IMensageiro::ERRO);
                }
            }
        } else {
            $mensageiro = new \Ead1_Mensageiro('Não há salas para integrar', \Ead1_IMensageiro::SUCESSO);
        }

        return $mensageiro;
    }


    /**
     * @param array $dados
     * @param \G2\Entity\SalaDeAula $saladeaula
     * @return \G2\Entity\SalaDeAula
     */
    private function setDadosEntity(array $dados, \G2\Entity\SalaDeAula $saladeaula)
    {

        $nu_diasencerramento = null;
        $bl_todasentidades = false;
        $nu_diasaluno = 0;
        $id_modalidadesaladeaula = null;

        if (isset($dados['nu_diasencerramento']) && !empty($dados['nu_diasencerramento']))
            $nu_diasencerramento = $dados['nu_diasencerramento'];

        if (isset($dados['bl_todasentidades']))
            $bl_todasentidades = $dados['bl_todasentidades'];

        if (isset($dados['nu_diasaluno']) && !empty($dados['nu_diasaluno']))
            $nu_diasaluno = $dados['nu_diasaluno'];

        if (!empty($dados['id_modalidadesaladeaula']))
            $id_modalidadesaladeaula = $this
                ->getReference(\G2\Entity\ModalidadeSalaDeAula::class, $dados['id_modalidadesaladeaula']);


        $saladeaula->setDt_cadastro(new \DateTime())
            ->setSt_saladeaula($dados['st_saladeaula'])
            ->setBl_ativa(true)
            ->setNu_diasaluno($nu_diasaluno)
            ->setNu_diasextensao(0)
            ->setId_entidade($this->getId_entidade())
            ->setNu_diasencerramento($nu_diasencerramento)
            ->setId_modalidadesaladeaula($id_modalidadesaladeaula)
            ->setId_usuariocadastro($this->sessao->id_usuario)
            ->setBl_todasentidades($bl_todasentidades);

        if (!empty($dados['nu_maxalunos'])) {
            $saladeaula->setNu_maxalunos($dados['nu_maxalunos']);
        }

        if (!empty($dados['bl_semencerramento'])) {
            $saladeaula->setBl_semencerramento($dados['bl_semencerramento']);
        } else {
            $saladeaula->setBl_semencerramento(false);
        }

        if (!empty($dados['bl_semdiasaluno'])) {
            $saladeaula->setBl_semdiasaluno($dados['bl_semdiasaluno']);
        } else {
            $saladeaula->setBl_semdiasaluno(false);
        }

        if (!empty($dados['bl_ofertaexcepcional'])) {
            $saladeaula->setbl_ofertaexcepcional($dados['bl_ofertaexcepcional']);
        }

        if (!is_null($dados['bl_vincularturma'])) {
            $saladeaula->setBl_vincularturma($dados['bl_vincularturma']);
        }

        if (!empty($dados['dt_inicioinscricao'])) {
            $saladeaula->setDt_inicioinscricao($this->converteDataBanco($dados['dt_inicioinscricao']));
        }

        if (isset($dados['dt_fiminscricao']) && $dados['dt_fiminscricao']) {
            $saladeaula->setDt_fiminscricao($this->converteDataBanco($dados['dt_fiminscricao']));
        }

        if (!empty($dados['dt_abertura'])) {
            $saladeaula->setDt_abertura($this->converteDataBanco($dados['dt_abertura']));
        }

        if (isset($dados['dt_encerramento']) && $dados['dt_encerramento']) {
            $saladeaula->setDt_encerramento($this->converteDataBanco($dados['dt_encerramento']));
        }


        if (isset($dados['id_periodoletivo']) && $dados['id_periodoletivo']) {
            $saladeaula->setId_periodoletivo($this->getReference(\G2\Entity\PeriodoLetivo::class, $dados['id_periodoletivo']));
            $saladeaula->setBl_usardoperiodoletivo(true);
        } else {
            $saladeaula->setBl_usardoperiodoletivo(false);
        }

        if (!empty($dados['id_tiposaladeaula'])) {
            $saladeaula->setId_tiposaladeaula($this->getReference(\G2\Entity\TipoSaladeAula::class, $dados['id_tiposaladeaula']));
        }

        if (!empty($dados['id_situacao'])) {
            $saladeaula->setId_situacao($this->getReference(\G2\Entity\Situacao::class, $dados['id_situacao']));
        }


        if (!empty($dados['id_categoriasala']))
            $saladeaula->setId_categoriasala($this->getReference(\G2\Entity\CategoriaSala::class, $dados['id_categoriasala']));


        if (array_key_exists("id_entidadeintegracao", $dados) && !empty($dados['id_entidadeintegracao'])) {
            $saladeaula->setId_entidadeintegracao($this
                ->getReference(\G2\Entity\EntidadeIntegracao::class, $dados['id_entidadeintegracao']));
        }

        return $saladeaula;
    }


    /**
     * Verifica se a sala de aula já existe com o nome passado
     * @param $dados
     * @return bool
     * @throws \Exception
     */
    private function verificarSalaExistente($dados)
    {
        try {
            $excludeCriteria = [
                'st_saladeaula' => $dados['st_saladeaula'],
                'id_entidade' => $this->getId_entidade()
            ];

            if (array_key_exists('id_saladeaula', $dados) && !empty($dados['id_saladeaula'])) {
                $excludeCriteria["id_saladeaula"] = "!= {$dados['id_saladeaula']}";
            }

            $existeSalaComMesmoNome = $this->findCustom(\G2\Entity\SalaDeAula::class, $excludeCriteria);

            /* Verifica se existem salas idênticas. Caso positivo, retorna como resposta um JSON com os dados
            necessários para montar o pnotify no Javascript e não salva.
            Caso contrário, salva a sala normalmente.
            */
            if (!$existeSalaComMesmoNome) {
                return false;
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return true;
    }

    /**
     * Método para salvar os dados basicos da sala de aula
     * Tela Pedagogico -> Sala de Aula
     *
     * @atualização Neemias Santos <neemias.santos@unyleya.com.br>
     * @param $dados
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarSalaDeAulaDadosBasicos($dados)
    {

        $saladeaula = new \G2\Entity\SalaDeAula();
        if (array_key_exists('id_saladeaula', $dados) && !empty($dados['id_saladeaula'])) {
            $saladeaula = $this->find('\G2\Entity\SalaDeAula', $dados['id_saladeaula']);
            if (!$saladeaula) {
                throw  new \Exception("Sala de aula com o id informado não encontrada.");
            }
        }

        if ($this->verificarSalaExistente($dados)) {
            return array(
                'existeSalaComMesmoNome' => true,
                'title' => 'Impossível salvar sala',
                'type' => 'error',
                'text' => 'Já existe uma sala de aula com este mesmo nome. Altere-o e tente novamente.'
            );
        }


        try {
            $this->beginTransaction();

            $saladeaula = $this->setDadosEntity($dados, $saladeaula);

            /** @var \G2\Entity\SalaDeAula $retornoSalaDeAula */
            $retornoSalaDeAula = $this->save($saladeaula);
            $retornoSalaDeAulaArray = $retornoSalaDeAula->toBackboneArray();
            $this->commit();

            //$retornoSalaDeAulaArray['array_coordenadores'] = null;
            $mensageiro = new \Ead1_Mensageiro($retornoSalaDeAulaArray, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Sala de aula salva com sucesso!');


        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao salvar a sala de aula. " . $e->getMessage());
        }

        /* Vinculando sala de aula com a disciplina */
        try {

            /*
             * Verifica se existe alguma sala de aula vinculado a este Id_saladeaula
             * altera ou insere um novo.
             */
            if (!$retornoSalaDeAula->getId_saladeaula()) {
                return $this->mensageiro->setMensageiro('Erro ao Cadastrar Sala de Aula, O Id da Sala de aula não foi definido.', Ead1_IMensageiro::ERRO, $this->dao->excecao);
            }

            if (!empty($dados['id_disciplina'])) {
                $disciplinaSala = $this->findOneBy('\G2\Entity\DisciplinaSalaDeAula', array(
                    'id_saladeaula' => $retornoSalaDeAula->getId_saladeaula()
                ));

                if (!$disciplinaSala) {
                    $disciplinaSala = new \G2\Entity\DisciplinaSalaDeAula();
                }

                $this->beginTransaction();

                $disciplinaSala->setId_disciplina($dados['id_disciplina']);
                $disciplinaSala->setId_saladeaula($retornoSalaDeAula->getId_saladeaula());
                $this->save($disciplinaSala);

                $this->commit();
            }
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao vincular a disciplina na sala de aula. " . $e->getMessage());
        }

        if (isset($dados['id_avaliacaoconjunto']) && $dados['id_avaliacaoconjunto']) {
            /*Salva o conjunto de avaliação */
            try {
                $this->beginTransaction();
                //setando avaliação

                $avaliacaoConjunto = new \G2\Entity\AvaliacaoConjuntoReferencia();
                $ava = $dados['id_avaliacaoconjunto'];
                $avaliacaoConjunto->setId_avaliacaoconjunto($ava);
                $avaliacaoConjunto->setDt_inicio(new \DateTime());
                $avaliacaoConjunto->setId_saladeaula($retornoSalaDeAula->getId_saladeaula());
                $retornoAvaliacaoConjunto = $this->save($avaliacaoConjunto);
                $this->commit();

            } catch (\Exception $e) {
                $this->rollback();
                throw new \Zend_Exception('Erro ao configurar conjunto de avaliação.' . $e->getMessage());
            }
        }

        return $mensageiro;
    }

    /**
     * Método utilizado no menu Pedagogico->Sala de Aula
     * responsável pelo cadastro do bloco de permissões
     * Projetos Pedagogicos, Professores e Entidades
     */
    public function salvarSalaDeAulaPermissoes($param)
    {
        try {

            if (empty($param['id_entidade']) && $param['bl_todasentidades'] != 1) {
                $mensageiro = new \Ead1_Mensageiro('Selecione uma organização', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }

            $salvarEntidade = $this->vincularSalaDeAulaEntidade($param);

            if ($salvarEntidade->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                $salvarProjeto = $this->vincularSalaProjetos($param);

                if ($salvarProjeto->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                    $salvarProfessor = $this->vincularSalaProfessor($param);

                    if ($salvarProfessor->getTipo() != \Ead1_IMensageiro::SUCESSO) {

                        THROW new \Zend_Exception('Erro ao salvar Professores!');
                    }
                } else {
                    THROW new \Zend_Exception('Erro ao salvar Projetos!');
                }
            } else {
                THROW new \Zend_Exception('Erro ao salvar Entidades!');
            }


            $mensageiro = new \Ead1_Mensageiro('Permissões salvas com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /*
     * Método utilizado no menu Pedagogico->Sala de Aula
     * responsável pelo cadastro de valor do produto, caso seja prr
     * @params - id_saladeaula, dt_inicio, dt_fim, nu_ valor
     */

    /**
     * Remoção do método de save com as BO's e utilização do Doctrine para o save
     * Mudança feita para a funcionalidade gerar log
     *
     * @atualização Neemias Santos <neemias.santos@unyleya.com.br>
     * @param $param
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function vincularSalaDeAulaEntidade($param)
    {
        try {
            if ($param['bl_todasentidades'] != 1) { //salvar relação sala e entidades

                $this->beginTransaction();
                foreach ($param['id_entidade'] as $value) {
                    $entidadeSala = new \G2\Entity\SalaDeAulaEntidade();
                    $entidadeSala->setId_entidade($value);
                    $entidadeSala->setId_saladeaula($param['id_saladeaula']);
                    $this->save($entidadeSala);
                }
                $this->beginTransaction();
                $saladeaula = $this->find('\G2\Entity\SalaDeAula', $param['id_saladeaula']);
                $saladeaula->setBl_todasentidades(0);
                $saladeaula->setDt_atualiza(new \DateTime());
                $saladeaula->setId_usuarioatualiza($this->sessao->id_usuario);
                $this->save($saladeaula);
                $this->commit();

                $this->commit();
                $mensageiro = new \Ead1_Mensageiro('Sala de Aula Editada com Sucesso!', \Ead1_IMensageiro::SUCESSO);
            } else {
                //caso seja setado o todas entidades, apenas é alterado esse parâmetro na tabela tb_saladeaula
                $this->beginTransaction();
                $saladeaula = $this->find('\G2\Entity\SalaDeAula', $param['id_saladeaula']);
                $saladeaula->setBl_todasentidades(1);
                $saladeaula->setDt_atualiza(new \DateTime());
                $saladeaula->setId_usuarioatualiza($this->sessao->id_usuario);
                $this->save($saladeaula);
                $this->commit();
                $mensageiro = new \Ead1_Mensageiro('Sala de Aula Editada com Sucesso!', \Ead1_IMensageiro::SUCESSO);
            }
            return $mensageiro;
        } catch (\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao vincular Sala De Aula Entidade. ' . $e->getMessage());
        }
    }

    /*
     * Método utilizado no menu Pedagogico->Sala de Aula
     * responsável pela integração da sala
     * @params - id_saladeaula e id_sistema
     */

    /**
     * Refatoração do métodos Bo's para Doctrine
     * @param array $param
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     * @author Neemias Santos <neemias.santos@unyleya.com.br>
     * @author Kayo Silva <kayo.silva@unyleya.com.br> 2015-09-10
     */
    public function vincularSalaProjetos($param)
    {
        try {
            $this->beginTransaction();
            $projetos = $param['projetos'];
            $mensageiro = new \Ead1_Mensageiro();
            if (!empty($projetos)) {
                foreach ($projetos as $values) {

                    //Se não existir ID é porque o projeto pedagogico ainda não faz parte da sala de aula
                    if (!array_key_exists('id', $values) || (array_key_exists('id', $values) && empty($values['id']))) {
                        $projetoSala = new \G2\Entity\AreaProjetoSala();

                        $projetoSala->setId_projetopedagogico($values['id_projetopedagogico']);
                        $projetoSala->setId_nivelensino(isset($param['id_nivelensino']) ? $param['id_nivelensino'] : null);
                        $projetoSala->setId_saladeaula($param['id_saladeaula']);
                        $projetoSala->setId_areaconhecimento(isset($param['id_areaconhecimento']) ? $param['id_areaconhecimento'] : null);
                        $projetoSala->setSt_referencia(isset($param['st_referencia']) ? $param['st_referencia'] : null);
                        $projetoSala->setNu_diasacesso(isset($values['nu_diasacesso']) ? $values['nu_diasacesso'] : null);
                        $retornoProjetoSala = $this->save($projetoSala);
                        $mensageiro = new \Ead1_Mensageiro('vincular Sala projeto efetuado com sucesso', \Ead1_IMensageiro::SUCESSO);
                    }
                }

            } else {
                Throw new \Zend_Exception('Não foi setado nenhum projeto para o vínculo.! ');
            }
            $this->commit();
            return $mensageiro;
        } catch (\ORMException $e) {
            $this->rollback();
            Throw new \Zend_Exception('Erro ao salvar area projeto sala! ' . $e->getMessage());
        }
    }

    /**
     * Atualização do método para gerar log usando doctrine
     * @param $param
     * @return \Ead1_Mensageiro
     * @throws Zend_Exception
     * @throws \Zend_Exception
     */
    public function vincularSalaProfessor($param)
    {

        $profTitular = explode('#', $param['professor_titular']);

        if (isset($param['idProfessores']) && $param['idProfessores']) { //salva a relação de professores e sala de aula
            $this->beginTransaction();
            try {
                foreach ($param['idProfessores'] as $value) {

                    $usu = explode('#', $value);

                    /* Verifica se já existe um cadastro para aquela sala com esse professor e edita  ou cria uma nova */
                    $usuarioPerfil = $this->findOneBy('\G2\Entity\UsuarioPerfilEntidadeReferencia', array(
                        'id_entidade' => $this->sessao->id_entidade,
                        'id_saladeaula' => $param['id_saladeaula'],
                        'id_usuario' => $usu[0]
                    ));
                    if (!$usuarioPerfil) {
                        $usuarioPerfil = new \G2\Entity\UsuarioPerfilEntidadeReferencia();
                    }
                    $usuarioPerfil->setBl_ativo(1);
                    $usuarioPerfil->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
                    if ((int)$profTitular[0] == (int)$usu[0]) {
                        $usuarioPerfil->setBl_titular(1);
                    } else {
                        $usuarioPerfil->setBl_titular(0);
                    }
                    $usuarioPerfil->setId_saladeaula($this->getReference('\G2\Entity\SalaDeAula', $param['id_saladeaula']));
                    if (isset($param['idProfessores']) && !empty($param['idProfessores'])) {
                        if (is_array($usu)):
                            $usuarioPerfil->setId_perfil($this->getReference('\G2\Entity\Perfil', $usu[1]));
                            $usuarioPerfil->setId_usuario($this->getReference('\G2\Entity\Usuario', $usu[0]));
                        endif;
                    }
                    $retornoUsuarioPerfil = $this->save($usuarioPerfil);


                    $uperTO = $this->entityToTO($retornoUsuarioPerfil);
                    $professorSala = new \SalaDeAulaProfessorTO();
                    if ((int)$profTitular[0] == (int)$usu[0]) {
                        $professorSala->setBl_titular(1);
                    } else {
                        $professorSala->setBl_titular(0);
                    }
                    $professorSala->setId_saladeaula((int)$param['id_saladeaula']);

                    $orm = new \SalaDeAulaIntegracaoORM();
                    $salaDeAulaIntegracaoTO = $orm->consulta(new \SalaDeAulaIntegracaoTO(array('id_saladeaula' => $uperTO->getId_saladeaula())), false, true);
                    if ($salaDeAulaIntegracaoTO) {
                        switch ($salaDeAulaIntegracaoTO->getId_sistema()) {
                            case \SistemaTO::ACTOR:
                                $this->_processoIntegrarProfessorActor($uperTO, $professorSala);
                                break;
                            case \SistemaTO::MOODLE:
                                $this->_processoIntegrarProfessorMoodle($uperTO->getId_usuario(), $salaDeAulaIntegracaoTO->getId_saladeaula());
                                break;
                            case \SistemaTO::BLACKBOARD:
                                $this->_processoIntegrarProfessorBlackBoard($uperTO, $salaDeAulaIntegracaoTO);
                                break;
                            default:
                                break;
                        }
                    }
                };
                $this->commit();
                return new \Ead1_Mensageiro('vinculo Sala Professor com Sucesso!', \Ead1_IMensageiro::SUCESSO);
            } catch (\Exception $e) {
                $this->rollBack();
                throw new \Zend_Exception('Erro ao vincular Sala Professor. ' . $e->getMessage());
            }
        };
    }


    /*
     * Método para editar os dados basicos da sala de aula
     * Tela Pedagogico -> Sala de Aula
     */

    /**
     * Processo que Integra o Professor ao Actor
     * @param UsuarioPerfilEntidadeReferenciaTO $uperTO
     * @param SalaDeAulaProfessorTO $sdapTO
     * @throws Zend_Exception
     */
    public function _processoIntegrarProfessorActor(UsuarioPerfilEntidadeReferenciaTO $uperTO, SalaDeAulaProfessorTO $sdapTO)
    {
        try {
            $papelWebServices = new \PapelWebServices();
            $retornoWS = $papelWebServices->vincularPapelProfessor($uperTO, $sdapTO);
            //Atualiza Papel se já existir
            if ($retornoWS['retorno'] == 'aviso' && $retornoWS['papel']['codpapel'] != '') {
                $retornoWS = $papelWebServices->atualizaPapel(array(
                    'codpapel' => $retornoWS['papel']['codpapel']
                , 'flastatus' => 'A'
                , 'flamaster' => ($sdapTO->bl_titular ? 'S' : 'N')
                ));
            }
            $perfilReferenciaIntegracaoTO = new \PerfilReferenciaIntegracaoTO();
            $perfilReferenciaIntegracaoTO->setId_perfilreferencia($uperTO->id_perfilreferencia);
            $perfilReferenciaIntegracaoTO->setId_sistema(SistemaTO::ACTOR);
            $perfilReferenciaIntegracaoTO->setId_usuariocadastro($uperTO->id_usuario);
            $perfilReferenciaIntegracaoTO->setSt_codsistema($retornoWS['papel']['codpapel']);
            $perfilReferenciaIntegracaoTO->fetch(false, true, true);
            //Verifica se o Usuario Ja tem Papel
            if (!$perfilReferenciaIntegracaoTO->id_perfilreferenciaintegracao) {
                $perfilReferenciaIntegracaoORM = new \PerfilReferenciaIntegracaoORM();
                $perfilReferenciaIntegracaoORM->insert($perfilReferenciaIntegracaoTO->toArrayInsert());
            }
        } catch (\Zend_Exception $e) {
            throw new \Zend_Exception('Erro ao executar as rotinas no WebService: ' . $e->getMessage());
        }
    }

    /**
     * Processo que Integra o Professor ao Moodle
     * @param int $idUsuario
     * @param int $idSalaDeAula
     * @throws \Exception
     */
    public function _processoIntegrarProfessorMoodle($idUsuario, $idSalaDeAula)
    {
        try {
            /** @var \G2\Entity\SalaDeAula $sala */
            $sala = $this->find(\G2\Entity\SalaDeAula::class, $idSalaDeAula);

            if (!$sala) {
                throw new \Exception("Sala de Aula não encontrada.");
            }

            if (!$sala->getId_entidadeintegracao()) {
                throw new \Exception("Entidade Integração não configurada.");
            }

            $ws = new \MoodleAlocacaoWebServices(null, $sala->getId_entidadeintegracao()->getId_entidadeintegracao());
            $roleId = \MoodleAlocacaoWebServices::ROLEID_PROFESSOR;
            $mensageiro = $ws->cadastrarPapel(
                $idUsuario,
                $idSalaDeAula,
                $roleId,
                null,
                false,
                'processoIntegrarProfessorMoodle');
            if ($mensageiro->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception($mensageiro->getText());
            }

            $this->vincularPerfilProfessorIntegracao($idSalaDeAula, $idUsuario, $roleId);

            $mensageiro->subtractMensageiro();
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Vincula os dados do professor com a tabela tb_perfilreferenciaintegracao
     * @param $idSala
     * @param $idUsuario
     * @param $roleId
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function vincularPerfilProfessorIntegracao($idSala, $idUsuario, $roleId)
    {
        try {

            $idSistema = \G2\Constante\Sistema::MOODLE;
            /** @var \G2\Entity\UsuarioPerfilEntidadeReferencia $uper */
            $uper = $this->findOneBy(\G2\Entity\UsuarioPerfilEntidadeReferencia::class, [
                "id_saladeaula" => $idSala,
                "id_usuario" => $idUsuario,
                "bl_ativo" => true
            ]);

            /** @var \G2\Entity\SalaDeAulaIntegracao $saladeAulaIntegracao */
            $saladeAulaIntegracao = $this->findOneBy(\G2\Entity\SalaDeAulaIntegracao::class, [
                "id_saladeaula" => $idSala,
                "id_sistema" => $idSistema
            ]);

            if (!$uper || !$saladeAulaIntegracao) {
                throw new \Exception("Usuario Perfil Entidade Referência ou Sala de Aula Integração não encontrado.");
            }

            $perfilReferenciaIntegracao = $this->findOneBy(PerfilReferenciaIntegracao::class, [
                "id_sistema" => $idSistema,
                "id_perfilreferencia" => $uper->getId_perfilreferencia(),
                "id_usuariocadastro" => $idUsuario,
                "id_saladeaulaintegracao" => $saladeAulaIntegracao->getId_saladeaulaintegracao()
            ]);

            if (!$perfilReferenciaIntegracao) {
                $perfilReferenciaIntegracao = new PerfilReferenciaIntegracao();
                $perfilReferenciaIntegracao->setId_sistema($this->getReference(Sistema::class, $idSistema))
                    ->setId_perfilreferencia($this->getReference(\G2\Entity\UsuarioPerfilEntidadeReferencia::class,
                        $uper->getId_perfilreferencia()))
                    ->setId_usuariocadastro($this->getReference(Usuario::class, $idUsuario))
                    ->setSt_codsistema($roleId)
                    ->setId_saladeaulaintegracao($saladeAulaIntegracao)
                    ->setDt_cadastro(new \DateTime())
                    ->setBl_encerrado(false);

                if ($this->save($perfilReferenciaIntegracao)) {
                    return new \Ead1_Mensageiro("Perfil Referencia Integração salvo com sucesso!");
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param UsuarioPerfilEntidadeReferenciaTO $up
     * @param SalaDeAulaIntegracaoTO $si
     */
    public function _processoIntegrarProfessorBlackBoard(UsuarioPerfilEntidadeReferenciaTO $up, SalaDeAulaIntegracaoTO $si)
    {
        $bb = new \BlackBoardBO($up->getId_entidade());
        $mensageiro = $bb->processoCadastrarProfessor($up, $si);
        $mensageiro->subtractMensageiro();
    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function salvarIntegracaoSalaDeAula($dados)
    {

        $sala = new \SalaDeAulaTO();
        $sala->setId_saladeaula($dados['idSalaIntegrar']);
        $sala->fetch(true, true, true);


        $id_disciplinaintegracao = array_key_exists('id_disciplinaintegracao', $dados)
        && !empty($dados['id_disciplinaintegracao']) ? $dados['id_disciplinaintegracao'] : false;

        if (!$id_disciplinaintegracao) {
            return new \Ead1_Mensageiro('A sala de referência deve ser selecionada. 
                        Vá até o cadastro da disciplina vinculada e atualize-o.'
                , \Ead1_IMensageiro::AVISO);
        }

        $salaBO = new \SalaDeAulaBO;
        $retorno = $salaBO->wsSalaDeAula($sala, (int)$dados['id_sistema'], $id_disciplinaintegracao);
        if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $salaIntegracao = new \SalaDeAulaIntegracaoTO();
            $salaIntegracao->setId_saladeaula($dados['idSalaIntegrar']);
            $salaIntegracao->fetch(false, true, true);
            $retorno->setMensagem($salaIntegracao->getSt_codsistemacurso());
        }

        return $retorno;
    }

    /*
     * Método utilizado no menu Pedagogico->Sala de Aula
     * responsável pela edição do bloco de permissões
     * Projetos Pedagogicos, Professores e Entidades
     */

    /**
     * Faz a reintegração de uma Sala de Aula
     * @param unknown_type $id_saladeaula
     * @param unknown_type $id_sistema
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function reintegrarSalaDeAula($id_saladeaula, $id_sistema)
    {

        try {

            $sbo = new \SalaDeAulaBO();

            if (!$id_saladeaula) {
                throw new \Zend_Exception("Sala não informada");
            }

            if (!$id_sistema) {
                throw new \Zend_Exception("Sistema não informado");
            }

            $sl = new \SalaDeAulaTO();
            $sl->setId_saladeaula($id_saladeaula);
            $sl->fetch(true, true, true);

            $si = new \SalaDeAulaIntegracaoTO();
            $si->setId_saladeaula($id_saladeaula);
            $si->setId_sistema($id_sistema);
            $si->fetch(false, true, true);

            switch ($id_sistema) {
                case \SistemaTO::BLACKBOARD:


                    $bo = new \BlackBoardBO($sl->getId_entidade());
                    $retorno = $bo->getCourse($bo->prefix_sala . $id_saladeaula);
                    if ($retorno->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                        throw new \Zend_Exception($retorno->getMensagem());
                    }

                    if ($retorno->getMensagem('Courses') == true) {

                        $curso = $retorno->getMensagem('Courses');

                        $si->setSt_retornows(json_encode($curso));
                        $si->setSt_codsistemacurso($curso['courseId']);
                        $si->setSt_codsistemasala($curso['courseId']);

                        $mensageiro = $sbo->editarSalaDeAulaIntegracao($si);
                        if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            return new \Ead1_Mensageiro($mensageiro->getFirstMensagem(), \Ead1_IMensageiro::ERRO);
                        }

                        return new \Ead1_Mensageiro("Sala de Aula encontrada no BlackBoard com o código \"" . $si->getSt_codsistemacurso() . "\", favor ir até o Menu Integrações para continuar", \Ead1_IMensageiro::SUCESSO);
                    }

                    if ($si->getSt_codsistemacurso() == 'AGUARDANDOBLACKBOARD') {
                        return new \Ead1_Mensageiro("Essa sala ainda não foi integrada no Menu Integrações", \Ead1_IMensageiro::AVISO);
                    }

                    if ($retorno->getMensagem('Courses') == false) {

                        $json = '{"verificacao":0}';
                        $si->setSt_retornows($json);
                        $si->setSt_codsistemacurso('AGUARDANDOBLACKBOARD');

                        $mensageiro = $sbo->editarSalaDeAulaIntegracao($si);
                        if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            return new \Ead1_Mensageiro($mensageiro->getFirstMensagem(), \Ead1_IMensageiro::ERRO);
                        }
                    } else {
                        return new \Ead1_Mensageiro("Essa sala já existe no BlackBoard com o ID: " . $si->getSt_codsistemacurso(), \Ead1_IMensageiro::AVISO);
                    }


                    break;
                default:
                    throw new \Zend_Exception("Sistema não implementado");
                    break;
            }

            return new \Ead1_Mensageiro("Sala de Aula reintegrada com sucesso no status \"" . $si->getSt_codsistemacurso() . "\", favor ir até o Menu Integrações para continuar", \Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new \Ead1_Mensageiro("Erro ao reintegrar a Sala de Aula: " . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function editarSalaDeAulaDadosBasicos($dados)
    {
        try {
            $mensageiroAva = $this->editarAvaliacao($dados);
            if ($mensageiroAva->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return $mensageiroAva;
            }
            $mensageiroDisciplina = $this->editarDisciplina($dados);
            if ($mensageiroDisciplina->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return $mensageiroDisciplina;
            }
            $mensageiro = $this->editarSalaDeAula($dados);

            return $mensageiro;
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);

            return $mensageiro;
        }
    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function editarAvaliacao($dados)
    {

        $avaliacaoConjunto = new \AvaliacaoConjuntoReferenciaTO();
        $avaliacaoConjunto->setId_saladeaula($dados['id_saladeaula']);
        $avaliacaoConjunto->fetch(false, true, true);

        //verifica se já existe um avaliacao conjunto cadastrado e se foi alterado para evitar
        //chamadas desnecessárias.
        $avaliConjunto = $avaliacaoConjunto->getId_avaliacaoconjunto();

        if (isset($dados['id_avaliacaoconjunto']) && $avaliConjunto != $dados['id_avaliacaoconjunto']) {
            $avalReferencia = $avaliacaoConjunto->getId_avaliacaoconjuntoreferencia();

            $boAva = new \AvaliacaoBO();

            if (!empty($avalReferencia)) {
                $AvaliacaoAluno = new \AvaliacaoAlunoTO();
                $AvaliacaoAluno->setId_avaliacaoconjuntoreferencia($avalReferencia);
                $AvaliacaoAluno->fetch(false, true, true);

                if ($AvaliacaoAluno->getId_avaliacaoaluno()) {
                    $mensageiro = new \Ead1_Mensageiro('Não é possível fazer alterar a AVALIAÇÃO, pois existem notas de alunos lançadas !', \Ead1_IMensageiro::AVISO);
                    return $mensageiro;
                }
                //caso exista uma avaliação conjunto, primeiro é preciso deletá-la
                $deletar = $boAva->deletarAvaliacaoConjuntoReferencia($avaliacaoConjunto);
                if ($deletar->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $avaNovo = new \AvaliacaoConjuntoReferenciaTO();
                    $avaNovo->setId_saladeaula($dados['id_saladeaula']);
                    $avaNovo->setId_avaliacaoconjunto($dados['id_avaliacaoconjunto']);
                    $avaNovo->setDt_inicio(date("Y-m-d H:i:s"));
                    $editaAvaliacao = $boAva->cadastrarAvaliacaoConjuntoReferencia($avaNovo);
                } else {
                    $mensageiro = new \Ead1_Mensageiro('Não foi possível editar o conjunto avaliação!', \Ead1_IMensageiro::AVISO);
                    return $mensageiro;
                }
            } else {
                //caso ainda não exista um avaliacação conjunto apenas é cadastrado um novo
                $avaliacaoConjunto->setId_avaliacaoconjunto($dados['id_avaliacaoconjunto']);
                $avaliacaoConjunto->setDt_inicio(date("Y-m-d H:i:s"));
                $editaAvaliacao = $boAva->cadastrarAvaliacaoConjuntoReferencia($avaliacaoConjunto);
            }
        } else {
            $editaAvaliacao = new \Ead1_Mensageiro('Não há avaliação para ser alterada !', \Ead1_IMensageiro::SUCESSO);
        }
        return $editaAvaliacao;
    }

    public function editarDisciplina($dados)
    {
        if (empty($dados['id_saladeaula']) || !isset($dados['id_saladeaula'])) {
            $mensageiro = new \Ead1_Mensageiro('Não foi encontrado ID da sala para edição!', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }
        if (empty($dados['id_disciplina']) || !isset($dados['id_disciplina'])) {
            $mensageiro = new \Ead1_Mensageiro('Não foi encontrado ID da disciplina para edição!', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }

        $bo = new \SalaDeAulaBO();

        //altera a disciplina
        $disciplinaSala = new \DisciplinaSalaDeAulaTO();
        $disciplinaSala->setId_disciplina($dados['id_disciplina']);
        $disciplinaSala->setId_saladeaula($dados['id_saladeaula']);
        $editarDisciplina = $bo->salvarDisciplinaSalaDeAula($disciplinaSala);

        return $editarDisciplina;
    }

    public function editarSalaDeAula($dados)
    {
        try {
            if (empty($dados['id_saladeaula']) || !isset($dados['id_saladeaula'])) {
                $mensageiro = new \Ead1_Mensageiro('Não foi encontrado ID da sala para edição!', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }
            $this->beginTransaction();
            $saladeaula = $this->find('\G2\Entity\SalaDeAula', $dados['id_saladeaula']);
            $saladeaula->setSt_saladeaula($dados['st_saladeaula']);
            $saladeaula->setDt_inicioinscricao($dados['dt_inicioinscricao'] ? $this->converteDataBanco($dados['dt_inicioinscricao']) : NULL);
            $saladeaula->setDt_fiminscricao($dados['dt_fiminscricao'] ? $this->converteDataBanco($dados['dt_fiminscricao']) : NULL);
            $saladeaula->setDt_abertura($this->converteDataBanco($dados['dt_abertura']));
            $saladeaula->setDt_encerramento($dados['dt_encerramento'] ? $this->converteDataBanco($dados['dt_encerramento']) : NULL);
            $saladeaula->setId_modalidadesaladeaula($this->getReference('\G2\Entity\ModalidadeSalaDeAula', $dados['id_modalidadesaladeaula']));
            $saladeaula->setNu_diasextensao(0);
            $saladeaula->setId_tiposaladeaula($this->getReference('\G2\Entity\TipoSaladeAula', $dados['id_tiposaladeaula']));
            if (isset($dados['id_periodoletivo']) && $dados['id_periodoletivo']) {
                $saladeaula->setId_periodoletivo($this->getReference('\G2\Entity\PeriodoLetivo', $dados['id_periodoletivo']));
                $saladeaula->setBl_usardoperiodoletivo(1);
            } else if (isset($dados['id_periodoletivo']) && $dados['id_periodoletivo']) {
                $saladeaula->setBl_usardoperiodoletivo(0);
            }
            $saladeaula->setId_situacao($this->getReference('\G2\Entity\Situacao', $dados['id_situacao']));
            $saladeaula->setNu_maxalunos($dados['nu_maxalunos']);

            //AC-27410: permite que os valores sejam salvos corretamente quando a entrada for indefinida.
            $dados['nu_diasaluno'] ? $saladeaula->setNu_diasaluno($dados['nu_diasaluno']) : $saladeaula->setNu_diasaluno(NULL);
            $dados['nu_diasencerramento'] ? $saladeaula->setNu_diasencerramento($dados['nu_diasencerramento']) : $saladeaula->setNu_diasencerramento(NULL);

            $saladeaula->setBl_semencerramento($dados['bl_semencerramento']);
            $saladeaula->setBl_semdiasaluno($dados['bl_semdiasaluno']);
            $saladeaula->setId_categoriasala($this->getReference('\G2\Entity\CategoriaSala', $dados['id_categoriasala']));
            $saladeaula->setBl_ofertaexcepcional($dados['bl_ofertaexcepcional']);
            $saladeaula->setBl_vincularturma($dados['bl_vincularturma']);

            $retornoEditarSalaDeAula = $this->save($saladeaula);
            $this->commit();

            $mensageiro = new \Ead1_Mensageiro('Sala de Aula editada com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;

        } catch (\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao vincular Sala De Aula Entidade. ' . $e->getMessage());
        }
    }

    /**
     * @param $param
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function editarSalaDeAulaPermissoes($param)
    {
        try {
            if (empty($param['id_saladeaula']) || !isset($param['id_saladeaula'])) {
                $mensageiro = new \Ead1_Mensageiro('Não foi encontrado ID da sala para edição!', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }
            if (empty($param['id_entidade']) && $param['bl_todasentidades'] != 1) {
                $mensageiro = new \Ead1_Mensageiro('Selecione uma organização', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }

            /*Deleta os vinculos da sala de aula na entidade*/
            $this->deletarVinculosSalaDeAulaEntidade($param['id_saladeaula']);

            if (isset($param['projetos'])) {
                $this->vincularSalaDeAulaEntidade($param);
                $this->vincularSalaProjetos($param);
                $this->vincularSalaTurmas($param);
            }
            if (isset($param['idProfessores']) && $param['idProfessores'] != '') {
                $vinculaProfessor = $this->vincularSalaProfessor($param);
            } else {
                $vinculaProfessor = new \Ead1_Mensageiro('', \Ead1_IMensageiro::SUCESSO);
            }

            if ($vinculaProfessor->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Permissões editadas com sucesso!', \Ead1_IMensageiro::SUCESSO);
                return $mensageiro;
            } else {
                $mensageiro = new \Ead1_Mensageiro('Erro ao vincular Professores', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function vincularSalaTurmas($param)
    {
        try {
            $projetos = $param['projetos'];
            $objSala = $this->find('\G2\Entity\SalaDeAula', $param['id_saladeaula']);

            $turmaSala = $this->findBy('\G2\Entity\TurmaSala', array('id_saladeaula' => $param['id_saladeaula']));

            if ($turmaSala) {
                foreach ($turmaSala as $ts) {
                    $this->delete($ts);
                }
            }

            foreach ($projetos as $projeto) {
                if ($projeto['turmas']) {
                    foreach ($projeto['turmas'] as $pt) {
                        $objTurmaSala = $this->findBy('\G2\Entity\TurmaSala', array('id_turma' => $pt['id_turma'],
                            'id_saladeaula' => $param['id_saladeaula']));
                        if (!$objTurmaSala) {
                            $objTurmaSala = new \G2\Entity\TurmaSala();
                            $objTurmaSala->setId_turma($this->getReference('\G2\Entity\Turma', $pt['id_turma']));
                            $objTurmaSala->setId_saladeaula($objSala);
                            $objTurmaSala->setDt_cadastro(new \DateTime());
                            $this->save($objTurmaSala);
                        }
                    }
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Vinculo de turma a sala de aula executado com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        } catch (\ORMException $e) {
            Throw new \Zend_Exception('Erro ao salvar vincular turma a sala! ' . $e->getMessage());
        }
    }


    /**
     * Busca as salas de aulas e deleta e salva quem fez a ação
     *
     * @param $id
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     * @atualização Neemias Santos <neemias.santos@unyleya.com.br>
     */
    public function deletarVinculosSalaDeAulaEntidade($id)
    {

        try {
            $salaDeAulaEntidade = $this->findBy('\G2\Entity\SalaDeAulaEntidade', array('id_saladeaula' => $id));
            if ($salaDeAulaEntidade) {
                $this->beginTransaction();
                foreach ($salaDeAulaEntidade as $value) {
                    $this->delete($value);
                }
                $this->commit();
            }
        } catch (\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao deletar Sala de aula entidade. " . $e->getMessage());
        }

        try {
            $saladeaula = $this->find('\G2\Entity\SalaDeAula', $id);
            $this->beginTransaction();
            $saladeaula->setDt_atualiza(new \DateTime());
            $saladeaula->setId_usuarioatualiza($this->sessao->id_usuario);
            $this->save($saladeaula);
            $this->commit();

            $mensageiro = new \Ead1_Mensageiro('Sala editada com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;

        } catch (\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao deletar Sala de aula entidade. " . $e->getMessage());
        }
    }


    public function deletarVinculosProjetoSala($id)
    {
        try {
            $salaProjeto = $this->findBy('\G2\Entity\AreaProjetoSala', array('id_saladeaula' => $id));
            $this->beginTransaction();
            if (isset($salaProjeto)) {
                foreach ($salaProjeto as $value) {
                    $this->delete($value);
                }
            }
            $this->commit();

            $mensageiro = new \Ead1_Mensageiro('Vínculos com Projetos deletados com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        } catch (\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao deletar vínculos de projetos sala. " . $e->getMessage());
        }
    }

    /**
     * Deleta fisicamente o registro da tb_areaprojetosala
     * Alterado o IF seguindo as orientações do Pastor por Elcio Guimarães
     * @param int $id Id, chave primaria da tabela
     * @return bool
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function removeAreaProjetoSalaById($id)
    {
        try {
            if (!$id) {
                throw new \Exception("Id de AreaProjetoSala não informado.");
            }

            $areaProjetoSala = $this->find('\G2\Entity\AreaProjetoSala', $id);
            if ($areaProjetoSala instanceof \G2\Entity\AreaProjetoSala && $areaProjetoSala->getId_areaprojetosala() && $this->delete($areaProjetoSala))
                return true;

            return false;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar deletar dados de AreaProjetoSala. " . $e->getMessage());
        }
    }

    public function editarProdutoValorPrr($dados)
    {
        $ProdutoRO = new \ProdutoRO();
        if ($dados['id_produtovalor'] != '') {
            $idProduto = new \ProdutoValorTO();
            $idProduto->setId_produtovalor($dados['id_produtovalor']);
            $idProduto->fetch(true, true, true);

            $toProdutoValor = new \ProdutoValorTO();
            $toProdutoValor->montaToDinamico($dados);
            $toProdutoValor->setNu_valor($toProdutoValor->getNu_valor() ? $toProdutoValor->getNu_valor() : 0);
            $toProdutoValor->setId_produto($idProduto->getId_produto());
            $retorno = $ProdutoRO->salvarProdutoValor($toProdutoValor);
        } else {
            $retorno = $this->salvarDadosProdutoPrr($dados);
        }

        return $retorno;
    }

    public function salvarDadosProdutoPrr($parametros)
    {
        try {
            $boProduto = new \ProdutoBO();

            $salaDeAula = new \SalaDeAulaTO();
            $salaDeAula->setId_saladeaula($parametros['id_saladeaula']);
            $salaDeAula->fetch(true, true, true);

            $toProduto = new \ProdutoTO();
            $toProduto->setId_tipoproduto(8);
            $toProduto->setId_entidade($this->sessao->id_entidade);
            $toProduto->setId_usuariocadastro($this->sessao->id_usuario);
            $toProduto->setId_situacao(45);
            $toProduto->setBl_todasformas(1);
            $toProduto->setBl_ativo(1);
            $toProduto->setSt_produto($salaDeAula->getSt_saladeaula());
            $toProduto->setBl_todascampanhas(1);

            $toProdutoPrr = new \ProdutoPrrTO();
            $toProdutoPrr->setId_saladeaula($parametros['id_saladeaula']);
            $toProdutoPrr->setId_entidade($this->sessao->id_entidade);

            $valor = str_replace(',', '.', $parametros['nu_valor']);
            $parametros['nu_valor'] = $valor;

            $toProdutoValor = new \ProdutoValorTO();
            $toProdutoValor->montaToDinamico($parametros);
            $toProdutoValor->setNu_valor($toProdutoValor->getNu_valor() ? $toProdutoValor->getNu_valor() : 0);

            $retorno = $boProduto->cadastrarProdutoPrr($toProduto, $toProdutoPrr, $toProdutoValor);

            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Valor do Produto cadastrado com sucesso!', \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro('Erro ao cadastrar produto!', \Ead1_IMensageiro::SUCESSO);
            }
            return $mensageiro;
        } catch (Exception $e) {
            $mensageiro = new \Ead1_Mensageiro('Erro ao salvar Produto' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
            return $mensageiro;
        }
    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function desvincularProfessorSalaMoodle($dados)
    {
        try {
            if (empty($dados['id_perfilreferencia']) || !isset($dados['id_perfilreferencia'])) {
                $mensageiro = new \Ead1_Mensageiro('Não foi encontrado ID do professor para edição!', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }


            $pito = new \VwUsuarioPerfilEntidadeReferenciaTO();
            $pito->setId_perfilreferencia($dados['id_perfilreferencia']);
            $pito->setBl_ativo(true);

            $pbo = new \ProjetoPedagogicoBO();
            $moodleBO = new \MoodleBO();

            $professores = $pbo->retornarVwUsuarioPerfilEntidadeReferencia($pito);

            if ($professores->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                return $professores;
            }

            foreach ($professores->getMensagem() as $professor) {
                if ($professor instanceof \VwUsuarioPerfilEntidadeReferenciaTO) {

                    $uperTO = new \UsuarioPerfilEntidadeReferenciaTO();
                    $uperTO->montaToDinamico($professor->toArray());

                    $orm = new \SalaDeAulaIntegracaoORM();
                    $salaDeAulaIntegracaoTO = $orm->consulta(new \SalaDeAulaIntegracaoTO(array(
                        'id_saladeaula' => $uperTO->getId_saladeaula()
                    )), false, true);

                    if ($salaDeAulaIntegracaoTO instanceof \SalaDeAulaIntegracaoTO) {
                        switch ($salaDeAulaIntegracaoTO->getId_sistema()) {
                            case \SistemaTO::MOODLE:
                                $to = new \VwSalaDeAulaProfessorIntegracaoTO();
                                $to->setId_usuario($uperTO->getId_usuario());
                                $to->setId_sistema(\SistemaTO::MOODLE);
                                $to->setSt_codsistemacurso($salaDeAulaIntegracaoTO->getSt_codsistemacurso());
                                $to->setId_entidade($this->sessao->id_entidade);

                                $mensageiro = $moodleBO->retirarProfessorSalaMoodle($to);
                                break;
                            default:
                                break;
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Metodo resposavel por acessar o repository e retornar VwSalaDeAulaQuantitativos com salas de aula
     * que ainda nao foram iniciadas.
     *
     * @param $params
     * @param $stSalaDeAulaLike | string nome da sala de aula
     * @param array $order
     * @param null $limit
     * @param null $next
     * @param null $offset
     * @return mixed
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     *
     */
    public function retornaVwSalaDeAulaQuantitativosSalasNaoIniciadas($params, $stSalaDeAulaLike, array $order = array(), $limit = NULL, $next = NULL, $offset = NULL)
    {
        return $this->getRepository('\G2\Entity\VwSalaDeAulaQuantitativos')
            ->retornaVwSalaDeAulaQuantitativosSalasNaoIniciadas($params, $stSalaDeAulaLike, $order, $limit, $next, $offset);
    }

    /**
     * Metodo responsavel por desalocar alunos de uma sala de aula de origem
     * nao iniciada e aloca-los novamente em uma sala de aula de destino nao iniciada,
     * por um ou mais projetos pedagogicos especificos.
     *
     * @param array $params
     * @return array|void
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function salvarDivisaoSala(array $params)
    {
        $result = array();

        //Quando enviados varios projetos os processos sao baseados na lista
        //de projetos pedagogicos enviados
        if (array_key_exists('projetos', $params)) {
            $result = $this->dividirSalasPorProjetos($params);
        } else {
            $result = $this->dividirSalasAleatorio($params);
        }

        return $result;

    }

    /**
     * Metodo responsavel por desalocar alunos de uma sala de aula nao iniciada
     * e aloca-los novamente em uma sala de aula de destino nao iniciada por uma
     * quantidade de alunos informada informada.
     *
     * @param $params
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function dividirSalasPorProjetos($params)
    {

        try {
            $alocacaoNegocio = new \G2\Negocio\Alocacao();

            $array = array();

            $projetos = array();

            foreach ($params['projetos'] as $key => $value) {
                $projetos[] = $value['id_projetopedagogico'];
            }

            //Determinando parametros em filtros para a pesquisa de alocacoes
            $where = array(
                'id_saladeaula' => $params['id_saladeaulaorigem'],
                'bl_ativo' => 1,
                'id_entidade' => $this->sessao->id_entidade
            );

            //Buscando alunos alocados na sala informado em $params['id_saladeaulaorigem'] alem,
            //de filtrar pelos projetos pedagogicos informados.
            $alocacoes = $this->getRepository('\G2\Entity\Alocacao')
                ->retornarAlocacoesSalasNaoIniciadasPorProjetosPedagogicos($projetos, $where);

            foreach ($alocacoes as $alocacao) {

                $data = array(
                    'id_saladeaula' => $params['id_saladeauladestino'],
                    'dt_inicio' => $alocacao['dt_inicio'],
                    'id_categoriasala' => $alocacao['id_categoriasala'],
                    'id_matriculadisciplina' => $alocacao['id_matriculadisciplina']
                );

                $array[] = $alocacaoNegocio->salvarAlocacao($data, true);
            }

            return $array;

        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), 500);
        }

    }

    /**
     * Metodo responsavel por desalocar alunos de uma sala de aula nao iniciada,
     * e aloca-los novamente em uma sala de aula nao iniciada de destino, pegando
     * os ultimos x alunos informado no parametro $params['qtd_alunos'], alocados
     * da sala de origem .
     *
     * Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $params
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function dividirSalasAleatorio($params)
    {
        $invalidDtInicio = new \DateTime('1970-12-31');

        try {
            $alocacaoNegocio = new \G2\Negocio\Alocacao();

            $array = array();

            //Determinando parametros em filtros para a pesquisa de alocacoes
            $where = array(
                'bl_ativo' => 1,
                'id_saladeaula' => $params['id_saladeaulaorigem'],
            );

            //Determinando a ordem dos registros do resultado
            $order = array('id_alocacao' => 'DESC');

            //Buscando em ordem inversa x alunos alocados na sala informado em $params['id_saladeaulaorigem']
            $alocacoes = $this->findBy('\G2\Entity\VwSalaDisciplinaAlocacao', $where, $order, $params['qtd_alunos']);

            foreach ($alocacoes as $alocacao) {

                $eAlocacao = $this->find('\G2\Entity\Alocacao', $alocacao->getId_alocacao());
                $dtInicio = $eAlocacao->getDt_inicio();

                if ($invalidDtInicio > $dtInicio) {
                    $eSalaAula = $this->find('\G2\Entity\SalaDeAula', $params['id_saladeaulaorigem']);
                    $dtInicio = $eSalaAula->getDt_abertura()->format(\DATE_ISO8601);
                }

                $data = array(
                    'id_saladeaula' => $params['id_saladeauladestino'],
                    'dt_inicio' => $dtInicio,
                    'id_categoriasala' => $alocacao->getId_categoriasala(),
                    'id_matriculadisciplina' => $alocacao->getId_matriculadisciplina()
                );

                $array[] = $alocacaoNegocio->salvarAlocacao($data, true);
            }

            return $array;

        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), 500);
        }
    }

    /**
     * Metodo que retorna somente um registro da vw_alunogradeintegracao,
     * filtrando pelos parametros informados.
     *
     * IMPORTANTE: Metodo usado tambem pelo aplicativo do portal para
     * retornar os detalhes da sala/disciplina.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $params
     * @return null|object
     * @throws \Zend_Exception
     */
    public function retornaUnicoRegistroVwAlunoGradeIntegracao(array $params)
    {
        return $this->findOneBy('\G2\Entity\VwAlunoGradeIntegracao', $params);
    }

    /**
     * Retorna os coordenadores titulares dos projetos de uma sala
     * @param $id_saladeaula
     * @return array
     */
    public function retornaCoordenadoresProjetoSala($id_saladeaula)
    {
        $array_retorno = [];
        try {
            $qb = $this->em->createQueryBuilder();
            $qb->select(array('vw.id_projetopedagogico', 'vw.st_projetopedagogico', 'pe.st_coordenadortitular'))
                ->from('G2\Entity\VwAreaProjetoSala', 'vw')
                ->Join(
                    'G2\Entity\VwProjetoEntidade',
                    'pe',
                    \Doctrine\ORM\Query\Expr\Join::WITH,
                    'vw.id_projetopedagogico = pe.id_projetopedagogico'
                )
                ->where('vw.id_saladeaula = :id_saladeaula')
                ->andWhere('pe.id_entidade = :id_entidade')
                ->setParameters(array('id_entidade' => (int)$this->sessao->id_entidade, 'id_saladeaula' => (int)$id_saladeaula));

            $arrayProjetos = $qb->getQuery()->getResult();
            if (is_array($arrayProjetos) && count($arrayProjetos)) {
                foreach ($arrayProjetos as $key => $vwObj) {
                    if (!empty($vwObj['st_coordenadortitular']))
                        $array_retorno[] = strtoupper($vwObj['st_coordenadortitular']);
                }
            }
            return array_unique($array_retorno);

        } catch (\Exception $e) {
            return $array_retorno;
        }
    }
}

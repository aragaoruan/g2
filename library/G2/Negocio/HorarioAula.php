<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Horário Aula (tb_horarioaula)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class HorarioAula extends Negocio
{

    private $repositoryName = 'G2\Entity\HorarioAula';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null)
    {
        $result = $this->findBy($this->repositoryName, array('id_entidade' => $this->sessao->id_entidade));

        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {

                $arrReturn[] = array(
                    'id_horarioaula' => $row->getId_horarioaula(),
                    'st_horarioaula' => $row->getSt_horarioaula(),
                    'hr_inicio' => date("H:i", strtotime($row->getHr_inicio())),
                    'hr_fim' => date("H:i", strtotime($row->getHr_fim())),
                    'id_turno' => array(
                        'id_turno' => $row->getId_turno()->getId_turno(),
                        'st_turno' => $row->getId_turno()->getSt_turno()
                    ),
                    'id_entidade' => array(
                        'id_entidade' => $row->getId_entidade()->getId_entidade(),
                        'st_nomeentidade' => $row->getId_entidade()->getSt_nomeentidade()
                    )
                );

            }

            return $arrReturn;
        }
    }

    public function save($data)
    {

        try {
            if (isset($data['id_horarioaula'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id_horarioaula']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\HorarioAula();
            }


            /*********************
             * Definindo atributos
             *********************/

            /*
             * FK id_turno (tb_turno)
             */
            if ($data['id_turno']) {
                $entity->setId_turno($this->find('\G2\Entity\Turno', $data['id_turno']));
            }

            /*
             * FK id_entidade (tb_entidade)
             */
            if (!$data['id_horarioaula'])
                $data['id_entidade'] = $this->sessao->id_entidade;
            if ($data['id_entidade']) {
                $entity->setId_entidade($this->find('\G2\Entity\Entidade', $data['id_entidade']));
            }

            /*
             * Outros campos da tabela
             */
            $entity->setSt_horarioaula($data['st_horarioaula']);

            if(isset($data['id_codhorarioacesso']) && $data['id_codhorarioacesso'])
                $entity->setId_codhorarioacesso($data['id_codhorarioacesso']);

            if(isset($data['st_codhorarioacesso']) && $data['st_codhorarioacesso'])
                $entity->setSt_codhorarioacesso($data['st_codhorarioacesso']);

            $entity->setHr_inicio($data['hr_inicio']);
            $entity->setHr_fim($data['hr_fim']);
            $entity->setId_tipoaula($this->getReference('\G2\Entity\TipoAula',$data['id_tipoaula']));
            $entity->setBl_ativo(1);


            if ($data['id_horarioaula']) {
                $retorno = $this->merge($entity);
            } else {
                $retorno = $this->persist($entity);
            }

            $data['id_horarioaula'] = $retorno->getId_horarioaula();

            // Buscar HorarioDiaSemana e remover todos
            // para poder inserir somente os selecionados
            $NegocioDiaSemana = new \G2\Negocio\HorarioDiaSemana();
            $DiaSemana = $NegocioDiaSemana->findBy('\G2\Entity\HorarioDiaSemana', array(
                'id_horarioaula' => $data['id_horarioaula']
            ));
            // Apagar cada registro encontrado
            foreach ($DiaSemana as $DiaSemanaEntity) {
                $NegocioDiaSemana->delete($DiaSemanaEntity);
            }

            // Adicionar os selecionados
            $index = 1;
            foreach ($data['diasemana'] as $key => $item) {
                if ($item) {
                    $novo = new \G2\Entity\HorarioDiaSemana();
                    $novo->setId_diasemana($index);
                    $novo->setId_horarioaula($data['id_horarioaula']);

                    $NegocioDiaSemana->save($novo);
                }
                $index++;
            }


            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro('Salvo com sucesso', \Ead1_IMensageiro::SUCESSO, $retorno->getId_horarioaula());
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar horário de aula: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Retorna horario aulas by turno e entidade
     * @param integer $id_turno
     * @param integer $id_entidade
     * @return mixed
     */
    public function retornarHorarioAulaByTurno($id_turno = null, $id_entidade = null)
    {
        $params['id_turno'] = $id_turno;
        $params['id_entidade'] = $id_entidade ? $id_entidade : $this->sessao->id_entidade;
        return $this->findOneBy($this->repositoryName, $params);
    }

    /**
     * @param mixed $dt
     * @param array $params
     * @return array
     * @description Retorna os Horarios de Aula disponiveis no Dia da Semana (exemplo segunda-feira) igual da data informada no parametro
     */
    public function findHorariosDisponiveisPorData($dt, $params = array()) {
        $arr_return = array();

        if ($dt) {
            // Retorna 0 (para domingo) até 6 (para sábado)
            $day_of_week = $this->converterData($dt, 'w');

            // Array de campos do banco referente aos dias da semana
            $dias = array('bl_domingo', 'bl_segunda', 'bl_terca', 'bl_quarta', 'bl_quinta', 'bl_sexta', 'bl_sabado');

            // Define o parametro do dia da semana como TRUE para buscar no banco
            $field = isset($dias[$day_of_week]) ? $dias[$day_of_week] : null;

            if ($field) {
                $params[$field] = true;

                if (!isset($params['id_entidade']) || !$params['id_entidade']) {
                    $params['id_entidade'] = $this->sessao->id_entidade;
                }

                $results = $this->findBy('\G2\Entity\VwHorarioAula', $params);

                if ($results) {
                    foreach ($results as $entity) {
                        $arr_return[] = $this->toArrayEntity($entity);
                    }
                }
            }
        }

        return $arr_return;
    }


}
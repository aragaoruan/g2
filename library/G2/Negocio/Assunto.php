<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Assunto
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Assunto extends Negocio {

    /**
     * Metodo responsavel por consultar e retornar VwEntidadeRecursivaId, ordenada
     * pelo campo id_entidade.
     * @param array $where
     * @return G2\Entity\VwEntidadeRecursivaId
     */
    public function findVwEntidadeRecursivaId($where)
    {
        return $this->findBy('\G2\Entity\VwEntidadeRecursivaId', $where, array('id_entidade' => 'ASC'));
    }

    /**
     * Metodo responsavel por consultar de retornar VwAssuntoEntidade, ordenada
     * pelo id_assunto.
     * @param array $where
     * @return G2\Entity\VwAssuntoEntidade
     */
    public function findVwAssuntoEntidade($where)
    {
        return $this->findBy('\G2\Entity\VwAssuntoEntidade', $where, array('id_assunto' => 'ASC'));
    }

    /**
     * Metodo responsavel por receber array com informacoes a serem salvas
     * no banco de dados.
     * @param array post
     * @return boolean|\G2\Negocio\Zend_Exception
     */
    public function atualizarCompartilhamentoAssuntos($data)
    {
        $dao = new \AssuntoCoDAO();
        $assuntoEntidadeTO = new \AssuntoEntidadeCoTO();
        $ormAssuntoEntidade = new \AssuntoEntidadeCoORM();

        $dao->beginTransaction();

        try {
            foreach ($data['arrassuntos'] as $assunto) {
                $assuntoEntidadeTO->setId_assuntoco($assunto['id_assunto']);
                $assuntoEntidadeTO->setId_entidade($assunto['id_entidade']);
                $assuntoEntidadeTO->setId_assuntoentidadeco($assunto['id_assuntoentidadeco']);

                if (array_key_exists('attr_check', $assunto)) {
                    if ($assunto['attr_check'] == 'true' && empty($assunto['id_assuntoentidadeco'])) {
                        $ormAssuntoEntidade->insert($assuntoEntidadeTO->toArrayInsert());
                    }

                    if ($assunto['attr_check'] == 'false' && !empty($assunto['id_assuntoentidadeco'])) {
                        $ormAssuntoEntidade->delete($ormAssuntoEntidade->montarWhere($assuntoEntidadeTO, false, true));
                    }
                }
            }

            $dao->commit();
            return TRUE;
        } catch (Zend_Exception $e) {
            $dao->rollBack();
            return $e;
        }
    }

}

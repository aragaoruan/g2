<?php

namespace G2\Negocio;

/**
 * Classe de Negocio para Categoria
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-11
 */
class Categoria extends Negocio
{

    private $repositoryName = 'G2\Entity\Categoria';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna Categorias
     * @param array $where
     * @param array $order
     * @return G2\Entity\Categoria
     * @throws \Zend_Exception
     */
    public function retornarCategorias(array $where = array(), array $order = array())
    {
        try {
            $where['id_entidadecadastro'] = (isset($where['id_entidadecadastro']) ? $where['id_entidadecadastro'] : $this->sessao->id_entidade);
            $where['bl_ativo'] = (isset($where['bl_ativo']) ? $where['bl_ativo'] : true);
            return $this->findBy($this->repositoryName, $where, $order);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Metodo que retorna as categorias associadas a entidade, após busca os produtos
     * vendidos pela a entidade sem categorias associadas a entidade.
     * Utilização no RelatorioComercialEcommerceController
     *
     * Author: Neemias Santos <neemias.santos@unyleya.com.br>
     * @param type $where
     * @return Array VwCategoriaEntidade
     */
    public function retornarCategoriaEntidadeParams($where)
    {
        try {
            if ($where['id_entidade'] == '0') {
                unset($where['id_entidade']);
                $where['id_entidadepai'] = $this->sessao->id_entidade;
            }
            $result = $this->em->getRepository($this->repositoryName)->retornaCategoriasProdutoEntidadeParams($where);

            return $result;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }

    }

    /**
     *
     * @param type $array
     * @return type
     * @throws \Zend_Exception
     * @author Helder Silva <helder.silva@unyleya.com.br>
     */
    public function retornarCategoriaEntidade($array)
    {
        try {
            $param['bl_ativo'] = $array['bl_ativo'];
            $param['id_entidade'] = $this->sessao->id_entidade;
            $resultado = $this->findBy('\G2\Entity\VwCategoriaEntidade', $param);
            $retorno = array();
            foreach ($resultado as $key => $categoria) {
                if ($categoria->getId_situacao() == 96 || $categoria->getId_situacao() == 108) {
                    continue;
                } else {
                    $retorno[] = $this->toArrayEntity($categoria);
                }
            }
            return $retorno;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }


    public function save($data)
    {

        try {
            $this->beginTransaction();
            $mensageiro = new \Ead1_Mensageiro();


            if (isset($data['id_categoria'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id_categoria']);
            } else {
                //se não existir o id cria um novo registroliga lá
                $entity = new \G2\Entity\Categoria();
            }

            /*********************
             * Definindo atributos
             *********************/

            if (isset($data['st_nomeexibicao'])) {
                $entity->setSt_nomeexibicao($data['st_nomeexibicao']);
            }

            if (isset($data['st_categoria']))
                $entity->setSt_categoria($data['st_categoria']);

            if (isset($data['id_usuariocadastro']['id_usuario']))
                $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $data['id_usuariocadastro']['id_usuario']));
            else
                $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));

            if (isset($data['id_entidadecadastro']))
                $entity->setId_entidadecadastro($data['id_entidadecadastro']);
            else
                $entity->setId_entidadecadastro($this->sessao->id_entidade);

            if (isset($data['id_uploadimagem']['id_upload']))
                $entity->setId_uploadimagem($this->find('\G2\Entity\Upload', $data['id_uploadimagem']['id_upload']));

            if (!empty($data['id_categoriapai']['id_categoria']))
                $entity->setId_categoriapai($this->find('\G2\Entity\Categoria', $data['id_categoriapai']['id_categoria']));

            if (isset($data['id_situacao']['id_situacao']))
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', $data['id_situacao']['id_situacao']));

            if (isset($data['dt_cadastro']))
                $entity->setDt_cadastro($data['dt_cadastro']);
            else
                $entity->setDt_cadastro(date('Y-m-d H:i:s'));

            if (isset($data['bl_ativo']))
                $entity->setBl_ativo($data['bl_ativo']);
            else
                $entity->setBl_ativo(1);


//            if (isset($data['id_categoria']) && !empty($data['id_categoria'])) {
//                $retorno = $this->merge($entity);
//            } else {
//                $retorno = $this->persist($entity);
//            }

            $retorno = parent::save($entity);

            if (!$retorno) {
                throw new \Exception("Erro ao tentar salvar dados de categoria.");
            } else {
                $arrRes = $this->toArrayEntity($retorno);
                $arrRes['id_usuariocadastro'] = $retorno->getId_usuariocadastro() ? $retorno->getId_usuariocadastro()->getId_usuario() : null;
                $arrRes['id_uploadimagem'] = $retorno->getId_uploadimagem() ? $retorno->getId_uploadimagem()->getId_upload() : null;
                $arrRes['id_categoriapai'] = $retorno->getId_categoriapai() ? $retorno->getId_categoriapai()->getId_categoria() : null;
                $arrRes['id_situacao'] = $retorno->getId_situacao() ? $retorno->getId_situacao()->getId_situacao() : null;
                $mensageiro->setMensageiro($arrRes, \Ead1_IMensageiro::SUCESSO);
            }


            $data['id_categoria'] = $retorno->getId_categoria();


            /**
             * CategoriaEntidade
             */
            // Buscar CategoriaEntidade e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\CategoriaEntidade', array(
//                'id_entidade' => $this->sessao->id_entidade,
                'id_categoria' => $data['id_categoria']
            ));

            if ($ItemsEntity) {
                // Apagar cada registro encontrado
                foreach ($ItemsEntity as $ItemEntity) {
                    if (!parent::delete($ItemEntity)) {
                        throw new \Exception("Houve um erro ao tentar desvincular a categoria da entidade cod: {$ItemEntity->getId_entidade()}");
                    }

                }
            }

            // Adicionar os selecionados
            if (isset($data['organizacoes']) && $data['organizacoes']) {
                $arrOganizacoes = array();
                $user = $entity->getId_usuariocadastro();//$this->getReference('\G2\Entity\Usuario', $entity->getId_usuariocadastro());
                foreach ($data['organizacoes'] as $key => $item) {
                    $novo = new \G2\Entity\CategoriaEntidade();
                    $novo->setId_categoria($data['id_categoria']);
                    $novo->setId_entidade((int)$item);
                    $novo->setId_usuariocadastro($user);
                    $novo->setDt_cadastro(date('Y-m-d H:i:s'));

                    if (!parent::save($novo)) {
                        throw new \Exception("Erro ao salvar vinculo de categoria com entidade cod: {$item}");
                    } else {
                        $arrOganizacoes[] = $item;
                    }


                }
                if ($arrOganizacoes) {
                    $mensageiro->addMensagem($arrOganizacoes, 'organizacoes');
                }
            }


            $this->commit();
            return $mensageiro;
        } catch (Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao o registro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }


}

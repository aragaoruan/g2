<?php

namespace G2\Negocio;

//use G2;

use G2;
use G2\Entity\VwVendaLancamento;

//use G2;


/**
 * Classe negocio Venda
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Venda extends Negocio
{

    private $mensageiro;


    public function __construct()
    {
        parent::__construct();

        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Metodo responsavel por receber e executar outros metodos responsaveis
     * por salvar uma venda de PRR
     * @param array $data
     * @return \Doctrine\ORM\NoResultException|\Exception|G2\Entity\Venda|mixed|null|object
     * @throws \Zend_Exception
     */
    public function salvarVendaPrr(array $data)
    {

        try {

            $this->em->beginTransaction();
            $vendaParams = $data['venda'];
            $produtoVendaParams = $data['produtos'];
            $ocorrencia = $this->getReference('G2\Entity\Ocorrencia', $vendaParams['id_ocorrencia']);

            if (!array_key_exists('id', $vendaParams) or empty($vendaParams['id'])) {
                $venda = new \G2\Entity\Venda();
                $usuario = $this->getReference('G2\Entity\Usuario', $vendaParams['id_usuario']);
                $evolucao = $this->getReference('G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_NEGOCIACAO);
                $situacao = $this->getReference('G2\Entity\Situacao', \G2\Constante\Situacao::TB_VENDA_ATIVO);

                $venda->setIdEvolucao($evolucao);
                $venda->setIdSituacao($situacao);
                $venda->setBlAtivo(TRUE);
                $venda->setBlContrato(FALSE);
                $venda->setIdEntidade($this->sessao->id_entidade);
                $venda->setDtCadastro(new \DateTime());
                $venda->setIdUsuario($usuario);
                $venda->setIdOrigemvenda(\G2\Constante\OrigemVenda::INTERNO);
            } else {
                $venda = $this->find('G2\Entity\Venda', $vendaParams['id']);
            }

            //Somando o valor da venda atual com o valor adicional enviado
            $venda->setNuValorbruto($venda->getNuValorbruto() + $vendaParams['nu_valorbruto']);
            $venda->setNuValorliquido($venda->getNuValorliquido() + $vendaParams['nu_valorliquido']);
            $venda->setIdUsuarioCadastro($this->sessao->id_usuario);
            $venda->setIdOcorrencia($ocorrencia);

            //Salvando dados de venda
            $venda = $this->save($venda);

            //Salvando dados de venda produto
            foreach ($produtoVendaParams as $produto) {
                $this->salvarVendaProduto($produto, $venda);
            }

            $this->em->commit();
            return $venda;
        } catch (\Zend_Exception $e) {
            $this->em->rollback();
            throw $e;
        }
    }
    public function verificarVendaOcorrencia(array $data){

        try{
            $venda = 'G2\Entity\Venda';
            return $this->findOneBy($venda, array('id_ocorrencia' => $data['id_ocorrencia']));
        }catch (\Zend_Amf_Exception $e){
            $this->em->rollback();
            throw $e;
        }
    }

    public function salvarVendaOcorrencia(array $data){
//        438439
        try{

            $this->em->beginTransaction();
            $venda = new \G2\Entity\Venda();
            $ocorrencia = $this->getReference('G2\Entity\Ocorrencia', $data['id_ocorrencia']);

            $formapagamento = $this->getReference('G2\Entity\FormaPagamento', \G2\Constante\FormaPagamento::GERAL_CICLICO);
            $evolucao = $this->getReference('G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_NEGOCIACAO);
            $situacao = $this->getReference('G2\Entity\Situacao', \G2\Constante\Situacao::TB_VENDA_ATIVO);
            $usuariocadastro = $this->getReference('G2\Entity\Usuario', (int)$data['id_usuariointeressado']);

            $venda->setId_formapagamento($formapagamento);
            $venda->setId_evolucao($evolucao);
            $venda->setId_situacao($situacao);
            $venda->setBl_ativo(true);
            $venda->setId_usuariocadastro($this->sessao->id_usuario);
            $venda->setNu_valorliquido(0);
            $venda->setNu_valorbruto(0);
            $venda->setId_entidade($this->sessao->id_entidade);
            $venda->setId_usuario($usuariocadastro);
            $venda->setBl_contrato(1);
            $venda->setId_ocorrencia($ocorrencia);
            $venda->setDt_cadastro(new \DateTime());

            $venda = $this->save($venda);

            $parametros['ocorrencia'] = array(
                'id_venda' => $venda->getId_venda(),
                'id_ocorrencia' => $data['id_ocorrencia']
            );


            $msg = 'Foi gerado uma venda com id: '. $venda->getId_venda();
            $tramite = new \G2\Negocio\Ocorrencia();

            $tramite->tramiteOcorrenciaVinculada(
                $data['id_ocorrencia'],
                $msg
            );

            $this->registraTramite($parametros);
            $this->commit();

            return $venda;

        }catch (\Zend_Acl_Exception $e){
            $this->em->rollback();
            throw $e;
        }
    }

    /**
     * Metodo responsavel por receber dados de venda e salvar uma
     * venda produto
     *
     * @param array $data
     * @param G2\Entity\Venda $venda
     * @return mixed
     * @throws \Exception
     */
    public function salvarVendaProduto(array $data, \G2\Entity\Venda $venda)
    {

        if (!array_key_exists('id', $data) or empty($data['id'])) {

            try {

                $vendaReference = $this->getReference('\G2\Entity\Venda', $venda->getIdVenda());
                $produto = $this->find('\G2\Entity\Produto', $data['id_produto']);
                $matricula = $this->getReference('\G2\Entity\Matricula', $data['id_matricula']);

                $vendaProduto = new \G2\Entity\VendaProduto();
                $vendaProduto
                    ->setId_venda($vendaReference)
                    ->setNu_valorbruto($data['nu_valorbruto'])
                    ->setNu_valorliquido($data['nu_valorliquido'])
                    ->setDt_cadastro(new \DateTime())
                    ->setDt_entrega(new \DateTime())
                    ->setId_produto($produto)
                    ->setId_matricula($matricula);

                return $this->save($vendaProduto);
            } catch (\Exception $e) {
                throw $e;
            }
        }
    }

    /**
     * Metodo responsavel por deletar uma vendaproduto
     * @param $idVendaProduto
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object
     * @throws \Exception
     */
    public function deleteVendaProduto($idVendaProduto)
    {
        $this->beginTransaction();

        try {

            $vendaProduto = $this->find('G2\Entity\VendaProduto', $idVendaProduto);
            $venda = $this->find('\G2\Entity\Venda', $vendaProduto->getId_venda());

            $venda->setNuValorLiquido($venda->getNuValorLiquido() - $vendaProduto->getNu_valorliquido());
            $venda->setNuValorBruto($venda->getNuValorBruto() - $vendaProduto->getNu_valorliquido());
            $this->save($venda);

            $this->delete($vendaProduto);
            $this->commit();

            return $vendaProduto;
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * Metodo responsavel por retornar lista de lançamentos feitos para uma venda
     *
     * @param array $where
     * @return array G2\Entity\ResumoFinanceiro
     */
    public function findVwResumoFinanceiro(array $where)
    {
        return $this->findBy('\G2\Entity\VwResumoFinanceiro', $where);
    }

    /**
     * @param array $lancamentos
     * @return mixed
     * @throws \Exception
     */
    public function salvarArrayLancamentoDadosRecebimentoCartao(array $lancamentos)
    {
        try {
            $this->beginTransaction();
            foreach ($lancamentos as $i => $parcela) {
                $lancamentos[$i]['id_lancamento'] = $parcela['id_lancamento'];
                $lancamentos[$i]['id_cartaoconfig'] = $parcela['id_cartaoconfig'];
                $lancamentos[$i]['st_autorizacao'] = $parcela['st_autorizacao'];
                $lancamentos[$i]['st_ultimosdigitos'] = isset($parcela['digitos']) ? $parcela['digitos'] : null;
                $lancamentos[$i]['dt_prevquitado'] = $parcela['dt_vencimento'];


                unset($lancamentos[$i]['digitos']);

                if (!$this->salvarLancamento($lancamentos[$i])) {
                    throw new \Exception("Erro ao salvar dados do cartão nos lançamentos das parcelas.");
                }
            }
            $this->commit();
            return $lancamentos;
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * Salva um Lancamento
     *
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function salvarLancamento(array $data)
    {

        try {
            //pega as referencias
            $tipoLancamentoRef = $this->getReference(
                '\G2\Entity\TipoLancamento',
                $data['id_tipolancamento']
            );

            $meioPagamentoRef = $this->getReference(
                '\G2\Entity\MeioPagamento',
                $data['id_meiopagamento']
            );

            if ($data['dt_vencimento'] instanceof \Zend_Date) {
                $dateTime = $this->converteDataBanco($data['dt_vencimento']->toString('Y/M/d'));
            } else {
                //new \DateTime($this->converteDate($data['dt_vencimento']));
                $dateTime = $this->converteDataBanco($data['dt_vencimento']);
            }

            //instancia a entity
            $entity = new \G2\Entity\Lancamento();

            //verifica se foi passado o id do lançamento e busca o registro
            if (array_key_exists('id_lancamento', $data) && !empty($data['id_lancamento'])) {
                $entity = $this->find('\G2\Entity\Lancamento', $data['id_lancamento']);

            }

            //seta os atributos
            $entity->setBl_ativo(
                array_key_exists('bl_ativo', $data) && !empty($data['bl_ativo']) ?
                    $data['bl_ativo'] :
                    true
            );

            //verifica se foi passado esses atributos ou seta o valor padrão
            $entity->setBl_quitado(
                array_key_exists('bl_quitado', $data) && !empty($data['bl_quitado']) ?
                    $data['bl_quitado'] :
                    false
            );

            $entity->setDt_quitado(
                array_key_exists('dt_quitado', $data) && !empty($data['dt_quitado']) ?
                    $this->converteDataBanco($data['dt_quitado']) :
                    NULL
            );

            $entity->setId_tipolancamento($tipoLancamentoRef);
            $entity->setDt_vencimento($dateTime);
            $entity->setId_meiopagamento($meioPagamentoRef);

            $entity->setId_usuariocadastro(
                $entity->getId_usuariocadastro() ?
                    $entity->getId_usuariocadastro() :
                    $data['id_usuariocadastro']
            );

            if (array_key_exists('id_usuariolancamento', $data)) {
                $entity->setId_usuariolancamento($data['id_usuariolancamento'] ?: null);
            }

            if (array_key_exists('id_entidadelancamento', $data)) {
                $entity->setId_entidadelancamento($data['id_entidadelancamento'] ?
                    $this->em->getReference('\G2\Entity\Entidade', $data['id_entidadelancamento']) :
                    null
                );
            }

            $entity->setBl_original(
                array_key_exists('bl_original', $data) && !empty($data['bl_original']) ?
                    $data['bl_original'] :
                    false
            );

            //Pega o id_entidade aue foi enviado
            if (
                array_key_exists('id_entidade', $data)
                && !empty($data['id_entidade'])
            ) {
                $entity->setId_entidade($data['id_entidade']);
            } else {
                $entity->setId_entidade(
                    $entity->getId_entidade() ?
                        $entity->getId_entidade() :
                        $this->sessao->id_entidade
                );
            }

            if (
                array_key_exists('nu_valor', $data)
                && !empty($data['nu_valor'])
            ) {
                $entity->setNu_valor($data['nu_valor']);
            }

            if (
                array_key_exists('id_cartaoconfig', $data)
                && !empty($data['id_cartaoconfig'])
            ) {
                $entity->setId_cartaoconfig(
                    $this->getReference('\G2\Entity\CartaoConfig', $data['id_cartaoconfig'])
                );
            }

            if (
                array_key_exists('st_autorizacao', $data)
                && !empty($data['st_autorizacao'])
            ) {
                $entity->setSt_autorizacao($data['st_autorizacao']);
            }

            if (
                array_key_exists('st_ultimosdigitos', $data)
                && !empty($data['st_ultimosdigitos'])
            ) {
                $entity->setSt_ultimosdigitos($data['st_ultimosdigitos']);
            }

            if (
                array_key_exists('st_coddocumento', $data)
                && !empty($data['st_coddocumento'])
            ) {
                $entity->setSt_coddocumento($data['st_coddocumento']);
            }

            if (
                array_key_exists('dt_prevquitado', $data)
                && !empty($data['dt_prevquitado'])
            ) {
                $entity->setDt_prevquitado(
                    (isset($data['dt_prevquitado'])) ?
                        $this->converteDataBanco($data['dt_prevquitado']) :
                        null
                );
            }

            $entity->setDt_cadastro(new \DateTime());
            $entity->setNu_verificacao(0);

            $entity->setBl_chequedevolvido(
                isset($data['bl_chequedevolvido']) ?
                    $data['bl_chequedevolvido'] :
                    false
            );

            $entity->setId_lancamentooriginal(
                array_key_exists('id_lancamentooriginal', $data) && !empty($data['id_lancamentooriginal']) ?
                    $data['id_lancamentooriginal'] :
                    NULL
            );

            $entity->setnu_cartao(isset($data['nu_cartao']) ? $data['nu_cartao'] : 1);

            if(!empty($data['nu_cobranca'])){
                $entity->setNu_cobranca($data['nu_cobranca']);
            }

            if(!empty($data['nu_assinatura'])){
                $entity->setNu_assinatura($data['nu_assinatura']);
            }

            $savedEntity = $this->save($entity);

            if (
                $entity->getId_meiopagamento()->getId_meiopagamento() == G2\Constante\MeioPagamento::BOLETO
            ) {
                $this->atualizarBoleto($entity);
            }

            return $savedEntity;
        } catch (\Exception $e) {
            throw $e;
        }
    }


    /**
     * Atualiza o Boleto na Pagar.me
     *
     * http://jira.unyleya.com.br/browse/GII-7379
     * Proposta de solução:
     * Sempre que alterar os dados do lançamento do aluno, é necessário gerar um novo boleto.
     * Dados que podem ser alterados:
     * Vencimento (data);
     * Valor;
     * Nome do aluno;
     * CPF.
     * @param G2\Entity\Lancamento $lancamento
     * @return bool|\Ead1_Mensageiro
     * @throws \Exception
     */
    public function atualizarBoleto(G2\Entity\Lancamento $lancamento)
    {
        $pagarme = new Pagarme();

        try {

            $transacao = null;
            $alterarPagarme = false;

            if ($lancamento->getSt_idtransacaoexterna()) {

                $transacaoM = $pagarme->getPagarmeTransaction(
                    $lancamento->getSt_idtransacaoexterna(),
                    $lancamento->getId_entidade()
                );

                if ($transacaoM->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $transacao = $transacaoM->getFirstMensagem();

                    if ($transacao->metadata->id_lancamento != $lancamento->getId_lancamento())
                        return false;

                    // Verificando a Data de Vencimento
                    $transVencimento = new \DateTime($transacao->boleto_expiration_date);
                    $lanVencimento = $lancamento->getDt_vencimento()->format("Y-m-d");

                    if ($transVencimento != $lanVencimento)
                        $alterarPagarme = true;

                    //verificando os dados do Usuário
                    $pessoa = new G2\Entity\VwPessoa();
                    $pessoa->findOneBy(
                        array(
                            'id_usuario' => $lancamento->getId_usuariolancamento(),
                            'id_entidade' => $lancamento->getId_entidade()
                        )
                    );

                    if ($transacao->customer->name != $pessoa->getst_nomecompleto()) $alterarPagarme = true;
                    if ($transacao->customer->document_number != $pessoa->getst_cpf()) $alterarPagarme = true;

                    //verificando o valor
                    if (($transacao->amount / 100) != $lancamento->getNu_valor())
                        $alterarPagarme = true;


                    if ($alterarPagarme) {
                        $lancamento->setSt_idtransacaoexterna(null);
                        $lancamento->setSt_Urlboleto(null);
                        $lancamento->setSt_codigobarras(null);

                        $this->save($lancamento);

                        return $this->mensageiro->setMensageiro(
                            'Lançamento atualizado com sucesso',
                            \Ead1_IMensageiro::SUCESSO
                        );
                    } else {
                        return $this->mensageiro->setMensageiro(
                            'Lançamento identico ao já cadastrado',
                            \Ead1_IMensageiro::AVISO
                        );
                    }
                }
//                \Zend_Debug::dump($transVencimento->format("Y-m-d"), __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($lancamento->getDt_vencimento()->format("Y-m-d"), __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump('-------------------------------', __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($transacao->amount/100, __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($lancamento->getNu_valor(), __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump('-------------------------------', __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($transacao->customer->name, __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($pessoa->getst_nomecompleto(), __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump('-------------------------------', __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($transacao->customer->document_number, __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($pessoa->getst_cpf(), __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($alterarPagarme, __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($transacao->metadata->id_lancamento, __FILE__ . ' - '.__line__ );
//                \Zend_Debug::dump($transacao, __FILE__ . ' - '.__line__ );
//                exit;
            }

            return $this->mensageiro->setMensageiro('Lançamento sem transação externa', \Ead1_IMensageiro::AVISO);

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Salva array de lançamentos PRR
     * (Foi criando separado chamando o método salvarArrayLancamentos() para não quebrar a funcionalidade de PRR feita pelo RBD)
     *
     * @param array $data
     */
    public function salvarLancamentoPrr(array $data)
    {
        return $this->salvarArrayLancamentos($data);
    }

    /**
     * Salva array de lançamentos
     *
     * @param array $data
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarArrayLancamentos(array $data)
    {

        $this->beginTransaction();

        try {
            foreach ($data as $key => $value) {

                //Caso exista um lancamento feito sera apagado e substituido
                if ($value['id_lancamento']) {
                    $lancamento = $this->find('\G2\Entity\Lancamento', $value['id_lancamento']);
                    $lancamento->setBlAtivo(FALSE);
                    $this->save($lancamento);
                }

                //Setando valores e padrao de valores para serem gravados em lancamento
                $value['nu_valor'] = $nuValor = str_replace(',', '.', str_replace('.', '', $value['nu_valor']));
                $value['id_tipolancamento'] = \G2\Constante\TipoLancamento::CREDITO;
                $value['id_usuariocadastro'] = $value['id_usuario'];
                $value['nu_cobranca'] = (!empty($value['nu_cobranca']) ? $value['nu_cobranca'] : ($key + 1));
                $entityLancamento = $this->salvarLancamento($value);

                /*
                 * Set numero de ordem das parcelas e qual parcela e a entrada
                 * no caso sempre a primeira parcela
                 */
                $value['nu_ordem'] = ($key + 1);

                if ($value['nu_ordem'] === 1 || (array_key_exists('bl_entrada', $value) && $value['bl_entrada'])) {
                    $value['bl_entrada'] = TRUE;
                }

                $this->salvarLancamentoVenda($value, $entityLancamento);
            }

            $this->commit();
        } catch (\Zend_Exception $ex) {
            $this->rollback();
            throw $ex;
        }
    }

    /**
     * @param array $data
     * @param \G2\Entity\Lancamento $lancamento
     * @return mixed
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarLancamentoVenda(array $data, \G2\Entity\Lancamento $lancamento)
    {
        try {

            $vendaRef = $this->find('\G2\Entity\Venda', $data['id_venda']);

            $entity = new \G2\Entity\LancamentoVenda();
            $entity->setIdLancamento($lancamento);
            $entity->setIdVenda($vendaRef);
            $entity->setBlEntrada($data['bl_entrada']);
            $entity->setNuOrdem($data['nu_ordem']);

            return $this->save($entity);
        } catch (\Zend_Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Abona uma venda de PRR
     *
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function abonarVendaPrr(array $data)
    {
        $this->beginTransaction();
        $ocorrencia = new Ocorrencia();

        try {

            //Chama o metodo para cancelar todos os lancamentos da venda caso haja
            if (array_key_exists('lancamentos', $data)) {
                $this->cancelarLancamentos($data['lancamentos']);
            }

            //Chama o metodo para salvar uma venda
            $result = $this->salvarVenda($data['venda']);

            //Chama o metodo para gerar um novo tramite
            $ocorrencia->gerarNovaInteracao($data['tramite']);

            $this->commit();

            return $result;
        } catch (\Exception $ex) {
            $this->rollback();
            throw $ex;
        }
    }

    /**
     * Cancela Lançamentos passados em um array. array(array('id_lancamento'=>1), array('id_lancamento'=>3))
     *
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function cancelarLancamentos(array $data)
    {
        $this->beginTransaction();
        $result = array();

        try {
            foreach ($data as $value) {
                $lancamento = $this->find('\G2\Entity\Lancamento', $value['id_lancamento']);
                $lancamento->setBlAtivo(FALSE);
                array_push($result, $this->save($lancamento));
            }
            $this->commit();
            return $result;
        } catch (\Exception $ex) {
            $this->rollback();
            throw $ex;
        }
    }

    /**
     * Metodo responsavel por salvar Venda
     *
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarVenda(array $data)
    {
        try {
            $entity = new \G2\Entity\Venda();

            $id = null;
            if (array_key_exists('id', $data) && $data['id']) {
                $id = $data['id'];
            } else {
                if (array_key_exists('id_venda', $data) && $data['id_venda']) {
                    $id = $data['id_venda'];
                }
            }

            if ($id) {
                $entity = $this->findVenda($id);
            }

            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $newEntity = $serialize->arrayToEntity($data, $entity);
            return $this->save($newEntity);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar venda." . $e->getMessage());
        }
    }

    /**
     * Retorna Venda por Id
     * @param $id
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object
     * @throws \Exception
     */
    public function findVenda($id)
    {
        try {
            return $this->find('\G2\Entity\Venda', $id);

        } catch (\Exception $e) {
            throw new \Exception("Erro ao consultar venda por id. " . $e->getMessage());
        }
    }

    /**
     * Marca como quitado um boleto
     *
     * @param array $data
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object|string
     * @throws \Exception
     */
    public function quitarBoleto(array $data)
    {

        $bo = new \VendaBO();
        //setando atributo na to
        $to = new \LancamentoTO();
        $to->setId_lancamento($data['id_lancamento']);

        try {
            $result = $bo->quitarBoleto($to);
            //retorna um find
            if ($result->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                try {
                    return $this->find('G2\Entity\Lancamento', $data['id_lancamento']);
                } catch (\Exception $e) {
                    return $e->getMessage();
                }
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Find By vendas
     *
     * @param array $where
     * @param array $order
     * @return \G2\Entity\Venda
     */
    public function findVendas(array $where = array(), array $order = null)
    {
        return $this->findBy('\G2\Entity\Venda', $where, $order);
    }

    /**
     * Quita um lançamento
     * @param $arrayLancamentos
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function quitarCredito($arrayLancamentos)
    {
        try {
            if ($arrayLancamentos[0]->getDtPrevquitado() instanceof \DateTime) {
                $dataAux = $arrayLancamentos[0]->getDtPrevquitado();
            } elseif (!$arrayLancamentos[0]->getDtPrevquitado()) {
                $dataAux = new \DateTime();
            }

            foreach ($arrayLancamentos as &$lancamentoTO) {

                $dataAux = $dataAux->add(new \DateInterval("P1M"));
                $lancamentoTO->setDtPrevquitado($dataAux);
                $lancamentoTO->setBlQuitado(false);
                $mensageiro = $this->quitarLancamento($lancamentoTO);

                /**
                 * Não imagino o porque de não ter esta validação
                 */
                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) throw new \Exception($mensageiro->getFirstMensagem());

            }

            $mensageiro = new \Ead1_Mensageiro("Crédito Quitado com Sucesso!", \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro("Erro ao Quitar Débito!", \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $mensageiro;
    }

    /**
     * Método que quita o lancamento
     *
     * @param \G2\Entity\Lancamento $to
     * @return \Ead1_Mensageiro
     */
    protected function quitarLancamento(\G2\Entity\Lancamento &$to)
    {

        $this->beginTransaction();
        try {
            if (!$to->getIdLancamento()) {
                THROW new \Zend_Validate_Exception('Lançamento Não Identificado!');
            }

            $to->setNuQuitado($to->getNuQuitado() ? $to->getNuQuitado() : $to->getNuValor());

            if ($to->getIdMeiopagamento()->getId_meiopagamento() != \MeioPagamentoTO::CARTAO) {
                $to->setBlQuitado(true);
                $to->setDtQuitado($to->getDtQuitado() ? $to->getDtQuitado() : new \DateTime());
                $to->setDtPrevquitado(NULL);
            } else {
                $to->setDtPrevquitado($to->getDtPrevquitado());
            }

            $vwVendaLancamentoTO = new \VwVendaLancamentoTO();
            $vwVendaLancamentoTO->setId_lancamento($to->getIdLancamento());
            $vwVendaLancamentoTO->fetch(false, true, true);

            $cc = new \CampanhaComercialBO();
            $mensageiro = $cc->vincularLancamentoCampanhaPontualidade($vwVendaLancamentoTO);
            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                THROW new \Zend_Exception($mensageiro->getFirstMensagem());
            }

            $this->save($to);
            $this->commit();

            if ($vwVendaLancamentoTO->getBl_entrada() && $vwVendaLancamentoTO->getId_entidade() && $vwVendaLancamentoTO->getId_venda()) {
                $ei = new \G2\Entity\EntidadeIntegracao();
                $ei->findOneBy(array('id_sistema' => 21, 'id_entidade' => $vwVendaLancamentoTO->getId_entidade()));
                if ($ei->getId_entidadeintegracao()) {
                    $crm = new \G2\Negocio\IntegracaoCRM($vwVendaLancamentoTO->getId_entidade());
                    $crm->atualizarPagamentoCRM($vwVendaLancamentoTO->getId_venda());
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Lançamento Quitado com sucesso', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
            $this->rollBack();
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro("Erro ao Quitar Lançamento!", \Ead1_IMensageiro::ERRO, $e->getMessage());
            $this->rollBack();
        }
        return $mensageiro;
    }

    /**
     * Retorna dados de Venda Produto
     *
     * @param array $where
     * @return \G2\Entity\VendaProduto
     * @throws \Zend_Exception
     */
    public function retornarVendaProduto(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VendaProduto', $where);
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao consultar Venda Produto. ' . $ex->getMessage());
        }
    }

    /**
     * Retorna Venda Produto por id
     *
     * @param integer $id
     * @return \G2\Entity\VendaProduto
     * @throws \Zend_Exception
     */
    public function findVendaProduto($id)
    {
        try {
            return $this->find('\G2\Entity\VendaProduto', $id);
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao consultar Venda Produto. ' . $ex->getMessage());
        }
    }

    /**
     * Retorna vendas de produtos do modelo assinatura
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaVendaProdAssinatura(array $params = array())
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            return $this->em->getRepository('\G2\Entity\Venda')->retornaVendaProdutoAssinatura($params);
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao consultar Venda Produto Assinatura. ' . $ex->getMessage());
        }
    }

    /**
     * Retornar dados de sp_campanhavendaproduto
     *
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarSpCampanhaVendaProduto(array $params = array())
    {
        try {
            $to = new \SpCampanhaVendaProdutoTO(); //instancia a TO
            $to->montaToDinamico($params); //seta os parametros na TO
            $bo = new \VendaBO(); //Instancia a BO
            //Busca os dados da BO
            $result = $bo->retornaSpCampanhaVendaProduto($to);

            //monta o retorno
            $arrReturn = array();
            if ($result->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                foreach ($result->getMensagem() as $row) {
                    $arrReturn[] = $row->toArray();
                }
            }
            unset($result);

            return $arrReturn; //retorna
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar dados da SP. ' . $e->getMessage());
        }
    }

    /**
     * Retorna array com os id_lancamento vinculados a uma venda. Ex.: array(1,2,3)
     *
     * @param integer $idVenda id_venda
     * @return mixed | array Retornar NULL ou array com os id_lancamento
     * @throws \Zend_Exception
     */
    public function retornarArrayIdLancamentosDaVenda($idVenda)
    {
        try {
            //Busca relacionamentos de Lançamentos com Venda
            $result = $this->retornarLancamentosVenda(array('id_venda' => $idVenda));

            if ($result) { //verifica se retornou algo
                //percorre o resultado
                foreach ($result as $key => $lancamento) {
                    //reescreve a variavel result atribuindo somente os ids do lançamento
                    $result[$key] = $lancamento->getIdLancamento()->getIdLancamento();
                }
            }
            //Retorna
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao retornar lançamentos vinculados a venda. " . $e->getMessage());
        }
    }

    /**
     * Retorna Lançamento das vendas
     *
     * @param array $where
     * @return \G2\Entity\LancamentoVenda
     * @throws \Zend_Exception
     */
    public function retornarLancamentosVenda(array $where)
    {
        try {
            return $this->findBy('\G2\Entity\LancamentoVenda', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao tentar consultar dados. ' . $e->getMessage());
        }
    }

    /**
     * Procedimento para salvar vendas
     *
     * @param array $data
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function procedimentoSalvarVenda(array $data, $id_situacao = null, $id_evolucao = null)
    {
        $venda = $data['venda'];

        $id_situacao = $id_situacao ?: \G2\Constante\Situacao::TB_VENDA_PENDENTE;
        $id_evolucao = $id_evolucao ?: (!empty($venda['bl_renovacao'])
            ? \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO
            : \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_NEGOCIACAO);

        try {
            //Abre Transação
            $this->beginTransaction();

            $esquemaConfiguracao = new \G2\Negocio\EsquemaConfiguracao();
            $esquemaConfiguracaoEntidade = $esquemaConfiguracao->retornarEsquemaValores(array(
                'id_entidade' => !empty($venda['id_entidade'])
                    ? $venda['id_entidade']
                    : $this->sessao->id_entidade,
                'id_itemconfiguracao' => 22
            ))->getFirstMensagem();

            //registra os tramites da venda
            $this->verificaRegistroTramite($data);

            //pega os arrays e atribui nas variaveis
            $pessoa = $data['pessoa'];

            $produtos = $data['produtos'];
            $formaPagamento = $data['forma_pagamento'];
            $cartaCredito = isset($data['carta_credito']) ? $data['carta_credito'] : array();
            $disciplinas = isset($data['disciplinas']) ? $data['disciplinas'] : array();

            $id_contratoregra = null; //seta id_contratoregra como nulo
            //verifica se id_contratoregra existe no array de forma de pagamento e se ele é diferente de vazio
            if (array_key_exists('id_contratoregra', $formaPagamento) && !empty($formaPagamento['id_contratoregra'])) {
                $id_contratoregra = $formaPagamento['id_contratoregra']; //atribui o id_contratoregra na variavel
            }

            $lancamentos = null;
            //verifica se o array de lançamentos foi passado em $data
            if (array_key_exists('lancamentos', $data)) {
                $lancamentos = $data['lancamentos'];
            }

            if (isset($venda['id_venda']) && $venda['id_venda']) {
                $vendaEntity = $this->find('\G2\Entity\Venda', $venda['id_venda']);
                $vendaEntity->setDtAtualizado(new \DateTime());
                unset($venda['dt_cadastro']);
            } else {
                $vendaEntity = new \G2\Entity\Venda();
                $vendaEntity->setDt_cadastro(new \DateTime());
                $vendaEntity->setId_entidade(isset($venda) && array_key_exists('id_entidade', $venda) && $venda['id_entidade'] ? $venda['id_entidade'] : $this->sessao->id_entidade);
                $vendaEntity->setBl_ativo(true);
                $vendaEntity->setId_usuariocadastro(isset($venda) && array_key_exists('id_entidade', $venda) && $venda['id_usuario'] ? $venda['id_usuario'] : $this->sessao->id_usuario);
                $vendaEntity->setId_situacao($this->getReference('\G2\Entity\Situacao', $id_situacao));
                $vendaEntity->setId_evolucao($this->getReference('\G2\Entity\Evolucao', $id_evolucao));
                $vendaEntity->setBl_renovacao(array_key_exists('bl_renovacao', $venda) && $venda['bl_renovacao'] ? $venda['bl_renovacao'] : 0);
            }

            // Tratar os valores
            $venda['nu_valorbruto'] = str_replace(",", ".", str_replace(".", "", $venda['nu_valorbruto']));
            $venda['nu_valorliquido'] = str_replace(",", ".", str_replace(".", "", $venda['nu_valorliquido']));
            $venda['id_formapagamento'] = $formaPagamento['id_formapagamento'];

            $venda['dt_limiterenovacao'] = !empty($venda['dt_limiterenovacao'])
                ? $this->converterData($venda['dt_limiterenovacao'], 'datetime')
                : null;

            // Manter o nu_parcelas da venda
            $venda['nu_parcelas'] = !empty($venda['nu_parcelas']) ? $venda['nu_parcelas'] : count($lancamentos);

            /*
             * Verifica se existe o id_venda no array $venda e se ele é vazio OU se id_venda não existe em $venda.
             * Se o id_venda existir em $venda e estiver vazio ou se ele não existir, significa que é uma nova venda então seta os valores
             */
            if ((array_key_exists('id_venda', $venda) && empty($venda['id_venda'])) || !array_key_exists('id_venda', $venda)) {
                //seta os valores
                $venda['bl_ativo'] = 1;
                $venda['id_tipocampanha'] = 2;
                $venda['bl_contrato'] = 1;
                $venda['id_usuariocadastro'] = array_key_exists('id_usuariocadastro', $venda) && $venda['id_usuariocadastro'] ? $venda['id_usuariocadastro'] : $this->sessao->id_usuario;
                $venda['id_entidade'] = array_key_exists('id_entidade', $venda) && $venda['id_entidade'] ? $venda['id_entidade'] : $this->sessao->id_entidade;
                $venda['id_situacao'] = $id_situacao;
                $venda['id_evolucao'] = $id_evolucao;

                if ($esquemaConfiguracaoEntidade['st_valor'] == 2) { //@todo alterar para constantes
                    $venda['nu_semestre'] = $this->retornaSemestre($produtos);
                }
            }

            $venda['bl_transferidoentidade'] = !empty($venda['bl_transferidoentidade']) ? $venda['bl_transferidoentidade'] : false;

            $vendaObj = $this->save($this->entitySerialize->arrayToEntity($venda, $vendaEntity));

            //atribui o id da venda na variavel
            $idVenda = $vendaObj->getIdVenda();
            $produtoProjeto = null;
            $contratoRegraTO = null;

            //Salvar Produto
            if ($produtos) {
                //Salva o array de produto venda
                $dt_ingresso = empty($venda['dt_ingresso']) ? null : $venda['dt_ingresso'];
                $mensageiroVendaProd = $this->procedimentoSalvarArrayProdutosVenda($produtos, $idVenda, $dt_ingresso);

                //verifica se deu erro ao salvar o produto venda
                if ($mensageiroVendaProd->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $mensagem = $mensageiroVendaProd->getFirstMensagem();
                    $produtoProjeto = $mensagem['produto_projeto'];
                    $contratoRegraTO = $mensagem['ContratoRegraTO'];
                }
            }

            // Desativa os lancamentos que sao enviados para remover
            if (isset($data['lancamentos_remover']) && $data['lancamentos_remover']) {
                $this->inativaArrayLancamentos($data['lancamentos_remover']);
            }

            //Salvar Lancamentos
            if ($lancamentos) {
                //manda o array de lancamento para salvar
                $vendaTO = $this->entityToTO($vendaObj);

                // Usado para verificar se o intervalo de meses da segunda parcela foi alterado
                $nu_mesesintervalovencimento = empty($venda['nu_mesesintervalovencimento']) ? 1 : $venda['nu_mesesintervalovencimento'];
                $vendaTO->nu_mesesintervalovencimento = $nu_mesesintervalovencimento;

                $resultLancamentos = $this->procedimentoSalvarArrayLancamentosVenda($lancamentos, $vendaTO);

                if ($resultLancamentos->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    $mensagemErro = "[" . $resultLancamentos->getText() . "].<br> Erro ao tentar salvar lançamentos da venda. Verifique se não existem lançamentos com valores zero!<br>";
                    throw new \Exception($mensagemErro);
                }
            }

            //Salva o contrato
            if ($produtoProjeto) {
                $resultContrato = $this->salvarContratoVenda($idVenda, $pessoa, $contratoRegraTO, $id_contratoregra);
                if ($resultContrato->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception($resultContrato->getFirstMensagem());
                }
            }

            //Salva Carta Credito
            $cartaCreditoNegocio = new CartaCredito();
            $resultCartaCredito = $cartaCreditoNegocio->salvarArrVendaCartaCredito($cartaCredito, $idVenda);
            if ($resultCartaCredito->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception('Erro ao tentar salvar VendaCartaCredito');
            }

            /**
             * Salvar disciplinas da GRADE DE NEGOCIACAO DA GRADUACAO (PreMatriculaDisciplina)
             */
            if (array_key_exists('disciplinas', $data)) {
                $ng_premd = new \G2\Negocio\PreMatriculaDisciplina();

                if (!empty($data['renovacaoautomatica'])) {
                    $usuariocadastro = null;
                } else {
                    $usuariocadastro = $vendaObj->getId_usuariocadastro();
                }

                $ng_premd->sincronizarRegistros($idVenda, $disciplinas, $usuariocadastro);
            }

            //Commit
            $this->commit();

            //retorna o mensageiro
            return new \Ead1_Mensageiro($vendaObj, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollBack();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     * Registra os tramites da venda.
     */
    private function verificaRegistroTramite(array $params)
    {
        if (array_key_exists('alteracoesVenda', $params) && $params['alteracoesVenda']) {
            for ($i = count($params['alteracoesVenda']); $i > 0; $i--)
                $this->registraTramite($params['alteracoesVenda'][$i - 1]);
        }

        if (isset($params['venda']['id_venda']) && $params['venda']['id_venda']) {
            $vendaAnterior = $this->retornaVenda($params['venda']['id_venda']);
            if ($vendaAnterior->getIdEvolucao()->getId_evolucao() != $params['venda']['id_evolucao']) {
                $parametros['evolucao'] = array(
                    'id_venda' => $params['venda']['id_venda'],
                    'id_evolucao' => $params['venda']['id_evolucao']
                );
                $this->registraTramite($parametros);
            }

            if ($vendaAnterior->getIdSituacao()->getId_situacao() != $params['venda']['id_situacao']) {
                $parametros['situacao'] = array(
                    'id_venda' => $params['venda']['id_venda'],
                    'id_situacao' => $params['venda']['id_situacao']
                );
                $this->registraTramite($parametros);
            }
        }
    }

    /**
     * @param $params
     * @return bool
     * @throws \Exception
     */
    public function registraTramite($params)
    {
        try {

            //verifica se o tramite é do tipo ocorrencia
            if (array_key_exists('ocorrencia', $params)) {
                $params['tipo'] = \G2\Constante\TipoTramite::MATRICULA_AUTOMATICO;
                $params['st_texto'] = 'Venda gerada através da ocorrência - ' . $this->find('\G2\Entity\Ocorrencia', $params['ocorrencia']['id_ocorrencia'])->getId_ocorrencia();
                $params['id_venda'] = $params['ocorrencia']['id_venda'];
            }

            //verifica se o tramite é do tipo evolução
            if (array_key_exists('evolucao', $params)) {
                $params['tipo'] = \G2\Constante\TipoTramite::EVOLUCAO_VENDA;
                $params['st_texto'] = 'Alterando Evolução da Venda para: ' . $this->find('\G2\Entity\Evolucao', $params['evolucao']['id_evolucao'])->getSt_evolucao();
                $params['id_venda'] = $params['evolucao']['id_venda'];
            }

            //verifica se o tramite é do tipo situacao
            if (array_key_exists('situacao', $params)) {
                $params['tipo'] = \G2\Constante\TipoTramite::EVOLUCAO_VENDA;
                $params['st_texto'] = 'Alterando Situação da Venda para: ' . $this->find('\G2\Entity\Situacao', $params['situacao']['id_situacao'])->getSt_situacao();
                $params['id_venda'] = $params['situacao']['id_venda'];
            }

            //verifica se o tramite é uma alteração do atendente da venda
            if (array_key_exists('tipo', $params) && $params['tipo'] == \G2\Constante\TipoTramite::ALTERANDO_ATENDENTE_VENDA) {
                $findVenda = $this->find('\G2\Entity\Venda', $params['id_venda']);
                if (!is_null($findVenda->getIdAtendente()) && $params['id_atendente'] == $findVenda->getIdAtendente()->getId_usuario())
                    return true;
            }

            //verifica se o tramite é do tipo alterando lançamento
            if (array_key_exists('tipo', $params) && $params['tipo'] == \G2\Constante\TipoTramite::ALTERANDO_LANCAMENTO) {
                if ($params['alteracaoParcela'] == 'true') {
                    $params['st_texto'] = $params['st_texto'] . $params['st_meiopagamento'] . ' ' . $params['nu_cartao'] . ' com ' . $params['st_tipolancamento'] . ' de R$ ' . $params['nu_valor'];
                } else {
                    if ($params['st_tipolancamento'] == 'Parcela')
                        $params['st_tipolancamento'] = $params['st_tipolancamento'] . '(s)';

                    $params['st_texto'] = $params['st_texto'] . $params['st_meiopagamento'] . ' ' . $params['nu_cartao'] . ' com ' . $params['nu_parcelas'] . ' ' . $params['st_tipolancamento'] . ' de R$ ' . $params['nu_valor'];
                }
            }

            //verifica se o tramite é do tipo iniciando uma venda
            if (array_key_exists('iniciandoVenda', $params) && $params['iniciandoVenda'] == 'true') {
                $params['st_texto'] = 'Venda Iniciada';
                $params['tipo'] = \G2\Constante\TipoTramite::EVOLUCAO_VENDA;
            }

            //gera o tramite

            $objTramite = new \G2\Entity\Tramite();
            $objTramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', $params['tipo']));

            if ($this->sessao->id_usuario)
                $objTramite->setId_usuario($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));

            if ($this->sessao->id_entidade)
                $objTramite->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));

            $objTramite->setDt_cadastro(new \DateTime());
            $objTramite->setSt_tramite($params['st_texto']);


            $objTramite->setDt_cadastro(new \DateTime());
            $objTramite->setBl_visivel(false);

            $tramit = $this->save($objTramite);

            $objTramiteVenda = new \G2\Entity\TramiteVenda();
            $objTramiteVenda->setId_tramite($tramit->getId_tramite());
            $objTramiteVenda->setId_venda($params['id_venda']);
            $this->save($objTramiteVenda);
            return true;
        } catch (\Exception $e) {
            throw new \Exception('Erro ao salvar tramite da venda - ' . $e->getMessage());
        }

    }

    public function retornaVenda($id_venda)
    {
        return $this->find('\G2\Entity\Venda', $id_venda);
    }

    //TODO Ver isso depois
//    public function retornarProjetoContrato($id_venda)
//    {
//        $orm = new \ContratoRegraORM();
//        $select = $orm->select()->from(array("viu" => "vw_gerarcontrato"))
//            ->setIntegrityCheck(false)
//            ->joinInner(array("vd" => "tb_venda"), "vd.id_venda = viu.id_venda")
//            ->joinInner(array("vp" => "tb_vendaproduto"), "vp.id_venda = vd.id_venda")
//            ->joinInner(array("ppp" => "tb_produtoprojetopedagogico"), "ppp.id_produto = vp.id_produto")
//            ->joinInner(array("pp" => "tb_projetopedagogico"), "pp.id_projetopedagogico = ppp.id_projetopedagogico")
//            ->joinInner(array("ctr" => "tb_contratoregra"), "ctr.id_contratoregra = pp.id_contratoregra")
//            ->joinInner(array("ts" => "tb_textosistema"), "ts.id_textosistema = ctr.id_contratomodelo")
//            ->where("pvp.id_venda = " . $id_venda);
//        $produtos['projetos'] = $orm->fetchAll($select)->toArray();
//    }

    /**
     * PARA ENTIDADES COM LINHA DE NEGOCIO GRADUAÇÃO (2)
     * Retorna o semestre da venda - Para primeira venda (sem id_matricula) retorna 1
     * @param $produtos
     * @return bool|int
     */
    public function retornaSemestre($produtos)
    {
        try {
            $nu_semestre = 1;
            if (is_array($produtos) && array_key_exists('id_matricula', $produtos[0])) {
                if ($produtos[0]['id_matricula'] != null) {
                    $nu_semestre = $this->em->getRepository('\G2\Entity\VwMatricula')->proximoSemestre($produtos[0]['id_matricula']);
                }
            }
            return $nu_semestre;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Salva produtos vinculados a venda
     *
     * @param array $produtos
     * @param integer $id_venda
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function procedimentoSalvarArrayProdutosVenda(array $produtos, $id_venda, $dt_ingresso = null)
    {
        try {
            $produtoNegocio = new \G2\Negocio\Produto();
            $contratoNegocio = new \G2\Negocio\Contrato();
            $vendaBO = new \VendaBO();

            $contratoRegraTO = null;
            //Monta o array de venda produto
            $arrVendaProduto = array();
            //define que não é projeto
            $produtoProjeto = false;
            $venda = $this->find('\G2\Entity\Venda', $id_venda);

            if ($produtos) {
                //Percorre o array de produtos
                foreach ($produtos as $produto) {
                    //procura os dados do produto
                    $resultProduto = $produtoNegocio->findProduto($produto['id_produto']);

                    //verifica se o produto é do tipo Projeto Pedagogico
                    if ($resultProduto->getId_tipoproduto() && $resultProduto->getId_tipoproduto()->getId_tipoproduto() == \TipoProdutoTO::PROJETO_PEDAGOGICO && !$produtoProjeto) {
                        $produtoProjeto = true; //se for do tipo projeto ele seta a variavel como true
                        //busca dados da regra de contrato
                        $contratoRegra = $contratoNegocio->buscarRegraContratoProduto($resultProduto->getId_produto(), ($this->sessao->id_entidade ? $this->sessao->id_entidade : $venda->getId_entidade()));
                        if ($contratoRegra->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            throw new \Exception($contratoRegra->getFirstMensagem());
                        }
                        //Pega a mensagem do contrato regra
                        $contratoRegraTO = $contratoRegra->getFirstMensagem();
                    }

                    $vendaProd = array(
                        'id_produto' => $resultProduto->getId_produto(),
                        'nu_valorbruto' => $produto['nu_valorbruto'],
                        'nu_valorliquido' => $produto['nu_valorliquido'],
                        'id_venda' => $id_venda,
                        'nu_desconto' => ((float)$produto['nu_valorbruto'] - (float)$produto['nu_valorliquido']),
                        'id_campanhacomercial' => $produto['id_campanhacomercial'],
                        'id_turma' => $produto['id_turma'],
                        'id_tiposelecao' => isset($produto['id_tiposelecao']) ? $produto['id_tiposelecao'] : null,
                        'bl_ativo' => true,
                        'id_matricula' => array_key_exists('id_matricula', $produto) && isset($produto['id_matricula']) ? $produto['id_matricula'] : null,
                        'dt_ingresso' => $dt_ingresso ? $this->converterData($dt_ingresso) : null,
                    );

                    if (array_key_exists('id_vendaproduto', $produto) && $produto['id_vendaproduto']) {
                        //verifica se existe o registro
                        $vendaProdutoResult = $this->findOneBy('\G2\Entity\VendaProduto', array('id_vendaproduto' => $produto['id_vendaproduto']));
                        if ($vendaProdutoResult)
                            $vendaProd['id_vendaproduto'] = $produto['id_vendaproduto'];
                    }

                    //Instancia a TO de VendaProduto
                    $vendaProdutoTO = new \VendaProdutoTO();
                    //monta o array dentro da TO
                    $vendaProdutoTO->montaToDinamico($vendaProd);

                    //Atribui numa posição do array a TO montada
                    $arrVendaProduto[] = $vendaProdutoTO;
                }
            }

            //Salva o array de produto venda
            $result = $this->salvarArrVendaProduto($arrVendaProduto);

            if ($result->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                $return = $result->getCodigo();
                $message = '';
                foreach ($return as $error) {
                    if ($error['error']) {
                        $message .= ($message == '' ? '' : ' | ') . $error['message'];
                    }
                }
                throw new \Exception($message);
            }

            return new \Ead1_Mensageiro(array(
                'produto_projeto' => $produtoProjeto,
                'ContratoRegraTO' => $contratoRegraTO,
                'VendaProduto' => $result->getFirstMensagem()
            ), \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao tentar salvar produtos da venda. ' . $e->getMessage());
        }
    }

    /**
     * @param $arrVendaProdutoTO
     * @return \Ead1_Mensageiro
     */
    public function salvarArrVendaProduto($arrVendaProdutoTO)
    {
        try {
            $this->beginTransaction();
            if (empty($arrVendaProdutoTO)) {
                THROW new \Zend_Exception('O array de VendaProdutoTO veio vazio!');
            }
            $arrayReturn = array();
            $warning = false;
            foreach ($arrVendaProdutoTO as $vendaProdutoTO) {
                if (!($vendaProdutoTO instanceof \VendaProdutoTO)) {
                    THROW new \Zend_Exception('Um dos items do array não é instancia de VendaProdutoTO');
                }
                $return = $this->salvarTbVendaProduto($vendaProdutoTO);

                if (!($return instanceof \G2\Entity\VendaProduto)) {
                    $warning = true;
                    $arrayReturn[] = array('message' => $return, 'error' => true);
                } else {
                    $arrayReturn[] = array('produto' => $return, 'error' => false);
                }

            }
            $this->commit();
            return new \Ead1_Mensageiro('Produtos da Venda Salvos com Sucesso!', $warning ? \Ead1_IMensageiro::AVISO : \Ead1_IMensageiro::SUCESSO, $arrayReturn);
        } catch (\Exception $e) {
            //\Zend_Debug::dump($e->getMessage()); die;
            $this->rollback();
            //throw $e;
            return new \Ead1_Mensageiro('Erro ao Salvar os Produtos da Venda!', \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * @param \VendaProdutoTO $vendaProdutoTO
     * @return bool|\Doctrine\Common\Proxy\Proxy|G2\Entity\VendaProduto|null|object|string
     */
    public function salvarTbVendaProduto(\VendaProdutoTO $vendaProdutoTO)
    {
        try {
            if ($vendaProdutoTO->getId_vendaproduto()) {
                $vendaProduto = $this->getReference('\G2\Entity\VendaProduto', $vendaProdutoTO->getId_vendaproduto());
            } else {
                $vendaProduto = new \G2\Entity\VendaProduto();
            }
            $vendaProduto->setId_venda($this->getReference('\G2\Entity\Venda', $vendaProdutoTO->getId_venda()));
            $vendaProduto->setid_tiposelecao($vendaProdutoTO->getid_tiposelecao() ? $this->getReference('\G2\Entity\TipoSelecao', $vendaProdutoTO->getid_tiposelecao()) : null);
            $vendaProduto->setId_campanhacomercial($vendaProdutoTO->getId_campanhacomercial() ? $this->getReference('\G2\Entity\CampanhaComercial', $vendaProdutoTO->getId_campanhacomercial()) : null);
            $vendaProduto->setId_matricula($vendaProdutoTO->getId_matricula() ? $this->getReference('\G2\Entity\Matricula', $vendaProdutoTO->getId_matricula()) : null);
            $vendaProduto->setId_produtocombo($vendaProdutoTO->getId_produtocombo() ? $this->getReference('\G2\Entity\ProdutoCombo', $vendaProdutoTO->getId_produtocombo()) : null);
            $vendaProduto->setId_turma($vendaProdutoTO->getId_turma() ? $this->getReference('\G2\Entity\Turma', $vendaProdutoTO->getId_turma()) : null);
            $vendaProduto->setId_situacao($vendaProdutoTO->getId_situacao() ? $this->getReference('\G2\Entity\Situacao', $vendaProdutoTO->getId_situacao()) : null);
            $vendaProduto->setId_produto($this->getReference('\G2\Entity\Produto', $vendaProdutoTO->getId_produto()));
            $vendaProduto->setNu_desconto($vendaProdutoTO->getNu_desconto());
            $vendaProduto->setNu_valorliquido($vendaProdutoTO->getNu_valorliquido());
            $vendaProduto->setNu_valorbruto($vendaProdutoTO->getNu_valorbruto());
            $vendaProduto->setDt_cadastro($vendaProdutoTO->getDt_cadastro() ? $vendaProdutoTO->getDt_cadastro() : new \DateTime());
            $vendaProduto->setId_avaliacaoaplicacao($vendaProdutoTO->getId_avaliacaoaplicacao());
            $vendaProduto->setBl_ativo($vendaProdutoTO->getBl_ativo());
            $vendaProduto->setDt_ingresso($vendaProdutoTO->getDt_ingresso());

            if (!$vendaProduto->getId_vendaproduto()) {
                $vendaBO = new \VendaBO();
                $verificar = $vendaBO->verificarProdutoVendaUnica($vendaProdutoTO->getId_venda(), $vendaProdutoTO->getId_produto());
                if ($verificar->getTipo() == \Ead1_IMensageiro::ERRO) {
                    THROW new \Zend_Exception($verificar->getFirstMensagem());
                }
            }

            $this->save($vendaProduto);

            return $vendaProduto;
        } catch (\Exception $ex) {
            /*    \Zend_Debug::dump($ex->getMessage());
            die;*/
            return $ex->getMessage();
        }
    }

    /**
     * Inativa os lancamentos passados pelo array
     *
     * @param array $arrayIdLancementos array com id_lancamento para serem inativados. Ex.: array(1,2,3,4)
     * @return array Retorna array vazio ou array de objetos
     * @throws \Zend_Exception
     */
    public function inativaArrayLancamentos(array $arrayIdLancementos)
    {
        try {
            $arrReturn = array();
            //verifica se é um array
            if (is_array($arrayIdLancementos)) {

                //inicia  transaction
                $this->beginTransaction();

                //percorre o array dos id_lacanemtnos
                foreach ($arrayIdLancementos as $idLancamento) {
                    //Busca lancamento
                    $lancamento = $this->find('\G2\Entity\Lancamento', $idLancamento);
                    $lancamento->setBlAtivo(false); //seta o bl_ativo para false
                    //Verifica se salvou e atribui  a entity a uma posição no array
                    if ($this->save($lancamento)) {
                        $arrReturn[] = $lancamento;
                    } else { //verifica se o save() retorna false e cria um exception
                        throw new \Exception("Erro ao inativar lancamento id:" . $idLancamento);
                    }
                }
                //commit
                $this->commit();
            }

            //retorna o array com as entities alteradas ou array vazio
            return $arrReturn;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao tentar inativar lancamentos. ' . $e->getMessage());
        }
    }

    /**
     * Percorre o array de lançamentos e envia para a BO para salvar os dados
     *
     * @param array $lancamentos
     * @param \VendaTO $vendaTO
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function procedimentoSalvarArrayLancamentosVenda(array $lancamentos, $vendaTO)
    {
        try {
            $vendaBO = new \VendaBO();

            //Tratando dados de lançamento
            $arrOrdem = array();
            $arrLancamentoTO = array();
            $hasLancamentoEntrada = false;
            if ($lancamentos) {
                $venc_entrada = null;
                $venc_parcela = null;

                //Percorre o array de lançamentos
                foreach ($lancamentos as $index => $lancamento) {
                    if ($lancamento['nu_valor']) {
                        //verifica se veio setado o responsavel
                        if (empty($lancamento['id_usuariolancamento']) && empty($lancamento['id_entidadelancamento'])) {
                            throw new \Exception('Nenhum responsável setado para lançamento!');
                        }

                        //trata o valor
                        $nu_valor = floatval(str_replace(',', '.', $lancamento['nu_valor']));
                        //intancia a TO de lançamento e seta os atributos
                        $lancamentoEntity = new VwVendaLancamento();


                        $lancamentoEntity->setIdVenda($lancamento['id_venda'] ? $lancamento['id_venda'] : $vendaTO->getId_venda());
                        $lancamentoEntity->setIdMeiopagamento($lancamento['id_meiopagamento']);
                        $lancamentoEntity->setNuOrdem(isset($lancamento['nu_ordem']) ? $lancamento['nu_ordem'] : null);
                        $lancamentoEntity->setNuValor($nu_valor);
                        $lancamentoEntity->setBlQuitado($lancamento['bl_quitado'] ? $lancamento['bl_quitado'] : false);
                        $lancamentoEntity->setIdUsuariocadastro($lancamento['id_usuariocadastro'] ? $lancamento['id_usuariocadastro'] : $this->sessao->id_usuario);
                        $lancamentoEntity->setIdEntidade($lancamento['id_entidade'] ? $lancamento['id_entidade'] : $this->sessao->id_entidade);
                        $lancamentoEntity->setBlOriginal(array_key_exists('bl_original', $lancamento) && $lancamento['bl_original'] ? $lancamento['bl_original'] : false);
                        $lancamentoEntity->setNuQuitado((isset($lancamento['nu_quitado']) ? $lancamento['nu_quitado'] : null));
                        $lancamentoEntity->setIdTipolancamento($lancamento['id_tipolancamento']);
                        $lancamentoEntity->setBlEntrada($lancamento['bl_entrada'] == 'true' ? true : false);
                        $lancamentoEntity->setNuCartao((isset($lancamento['nu_cartao']) ? $lancamento['nu_cartao'] : null));
                        $lancamentoEntity->setBlAtivo(isset($lancamento['bl_ativo']) ? $lancamento['bl_ativo'] : true);
                        $lancamentoEntity->setNu_cobranca(!empty($lancamento['nu_ordem']) ? $lancamento['nu_ordem'] : ($index + 1));

                        if ($lancamentoEntity->getBlEntrada()) {
                            $hasLancamentoEntrada = $lancamentoEntity->getBlEntrada();
                        }

                        //Seta o id_usuario para responsavel
                        $lancamentoEntity->setIdUsuariolancamento($lancamento['id_usuariolancamento'] ?: null);
                        $lancamentoEntity->setIdEntidadelancamento($lancamento['id_entidadelancamento'] ?: null);

                        //Seta o id do lançamento
                        if (array_key_exists('id_lancamento', $lancamento) && !empty($lancamento['id_lancamento'])) {
                            $lancamentoEntity->setIdLancamento($lancamento['id_lancamento']);
                        }

                        $dt_cadastro = date('Y-m-d H:i:s');
                        //Seta a data de cadastro
                        if (array_key_exists('dt_cadastro', $lancamento) && !empty($lancamento['dt_cadastro'])) {
                            $dt_cadastro = new \Zend_Date($lancamento['dt_cadastro']);
                        }
                        $lancamentoEntity->setDtCadastro($dt_cadastro);

                        //Formata a data de vencimento
                        if (array_key_exists('dt_vencimento', $lancamento) && !empty($lancamento['dt_vencimento'])) {
                            //seta a data de vencimento na TO
                            $dt_vencimento = $this->converteDataBancoZendDate($lancamento['dt_vencimento'])->toString('yyyy-MM-dd');
                            $lancamentoEntity->setDtVencimento($dt_vencimento);

                            //verifica se o meio de pagamento é cartão
                            if ($lancamentoEntity->getIdMeiopagamento() != \MeioPagamentoTO::CARTAO) {
                                $lancamentoEntity->setStCoddocumento((isset($lancamento['nu_documento']) ? $lancamento['nu_documento'] : null));
                                //comentado a pedido do Toffolo, esse dt_prevquitado não tem necessidade de ser preenchido aqui.
//                                $lancamentoEntity->setDtPrevquitado($dt_vencimento);
                            }
                        }

                        //verifica se tem a dt_quitado
                        if (array_key_exists('dt_quitado', $lancamento) && !empty($lancamento['dt_quitado'])) {
                            //verifica se o meio de pagamento é cartão
                            if ($lancamentoEntity->getIdMeiopagamento() != \MeioPagamentoTO::CARTAO) {

                                if (is_array($lancamento['dt_quitado']) && array_key_exists('date', $lancamento['dt_quitado'])) {
                                    $dt_quitado = $lancamento['dt_quitado']['date'];
                                } else {
                                    $dt_quitado = $lancamento['dt_quitado'];
                                }

                                //seta a data na TO
                                $lancamentoEntity->setDtQuitado($this->converteDataBancoZendDate($dt_quitado)->toString('yyyy-MM-dd'));
                                //seta como true definindo o o lancamento como quitado
                                $lancamentoEntity->setBlQuitado(true);
                            }
                        }


                        // atribui o numero da ordem numa posição do array
                        $arrOrdem[] = $lancamento['nu_ordem'];

                        // atribui a TO numa posição do array
                        // Transformando todas as entities em TO
                        $arrLancamentoTO[] = $this->entityToTO($lancamentoEntity);

                        if ($lancamentoEntity->getBl_entrada()) {
                            $venc_entrada = \G2\Utils\Helper::converterData($lancamentoEntity->getDt_vencimento(), 'datetime');
                        } else if (!$venc_parcela) {
                            $venc_parcela = \G2\Utils\Helper::converterData($lancamentoEntity->getDt_vencimento(), 'datetime');
                        }
                    }
                }

                /**
                 * @history GII-8685
                 * O sistema deverá registrar o log do usuário que realizou a alteração do intervalo de vencimento da
                 * segunda parcela no Relatório de Logs do sistema G2 ( Relatórios » Holding » Relatório de Logs)
                 */
                $this->verificarAlteracaoIntervaloVencimento($vendaTO, $venc_entrada, $venc_parcela);
            }

            /*
             * Dados passados para uma função refatorada e
             * sem chamadas para metodos antigos encontrados na BO's.
             * */
            if ($arrLancamentoTO) {
                $return = $this->salvarArrayLancamentosCompleto($arrLancamentoTO, $vendaTO, false, false, $arrOrdem, 1, $hasLancamentoEntrada);
                return $return;
            } else {
                return new \Ead1_Mensageiro('', \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $e) {
            //throw new \Zend_Exception('Erro ao tentar salvar lançamentos da venda. Verifique se existem lançamentos com valor zero.');
//            throw new \Zend_Exception($e->getMessage());
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param \VendaTO $vendaTO
     * @param \DateTime $venc_entrada
     * @param \DateTime $venc_parcela
     */
    public function verificarAlteracaoIntervaloVencimento($vendaTO, $venc_entrada, $venc_parcela)
    {
        // Descobrir a quantidade de meses de intervalo da Entrada para a Segunda Parcela
        if ($vendaTO->getId_venda() && $venc_entrada && $venc_parcela) {
            $diff = $venc_parcela->diff($venc_entrada);
            $intervalo_old = (($diff->format('%y') * 12) + $diff->format('%m'));

            // Verificar se o intervalo de meses da segunda parcela foi alterado
            $intervalo_new = empty($vendaTO->nu_mesesintervalovencimento) ? 1 : $vendaTO->nu_mesesintervalovencimento;

            // Se for diferente, então registra o LOG
            if ($intervalo_old != $intervalo_new) {
                $sess = \Ead1_Sessao::getObjetoSessaoGeral();
                $ng_log = new \G2\Negocio\Log();

                // É necessário criar um Log manual simulando o campo, porquê o campo é apenas lógico
                // (não existe no banco de dados)
                $ng_log->salvarLogIndividual(array(
                    'id_funcionalidade' => \G2\Constante\Funcionalidade::NEGOCIACAO_GRADUACAO,
                    'id_entidade' => $sess->getid_entidade(),
                    'id_perfil' => $sess->getid_perfil(),
                    'id_usuario' => $sess->getid_usuario(),
                    'st_antes' => $intervalo_old,
                    'st_depois' => $intervalo_new,
                    'st_tabela' => 'tb_venda',
                    'id_tabela' => $vendaTO->getId_venda(),
                    'st_coluna' => 'nu_mesesintervalovencimento',
                    'dt_cadastro' => new \DateTime(),
                    'id_operacao' => $this->getReference('\G2\Entity\OperacaoLog',
                        \G2\Constante\OperacaoLog::UPDATE),
                    'st_motivo' => 'update'
                ));
            }
        }
    }

    /**
     * @description Metodo que exclui os lançamentos de uma venda e cadastra um array de Lançamentos e seus vinculos com a venda
     * @param array $arrLancamentoTO
     * @param \VendaTO $vendaTO
     * @param bool|false $gerarDataPrimeiroLancamento | indica que sera gerada a data do primeiro lançamento
     * @param bool|true $gerarDatasVencimento | Indica se sera gerado automaticamente os vencimentos dos lançamentos pós o primeiro lançamento
     * @param null $arrOrdens
     * @param int $id_modelovenda
     * @param bool|false $hasLancamentoEntrada
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function salvarArrayLancamentosCompleto($arrLancamentoTO, \VendaTO $vendaTO, $gerarDataPrimeiroLancamento = false, $gerarDatasVencimento = true, $arrOrdens = null, $id_modelovenda = 1, $hasLancamentoEntrada = false)
    {
        try {
            $this->beginTransaction();
            //Variavel que indica se já voram excluidos os vinculos
            $delected = false;
            //Variavel que indica se é o primeiro lançamento para cadastrar como entrada
            $first = true;
            $setaRecorrente = true;
            $data_entrada_recorrente = new \Zend_Date(null, \Zend_Date::ISO_8601);
            $data_entrada = new \Zend_Date(null, \Zend_Date::ISO_8601);

            $efTO = $this->findOneBy('\G2\Entity\EntidadeFinanceiro', array('id_entidade' => $vendaTO->getId_entidade()));

            $lancamento = $arrLancamentoTO[0];
            if ($id_modelovenda == \G2\Entity\ModeloVenda::ASSINATURA && $lancamento instanceof \LancamentoTO) {
                $data_entrada_recorrente = $lancamento->getDt_vencimento();
                $data_entrada = $lancamento->getDt_vencimento();
            } else {
                if ($efTO->getNu_diaspagamentoentrada()) {
                    $data_entrada->addDay($efTO->getNu_diaspagamentoentrada());
                }
            }
            $dia_vencimento = $vendaTO->getNu_diamensalidade();

            //Apagando os dados da TransacaoLancamento
            $this->em->getRepository('\G2\Entity\Venda')->deletarTransacaoLancamento($vendaTO->getid_venda());

            //Apagando os dados da LancamentoVenda
            $this->em->getRepository('\G2\Entity\LancamentoVenda')->deletarLancamentoVenda($vendaTO->getId_venda());

            $arrLancamentos = $this->em->getRepository('\G2\Entity\VwVendaLancamento')->findBy(array('id_venda' => $vendaTO->getId_venda()));

            //Excluindo Lançamentos
            if ($arrLancamentos) {
                $arrWhere = [];
                foreach ($arrLancamentos as $vwLancamento) {
                    $arrWhere[] = $vwLancamento->getIdLancamento();
                }
                $where = "id_lancamento in (" . implode(" ,", $arrWhere) . ")";
                $this->em->getRepository('\G2\Entity\Lancamento')->deletarLancamento($where);
            }


            foreach ($arrLancamentoTO as $index => $lancamentoTO) {

                $lancamentoTO->setId_usuariocadastro($lancamentoTO->getId_usuariocadastro() ? $lancamentoTO->getId_usuariocadastro() : $lancamentoTO->getSessao()->id_usuario);
                $lancamentoVendaTO = new \LancamentoVendaTO();
                //validar lançamentos de entrada e vencimento
                if ($first) {
                    $first = false;

                    //verifica se existe algum lançamento como entrada
                    if ($hasLancamentoEntrada) {
                        //se existir seta o bl_entrada como o valor que esta na to
                        $lancamentoVendaTO->setBl_entrada($lancamentoTO->getBl_entrada());
                    } else {//se não existir assume o primeiro como a entrada
                        $lancamentoVendaTO->setBl_entrada(true);
                    }

                    //Verificando se sera gerada a data do primeiro lançamento
                    if ($gerarDataPrimeiroLancamento) {
                        $lancamentoTO->setDt_vencimento($data_entrada);
                        $dia_vencimento = $data_entrada->toString('dd');
                    } else {
                        $data_entrada = new \Zend_Date($lancamentoTO->getDt_vencimento(), \Zend_Date::ISO_8601);
                    }
                } else {
                    //transforma a data de vencimento num zend_date
                    $dt_vencimento = $this->converteDataBancoZendDate($lancamentoTO->getDt_vencimento());
                    //verifica se existe mais de um cartao de credito e se a data da entrada é igual a data de vencimento e se a ordem é 1
                    if ($lancamentoTO->getnu_cartao() > 1 && $data_entrada->toString('YYYY-MM-dd') == $dt_vencimento->toString('YYYY-MM-dd') && $arrOrdens[$index] == 1) {
                        $lancamentoVendaTO->setBl_entrada(true); // se for seta como true a entrada
                    } else {
                        //verifica se existe algum lançamento como entrada
                        if ($hasLancamentoEntrada) {
                            //se existir seta o bl_entrada como o valor que esta na to
                            $lancamentoVendaTO->setBl_entrada($lancamentoTO->getBl_entrada());
                        } else {
                            $lancamentoVendaTO->setBl_entrada(false);
                        }
                    }

                    // Coloca o vencimento do Recorrente para o dia da compra
                    if ($lancamentoTO->getId_meiopagamento() == \MeioPagamentoTO::RECORRENTE && $setaRecorrente) {
                        $dia_vencimento = $data_entrada_recorrente->toString('dd');
                        $data_entrada = $data_entrada_recorrente;
                        $setaRecorrente = false;
                    }
                    //Verificando se serão geradas as datas do restante dos lançamentos
                    if ($gerarDatasVencimento) {
                        $data_entrada = $data_entrada->addMonth(1);
                        $mes_lancamento_atual = (int)$data_entrada->toString('MM');
                        $data_entrada = $data_entrada->setDay($dia_vencimento);
                        //Se o mes for diferente do definido quer dizer que o dia do mes anterior é maior que o do mes atual
                        //Colocando o vencimento para o ultimo dia do mes atual
                        if ((int)$data_entrada->toString('MM') != $mes_lancamento_atual) {
                            $data = clone $data_entrada;
                            $data = $data->setDay(1);
                            $data = $data->setMonth($mes_lancamento_atual);
                            $data = $data->addMonth(1);
                            $data = $data->subDay(1);
                            $ultimo_dia_mes = (int)$data->toString('dd');
                            $data_entrada = $data_entrada->setDay($ultimo_dia_mes);
                            $data_entrada = $data_entrada->setMonth($mes_lancamento_atual);
                        }
                        $lancamentoTO->setDt_vencimento($data_entrada);
                    }
                }

                $lancamentoTO->setBl_quitado($lancamentoTO->getBl_quitado() ? $lancamentoTO->getBl_quitado() : false);
                $lancamentoTO->setId_usuariocadastro($lancamentoTO->getId_usuariocadastro() ? $lancamentoTO->getId_usuariocadastro() : $lancamentoTO->getSessao()->id_usuario);
                $lancamentoTO->setId_entidade($lancamentoTO->getId_entidade() ? $lancamentoTO->getId_entidade() : $vendaTO->getId_entidade());
                //Cadastra Lançamento

                $id_lancamento = $this->salvarLancamento($lancamentoTO->toArray());
                if (!$id_lancamento) {
                    throw new \Exception("Erro ao salvar lancamento.");
                }

                //Monta o TO  de lançamento venda
                $lancamentoVendaTO->setId_lancamento($id_lancamento->getIdLancamento());
                $lancamentoVendaTO->setId_venda($vendaTO->getId_venda());
                $lancamentoVendaTO->setNu_ordem((!empty($arrOrdens) ? $arrOrdens[$index] : $index));

                //Cadastro relacionamento entre lançamentos da venda
                if (!$this->salvarLancamentoVenda($lancamentoVendaTO->toArray(), $id_lancamento)) {
                    throw new \Exception("Erro ao salvar vinculo de Lançamento com Venda.");
                }

            }

            $this->commit();
            $this->mensageiro->setMensageiro('Lançamentos Cadastrados Com Sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $e) {
            $this->rollBack();
            $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
            throw $e;
        } catch (\Zend_Exception $e) {
            $this->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Lançamentos', \Ead1_IMensageiro::ERRO, $e);
            throw $e;
        } catch (\Exception $e) {
            $this->rollBack();
            $this->mensageiro->setMensageiro('Erro ao Cadastrar Lançamentos', \Ead1_IMensageiro::ERRO, $e);
            throw $e;
        }
        return $this->mensageiro;
    }

    /**
     * Salva dados do contrato da venda
     *
     * @param integer $id_venda
     * @param array $pessoa
     * @param \ContratoRegraTO $contratoRegraTO
     * @param int $idContratoRegra
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function salvarContratoVenda($id_venda, array $pessoa, \ContratoRegraTO $contratoRegraTO, $idContratoRegra = null)
    {
        try {


            $objContrato = new \G2\Entity\Contrato();
            $negocio = new \G2\Negocio\Negocio();
            $retorno = null;
            //verifica se a venda já gerou contrato ativo para a entidade da sessao
            $contrato = $negocio->findOneBy('\G2\Entity\Contrato', array('id_venda' => $id_venda));
            //Verifica se o contrato existe e não faz nada
            if ($contrato) {
                $contrato->setId_entidade($contrato->getId_entidade() ? $contrato->getId_entidade() : $this->sessao->id_entidade);
                $retorno = $negocio->save($contrato);
                if (!$retorno) {
                    throw new \Exception('Erro ao tentar editar contrato.');
                }
            } else {

                $venda = $this->find('\G2\Entity\Venda', $id_venda);

                //cadastra um novo contrato
                $objContrato->setId_entidade($this->sessao->id_entidade ? $this->sessao->id_entidade : $venda->getId_entidade());
                $objContrato->setId_venda($this->em->getReference('\G2\Entity\Venda', $id_venda));
                $objContrato->setId_usuariocadastro($this->sessao->id_usuario ? $this->sessao->id_usuario : $venda->getId_usuariocadastro());
                $objContrato->setId_usuario($pessoa['id_usuario']);
                $objContrato->setId_contratoregra($idContratoRegra ? $this->getReference('\G2\Entity\ContratoRegra', $idContratoRegra) : $this->getReference('\G2\Entity\ContratoRegra', $contratoRegraTO->getId_contratoregra()));
                $objContrato->setId_evolucao($this->em->getReference('\G2\Entity\Evolucao', 1));
                $objContrato->setId_situacao($this->em->getReference('\G2\Entity\Situacao', 47));
                $objContrato->setId_textosistema(null);
                $objContrato->setDt_cadastro(new \DateTime());
                $objContrato->setBl_ativo(true);
                $objContrato->setNu_codintegracao(null);
                $retorno = $negocio->save($objContrato);
                if (!$retorno->getId_contrato()) {
                    throw new \Exception('Erro ao tentar cadastrar contrato.');
                }
            }

            $retorno = new \Ead1_Mensageiro($this->entityToTO($retorno), \Ead1_IMensageiro::SUCESSO);
            return $retorno;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados do contrato. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_produtovenda
     *
     * @param array $where
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwProdutoVenda(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VwProdutoVenda', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados de vw_produtovenda. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_vendalancamento
     *
     * @param array $where
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaVwVendaLancamento(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VwVendaLancamento', $where, array('nu_ordem' => 'ASC'));
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar dados de vw_vendalancamento. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_vendausuario
     *
     * @param array $where
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwVendaUsuario(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VwVendaUsuario', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar dados de vw_vendausuario. " . $e->getMessage());
        }
    }

    /**
     * Salva a confirmação de pagamento de vários lançamentos de acordo com o array passado e altera a evolução e a situação da venda
     *
     * @param array $lancamentos array de Lançamentos contendo obrigatoriamente pelo menos as seguintes chaves em cada array
     * - id_meiopagamento
     * - id_venda
     * - id_lancamento
     * @return array
     * @throws \Zend_Exception
     */
    public function salvarConfirmacaoPagamentoByArrayLancamentos(array $lancamentos)
    {
        try {
            //Instancia as classes necessarias;
            $dao = new \VendaDAO();
            $recebimentoNegocio = new Recebimento(); //Negocio
            $arrReturn = array();

            //verifica se tem lancamento
            if ($lancamentos) {
                //abre transação
                $dao->beginTransaction();
                $id_venda = null;
                //Percorre os lancamentos
                foreach ($lancamentos as $lancamento) {
                    //Recupera o id_meiopagamento
                    $id_meiopagamentof = $lancamento['id_meiopagamento'];
                    $id_venda = $lancamento['id_venda'];
                    //chama o método correto
                    switch ($id_meiopagamentof) {
                        case 1:
                            break; //cartão de crédito
                        case 7://cartão de debito
                            break;
                        case 4://cheque
                            break;
                        case 11://cartão recorrente
                            break;
                        case 3://pagamento em dinheiro
                            $result = $recebimentoNegocio->pagarDinheiro($lancamento['id_lancamento']);
                            if ($result->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                                throw new \Exception($result->getText());
                            }
                            $arrReturn['lancamentos'][] = $result->getFirstMensagem();
                            break;
                        default ://qualquer outra forma de pagamento
                            break;
                    }
                }
                //Confirma o recebimento na venda
                if ($id_venda) {
                    $resultVenda = $recebimentoNegocio->confirmarRecebimento($id_venda);
                    if ($resultVenda->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                        throw new \Exception($resultVenda->getText());
                    }
                    $arrReturn['venda'] = $this->retornarVwVendaUsuarioByVenda($id_venda);
                }


                $dao->commit();
                return $arrReturn;
            }
        } catch (\Exception $e) {
            $dao->rollBack();
            throw new \Zend_Exception('Erro ao tentar salvar confirmação de pagamento. ' . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_vendausuario por id_venda
     *
     * @param integer $id_venda
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwVendaUsuarioByVenda($id_venda)
    {
        try {
            return $this->find('\G2\Entity\VwVendaUsuario', $id_venda);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar dados de vw_vendausuario. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_vendasrecebimento buscando pelo id_venda
     *
     * @param $id_venda integer
     * @return mixed VwVendasRecebimento or NULL
     * @throws \Zend_Exception
     */
    public function retornaDadosVwVendasRecebimentoByVenda($id_venda)
    {
        try {
            return $this->find('\G2\Entity\VwVendasRecebimento', $id_venda);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar buscar dados de VwVendasRecebimento." . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_vendasrecebimento
     *
     * @param array $where array par chave valor
     * @param array $order
     * @param integer $limit
     * @param integer $offset
     * @return mixed VwVendasRecebimento or NULL
     * @throws \Zend_Exception
     */
    public function retornaDadosVwVendasRecebimento(array $where = array(), array $order = array(), $limit = NULL, $offset = NULL)
    {
        try {
            return $this->findBy('\G2\Entity\VwVendasRecebimento', $where, $order, $limit, $offset);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar buscar dados de VwVendasRecebimento." . $e->getMessage());
        }
    }

    /**
     * Método para retornar os dados da consulta do robo de ativação automatica
     * @param array $where
     * @param array $order
     * @param integer $limit
     * @return mixed
     * @throws \Exception
     */
    public function retornarDadosRoboAtivacaoAutomatica(array $where = array(), array $order = array(), $limit = NULL)
    {
        try {
            $resultQuery = $this->getRepository('\G2\Entity\VwVendasRecebimento')
                ->retornarDadosVwVendasRecebimentos($where, $order, $limit);
            return $resultQuery;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao consultar dados retornarDadosRoboAtivacaoAutomatica(). " . $e->getMessage());

        }
    }

    /**
     * Retorna dados da carta de crédito pelo id da venda
     *
     * @param integer $idVenda
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarVendaCartaCreditoByVenda($idVenda)
    {
        try {
            $cartaCreditoNegocio = new CartaCredito();
            return $cartaCreditoNegocio->retornarDadosVendaCartaCredito(array('id_venda' => $idVenda), true);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar dados de VendaCartaCredito by Venda. " . $e->getMessage());
        }
    }

    public function verificaMatriculasFreePassDisponiveis($id_turma)
    {
        $objetoTurma = $this->find('\G2\Entity\Turma', $id_turma);
        $countMatriculaFreePass = count($this->findBy('\G2\Entity\VwMatricula', array('id_turma' => $id_turma, 'id_tiporegracontrato' => \G2\Constante\TipoRegraContrato::TB_TIPO_REGRA_CONTRATO_FREE_PASS, 'id_evolucao' => \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO)));
        if ($countMatriculaFreePass < $objetoTurma->getNu_maxfreepass()) {
            $retorno = array('TotalFreePass' => $objetoTurma->getNu_maxfreepass(), 'RestanteFreePass' => $objetoTurma->getNu_maxfreepass() - $countMatriculaFreePass, 'retorno' => true);
            return $retorno;
        } else {
            $retorno = array('TotalFreePass' => $objetoTurma->getNu_maxfreepass(), 'RestanteFreePass' => $objetoTurma->getNu_maxfreepass() - $countMatriculaFreePass, 'retorno' => false);
            return $retorno;
        }

    }

    /**
     * Salva o arquivo do Contrato em Formado PDF
     *
     * @param $id_venda
     * @param $arquivo
     * @param $id_matricula
     * @return \Ead1_Mensageiro
     */
    public function salvarArquivoContrato($id_venda, $arquivo, $id_matricula)
    {
        $this->beginTransaction();
        try {

            $ararq = explode('.', $arquivo['name']);
            $ext = strtolower($ararq[(count($ararq) - 1)]);
            $nomeContrato = 'Contrato ' . $id_venda . '.' . $ext;
            $arquivo['name'] = 'Contrato_' . $id_venda . date('dmyyHis') . '.' . $ext;

            $type = explode('/', $arquivo['type']);

            if ($arquivo['size'] >= 10000000) {
                return new \Ead1_Mensageiro('Só é permitido o envio de arquivo igual ou inferior a 10mb.', \Ead1_IMensageiro::ERRO);
            }
            if ($type[1] != 'pdf') {
                return new \Ead1_Mensageiro('Só é permitido o envio de arquivo no formato PDF igual ou inferior a 10mb.', \Ead1_IMensageiro::ERRO);
            }

            if (!is_dir('upload/minhapasta/' . DIRECTORY_SEPARATOR . $id_venda . DIRECTORY_SEPARATOR)) {
                if (mkdir('upload/minhapasta/' . DIRECTORY_SEPARATOR . $id_venda . DIRECTORY_SEPARATOR, 0755, true) === false) {
                    throw new \Exception ('Não foi possível criar o diretório informado.');
                }
            }

            $negocio = new \G2\Negocio\Upload();
            $mensageiro = $negocio->upload($arquivo, $destination = 'upload/minhapasta/' . $id_venda, true, array('pdf'));

            // Se fazer o upload, alterar no projeto
            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $ve = $this->find('\G2\Entity\Venda', $id_venda);
                if (!$ve->getIdVenda()) {
                    throw new \Exception("Venda não encontrada!");
                }
                $up = $this->find('\G2\Entity\Upload', $mensageiro->getCodigo());
                if (!$up->getId_upload()) {
                    throw new \Exception("Upload não encontrado!");
                }


                //Salva os dados na tb_minhapasta
                $minhaPastaNegocio = new MinhaPasta();
                $minhaPastaNegocio->salvarContratoMinhaPasta($nomeContrato, $id_venda, $id_matricula, $up->getId_upload(), $arquivo['name']);

                $ve->setIdUploadcontrato($up);
                $this->save($ve);
                $this->commit();
            } else {
                throw new \Exception($mensageiro->getFirstMensagem());
            }
        } catch (\Exception $e) {
            $this->rollback();
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @param $id_venda
     * @param $nu_viacontrato
     * @return array
     * @throws \Zend_Exception
     * Efetua a contabilização das vias de contrato geradas, gerando os tramites
     */
    public function contabilizaViaContrato($id_venda, $nu_viacontrato, $st_justificativa = null)
    {
        try {

            /**
             * @var \G2\Entity\Venda $objVenda
             */
            $objVenda = $this->find('\G2\Entity\Venda', $id_venda);
            $objVenda->setNu_viascontrato((int)$objVenda->getNu_viascontrato() + 1);
            $retorno = $this->save($objVenda);
            $retornoArray = $this->toArrayEntity($retorno);

            if ($retorno) {
                //Verifica se é a primeira via, se sim, texto simples, se não concatena a justificativa gerada.
                if ($retorno->getNuViasContrato() == 1) {
                    $st_tramite = $retorno->getNuViasContrato() . 'ª via de Contrato';
                } else {
                    $st_tramite = $retorno->getNuViasContrato() . "ª via de Contrato com justificativa salvo - " . $st_justificativa;
                }

                //gera o tramite referente a geração de contrato
                $objTramite = new \G2\Entity\Tramite();
                $objTramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::GERACAO_CONTRATO));
                $objTramite->setId_usuario($this->sessao->id_usuario ? $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario) : null);
                $objTramite->setId_entidade($this->sessao->id_entidade ? $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade) : null);
                $objTramite->setDt_cadastro(new \DateTime());
                $objTramite->setSt_tramite($st_tramite);


                $objTramite->setDt_cadastro(new \DateTime());
                $objTramite->setBl_visivel(false);

                $tramit = $this->save($objTramite);

                $objTramiteVenda = new \G2\Entity\TramiteVenda();
                $objTramiteVenda->setId_tramite($tramit->getId_tramite());
                $objTramiteVenda->setId_venda($id_venda);
                $this->save($objTramiteVenda);
            }
            return $retornoArray;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao contabilizar via de contrato." . $e->getMessage());
        }
    }

    /**
     * @param $params
     * @return array
     * @throws \Exception
     * Efetua o registro da justificativa.
     */
    public function salvaJustificativa($params)
    {
        $objJustificativa = new \G2\Entity\JustificativaViaContrato();
        $objJustificativa->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
        $objJustificativa->setId_venda($params['id_venda']);
        $objJustificativa->setSt_justificativa($params['st_justificativa']);
        $objJustificativa->setNu_viacontrato($params['nu_viascontrato'] + 1);
        $objJustificativa->setDt_cadastro(new \DateTime());
        try {
            return $this->toArrayEntity($this->save($objJustificativa));
        } catch (\Exception $e) {
            throw new \Exception("Erro ao salvar justificativa de via do contrato.");
        }
    }

    public function removeProduto($id_vendaproduto)
    {
        $objVendaProduto = $this->find('\G2\Entity\VendaProduto', $id_vendaproduto);

        if ($objVendaProduto)
            $objVendaProduto->setBl_ativo(0);

        $result = $this->save($objVendaProduto);
        if ($result)
            return $result;

    }

    /**
     * Método para salvar as alterações de venda da funcionalidade Alterar Negociação
     * @param \VendaTO $vendaTO
     * @param $id_contratoregra
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     * @author Vinícius Alcântara <vinicius.alcantara@unyleya.com.br>
     * @since 18/05/2016
     */
    public function salvarAlteracoesVenda($params)
    {
        try {

            $this->verificaRegistroTramite($params);

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->findVenda($params['id_venda']);
            /** @var \G2\Entity\Contrato $contrato */
            $contrato = $this->findOneBy('\G2\Entity\Contrato', array('id_venda' => $params['id_venda']));

            //Verifica se foi retornado algum contrato para a venda
            if (!empty($contrato)) {
                //Setando o novo valor da regra de contrato conforme informado na tela
                $contrato->setId_contratoregra($this->getReference('G2\Entity\ContratoRegra', $params['id_contratoregra']));
                //Salvando as modificações no banco de dados
                /** @var \G2\Entity\Contrato $resultadoContrato */
                $resultadoContrato = $this->save($contrato);
                //Caso ouver algum erro no momento de salvar os dados da regra de contrato, lançamos uma mensagem de erro
                if (!$resultadoContrato->getId_contrato()) {
                    throw new \Exception('Erro ao tentar salvar a Regra do Contrato');
                }
            }

            if (!isset($params['id_atendente']) || empty($params['id_atendente'])) {
                throw new \Exception('Informe um atendente para alterar a venda!');
            }

            //Setando um novo atendente para a venda conforme informado pelo usuário
            $venda->setId_atendente($this->getReference('\G2\Entity\Usuario', $params['id_atendente']));

            //Se informado uma nova Campanha Comercial, Setamos esse novo valor para a venda
            if (array_key_exists('vendaProduto', $params) && $params['vendaProduto']) {
                foreach ($params['vendaProduto'] as $valor) {
                    if ($valor['id_campanhacomercial']) {
                        $objVp = $this->find('\G2\Entity\VendaProduto', $valor['id_vendaproduto']);
                        $objVp->setId_campanhacomercial($this->getReference('\G2\Entity\CampanhaComercial', $valor['id_campanhacomercial']));
                        $this->save($objVp);
                    }
                }
            }

            // Setando campanha de pontualidade no $venda.

            empty($params['id_campanhapontualidade'])
                ? $venda->setId_campanhapontualidade(null)
                : $venda->setId_campanhapontualidade($this->getReference('\G2\Entity\CampanhaComercial', $params['id_campanhapontualidade']));

            $resultado = $this->save($venda);
            if (!$resultado->getId_venda()) {
                throw new \Exception('Erro ao tentar salvar as alterações na venda');
            }

            return new \Ead1_Mensageiro("Alteração feita com sucesso! " . (empty($contrato) ? " Regra de Contrato não alterada, contrato inexistente para essa venda." : ""), \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro("Erro ao alterar venda! Por favor, tente outra vez. Erro: " . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Este método recupera as parcelas em atraso com pagamento recorrente e envia ao Cliente uma mensagem de aviso.
     * @return array|null
     * @throws \Exception
     */
    public function notificarPagamentoRecorrenteEmAtraso(){
        try {

            $rep = $this->getRepository(\G2\Entity\VwVendaLancamento::class);
            if($rep instanceof G2\Repository\VwVendaLancamento){
                $lancamentos = $rep->listarLancamentosCartaoRecorrenteACobrar();

                $success = array();
                $error = array();

                if (!empty($lancamentos)) {

                    $mensagemBO = new \MensagemBO();
                    foreach ($lancamentos as $lancamento) {

                        $lancamentoTO = new \LancamentoTO();
                        $lancamentoTO->setId_lancamento($lancamento[ 'id_lancamento']);
                        $lancamentoTO->fetch(true, true, true);

                        $mensageiro_aux = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::AVISO_RECORRENTE_ATRASADO, $lancamentoTO, \TipoEnvioTO::EMAIL, true, 0, $lancamento);
                        if ($mensageiro_aux->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $rep->salvarDataNotificacaoRecorrente($lancamento['id_lancamento']);
                            $success[] = $lancamento;
                        } else {
                            $error[] = array('mensagem'=>$mensageiro_aux->getFirstMensagem(),'lancamento'=>$lancamento);
                        }
                    }

                    return array(
                        'error' => $error,
                        'success' => $success
                    );

                }

            }

            return null;

        } catch (\Exception $e){
           throw $e;
        }
    }
}

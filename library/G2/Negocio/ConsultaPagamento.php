<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Consulta Pagamento
 * @author Denise Xavier <denise.xavier@unyleya.com.br>;
 */
class ConsultaPagamento extends Negocio
{
    CONST FINANCEIRO = 1;
    CONST PAGOS = 2;
    public $repositoryName;

    public function __construct() {
        parent::__construct();
        $this->repositoryName = 'G2\Entity\VwConsultaPagamento';
    }

    public function retornarPagamentosConsulta($params , $tipo)
    {
        try {

            $repo = $this->em->getRepository('G2\Entity\VwConsultaPagamento');
            $query = $repo->createQueryBuilder("vw")
                //->select("vw.*")
                ->where('1=1');

            if ($params['id_categoriasala'] && $params['id_categoriasala']) {
                $query->andWhere("vw.id_categoriasala =  :id_categoriasala")
                    ->setParameter('id_categoriasala', $params['id_categoriasala']);
            }

            if ($params['id_tipodisciplina'] && $params['id_tipodisciplina']) {
                $query->andWhere("vw.id_tipodisciplina =  :id_tipodisciplina")
                    ->setParameter('id_tipodisciplina', $params['id_tipodisciplina']);
            }

            if (array_key_exists('id_professor' , $params) && $params['id_professor']) {
                $query->andWhere("vw.id_usuarioprofessor =  :id_professor")
                    ->setParameter('id_professor', $params['id_professor']);
            }

            $query->andWhere('vw.id_coordenador = :id_coordenador')
                ->setParameter('id_coordenador', $this->sessao->id_usuario);

            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);

            if($tipo == $this::FINANCEIRO){
                $query->andWhere('vw.dt_encerramentocoordenador IS NOT NULL and vw.dt_encerramentopedagogico IS NOT NULL and vw.dt_encerramentofinanceiro IS NULL');
            }elseif($tipo == $this::PAGOS){
                $query->andWhere('vw.dt_encerramentofinanceiro IS NOT NULL and vw.nu_valorpago IS NOT NULL');
            }

            $result = $query->getQuery()->getResult();
            if ($result) {
                return $result;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Monta o combo com professores relacionados a um coordenador para consulta de pagamento
     * @param array $params
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function retornarProfessorCoordenador(array $params = [])
    {
        $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
        $params['id_usuario'] = isset($params['id_usuario']) ? $params['id_usuario'] : $this->sessao->id_usuario;

        // busca apenas professores ligados ao coordenador logado
        $sql = "SELECT DISTINCT upr.id_usuario, upr.st_nomecompleto, sa.id_entidade, uper.id_usuario as id_coordenador
          FROM tb_saladeaula sa
            JOIN dbo.vw_usuarioperfilentidadereferencia AS upr ON upr.id_saladeaula = sa.id_saladeaula
            JOIN dbo.tb_disciplinasaladeaula AS ds ON ds.id_saladeaula = sa.id_saladeaula
            JOIN dbo.tb_disciplina AS dc ON dc.id_disciplina = ds.id_disciplina
            JOIN dbo.tb_areaprojetosala AS aps ON aps.id_saladeaula = sa.id_saladeaula
            JOIN dbo.tb_usuarioperfilentidadereferencia AS uper ON uper.id_projetopedagogico = aps.id_projetopedagogico
                  WHERE sa.id_entidade =  ". $params['id_entidade']
                    ." AND uper.id_usuario = ".$params['id_usuario'];

        if(array_key_exists('tipo_disciplina', $params) && (!empty($params['tipo_disciplina'])))
            $sql .= " AND dc.id_tipodisciplina = " .$params['tipo_disciplina'];

        $sql .=" ORDER BY upr.st_nomecompleto ASC ";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }


}

<?php

namespace G2\Negocio;

/**
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Vendedor extends Negocio
{

    private $repositoryName = '\G2\Entity\Vendedor';

    /**
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function salvar($data)
    {
        $mensageiro = new \Ead1_Mensageiro();

        try {
            $entity = $this->find($this->repositoryName, $data['id_vendedor']);

            if (!($entity instanceof \G2\Entity\Vendedor)) {
                $entity = new $this->repositoryName;
            }

            if (empty($data['bl_ativo'])) {
                $data['bl_ativo'] = 0;
            }

            $this->entitySerialize->arrayToEntity($data, $entity);

            /**********************
             * Ação de salvar
             **********************/
            if ($entity instanceof \G2\Entity\Vendedor) {
                // Atualizar o registro existente
                $entity = $this->merge($entity);
            } else {
                // Salvar novo registro
                $entity = $this->persist($entity);
            }

            $mensageiro->setTipo(\Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Registro salvo com sucesso!');
        } catch (\Exception $e) {
            $mensageiro->setTipo(\Ead1_IMensageiro::ERRO);
            $mensageiro->setText('Erro ao salvar o registro');
        }

        return $mensageiro->setMensagem($data);
    }

}
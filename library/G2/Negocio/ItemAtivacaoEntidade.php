<?php
namespace G2\Negocio;

/**
 * Classe de negócio para ItemAtivacaoEntidade (tb_itemativacaoentidade)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class ItemAtivacaoEntidade extends Negocio
{

    private $repositoryName;

    public function __construct() {
        parent::__construct();
        $this->repositoryName = str_replace('\Negocio', '\Entity', __CLASS__);
    }

    public function save($data)
    {

        $id_attribute = 'id_itemativacaoentidade';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }


        /*********************
         * Definindo atributos
         *********************/

        if (isset($data['id_itemativacao']) && $data['id_itemativacao'])
            $entity->setId_itemativacao($data['id_itemativacao']);

        if (isset($data['id_entidade']) && $data['id_entidade'])
            $entity->setId_entidade($data['id_entidade']);

        if (isset($data['id_situacao']))
            $entity->setId_situacao($data['id_situacao']);
        //else
        //    $entity->setId_situacao(150); // 150 = NOK

        //if (isset($data['id_usuarioatualizacao']))
        //    $entity->setId_usuarioatualizacao($data['id_usuarioatualizacao']);
        //else
        $entity->setId_usuarioatualizacao($this->sessao->id_usuario);

        //if (isset($data['dt_atualizacao']))
        //    $entity->setDt_atualizacao($data['dt_atualizacao']);
        //else
        $entity->setDt_atualizacao(date('Y-m-d H:i:s'));


        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            $entity = $this->merge($entity);
        } else {
            $entity = $this->persist($entity);
        }

        $getId = 'get' . ucfirst($id_attribute);
        $data[$id_attribute] = $entity->$getId();

        $mensageiro = new \Ead1_Mensageiro();
        return $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);

    }


}
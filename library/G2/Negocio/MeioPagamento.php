<?php
/**
 * Description of Meio Pagamento
 *
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Negocio;


class MeioPagamento extends Negocio {

    private $repositoryName = 'G2\Entity\MeioPagamento';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retornar Meio de pagamento
     * @param array $where
     * @param array $order
     * @return G2\Entity\MeioPagamento
     * @throws \Exception
     */
    public function retornaMeioPagamento(array $where = array(), array $order = array()){
        try {
            return $this->findBy($this->repositoryName, $where, $order);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

} 
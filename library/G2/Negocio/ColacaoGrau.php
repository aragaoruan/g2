<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use Doctrine\DBAL\DBALException;
use G2\Utils\Helper;

/**
 * Classe de negócio para Colação de Grau
 * @author Rafael Leite <rafael.leite@unyleya.com.br>;
 */
class ColacaoGrau extends Negocio
{


    private $arrTextoTramite = [
        "dataColacao" => [
            "Data da Colação de Grau não informada ou removida.",
            "Data da Colação de Grau: %data%"
        ],
        "presencaColacao" => [
            "Presença Lançada para Colação de Grau: Ausente",
            "Presença Lançada para Colação de Grau: Presente",

        ]
    ];

    /**
     * @param $params
     * @return array|null|string
     */
    public function pesquisaAlunoColacao($params)
    {
        try {
            $arrWhere = array(
                'bl_ativo' => 1,
                'id_entidadematricula' => $this->sessao->id_entidade,
                'id_evolucao' => \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE
            );

            $customWhere = array();

            if (!empty($params['st_nomecompleto']))
                $arrWhere['st_nomecompleto'] = " LIKE '%" . trim($params['st_nomecompleto']) . "%'";

            if (!empty($params['id_projetopedagogico']))
                $arrWhere['id_projetopedagogico'] = (int)$params['id_projetopedagogico'];

            //dt_conclusao
            if (!empty($params['dt_conclusao_inicio']) && !empty($params['dt_conclusao_fim'])) {
                $arrWhere['dt_colacao'] = " BETWEEN '" . Helper::converterData($params['dt_conclusao_inicio'], 'Y-m-d') .
                    "' AND  '" . Helper::converterData($params['dt_conclusao_fim'], 'Y-m-d') . "' ";
            }

            //dt_colacao
            if (!empty($params['dt_colacao_inicio']) && !empty($params['dt_colacao_fim'])) {
                $arrWhere['dt_colacao'] = " BETWEEN '" . Helper::converterData($params['dt_colacao_inicio'], 'Y-m-d') .
                    "' AND  '" . Helper::converterData($params['dt_colacao_fim'], 'Y-m-d') . "' ";
            }

            // verifica se o bl_documentacao foi passado e se ele é diferente de vazio
            if (isset($params['bl_documentacao']) && $params['bl_documentacao'] != "") {

                $arrWhere['bl_documentacao'] = $params['bl_documentacao'];

                if ($params['bl_documentacao'] == 'null') {
                    $arrWhere['bl_documentacao'] = 'IS NULL';
                }

            }

            $arrOrder = array('st_nomecompleto' => 'ASC');

            return $this->findCustom('G2\Entity\VwMatriculaDiplomacao', $arrWhere, $customWhere, $arrOrder);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva array de matriculas para colação de grau
     * @param array $dados
     * @return \Ead1_Mensageiro
     */
    public function salvarColacaoGrau(array $dados)
    {
        try {

            $this->beginTransaction();

            foreach ($dados['data'] as $vwMatriculaDiplomacao) {

                /** @var \G2\Entity\MatriculaColacao $matriculaColacaoEntity * */
                $matriculaColacaoEntity = new \G2\Entity\MatriculaColacao;

                //faz um Update, desabilidandoo registro se a matrícula já existir na tb_matriculacolação
                if (!empty($vwMatriculaDiplomacao['id_matriculacolacao'])) {
                    //desabilita o registro
                    $enMatriculaColacao = $this->find('\G2\Entity\MatriculaColacao', (int)$vwMatriculaDiplomacao['id_matriculacolacao']);
                    $enMatriculaColacao->setId_matriculacolacao((int)$vwMatriculaDiplomacao['id_matriculacolacao']);
                    $enMatriculaColacao->setBl_ativo(false);
                    $this->save($enMatriculaColacao);
                }
                // Salvando informações da tabela matrícula colacao
                $matriculaColacaoEntity->setDt_cadastro(date('Y-m-d'));
//                $matriculaColacaoEntity->setDt_colacao($this->converterData($vwMatriculaDiplomacao['dt_colacao']));
                $matriculaColacaoEntity->setDt_colacao($vwMatriculaDiplomacao['dt_colacao'] ? Helper::converterData($vwMatriculaDiplomacao['dt_colacao']) : null);

                if ($vwMatriculaDiplomacao['bl_presenca'] != 'null')
                    $matriculaColacaoEntity->setBl_presenca((bool)$vwMatriculaDiplomacao['bl_presenca']);

                $matriculaColacaoEntity->setId_matricula($this->getReference('G2\Entity\Matricula', $vwMatriculaDiplomacao['id_matricula']));
                $matriculaColacaoEntity->setId_usuariocadastro($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));
                $matriculaColacaoEntity->setBl_ativo(true);

                //Salva matriculacolacao
                $return = $this->save($matriculaColacaoEntity);

                //Salva tramite
                if ($return) {
                    //Salva o tramite referente a data de colação
                    $textoTramite = str_replace("%data%", $vwMatriculaDiplomacao['dt_colacao'], $this->arrTextoTramite["dataColacao"][!!$matriculaColacaoEntity->getDt_colacao()]);
                    $this->salvarTramiteColacao($vwMatriculaDiplomacao['id_matricula'], $textoTramite);


                    //Se tive dados na presença, salva um novo tramite
                    if ($vwMatriculaDiplomacao['bl_presenca'] != 'null') {
                        $textoTramite = $this->arrTextoTramite["presencaColacao"][!!$vwMatriculaDiplomacao['bl_presenca']];
                        $this->salvarTramiteColacao($vwMatriculaDiplomacao['id_matricula'], $textoTramite);
                    }
                }
            }


            //se tiver data de colação e presença = presente (1), salva registro na tb_matriculadiplomacao
            if ($matriculaColacaoEntity->getDt_colacao() && $matriculaColacaoEntity->getBl_presenca() == 1) {
                //salva registro na tb_matriculadiplomacao
                $ngDiplomacao = new \G2\Negocio\Diplomacao();
                $ngDiplomacao->primeiraEtapaDiplomacao((int)$vwMatriculaDiplomacao['id_matricula']);
            }

            $this->commit();

            return new \Ead1_Mensageiro('Dados salvos com sucesso.', \Ead1_IMensageiro::SUCESSO);
        } catch (DBALException $de) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao salvar matriculas para colacao de grau [bancodedados]: ' . $de->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao salvar matriculas para colacao de grau: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param $matricula
     * @return \Ead1_Mensageiro
     */
    public function salvarTramiteColacao($matricula, $textoTramite)
    {
        try {
            $tramite = new \G2\Negocio\Tramite();
            $tipoTramite = \G2\Constante\TipoTramite::MATRICULA_COLACAO_GRAU;

            $tramite->salvarTramiteMatricula($textoTramite, $matricula, $tipoTramite);
            return new \Ead1_Mensageiro('Tramite da colação de grau salvo com sucesso');
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar trâmite da diplomação: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }
}

<?php
/**
 * Classe de negócio para VwAlunosForaRenovacao.php
 * @author Janilson Mendes <janilson.silva@unyleya.com.br>
 */

namespace G2\Negocio;


class VwAlunosForaRenovacao extends Negocio
{

    private $repository = '\G2\Entity\VwAlunosForaRenovacao';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Parâmetros para Pesquisa:
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaAlunosForaRenovacao()
    {
        try {
            /*
             * Essa data foi definida para que o robo comece a executar pelo início da sprint
             * dessa atividade.
             */
            $dataInicioVenda = '2018-04-25';

            $date = new \DateTime();
            $date->add(new \DateInterval('P10D'));

            $alunos = $this->findCustom($this->repository,
                array('dt_ultimaoferta' => "BETWEEN '{$dataInicioVenda}' AND '" . \G2\Utils\Helper::converterData($date, 'Y-m-d') . "'",
                    'bl_motivofinanceiro' => true,
                    'nu_totalocorrencia' => 0,));

            $success = array();
            $error = array();

            if (is_array($alunos) && count($alunos) > 0) {

                $mensagemBO = new \MensagemBO();
                $mensagemPadrao = \MensagemPadraoTO::RENOVACAO_NEGOCIACAO_INADIMPLENCIA;
                $tipoEnvio = \TipoEnvioTO::EMAIL;

                foreach ($alunos as $key => $entity) {
                    $en_to_array = $this->toArrayEntity($entity);
                    $entity_to = $this->entityToTO($entity);
                    $ocorrencia = $this->saveOcorrencia($en_to_array);

                    if ($ocorrencia->getCodigo() == '001') {
                        $success[$key] = $en_to_array;

                        $mensageiro_aux = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade($mensagemPadrao,
                            $entity_to,
                            $tipoEnvio);

                        if ($mensageiro_aux->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            $en_to_array['message'] = $mensageiro_aux->getText();
                            $error[$key] = $en_to_array;
                        }
                    } else {
                        $en_to_array['message'] = $ocorrencia->getText();
                        $error[$key] = $en_to_array;
                    }
                }
            }

            return array('results' => $alunos,
                'error' => $error,
                'success' => $success);

        } catch (\Exception $e) {
            return array('results' => false);
        }
    }

    public function saveOcorrencia($params)
    {
        $desc = 'Não foi possível iniciar o processo de renovação de matrícula porque existe parcelas em aberto.';

        try {
            $paramsOcorrencia = array('id_entidade' => $params['id_entidade'],
                'id_tipoocorrencia' => 1,
                'bl_ativo' => 1,);

            $nucleoCo = $this->findOneBy('\G2\Entity\NucleoCo', $paramsOcorrencia);

            if (null == $nucleoCo) {
                return new \Ead1_Mensageiro('Não existe núcleo.', \Ead1_IMensageiro::ERRO, '004');
            }

            if (null == $nucleoCo->getId_assuntorenovacao()) {
                return new \Ead1_Mensageiro('Não há subassunto referente à renovação de matrícula vinculado ao núcleo.', \Ead1_IMensageiro::ERRO, '003');
            }

            $assuntoCo = $this->getReference('\G2\Entity\AssuntoCo',
                                             $nucleoCo->getId_assuntorenovacao());

            $textoSistemaBO = new \TextoSistemaBO ();
            $textoMensagem = null != $assuntoCo->getId_textosistema()
                             ? $textoSistemaBO->gerarTexto(new \TextoSistemaTO(array('id_textosistema' =>
                                                                                    $assuntoCo->getId_textosistema()
                                                                                              ->getId_textosistema())),
                                                          $params)
                                              ->subtractMensageiro()
                                              ->getFirstMensagem()->getSt_texto()
                             : $desc;

            $this->beginTransaction();

            $usuario = $this->getReference('\G2\Entity\Usuario', $params['id_usuario']);
            $matricula = $this->getReference('\G2\Entity\Matricula', $params['id_matricula']);

            unset($paramsOcorrencia['id_entidade']);
            $paramsOcorrencia['id_entidadecadastro'] = $params['id_entidade'];

            $objetoOcorrencia = new \G2\Entity\Ocorrencia();

            $objetoOcorrencia->setId_categoriaocorrencia($this->findOneBy('\G2\Entity\CategoriaOcorrencia',
                $paramsOcorrencia));
            $objetoOcorrencia->setId_evolucao($this->getReference('\G2\Entity\Evolucao',
                \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO));
            $objetoOcorrencia->setId_situacao($this->getReference('\G2\Entity\Situacao',
                \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE));
            $objetoOcorrencia->setId_usuariointeressado($usuario);
            $objetoOcorrencia->setId_usuariocadastro($usuario);
            $objetoOcorrencia->setId_matricula($matricula);
            $objetoOcorrencia->setId_entidade($this->getReference('\G2\Entity\Entidade', $params['id_entidade']));
            $objetoOcorrencia->setSt_titulo('Renovação - Negociação Inadimplência');
            $objetoOcorrencia->setSt_ocorrencia($textoMensagem);
            $objetoOcorrencia->setDt_cadastro(new \DateTime());
            $objetoOcorrencia->setDt_atendimento(new \DateTime());
            $objetoOcorrencia->setId_assuntoco($assuntoCo);
            $objetoOcorrencia->setId_venda($params['id_venda']);

            $ocorrencia = $this->save($objetoOcorrencia);

            $this->commit();

            return new \Ead1_Mensageiro(array('ocorrencia' => $ocorrencia), \Ead1_IMensageiro::SUCESSO, '001');

        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, '002');
        }
    }
}

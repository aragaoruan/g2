<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Horário Aula (tb_horariodiasemana)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class HorarioDiaSemana extends Negocio
{

    private $repositoryName = 'G2\Entity\HorarioDiaSemana';

    public function __construct()
    {
        parent::__construct();
    }

    public function findById($id) {
        $result = parent::find($this->repositoryName, $id);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {

                $arrReturn[] = array(
                    'id_diasemana' => $row->getId_diasemana(),
                    'id_horarioaula' => $row->getId_horarioaula()
                );


            }

            return $arrReturn;
        }
    }

//    function save() {
//        parent::save()
//        $entity = new \G2\Entity\HorarioAula();
//    }


}
<?php

namespace G2\Negocio;

use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Doctrine\Common\Util\Debug;
use G2\Constante\MeioPagamento;

class BoletoPHP extends Negocio
{

    CONST nomePDF = 'boleto.pdf';
    CONST caminhoPDF = "upload/boletos";

    public function __construct()
    {
        parent::__construct();
    }


    /** Método para gerar o boleto com as informações
     * @param integer $idLancamento
     * @param bool $soDados Booleano que define se irá retornar somente um array com os dados do
     * boleto ou o boleto gerado para impressao
     * @param bool $verificaRetorno Este parametro define se a url do boleto vai ser verificada e desta forma
     * se for vazia, retorna aviso e se contiver a url retorna sucesso e a url
     * @return array|void
     * @throws \Zend_Exception
     */
    public function emitirBoleto($idLancamento, $soDados = false, $pdf = true, $verificaRetorno = false)
    {

        try {
            $pagarme = new \G2\Negocio\Pagarme();
            /** @var \Ead1_Mensageiro $mensageiro */
            $mensageiro = $pagarme->transactionBoleto($idLancamento);
            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                /** @var \G2\Entity\Lancamento $lancamento */
                $lancamento = $mensageiro->getFirstMensagem();
                if($soDados){
                    return $lancamento;
                }

                //trecho utilizado para verificar se já existe a url do boleto e não retornar ela, ao invés do pdf do boleto.
                if($verificaRetorno){
                    if(empty($lancamento->getSt_urlboleto())){
                        return new \Ead1_Mensageiro($lancamento, \Ead1_IMensageiro::AVISO, true);
                    }else{
                        return new \Ead1_Mensageiro($lancamento, \Ead1_IMensageiro::SUCESSO, true);
                    }
                }

                $ch = curl_init();

                if(is_array($lancamento) && $lancamento['st_urlboleto'] != null)
                    curl_setopt( $ch, CURLOPT_URL, $lancamento['st_urlboleto'] );
                else if($lancamento->getSt_urlboleto() != null)
                    curl_setopt($ch, CURLOPT_URL, $lancamento->getSt_urlboleto());

                // define que o conteúdo obtido deve ser retornado em vez de exibido
                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
                $retorno = curl_exec( $ch );
                curl_close( $ch );

                if ($pdf) {
                    $mpdf = new \Ead1_IMPdf();
                    $mpdf->carregarHTML('<style type="text/css">
                                body { font-size: 10px }
                                .barcode { height: 50px; font-size: 1px}
                            </style>');
                    $mpdf->carregarHTML($retorno);

                    echo $mpdf->gerarPdf(self::nomePDF,
                        self::caminhoPDF . DIRECTORY_SEPARATOR .$idLancamento,
                        'I');
                    return '';
                }

                return $retorno;

            } else {
                throw new \Exception($mensageiro->getFirstMensagem());
            }
        } catch (\Exception $exc) {
            throw new \Zend_Exception($exc->getMessage());
        }
    }

    /**
     * @param $idVenda
     * @return string
     * @throws \Zend_Exception
     */
    public function gerarPdfBoletosVenda($idVenda)
    {
        ini_set("allow_url_fopen", 1);
        ini_set("allow_url_include", 1);

        $lancamentos = $this->findBy('\G2\Entity\VwResumoFinanceiro', array('id_meiopagamento' => \G2\Constante\MeioPagamento::BOLETO, 'bl_quitado' => 0, 'id_venda' => $idVenda));

        $mpdf = new \Ead1_IMPdf();
        $mpdf->carregarHTML('<style type="text/css">
                                table { border-collapse: collapse; border: 0px ; border-spacing: 0px; padding: 0px; }
                                table tr th,table tr, table tr td { border: 0px ; padding: 0px; border-spacing: 0px; }
                                .cp { font: bold 10px arial;  color: black; }
                                .ti { font: 9px arial, helvetica, sans-serif; }
                                .ld { font: bold 13px arial; color: #000000; }
                                .ct { font: 8px "arial narrow"; color: #000033; }
                                .cn { font: 9px arial; color: black; }
                                .bc { font: bold 18px arial; color: #000000; }
                                .ld2 {font: bold 12px arial; color: #000000; }
                            </style>');

        foreach ($lancamentos as $index => $lancamento) {
            ob_start();
            $this->emitirBoleto($lancamento->getId_lancamento());
            $html = ob_get_contents();
            ob_end_clean();

            $html = explode("</head>", $html);
            $html = $html[1];
            $html = str_replace('</BODY>', '', $html);
            $html = str_replace('</HTML>', '', $html);

            $mpdf->ImageBoleto('loja/boletophp/imagens/6.png', $html, '6');
            $mpdf->ImageBoleto('loja/boletophp/imagens/logoitau.jpg', $html, 'logoitau');
            $mpdf->ImageBoleto('loja/boletophp/imagens/3.png', $html, '3');
            $mpdf->ImageBoleto('loja/boletophp/imagens/2.png', $html, '2');
            $mpdf->ImageBoleto('loja/boletophp/imagens/1.png', $html, '1');
            $mpdf->ImageBoleto('loja/boletophp/imagens/p.png', $html, 'p');
            $mpdf->ImageBoleto('loja/boletophp/imagens/b.png', $html, 'b');

            $mpdf->carregarHTML($html);
            if ($index != (count($lancamentos) - 1)) {
                $mpdf->inserirQuebraPagina();
            }
        }

        $mpdf->gerarPdf(self::nomePDF, self::caminhoPDF . DIRECTORY_SEPARATOR . $idVenda, 'F');
        return '';
    }


    /**
     * Gera o pdf do boleto
     * @param $idLancamento
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function gerarPdfBoleto($idLancamento)
    {
        try {
            ini_set("allow_url_fopen", 1);
            ini_set("allow_url_include", 1);

            $lancamento = $this->findOneBy('\G2\Entity\VwResumoFinanceiro', array(
                'id_meiopagamento' => \G2\Constante\MeioPagamento::BOLETO,
                'bl_quitado' => 0,
                'id_lancamento' => $idLancamento
            ));

            //Verifica se encontrou o lançamento.
            if (!$lancamento) {
                throw new \Exception("Lançamento não encontrado.");
            }


            $mpdf = new \Ead1_IMPdf();
            $mpdf->carregarHTML('<style type="text/css">
                                table { border-collapse: collapse; border: 0px ; border-spacing: 0px; padding: 0px; }
                                table tr th,table tr, table tr td { border: 0px ; padding: 0px; border-spacing: 0px; }
                                .cp { font: bold 10px arial;  color: black; }
                                .ti { font: 9px arial, helvetica, sans-serif; }
                                .ld { font: bold 13px arial; color: #000000; }
                                .ct { font: 8px "arial narrow"; color: #000033; }
                                .cn { font: 9px arial; color: black; }
                                .bc { font: bold 18px arial; color: #000000; }
                                .ld2 {font: bold 12px arial; color: #000000; }
                            </style>');

            ob_start();
            $this->emitirBoleto($lancamento->getId_lancamento());
            $html = ob_get_contents();
            ob_end_clean();

            $html = explode("</head>", $html);
            $html = $html[1];
            $html = str_replace('</BODY>', '', $html);
            $html = str_replace('</HTML>', '', $html);

            $mpdf->ImageBoleto('loja/boletophp/imagens/6.png', $html, '6');
            $mpdf->ImageBoleto('loja/boletophp/imagens/logoitau.jpg', $html, 'logoitau');
            $mpdf->ImageBoleto('loja/boletophp/imagens/3.png', $html, '3');
            $mpdf->ImageBoleto('loja/boletophp/imagens/2.png', $html, '2');
            $mpdf->ImageBoleto('loja/boletophp/imagens/1.png', $html, '1');
            $mpdf->ImageBoleto('loja/boletophp/imagens/p.png', $html, 'p');
            $mpdf->ImageBoleto('loja/boletophp/imagens/b.png', $html, 'b');

            $mpdf->carregarHTML($html);
            $pathPdf = $mpdf->gerarPdf('Boleto' . $lancamento->getId_lancamento() . '.pdf', self::caminhoPDF . DIRECTORY_SEPARATOR . $lancamento->getId_venda(), 'F');

            return new \Ead1_Mensageiro($pathPdf, \Ead1_IMensageiro::SUCESSO, true);


        } catch (\Exception $e) {
            throw $e;
        }
    }

}

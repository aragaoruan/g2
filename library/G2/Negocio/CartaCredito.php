<?php

namespace G2\Negocio;

/**
 * Class CartaCredito
 * @package G2\Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @sicne 2014-13-01
 */
class CartaCredito extends Negocio
{
    private $repositoryName = '\G2\Entity\CartaCredito';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados da vw_cartacredito
     * @param array $params
     * @param bool $toArray
     * @throws \Zend_Exception
     * @return mixed | array or object
     */
    public function retornarDadosVwCartaCredito(array $params = [], $toArray = false)
    {
        try {
            //Verifica se a entidade foi passada, senão pega o valor da sessão
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            //busca os dados
            $result = $this->findBy('\G2\Entity\VwCartaCredito', $params);

            //verifica se tem resultado e se foi passado como true o parametro toArray
            if ($result && $toArray) {
                //percore os registros e transforma em um array
                foreach ($result as $key => $row) {
                    $dt_cadastro = new \DateTime($row->getDt_cadastro());
                    $result[$key] = $this->toArrayEntity($row);
                    $result[$key]['dt_cadastro'] = $dt_cadastro->format('d/m/Y');
                }
            }
            //retorna os dados
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da vw_cartacredito. " . $e->getMessage());
        }
    }


    /**
     * Atualiza situação da Carta de Crédito
     * @param integer $id_venda
     * @param integer $id_cartacredito
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function atualizaSituacaoCartaCredito($id_venda, $id_cartacredito)
    {
        try {
            //verifica se o id_cartacredito foi passado
            if (!$id_cartacredito) {
                throw new \Exception("ID carta de crédito não informado.");
            }

            //procura venda
            $resultVenda = $this->find('\G2\Entity\Venda', $id_venda);
            $idEvolucaoVenda = $resultVenda->getIdEvolucao() ? $resultVenda->getIdEvolucao()->getId_evolucao() : NULL;
            //verifica se a evolução da venda é igual a "Aguardando Recebimento" (9)
            if ($idEvolucaoVenda == \G2\Constante\Evolucao::TB_VENDA_AGUARDANDO_RECEBIMENTO) {

                $idSituacaoCarta = \G2\Constante\Situacao::TB_CARTACREDITO_INATIVA;//seta o id_situacao da carta

                //busca os dados da vw_carta de credito
                $resultVwCartaCredito = $this->retornarDadosVwCartaCredito(array('id_cartacredito' => $id_cartacredito));

                //verifica se achou
                if ($resultVwCartaCredito) {
                    foreach ($resultVwCartaCredito as $vwCartaCredito) {

                        //verifica se o valor disponivel da carta de crédito esta zerado
                        if ($vwCartaCredito->getNu_valordisponivel() == 0) {//se o valor estiver zerado atualizaremos a situacao da carta
                            //busca a carta de credito
                            $cartaCredito = $this->find($this->repositoryName, $id_cartacredito);
                            //verifica se encontrou
                            if (!$cartaCredito) {
                                throw new \Exception("Carta de crédito não encontrada.");
                            }

                            //seta a nova situação
                            $cartaCredito->setId_situacao($this->getReference('\G2\Entity\Situacao', $idSituacaoCarta));
                            //salva
                            $cartaCredito = $this->save($cartaCredito);
                            if (!$cartaCredito) {
                                throw new \Exception("Erro ao tentar atualizar situação da carta de crédito. [{$id_cartacredito}]");
                            }
                        }
                    }

                }
            }
            return new \Ead1_Mensageiro("Cartas de crédito atualizadas com sucesso.", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar atualizar situação da carta de credito. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados de VendaCartaCredito
     * @param array $params
     * @param bool $toArray
     * @return array
     * @throws \Exception
     */
    public function retornarDadosVendaCartaCredito(array $params = [], $toArray = false)
    {
        try {
            $result = $this->findBy('\G2\Entity\VendaCartaCredito', $params);
            if ($result && $toArray) {
                foreach ($result as $key => $row) {
                    $result[$key] = array(
                        'id_vendacartacredito' => $row->getId_vendacartacredito(),
                        'id_cartacredito' => $row->getId_cartacredito() ? $row->getId_cartacredito()->getId_cartacredito() : NULL,
                        'id_venda' => $row->getId_venda() ? $row->getId_venda()->getIdvenda() : NULL,
                        'nu_valorutilizado' => $row->getNu_valorutilizado()
                    );
                }
            }
            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Método para salvar array de carta de crédito
     * @param array $arrCartaCredito
     * @param integer $id_venda
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarArrVendaCartaCredito(array $arrCartaCredito, $id_venda)
    {
        try {
            $this->beginTransaction();//abre a transação
            $arrItensVendaCarta = array();
            //verifica se foi passado um array de cartas
            if ($arrCartaCredito) {
                //percorre o array
                foreach ($arrCartaCredito as $key => $carta) {
                    //força uma posição do array com o id_venda
                    $carta['id_venda'] = $id_venda;
                    //verifica carta de credito
                    if (empty($carta['id_vendacartacredito'])) {
                        if (!$this->verificaCartaCreditoDisponivel($carta['id_cartacredito'])) {
                            continue;
                        }
                    }

                    //salva o relacionamento da carta com a venda
                    $result = $this->salvarVendaCartaCredito($carta);
                    //verifica se retornou false ou vazio e cria um exception
                    if (!$result) {
                        throw new \Exception("Houve um erro ao tentar salvar array. [" . $key . "]");
                    } else {//se sucesso atualiza a carta de crédito
                        $arrItensVendaCarta[] = $result->getId_vendacartacredito();
                        $resultAtualiza = $this->atualizaSituacaoCartaCredito($carta['id_venda'], $result->getId_cartacredito()->getId_cartacredito());
                        if ($resultAtualiza->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                            throw new \Exception("Erro ao tentar atualizar Situação Carta Credito");
                        }
                    }
                }
            }
            $this->removeItensVendaCarta($arrItensVendaCarta, $id_venda);
            $this->commit();
            return new \Ead1_Mensageiro("Array de VendaCartaCredito salvos com sucesso!", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar array de Venda Carta de Crédito. " . $e->getMessage());
        }
    }

    /**
     * Método para remover os vinculos da Carta de Crédito com a venda
     * @param array $arrItensManter
     * @param integer $idVenda
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    private function removeItensVendaCarta($arrItensManter, $idVenda)
    {
        try {
            $this->beginTransaction();
            $resultItensVenda = $this->findBy('\G2\Entity\VendaCartaCredito', array('id_venda' => $idVenda));
            if ($resultItensVenda) {
                foreach ($resultItensVenda as $row) {
                    if (!in_array($row->getId_vendacartacredito(), $arrItensManter)) {
                        if (!$this->delete($row)) {
                            throw new \Exception("Erro ao apagar vínculo de venda com Carta de Crédito. id: " . $row->getId_vendacartacredito());
                        }

                    }
                }
            }
            $this->commit();
            return new \Ead1_Mensageiro("Vinculos de Venda com Carta de Crédito removidos com sucesso.", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar excluir Cartas de Crédito vinculadas a Venda. " . $e->getMessage());
        }
    }

    /**
     * Consulta carta de crédito e verifica se o valor disponivel dela é diferente de zero
     * @param integer $idCartaCredito
     * @return bool
     * @throws \Zend_Exception
     */
    public function verificaCartaCreditoDisponivel($idCartaCredito)
    {
        try {
            $resultCarta = $this->findOneBy('\G2\Entity\VwCartaCredito', array('id_cartacredito' => $idCartaCredito));
            if ($resultCarta) {
                if ($resultCarta->getNu_valordisponivel() > 0) {
                    return true;
                } else {
                    return false;
                }
            } else {
                throw new \Exception("Carta de Crédito não encontrada.");
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar verificar disponibilidade da Carta de Crédito. " . $e->getMessage());
        }
    }

    /**
     * Método para salvar Venda Carta Credito
     * @param array $data
     * @return \G2\Entity\VendaCartaCredito
     * @throws \Zend_Exception
     */
    public function salvarVendaCartaCredito(array $data)
    {
        try {
            $entity = new \G2\Entity\VendaCartaCredito();//instancia a entity
            //verifica se o id_vendacartacredito e procura os dados
            if (isset($data['id_vendacartacredito']) && !empty($data['id_vendacartacredito'])) {
                $vendaCartaCredito = $this->find('\G2\Entity\VendaCartaCredito', $data['id_vendacartacredito']);
                if ($vendaCartaCredito) {
                    //atribui o resultado do find a variavel $entity
                    $entity = $vendaCartaCredito;
                }
            }
            //busca os dados da carta de credito
            if (isset($data['id_cartacredito'])) {
                $entity->setId_cartacredito($this->getReference($this->repositoryName, $data['id_cartacredito']));
            }

            //busca os dados da venda
            if (isset($data['id_venda'])) {
                $entity->setId_venda($this->getReference('\G2\Entity\Venda', $data['id_venda']));
            }

            //seta o valor utilizado
            if (isset($data['nu_valorutilizado'])) {
                $entity->setNu_valorutilizado($data['nu_valorutilizado']);
            }

            $result = $this->save($entity);
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar VendaCartaCredito. " . $e->getMessage());
        }
    }

    /**
     * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
     * Método para retornar o histórico da carta de credito segundo os pa
     * @param array $where
     * @param array $order
     * @param null $limit
     * @param null $offset
     * @return Object
     */
    public function retornaHistoricoCartaCredito(array $where = array(), array $order = array(), $limit = NULL, $offset = NULL)
    {
        $result = $this->findBy('G2\Entity\VwHistoricoCartaCredito', $where, $order, $limit, $offset);
        return $result;
    }

    /**
     * Retorna todas as cartas de créditos baseadas na vwcartacredito
     * @param $params
     * @return Object
     */
    public function retornaAllCartasCredito($params)
    {
        return $this->findBy('\G2\Entity\VwCartaCredito', $params);
    }

    /**
     * Deleta logicamente uma carta de crédito
     * @param $id
     * @return mixed|string
     */
    public function deleta($id)
    {
        try {
            $objetoCarta = $this->find('\G2\Entity\CartaCredito', $id);
            $objetoCarta->setBl_ativo(false);
            return $this->save($objetoCarta);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna as entidades das holdings compartilhadas baseadas nos parâmetros passados
     * @param $param
     * @return string
     */
    public function retornaEntidadesHoldingsParameters($param)
    {
        try {
            return $this->getRepository('\G2\Entity\Holding')->retornaEntidadesHoldingsParameters($param);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function saveCartaCredito($params)
    {
        try {
            $this->beginTransaction();
            $objCarta = new \G2\Entity\CartaCredito();

            if (array_key_exists('id_cartacredito', $params) && $params['id_cartacredito'])
                $objCarta = $this->find('\G2\Entity\CartaCredito', $params['id_cartacredito']);

            $objCarta->setId_usuario($this->getReference('\G2\Entity\Usuario', $params['id_usuario']));
            $objCarta->setBl_ativo(true);
            $objCarta->setDt_cadastro( (array_key_exists('dt_cadastro', $params) && $params['dt_cadastro']) ? $this->converteDataBancoZendDate($params['dt_cadastro']):new \DateTime());

            $objCarta->setId_usuariocadastro(array_key_exists('id_usuariocadastro', $params) && $params['id_usuariocadastro'] ? $this->getReference('\G2\Entity\Usuario', $params['id_usuariocadastro']) : $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));

            $objCarta->setDt_validade($this->converteDate($params['dt_validade']));
            $objCarta->setNu_valororiginal(str_replace(',', '.', $params['nu_valororiginal']));
            $objCarta->setId_Entidade($this->getReference('\G2\Entity\Entidade', $params['id_entidade']));
            $objCarta->setId_situacao($this->getReference('\G2\Entity\Situacao', $params['id_situacao']));
            $objCarta->setDt_cadastro(array_key_exists('dt_cadastro', $params) ? $this->converteDataBanco($params['dt_cadastro']) : new \DateTime());

            $resultado = $this->save($objCarta);
            $this->commit();
            if ($resultado){
                $objetoRetorno = $this->toArrayEntity($resultado);
                $objetoRetorno['st_nomeentidade'] = $resultado->getId_entidade()->getSt_nomeentidade();
                return array('objeto' => $objetoRetorno, 'notify' => array('text' => 'Carta de crédito salva com sucesso!', 'type' => 'success', 'title' => 'Sucesso'));
            }
        } catch (\Exception $e) {
            $this->rollback();
            return array('text' => 'Erro ao salvar carta de crédito!', 'type' => 'error', 'title' => 'Erro');
        }
    }
}
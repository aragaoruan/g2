<?php

namespace G2\Negocio;

use G2\Entity\VwAvaliacaoAgendamento;
use G2\Entity\VwPesquisarPessoa;
use G2\Negocio\AvaliacaoAluno;
use G2\Transformer\Maximize\NotasTransformer;
use G2\Transformer\Maximize\UsuarioAplicacaoTransformer;
use G2\Transformer\Maximize\UsuariosIntegracaoTransformer;
use G2\Transformer\Maximize\VwPesquisarPessoaTransformer;
use G2\Utils\Helper;

class Maximize extends Negocio
{

    /** @var \MaximizeWebServices $maximizeWS */
    private $maximizeWS;

    /** @var UsuarioIntegracao $_negocioUsuarioIntegracao */
    private $_negocioUsuarioIntegracao;

    /**
     * @var VwAvaliacaoAgendamento $avaliacaoAgendamento
     */
    private $avaliacaoAgendamento;

    /**
     * @var AvaliacaoAluno $avaliacaoAluno
     */
    private $avaliacaoAluno;

    public function __construct()
    {
        parent::__construct();
        $this->maximizeWS = new \MaximizeWebServices();
        $this->_negocioUsuarioIntegracao = new UsuarioIntegracao();
    }

    /**
     * Retorna o aluno transformado para o WS (pronto para a integração)
     * @param $id
     * @param null $idEntidade
     * @return \Ead1_Mensageiro|mixed
     * @throws \Exception
     */
    public function getAluno($id, $idEntidade = null)
    {
        try {
            if (!$idEntidade) {
                $idEntidade = $this->getId_entidade();
            }

            $pessoa = $this->find(VwPesquisarPessoa::REPOSITORY, [
                "id_usuario" => $id,
                "id_entidade" => $idEntidade
            ]);

            return (new VwPesquisarPessoaTransformer($pessoa))
                ->getTransformer();
        } catch (\Zend_Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Valida se foi recebido um array de alunos
     * @param $alunos
     * @throws \Zend_Exception
     */
    public function validateAlunos($alunos)
    {
        if (!is_array($alunos))
            throw new \Zend_Exception('Não foi recebido uma lista de alunos!');
    }

    /**
     * @param $nota
     * @return bool
     * @throws \Exception
     */
    public function validateNota($nota)
    {
        if (!is_array($nota))
            return false;

        try {
            $arrayIsComplete = array_key_exists('codProva', $nota)
                && array_key_exists('codigoUsuarioSistemaOrigem', $nota)
                && array_key_exists('pontuacaoObtida', $nota);

            if ($arrayIsComplete) {
                return is_int($nota['codProva'])
                    && is_string($nota['codigoUsuarioSistemaOrigem'])
                    && (is_float($nota['pontuacaoObtida']) || is_int($nota['pontuacaoObtida']));
            }
            return false;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Busca e retorna (um array) dos alunos que não estão cadastrados no G2
     * @param $alunos
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function getAlunosNaoIntegrados($alunos)
    {
        $pessoas = [];
        foreach ($alunos as $aluno) {
            $where = [
                'id_usuario' => $aluno["id_usuario"],
                'id_sistema' => \G2\Constante\Sistema::MAXIMIZE,
                'id_entidade' => $aluno["id_entidade"]
            ];
            /** @var \G2\Entity\UsuarioIntegracao $result */
            $result = $this->_negocioUsuarioIntegracao->findOneBy(\G2\Entity\UsuarioIntegracao::class, $where);

            if (empty($result)) {
                $pessoa = $this->getAluno($aluno["id_usuario"], $aluno["id_entidade"]);
                if ($pessoa instanceof \Ead1_Mensageiro
                    && $pessoa->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw new \Exception($pessoa->getText());
                }
                array_push($pessoas, $pessoa);
            }
        }
        return $pessoas;
    }

    /**
     * Salva os usuários integrados na Maximize
     * @param $alunosIntegrados
     * @return \Ead1_Mensageiro
     */
    public function salvarUsuariosIntegrados($alunosIntegrados)
    {
        $mensageiro = new \Ead1_Mensageiro();
        foreach ($alunosIntegrados as $alunoIntegrado) {
            try {
                $result = $this->_negocioUsuarioIntegracao->salvarIntegracao([
                    'id_usuariocadastro' => null,
                    'id_usuario' => $alunoIntegrado['codigoSistemaOrigem'],
                    'st_codusuario' => 'OK',
                    'bl_encerrado' => false,
                    'id_sistema' => \G2\Constante\Sistema::MAXIMIZE,
                    'dt_cadastro' => date('Y-m-d H:i:s'),
                    'id_entidade' => $alunoIntegrado['entidade']
                ]);

                if (!($result instanceof \G2\Entity\UsuarioIntegracao)) {
                    throw new \Exception("Erro ao salvar dados de integração do usuário.");
                }
                $mensageiro->addMensagem('Usuário integrado', \Ead1_IMensageiro::TYPE_SUCESSO);
            } catch (\Exception $e) {
                $mensageiro->addMensagem($e->getMessage(), \Ead1_IMensageiro::TYPE_ERRO);
            }
        }

        return $mensageiro;
    }

    /**
     * Integra os alunos com a Maximize
     * @param array $alunos
     * @return \Ead1_Mensageiro
     */
    public function criarAlunos(array $alunos)
    {
        try {
            $this->validateAlunos($alunos);
            $alunosNaoIntegrados = $this->getAlunosNaoIntegrados($alunos);

            $arrReturn = [];
            if (!empty($alunosNaoIntegrados)) {
                foreach ($alunosNaoIntegrados as $aluno) {
                    $maximizeArr = $aluno;
                    //remove essa posição do array
                    unset($maximizeArr['entidade']);

                    //envia o aluno para maximize
                    $resultMaximize = $this->maximizeWS
                        ->exec(\G2\Constante\Maximize::ENDPOINT_SALVA_USUARIO, [$maximizeArr]);

                    //verifica o retorno do serviço em caso diferentes de sucesso
                    if ($resultMaximize->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                        array_push($arrReturn, $resultMaximize);
                        continue;
                    }

                    //salva os dados de integração do aluno
                    $arrSalvarIntegracao = $this->salvarUsuariosIntegrados([$aluno]);
                    if ($arrSalvarIntegracao->getType() === \Ead1_IMensageiro::TYPE_SUCESSO) {
                        array_push($arrReturn, $arrSalvarIntegracao->setMensagem($maximizeArr));
                    }

                }

                if (!$arrReturn) {
                    throw new \Exception("Erro ao integrar dados com o Sistema de provas");
                }

            }
            return new \Ead1_Mensageiro($alunos);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro("Erro ao criar alunos. " . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Lista provas do sistema maximize
     * @param $st_prova
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function listarProvas($st_prova)
    {
        try {
            if (strlen(trim($st_prova)) < 4) {
                throw new \Exception('Para fazer a pesquisa é preciso inserir pelo menos 4 caracteres do nome da prova.');
            } else {

                //retorna o esquema de configuração e a linha de negócio da entidade logada
                $linhadenegocio = (new \G2\Negocio\EsquemaConfiguracao())
                    ->retornaItemPorEntidade(
                        \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO,
                        $this->sessao->id_entidade
                    );

                //verifica se a linha de negócio e o esquema de configuração é da graduação ou pós-graduação
                if ($linhadenegocio['id_esquemaconfiguracao'] == \G2\Constante\EsquemaConfiguracao::GRADUACAO
                    && $linhadenegocio['st_valor'] == \G2\Constante\LinhaDeNegocio::GRADUACAO) {
                    $segmento = 'fac-uny-grad';
                } else if ($linhadenegocio['id_esquemaconfiguracao'] == \G2\Constante\EsquemaConfiguracao::Pos_Graduacao
                    && $linhadenegocio['st_valor'] == \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
                    $segmento = 'fac-uny-pos';
                } else {
                    throw new \Exception('Aviso ao retornar provas: Linha de negócio não cadastrada na Maximize.');
                }

                $data = array(
                    'buscaTextual' => $st_prova,
                    'codigoAmbienteSistemaOrigem' => $segmento
                );

                $msgprovas = $this->maximizeWS->exec(\G2\Constante\Maximize::ENDPOINT_LISTA_PROVAS, $data);
                if ($msgprovas->getTipo() !== \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception('Aviso ao retornar provas: [ ' . $msgprovas->getFirstMensagem() . '].(Maximize)');
                }
            }
            return $msgprovas;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * Retorna os aplicadores integrados com a Maximize, monta um array com os alunos por
     * aplicador e aplicação e envia para a Maximize agendar as provas
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function agendarAlunosMaximize()
    {
        try {
            //retorna aplicadores que tem aplicacoes cadastradas e alunos a serem integrados
            $msgAplicador = $this->retornaAplicadoresIntegracao();
            if ($msgAplicador->getTipo() !== \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($msgAplicador->getFirstMensagem());
            }

            /** @var \G2\Entity\VwAplicadorIntegracao[] $aplicadores */
            $aplicadores = $msgAplicador->getMensagem();

            /** Verifica se os aplicadores estão integrados
             * Se não, integra-os
             * Retorna um array com os id's dos aplicadores para fazer a busca das aplicações / avaliações
             * @var array $array_aplicadores
             */
            $array_aplicadores = $this->verificaIntegracaoAplicadorSala($aplicadores);

            if ($array_aplicadores->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                $this->notificarNewRelic($array_aplicadores->getText());
                throw new \Exception($array_aplicadores->getText());
            }

            $where = [
                'id_situacaopresenca' => 0,
                'id_situacao' => 'IN(' . implode(",", [
                        \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                        \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                    ]) . ')',
                'id_tipoavaliacao' => 'IN(' . implode(",", [
                        \G2\Constante\TipoAvaliacao::AVALIACAO_RECUPERACAO,
                        \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL
                    ]) . ')',
                'id_aplicadorprova' => 'IN(' . implode(",", $array_aplicadores->getMensagem()) . ')'
            ];
            $customWhere = [
                "tb.cod_prova IS NOT NULL OR tb.cod_prova <> 0",
                "tb.bl_sincronizado IS NULL OR tb.bl_sincronizado = 0"
            ];
            $alunos = $this->findCustom('G2\Entity\VwAvaliacaoAgendamento', $where, $customWhere, [
                "id_aplicadorprova" => "ASC"
            ], 1000);

            if (empty($alunos)) {
                throw new \Exception('Nenhum dado encontrado de agendamento avaliação para os parâmetros informados!'
                    . json_encode(array_merge($customWhere, $where)));
            }

            $msgIntegracaoAlunos = $this->criarAlunos((new UsuariosIntegracaoTransformer($alunos))
                ->getTransformer());

            if ($msgIntegracaoAlunos->getTipo() !== \Ead1_IMensageiro::SUCESSO) {
                $message = 'Erro ao integrar os alunos a Maximize: [ ' . $msgIntegracaoAlunos->getText() . '].';
                $this->notificarNewRelic($message);
                throw new \Exception($message);
            }

            $msg = new \Ead1_Mensageiro();
            foreach ($alunos as $aluno) {
                $maximizeData = [
                    'codigoSalaSistemaOrigem' => (string)$aluno->getId_aplicadorprova(),
                    'dataAplicacao' => Helper::converterData($aluno->getDt_aplicacao(), 'd/m/Y')
                        . ' ' . Helper::converterData($aluno->getHr_inicio(), 'H:i:s'),
                    'isExcluido' => false,
                    'provaUsuario' => [[
                        'codigoUsuarioSistemaOrigem' => (string)$aluno->getId_usuario(),
                        'codProva' => (int)$aluno->getCod_prova(),
                        'isExcluido' => false,
                        'idAvaliacaoAgendamento' => (int)$aluno->getId_avaliacaoagendamento()
                    ]]
                ];

                $mensageiro = $this->sincronizarAgendamento($maximizeData);

                if ($mensageiro->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                    $msg->addMensagem([
                        "type" => $mensageiro->getType(),
                        "text" => $mensageiro->getText(),
                        "data" => $maximizeData
                    ]);
                    continue;
                }


                $this->salvaIntegracaoAplicacaoSala($aluno->getId_avaliacaoagendamento());
                $msg->addMensagem([
                    "type" => $mensageiro->getType(),
                    "text" => $mensageiro->getText(),
                    "data" => $maximizeData
                ]);

            }
            return $msg;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $dados
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    private function sincronizarAgendamento(array $dados)
    {
        try {

            $response = $this->maximizeWS
                ->exec(\G2\Constante\Maximize::ENDPOINT_AGENDAR_SALA_APLICACAO, [$dados]);

            if ($response->getTipo() !== \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception('Erro ao agendar aplicação em sala: [ ' . $response->getText() . '].');
            }

        } catch (\Exception $e) {
            $this->notificarNewRelic($e);
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $response;
    }

    /**
     * Envia o erro lá para o new relic
     * @param $exception
     * @throws \Exception
     */
    private function notificarNewRelic($exception)
    {
        $stringLog = (date('Y-m-d H:i:s') . ' in '
            . $exception->getFile() . ':(' . $exception->getLine() . ') - ' . $exception->getMessage());
        \G2\Utils\LogSistema::enviarLogNewRelic($stringLog);
    }


    /**
     * Salva a aplicação como integrada com a Maximize
     * @param $id_avaliacaoagendamento
     * @return bool
     * @throws \Exception
     */
    public function salvaIntegracaoAplicacaoSala($id_avaliacaoagendamento)
    {
        try {
            $avaliacaoAgendamento = $this->find('G2\Entity\AvaliacaoAgendamento', $id_avaliacaoagendamento);

            $avaliacaoAgendamento->setBl_sincronizado(true)
                ->setDt_sincronizado(date('Y-m-d H:i:s'));

            if ($this->save($avaliacaoAgendamento)) {
                return true;
            }
        } catch (\Zend_Exception $e) {
            echo $e->getMessage();
        }
    }


    /**
     * Retorna os aplicadores que possuem aplicação ativa onde
     * - Data limite de alteração da aplicação é MENOR que a data atual
     * - Possuem alunos com agendimento ativo não sincronizados
     * - Aplicação esteja ativa
     * - Data da aplicação seja MAIOR que a data atual (não contempla aplicações antigas)
     * @return \Ead1_Mensageiro
     */
    public function retornaAplicadoresIntegracao($idAplicador = null)
    {
        try {
            $params = [
                "bl_ativo" => true
            ];
            if ($idAplicador) {
                $params["id_aplicadorprova"] = $idAplicador;
            }

            /** @var \G2\Entity\VwAplicadorIntegracao[] $aplicadores */
            $aplicadores = $this->findBy('\G2\Entity\VwAplicadorIntegracao', $params);

            if (empty($aplicadores)) {
                return new \Ead1_Mensageiro(
                    'Nenhum aplicação com alunos para serem sincronizados.', \Ead1_IMensageiro::AVISO);
            }

            return new \Ead1_Mensageiro($aplicadores);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Verifica integração dos aplicadores (G2) como sala na Maximize
     * Retorna um array de id's com os aplicadores que tem avaliações e aplicações a serem cadastradas para busca
     * @param \G2\Entity\VwAplicadorIntegracao[] $arr_vwaplicadorintegracao
     * @return \Ead1_Mensageiro
     */
    public function verificaIntegracaoAplicadorSala($arr_vwaplicadorintegracao)
    {
        try {
            $arr_sincronizar = [];
            $array_retorno = [];

            foreach ($arr_vwaplicadorintegracao as $key => $vw_aplicadorintegracao) {
                //Se o aplicador não for sincronizado, cria um array para envio da maximize
                if (!$vw_aplicadorintegracao->isBl_sincronizado() && !$vw_aplicadorintegracao->getDt_sincronizado()) {
                    $sala = array('codigoSistemaOrigem' => (string)$vw_aplicadorintegracao->getId_aplicadorprova(),
                        'nome' => $vw_aplicadorintegracao->getSt_aplicadorprova(),
                        'endereco' => $vw_aplicadorintegracao->getSt_endereco(),
                        'cidade' => $vw_aplicadorintegracao->getSt_nomemunicipio(),
                        'uf' => $vw_aplicadorintegracao->getSg_uf());
                    array_push($arr_sincronizar, $sala);
                } else {
                    //se ele for sincronizado, add o aplicador no array para busca  das aplicações
                    array_push($array_retorno, $vw_aplicadorintegracao->getId_aplicadorprova());
                }
            }
            if (!empty($arr_sincronizar)) {
                $returnWs = $this->maximizeWS->exec(\G2\Constante\Maximize::ENDPOINT_CADASTRA_SALA, $arr_sincronizar);
                if ($returnWs->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    //percorre o array e seta as datas de sincronização e bl_sincronizado
                    foreach ($returnWs->getMensagem() as $return) {
                        /** @var \G2\Entity\AplicadorProva $entity */
                        $entity = $this->find('\G2\Entity\AplicadorProva', (int)$return->codigoSistemaOrigem);
                        if ($entity instanceof \G2\Entity\AplicadorProva && $entity->getId_aplicadorprova()) {
                            $entity->setDt_sincronizado(new \DateTime());
                            $entity->setBl_sincronizado(1);

                            //salva sincronização do aplicador com sala
                            if ($this->save($entity)) {
                                //add ao array de aplicadores para busca
                                array_push($array_retorno, $entity->getId_aplicadorprova());
                            }
                        }
                    }
                }
            }
            return new \Ead1_Mensageiro($array_retorno);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao sincronizar/ retornar aplicadores: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna uma avaliação agendamento de um aluno
     * @param $where
     * @return $this
     * @throws \Zend_Exception
     */
    public function getAvaliacaoAgendamento($where)
    {
        $this->avaliacaoAgendamento = $this->findOneBy(VwAvaliacaoAgendamento::class, $where);
        return $this->avaliacaoAgendamento;
    }

    /**
     * Prepara o array de notas vindo da Maximize para o G2
     * @param $nota
     * @return mixed
     * @throws \Exception
     */
    public function prepararNotas($nota, $avaliacaoAgendamento)
    {
        if (!$avaliacaoAgendamento) {
            throw new \Exception("Avaliação Agendamento não encontrado.");
        }

        $notas = (new NotasTransformer($avaliacaoAgendamento))
            ->getTransformer();
        $notas['st_nota'] = $nota;
        return $notas;
    }

    /**
     * Salva as notas do aluno
     * @param $notas
     * @param $idMatricula
     * @param null $idEntidade
     * @param bool $verificarConcluite
     * @return \Ead1_Mensageiro
     */
    public function salvarNotas($notas, $idMatricula, $idEntidade = null, $verificarConcluite = true)
    {
        $this->avaliacaoAluno = new AvaliacaoAluno();
        $result = $this->avaliacaoAluno->salvarNotaAvaliacaoPresencial($notas, $verificarConcluite);

        if ($result->getType() === \Ead1_IMensageiro::TYPE_SUCESSO && $idMatricula) {
            $this->criarJobNotificarAluno($idMatricula, $idEntidade);
        }

        return $result;
    }

    /**
     * Cria o job queue para notificar o aluno que a nota foi lançada.
     * @param $idMatricula
     * @param $idEntidade
     * @throws \Exception
     * @throws \Zend_Exception
     */
    private function criarJobNotificarAluno($idMatricula, $idEntidade)
    {
        //cria o job
        $schedule_time = date('Y-m-d H:i:s', time() + 10);
        $this->createHttpJob(
            \Ead1_Ambiente::geral()->st_url_gestor2 . '/robo/notificar-aluno-nota-maximize',
            [
                "id_matricula" => $idMatricula,
                "id_entidade" => $idEntidade
            ],
            [
                'name' => 'Notificar Aluno Nota Maximize.',
                'schedule_time' => $schedule_time,
                'queue_name' => 'Default'
            ],
            \G2\Constante\Sistema::GESTOR
        );

    }


    /**
     * User: Alex Alexandre
     * E-mail: <alex.alexandre@unyleya.com.br>
     * Date: 19/12/17
     * Time: 08:50
     *
     */
    /**
     * Função responsável por verificar se um aluno já esta integrado
     *
     * @param $idAluno
     * @return array|\Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function verificaAlunoIntegrado($idAluno)
    {
        $aluno = $this->findOneBy("G2\Entity\UsuarioIntegracao", array("id_usuario" => $idAluno));

        if (!$aluno) {
            return array(
                "message" => "Você não tem provas para revisão",
                "type" => "error",
                "cssClass" => "red");
        }

        return $this->verificarProva($aluno->getId_usuario());
    }

    /**
     * Função responsável por verificar se existe uma prova para o aluno
     *
     * @param $idAluno
     * @return array|\Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function verificarProva($idAluno)
    {
        $prova = $this->findOneBy("G2\Entity\VwAvaliacaoAgendamento", array("id_usuario" => $idAluno));

        if (!$prova) {
            return array(
                "message" => "Você não tem provas para revisão",
                "type" => "error",
                "cssClass" => "red");
        }

        return $this->montarUrlAutenticacao($idAluno);
    }

    /**
     * Função responsável por disparar uma requisição para gerar o token de acesso
     * E montar a url final, que irá redirecionar o aluno para a fabrica de provas
     *
     * @param $idAluno (O $idAluno é o campo codigoSistemaOrigem)
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function montarUrlAutenticacao($idAluno)
    {
        $url = $this->maximizeWS->getUrl();
        $token = $this->maximizeWS->exec(
            \G2\Constante\Maximize::ENDPOINT_GERAR_CHAVE_ACESSO,
            $idAluno,
            'string',
            true
        );

        if (!$token) {
            return array(
                "message" => "Não foi possível gerar um token, por favor contate o admim do sistema",
                "type" => "error",
                "cssClass" => "red");
        }

        $fullUrl = $url . \G2\Constante\Maximize::ENDPOINT_AUTENTICAR_USUARIO . $token;

        return $fullUrl;
    }

}

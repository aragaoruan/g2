<?php

namespace G2\Negocio;

use G2\Constante\OperacaoLog;
use G2\Transformer\AtividadeComplementar\LogAtividadeTransformer;

/**
 * Classe de negócio para métodos realcionados ao Log
 *
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class Log extends Negocio
{

    private $repository = 'G2\Entity\Log';

    /**
     * Classe para salvar log de Alterações em Tabela
     * As classes tem que derivar da G2Entity para que seja possível a tipagem e o uso do toBackboneArray
     *
     * @param $id_funcionalidade
     * @param \G2\G2Entity $antes
     * @param \G2\G2Entity $depois
     * @param null $st_motivo
     * @return bool
     * @throws \Exception
     */
    public function salvar($id_funcionalidade = null, \G2\G2Entity $antes, \G2\G2Entity $depois, $st_motivo = null)
    {

        try {
            $class = get_class($depois);
            if (($antes instanceof $class) == false) {
                throw new \Exception("Os Objetos tem que ser do mesmo tipo!");
            }
            $entityAntes = $antes->toBackboneArray();
            $entityDepois = $depois->toBackboneArray();
            $alterados = null;
            foreach ($entityAntes as $chave => $valor) {
                if ($entityDepois[$chave] != $valor) {
                    $alterados[] = $chave;
                }
            }
            $ar_antes = null;
            $ar_depois = null;
            if ($alterados) {
                foreach ($alterados as $alterado) {
                    $ar_antes[$alterado] = $entityAntes[$alterado];
                    $ar_depois[$alterado] = $entityDepois[$alterado];
                }
            } else {
                return true;
            }
            $st_antes = json_encode($ar_antes);
            $st_depois = json_encode($ar_depois);


            $id = $this->em->getClassMetadata(get_class($antes))->getIdentifier();
            $getid = "get" . $id[0];

            $se = \Ead1_Sessao::getObjetoSessaoGeral();
            $logEntity = new \G2\Entity\Log();
            $logEntity->setdt_cadastro(new \DateTime());
            $logEntity->setid_entidade($this->find('\G2\Entity\Entidade', $se->getid_entidade()));
            $logEntity->setid_perfil($this->find('\G2\Entity\Perfil', $se->getid_perfil()));
            $logEntity->setid_usuario($this->find('\G2\Entity\Usuario', $se->getid_usuario()));
            $logEntity->setid_funcionalidade($id_funcionalidade ? $this->find('\G2\Entity\Funcionalidade', $id_funcionalidade) : null);
            $logEntity->setst_antes($st_antes);
            $logEntity->setst_depois($st_depois);
            $logEntity->setid_tabela($antes->$getid());
            $logEntity->setst_tabela($this->em->getClassMetadata(get_class($antes))->getTableName());
            $this->save($logEntity);
            return true;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Método apenas para listar um exemplo, pode ser apagado e alterado a vontade
     *
     * @param $id_funcionalidade
     */
    public function retornar()
    {

        $logs = $this->findAll($this->repository);
        if ($logs) {
            foreach ($logs as $log) {
                if ($log instanceof \G2\Entity\Log) {

                    $data = new \DateTime($log->getdt_cadastro());

                    echo $log->getid_log() . '-' . $data->format('d/m/Y H:i:s') . ' |  Funcionalidade: ' . $log->getid_funcionalidade()->getSt_funcionalidade()
                        . ', Entidade: ' . $log->getid_entidade()->getSt_nomeentidade()
                        . ', Perfil: ' . $log->getid_perfil()->getSt_nomeperfil()
                        . ', Usuário: ' . $log->getid_usuario()->getSt_nomecompleto()
                        . ', Antes: ' . $log->getst_antes()
                        . ', Depois: ' . $log->getst_depois()
                        . ', Motivo: ' . $log->getst_motivo() . '<br>';
                }
            }
        }

    }

    private function getValorPkLog($params)
    {
        //verifica se as chaves da tabela são passadas e são um array
        if (isset($params['st_pk']) && is_array($params['st_pk'])) {

            $arrValorPk = null;

            if ($params['st_pk']) {
                //percorre as chaves pegando seus respectivos valores e adiciona num array
                foreach ($params['st_pk'] as $itemPk) {
                    $arrValorPk[] = $params['depois'][$itemPk];
                }
            }
            return json_encode($arrValorPk);
        }

        if (isset($params['depois'][$params['st_pk']])) {
            return $params['depois'][$params['st_pk']];
        }
    }

    /**
     * Método para processar dados e gerar o respectivo log
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function processaJobGerarLog(array $params)
    {
        try {
            $this->beginTransaction();//abre transação
            $operacao = $this->getOperacaoLog($params);//recupera a operação
            //salva o log em operações de update
            if ($operacao->getId_operacaolog() == \G2\Constante\OperacaoLog::UPDATE) {

                $arrDiff = $this->compararDadosAntesEDepois($params['antes'], $params['depois']);
                //percorre o array com as diferenças
                foreach ($arrDiff as $key => $valor) {

                    //monta os dados
                    $arrDados = array(
                        'dt_cadastro' => $params['dt_cadastro'],
                        'id_funcionalidade' => array_key_exists('id_funcionalidade', $params) && $params['id_funcionalidade'] ? $params['id_funcionalidade'] : null,
                        'id_entidade' => array_key_exists('id_entidade', $params) && $params['id_entidade'] ? $params['id_entidade'] : null,
                        'id_perfil' => array_key_exists('id_perfil', $params) && $params['id_perfil'] ? $params['id_perfil'] : null,
                        'id_usuario' => array_key_exists('id_usuario', $params) && $params['id_usuario'] ? $params['id_usuario'] : null,
                        'st_tabela' => $params['st_tabela'],
                        'st_antes' => ($params['antes'][$key] != false || $params['antes'][$key] != 0) ? $params['antes'][$key] : 0,
                        'st_depois' => ($params['depois'][$key] != false || $params['depois'][$key] != 0) ? $params['depois'][$key] : 0,
                        'id_tabela' => $this->getValorPkLog($params),
                        'st_coluna' => $key,
                        'id_operacao' => $operacao,
                        'st_motivo' => \G2\Constante\OperacaoLog::getNomeOperacao($operacao->getId_operacaolog()),
                    );
                    //salva os dados
                    if (!$this->salvarLogIndividual($arrDados)) {
                        throw new \Exception("Erro ao tentar salvar Log. Operação UPDATE.");
                    }

                }

                //salva o log na operação insert
            } elseif ($operacao->getId_operacaolog() == \G2\Constante\OperacaoLog::INSERT) {
                if (isset($params['depois']) && !empty($params['depois'])) {
                    foreach ($params['depois'] as $key => $valor) {//percorre os dados inseridos
                        //verifica o valor e sata os dados
                        if ($valor) {
                            $arrDados = array(
                                'dt_cadastro' => $params['dt_cadastro'],
                                'id_funcionalidade' => array_key_exists('id_funcionalidade', $params) && $params['id_funcionalidade'] ? $params['id_funcionalidade'] : null,
                                'id_entidade' => array_key_exists('id_entidade', $params) && $params['id_entidade'] ? $params['id_entidade'] : null,
                                'id_perfil' => array_key_exists('id_perfil', $params) && $params['id_perfil'] ? $params['id_perfil'] : null,
                                'id_usuario' => array_key_exists('id_usuario', $params) && $params['id_usuario'] ? $params['id_usuario'] : null,
                                'st_tabela' => $params['st_tabela'],
                                'st_antes' => NULL,
                                'st_depois' => $valor == false || $valor == 0 ? 0 : $valor,
                                'id_tabela' => $this->getValorPkLog($params),
                                'st_coluna' => $key,
                                'id_operacao' => $operacao,
                                'st_motivo' => \G2\Constante\OperacaoLog::getNomeOperacao($operacao->getId_operacaolog()),
                            );

                            if (!$this->salvarLogIndividual($arrDados)) {
                                throw new \Exception("Erro ao tentar salvar Log. OPERAÇÂO INSERT.");
                            }

                        }
                    }

                } elseif ($operacao->getId_operacaolog() == \G2\Constante\OperacaoLog::DELETE) {
                    \Zend_Debug::dump("DELETE");
                }

            }
            $this->commit();//fecha a transação

            //retorna o array
            return array(
                'status' => true,
                'operacao' => \G2\Constante\OperacaoLog::getNomeOperacao($operacao->getId_operacaolog())
            );
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception("Erro ao processar job para gerar log. " . $e->getMessage());
        }

    }

    /**
     * Verifica os parametros passado para gerar o log
     *
     * @param array $params
     * @return bool
     * @throws \Exception
     */
    private function verificaParametrosJobLog(array $params)
    {
        try {
//            if (array_key_exists('id_funcionalidade', $params) && empty($params['id_funcionalidade'])) {
//                throw new \Exception("Código da funcionalidade não informado");
//            }

            if (array_key_exists('id_entidade', $params) && empty($params['id_entidade'])) {
                throw new \Exception("Código da entidade não informado");
            }

            if (array_key_exists('id_usuario', $params) && empty($params['id_usuario'])) {
                throw new \Exception("Código do usuário não informado");
            }

            if (array_key_exists('id_perfil', $params) && empty($params['id_perfil'])) {
                throw new \Exception("Código do perfil não informado");
            }

            if (array_key_exists('st_tabela', $params) && empty($params['st_tabela'])) {
                throw new \Exception("Nome da tabela não informado");
            }
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna a referencia da entity OperacaoLog
     *
     * @param array $params
     * @return Doctrine
     * @throws \Exception
     */
    private function getOperacaoLog(array $params)
    {
        try {
            //verifica se existe a posição antes no array e se tem valores
            if (array_key_exists('antes', $params) && count($params['antes'])) {
                //isso indica que é um update então retorna a referencia da operação update
                return $this->getReference('\G2\Entity\OperacaoLog', \G2\Constante\OperacaoLog::UPDATE);
            } elseif (!array_key_exists('antes', $params) || !count($params['antes'])) {
                // se não houver a posição antes, significa que é um insert
                return $this->getReference('\G2\Entity\OperacaoLog', \G2\Constante\OperacaoLog::INSERT);
            } else if (array_key_exists('depois', $params) && !count($params['depois'])) {
                //verifica se tem a posição depois e se ela não tem valores, isso indica que é um delete
                return $this->getReference('\G2\Entity\OperacaoLog', \G2\Constante\OperacaoLog::DELETE);
            } else {
                throw new \Exception("Não foi possível identificar a operação efetuada no registro.");
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Compara o array antes e depois, esse método foi criado porque o array_diff do php não compara os valores boleanos
     * se alguém tiver uma alteranativa melhor, por favor refatore o trecho. (até onde pesquisei não encontrei solução)
     *
     * @param array $arrAntes
     * @param array $arrDepois
     * @return array
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * https://bugs.php.net/bug.php?id=40769
     * http://php.net/manual/pt_BR/function.array-diff.php
     */
    private function compararDadosAntesEDepois(array $arrAntes, array $arrDepois)
    {
        $arrDiff = array();
        if ($arrAntes && $arrDepois) {
            foreach ($arrDepois as $key => $value) {
                if ($value != $arrAntes[$key]) {
                    $arrDiff[$key] = $value;
                }
            }
        }
        return $arrDiff;
    }

    /**
     * Método para persistir dados
     *
     * @param array $dados
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarLogIndividual(array $dados)
    {
        try {
            $entityLog = new \G2\Entity\Log();
            $entityLog->setdt_cadastro($dados['dt_cadastro'])
                ->setid_funcionalidade($dados['id_funcionalidade'] ? $this->getReference('\G2\Entity\Funcionalidade', $dados['id_funcionalidade']) : null)
                ->setid_entidade($dados['id_entidade'] ? $this->getReference('\G2\Entity\Entidade', $dados['id_entidade']) : null)
                ->setid_perfil($dados['id_perfil'] ? $this->getReference('\G2\Entity\Perfil', $dados['id_perfil']) : null)
                ->setid_usuario($dados['id_usuario'] ? $this->getReference('\G2\Entity\Usuario', $dados['id_usuario']) : null)
                ->setst_tabela($dados['st_tabela'])
                ->setst_antes($dados['st_antes'])
                ->setst_depois($dados['st_depois'])
                ->setid_tabela($dados['id_tabela'])
                ->setst_coluna($dados['st_coluna'])
                ->setid_operacao($dados['id_operacao'])
                ->setst_motivo($dados['st_motivo']);

            return $this->save($entityLog);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao salvar log. " . $e->getMessage());
        }
    }


    /**
     * Retorna o histórico de alterações para o parametro de renovação consultado pelo id do esquema de configuração
     * @param $idEsquemaConfiguracao
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function retornarLogParametroRenovacaByEsquemaConfiguracao($idEsquemaConfiguracao)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            //verifica se o esquema foi passado
            if (!$idEsquemaConfiguracao) {
                throw new \Exception(\G2\Constante\MensagemSistema::ID_ESQUEMA_VAZIO);
            }

            //consulta os dados no banco passandoa coluna que quero consultar, o id do esqeuma e o id do item
            $result = $this->getRepository($this->repository)->findBy(array(
                'st_coluna' => 'st_valor',
                'id_tabela' => "[" . $idEsquemaConfiguracao . "," . \G2\Constante\ItemConfiguracao::RENOVACAO_AUTOMATICA_A_PARTIR . "]"
            ), array(
                'dt_cadastro' => 'desc'
            ));

            //define o mensageiro para sem resultado
            $mensageiro->setMensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);

            //verificar o resultado
            if ($result) {
                //cria uma variavel com array vazio para auxilio
                $arrHistorico = [];

                /** @var \G2\Entity\Log $log */
                foreach ($result as $index => $log) {
                    $arrHistorico[$index]['id_log'] = $log->getid_log();
                    //formata a data
                    $arrHistorico[$index]['dt_alteracao'] = \G2\Utils\Helper::converterData($log->getDt_cadastro(), 'd/m/Y H:i:s');

                    //busca o nome do usuário
                    $arrHistorico[$index]['st_usuario'] = $log->getid_usuario() ? $log->getid_usuario()->getSt_nomecompleto() : null;

                    //define o texto da ação
                    $acao = "Definiu ";
                    if ($log->getid_operacao() && $log->getid_operacao()->getId_operacaolog() == \G2\Constante\OperacaoLog::UPDATE) {
                        $acao = "Alterou ";
                    }

                    $acao .= "o parâmetro da renovação para " . $log->getst_depois() . "&ordm; Parcela";
                    $arrHistorico[$index]['st_acao'] = $acao;
                }
                //seta o array no mensageiro
                $mensageiro->setMensageiro($arrHistorico, \Ead1_IMensageiro::SUCESSO);
            }

        } catch (\Exception $e) {
            throw $e;
        }
        return $mensageiro;
    }


    /**
     * Retorna o log das atividades
     * @param $idAtividade
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function logAtividadeComplementar($idAtividade)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {

            $logs = $this->findBy($this->repository, array(
                "st_tabela" => "tb_atividadecomplementar",
                "id_tabela" => $idAtividade,
                "id_operacao" => OperacaoLog::UPDATE,
                "st_coluna" => [
                    "id_situacao",
                    "st_tituloatividade",
                    "nu_horasconvalidada",
                    "st_observacaoavaliador",
                ]
            ), array("dt_cadastro" => "DESC"));

            //define o mensageiro para sem resultado
            $mensageiro->setMensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);

            if ($logs) {
                $arrHistorico = (new LogAtividadeTransformer($logs))
                    ->getTransformer();
                //seta o array no mensageiro
                $mensageiro->setMensageiro($arrHistorico, \Ead1_IMensageiro::SUCESSO);
            }

        } catch (\Exception $e) {
            throw $e;
        }
        return $mensageiro;
    }


}

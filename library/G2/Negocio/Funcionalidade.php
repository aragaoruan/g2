<?php

namespace G2\Negocio;

/**
 * Classe de negocio para funcionalidade
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-09-17
 */
class Funcionalidade extends Negocio
{

    /**
     *
     * @var \G2\Entity\Funcionalidade
     */
    private $repositoryName = "G2\Entity\Funcionalidade";

    /** @var  \Ead1_Mensageiro $mensageiro */
    protected $mensageiro;

    public function __construct()
    {
        parent::__construct();

        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Retorna funcionalidade pelo id
     * @param $id
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function findFuncionalidade($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /**
     * Pega a referencia de funcionalidade pelo id
     * @param $idFuncionalidade
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     * @throws \Zend_Exception
     */
    public function getReferenceFuncionalidae($idFuncionalidade)
    {
        return $this->getReference($this->repositoryName, $idFuncionalidade);
    }

    /**
     * @param $data
     * @return \Ead1_Mensageiro|mixed
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function save($data)
    {

        $id_attribute = 'id_funcionalidade';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }


        /*********************
         * Definindo atributos
         *********************/

        if (isset($data['st_funcionalidade'])) {
            $entity->setSt_funcionalidade($data['st_funcionalidade']);
        }

        if (isset($data['st_ajuda'])) {
            $entity->setSt_ajuda($data['st_ajuda']);
        }

        if (isset($data['id_funcionalidadepai']['id_funcionalidade'])
            && $data['id_funcionalidadepai']['id_funcionalidade']) {
            $entity->setId_funcionalidadepai(
                $this->find('\G2\Entity\Funcionalidade', $data['id_funcionalidadepai']['id_funcionalidade']));
        }

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            $entity = $this->merge($entity);
        } else {
            $entity = $this->persist($entity);
        }

        $getId = 'get' . ucfirst($id_attribute);
        $data[$id_attribute] = $entity->$getId();

        $mensageiro = new \Ead1_Mensageiro();
        return $mensageiro
            ->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);

    }

    /**
     * @param $idEntidade
     * @return \Ead1_Mensageiro
     */
    public function retornarMenuNovoPortal($idEntidade)
    {
        try {

            if (empty($idEntidade)) {
                throw new \Zend_Validate_Exception("É necessário informar a entidade!");
            }

            $dados = $this->getRepository($this->repositoryName)->retornarMenuNovoPortal($idEntidade);

            if (empty($dados)) {
                throw new \Zend_Validate_Exception("Não foi possível retornar as funcionalidades do Sistema. 
                Verifique se os menus estão liberados para a entidade.");
            }

            $this->mensageiro->setMensageiro($dados, \Ead1_IMensageiro::SUCESSO);

        } catch (\Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Zend_Exception $e) {
            $this->mensageiro
                ->setMensageiro(
                    "Erro ao Retornar as Funcionalidades do Sistema!", \Ead1_IMensageiro::ERRO, $e->getMessage()
                );
        }

        return $this->mensageiro;
    }
}

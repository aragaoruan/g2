<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 13/11/13
 * Time: 09:55
 */

namespace G2\Negocio;
use \G2\Entity\Erro as EntityErro;

class Erro extends Negocio {

    private $repositoryName = 'G2\Entity\Erro';

    /**
     * @param array $dados
     * @return mixed
     */
    public function saveError(array $dados = array()){
        $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);

        if ((!array_key_exists("id_sistema", $dados) && !array_key_exists('sistema', $dados)) || is_null($dados['id_sistema']) && is_null($dados['sistema'])){
            $dados['sistema'] = \G2\Constante\Sistema::FLUXUS;
        }elseif(array_key_exists('sistema', $dados) && !is_null($dados['id_sistema'])){
            $dados['sistema'] = $dados['id_sistema'];
        }

        if (!array_key_exists('id_usuario', $dados) || is_null($dados['id_usuario'])){
            $dados['id_usuario'] = $this->sessao->id_usuario;
        }

        if (!array_key_exists('id_entidade', $dados) || is_null($dados['id_entidade'])){
            $dados['id_entidade'] = $this->sessao->id_entidade;
        }

        if ((!array_key_exists("processo", $dados) && !array_key_exists('id_processo', $dados)) || is_null($dados['processo']) && is_null($dados['id_processo']) ){
            $dados['processo'] = 1;
        }elseif(!is_null($dados['id_processo'])){
            $dados['processo'] = $dados['id_processo'];
        }

        $erro = $serialize->arrayToEntity($dados, new \G2\Entity\Erro());

       $this->save($erro);
    }
} 
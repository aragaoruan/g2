<?php
/**
 * Created by PhpStorm.
 * User: Unyleya06
 * Date: 30/10/2015
 * Time: 11:13
 */

namespace G2\Negocio;

/**
 * Class PessoaDadosComplementares
 * @package G2\Negocio
 */
class PessoaDadosComplementares extends Negocio
{
    private $repositoryName = '\G2\Entity\PessoaDadosComplementares';


    /**
     * Salvar dados complementares
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function salvarDadosComplementares($data)
    {
        $id_attribute = 'id_usuario';

        //limpa os dados do campo st_descricaodeficiencia
        $data['st_descricaodeficiencia'] = trim($data['st_descricaodeficiencia']);

        //Verifica e salva na entity se houver entradas na tb para o usuário em questão.
        $entity = $this->findOneBy($this->repositoryName, array('id_usuario' => $data[$id_attribute]));

        //Se não houver entradas, instancia uma Entity vazia.
        if ($entity == null) {
            $entity = new $this->repositoryName;
        }

        /**********************
         * Preparar atributos FK
         **********************/
        if (isset($data['id_usuario'])) {
            $data['id_usuario'] = $this->getReference('\G2\Entity\Usuario', $data['id_usuario']);
        }

        if (isset($data['id_tipoescola'])) {
            $data['id_tipoescola'] = $this->getReference('\G2\Entity\TipoEscola', $data['id_tipoescola']);
        }

        if (isset($data['id_municipioinstituicao'])) {
            $data['id_municipioinstituicao'] = $this->getReference('\G2\Entity\Municipio', $data['id_municipioinstituicao']);
        }

        if (isset($data['sg_ufinstituicao'])) {
            $data['sg_ufinstituicao'] = $this->getReference('\G2\Entity\Uf', $data['sg_ufinstituicao']);
        }

        /**********************
         * Definindo atributos
         **********************/
        $this->arrayToEntity($entity, $data);

        /**********************
         * Acao de salvar
         **********************/
        $mensageiro = new \Ead1_Mensageiro();

        try {
            if (isset($data[$id_attribute]) && $data[$id_attribute]) {
                $entity = $this->merge($entity);
            } else {
                $entity = $this->persist($entity);
            }

            $deficiencias = !empty($data['deficiencias']) ? $data['deficiencias'] : array();
            $this->sincronizarDeficiencias($entity, $deficiencias);

            $getId = 'get' . ucfirst($id_attribute);
            $data[$id_attribute] = $entity->$getId();

            return $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $entity->getid_pessoadadoscomplementares());
        } catch (\Exception $e) {
            $mensageiro->setMensageiro(array(
                __FILE__ . ':' . __LINE__,
                $e->getMessage(),
            ), \Ead1_IMensageiro::ERRO, $data[$id_attribute]);
            $mensageiro->setText('Não foi possível salvar o registro.');
            return $mensageiro;
        }
    }

    /**
     * @param \G2\Entity\PessoaDadosComplementares $entity
     * @param array $deficiencias_manter
     * @return bool
     */
    public function sincronizarDeficiencias(\G2\Entity\PessoaDadosComplementares $entity, $deficiencias_manter = [])
    {
        if (!$entity->getId_pessoadadoscomplementares()) {
            return false;
        }

        // Buscar somente os registros que possuem o ID para, logo abaixo, remover o restante
        $ids_manter = array_filter(array_map(function ($item) {
            return $item['id_usuariodeficiencia'];
        }, $deficiencias_manter));

        // Preparar parâmetros
        $where = array('id_usuario' => $entity->getId_usuario()->getId_usuario());
        if ($ids_manter) {
            $where['id_usuariodeficiencia'] = 'NOT IN (' . implode(',', $ids_manter) . ')';
        }

        // Buscar os registros q já existem e que não foram reenviadas novamente, para remover do banco
        $defs_excluir = $this->findCustom('\G2\Entity\UsuarioDeficiencia', $where);

        // Excluir as encontradas que não foram reenviadas
        foreach ($defs_excluir as $item) {
            $this->delete($item);
        }

        // Salvar somente as novas deficiências, porquê as que já possuem ID não foram removidas do banco
        foreach ($deficiencias_manter as $deficiencia) {
            if (!$deficiencia['id_usuariodeficiencia']) {
                $en_usuariodeficiencia = new \G2\Entity\UsuarioDeficiencia();
                $en_usuariodeficiencia->setId_usuario($entity->getId_usuario());
                $en_usuariodeficiencia->setId_deficiencia(
                    $this->getReference('\G2\Entity\Deficiencia', $deficiencia['id_deficiencia'])
                );
                $this->save($en_usuariodeficiencia);
            }
        }

        return true;
    }
}

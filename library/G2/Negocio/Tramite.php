<?php

namespace G2\Negocio;

use G2\Entity\TramiteSalaDeAula;

/**
 * Classe Negocio para Tramites
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-11-05
 */
class Tramite extends Negocio
{

    /** @var \TramiteBO */
    private $bo;

    private $tramite;
    private $tramiteSalaDeAula;

    public function __construct()
    {
        $this->bo = new \TramiteBO();
        $this->tramite = new \G2\Entity\Tramite;
        $this->tramiteSalaDeAula = new TramiteSalaDeAula;

        parent::__construct();
    }

    public function __get($name)
    {
        return $this->$name;
    }

    /**
     * Salva os dados do tramite somente
     * @param array $data
     * @return \G2\Entity\Tramite
     */
    public function salvar(array $data)
    {
        /** @var '\G2\Entity\Tramite $entity */
        $entity = $this->find('\G2\Entity\Tramite', $data['id_tramite']); //Consulta o tramite
        $serializer = new \Ead1\Doctrine\EntitySerializer($this->em);
        $newEntity = $serializer->arrayToEntity($data, $entity);

        if (!$data['bl_visivel']) {
            $newEntity->setBl_visivel(false);
        }
        return parent::save($newEntity);
    }

    /**
     * @param $st_tramite
     * @return \Ead1_Mensageiro
     */
    public function salvarTramiteTCC($st_tramite)
    {
        {
            try {

                $entity = new \G2\Entity\Tramite();
                $entity->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \TramiteMatriculaTO::LIBERARTCC))
                    ->setId_usuario($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario))
                    ->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade))
                    ->setSt_tramite($st_tramite)
                    ->setDt_cadastro(new \DateTime())
                    ->setBl_visivel(1);
                $this->save($entity);

                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($entity->getId_tramite());
            } catch (\Exception $exc) {
                $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
            }
            return $mensageiro;
        }
    }

    public function salvarTramiteLiberarTCC($id_tramite, $id_alocacao)
    {

        try {
            $to = new \AlocacaoTO();
            $to->setId_alocacao($id_alocacao);
            $to->fetch(true, true, true);

            $toMat = new \MatriculaDisciplinaTO();
            $toMat->setId_matriculadisciplina($to->getId_matriculadisciplina());
            $toMat->fetch(true, true, true);

            if ($toMat->getId_matricula()) {

                $bo = new \TramiteBO();
                $toTM = new \TramiteMatriculaTO();
                $toTM->setId_matricula($toMat->getId_matricula());
                $toTM->setId_tramite($id_tramite);
                $mensageiro = $bo->cadastrarTramiteMatricula($toTM);

            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_ERRO, \Ead1_IMensageiro::ERRO);
            }


        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Retorna tramites
     * @param $idCategoriaTramite - Categoria do tramite
     * @param array $params - array com parametros. Par chave valor, ex: array(0=>[valor])
     * @param null $idTipoTramite
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornarTramiteSp($idCategoriaTramite, array $params, $idTipoTramite = null)
    {
        try {
            //Chama o método que retorna o tramite lá da bo
            $mensageiro = $this->bo->retornarTramiteSp($idCategoriaTramite, $params, $idTipoTramite);
            return $mensageiro; //retorna o mensageiro.
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar tramites. " . $e->getMessage());
        }
    }

    /**
     * Salva um tramite manual
     * @param array $data
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function salvarTramite(array $data, $arquivo = null)
    {
        try {
            if ($data) { //verifica se o $data não veio vazio
                //cria as variaveis para auxiliar
                $idCategoriaTramite = null;
                $idChave = null;

                //Instancia a TO
                $tramiteTO = new \TramiteTO();
                $tramiteTO->setBl_visivel(1);
                $tramiteTO->setDt_cadastro(new \Zend_Date());
                $tramiteTO->setId_entidade(!empty($data['id_entidade']) ? $data['id_entidade'] : $tramiteTO->getSessao()->id_entidade);
                $tramiteTO->setId_usuario(!empty($data['id_usuario']) ? $data['id_usuario'] : $tramiteTO->getSessao()->id_usuario);

                //Verifica se o st_tramite foi passado pelo parametro
                if (array_key_exists('st_tramite', $data) && !empty($data['st_tramite'])) {
                    $tramiteTO->setSt_tramite($data['st_tramite']); //atribui o valor na TO
                } else {
                    throw new \Exception('Texto do tramite não pode ser vazio.');
                }

                //Verifica se foi passado uma chave com id_categoriatramite e se ela não é vazia
                if (array_key_exists('id_categoriatramite', $data) && !empty($data['id_categoriatramite'])) {
                    $idCategoriaTramite = $data['id_categoriatramite']; //atribui o valor em $idCategoriaTramite
                } else {
                    throw new \Exception('Informe a categoria do tramite.');
                }

                //Verifica se existe uma posição no array chave
                if (array_key_exists('id_campo', $data) && !empty($data['id_campo'])) {
                    $idChave = $data['id_campo'];
                } else {
                    throw new \Exception('Informe a categoria do tramite.');
                }
                //Manda para o método na BO
                $mensageiro = $this->bo->cadastrarTramite($tramiteTO, $idCategoriaTramite, $idChave, $arquivo);
                return $mensageiro; //retorna o mensageiro
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro("Erro ao tentar salvar tramite. " . $e->getMessage(),
                \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * @param $stTramite - mensagem descrevendo o tramite
     * @param $idMatricula - id da matrícula
     * @param int $idtipotramite - tipo do tramite
     * @param \G2\Entity\Usuario|null $usuario - Deve ser uma instância de usuário
     * @param null $idEntidade - id da entidade para caso do método esteja sendo utilizado pelo robô ou coisa do tipo
     * que não tenha sessão setado
     * @return mixed
     * @throws \Exception
     */
    public function salvarTramiteMatricula($stTramite, $idMatricula, $idtipotramite = \TramiteMatriculaTO::EVOLUCAO, \G2\Entity\Usuario $usuario = null, $idEntidade = null)
    {
        try {
            if (!$idEntidade && !$this->sessao->id_entidade) {
                throw new \Exception("Não é possível salvar trâmite sem o ID da entidade.");
            }


            $entity = new \G2\Entity\Tramite();
            $entity->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', $idtipotramite))
                ->setId_usuario($usuario ?: $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario))
                ->setId_entidade($this->getReference('\G2\Entity\Entidade', $idEntidade ? $idEntidade : $this->sessao->id_entidade))
                ->setSt_tramite($stTramite)
                ->setDt_cadastro(new \DateTime())
                ->setBl_visivel(1);
            $tramite = $this->save($entity);

            $tramiteReference = $this->getReference('\G2\Entity\Tramite', $tramite->getId_tramite());
            $matriculaReference = $this->getReference('\G2\Entity\Matricula', $idMatricula);

            $tramiteMatricula = new \G2\Entity\TramiteMatricula();
            $tramiteMatricula->setId_matricula($matriculaReference);
            $tramiteMatricula->setId_tramite($tramiteReference);

            $retorno = $this->save($tramiteMatricula);

        } catch (\Exception $e) {
            throw new \Exception("Erro ao salvar Trâmite: " . $e->getMessage());
        }
        return $retorno;
    }

    /**
     * @description Recebe uma entity Matricula (no caso de PÓS) OU MatriculaDisciplina (no caso de GRAD) para identificar o id_situacaoagendamento ATUAL e o novo, e então gerar o trâmite de (APTO/NÃO APTO) para agendamento
     * @param \G2\Entity\Matricula | \G2\Entity\VwAlunosAptosAgendamentoPorDisciplina $matricula
     * @param int $id_situacaoagendamento
     * @param int $id_usuariocadastro
     * @param int $id_entidade
     * @param int $id_situacaoagendamentoparcial
     * @return bool
     */
    public function salvarTramiteAptoAgendamento($matricula, $id_situacaoagendamento, $id_usuariocadastro = null, $id_entidade = null, $id_situacaoagendamentoparcial = null)
    {
        // Buscar o id_situacaoagendamento ATUAL
        $id_situacaoagendamento_atual = $matricula->getId_situacaoagendamento();
        if (is_object($id_situacaoagendamento_atual)) {
            $id_situacaoagendamento_atual = $id_situacaoagendamento_atual->getId_situacao();
        }

        /**
         * @history GII-7924
         */

        /*
        // Adicionando verificação pois essa coluna existe somente na matricula
        $id_situacaoagendamentoparcial_atual = $matricula instanceof \G2\Entity\Matricula ? $matricula->getId_situacaoagendamentoparcial() : null;
        if (is_object($id_situacaoagendamentoparcial_atual)) {
            $id_situacaoagendamentoparcial_atual = $id_situacaoagendamentoparcial_atual->getId_situacao();
        }

        // Flag salvar tramite será TRUE somente se situacao diferenciar da flag $bl_alunoapto
        //Ou se o id_situacaoagedamentoparcial for difererente da situacao parcial já salva
        $bl_salvartramite = ($id_situacaoagendamento_atual != $id_situacaoagendamento) || ($id_situacaoagendamentoparcial_atual != $id_situacaoagendamentoparcial);
        */

        $bl_salvartramite = $id_situacaoagendamento_atual != $id_situacaoagendamento;

        if (!$bl_salvartramite) {
            return false;
        }

        // Parâmetros padrão
        $id_usuariocadastro = $id_usuariocadastro ? $id_usuariocadastro : $this->sessao->id_usuario;
        $id_entidade = $id_entidade ? $id_entidade : $this->sessao->id_entidade;

        if (in_array($id_situacaoagendamento, array(
            \G2\Constante\Situacao::TB_MATRICULA_APTO,
            \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_APTO_RECUPERACAO
        ))) {
            $st_tramite = 'O aluno se tornou APTO ao agendamento de Prova após alteração da nota.';
        } else {
            $st_tramite = 'O aluno se tornou NÃO APTO ao agendamento de Prova após alteração da nota.';
        }

        /**
         * @history GII-7924
         */

        //if ($id_situacaoagendamentoparcial) {
        //    /** @var \G2\Entity\Situacao $situacao_en */
        //    $situacao_en = $this->getReference('\G2\Entity\Situacao', (int)$id_situacaoagendamentoparcial);
        //    if ($situacao_en instanceof \G2\Entity\Situacao)
        //        $st_tramite .= ' [' . $situacao_en->getSt_descricaosituacao() . '.]';
        //}

        $tramite = new \G2\Entity\Tramite();
        $tramite
            ->setId_usuario($this->getReference('\G2\Entity\Usuario', $id_usuariocadastro))
            ->setId_entidade($this->getReference('\G2\Entity\Entidade', $id_entidade))
            ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::MATRICULA_AUTOMATICO))
            ->setSt_tramite($st_tramite)
            ->setDt_cadastro(date('Y-m-d H:i:s'))
            ->setBl_visivel(true);

        $this->save($tramite);

        if ($tramite && $tramite->getId_tramite()) {
            # há um fluxo de execução que $matricula na verdade é uma instancia
            # de G2\Entity\VwAlunosAptosAgendamentoPorDisciplina e quando isto
            # ocorrer, será necessário criar uma referencia de \G2\Entity\Matricula
            # requerida pelo '$tramitematricula->setId_matricula'
            if ($matricula instanceof \G2\Entity\VwAlunosAptosAgendamentoPorDisciplina) {
                $idMatricula = $matricula->getId_matricula();
                if (is_integer($idMatricula)) {
                    $matricula = $this->getReference('\G2\Entity\Matricula', $idMatricula);
                }
            }

            $tramitematricula = new \G2\Entity\TramiteMatricula();
            $tramitematricula->setId_matricula($matricula)
                ->setId_tramite($tramite);

            $this->save($tramitematricula);
        }

        return ($tramite instanceof \G2\Entity\Tramite && $tramite->getId_tramite());
    }

    /**
     * Salva o tramite da matrícula
     * @param $idMatricula
     * @param $dt_concluinte_nova
     * @param $idtipotramite
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarTramiteDataConclusao($idMatricula, $dt_concluinte_nova, $idtipotramite = \TramiteMatriculaTO::AUTOMATICO)
    {

        if ($dt_concluinte_nova instanceof \Zend_Date) {
            $dt_concluinte = $dt_concluinte_nova->tostring('YYYY-MM-dd');
        } else {
            $dt_concluinte = $dt_concluinte_nova;
        }

        $vmat = new \MatriculaTO();
        $vmat->setId_matricula($idMatricula);
        $vmat->fetch(true, true, true);
        if ($vmat->getDt_concluinte() instanceof \Zend_Date) {
            $dt_concluinte_antigo = $vmat->getDt_concluinte()->tostring('YYYY-MM-dd');
            if ($dt_concluinte_antigo != $dt_concluinte) {
                $dt_concluinte = new \Zend_Date($dt_concluinte);
                $st_tramite = 'Data de conclusão do curso alterado de ' . $vmat->getDt_concluinte()->tostring('dd/MM/YYYY') . ' para ' . $dt_concluinte->tostring('dd/MM/YYYY') . '.';
            }
        } else {
            $dt_concluinte = new \Zend_Date($dt_concluinte);
            $st_tramite = 'Data de conclusão informada ' . $dt_concluinte->tostring('dd/MM/YYYY') . '.';
        }

        if ($st_tramite) {
            return $this->salvarTramiteMatricula($st_tramite, $idMatricula, $idtipotramite);
        } else {
            return true;
        }
    }

    /**
     * Recupera um ou mais tramites da matrícula
     * @param int $idMatricula
     * @param boolean $inHistorico
     * @param array $orderBy
     * @param $boQtd
     * @param $boReturnArray
     * @return mixed
     */
    public function getTramiteMatricula($idMatricula, $inHistorico = false, array $orderBy = null, $boQtd = null, $boReturnArray = false)
    {
        $query = $this->getem()
            ->createQueryBuilder()
            ->select("tm")
            ->from("\G2\Entity\TramiteMatricula", "tm")
            ->orderBy('tm.id_tramitematricula', 'DESC');

        if ($boReturnArray) {
            $query->select("tm.id_tramitematricula,
                                   t.id_tramite,
                                   m.id_matricula")
                ->join("tm.id_tramite", "t")
                ->join("tm.id_matricula", "m");
        }
        $query->andWhere($query->expr()->eq("tm.id_matricula", ":id"))
            ->setParameter("id", $idMatricula);

        if ($inHistorico) {
            $query->leftJoin("\G2\Entity\HistoricoMatriculaGradeNota", "hmgn", 'WITH', 'hmgn.id_tramitematricula = tm.id_tramitematricula');
            $query->andWhere($query->expr()->eq("hmgn.id_matricula", ":id"));
        }

        if (count($orderBy) > 0) {
            foreach ($orderBy as $campo => $order) {
                $query->addOrderBy("tm.{$campo}", '' != $order ? $order : null);
            }
        }

        if (null != $boQtd) {
            $query->setMaxResults($boQtd);

            if (1 == $boQtd) {
                return $query->getQuery()->getSingleResult();
            }
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Recupera um ou mais tramites da matrícula
     * @param int $idMatricula
     * @param int $idHistoricoGradeNota
     * @param array $orderBy
     * @param $boQtd
     * @param $boReturnArray
     * @return mixed
     */
    public function getTramiteMatriculaHistoricoMatriculaGradeNota($idMatricula, $idHistoricoGradeNota = null,
                                                                   array $orderBy = null, $boQtd = null, $boReturnArray = false)
    {
        $query = $this->getem()
            ->createQueryBuilder()
            ->select("hmgn")
            ->from("\G2\Entity\HistoricoMatriculaGradeNota", "hmgn")
            ->orderBy('hmgn.id_tramitematricula', 'DESC');

        if ($boReturnArray) {
            $query->select("hmgn.id_historicogradenota,
                                   m.id_matricula,
                                   pp.id_projetopedagogico,
                                   tm.id_tramitematricula,
                                   hmgn.dt_cadastro,
                                   hmgn.st_htmlgrade")
                ->join("hmgn.id_matricula", "m")
                ->join("hmgn.id_projetopedagogico", "pp")
                ->join("hmgn.id_tramitematricula", "tm");
        }

        $query->andWhere($query->expr()->eq("hmgn.id_matricula", ":id"))
            ->setParameter("id", $idMatricula);

        if (null !== $idHistoricoGradeNota) {
            $query->andWhere($query->expr()->eq("hmgn.id_historicogradenota", ":id_historicogradenota"))
                ->setParameter("id_historicogradenota", $idHistoricoGradeNota);
        }

        if (count($orderBy) > 0) {
            foreach ($orderBy as $campo => $order) {
                $query->addOrderBy("hmgn.{$campo}", '' != $order ? $order : null);
            }
        }

        if (null != $boQtd) {
            $query->setMaxResults($boQtd);

            if (1 == $boQtd) {
                return $query->getQuery()->getSingleResult();
            }
        }

        return $query->getQuery()->getResult();
    }
}

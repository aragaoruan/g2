<?php

namespace G2\Negocio;

use G2\Entity\EncerramentoSala;

/**
 * Classe para assuntos de secretaria.
 *
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 */
class Secretaria extends Negocio
{

    /**
     * Metodo responsavel por retornar dados da vwProjetosPedagogicos
     * @param array $where
     */
    public function retornaVwProjetoPedagogico(array $where = null)
    {
        return $this->findBy('\G2\Entity\VwProjetoPedagogico', $where);
    }

    /**
     * Retorna \G2\Entity\VwProjetoEntidade
     * @param array $where
     * @return type array
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwProjetoEntidade(array $where = \NULL)
    {
        return $this->findBy('\G2\Entity\VwProjetoEntidade', $where, array(
            'st_projetopedagogico' => 'ASC'
        ));
    }


    /**
     * Salvar RecusaPagamento
     * @param array $data
     */
    public function salvarRecusaPagamento(array $data)
    {
        try {
//           \Zend_Debug::dump($data);die;
            if (!array_key_exists('id_encerramentosala', $data) || empty($data['id_encerramentosala'])) {
                throw new \Exception('O id_encerramentosala é obrigatório e não foi passado!');
            }

            if (!array_key_exists('st_recusa', $data) || empty($data['st_recusa']) || strlen($data['st_recusa']) < 15) {
                throw new \Exception('O motivo da recusa é obrigatório e não foi passado ou contém menos que 15 caracteres!');
            }
            $entity = $this->em->getReference('G2\Entity\EncerramentoSala', (int)$data['id_encerramentosala']);
            $usario_recusa = $this->em->getReference('G2\Entity\Usuario', (int)$this->sessao->id_usuario);

//           $entity = new EncerramentoSala();
            $entity->setSt_motivorecusa($data['st_recusa']);
            $entity->setId_usuariorecusa($usario_recusa);
            $entity->setDt_recusa(new \DateTime());


            $retorno = $this->save($entity);
            $mensageiro = new \Ead1_Mensageiro('Recusa salva com sucesso!', \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Recusa salva com sucesso');
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    public function liberaPagamento($data)
    {
        try {
//           \Zend_Debug::dump($data);die;
            if (!array_key_exists('id_encerramentosala', $data) || empty($data['id_encerramentosala'])) {
                throw new \Exception('O id_encerramentosala é obrigatório e não foi passado!');
            }

            $entity = $this->em->getReference('G2\Entity\EncerramentoSala', (int)$data['id_encerramentosala']);
            $entity->setDt_recusa(null);


            $retorno = $this->save($entity);
            $mensageiro = new \Ead1_Mensageiro('Pagamento liberado com sucesso!', \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Pagamento liberado com sucesso');
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Busca Requerimentos pela ocorrência gerada
     * @param array $data
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function buscaOcorrencia(array $params = null)
    {
        try {
            $params['id_usuario'] = isset($params['id_usuario']) ? $params['id_usuario'] : $this->sessao->id_usuario; //força o id_usuario
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade; //força o id_entidade

            return $this->em->getRepository('\G2\Entity\Ocorrencia')->pesquisarOcorrencia($params, true);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Buscar a Ocorrencia e os seus produtos de acordo com o id_venda
     * @param array $param
     * @return array
     */
    public function buscaOcorrenciaProduto(array $params)
    {
        try {
            $ocorrenciaNegocio = new \G2\Negocio\Ocorrencia();
            $ocorrencia = $this->em->getRepository('\G2\Entity\Ocorrencia')->pesquisarOcorrencia($params);
            $ocorrencia = $ocorrencia[0];

            $returnProduto = $ocorrenciaNegocio->getServicos($params);
            $ocorrencia['produto'] = $returnProduto['st_produto'];

            return $ocorrencia;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
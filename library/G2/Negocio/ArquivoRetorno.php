<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Entity\ArquivoRetornoLancamento;
use G2\Negocio\Negocio;

use Doctrine\ORM\Mapping\ClassMetadata,
    Doctrine\Common\Util\Inflector,
    Doctrine\ORM\EntityManager,
    Exception,
    Ead1\Financeiro\Fluxus\Integracao as Integracao,
    G2\Negocio\Erro as ErroNegocio;

class ArquivoRetorno  extends Negocio{

    public function __construct(){
        parent::__construct();
    }

    public function retornarVwRetornoLancamento(\VwRetornoLancamentoTO $to){
        try {
            $select = $this->em->createQueryBuilder()
                                ->select('vw')
                                ->from('G2\Entity\VwRetornoLancamento', 'vw')
                                ->where('vw.id_entidade = :id_entidade');

            $params['id_entidade'] = ($to->getId_entidade() ? $to->getId_entidade() : $to->getSessao()->id_entidade);
            foreach ($to as $key => $value) {
                if($value === 'is null'){
                    $select->andWhere('vw.'.$key.' is null');
                }elseif($key != 'id_entidade' && !empty($value) || $value === '0' || $value === 0){
                    $select->andWhere('vw.'.$key.' = :'.$key);
                    $params[$key] = $value;
                }
            }

            $lancamentos =  $select->setParameters($params)->getQuery()->getResult();

            return new \Ead1_Mensageiro($lancamentos, \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            return new \Ead1_Mensageiro("Erro ao listar Vw_retornolancamento: ".$e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    public function quitarLancamentosArquivoRetorno(array $arrTo){
        $quitados 		= 0;
        $naoquitados 	= 0;
        $fiBO = new \VendaBO();
        try {
            /** @var \G2\Entity\VwRetornoLancamento $to */
            foreach($arrTo as $to){
                $dt_quitado = $to->getDt_quitadoretorno();
                if(!$to instanceof \G2\Entity\VwRetornoLancamento){
                    throw new \Zend_Exception('O array deve ser composto de \G2\Entity\VwRetornoLancamento');
                }

                if(!$to->getBl_quitadoretorno() && $to->getId_lancamento()){

                    $laTO = new \LancamentoTO();
                    $laTO->setId_lancamento($to->getId_lancamento());
                    $laTO->fetch(true, true, true);

                    $laTO->setNu_quitado($to->getNu_valorretorno()+$to->getNu_tarifa());
                    $laTO->setBl_baixadofluxus(true);
                    $laTO->setBl_quitacaoretorno(true);
                    $laTO->setSt_statuslan(1);
                    $laTO->setNu_descontoboleto($to->getNu_descontoretorno());
                    $laTO->setNu_jurosboleto($to->getNu_jurosretorno());
                    $laTO->setId_usuarioquitacao($laTO->getSessao()->id_usuario);

                    $laTO->setDt_quitado($dt_quitado->format('d/m/Y'));

                    $me = $fiBO->quitarBoleto($laTO);
                    if($me->getTipo() != \Ead1_IMensageiro::SUCESSO){
                        throw new \Zend_Exception('Erro ao quitar o Boleto do Arquivo retorno: '.$me->getFirstMensagem());
                    } else {
                        /** @var \G2\Entity\ArquivoRetornoLancamento $relato */
                        $relato = $this->em->getRepository('G2\Entity\ArquivoRetornoLancamento')->findOneBy(array('id_arquivoretornolancamento' => $to->getId_arquivoretornolancamento()));
                        $relato->setBl_Quitado(true);
                        $relato->setId_Lancamento($laTO->getId_lancamento());

                        $this->save($relato);
                    }

                    $quitados++;
                } else {
                    $naoquitados++;
                }

            }

            $mensagem = '';
            if($quitados){
                $mensagem = $quitados.' boletos foram quitados com sucesso. ';
            }

            if($naoquitados){
                $mensagem .= $naoquitados.' não foram quitados.';
            }

            return new \Ead1_Mensageiro($mensagem, \Ead1_IMensageiro::SUCESSO);
        } catch (Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }
}
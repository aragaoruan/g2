<?php

namespace G2\Negocio;

use G2\Constante\Evolucao;
use G2\Constante\Situacao;
use G2\Constante\TipoTramite;
use G2\Entity\OcorrenciaResponsavel;
use G2\Entity\Upload;
use G2\Entity\VwNucleoOcorrencia;
use G2\Entity\VwOcorrenciasPessoa;

/**
 * Classe de negócio para Central de Atenção
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class CAtencao extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que trata reabertura de ocorrencias
     * @param array $dados
     * @return \Ead1_Mensageiro
     */
    public function reabrirOcorrencia(array $dados)
    {
        try {
            $validaCampos = $this->validaCamposObrigatorios($dados, array('st_tramite', 'id_evolucao', 'id_ocorrencia', 'id_situacao'));
            if ($validaCampos) {
                throw new \Zend_Exception($validaCampos);
            }
            $validaJustificativa = new \Zend_Validate_StringLength(array('min' => 15, 'max' => 8000));
            if (!$validaJustificativa->isValid($dados['st_tramite'])) {
                throw new \Zend_Exception('A justificativa pra reabrir a ocorrência é obrigatória e deve conter no mínimo 15 caracteres.!');
            }

            $tramite = new \G2\Entity\Tramite();
            $tramite->setSt_tramite($dados['st_tramite']);

            $ocorrencia = $this->find('G2\Entity\Ocorrencia', (int)$dados['id_ocorrencia']);
            if ($ocorrencia instanceof \G2\Entity\Ocorrencia) {

                $ocorrencia->setId_evolucao($this->find('G2\Entity\Evolucao', (int)$dados['id_evolucao']));
                $ocorrencia->setId_situacao($this->find('G2\Entity\Situacao', $dados['id_situacao']));

                //Retirando atendentes ativos da ocorrencia caso tenha
                $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
                $negocioOcorrencia->deletarOcorrenciaResponsavel(array('id_ocorrencia' => (int)$dados['id_ocorrencia']));

            } else {
                throw new \Zend_Exception('Ocorrência não encontrada para alteração');
            }

            if ($this->persist($ocorrencia)) {
                return $this->salvaTramiteReabrirOcorrencia($ocorrencia, $tramite);
            } else {
                return new \Ead1_Mensageiro('Ocorreu um erro ao salvar a ocorrência. Verifique os dados e tente novamente');
            }

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Tramite para reabrir ocorrencia -> sem upload de arquivos
     * @param Ocorrencia $ocorrencia
     * @param Tramite $tramite
     * @return \Ead1_Mensageiro
     */
    public function salvaTramiteReabrirOcorrencia(\G2\Entity\Ocorrencia $ocorrencia, \G2\Entity\Tramite $tramite = null)
    {
        try {
            if (((int)$ocorrencia->getId_ocorrencia()) == false) {
                throw new \Zend_Exception('O id da ocorrencia não foi informado!');
            }
            $this->beginTransaction();

            if ($ocorrencia->getId_situacao()->getId_situacao() == Situacao::TB_OCORRENCIA_ENCERRADA || $ocorrencia->getId_evolucao()->getId_evolucao() == Evolucao::REABERTURA_REALIZADA_INTERESSADO):

                if ($ocorrencia->getId_situacao()->getId_situacao() == Situacao::TB_OCORRENCIA_ENCERRADA):
                    $tramite->setSt_tramite('Ocorrência N ' . $ocorrencia->getId_ocorrencia() . ' encerrada por ' . $this->sessao->st_nomeexibicao . '.');
                elseif ($ocorrencia->getId_evolucao()->getId_evolucao() == Evolucao::REABERTURA_REALIZADA_INTERESSADO):
                    $tramite->setSt_tramite('Ocorrência N. ' . $ocorrencia->getId_ocorrencia() . ' reaberta por ' . $this->sessao->st_nomeexibicao . ' por motivo: ' . $tramite->getSt_tramite() . ' .');
                endif;

                $tramite->setId_usuario($this->find('G2\Entity\Usuario', $this->sessao->id_usuario));
                $tramite->setId_entidade($this->find('G2\Entity\Entidade', $this->sessao->id_entidade));
                $tramite->setDt_cadastro(new \DateTime());
                $tramite->setBl_visivel(1);
                $tramite->setId_tipotramite($this->find('G2\Entity\TipoTramite', 13));
                $this->save($tramite);


                if ($tramite->getId_tramite()) {
                    if (!$this->salvaTbTramiteOcorrencia($ocorrencia, $tramite)) {
                        throw new \Zend_Exception('Ocorreu um erro ao salvar o vinculo da ocorrencia com o tramite.');
                    }
                } else {
                    throw new \Zend_Exception('Erro ao salvar tramite');
                }

            endif;
            $this->commit();
            return new \Ead1_Mensageiro('Ocorrência alterada com sucesso! ', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao alterar ocorrência -  ' . $e, \Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Salva vinculo da ocorrencia com o tramite
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @param \G2\Entity\Tramite $tramite
     * @return \G2\Entity\TramiteOcorrencia|mixed
     * @throws \Exception
     */
    public function salvaTbTramiteOcorrencia(\G2\Entity\Ocorrencia $ocorrencia, \G2\Entity\Tramite $tramite)
    {
        try {
            $tramiteOcorrencia = new \G2\Entity\TramiteOcorrencia();
            $tramiteOcorrencia->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $tramiteOcorrencia->setId_tramite($tramite->getId_tramite());
            if (APPLICATION_SYS == \G2\Constante\Sistema::GESTOR):
                $tramiteOcorrencia->setBl_interessado(false);
            else:
                $tramiteOcorrencia->setBl_interessado(true);
            endif;
            $tramiteOcorrencia = $this->save($tramiteOcorrencia);
            if ($tramiteOcorrencia instanceof \G2\Entity\TramiteOcorrencia && $tramiteOcorrencia->getId_tramiteocorrencia()) {
                return $tramiteOcorrencia;
            }
        } catch (\Exception $e) {
            throw $e;
        }

    }


    /**
     * Verifica se os valores defaults não foram passados e seta os valores se for o caso.
     * @param array $ocorrencia
     * @return array
     */
    private function defineValoresDefaultOcorrencia(array $ocorrencia)
    {


        if (array_key_exists('cb_id_categoriaocorrencia', $ocorrencia)) {
            $ocorrencia['id_categoriaocorrencia'] = $ocorrencia['cb_id_categoriaocorrencia'];
            unset($ocorrencia['cb_id_categoriaocorrencia']);
        }

        if (array_key_exists('cb_id_assuntopai', $ocorrencia)) {
            $ocorrencia['id_assuntopai'] = $ocorrencia['cb_id_assuntopai'];
            unset($ocorrencia['cb_id_assuntopai']);
        }

        if (array_key_exists('cb_id_assuntoco', $ocorrencia)) {
            $ocorrencia['id_assuntoco'] = $ocorrencia['cb_id_assuntoco'];
            unset($ocorrencia['cb_id_assuntoco']);
        }

        if (array_key_exists('cb_id_saladeaula', $ocorrencia)) {
            $ocorrencia['id_saladeaula'] = $ocorrencia['cb_id_saladeaula'];
            unset($ocorrencia['cb_id_saladeaula']);
        }

        if (array_key_exists('id_usuario', $ocorrencia)) {
            $ocorrencia['id_usuariointeressado'] = $ocorrencia['id_usuario'];
            $ocorrencia['id_usuariocadastro'] = $ocorrencia['id_usuario'];
            unset($ocorrencia['id_usuario']);
        }

        //seta o valor default da evolucao caso nao tenha sido passada
        if (!isset($ocorrencia['id_evolucao']) || empty($ocorrencia['id_evolucao'])) {
            $ocorrencia['id_evolucao'] = \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO;
        }

        //seta o valor default da situacao caso nao tenha sido passada
        if (!isset($ocorrencia['id_situacao']) || empty($ocorrencia['id_situacao'])) {
            $ocorrencia['id_situacao'] = \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE;
        }

        //se o usuario cadastro nao for passado ou for passado como vazio se o valor da sessao
        if (!isset($ocorrencia['id_usuariocadastro']) || empty($ocorrencia['id_usuariocadastro'])) {
            $ocorrencia['id_usuariocadastro'] = $this->sessao->id_usuario;
        }

        //verifica se o id do usuario interessado não foi passado ou é vazio, seta o valor da sessao
        if (!isset($ocorrencia['id_usuariointeressado']) || empty($ocorrencia['id_usuariointeressado'])) {
            $ocorrencia['id_usuariointeressado'] = $this->sessao->id_usuario;
        }

        //verifica se o id da ocorrencia não foi passado ou é vazio, seta o valor da sesao
        if (!isset($ocorrencia['id_entidade']) || empty($ocorrencia['id_entidade'])) {
            $ocorrencia['id_entidade'] = $this->sessao->id_entidade;
        }


        $dataHoje = new \DateTime();

        if (!isset($ocorrencia['dt_cadastro']) || empty($ocorrencia['dt_cadastro'])) {
            $ocorrencia['dt_cadastro'] = $dataHoje;
        }

        if (!isset($ocorrencia['dt_atendimento']) || empty($ocorrencia['dt_atendimento'])) {
            $ocorrencia['dt_atendimento'] = $dataHoje;
        }

        if (!isset($ocorrencia['dt_cadastroocorrencia']) || empty($ocorrencia['dt_cadastroocorrencia'])) {
            $ocorrencia['dt_cadastroocorrencia'] = $dataHoje;
        }


        return $ocorrencia;
    }


    /**
     * @param $dados
     * @param null $arquivo
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarOcorrencia($dados, $arquivo = null)
    {
        try {
            $this->beginTransaction();

            //limpa o título e conteúdo da ocorrência
            if (!empty($dados['st_titulo'])) {
                $dados['st_titulo'] = $this->limpaEmojiTexto($dados['st_titulo']);
            }

            if (!empty($dados['st_ocorrencia'])) {
                $dados['st_ocorrencia'] = $this->limpaEmojiTexto($dados['st_ocorrencia']);
            }

            $dados = $this->defineValoresDefaultOcorrencia($dados);
            $validaCampos = $this->validaCamposObrigatorios($dados, array(
                'st_ocorrencia',
                'st_titulo',
                'id_assuntoco',
                'id_categoriaocorrencia',
            ));

            if ($validaCampos) {
                throw new \Zend_Exception($validaCampos);
            }

            $ocorrencia = new \G2\Entity\Ocorrencia();
            $ocorrencia->montarEntity($dados);

            //verifica se a matricula foi passada
            if ($ocorrencia->getId_matricula()) {
                //força a ocorrencia a receber a entidade como sendo a da matricula
                $ocorrencia->setId_entidade($this->getReference('\G2\Entity\Entidade', $ocorrencia->getId_matricula()->getId_entidadematricula()));

                //força a ocorrencia a receber o usuario interessado com o da matricula
                $ocorrencia->setId_usuariointeressado($ocorrencia->getId_matricula()->getId_usuario());

            }


            // Salvar a ocorrencia somente se nao existir uma ocorrencia cadastrada ha ate
            // 10 minutos atras com o mesmo id_usuariointeressado, id_matricula e id_assuntoco
            if ($this->possuiOcorrenciaCadastradaRecentemente($ocorrencia, 10)) {
                throw new \Exception("Uma ocorrência com o mesmo assunto foi criada recentemente. Aguarde alguns instantes e tente novamente.");
            }

            //GII-9366 (Não permitir cadastro de ocorrencia com mesmo assunto e subassunto em aberto.
            if ($this->possuiOcorrenciaComMesmoAssuntoSubassunto($dados)) {
                throw new \Exception("Já existe ocorrência em andamento com o assunto e subassunto escolhidos.");
            }

            // Se nao possuir o que foi citado acima, continuar com o save
            $ocorrencia = $this->save($ocorrencia);

            if ($ocorrencia instanceof \G2\Entity\Ocorrencia && $ocorrencia->getId_ocorrencia()) {
                $this->vinculaResposansavelOcorrencia($ocorrencia);


                $ocorrenciaNegocio = new Ocorrencia();
                //verifica se a variavel de retorno existe e chama o método para criar o job da ocorrencia no servidor
                $ocorrenciaNegocio->criarJobOcorrenciaJira($ocorrencia->getId_ocorrencia());

                $sessao = \Ead1_Sessao::getObjetoSessaoGeral();

                if (isset($sessao->nomeUsuario) && $sessao->nomeUsuario != '') {
                    $tramite1 = new \G2\Entity\Tramite();
                    $tramite1->setSt_tramite('A ocorrência foi aberta pelo atendente ' . $sessao->nomeUsuario . ' para atendimento de demanda do interessado.')
                        ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO))
                        ->setId_entidade($ocorrencia->getId_entidade())
                        ->setBl_visivel(true)
                        ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

                    $this->salvaTramite($ocorrencia, $tramite1);
                }

                if (isset($sessao->nomeUsuario) && $sessao->nomeUsuario != '') {
                    $tramite1 = new \G2\Entity\Tramite();
                    $tramite1->setSt_tramite('A ocorrência foi aberta pelo atendente ' . $sessao->nomeUsuario . ' para atendimento de demanda do interessado.')
                        ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO))
                        ->setId_entidade($ocorrencia->getId_entidade())
                        ->setBl_visivel(true)
                        ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

                    $this->salvaTramite($ocorrencia, $tramite1);
                }
                if (isset($arquivo['name']) && $arquivo['name']) {
                    $tramite = new \G2\Entity\Tramite();
                    $tramite->setSt_tramite('Anexo vinculado a ocorrência.')
                        ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO))
                        ->setId_entidade($ocorrencia->getId_entidade())
                        ->setBl_visivel(true)
                        ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

                    $this->salvaTramite($ocorrencia, $tramite, $arquivo);
                }


            }

            $this->commit();

            return new \Ead1_Mensageiro($ocorrencia, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Método para vincular o responsável da Ocorrencia
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @return bool
     */
    public function vinculaResposansavelOcorrencia(\G2\Entity\Ocorrencia $ocorrencia)
    {

        /*
         * Verifica se o assunto CO foi definico ou se é uma instancia do objeto e se o auto distribuir é falso
         * Se não houver vinculo com assunto CO ele deve parar, pois se continuar acontecerá erro no restante da função
         */
        if (is_null($ocorrencia->getId_assuntoco()) || ($ocorrencia->getId_assuntoco() instanceof \G2\Entity\AssuntoCo
                && !$ocorrencia->getId_assuntoco()->getBl_autodistribuicao())
        ) {
            return true;
        }

        $id_assuntoco = $ocorrencia->getId_assuntoco()->getId_assuntoco();
        $id_assuntocopai = $ocorrencia->getId_assuntoco()->getId_assuntocopai()->getId_assuntoco();

        /**
         * Distribuicao automatica de Ocorrencias:
         * PROCURAR RESPONSAVEIS COM BL_PRIORITARIO = 1, UTILIZANDO O SUBASSUNTO
         * Ordem de prioridade de responsavel:
         * 1° Subassunto:
         *      Atendente de Setor.
         *      Atendente
         *      Responsável
         * 2° Assunto
         *      Atendente de Setor.
         *      Atendente
         *      Responsável
         */
        $ar_assuntos = array($id_assuntoco, $id_assuntocopai);

        // BEGIN TRANSACTION
        //$this->beginTransaction();
        try {
            /**
             * @var \G2\Entity\VwNucleoPessoaCo $responsaveis
             */
            $responsaveis = false;

            //Busca pelos prioritários.
            foreach ($ar_assuntos as $assunto) {
                if ($responsaveis)
                    break;

                $responsaveis = $this->findBy('\G2\Entity\VwNucleoPessoaCo', array(
                    'id_assuntocooriginal' => $assunto,
                    'bl_prioritario' => true,
                    'id_entidade' => $ocorrencia->getId_entidade()->getId_entidade()
                ), array(
                    'bl_todos' => 'ASC',
                ));

                if (is_array($responsaveis) && $responsaveis) {
                    usort($responsaveis, function ($a, $b) {
                        // ESTA FUNCAO MANTEM O ARRAY DE RESPONSAVEIS ORDENADO NA SEGUINTE ORDEM POR id_funcao:
                        // 10, 8, 9
                        $a_id = $a->getid_funcao();
                        $b_id = $b->getId_funcao();

                        if ($a_id == $b_id) {
                            return 0;
                        } else if ($a_id == \G2\Constante\Funcao::ATENDENTE && $b_id == \G2\Constante\Funcao::RESPONSAVEL) {
                            // SE A FOR 8 E B FOR 9, MANTER O 8 ANTES DO 9
                            return -1;
                        } else if ($a_id == \G2\Constante\Funcao::RESPONSAVEL && $b_id == \G2\Constante\Funcao::ATENDENTE) {
                            // SE A FOR 9 E B FOR 8, MANTER O 8 ANTES DO 9
                            return 1;
                        }

                        return ($a_id > $b_id) ? -1 : 1;
                    });
                }
            }

            if (is_array($responsaveis) && $responsaveis) {

                /**
                 * Procedure ValidaInsertResponsavelOcorrencia
                 * Nao permite que atribua varios responsaveis na mesma ocorrencia
                 */
                $responsavel = array_shift($responsaveis);

                $oc_responsavel = new \G2\Entity\OcorrenciaResponsavel();
                $oc_responsavel->setBl_ativo(true);
                $oc_responsavel->setDt_cadastro(date('Y-m-d H:i:s'));
                $oc_responsavel->setId_ocorrencia($ocorrencia->getId_ocorrencia());
                $oc_responsavel->setId_usuario($this->getReference('G2\Entity\Usuario', $responsavel->getId_usuario()));
                $this->save($oc_responsavel);

                if (!$oc_responsavel || !$oc_responsavel->getId_ocorrenciaresponsavel())
                    throw new \Exception('Ocorreu um erro ao vincular o responsável à ocorrência.');

                //$this->commit();
            } else {
                throw new \Exception('Não foi encontrado um usuário responsável por ocorrências deste núcleo e assunto.');
            }
        } catch (\Exception $e) {
            //$this->rollBack();
            return false;
        }
        return true;
    }

    /**
     * @param $ocorrencia
     * @param $responsavel
     * @return mixed
     * @throws \Exception
     */
    public function vinculaResponsavelDefinido($ocorrencia, $responsavel)
    {
        try {
            $oc_responsavel = new \G2\Entity\OcorrenciaResponsavel();
            $oc_responsavel->setBl_ativo(true);
            $oc_responsavel->setDt_cadastro(date('Y-m-d H:i:s'));
            $oc_responsavel->setId_ocorrencia($ocorrencia->getId_ocorrencia());
            $oc_responsavel->setId_usuario($this->getReference('G2\Entity\Usuario', $responsavel));
            $result = $this->save($oc_responsavel);
            if (!$oc_responsavel || !$oc_responsavel->getId_ocorrenciaresponsavel())
                throw new \Exception('Ocorreu um erro ao vincular o responsável à ocorrência.');

            return $result;
        } catch (\Exception $e) {
            throw new \Exception('Ocorreu um erro ao vincular o responsável à ocorrência.' . $e->getMessage());
        }
    }


    /**
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @param \G2\Entity\Tramite $tramite
     * @param null $arquivo
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvaTramite(\G2\Entity\Ocorrencia $ocorrencia, \G2\Entity\Tramite $tramite, $arquivo = null)
    {
        $this->beginTransaction();
        try {
            $tramite->setDt_cadastro(new \DateTime());
            $this->save($tramite);

            if (!$tramite instanceof \G2\Entity\Tramite || (!$tramite->getId_tramite())) {
                throw new \Exception('Ocorreu um erro ao salvar o trâmite');
            }
            if ($arquivo) {
                $mensageiro = $this->salvarArquivoTramite($tramite, $arquivo);
                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Zend_Exception($mensageiro->getFirstMensagem());
                }
            }

            $this->salvaTbTramiteOcorrencia($ocorrencia, $tramite);
            $this->commit();
            $this->enviaNotificacao($ocorrencia);

            //chama o método que cria o job no jira
            $ocorrenciaNegocio = new Ocorrencia();
            $ocorrenciaNegocio->criarJobInteracaoOcorrenciaJira(
                $ocorrencia->getId_ocorrencia(),
                $this->entityToTO($tramite)
            );


            return new \Ead1_Mensageiro($tramite, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $this->rollBack();
            return new \Ead1_Mensageiro('Erro ao cadastrar tramite -' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }


    /**
     * @param \G2\Entity\Tramite $tramite
     * @param $arquivo
     * @return \Ead1_Mensageiro
     */
    public function salvarArquivoTramite(\G2\Entity\Tramite $tramite, $arquivo)
    {
        try {
            if (!empty($arquivo['name'])) {

                $diretorio = 'upload' . DIRECTORY_SEPARATOR . 'tramite' . DIRECTORY_SEPARATOR;

                if (!is_dir($diretorio)) {
                    mkdir('upload' . DIRECTORY_SEPARATOR . 'tramite' . DIRECTORY_SEPARATOR, 0777);
                }

                $diretorio = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR
                    . '..' . DIRECTORY_SEPARATOR
                    . 'public' . DIRECTORY_SEPARATOR . $diretorio);

                $renomear = explode('.', $arquivo['name']);
                $arquivo['name'] = $tramite->getId_tramite() . '.' . $renomear[1];

                $adapter = new \Zend_File_Transfer_Adapter_Http();
                $adapter->addFilter('Rename', $arquivo['name']);
                $adapter->setDestination($diretorio);
                if (!$adapter->receive()) {
                    $messages = $adapter->getMessages();
                }

                $upload = new \Zend_File_Transfer();
                $upload->receive();

            };

            $upload = new Upload();
            $upload->setSt_upload($arquivo['name']);
            $this->save($upload);

            if ($upload instanceof Upload && ($upload->getId_upload())) {
                $tramite->setId_upload($upload);
                $this->merge($tramite);
            } else {
                throw new \Zend_Exception('Ocorreu um erro ao salvar o arquivo do trâmite.');
            }

            return new \Ead1_Mensageiro('Arquivo salvo com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro(
                'Erro no upload do arquivo relativo ao trâmite: ' . $e->getMessage(),
                \Ead1_IMensageiro::ERRO, $e
            );
        }


    }

    /**
     * Converte a entity em TO para enviar notificações para o usuario (se estiver configurado para isso)
     * @param \G2\Entity\Ocorrencia $ocorrencia
     */
    public function enviaNotificacao(\G2\Entity\Ocorrencia $ocorrencia)
    {
        $bo = new \PortalCAtencaoBO();
        $ocorrenciaTO = new \OcorrenciaTO();
        $ocorrenciaTO->setId_ocorrencia($ocorrencia->getId_ocorrencia());

        if ($ocorrencia->getId_assuntoco()) {
            $ocorrenciaTO->setId_assuntoco($ocorrencia->getId_assuntoco()->getId_assuntoco());
        }

        if ($ocorrencia->getId_usuariointeressado()) {
            $ocorrenciaTO->setId_usuariointeressado($ocorrencia->getId_usuariointeressado()->getId_usuario());
        }

        $bo->enviaNotificacaoOcorrencia($ocorrenciaTO);
    }


    /**
     * Cadastro novo tramite manual de uma ocorrencia
     * @param array $dados
     * @param null $arquivo
     * @return \Ead1_Mensageiro
     */
    public function novoTramite(array $dados, $arquivo = null)
    {
        try {
            $validaCampos = $this->validaCamposObrigatorios($dados, array(
                'id_ocorrencia', 'id_evolucao', 'id_situacao'
            ));
            if ($validaCampos) {
                throw new \Zend_Exception($validaCampos);
            }
            $ocorrencia = $this->find('G2\Entity\Ocorrencia', $dados['id_ocorrencia']);

            if ($ocorrencia->getId_evolucao()->getId_evolucao() != Evolucao::ENCERRAMENTO_REALIZADO_ATENDENTE) {
                $tramite = new \G2\Entity\Tramite();
                $tramite->setSt_tramite(($dados['st_tramite'] ? $dados['st_tramite'] : 'Novo trâmite adicionado'));
                $tramite->setId_tipotramite($this->find('G2\Entity\TipoTramite', TipoTramite::OCORRENCIA_MANUAL));
                $tramite->setId_entidade($this->find('G2\Entity\Entidade', $this->sessao->id_entidade));
                $tramite->setBl_visivel(true);
                $tramite->setId_usuario($this->find('G2\Entity\Usuario', $this->sessao->id_usuario));
                $tramite->setDt_cadastro(new \DateTime());


                /**
                 * Verificando se a ocorrencia tem um responsavel antes de alterar a evolucao.
                 * A regra e que a evolucao somente podera ser alterada para interessado interagiu
                 * caso a ocorrencia tenha um responsavel ja determinado.
                 */
                $ocorrenciaResponsavel = $this->findOneBy('\G2\Entity\OcorrenciaResponsavel',
                    array('id_ocorrencia' => $dados['id_ocorrencia'], 'bl_ativo' => true));

                $retorno = $this->salvaTramite($ocorrencia, $tramite, $arquivo);

                if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO && $ocorrenciaResponsavel) {
                    $ocorrencia->setId_evolucao($this->find('G2\Entity\Evolucao', Evolucao::INTERESSADO_INTERAGIU));
                    $this->save($ocorrencia);
                }
            } else {
                $retorno = new \Ead1_Mensageiro('Não é permitido interagir em uma ocorrência fechada.', \Ead1_IMensageiro::ERRO);
            }

            return $retorno;

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Ocorreu um erro ao salvar o trâmite: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Busca uma ocorrencia e suas interações
     * @param $id_ocorrencia
     * @return \Ead1_Mensageiro
     */
    public function detalhaOcorrencia($id_ocorrencia)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            $vwocorrencia = $this->find('G2\Entity\VwNucleoOcorrencia', $id_ocorrencia);
            if ($vwocorrencia instanceof VwNucleoOcorrencia && ($vwocorrencia->getId_usuariointeressado() == $this->sessao->id_usuario)) {
                $interacoes = $this->retornaInteracoesOcorrencia($id_ocorrencia);
                $mensageiro->setMensageiro(array('ocorrencia' => $vwocorrencia, 'interacoes' => $interacoes), \Ead1_IMensageiro::SUCESSO);
            } else {
                /*
                 * Caso a ocorrencia com numero recebido não exista esta
                 * mensagem e criada para ser mostrada ao usuario
                 */
                $mensageiro->setMensageiro('Ocorrência de numero ' . $id_ocorrencia . ' não encontrada!', \Ead1_IMensageiro::AVISO);
            }

        } catch (\Exception $e) {
            $mensageiro->setMensageiro('Ocorreu um erro ao buscar a ocorrencia: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * Retorna as interações de uma ocorrencia
     * @param $id_ocorrencia
     * @return \Ead1_Mensageiro
     */
    public function retornaInteracoesOcorrencia($id_ocorrencia)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            if (!$id_ocorrencia) {
                Throw new \Zend_Exception('O id_ocorrencia é obrigatório e não foi passado!');
            }
            $retorno = $this->findBy('\G2\Entity\VwOcorrenciaInteracao',
                array('id_ocorrencia' => $id_ocorrencia),
                array('dt_cadastro' => 'DESC'));
            if (empty($retorno)) {
                $mensageiro->setMensageiro('Nenhuma interação encontrada', \Ead1_IMensageiro::AVISO);
            } else {
                foreach ($retorno as $en) {
                    $en->setDt_cadastro(new \Zend_Date($en->getDt_cadastro()));
                }
                $mensageiro->setMensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Zend_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * Método que encaminha uma ocorrência - Grava um trâmite para essa alteração
     * Corrige erro de salvar trâmite com data errada
     * @param VwOcorrenciasPessoa $vwOcorrenciaPessoa
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function encaminharOcorrencia(VwOcorrenciasPessoa $vwOcorrenciaPessoa)
    {
        $mensageiro = new \Ead1_Mensageiro();
        $dao = new \MatriculaDAO();
        $dao->beginTransaction();
        $this->beginTransaction();
        try {
            $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
            /** @var \G2\Entity\Usuario $pessoa */
            $pessoa = $this->find(\G2\Entity\Usuario::class, $vwOcorrenciaPessoa->getId_atendente());

            $ocorrenciaResp = $this->findOneBy(\G2\Entity\OcorrenciaResponsavel::class, array(
                'id_ocorrencia' => $vwOcorrenciaPessoa->getId_ocorrencia(),
                'bl_ativo' => true
            ));

            $responsavelAntigo = null;
            if (($ocorrenciaResp instanceof OcorrenciaResponsavel)
                && $ocorrenciaResp->getId_ocorrenciaresponsavel()
            ) {
                $responsavelAntigo = $ocorrenciaResp->getId_usuario()->getSt_nomecompleto();
                //Seta o atual atendente como inativo
                $negocioOcorrencia->deletarOcorrenciaResponsavel(array(
                    'id_ocorrencia' => $vwOcorrenciaPessoa->getId_ocorrencia()
                ));
            }

            //Vincula o novo responsável a ocorrencia
            $ocorrencia = new \G2\Entity\Ocorrencia();
            $ocorrencia->setId_ocorrencia($vwOcorrenciaPessoa->getId_ocorrencia());
            $this->vinculaResponsavelDefinido($ocorrencia, $pessoa->getId_usuario());

            //Salva trâmite para o encaminhar
            $assuntoco = $this->find(\G2\Entity\AssuntoCo::class, $vwOcorrenciaPessoa->getId_assuntoco());

            //atualiza ocorrencia para novo assunto
            $ocorrencia = $this->find(\G2\Entity\Ocorrencia::class, $vwOcorrenciaPessoa->getId_ocorrencia());
            if (!$ocorrencia instanceof \G2\Entity\Ocorrencia) {
                throw new \Exception('Ocorrência não encontrada.');
            }
            $ocorrencia
                ->setId_evolucao($this->getReference(\G2\Entity\Evolucao::class, Evolucao::AGUARDANDDO_ATENDENTE))
                ->setId_situacao($this->getReference(\G2\Entity\Situacao::class, Situacao::TB_OCORRENCIA_EM_ANDAMENTO))
                ->setId_assuntoco($assuntoco);

            $resultMerge = $this->save($ocorrencia);


            //Monta os dados do  tramite e salva
            $responsavelNovo = $pessoa->getSt_nomecompleto();
            $this->salvarTramiteEncaminhar(
                $ocorrencia,
                $vwOcorrenciaPessoa,
                $assuntoco,
                $responsavelAntigo,
                $responsavelNovo
            );

            //Cria o tramite especifico para ocorrencia com cancelamento vinculado
            if ($ocorrencia->getId_assuntoco()) {
                $this->criarTramiteFinaceiroCancelamento(
                    $ocorrencia,
                    $ocorrencia->getId_assuntoco()->getId_assuntoco(),
                    $responsavelAntigo
                );
            }

            //TRACER AC-27605
            if ((int)$ocorrencia->getId_entidade()->getId_entidade() == 352) {
                if (extension_loaded('newrelic')) {
                    $trace = \Zend_Json::encode(array(
                        'POST' => $this->toArrayEntity($vwOcorrenciaPessoa),
                        'PERSIST' => $this->toArrayEntity($resultMerge)
                    ));
                    newrelic_capture_params(true);
                    newrelic_notice_error($trace);
                }
            }

            $this->commit();
            $dao->db()->commit();
            $mensageiro->setMensageiro('Ocorrência encaminhada com sucesso', \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $this->rollBack();
            $dao->rollBack();
            $mensageiro->setMensageiro('Ocorreu um erro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

        return $mensageiro;

    }


    /**
     * Verifica se o assunto da ocorrencia é da categoria financeiro, verifica se a ocorrencia tem um tramite
     * vinculado, gera o texto e salva no tramite
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @param $idAssunto
     * @param $stAtendente
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    private function criarTramiteFinaceiroCancelamento(\G2\Entity\Ocorrencia $ocorrencia, $idAssunto, $stAtendente)
    {
        try {

            /** @var \G2\Entity\AssuntoCo $assunto */
            $assunto = $this->findOneBy(\G2\Entity\AssuntoCo::class, [
                'id_assuntoco' => $idAssunto,
                'id_assuntocategoria' => \G2\Constante\NucleoFinalidade::FINANCEIRO
            ]);

            /** @var \G2\Entity\Cancelamento $cancelamento */
            $cancelamento = $this->findOneBy(\G2\Entity\Cancelamento::class, [
                "id_ocorrencia" => $ocorrencia->getId_ocorrencia()
            ]);

            if ($cancelamento) {
                /* verifica se o assunto da ocorrencia (antes de encaminhar) é da categoria Financeiro
                e se a ocorrencia tem algum processo de cancelamento vinculado */
                if ($assunto && $assunto->getId_assuntocategoria() && $cancelamento->getId_ocorrencia()) {
                    $dtHoje = date("d/m/Y");
                    $strTramite = "Processo encaminhado para o setor de cancelamento em {$dtHoje}, ";
                    $strTramite .= "Pedido de cancelamento {$cancelamento->getId_cancelamento()}, ";
                    $strTramite .= "Atendente {$stAtendente}.";

                    $tramite = new \G2\Entity\Tramite();
                    $tramite->setBl_visivel(true)
                        ->setId_entidade($this->getReference(\G2\Entity\Entidade::class, $this->sessao->id_entidade))
                        ->setId_tipotramite($this
                            ->getReference(\G2\Entity\TipoTramite::class, TipoTramite::OCORRENCIA_AUTOMATICO))
                        ->setId_usuario($this->getReference(\G2\Entity\Usuario::class, $this->sessao->id_usuario))
                        ->setSt_tramite($strTramite)
                        ->setDt_cadastro(new \DateTime());

                    $retornoTramite = $this->salvaTramite($cancelamento->getId_ocorrencia(), $tramite);

                    //tenta alterar a evolução do cancelamento e da matricula
                    if ($retornoTramite->getType() === \Ead1_IMensageiro::TYPE_SUCESSO) {
                        $cancelamentoNegocio = new Cancelamento();
                        $cancelamentoNegocio->alterarEvolucaoCancelamento(
                            $cancelamento->getId_cancelamento(),
                            Evolucao::TB_MOTIVOCANCELANENTO_PROCESSO_CANCELAMENTO,
                            $ocorrencia->getId_matricula()->getId_matricula(),
                            $strTramite
                        );

                        //Atualiza a data do encaminhamento na tb_cancelamento
                        $cancelamentoPosNegocio = new CancelamentoPosGraduacao();
                        $cancelamentoPosNegocio->atualizaDataEncaminharFinanceiro([
                            "id_cancelamento" => $cancelamento->getId_cancelamento(),
                            "dt_encaminhamentofinanceiro" => new \DateTime()
                        ]);
                    }

                    return $retornoTramite;
                }
            }



        } catch (\Exception $e) {
            throw new \Exception("Erro ao criar tramite vinculado a ocorrencia com processo de Cancelamento. "
                . $e->getMessage());
        }
    }


    /**
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @param int $tempo_limite_minutos
     * @return bool
     */
    public function possuiOcorrenciaCadastradaRecentemente(\G2\Entity\Ocorrencia $ocorrencia,
                                                           $tempo_limite_minutos = 10)
    {
        if (!$ocorrencia->getId_usuariointeressado()
            || !$ocorrencia->getId_assuntoco()
            || !$ocorrencia->getId_matricula()
        ) {
            return false;
        }
        $dt_cadastro = date('Y-m-d H:i:s', strtotime("-{$tempo_limite_minutos} minutes"));

        $repo = $this->em->getRepository('G2\Entity\Ocorrencia');
        $query = $repo->createQueryBuilder('tb');

        $query->where("tb.id_usuariointeressado = {$ocorrencia->getId_usuariointeressado()->getId_usuario()}");
        $query->andWhere("tb.id_assuntoco = {$ocorrencia->getId_assuntoco()->getId_assuntoco()}");
        $query->andWhere("tb.id_matricula = {$ocorrencia->getId_matricula()->getId_matricula()}");
        $query->andWhere("tb.dt_cadastro >= '{$dt_cadastro}'");

        $results = $query->getQuery()->getResult();

        return $results ? true : false;
    }

    /*Verifica se existem ocorrências em aberto com mesmo assunto e subassunto
     *
     */
    public function possuiOcorrenciaComMesmoAssuntoSubassunto($dados)
    {
        if (!$dados['id_usuariointeressado'] || !$dados['id_assuntoco'] || !$dados['id_matricula']) {
            return false;
        }

        $negocio = new \G2\Negocio\CAtencao();
        $bo = new \PortalCAtencaoBO();
        $ocorrenciaTO = new \VwOcorrenciaTO();


        //$ocorrenciaTO->setId_tipoocorrencia(\AssuntoCoTO::TIPO_OCORRENCIA_TUTOR);
        $ocorrenciaTO->setId_entidade($negocio->sessao->id_entidade);
        $ocorrenciaTO->setId_assuntoco($dados['id_assuntoco']);

        //Portal
        if (isset($dados['id_assuntopai'])) {
            $ocorrenciaTO->setId_assuntocopai($dados['id_assuntopai']);
        }
        //G2
        if (isset($dados['id_assuntocopai'])) {
            $ocorrenciaTO->setId_assuntocopai($dados['id_assuntocopai']);
        }
        $ocorrenciaTO->setId_matricula($dados['id_matricula']);
        $ocorrenciaTO->setId_usuariointeressado($dados['id_usuariointeressado']);

        $resposta = $bo->retornarOcorrencias($ocorrenciaTO);
        if (is_object($resposta->getMensagem()[0])) {
            foreach ($resposta->getMensagem() as $ocorrencias) {
                if ($ocorrencias->getId_situacao() !== 99) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param \G2\Entity\Ocorrencia $ocorrencia
     * @param VwOcorrenciasPessoa $vwOcorrenciaPessoa
     * @param \G2\Entity\AssuntoCo|null $assuntoco
     * @param null $atendenteAtual
     * @param null $atendenteNovo
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    private function salvarTramiteEncaminhar(\G2\Entity\Ocorrencia $ocorrencia,
                                             \G2\Entity\VwOcorrenciasPessoa $vwOcorrenciaPessoa,
                                             \G2\Entity\AssuntoCo $assuntoco = null,
                                             $atendenteAtual = null,
                                             $atendenteNovo = null)
    {
        try {

            // LEIA REGRA: se a evolucao ou a situacao da ocorrencia for alterada sera gerado um tramite.
            $msgNovoTramite = NULL;
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoAtual = $arraySituacao[$vwOcorrenciaPessoa->getId_situacao()];
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_EM_ANDAMENTO];
            $stEvolucaoAtual = $arrayEvolucao[$vwOcorrenciaPessoa->getId_evolucao()];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::AGUARDANDDO_ATENDENTE];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $stEvolucaoAtual, $stEvolucaoNova);

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $stSituacaoAtual, $stSituacaoNova);

            //Criando texto para tramite
            $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
            //Fim Regra

            //Salva trâmite para o encaminhar
            $msgAssunto = null;

            if ($assuntoco) {
                $stAssuntoPaiAtual = $vwOcorrenciaPessoa->getSt_assuntocopai();
                $stAssuntoAtual = $vwOcorrenciaPessoa->getSt_assuntoco();
                $stAssuntoPaiNovo = $assuntoco->getId_assuntocopai()->getSt_assuntoco();
                $stAssuntoNovo = $assuntoco->getSt_assuntoco();
                $stAtentendeAtual = ($atendenteAtual ? $atendenteAtual : ' Não distribuído');
                $stAtendenteNovo = $atendenteNovo;

                $stTramiteEncaminhar = sprintf(
                    \G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_ASSUNTO_ENCAMINHAR,
                    $stAssuntoPaiAtual,
                    $stAssuntoAtual,
                    $stAssuntoPaiNovo,
                    $stAssuntoNovo,
                    $stAtentendeAtual,
                    $stAtendenteNovo
                );
            }

            //Criando texto para tramite
            $texto = 'Ocorrência N. '
                . $vwOcorrenciaPessoa->getId_ocorrencia()
                . $stTramiteEncaminhar . '. '
                . $stTramite;


            $tramite = new \G2\Entity\Tramite();
            $tramite->setBl_visivel(true)
                ->setId_entidade($this->getReference('G2\Entity\Entidade', $this->sessao->id_entidade))
                ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', TipoTramite::OCORRENCIA_AUTOMATICO))
                ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario))
                ->setSt_tramite($texto)
                ->setDt_cadastro(new \DateTime());

            return $this->salvaTramite($ocorrencia, $tramite);

        } catch (\Exception $e) {
            throw new \Exception("Erro ao tentar salvar Tramite do encaminhamento de ocorrencia. " . $e->getMessage());
        }
    }


}

<?php

namespace G2\Negocio;

/**
 * Classe de neg�cio para NucleoPessoaCo
 * @author Marcus Pereira <marcus.pereira@gmail.com>
 */

Class NucleoPessoaCo extends Negocio {

    private $repositoryName = '\G2\Entity\NucleoPessoaCo';

    public function save($data)
    {

        $id_attribute = 'id_nucleopessoaco';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se n�o existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }


        /**********************
         * Preparar atributos FK
         **********************/
        if (isset($data['id_nucleoco']) && $data['id_nucleoco'])
            $data['id_nucleoco'] = $this->find('\G2\Entity\NucleoCo', $data['id_nucleoco']);

        if (isset($data['id_assuntoco']) && $data['id_assuntoco'])
            $data['id_assuntoco'] = $this->find('\G2\Entity\AssuntoCo', $data['id_assuntoco']);

        if (isset($data['id_usuario']) && $data['id_usuario'])
            $data['id_usuario'] = $this->find('\G2\Entity\Usuario', $data['id_usuario']);

        if (isset($data['id_funcao']) && $data['id_funcao'])
            $data['id_funcao'] = $this->find('\G2\Entity\Funcao', $data['id_funcao']);

        if (isset($data['id_usuariocadastro']) && $data['id_usuariocadastro'])
            $data['id_usuariocadastro'] = $this->find('\G2\Entity\Usuario', $data['id_usuariocadastro']);
        else
            $data['id_usuariocadastro'] = $this->find('\G2\Entity\Usuario', $this->sessao->id_usuario);


        /**********************
         * Preparar atributos DEFAULT
         **********************/
        if (!isset($data['dt_cadastro']) || !$data['dt_cadastro'])
            $data['dt_cadastro'] = date('Y-m-d h:i:s');

        if (!isset($data['bl_prioritario']) || !$data['bl_prioritario'])
            $data['bl_prioritario'] = 0;

        if (!isset($data['bl_todos']) || !$data['bl_todos'])
            $data['bl_todos'] = 0;


        /**********************
         * Definindo atributos
         **********************/
        $this->arrayToEntity($entity, $data);


        /**********************
         * Acao de salvar
         **********************/
        $mensageiro = new \Ead1_Mensageiro();

        try {
            if (isset($data[$id_attribute]) && $data[$id_attribute])
                $entity = $this->merge($entity);
            else
                $entity = $this->persist($entity);

            $getId = 'get' . ucfirst($id_attribute);
            $data[$id_attribute] = $entity->$getId();

            return $mensageiro->setMensageiro($this->model($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);
        } catch(\Exception $e) {
            return $mensageiro->setMensageiro(array('Houve um erro ao salvar o registro.'), \Ead1_IMensageiro::ERRO, $data[$id_attribute]);
        }

    }


    public function model($entity) {
        return array(
            'id_nucleopessoaco' => $entity->getId_nucleopessoaco(),
            'dt_cadastro' => $entity->getDt_cadastro(),
            'id_nucleoco' => $entity->getid_nucleoco() ? $entity->getid_nucleoco()->getid_nucleoco() : null,
            'id_assuntoco' => $entity->getId_assuntoco() ? $entity->getId_assuntoco()->getId_assuntoco() : null,
            'id_usuario' => $entity->getId_usuario() ? $entity->getId_usuario()->getId_usuario() : null,
            'id_funcao' => $entity->getId_funcao() ? $entity->getId_funcao()->getId_funcao() : null,
            'id_usuariocadastro' => $entity->getId_usuariocadastro() ? $entity->getId_usuariocadastro()->getId_usuario() : null,
            'bl_prioritario' => $entity->getBl_prioritario(),
            'bl_todos' => $entity->getBl_todos(),

            //'st_assunto' => $entity->getId_assuntoco() ? $entity->getId_assuntoco()->getSt_assuntoco() : '',
            'st_nucleoco' => $entity->getId_nucleoco() ? $entity->getId_nucleoco()->getSt_nucleoco() : '',
            'st_assuntoco' => $entity->getId_assuntoco() ? $entity->getId_assuntoco()->getSt_assuntoco() : '',
            'st_nomecompleto' => $entity->getId_usuario() ? $entity->getId_usuario()->getSt_nomecompleto() : '',
            'st_funcao' => $entity->getId_funcao() ? $entity->getId_funcao()->getSt_funcao() : '',
        );
    }

    /* Busca todos os registros na tabela baseado no assunto, INCLUINDO entradas com bl_todos = 1 */

    public function getPessoaByAssunto($where = array()) {

        $repository = $this->getRepository($this->repositoryName);
        $query = $repository->createQueryBuilder('tb')
            ->select(array('tb', 'u'))
            ->leftJoin('tb.id_usuario', 'u')
            ->where('1 = 1');

        if(isset($where['id_assuntoco']) && $where['id_assuntoco']){
            $query->andWhere("(tb.id_assuntoco = {$where['id_assuntoco']} OR tb.bl_todos = 1)");
            unset($where['id_assuntoco']);
        }

        foreach ($where as $attr => $value) {
            $query->andWhere("tb.{$attr} = '{$value}'");
        }
        $query->orderBy('u.st_nomecompleto');

        return $query->getQuery()->getResult();


    }
}
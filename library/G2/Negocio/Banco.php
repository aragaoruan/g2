<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Contas e relacionados
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-07-30
 */
class Banco extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados dos tipos de conta
     * @return array $return
     */
    public function retornaBancos()
    {
        try {
            $return = $this->findAll('G2\Entity\Banco');
            $resultReal = array();
            foreach ($return as $r) {
                $resultReal[] = array(
                    'id_banco' => $r->getId_banco(),
                    'st_banco' => $r->getSt_banco()
                );
            }
            return $resultReal;
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    function findByBanco($params = array())
    {
        try {
            if (empty($params))
                $params = array('bl_ativo' => 1);
            $repositoryName = '\G2\Entity\Banco';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    function findOneByBanco($params)
    {
        try {
            $repositoryName = '\G2\Entity\Banco';
            return $this->findOneBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe negócio para Catraca
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2014-12-04
 */

class Catraca extends Negocio
{

    private $repositoryName = 'G2\Entity\Catraca';


    public function __construct()
    {
        parent::__construct();
    }

    public function salvaCatraca($parametros)
    {
        $objeto = new \G2\Entity\Catraca();

        if($parametros['id_catraca'])
            $objeto = $this->find('\G2\Entity\Catraca', $parametros['id_catraca']);

        $objeto->setSt_codigocatraca($parametros['st_codigocatraca']);
        $objeto->setBl_ativo(true);
        $objeto->setId_entidade($parametros['id_entidade']);
        $objeto->setId_usuariocadastro($this->sessao->id_usuario);
        $objeto->setDt_cadastro(new \DateTime());
        $objeto->setId_Situacao($parametros['id_situacao']);

        $result = $this->save($objeto);
        return $result;

    }

    public function retornarDadosCatraca(array $param = array())
    {
        $param['id_entidade'] = isset($param['id_entidade']) ? $param['id_entidade'] : $this->sessao->id_entidade;
        return $objeto = $this->findBy('\G2\Entity\Catraca', $param);

    }

    public function deleteCatraca($id)
    {
        $objeto = $this->find('\G2\Entity\Catraca', $id);

        return $this->delete($objeto);
    }

}
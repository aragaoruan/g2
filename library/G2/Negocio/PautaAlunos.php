<?php

namespace G2\Negocio;

use G2\Negocio\Negocio;
use Doctrine\Common\Util\Debug;
use G2\Entity\AvaliacaoAgendamento;
use G2\Entity\VwSalaDisciplinaEntidade;

/**
 * Classe de negócio para Relatório Pauta Alunos
 * @author Denise Xavier <denise.xavier@unyleya.com.br>;
 */
class PautaAlunos extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna combo de periodo letivo para relatorio Pauta de Alunos
     * @see PautaAlunosController::findPeriodoLetivoAction
     * @return array
     */
    public function findComboPeriodoLetivo()
    {
        $dql = "SELECT DISTINCT pl ";
        $dql .= "FROM G2\Entity\PeriodoLetivo pl ";
        $dql .= "JOIN G2\Entity\SalaDeAula AS sa WITH sa.id_periodoletivo = pl.id_periodoletivo ";
        $dql .= "JOIN G2\Entity\SalaDeAulaEntidade AS se WITH se.id_saladeaula = sa.id_saladeaula ";
        $dql .= "WHERE se.id_entidade = {$this->sessao->id_entidade} ";
        $dql .= "ORDER BY pl.st_periodoletivo ASC ";

        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }


    /**
     * retorna Disciplinas por período letivo
     * @see PautaAlunosController::findDisciplinaByPeriodoLetivoAction
     * @param $id_periodoletivo
     * @return array
     */
    public function findDisciplinaByPeriodoLetivo($id_periodoletivo)
    {
        if ($id_periodoletivo) {
            $dql = "SELECT DISTINCT vw.st_disciplina , vw.id_disciplina ";
            $dql .= "FROM \G2\Entity\VwSalaDisciplinaEntidade vw ";
            $dql .= "WHERE vw.id_periodoletivo =  {$id_periodoletivo} AND vw.id_entidade = {$this->sessao->id_entidade}";
            $dql .= " ORDER BY vw.st_disciplina ASC ";

            $query = $this->em->createQuery($dql);
            return $query->getResult();
        } else
            return array();
    }

    /**
     * Retorna projetos pedagogicos vinculados a uma disciplina
     * @param $id_disciplina
     * @see PautaAlunosController::findCursoByDisciplinaAction
     * @return array
     */
    public function findCursoByDisciplina($id_disciplina)
    {
        if ($id_disciplina) {
            $dql = "SELECT DISTINCT pp.id_projetopedagogico, pp.st_projetopedagogico ";
            $dql .= "FROM \G2\Entity\ProjetoPedagogico pp ";
            $dql .= "JOIN G2\Entity\AreaProjetoSala AS aps WITH aps.id_projetopedagogico = pp.id_projetopedagogico ";
            $dql .= "JOIN G2\Entity\SalaDeAula AS sa WITH sa.id_saladeaula = aps.id_saladeaula ";
            $dql .= "JOIN G2\Entity\DisciplinaSalaDeAula AS ds WITH ds.id_saladeaula = sa.id_saladeaula ";
            $dql .= "JOIN G2\Entity\ProjetoEntidade AS pe WITH pe.id_projetopedagogico = pp.id_projetopedagogico ";
            $dql .= "WHERE pe.id_entidade = {$this->sessao->id_entidade}  ";
            $dql .= " AND ds.id_disciplina =  {$id_disciplina} ";
            $dql .= " ORDER BY pp.st_projetopedagogico ASC ";

            $query = $this->em->createQuery($dql);
            return $query->getResult();
        } else
            return array();
    }


    /**
     * Pesquisa para relatorio Pauta de alunos
     * @param $params
     * @return array|bool
     */
    public function pesquisaAlunosPauta($params)
    {
        try {
            $dql = "SELECT vw ";
            $dql .= "FROM \G2\Entity\VwPautaAlunos vw ";
            $dql .= "WHERE vw.id_entidade = {$this->sessao->id_entidade}  ";
            if (array_key_exists('id_disciplina', $params) && $params['id_disciplina']) {
                $dql .= " AND vw.id_disciplina =  {$params['id_disciplina']} ";
            }
            if (array_key_exists('id_projetopedagogico', $params) && $params['id_projetopedagogico']) {
                $dql .= " AND vw.id_projetopedagogico =  {$params['id_projetopedagogico']} ";
            }
            $dql .= " ORDER BY vw.id_projetopedagogico, vw.st_aluno ASC ";
            $query = $this->em->createQuery($dql);
            return $this->organizaDadosRetornaPauta($query->getResult());
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Organiza os dados em um array para retorno de acordo com layout da tela
     * @param array $dados
     * @return array|bool
     */
    public function organizaDadosRetornaPauta(array $dados)
    {
        if (!empty($dados)) {
            $arrayReturn = array();
            $cont = 0;
            $arrayReturn[$cont]['id_projetopedagogico'] = $dados[0]->getId_projetopedagogico();
            $arrayReturn[$cont]['st_projetopedagogico'] = $dados[0]->getSt_projetopedagogico();
            $arrayReturn[$cont]['id_disciplina'] = $dados[0]->getId_disciplina();
            $arrayReturn[$cont]['st_disciplina'] = $dados[0]->getSt_disciplina();
            $arrayReturn[$cont]['st_professor'] = $dados[0]->getSt_professor();

            $id_projetoAux = $dados[0]->getId_projetopedagogico();

            foreach ($dados as $key => $dado) {
                if ($dado->getId_projetopedagogico() != $id_projetoAux) {
                    $id_projetoAux = $dado->getId_projetopedagogico();
                    $cont++;
                    $arrayReturn[$cont]['id_projetopedagogico'] = $dado->getId_projetopedagogico();
                    $arrayReturn[$cont]['st_projetopedagogico'] = $dado->getSt_projetopedagogico();
                    $arrayReturn[$cont]['id_disciplina'] = $dado->getId_disciplina();
                    $arrayReturn[$cont]['st_disciplina'] = $dado->getSt_disciplina();
                    $arrayReturn[$cont]['st_professor'] = $dado->getSt_professor();
                }
                $arrayReturn[$cont]['alunos'][] = array('st_aluno' => $dado->getSt_aluno()
                , 'id_usurio' => $dado->getId_usuario());
            }

            return $arrayReturn;
        } else
            return false;

    }

    /**
     * Valida campos obrigatórios para gerar PDF
     * @param $dados
     * @return bool|string
     */
    public function validaDadosPdf($dados)
    {
        $keys = array('id_disciplina', 'data');
        return $this->validaCamposObrigatorios($dados, $keys);
    }

}
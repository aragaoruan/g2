<?php

namespace G2\Negocio;

/**
* Classe de negócio para Trancamento de disciplinas - graduação
* @update Denise Xavier <denise.xavier@unyleya.com.br>
*/
class TrancamentoDisciplina extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Verifica a marcação das disciplinas na grade de trancamento de acordo com a regra:
     ** As disciplinas com mais de 5 dias que se iniciaram ou que esteja com a data de encerramento menor que a data atual deve ficar na cor laranja **
     * @param $gradeNota
     * @return bool
     */
    public function marcaDisciplinasTrancamento($gradeNota){
        $retorno = false;
        try{
            if(!$gradeNota instanceof \G2\Entity\VwGradeNota)
                throw  new \Exception('Deve ser uma instancia da VwGradeNota para o calculo');

            $dataAtual = new \Zend_Date();
            $dataInicio = $this->converterData($gradeNota->getDt_inicio(),  'zend_date' );
            $dataEncerramneto = is_object($gradeNota->getDt_encerramento()) ? $this->converterData($gradeNota->getDt_encerramento(), 'zend_date') : null;

            //Se a data de inicio de disciplina mais 5 dias for menor que a data atual
            if( $dataInicio->addDay(5)->isEarlier($dataAtual) ||
                //se a data de encerramento for diferente de null e
                    (!is_null($dataEncerramneto) && $dataEncerramneto->isEarlier($dataAtual) )
                )
                {
                    $retorno = true;
                }
        }catch (\Exception $e){
            $retorno = false;
        }
        return $retorno;
    }


    /**
     * Metodo que retorna dados da vw_gradenota usando findBy basico padrao doctrine
     * @param array $whereLike
     * @param array $where
     * @param array $order
     * @param array $limit
     * @param array $whereNull
     * @return array
     */
    public function findVwGradeNotaSearch(array $whereLike, array $where, array $order = null, $limit = null, $offset = null, array $whereNull = array())
    {
        $result = $this->findBySearch('\G2\Entity\VwGradeNota', $whereLike, $where, $order, $limit, $offset, $whereNull);
        return $result;
    }

    /**
     * Tranca as disciplinas (Graduação)
     * 1 - Desaloca o aluno marcando o bl_trancamento
     * 2 - Verifica se existe agendamento ativo para o aluno na disciplina, em caso afirmativo deleta
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function trancarDisciplinas($data){
        $mensageiroRetorno = new \Ead1_Mensageiro();
        $desalocadosSucesso = 0;
        try{
            if(!array_key_exists('id_matricula', $data) || empty($data['id_matricula']))
                throw new \Exception('O id_matricula é obrigatório e não foi passado.');

            if(!array_key_exists('id_alocacao', $data) || empty($data['id_alocacao']))
                throw new \Exception('Nehuma disciplina marcada para trancamento.');

            $array_alocacoes = $data['id_alocacao'];
            $alocacaoNegocio = new \G2\Negocio\Alocacao();
            foreach ($array_alocacoes as $key => $id_alocacao){
                $msg  = $alocacaoNegocio->desalocarAluno(array('id_alocacao' => $id_alocacao), true);
                if($msg->getTipo() == \Ead1_IMensageiro::SUCESSO){
                    $desalocadosSucesso ++;
                    $this->salvarTramiteTrancamentoDisciplina(array('id_matricula' => $data['id_matricula'] , 'id_alocacao' => $id_alocacao));
                }
                else
                    $mensageiroRetorno->setTipo(\Ead1_IMensageiro::AVISO);
            }

            //verifica se o aluno tem avaliacao agendada
            $this->verificaAgendamentoTrancamento($data);

            $mensageiroRetorno->setMensageiro('Disciplinas trancadas com sucesso.', $mensageiroRetorno->getTipo(), $desalocadosSucesso);

        }catch (\Exception $e){
            $mensageiroRetorno->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
        return $mensageiroRetorno;
    }

    /**
     * Verifica se as disciplinas que foram trancadas tem agendamento ativo
     * Em caso afirmativo, deleta os mesmsmos
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function verificaAgendamentoTrancamento($data){
        $gerenciaProva = new \G2\Negocio\GerenciaProva();
        try{
            if(!array_key_exists('id_matricula', $data) || empty($data['id_matricula']))
                throw new \Exception('O id_matricula é obrigatório e não foi passado.');

            if(!array_key_exists('id_disciplina', $data) || empty($data['id_disciplina']))
                throw new \Exception('Nehuma disciplina marcada para trancamento.');

            $array_disciplinas = $data['id_disciplina'];
            foreach ($array_disciplinas as $key => $id_disciplina){
                $vwAvaliacaoAgendamento = $this->findBy('\G2\Entity\VwAvaliacaoAgendamento'
                                                            , array('id_matricula' => $data['id_matricula']
                                                            , 'id_disciplina'      => (int)$id_disciplina
                                                            , 'id_situacao'        => \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO
                                                            , 'bl_ativo'           => 1 )
                                                        );
                if($vwAvaliacaoAgendamento instanceof \G2\Entity\VwAvaliacaoAgendamento && $vwAvaliacaoAgendamento->getId_avaliacaoagendamento()){
                    //cancela a avaliacao agendamento para a matricula trancada
                    $avaliacaoUpdate = array('id_matricula'            => $data['id_matricula'],
                                             'id_tipodeavaliacao'      => $vwAvaliacaoAgendamento->getId_tipodeavaliacao(),
                                             'id_avaliacaoagendamento' => $vwAvaliacaoAgendamento->getId_avaliacaoagendamento(),
                                             'st_justificativa'        => 'Agendamento cancelado após trancamento de disciplina.' );

                    $gerenciaProva->cancelarAgendamento($avaliacaoUpdate);


                }
            }

            return new \Ead1_Mensageiro('Agendamentos verificados e/ou deletados com sucesso');

        }catch (\Exception $e){
            return new \Ead1_Mensageiro('Erro: ' .$e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva TRAMITE do trancamento da disciplina
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function salvarTramiteTrancamentoDisciplina($params){
        try{
            if(!array_key_exists('id_matricula', $params) || empty($params['id_matricula'])){
                throw new \Exception('O id_matricula é obrigatório e não foi passado.');
            }

            $matriculaTO = new \MatriculaTO();
            $matriculaTO->setId_matricula($params['id_matricula']);

            if(!array_key_exists('id_alocacao', $params) && !array_key_exists('id_disciplina', $params)){
                throw new \Exception('O id_alocacao ou o id_disciplina é obrigatório e não foi passado.');
            }

            $disciplina = null;
            //Se for passado o id_alocacao
            if(array_key_exists('id_alocacao', $params) && !empty($params['id_alocacao'])){
                $en_ = $this->find('\G2\Entity\Alocacao', (int)$params['id_alocacao']);
                if($en_ instanceof \G2\Entity\Alocacao && $en_->getId_matriculadisciplina()->getDisciplina() instanceof \G2\Entity\Disciplina)
                    $disciplina = $en_->getId_matriculadisciplina()->getDisciplina()->getSt_disciplina();
            }

            //se for passado o id_disciplina
            if(array_key_exists('id_disciplina', $params) && !empty($params['id_disciplina'])){
                $en_ = $this->find('\G2\Entity\Disciplina', (int)$params['id_disciplina']);
                if($en_ instanceof \G2\Entity\Disciplina && $en_->getId_disciplina())
                    $disciplina = $en_->getSt_disciplina();
            }

            $tramiteBO = new \TramiteBO();
            $tramiteTO = new \TramiteTO();
            $tramiteTO->setId_tipotramite(\G2\Constante\TipoTramite::MATRICULA_TRANCAMENTO_DISCIPLINA);
            $tramiteTO->setBl_visivel(1);
            $tramiteTO->setId_entidade($this->sessao->id_entidade);
            $tramiteTO->setId_usuario($this->sessao->id_usuario);
            $tramiteTO->setSt_tramite('A disciplina [' . $disciplina . '] foi trancada.');

            $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteTO);

            return new \Ead1_Mensageiro('Trâmite da transferencia cadastrada com sucesso');

        }catch (\Exception $e){
            return new \Ead1_Mensageiro('Erro: '. $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Verifica se o aluno está em processo de renovação
     * Em caso afirmativo, ele não pode trancar disciplinas
     * @param $id_matricula
     * @return \Ead1_Mensageiro
     */
    public function verificaProcessoRenovacao($id_matricula){
        $retorno = new \Ead1_Mensageiro('Sem renovação ativa', \Ead1_IMensageiro::SUCESSO);
        try{
            $vendaRenovacaoAtiva = $this->em->getRepository('\G2\Entity\Matricula')->verificaRenovacaoAtiva($id_matricula);
            if($vendaRenovacaoAtiva && !empty($vendaRenovacaoAtiva)){
                $retorno->setMensageiro('A disciplina não pode ser trancada porque o aluno está no processo de renovação. 
                                             A disciplina poderá ser trancada após o dia ' . $this->converterData($vendaRenovacaoAtiva['dt_limiterenovacao'], 'd/m/Y')
                            , \Ead1_IMensageiro::AVISO);
            }
        }catch (\Exception $e){
            $retorno->setMensageiro('Erro ao retornar: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $retorno;
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para DocumentosCategoria (tb_documentoscategoria)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DocumentosCategoria extends Negocio
{

    private $repositoryName = 'G2\Entity\DocumentosCategoria';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_documentoscategoria' => $row->getId_documentoscategoria(),
                    'st_documentoscategoria' => $row->getSt_documentoscategoria(),
                    'bl_ativo' => $row->getBl_ativo()
                );
            }

            return $arrReturn;
        }
    }

}
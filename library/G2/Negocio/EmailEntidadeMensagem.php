<?php

namespace G2\Negocio;

use G2\Constante\ItemConfiguracao;

/**
 * Classe de EmailEntidadeMensagem
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @Update Kayo Silva <kayo.silva@unyleya.com.br> 2013-30-10
 * @Update Alex Alexandre <alex.alexandre@unyleya.com.br> 2018-27-02
 */
class EmailEntidadeMensagem extends Negocio
{

    private $repositoryName = 'G2\Entity\EmailEntidadeMensagem';
    private $vwRepositoryName = 'G2\Entity\VwEmailEntidadeMensagem';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Find Email Entidade Mensagem By Entidade Sessão
     *
     * Essa função mudou, de acordo com a demanda GII-9173, com a necessidade
     * de herdar as mensagens padrão das entidades mãe.
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     *
     * @return array
     * @throws \Zend_Exception
     */
    public function findEmailEntidadeMensagemByEntidadeSessao ()
    {
        $msgPadraoNegocio = new MensagemPadrao();

        return $msgPadraoNegocio->herdarMensagemPadraoEntidadeMae($this->vwRepositoryName, array(
            'id_entidade' => $this->sessao->id_entidade,
            'id_tipoenvio' => 3
        ));
    }

    public function delete ($params)
    {
        $email = $this->em->getRepository($this->repositoryName)->findOneBy($params);
        $this->em->remove($email);
        $this->em->flush();
        return $email;
    }

    public function save ($params)
    {
        $EmailEntiMens = $this->em->getRepository($this->repositoryName)->findby(array(
            'id_mensagempadrao' => $params['id'],
            'id_entidade' => $this->sessao->id_entidade
        ));
        if ($EmailEntiMens) {
            $this->delete(array(
                'id_mensagempadrao' => $params['id'],
                'id_entidade' => $this->sessao->id_entidade
            ));
        }
        $emailConfig = $this->populateEmailConfig($params['id_emailconfig']);
        $textoSistema = $this->populateTextoSistema($params['id_textosistema']);
        $usuario = $this->populateUsuario($this->sessao->id_usuario);
        $emailEntidadeMens = new \G2\Entity\EmailEntidadeMensagem();
        $emailEntidadeMens->setBl_ativo(true)
            ->setDt_cadastro(new \DateTime())
            ->setId_emailconfig($emailConfig)
            ->setId_entidade($this->sessao->id_entidade)
            ->setId_mensagempadrao($params['id'])
            ->setId_textosistema($textoSistema)
            ->setId_usuariocadastro($usuario);

        $this->em->persist($emailEntidadeMens);
        $this->em->flush();
        return $emailEntidadeMens;
    }

    /**
     * Retorna referencia de Mensagem Padrao
     * @param integer $id
     * @return object
     */
    private function populateMensagemPadrao ($id)
    {
        return $this->getReference('G2\Entity\MensagemPadrao', $id);
    }

    /**
     * Retorna referencia de Texto Sistema
     * @param integer $id
     * @return object
     */
    private function populateTextoSistema ($id)
    {
        return $this->getReference('G2\Entity\TextoSistema', $id);
    }

    /**
     * Retorna referencia de Email Config
     * @param integer $id
     * @return object
     */
    private function populateEmailConfig ($id)
    {
        $emailConfig = new EmailConfig();
        return $emailConfig->getReference($id);
    }

    /**
     * Retorna referencia de Entidade
     * @param integer $id
     * @return object
     */
    private function populateEntidade ($id)
    {
        $entidade = new Entidade();
        return $entidade->getReference($id);
    }

    /**
     * Retorna refrencia de Usuario
     * @param integer $id
     * @return object
     */
    private function populateUsuario ($id)
    {
        return $this->em->getReference('G2\Entity\Usuario', $id);
    }


    /**
     * Find Email Entidade Mensagem By Entidade Sessão - MENSAGEM INTERNA
     * @return object G2\Entity\EmailEntidadeMensagem
     */
    public function findEmailEntidadeMensagemInterna ()
    {
        return $this->findBy($this->vwRepositoryName, array(
            'id_entidade' => $this->sessao->id_entidade,
            'id_tipoenvio' => 4
        ));
    }

    /**
     * @param $id_mensagempadrao
     * @param $id_entidade (caso seja usado pelo robo e nao tenha sessão)
     * @return null|object
     */
    public function findOneByEmailEntidadeMensagem($id_mensagempadrao , $id_entidade = null){

        $parametros = array();
        $parametros['id_entidade'] = $this->sessao->id_entidade ? $this->sessao->id_entidade : $id_entidade;
        $parametros['bl_ativo'] = 1;
        $parametros['id_mensagempadrao'] = $id_mensagempadrao;
        return $this->findOneBy('G2\Entity\EmailEntidadeMensagem',$parametros);

    }
}


<?php

namespace G2\Negocio;
use G2\Entity\EntidadeIntegracao;
use G2\Entity\Sistema;

/**
 * Classe de negócio para Integração com o CRM
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class IntegracaoCRM extends Negocio
{


    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    private static $con;

    /**
     * @var integer
     */
    private $entidade;


    /**
     * @param null $idEntidade
     * @param bool $nlsTimeFormat
     */
    public function __construct($id_entidade)
    {

        parent::__construct();
        $this->entidade = $id_entidade;

    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getCon(){

        if(!self::$con){

            $ei = new EntidadeIntegracao();
            $ei->findOneBy(array('id_entidade'=>$this->entidade, 'id_sistema'=>21));
            if(!$ei->getId_entidadeintegracao())
                throw new \Exception("IntegracaoCRM: Integração não encontrada");

            $si = new Sistema();
            $si->find($ei->getId_sistema());
            if(!$si->getst_conexao())
                throw new \Exception("IntegracaoCRM: Conexão da Integração não encontrada");

            $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(array(APPLICATION_PATH . "/../library/G2/Entity/Integracao/CRM"), false);
            self::$con = \Doctrine\ORM\EntityManager::create(json_decode($si->getst_conexao(), true), $config);

        }

        return self::$con;
    }


    /**
     * Atualiza uma Venda no CRM
     * @param $id_venda
     * @return bool
     * @throws \Exception
     */
    public function atualizarPagamentoCRM($id_venda){

        try {

            if(!$id_venda)
                throw new \Exception("IntegracaoCRM: Venda não informada");

            $stmt = $this->getCon()->getConnection()->prepare("update parce_oportunidade_cstm set pago_c= '1'  where venda_c='$id_venda'");
            $stmt->execute();

            return new \Ead1_Mensageiro('IntegracaoCRM: Venda atualizada com sucesso', \Ead1_IMensageiro::SUCESSO);

        } catch(\Exception $e){
            throw new \Exception("IntegracaoCRM: ".$e->getMessage());
        }

    }

}
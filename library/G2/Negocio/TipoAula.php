<?php

namespace G2\Negocio;

/**
 * Classe de negócio para TipoAula (tb_tipoaula)
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoAula extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }
    
    public function findTipoAula($params){
        $result = $this->findBy('\G2\Entity\TipoAula', $params);
        return $result;
    }
    
}
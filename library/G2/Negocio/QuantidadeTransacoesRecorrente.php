<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 24/03/15
 * Time: 15:32
 */

namespace G2\Negocio;


use Doctrine\Common\Util\Debug;

class QuantidadeTransacoesRecorrente extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function executaRelatorio($params = array()){
        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])){
            $sel .= " AND e.id_entidade = :id_entidade";
        }else{
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($this->sessao->id_entidade);
            $arrayEntidade = array();
            foreach($result as $ent){
                $arrayEntidade[] = $ent['id_entidade'];
            }
            if(!empty($arrayEntidade))
                $sel .= " AND e.id_entidade in(".join(',', $arrayEntidade).")";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])){
            $sel .= " AND l.dt_vencimento BETWEEN :dt_inicio AND :dt_fim ";
        }elseif(!empty($params['dt_inicio'])){
            $sel .= " AND l.dt_vencimento > :dt_inicio ";
        }elseif(!empty($params['dt_fim'])){
            $sel .= " AND l.dt_vencimento < :dt_fim ";
        }

        $sql = "SELECT DISTINCT
                        e.st_nomeentidade, e.id_entidade,
                        COUNT(l.id_lancamento) AS nu_totaltransacoes,
                        SUM(l.nu_quitado) AS nu_totalsomatorio
                    FROM tb_lancamentovenda lv
                        JOIN tb_venda v ON lv.id_venda = v.id_venda
                        JOIN tb_entidade e ON e.id_entidade = v.id_entidade
                        JOIN tb_lancamento l ON l.id_lancamento = lv.id_lancamento

                    WHERE l.id_meiopagamento = 11
                      AND bl_quitado = 1
                      {$sel}
                    GROUP BY e.st_nomeentidade, e.id_entidade
                    ORDER BY st_nomeentidade";

        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])){
            $statement->bindValue('id_entidade', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])){

            $dt_fim = (new \DateTime($params['dt_fim']))->format('d/m/Y');
            $dt_inicio = (new \DateTime($params['dt_inicio']))->format('d/m/Y');

            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        }elseif(!empty($params['dt_inicio'])){

            $dt_inicio = (new \DateTime($params['dt_inicio']))->format('d/m/Y');
            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));

        }elseif(!empty($params['dt_fim'])){

            $dt_fim = (new \DateTime($params['dt_fim']))->format('d/m/Y');
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        }

        $statement->execute();
        $results = $statement->fetchAll();

        foreach($results as &$result){
            $result['nu_totalsomatorio'] = number_format($result['nu_totalsomatorio'], 2, ',', '.');
        }

        return $results;
    }

} 
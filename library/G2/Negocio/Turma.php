<?php

namespace G2\Negocio;

/**
 * Classe de negócio para métodos realcionados a Cadastro de Turma
 * @author Helder Fernandes  <helder.silva@unyleya.com.br>
 */
class Turma extends Negocio
{

    private $repositoryTurma = 'G2\Entity\Turma';
    private $repositoryTurmaEntity = 'G2\Entity\TurmaEntidade';

    public function __contruct()
    {
        parent::__construct();
    }

    /**
     *
     * @param integer $id
     * @return \G2\Entity\Turma
     *
     */
    public function enviaDadosTabela($array)
    {
        try {
            $this->beginTransaction();

            //------------------PREPARAÇÃO DOS MODELOS-----------

            $arrayProjetoPedagogico = (isset($array['id_projetopedagogico']) && is_array($array['id_projetopedagogico']) ? $array['id_projetopedagogico'] : array());
            $arrayTurma = $array['modeloTurma'];
            $arrayTurno = $array['modeloTurno'];
            $arrayHorario = array();
            $arrayIds = array();

            //--------------VERIFICA SE EXISTE UM ITEM NA ARVORE-----
            if (array_key_exists('id_entidade', $array)) {
                $arrayIds = $array['id_entidade'];
            }

            //--------------VERIFICA SE EXISTE UM HORARIO--------
            if (array_key_exists('horario', $array)) {
                $arrayHorario = $array['horario'];
            }

            //--------------CADASTRO DE TURMA--------------------

            $objetoTurma = new \G2\Entity\Turma();
            $usuario = $this->find('\G2\Entity\Usuario', $this->sessao->id_usuario);
            $entidade = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

            if (array_key_exists('id_turma', $arrayTurma) && $arrayTurma['id_turma']) {
                $countMatriculaFreePass = count($this->findBy('\G2\Entity\VwMatricula', array('id_turma' => $arrayTurma['id_turma'], 'id_tiporegracontrato' => \G2\Constante\TipoRegraContrato::TB_TIPO_REGRA_CONTRATO_FREE_PASS)));
                if($countMatriculaFreePass > $arrayTurma['nu_maxfreepass'] && $arrayTurma['id_evolucao'] == \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO){
                    throw new \Zend_Exception("Não foi possível alterar a quantidade máxima de alunos do tipo Free Pass. Já existe(m) ".$countMatriculaFreePass." matrícula(s) Free Pass alocadas nessa turma!");
                }
                $objetoTurma = $this->find('\G2\Entity\Turma', $arrayTurma['id_turma']);
            }

            $objetoTurma->setId_usuariocadastro($usuario);
            $objetoTurma->setId_entidadecadastro($entidade);
            $objetoTurma->setNu_maxalunos($arrayTurma['nu_maxalunos']);
            $objetoTurma->setBl_ativo(true);
            $objetoTurma->setSt_turma(substr($arrayTurma['st_turma'], 0, 150));
            $objetoTurma->setSt_tituloexibicao(substr($arrayTurma['st_tituloexibicao'], 0, 150));
            $objetoTurma->setNu_maxfreepass($arrayTurma['nu_maxfreepass']);

            if ($arrayTurma['st_codigo'])
                $objetoTurma->setst_codigo($arrayTurma['st_codigo']);

            $objetoTurma->setId_evolucao($this->find('\G2\Entity\Evolucao', $arrayTurma['id_evolucao']));
            if (strlen($arrayTurma['dt_inicioinscricao']) > 0) {
                $objetoTurma->setDt_inicioinscricao($this->converteDataBanco($arrayTurma['dt_inicioinscricao']));
            } else {
                $objetoTurma->setDt_inicioinscricao(null);
            }
            if (strlen($arrayTurma['dt_fiminscricao']) > 0) {
                $objetoTurma->setDt_fiminscricao($this->converteDataBanco($arrayTurma['dt_fiminscricao']));
            } else {
                $objetoTurma->setDt_fiminscricao(null);
            }
            if (strlen($arrayTurma['dt_inicio']) > 0) {
                $objetoTurma->setDt_inicio($this->converteDataBanco(substr($arrayTurma['dt_inicio'], 0, 10)));
            } else {
                $objetoTurma->setDt_inicio(null);
            }
            if (strlen($arrayTurma['dt_fim']) > 0) {
                $objetoTurma->setDt_fim($this->converteDataBanco($arrayTurma['dt_fim']));
            } else {
                $objetoTurma->setDt_fim(null);
            }
            if ($arrayTurma['id_situacao'] == 1) {
                $objetoTurma->setId_situacao($this->find('\G2\Entity\Situacao', 84));
            } else {
                $objetoTurma->setId_situacao($this->find('\G2\Entity\Situacao', 83));
            }
            if (!(array_key_exists('id_turma', $arrayTurma) && $arrayTurma['id_turma'])) {
                $objetoTurma->setDt_cadastro(new \DateTime());
            }

            if ($objetoTurma->getst_codigo()) {
                $objetoTurmaV = $this->findOneBy('\G2\Entity\Turma', array('st_codigo' => $objetoTurma->getst_codigo()));
                if ($objetoTurmaV instanceof \G2\Entity\Turma) {
                    if ($objetoTurmaV->getId_turma() != $objetoTurma->getId_turma()) {
                        throw new \Exception("O Código " . $objetoTurma->getst_codigo() . " já está sendo utilizado na Turma " . $objetoTurmaV->getSt_turma());
                    }
                }
            }



            $result = $this->save($objetoTurma); //salva turma

            if (!$result) {
                throw  new \Exception("Erro ao salvar turma");
            }

            //-----------------------SALVA VINCULOS DE TURNO---------------------------
            if ($arrayTurno['id_turno'] !== "0") {
                $usuario = $this->find('\G2\Entity\Usuario', $this->sessao->id_usuario);
                $objetoTurno = $this->findOneBy('\G2\Entity\TurmaTurno', array('id_turma' => $result->getId_turma()));
                $turno = $this->find('\G2\Entity\Turno', $arrayTurno['id_turno']);

                if ($objetoTurno != null) {
                    $objetoTurno->setBl_ativo(true);
                    $objetoTurno->setId_turno($this->getReference('\G2\Entity\Turno', $turno->getId_turno()));
//                    $this->save($objetoTurno);
                } else {
                    $objetoTurno = new \G2\Entity\TurmaTurno();
                    $objetoTurno->setBl_ativo(true);
                    $objetoTurno->setId_usuariocadastro((int)$this->sessao->id_usuario);
                    $objetoTurno->setId_turma($result->getId_turma());
                    $objetoTurno->setId_turno($this->getReference('\G2\Entity\Turno', $turno->getId_turno()));
                    $objetoTurno->setDt_cadastro(new \DateTime());
                    $resultado = $this->save($objetoTurno);
                }
                $resultado = $this->save($objetoTurno);
                if (!$resultado) {
                    throw new \Exception("Erro ao salvar turma turno.");
                }
            }

            //----------------------------SALVA OS VINCULOS DE HORARIO----------------------------
            if (!empty($arrayHorario)) {
                if ($result) {
                    $repositoryHorario = '\G2\Entity\TurmaHorario';
                    foreach ($arrayHorario as $key => $array) {
                        $objeto = $this->findOneBy($repositoryHorario, array('id_turma' => $result->getId_turma(), 'id_horarioaula' => $array['id_horarioaula']));
                        if ($objeto != null) {
                            $objeto->setBl_ativo($array['ck_selectedHorario']);
//                            $salva = $this->save($objeto);
                        } else {
                            $objeto = new \G2\Entity\TurmaHorario();
                            $objeto->setBl_ativo($array['ck_selectedHorario']);
                            $objeto->setDt_cadastro(new \DateTime());
                            $objeto->setId_horarioaula($array['id_horarioaula']);
                            $objeto->setId_turma($result->getId_turma());
                            $objeto->setId_usuariocadastro((int)$this->sessao->id_usuario);
                        }
                        $salva = $this->save($objeto);
                        if (!$salva) {
                            throw new \Exception("Erro ao salvar turma horario.");
                        }
                    }
                }
            }

            //----------------------------SALVA OS VINCULOS DE PROJETOS PEDAGOGICOS----------------
            $mensageiro = NULL;
            if ($result && $result->getId_turma()) {
                // REMOVER PROJETOS ATUALMENTE ASSOCIADOS
                $projetosTurma = $this->findBy('\G2\Entity\TurmaProjeto', array('id_turma' => $result->getId_turma(), 'bl_ativo' => true));
                if ($projetosTurma) {
                    foreach ($projetosTurma as $pt) {
                        if (!in_array($pt->getId_projetopedagogico(), $arrayProjetoPedagogico)) {
                            $pt->setBl_ativo(false);
                            $this->save($pt);
                        }
                    }
                }

                // SALVAR OS NOVOS PROJETOS
                $usuario = $this->find('\G2\Entity\Usuario', $this->sessao->id_usuario);
                foreach ($arrayProjetoPedagogico as $id_projetopedagogico) {
                    $projetopedagogico = $this->find('\G2\Entity\ProjetoPedagogico', (int) $id_projetopedagogico);

                    $turmaProjeto = $this->findOneBy('\G2\Entity\TurmaProjeto', array(
                        'id_turma' => $result->getId_turma(),
                        'id_projetopedagogico' => (int) $id_projetopedagogico,
                        'bl_ativo' => false
                    ));

                    if ($turmaProjeto && $turmaProjeto->getId_turmaprojeto()) {
                        $turmaProjeto->setBl_ativo(true);
                    } else {
                        $turmaProjeto = new \G2\Entity\TurmaProjeto();
                        $turmaProjeto->setId_turma($result);
                        $turmaProjeto->setId_projetopedagogico($projetopedagogico);
                        $turmaProjeto->setBl_ativo(true);
                        $turmaProjeto->setDt_cadastro(new \DateTime());
                        $turmaProjeto->setId_usuariocadastro($usuario);
                    }

                    $resultado = $this->save($turmaProjeto);

                    if (!$resultado) {
                        throw new \Exception("Erro ao salvar turma projeto.");
                    }

                }

            }

            //-------------------------SALVA OS REGISTROS DA ARVORE DE ENTIDADES----------------
            //Foreach que insere todos os registros da arvore de entidades na tabela turmaentidade
            //verificando se existem registros com as associações de turma entidade
            if (!empty($arrayIds)) {
                $base = $this->findBy($this->repositoryTurmaEntity, array('id_turma' => $result->getId_turma(), 'bl_ativo' => true));
                foreach ($base as $bs) {
                    $bs->setBl_ativo(false);
                    if (!$this->save($bs)) {
                        throw new \Exception("Erro ao inativar turma entidade");
                    }
                }

                foreach ($arrayIds as $array) {
                    $params = array();
                    $params['id_entidade'] = $array;
                    $params['id_turma'] = $result->getId_turma();

                    $query = $this->findOneBy($this->repositoryTurmaEntity, $params);
                    if ($query) {
                        if ($query->getBl_Ativo() == false) {
                            $entidade = $this->find($this->repositoryTurmaEntity, $query->getId_turmaentidade());
                            $entidade->setBl_Ativo(true);
                            if (!$this->save($entidade)) {
                                throw new \Exception("Erro ao ativar turma entidade");
                            }
                        }
                    } else {
                        $serializa = new \Ead1\Doctrine\EntitySerializer($this->em);
                        $turmaentidade = new \G2\Entity\TurmaEntidade();

                        $arr = array();
                        $arr['id_turma'] = $result->getId_turma();
                        $arr['dt_cadastro'] = new \DateTime();
                        $arr['id_usuariocadastro'] = $this->sessao->id_usuario;
                        $arr['id_entidade'] = $array;
                        $arr['bl_ativo'] = true;
                        $newser = $serializa->arrayToEntity($arr, $turmaentidade);
                        if (!$this->save($newser)) {
                            throw new \Exception("Erro ao salvar turma entidade");
                        }
                    }
                }
            } else {
                $base = $this->findBy($this->repositoryTurmaEntity, array('id_turma' => $result->getId_turma(), 'bl_ativo' => true));
                foreach ($base as $bs) {
                    $bs->setBl_ativo(false);
                    if (!$this->save($bs)) {
                        throw new \Exception("Erro ao inativar turmas entidade");
                    }
                }
            }


            /*
             * INICIO
             * Começo do processo de salvar a turma na pontosoft caso a entidade
             * for vinculada a esse sistema
             */
            $bl_integracaoexterna = $this->salvarTurmaNaPontoSoft($result);
            /*
             * FIM
             * Finalização do trecho de código para a integração da pontosoft
             */

            $this->commit();

            /*
             * Transformando o resultado final em array e inserindo um novo index de integracao externa
             * */
            $resultFinal = $this->toArrayEntity($result);
            $resultFinal['bl_integracaoexterna'] = $bl_integracaoexterna;

            return $resultFinal;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar turma com seus milhares de relacionamento. " . $e->getMessage());
        } catch (\Doctrine\ORM\ORMException $ex) {
            $this->rollback();
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Começo do processo de salvar a turma na pontosoft caso a entidade for vinculada a esse sistema
     * @param \G2\Entity\Turma $turma
     * @return bool
     * @throws \Zend_Exception
     */
    private function salvarTurmaNaPontoSoft(\G2\Entity\Turma $turma)
    {
        try {
            $entidadeIntegracao = $this->findOneBy('\G2\Entity\EntidadeIntegracao', array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_sistema' => \G2\Constante\Sistema::PONTOSOFT
            ));


            $bl_integracaoexterna = false;
            if ($entidadeIntegracao != null) {

                if (!$turma->getst_codigo())
                    throw new \Exception("Informe o Código para a integração PontoSoft");

                $turmaProjeto = $this->findOneBy('\G2\Entity\TurmaProjeto', array('id_turma' => $turma->getId_turma()));
                if ($turmaProjeto) {
                    $integracaoExterna = new IntegracaoExterna($this->getId_entidade(), true);
                    $ppIntegracao = $integracaoExterna->salvarDadosProjetoIntegracao($turmaProjeto->getId_projetopedagogico(), 20);
                    $turmaIntegracao = $integracaoExterna->salvarDadosTurmaIntegracao($turmaProjeto->getId_turma(), 20, $ppIntegracao);
                    if ($ppIntegracao && $turmaIntegracao) {
                        $bl_integracaoexterna = true;
                    }
                }
            }

            return $bl_integracaoexterna;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar turma na pontosoft. " . $e->getMessage());
        }
    }

    /**
     *
     * Finaliza uma Turma
     * @param $id_turma
     * @return \Ead1_Mensageiro
     */
    public function finalizar($id_turma)
    {

        $this->beginTransaction();
        try {

            if ($id_turma == false)
                throw new \Zend_Exception("Favor informar a Turma.");

            $turma = $this->findTurma($id_turma);
            if ($turma instanceof \G2\Entity\Turma) {

                $antes = clone $turma;

                $evolucao = $this->findOneBy('\G2\Entity\Evolucao', array('id_evolucao' => 53)); // Turma Finalizada
                if ($evolucao) {
                    $turma->setId_evolucao($evolucao);
                    $turma->setDt_fim(new \DateTime(date("Y-m-d")));
                } else {
                    throw new \Zend_Exception("Não foi possível encontrar a Evolução \"Encerrada\".");
                }
                $entitySalva = $this->save($turma);
                if ($entitySalva->getid_evolucao() instanceof \G2\Entity\Evolucao && $entitySalva->getid_evolucao()->getid_evolucao() == 53) {


                    $log = new Log();
                    $log->salvar(409, $antes, $entitySalva);

                    $ei = new \G2\Negocio\EntidadeIntegracao();
                    $eiEntity = $ei->retornaEntidadeIntegracao($turma->getId_entidadecadastro(), 20);
                    if ($eiEntity) {
                        $ps = new \G2\Negocio\IntegracaoExterna(21, true);
                        $ps->cancelarTurmaIntegracao($entitySalva);
                    }

                    $this->commit();
                    return new \Ead1_Mensageiro($entitySalva->toBackboneArray(), \Ead1_IMensageiro::SUCESSO);
                }

            }

            throw new \Zend_Exception("Houve algum problema ao finalizar a turma, favor contactar o suporte.");

        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }


    public function findTurma($id)
    {
        return parent::find($this->repositoryTurma, $id);
    }

    public function findProjetoTurma($id)
    {
        try {
            $params['id_turma'] = $id;
            $params['bl_ativo'] = true;
            $repositoryName = 'G2\Entity\VwTurmaProjetoPedagogico';
            return $this->findBy($repositoryName, $params, array('st_projetopedagogico' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findVinculosEntidades($id)
    {
        try {
            $params['id_turma'] = $id;
            $params['bl_ativo'] = true;
            $repositoryName = 'G2\Entity\TurmaEntidade';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function deletaProjetoTurma($array)
    {
        try {
            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $objeto = new \G2\Entity\TurmaProjeto();
            if ($array['id_turma']) {
                $objeto = $this->findOneBy("\G2\Entity\TurmaProjeto", array('id_projetopedagogico' => $array['id_projetopedagogico'],
                    'id_turma' => $array['id_turma']));
            }
            $newSerialize = $serialize->arrayToEntity($array, $objeto);
            $newSerialize->setBl_ativo(false);
            return $this->save($newSerialize);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    public function findByTurmaEntidade($params)
    {
        try {
            $repositoryName = 'G2\Entity\TurmaEntidade';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function enviaemail($param)
    {
        try {
            $vw = new \VwMatriculaTO();
            $vw->setId_turma($param);
            $resultado = new \MatriculaRO();
            $resultado->enviarArrayMensagemAtivacaoMatricula($vw);

            return $resultado->bo->mensageiro->toArray();
        } catch (\Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function retornaTurmaEntidade($params)
    {
        try {
            $repositoryName = 'G2\Entity\Turma';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo para retornar dados da turma com maior carga horaria
     * @param $params
     * @return \Ead1_Mensageiro
     */
    public function retornarTurmaMaiorCargaHoraria($params)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            //busca a turma com o total de carga horaria, ordenando pelo total de carga horaria
            // e buscando somente o primeiro
            $result = $this->findBy('\G2\Entity\VwTurmaTotalCargaHoraria',
                $params, array('nu_totalcargahoraria' => 'DESC'), 1);

            //verificar se existe valor no array
            if (!empty($result)) {
                //transforma em um array
                foreach ($result as $row) {
                    $result = $this->toArrayEntity($row);
                }
                $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro->setMensageiro("Turma não vinculada ao projeto.", \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            $mensageiro->setMensageiro("Erro ao tentar consultar turma carga horária." . $e->getMessage(),
                \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Retorna dados de vw_turma
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwTurma(array $params = array())
    {
        try {
            $params['id_entidadecadastro'] = isset($params['id_entidadecadastro']) ? $params['id_entidadecadastro'] : $this->sessao->id_entidade;
            return $this->findBy('\G2\Entity\VwTurma', $params);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna dados de vw_turmadisciplina
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwTurmaDisciplina(array $params = array())
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            return $this->findBy('\G2\Entity\VwTurmaDisciplina', $params);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna dados dos alunos para carteirinha
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarAlunosTurmaCarteirinha(array $params = [])
    {
        try {
            $params['id_entidadematricula'] = isset($params['id_entidadematricula']) ? $params['id_entidadematricula'] : $this->sessao->id_entidade;
            $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
            $params['id_evolucao'] = isset($params['id_evolucao']) ? $params['id_evolucao'] : \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
            $params['id_NotTiporegracontrato'] = \G2\Constante\TipoRegraContrato::TB_TIPO_REGRA_CONTRATO_FREE_PASS;
            $params['st_urlavatar'] = true;
            return $this->em->getRepository('\G2\Entity\VwMatricula')->retornarUsuariosTurmaCarteirinha($params);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar alunos para carteirinha. ' . $e->getMessage());
        }
    }


    /**
     * @history TEC-241
     * @description Retorna turmas disponiveis de acordo com um produto
     * @param int $id_produto
     * @param int $id_turma (opciona)
     * @return array \G2\Entity\VwTurmasProduto
     */
    public function getAllByProduto($id_produto, $id_turma = null, $id_situacao = \G2\Constante\Situacao::TB_TURMA_ATIVA)
    {
        $now = date('Y-m-d H:i:s');
        $dql = "SELECT vw FROM \G2\Entity\VwTurmasProduto vw WHERE ('{$now}' >= vw.dt_inicioinscricao AND '{$now}' <= vw.dt_fiminscricao AND vw.id_produto = {$id_produto}) ";
        if ($id_turma) {
            $dql .= " OR vw.id_turma = {$id_turma}";
        }
        if ($id_situacao)
        {
            $dql .= " AND vw.id_situacao = {$id_situacao}";
        }
        return $this->em->createQuery($dql)->getResult();
    }

    /**
     * Retorna Relacionamento de Turma com os Turnos que estão com bl_ativo = 1
     * @param array $params
     * @return array | object
     */
    public function retornaTurmaTurnoAtivos(array $params = array())
    {
        $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
        $result = $this->findBy('\G2\Entity\TurmaTurno', $params, array('dt_cadastro' => 'desc'));
        return $result;
    }

    /**
     * Retorna os dias da semana baseados na turma
     * @param integer $id_turma
     * @return \Ead1_Mensageiro
     */
    public function retornarDiaSemanaByTurma($id_turma)
    {
        $mensageiro = new \Ead1_Mensageiro();
        $result = $this->em->getRepository('\G2\Entity\Turma')->retornarDiaSemanaByTurma($id_turma);
        if ($result) {
            $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro->setMensageiro("Nenhum Dia da Semana encontrado.", \Ead1_IMensageiro::AVISO);
        }
        return $mensageiro;
    }

    /**
     * Retorna dados dos alunos para carteirinha
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarAlunosTurmaCarteirinhaCombo(array $params = [])
    {
        try {
            $params['id_entidadematricula'] = isset($params['id_entidadematricula']) ? $params['id_entidadematricula'] : $this->sessao->id_entidade;
            $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
            $params['id_evolucao'] = isset($params['id_evolucao']) ? $params['id_evolucao'] : \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
            $params['id_NotTiporegracontrato'] = \G2\Constante\TipoRegraContrato::TB_TIPO_REGRA_CONTRATO_FREE_PASS;
            if (isset($params['dt_matricula_inicial']) && isset($params['dt_matricula_fim'])) {
                $params['dt_matricula_inicial'] = $this->converteDate($params['dt_matricula_inicial']) . ' 00:00:00';
                $params['dt_matricula_fim'] = $this->converteDate($params['dt_matricula_fim']) . ' 23:59:59';
            }
            $alunos = $this->em->getRepository('\G2\Entity\VwMatricula')->retornarUsuariosTurmaCarteirinha($params);
            //Faz uma limpeza de dados duplicados
            $alunosVerificado = $this->limparArrayDuplicados($alunos, 'id_usuario');
            return $alunosVerificado;
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar alunos para carteirinha. ' . $e->getMessage());
        }
    }


}

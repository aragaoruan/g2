<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 30-03-2015
 * Time: 15:33
 */

namespace G2\Negocio;


class NucleoTm extends Negocio
{
    private $entityNucleoTm;

    private $mensageiro;

    public function __construct()
    {
        parent::__construct();
        //abre esse arquivo
        $this->entityNucleoTm = 'G2\Entity\NucleoTm';
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Retorna os dados do NucleoTm por id
     * @param $id
     * @return mixed
     */
    public function retornarNucleoTmById($id)
    {
        return $this->find($this->entityNucleoTm, $id);
    }


    /**
     * Método para retornar a pesquisa da funcionalidade
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function retornarPesquisaNucleo(array $params = [], $orderBy = null, $limit = 0, $offset = 0)
    {

        $arrColumns = array();
        if (isset($params['grid'])) {

            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "ntm");
        }
        unset($params['to'], $params['txtSearch'], $params['grid']);


        $params['id_entidadematriz'] = isset($params['id_entidadematriz']) && !empty($params['id_entidadematriz']) ? $params['id_entidadematriz'] : $this->getId_entidade();

        $result = $this->em->getRepository($this->entityNucleoTm)
            ->retornarPesquisa($arrColumns, $params, $orderBy, $limit, $offset);

        if ($result->rows) {
            $arrData = [];
            foreach ($result->rows as $i => $row) {
                $arrData[$i] = $row;
                $arrData[$i]['id'] = $row['id_nucleotm'];
            }

            unset($result->rows);
            $result->rows = $arrData;

        }

        return $result;


    }


    /**
     * Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
     * @param string $prefix
     * @param json string $grid
     * @return array
     */
    private function getColumnsFromPesquisaDinamica($grid, $prefix = '')
    {
        $grids = json_decode($grid);
        $arrGrid = array();
        $prefix = (!empty($prefix) ? $prefix . "." : null);
        if (is_object($grids)) {
            foreach ($grids->grid as $key => $grid) {
                array_push($arrGrid, $prefix . $grid->name);
            }
        }
        return $arrGrid;
    }

    /**
     * Retorna Dados do nucleo
     * @param array $where
     * @return \G2\Entity\NucleoTm
     */
    public function retornarNucleo(array $params = null)
    {
        $id_entidade = $this->sessao->id_entidade;
        $params['id_entidade'] = $id_entidade;
        //Seleciona o repository para busca dos dados
        $result = $this->em->getRepository($this->entityNucleoTm)->retornaNucleoFuncionarioTm($params);

        if (empty($result)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', \Ead1_IMensageiro::AVISO);
        }
        return $result;
    }

    private function entidadeRecursiva($eTO, $arrAux)
    {
        $arrRecurssivo = array();
        if ($arrAux) {
            foreach ($arrAux as $index => $entidadeTO) {
                if ($entidadeTO->getId_entidadepai() == $eTO->getId_entidade()) {
                    $arrRecurssivo[$index]['to'] = $entidadeTO;
                    $arrRecurssivo[$index]['data'] = $entidadeTO->getSt_nomeentidade();
                    $arrRecurssivo[$index]['tipo'] = 'Entidade';
                    $arrRecurssivo[$index]['metadata'] = $entidadeTO->getId_entidade();
                    $arrRecurssivo[$index]['children'] = $this->entidadeRecursiva($entidadeTO, $arrAux);
                }
            }
        }
        return (empty($arrRecurssivo) ? null : $arrRecurssivo);
    }


    /**
     * Salva núcleos
     * @param array $data
     * @author Vinícius Avelino Alcântara <vinicius.alcantara@unyleya.com.br>
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarNucleoAtendente($data)
    {
        try {

            if (!empty($data)) {

                //instancia as variaveis e pega os valores padrões
                $situacaoNegocio = new Situacao();
                $id_entidadematriz = $this->sessao->id_entidade;
                $id_usuariocadastro = $this->sessao->id_usuario;
                //pega a referencia da entidade
                $entidade = $this->getReference('\G2\Entity\Entidade', $data['id_entidade']);
                //pega a instancia da entity
                $nucleotm = new \G2\Entity\NucleoTm();
                $nucleo = new \G2\Entity\NucleoTm();

                //verifica se foi passado a chave primaria da entity e se tem valor
                if (in_array('id_nucleotm', $data) && !empty($data['id_nucleotm'])) {
                    //busca o dado no banco para ser editado
                    $nucleotm = $this->findOneBy($this->entityNucleoTm,array('id_nucleotm'=>$data['id_nucleotm'], 'id_entidadematriz'=>$id_entidadematriz));
                    $nucleo = $this->findOneBy($this->entityNucleoTm,array('id_entidade'=>$data['id_entidade'],'id_entidadematriz'=>$id_entidadematriz));
                    if(!empty($nucleo) && $nucleo->getId_entidade() != $nucleotm->getId_entidade())
                    {
                        return $this->mensageiro->setMensageiro('Núcleo já existe nessa holding!', \Ead1_IMensageiro::ERRO);
                    }
                }else{
                    //vê se o dado ja existe (usado para o cadastro de novos núcleos)
                    $nucleotm = $this->findOneBy($this->entityNucleoTm,array('id_entidade'=>$data['id_entidade'],'id_entidadematriz'=>$id_entidadematriz));
                }

                if(!empty($nucleotm) && $nucleotm->getId_nucleotm()!=$data['id_nucleotm'])
                {
                    return $this->mensageiro->setMensageiro('Núcleo já existe nessa holding!', \Ead1_IMensageiro::ERRO);
                }elseif(empty($nucleotm))
                {
                    $nucleotm = new \G2\Entity\NucleoTm();
                }
                $dt_cadastro = date('Y-m-d h:i:s');
                $nucleotm->setId_entidade($entidade->getId_entidade())
                    ->setSt_nucleotm($entidade->getSt_nomeentidade())
                    ->setId_entidadematriz($id_entidadematriz)
                    ->setId_usuariocadastro($id_usuariocadastro)
                    ->setId_situacao($situacaoNegocio->getReference($data['id_situacao']))
                    ->setDt_cadastro($dt_cadastro)
                    ->setBl_ativo(true);



                //salva no banco
                $nucleotm = $this->save($nucleotm);


                //verifica se deu erro
                if (!$nucleotm) {
                    throw new \Exception('Erro ao salvar núcleo.');
                } else {
                    //manda o mensageiro de sucesso com os dados
                    return $this->mensageiro->setMensageiro($this->toArrayEntity($nucleotm), \Ead1_IMensageiro::TYPE_SUCESSO);
                }
            } else {
                return $this->mensageiro->setMensageiro('Dados inválidos!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao tentar salvar dados,' . $e->getMessage());
        }
    }
}
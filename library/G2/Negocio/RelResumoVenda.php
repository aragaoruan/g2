<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 25/06/2015
 * Time: 14:41
 */

namespace G2\Negocio;


class RelResumoVenda extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function executaRelatorio($params = array())
    {
        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])) {
            $sel .= " v.id_entidade  = :id_entidadeatendimento";
        } else {
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($this->sessao->id_entidade);
            $arrayEntidade = array();
            foreach ($result as $ent) {
                $arrayEntidade[] = $ent['id_entidade'];
            }
            if (!empty($arrayEntidade))
                $sel .= " v.id_entidade  in(" . join(',', $arrayEntidade) . ")";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro BETWEEN :dt_inicio AND :dt_fim ";
        } elseif (!empty($params['dt_inicio'])) {
            $sel .= " AND v.dt_cadastro > :dt_inicio ";
        } elseif (!empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro < :dt_fim ";
        }

        $sql = "SELECT
                        e.st_nomeentidade AS 'entidade'
                        , COUNT(v.id_venda) AS 'total_vendas'
                        , SUM(total.nu_valor) AS 'valor_recebido'
                        , SUM(dinheiro.total_dinheiro) AS 'valor_dinheiro'
                        , SUM(cheque.total_cheque) AS 'valor_cheque'
                        , SUM(cartao.total_cartao) AS 'valor_cartao'
                        , SUM(transferencia.total_transferencia) AS 'valor_transferencia'
                        , SUM(carta_credito.total_carta_credito) AS 'valor_carta'
                        , SUM(bolsa.total_bolsa) AS 'valor_bolsa'
                        , SUM(deposito_bancario.total_deposito_bancario) AS 'valor_deposito'
                        , SUM(empenho.total_empenho) AS 'valor_empenho'
                        , SUM(boleto.total_boleto) AS 'valor_boleto'
                        , SUM(permuta.total_permuta) AS 'valor_permuta'

                    FROM tb_venda v
                        INNER JOIN tb_entidade e ON v.id_entidade = e.id_entidade
                        /* valor total recebido pela Entidade */
                        OUTER APPLY ( SELECT SUM(nu_valor) AS nu_valor
                                        FROM tb_lancamentovenda lv
                                          INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                        WHERE v.id_venda = lv.id_venda
                                          AND l.bl_ativo = 1) AS total

                        /* valor total em dinheiro*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_dinheiro, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 3 --dinheiro
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS dinheiro ON v.id_venda = dinheiro.id_venda

                        /* valor total em cheque*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_cheque, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 4 --cheque
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS cheque ON v.id_venda = cheque.id_venda

                        /* valor total em cartão*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_cartao, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento in (1, 7, 11) --– 1 - Cartão Crédito | 7 - Cartão Débito | 11 - Cartão Recorrente
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS cartao ON v.id_venda = cartao.id_venda

                        /* valor total em Transferência*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_transferencia, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 8 -- Transferência
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS transferencia ON v.id_venda = transferencia.id_venda

                        /* valor total em Carta de Crédito*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_carta_credito, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 13 --– Carta de Crédito
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS carta_credito ON v.id_venda = carta_credito.id_venda

                        /* valor total em Bolsa*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_bolsa, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 10 --– Bolsa
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS bolsa ON v.id_venda = bolsa.id_venda

                        /* valor total em Depósito Bancário*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_deposito_bancario, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 6 --– Depósito Bancário
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS deposito_bancario ON v.id_venda = deposito_bancario.id_venda

                        /* valor total em Empenho*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_empenho, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 5 --– Empenho
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS empenho ON v.id_venda = empenho.id_venda

                        /* valor total em Boleto*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_boleto, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 2 --– Boleto
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS boleto ON v.id_venda = boleto.id_venda

                        /* valor total em Permuta*/
                        LEFT JOIN ( SELECT SUM(nu_valor) total_permuta, lv.id_venda
                                    FROM tb_lancamentovenda lv
                                      INNER JOIN tb_lancamento l ON lv.id_lancamento = l.id_lancamento
                                    WHERE l.id_meiopagamento = 9 --– Permuta
                                      AND l.bl_ativo = 1
                                    GROUP BY lv.id_venda) AS permuta ON v.id_venda = permuta.id_venda

                    WHERE {$sel}
                    GROUP BY e.st_nomeentidade";

        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])) {
            $statement->bindValue('id_entidadeatendimento', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);

            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        } elseif (!empty($params['dt_inicio'])) {

            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);
            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));

        } elseif (!empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        }

        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

    public function executaRelatorioIndividualAtendente($params = array())
    {
        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])) {
            $sel .= " AND v.id_entidade  = :id_entidadeatendimento";
        } else {
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($this->sessao->id_entidade);
            $arrayEntidade = array();
            foreach ($result as $ent) {
                $arrayEntidade[] = $ent['id_entidade'];
            }
            if (!empty($arrayEntidade))
                $sel .= " AND v.id_entidade  in(" . join(',', $arrayEntidade) . ")";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro BETWEEN :dt_inicio AND :dt_fim ";
        } elseif (!empty($params['dt_inicio'])) {
            $sel .= " AND v.dt_cadastro > :dt_inicio ";
        } elseif (!empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro < :dt_fim ";
        }

        $sql = "select * from rel.vw_atendentenegociacao as v WHERE id_atendente = " . $this->sessao->id_usuario . $sel;
        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])) {
            $statement->bindValue('id_entidadeatendimento', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);

            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d') . ' 00:00:00');
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d') . ' 23:59:59');

        } elseif (!empty($params['dt_inicio'])) {

            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);
            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d') . ' 00:00:00');

        } elseif (!empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d') . ' 23:59:59');

        }

        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

    public function executaRelatorioIndividualResumoAtendente($params = array())
    {
        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])) {
            $sel .= " AND v.id_entidade  = :id_entidadeatendimento";
        } else {
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($this->sessao->id_entidade);
            $arrayEntidade = array();
            foreach ($result as $ent) {
                $arrayEntidade[] = $ent['id_entidade'];
            }
            if (!empty($arrayEntidade))
                $sel .= " AND v.id_entidade  in(" . join(',', $arrayEntidade) . ")";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro BETWEEN :dt_inicio AND :dt_fim ";
        } elseif (!empty($params['dt_inicio'])) {
            $sel .= " AND v.dt_cadastro > :dt_inicio ";
        } elseif (!empty($params['dt_fim'])) {
            $sel .= " AND v.dt_cadastro < :dt_fim ";
        }

        $sql = "SELECT
                      COUNT(id_venda) AS nu_totalvendas
                    , SUM(nu_valorliquido) AS nu_totalrecebido
                    , SUM(nu_valordinheiro) AS nu_dinheiro
                    , SUM(nu_valorcheque) AS nu_cheque
                    , SUM(nu_valorcartao) AS nu_cartao
                    , SUM(nu_valortransferencia) AS nu_transferencia
                    , SUM(nu_valorcarta) AS nu_carta
                    , SUM(nu_valorbolsa) AS nu_bolsa
                    , SUM(nu_valorpermuta) AS nu_permuta
                    , SUM(nu_valordeposito) AS nu_deposito
                    , SUM(nu_valorempenho) AS nu_empenho
                    , SUM(nu_valorboleto) AS nu_boleto
                 FROM rel.vw_atendentenegociacao AS v
                 WHERE id_atendente = " . $this->sessao->id_usuario . $sel;

        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])) {
            $statement->bindValue('id_entidadeatendimento', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);

            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d') . ' 00:00:00');
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d') . ' 23:59:59');

        } elseif (!empty($params['dt_inicio'])) {

            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params['dt_inicio']);
            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d') . ' 00:00:00');

        } elseif (!empty($params['dt_fim'])) {

            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params['dt_fim']);
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d') . ' 23:59:59');

        }

        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }
}
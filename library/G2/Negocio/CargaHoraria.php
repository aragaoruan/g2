<?php

namespace G2\Negocio;

use Ead1\Doctrine\EntitySerializer;

/**
 */
class CargaHoraria extends Negocio
{

    private $repositoryName = 'G2\Entity\CargaHoraria';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null)
    {
        $result = $this->findBy('G2\Entity\CargaHoraria', array(), array('nu_cargahoraria' => 'ASC'));;
        if (count($result) > 0) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function save($entity)
    {
        $result = parent::save($entity);
        $serializer = new EntitySerializer($this->em);
        if ($result->getId_cargahoraria()) {
            return new \Ead1_Mensageiro($serializer->toArray($entity), \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro('Erro ao salvar Carga Horária', \Ead1_IMensageiro::ERRO);
        }
    }

}

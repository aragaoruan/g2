<?php

namespace G2\Negocio;

use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Ead1\Doctrine\EntitySerializer;
use G2\Utils\LogSistema;
use G2\Utils\Pagination;
use Zend_Date;

/**
 * Classe abstrata para Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-09-11
 */
class Negocio
{
    /**
     * Atributo que contém os dados de sessão
     * @var \Zend_Session_Namespace
     */
    public $sessao;
    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;
    protected $bootstrap;
    protected $connection;
    protected $entitySerialize;
    /**
     * Atributo que contém o código da entidade do usuário logado
     * @var int
     */
    private $id_entidade;

    public function __construct()
    {
        $this->sessao = new \Zend_Session_Namespace('geral');
        if (isset($this->sessao->id_entidade)) {
            $this->id_entidade = $this->sessao->id_entidade;
        }
        $this->bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->em = $this->bootstrap->getResource('doctrine')->getEntityManager();
        $this->entitySerialize = new \Ead1\Doctrine\EntitySerializer($this->em);

    }

    /**
     * renderiza a data presente no objeto informado
     * Troque.: $obj->getDate() instanceof \DateTime ? $vw->getDate()->format("d/m/Y") : $vw->getDate();
     * Por....: dateRender($obj, 'getDate')
     *
     * @param  object $source
     * @param  string $accessor
     * @param  string $format
     * @param  string $default
     * @return string
     */
    public static function dateRender($source, $accessor, $format = 'd/m/Y', $default = '-')
    {
        if (empty($source)) {
            return $default;
        }

        if ($source instanceof \DateTime) {
            return $source->format($format);
        } elseif (is_object($source)) {
            $source = $source->$accessor();

            if (is_object($source)) {
                return self::dateRender($source, $accessor);
            }
        }
        return $default;
    }

    /**
     *
     * Os nomes das conexões serão padronizados para precisarmos apenas o id da entidade e
     * este pegar a conexão relacionada
     * @param mixed $idIntegracao
     * @return mixed
     * @throws \Exception
     */
    public function getConnectionIntegracao($idIntegracao = null)
    {
        try {
            if ($idIntegracao != null) {
                $this->connection = $this->bootstrap->getResource('doctrine')->getEntityManager('integracao_' . $idIntegracao);
                if (!$this->connection->isOpen()) {
                    $this->connection = $this->connection->create(
                        $this->connection->getConnection(), $this->connection->getConfiguration());
                }

                return $this->connection;
            } else
                return false;
        } catch (\Exception $e) {
            throw  $e;
        }
    }

    /**
     * @param null $idIntegracao
     * @return mixed
     * @throws \Exception
     */
    public function restartConnectionIntegracao($idIntegracao = null)
    {
        try {
            if ($idIntegracao != null)
                return $this->bootstrap->getResource('doctrine')->getEntityManager('integracao_' . $idIntegracao)->resetEntityManager('integracao_' . $idIntegracao);
            else
                return false;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @return int|mixed
     */
    public function getId_entidade()
    {
        return $this->id_entidade;
    }

    /**
     * Retorna instância da classe EntitySerializer
     * @param $object
     * @return \Ead1\Doctrine\EntitySerializer
     */
    public function entitySerialize($object)
    {
        $serialize = new EntitySerializer($this->getem());
        return $serialize->entityToArray($object);
    }

    /**
     * Convert an entity to a JSON object
     *
     * @param $entity
     * @return string
     */
    public function toJsonEntity($entity)
    {
        $serialize = new EntitySerializer($this->getem());
        return $serialize->toJson($entity);
    }

    /**
     * Retorna todos os registros de uma determinada Entity
     * @param $repositoryName
     * @return array
     * @throws \Zend_Exception
     */
    public function findAll($repositoryName)
    {
        try {
            $ref = new \ReflectionClass($repositoryName);
            return $this->getem()->getRepository($ref->getName())->findAll();
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Retorna um unico registro de acordo com os filtros da consulta
     * @param $repositoryName
     * @param array $where
     * @param null $orderBy
     * @return null|object The entity instance or NULL if the entity can not be found.
     * @throws \Zend_Exception
     */
    public function findOneBy($repositoryName, array $where, $orderBy = null)
    {

        try {
            $ref = new \ReflectionClass($repositoryName);
            return $this->getem()->getRepository($ref->getName())->findOneBy($where, $orderBy);
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Insere ou altera dados na base de dados
     * @param $entity
     * @return mixed
     * @throws \Zend_Exception
     */
    public function save($entity)
    {
        //faz a instancia da classe de log passando a entity e a velha entity que é opcional
        $logSistema = new LogSistema();
        try {
            $this->getem()->persist($entity);

            $logSistema->setEntity($entity);

            $this->getem()->flush();

            if ($entity) {
                //seta a entity apos persistir no banco e chama o método de criar o log
                $logSistema->criarLog();
            }

            return $entity;
        } catch (\PDOException $exception) {
            //gerando log de erros para PDO, Obs: docs/logs/application.log
            $logSistema->escreverErroArquivoLog($exception);
            throw new \Zend_Exception($exception);
        } catch (\Doctrine\DBAL\DBALException $de) {
            //gerando log de erros para DBALException, Obs: docs/logs/application.log
            $logSistema->escreverErroArquivoLog($de);
            throw new \Zend_Exception($de);
        } catch (\Doctrine\ORM\ORMException $e) {
            $logSistema->escreverErroArquivoLog($e);
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Insere ou altera dados na base de dados
     * @param $entity
     * @return \Exception|\PDOException
     * @throws \Zend_Exception
     */
    public function persist($entity)
    {
        $logSistema = new LogSistema();
        try {
            $this->getem()->persist($entity);

            $logSistema->setEntity($entity);

            $this->getem()->flush();

            if ($entity) {
                //seta a entity apos persistir no banco e chama o método de criar o log
                $logSistema->criarLog();
            }

            return $entity;
        } catch (\PDOException $exception) {
            $logSistema->escreverErroArquivoLog($exception);
            return $exception;
        } catch (\Doctrine\ORM\ORMException $e) {
            $logSistema->escreverErroArquivoLog($e);
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            $logSistema->escreverErroArquivoLog($de);
            throw new \Zend_Exception($de->getMessage(), $de->getCode());
        }
    }

    /**
     * @param $entity
     * @return mixed
     * @throws \Zend_Exception
     */
    public function merge($entity)
    {
        $logSistema = new LogSistema();
        try {
            $this->getem()->persist($entity);

            $logSistema->setEntity($entity);

            $this->getem()->merge($entity);
            $this->getem()->flush();

            if ($entity) {
                //seta a entity apos persistir no banco e chama o método de criar o log
                $logSistema->criarLog();
            }

            return $entity;
        } catch (\PDOException $exception) {
            $logSistema->escreverErroArquivoLog($exception);
            throw $exception;
        } catch (\Doctrine\ORM\ORMException $e) {
            $logSistema->escreverErroArquivoLog($e);
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            $logSistema->escreverErroArquivoLog($de);
            throw new \Zend_Exception($de->getMessage(), $de->getCode());
        }
    }

    /**
     * Deleta um ou mais registros da base de dados
     * @param $entity
     * @return mixed
     * @throws \Zend_Exception
     */
    public function delete($entity)
    {
        $logSistema = new LogSistema();
        try {
            $this->getem()->persist($entity);

            $logSistema->setEntity($entity);

            $this->getem()->remove($entity);
            $this->getem()->flush();

            if ($entity) {
                //seta a entity apos persistir no banco e chama o método de criar o log
                $logSistema->criarLog();
            }
            return $entity;
        } catch (\Doctrine\ORM\ORMException $e) {
            $logSistema->escreverErroArquivoLog($e);
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            $logSistema->escreverErroArquivoLog($de);
            throw new \Zend_Exception($de);
        }
    }

    /**
     * @description Retornar o repositório de uma entity
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param string $entity
     * @return \Doctrine\ORM\EntityRepository
     * @throws \Zend_Exception
     */
    public function getRepository($entity)
    {
        try {
            $ref = new \ReflectionClass($entity);
            return $this->getem()->getRepository($ref->getName());
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Converte data para o formato do banco retornando um Zend_Date
     * @param string $data data para ser formatada no formato PT_BR dia/mes/ano
     * @return Zend_Date
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @date 2014-09-23
     * @deprecated
     * @see \G2\Utils\Helper::converterData
     */
    public function converteDataBancoZendDate($data)
    {
        //converte a data
        $dt = $this->converteDataBanco($data);
        //retorna o Zend_Date
        return new \Zend_Date(array(
                'year' => $dt->format('Y'),
                'month' => $dt->format('m'),
                'day' => $dt->format('d')
            )
        );
    }

    /**
     * Função para Converter Data para o Formato do Banco
     * @param string $data
     * @return object $datetime DateTime object
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @deprecated
     * @see \G2\Utils\Helper::converterData
     */
    public function converteDataBanco($data)
    {
        $datetime = new \DateTime();
        if ($data) {

            $data = $this->converterData($data, 'd/m/Y H:i:s');

            $arrDateTime = explode(' ', $data);
            if (array_key_exists(1, $arrDateTime)) {
                $arrTime = explode(':', $arrDateTime[1]);
                $datetime->setTime($arrTime[0], $arrTime[1], $arrTime[2]);
            }
            $arrDate = explode('/', $arrDateTime[0]);
            $datetime->setDate($arrDate[2], $arrDate[1], $arrDate[0]);
            //se tipo = 1 vai colocar a hora como 00:00:00, se 2 vai colocar como 23:59:59
            //utilizada para colocar a data inicial do dia e a data final
        }
        return $datetime;
    }

    /**
     * @deprecated
     * @see \G2\Utils\Helper::converterData
     */
    public function converterData($dt, $type_or_format = 'Y-m-d')
    {
        return \G2\Utils\Helper::converterData($dt, $type_or_format);
    }

    /**
     * @param int $nu_mes
     * @return string
     */
    public function stringMes($nu_mes)
    {
        $meses = array('Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro');
        $nu_mes = (int)$nu_mes - 1;
        return (isset($meses[$nu_mes]) ? $meses[$nu_mes] : '');
    }

    /**
     * De um array ele monta a Entity
     * @param \G2\Entity\* $entity
     * @param array $params
     * @param $noUnderline
     * @return \G2\Entity\*
     */
    public function arrayToEntity($entity, array $params, $noUnderline = false)
    {
        foreach ($params as $key => $value) {
            //Modificado para tratar entities criadas com o padrão camelcase
            $method = $noUnderline ? str_replace('_', '', $key) : $key;
            $method_name = 'set' . $method;

            if (method_exists($entity, $method_name)) {
                $entity->$method_name($value);
            }
        }
        return $entity;
    }

    /**
     * Retorna Referencia
     * @param $repositoryName
     * @param $id
     * @return bool|\Doctrine\Common\Proxy\Proxy|null|object
     * @throws \Zend_Exception
     */
    public function getReference($repositoryName, $id)
    {
        try {
            $ref = new \ReflectionClass($repositoryName);
            return $this->getem()->getReference($ref->getName(), $id);
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $dataInicial
     * @param $dataFinal
     * @return bool
     */
    public function comparaData($dataInicial, $dataFinal)
    {
        $zdataInicial = new \Zend_Date($dataInicial);
        $zdataFinal = new \Zend_Date($dataFinal);
        return $zdataFinal->isLater($zdataInicial);
    }

    /**
     * @param $data
     * @return string
     */
    public function converteDate($data)
    {
        $data_nova = implode(preg_match("~\/~", $data) == 0 ? "/" : "-", array_reverse(explode(preg_match("~\/~", $data) == 0 ? "-" : "/", $data)));
        return $data_nova;
    }

    /**
     * @param $data
     * @return bool|string
     * @deprecated
     * @see \G2\Utils\Helper::converterData
     */
    public function converteDataPt($data)
    {
        $data_nova = date('d/m/Y', strtotime($data));
        return $data_nova;
    }

    /**
     * Metodo responsavel por chamar variavel EM doctrine e para executar BEGIN_TRANSACTION
     * Utilidade: dar suporte ao desenvolvedor devolvendo os metodos de forma visivel
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function beginTransaction()
    {
        try {
            return $this->getem()->beginTransaction();
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            throw new \Zend_Exception($de->getMessage(), $de->getCode());
        }
    }

    /**
     * Metodo responsavel por chamar variavel EM doctrine para executar COMMIT.
     * Utilidade: dar suporte ao desenvolvedor devolvendo os metodos de forma visivel
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function commit()
    {
        try {
            return $this->getem()->commit();
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            throw new \Zend_Exception($de->getMessage(), $de->getCode());
        }
    }

    /**
     * Metodo responsavel por chamar variavel EM doctrine para executar ROLLBACK.
     * Utilidade: dar suporte ao desenvolvedor devolvendo os metodos de forma visivel
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     */
    public function rollback()
    {
        try {
            return $this->getem()->rollback();
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        } catch (\Doctrine\DBAL\DBALException $de) {
            throw new \Zend_Exception($de->getMessage(), $de->getCode());
        }
    }

    /**
     * Metodo que converte TO em Entity
     * @param \Ead1_TO_Dinamico $to
     * @return string
     */
    public function toToEntity(\Ead1_TO_Dinamico $to)
    {
        try {
            $array = $to->toArray();
            $name = '\\G2\Entity\\' . substr(get_class($to), 0, -2);
            $entity = new $name;

            if (!is_object($entity)) {
                throw new \Exception('Não existe esse objeto para conversão');
            }

            if (!is_null($array)) {
                $arPropriedades = get_object_vars($to);
                foreach ($array as $propriedade => $valor) {
                    if (array_key_exists($propriedade, $arPropriedades) !== false) {
                        $metodo = 'set' . $propriedade;
                        if (method_exists($entity, $metodo)) {
                            $entity->$metodo($valor);
                        }
                    }
                }
            }
            return $entity;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $entity
     * @return mixed
     * @throws \Exception
     */
    public function entityToTO($entity)
    {
        try {
            $array = $this->toArrayEntity($entity);
            $name = str_replace('G2\Entity', '', get_class($entity)) . 'TO';
            $to = new $name;
            if (!($to instanceof \Ead1_TO_Dinamico)) {
                throw new \Exception('Não existe esse objeto para conversão');
            }
            if (!is_null($array)) {
                $arPropriedadeTO = get_object_vars($to);
                foreach ($array as $propriedade => $valor) {
                    if (array_key_exists($propriedade, $arPropriedadeTO) !== false) {
                        if (is_array($valor)) {
                            foreach ($valor as $key => $value) {
                                $to->$propriedade = $value;
                            }
                        } else {
                            $to->$propriedade = $valor;
                        }
                    }
                }
            }
            return $to;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Serialize an entity OR an array of entities to an array
     * @param mixed $entity
     * @return array
     * update 04-12-16 by Caio Eduardo
     */
    public function toArrayEntity($entity)
    {

        try {

            if (!$entity) {
                throw new \Exception("Entity não informada");
            }

            // Se for um array de entities, converter cada Entity para array
            if (is_array($entity)) {
                return array_map(array($this, 'toArrayEntity'), $entity);
            }

            $serialize = new EntitySerializer($this->getem());
//            $serialize->setMaxRecursionDepth(1);
            return $serialize->toArray($entity);
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @description Função responsável por limpar o CPF e CNPJ
     * @param $cpf
     * @return mixed
     */
    public function limpaCpfCnpj($cpf)
    {
        $chars = array('.', '-', '/');
        $cpf_final = str_replace($chars, '', $cpf);
        return $cpf_final;
    }

    /**
     * Valida se array tem chaves obrigatorias
     * @see \G2\Negocio\CAtencao.php ::salvarOcorrencia
     * @param $array
     * @param array $keys
     * @return bool|string
     */
    public function validaCamposObrigatorios($array, array $keys)
    {

        $mensagem = false;
        foreach ($keys as $key) {
            if (!array_key_exists($key, $array)) {
                $mensagem .= ' A chave ' . $key . ' é obrigatória e não foi passada. ';
            }
        }
        return $mensagem;
    }

    /**
     * Metodo que soma uma determinada quantidade de dias em uma data
     * @param1 data para soma
     * @param2 dias para serem somados a uma data
     * @param3 meses para serem somados a uma data
     * @param4 anos para serem somados a uma data
     * @return string
     */
    function somarData($data, $dias)
    {
        $zendData = new Zend_date();
        $zendData->toString("dd/MM/yyyy");
        if (!is_array($data)) {
            $zendData->set($data);
        } else {
            $zendData->set($data['date']);
        }
        $zendData->addDay($dias);

        return $zendData;
    }

    /*
     public function toArrayEntity($entity)
     {
         $serialize = new EntitySerializer($this->em);
         $serialize->getMaxRecursionDepth(1);
         return $serialize->toArray($entity);
     }*/

    /**
     * @deprecated since 2017-06-23 - use findCustom instead.
     * @autor Caio Eduardio <caio.teixeira@unyleya.com.br>
     * @update Denise Xavier - 2017-05-22
     * @date 2015-07-03
     * @description - Traz os registros usando o WHERE como LIKE
     * Aceita strings como parametro (incluindo NULL ou NOT NULL)
     * @param array $whereLike
     * @param array $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @param array $whereNull
     * @return array
     */
    public function findBySearch($repositoryName, $whereLike = array(), $where = array(), $order = array(), $limit = null, $offset = 0, $whereNull = array())
    {
        if ($whereLike || $whereNull) {
            $repository = $this->getem()->getRepository($repositoryName);
            $query = $repository->createQueryBuilder("tb")
                ->select('tb')
                ->where('1 = 1');


            if ($where) {

                foreach ($where as $attr => $value) {
                    if (is_array($value)) {
                        $query->andWhere("tb.{$attr} IN (" . implode(',', $value) . ")");
                    } else {
                        $query->andWhere("tb.{$attr} = '{$value}'");
                    }
                }
            }


            if ($whereLike) {

                foreach ($whereLike as $attr => $value) {
                    $query->andWhere("tb.{$attr} LIKE '%{$value}%'");
                }
            }

            if ($whereNull) {
                foreach ($whereNull as $attr => $value) {
                    $query->andWhere("tb.{$attr} {$value}");
                }
            }

            if ($order) {
                $andOrder = false;
                foreach ($order as $attr => $sort) {
                    if ($andOrder)
                        $query->addOrderBy("tb.{$attr}", $sort);
                    else {
                        $query->orderBy("tb.{$attr}", $sort);
                        $andOrder = true;
                    }
                }
            }

            if ($limit) {
                $query->setFirstResult($offset);
                $query->setMaxResults($limit);
            }

            return $query->getQuery()->getResult();
        } else {
            return $this->findBy($repositoryName, $where, $order, $limit, $offset);
        }
    }

    /**
     * @description Retornar vários registros de acordo com os filtros passados
     * @param string $repositoryName
     * @param array $where
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws \Zend_Exception
     */
    public function findBy($repositoryName, array $where, array $order = null, $limit = null, $offset = null)
    {
        try {
            $ref = new \ReflectionClass($repositoryName);
            return $this->getem()->getRepository($ref->getName())->findBy($where, $order, $limit, $offset);
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Método que busca na Entity paginando
     * @param object $repositoryName -- Nome da Entity
     * @param array $array - fltro baseado na tabela
     * @param null $order - campo para ordenação
     * @param int $page - página
     * @param int $per_page - quantidade de registros por página
     * @param bool|true $returnEntity
     * @return Pagination
     * @throws \Exception
     */
    public function paging($repositoryName, array $array, $order = null, $page = 1, $per_page = 25, $returnEntity = true)
    {
        try {
            $result = array();

            $qb = $this->getEm()->getRepository($repositoryName)->createQueryBuilder('o')->where('1=1');
            $qbc = $this->getEm()->createQueryBuilder()->select($qb->expr()->count('o'))->from($repositoryName, 'o')->where('1=1');
            if ($array) {
                foreach ($array as $chave => $valor) {
                    switch (substr($chave, 0, 2)) {
                        case "st":
                            $qb->andWhere('o.' . $chave . ' like :' . $chave);
                            $qb->setParameter($chave, '%' . $valor . '%');
                            $qbc->andWhere('o.' . $chave . ' like :' . $chave);
                            $qbc->setParameter($chave, '%' . $valor . '%');
                            break;
                        default:
                            if ($valor === null) {
                                $qb->andWhere('o.' . $chave . ' is null');
                                $qbc->andWhere('o.' . $chave . ' is null');
                            } else {
                                $qb->andWhere('o.' . $chave . ' = :' . $chave);
                                $qb->setParameter($chave, $valor);
                                $qbc->andWhere('o.' . $chave . ' = :' . $chave);
                                $qbc->setParameter($chave, $valor);
                            }
                    }

                }
            }

            $paginated = new Pagination();

            $count = $qbc->getQuery()->getSingleScalarResult();
            if (!$count)
                return $paginated;

            if ($order) {
                $qb->orderBy('o.' . $order[0], $order[1]);
            }

            $offset = ($page * $per_page) - $per_page;

            if ($per_page)
                $qb->setMaxResults($per_page);

            if ($offset)
                $qb->setFirstResult($offset);

            if (!$returnEntity) {
                $temp = $qb->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
            } else {
                $temp = $qb->getQuery()->getResult();
            }

            if ($temp) {
                foreach ($temp as $entity) {
                    /**
                     * Caso queira tratar algum registro antes
                     */
                    $result[] = $entity;
                }
            }
            $paginated->setdata($result);
            $paginated->setpage($page);
            $paginated->setperPage($per_page);
            $paginated->settotal($count);
            $paginated->settotalPages(ceil(($count / $per_page)));
            $paginated->settotalThisPage(count($result));

            return $paginated;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getem()
    {

        if (!$this->em || !$this->em->isOpen()) {
            $this->bootstrap = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $this->em = $this->bootstrap->getResource('doctrine')->getEntityManager();
        }

        //        if(APPLICATION_ENV=='desenvolvimento')
        //        $this->em->getConnection()->getConfiguration()->setSQLLogger(new FirebugDoctrineLogger());

        return $this->em;

    }


    /**
     * @return bool
     */
    public function dataBaseStatus()
    {
        $platform = $this->getem()->getConnection()->getDatabasePlatform();
        return $this->getem()->getConnection()->ping();
    }

    /**
     * Verifica se um ano é bissexto
     * Retorna TRUE se for bissexto e False se não for
     * @param $ano
     * @return bool
     */
    public function isBissexto($ano)
    {
        if ((($ano % 4) == 0 && ($ano % 100) != 0) || ($ano % 400) == 0)
            return true;
        else
            return false;
    }

    /**
     * Recebe array multidimensional que tenham valores repetidos e limpa retornando o array
     *
     * @param $array
     * @param $indice
     * @return mixed
     */
    function limparArrayDuplicados($array, $indice)
    {
        //remove dados duplicados
        $return = array_intersect_key($array, array_unique(array_column($array, $indice)));
        //limpa e ordena o array
        array_multisort($return);

        return $return;
    }

    /**
     * Invoca metodo para criacao de job no zend server.
     *
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     * @param $url /robo/auto-alocar-alunos
     *
     * @param array $post
     * @return mixed
     * @throws \Exception
     * @throws \Zend_Exception
     *
     * @param array $options
     * "name" - The job name
     * "priority" - The job priority (see PRIORITY_* constants)
     * "persistent" - The persistence flag
     * "predecessor" - The job predecessor
     * "http_headers" - The additional HTTP headers for HTTP jobs
     * "schedule" - The CRON-like schedule command
     * "schedule_time" - The time when the job execution was scheduled
     * "queue_name" - The queue name
     * "validate_ssl" - Boolean (validate ssl certificate")
     * "job_timeout" - The timeout for the job
     */
    public function createHttpJob($url, array $post = array(), array $options = array(), $sistema = NULL)
    {
        try {

            $zendJobQueue = new \ZendJobQueue();

            if (!array_key_exists('name', $options)) {
                throw new \Zend_Exception('Informe o nome para o job');
            } else {
                if ($sistema && \G2\Constante\Sistema::arraySistemas($sistema)) {
                    $options['name'] = \G2\Constante\Sistema::arraySistemas($sistema) . ': ' . $options['name'];
                }
            }

            if (!array_key_exists('schedule_time', $options)) {

                //time default para um job criado e nao determinado o tempo para a execucao
                $options['schedule_time'] = date('Y-m-d H:i:s', time() + 10);
            }

            return $zendJobQueue->createHttpJob($url, $post, $options);

        } catch (\Zend_Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna parametros do job corrente em execucao.
     *
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     * @return mixed
     */
    public function getCurrentJobParams()
    {
        return \ZendJobQueue::getCurrentJobParams();
    }

    /**
     * Retorna informacoes de um job especifico
     *
     * @author Rafael Bruno <rafael.oliveira@unyleya.com.br>
     * @param $key
     * @return mixed
     */
    public function getJobInfo($key)
    {
        $zendJobQueue = new \ZendJobQueue();
        return $zendJobQueue->getJobInfo($key);
    }

    /**
     * @description Gerar um array de DateTime com datas que mantem o intervalo de 1 mês, SEMPRE mantendo a ordem dos meses, geralmente usando para parcelas de venda. Exemplo: 31/01/2017 - 28/02/2017 - 31/03/2017
     * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
     * @param string|\DateTime|\Zend_Date $dt_base
     * @param int $qtd_meses
     * @return \DateTime[]
     */
    public function gerarDatasMensais($dt_base, $qtd_meses = 6)
    {
        $ar_datas = array();

        // Garantir $dt_base como \DateTime
        $dt_base = \G2\Utils\Helper::converterData($dt_base, 'DateTime');

        // Registra o mes base em uma variável
        $mes_base = $dt_base->format('m');

        for ($i = 0; $i < $qtd_meses; $i++) {
            // Descobrir o mês
            $mes_atual = ($mes_base + $i) % 12;

            // Se o mês for ZERO, então o mês é 12 (O resto de 12 dividido por 12 é ZERO)
            if (!$mes_atual) {
                $mes_atual = 12;
            }

            // Adiciona a quantidade $i de meses à próxima data
            $dt_new = new \DateTime($dt_base->format('Y-m-d H:i:s'));
            $dt_new->add(new \DateInterval("P{$i}M"));

            // Esta variável será usada para que o while não entre em um loop infinito caso ocorra algum problema com a data
            // Por padrão 3 porque é o total da diferença do mês com mais dias (31) para o com menos (28)
            $tentativas_restantes = 3;

            // Se ao adicionar o mês a nova data tiver virado de mês, então diminui 1 dia até voltar ao mês correto
            while ($tentativas_restantes && $dt_new->format('n') != $mes_atual) {
                $dt_new->sub(new \DateInterval("P1D"));
                $tentativas_restantes--;
            }

            // Adiciona a nova data no array
            $ar_datas[] = $dt_new;
        }

        return $ar_datas;
    }

    /**
     * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
     * @param $respositoryName
     * @param $id_attribute
     * @param $data
     * @param array $defaults
     * @return NoResultException|\Exception|null|object
     */
    public function preencherEntity($respositoryName, $id_attribute, $data, $defaults = array())
    {
        if ($id_attribute && isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($respositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $respositoryName;
        }

        // Nao permite campos NULLS que estejam no DEFAULTS
        foreach ($data as $attr => $val) {
            if (array_key_exists($attr, $defaults) && $val === null) {
                $data[$attr] = $defaults[$attr];
            }
        }

        $data = array_merge($defaults, $data);

        $this->entitySerialize->arrayToEntity($data, $entity);

        return $entity;
    }

    /**
     * Retorna todos os registros de uma determinada Entity
     * @param $repositoryName
     * @param $id
     * @return NoResultException|\Exception|null|object
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function find($repositoryName, $id)
    {
        try {
            if (!$id) {
                throw new \Exception(\G2\Constante\MensagemSistema::INFORME_PARAMETRO_ID . " - $repositoryName");
            }

            $ref = new \ReflectionClass($repositoryName);
            return $this->getem()->getRepository($ref->getName())->find($id);
        } catch (NoResultException $exception) {
            return $exception;
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Retorna dados aceitando parametros especificos para a busca
     * @link http://confluence.unyleya.com.br/display/DEV/FindCustom+-+FindBy+customizado
     * @author Denise Xavier <denise.xavier07@gmail.com>
     * @author Caio Eduardo <caioedut@gmail.com>
     * @param string $repositoryName
     * @param array $where
     * @param array $customWhere (IMPORTANTE usar o prefixo "tb." antes de qualquer campo)
     * @param array $order
     * @param int $limit
     * @param int $offset
     * @return array|null
     */
    public function findCustom($repositoryName, $where = array(), $customWhere = array(), $order = array(), $limit = null, $offset = 0)
    {
        $repository = $this->getem()->getRepository($repositoryName);
        $query = $repository->createQueryBuilder("tb")
            ->select('tb');

        foreach ($where as $attr => $val) {
            $val = trim($val);
            $rules = array('=', '!=', '<', '<=', '>', '>=', 'IN', 'NOT', 'IS', 'LIKE', 'BETWEEN');
            $bl_comparator = false;
            foreach ($rules as $rule) {
                if (strtoupper(substr($val, 0, strlen($rule))) == $rule) {
                    $bl_comparator = true;
                }
            }
            if (!$bl_comparator) {
                $val = "= '{$val}'";
            }
            $query->andWhere("tb.{$attr} {$val}");
        }
        if ($customWhere) {
            foreach ($customWhere as $item) {
                if ($item)
                    $query->andWhere($item);
            }
        }
        if ($order) {
            $andOrder = false;
            foreach ($order as $attr => $sort) {
                if ($andOrder)
                    $query->addOrderBy("tb.{$attr}", $sort);
                else {
                    $query->orderBy("tb.{$attr}", $sort);
                    $andOrder = true;
                }
            }
        }
        if ($limit) {
            $query->setFirstResult($offset);
            $query->setMaxResults($limit);
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Serialize an entity OR an array of entities to an array
     * @param mixed $entity
     * @param callable $arr_map_fn
     * @return array
     * @date 18/05/2017 - Denise Xavier
     */
    public function _toArrayEntityFunction($entity, $arr_map_fn = null)
    {
        // Se for um array de entities, converter cada Entity para array
        if (is_array($entity)) {
            return array_map(function ($item) use ($arr_map_fn) {
                return $this->toArrayEntity($item, $arr_map_fn);
            }, $entity);
        }
        $serialize = new EntitySerializer($this->em);
//        $serialize->setMaxRecursionDepth(1);
        $response = $serialize->toArray($entity);

        // Se for uma funcao, realiza o callback da função
        if (is_callable($arr_map_fn)) {
            $response = $arr_map_fn($response);
        }
        return $response;
    }

    /**
     * Retorna numero com máscara
     *
     * @author Rafael Leite <rafael.leite@unyleya.com.br>
     * @param $val
     * @param $mask
     *  exemplos de mask
     *      -cnpj = ##.###.###/####-##
     *      -cpf = ###.###.###-##
     *      -cep = #####-###
     *      -data = ##/##/####
     * @return mixed
     */
    public function mask($val, $mask)
    {

        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * Remove os emojis do texto
     *
     * @param $texto
     * @return string
     */
    public function limpaEmojiTexto($texto)
    {
        // Match Emoticons
        return preg_replace('/[\x{1F600}-\x{1F64F}]/u', '', $texto);
    }
}

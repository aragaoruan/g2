<?php

namespace G2\Negocio;

/**
 * Classe de negocio para Mensagem Cobrança
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class MensagemCobranca extends Negocio
{

    /**
     *
     * @var G2\Entity\MensagemCobranca
     */
    private $repositoryName = "\G2\Entity\MensagemCobranca";

    public function save($data)
    {
        try {

            $entity = new $this->repositoryName;

            if (isset($data['id_mensagemcobranca']) && !empty($data['id_mensagemcobranca'])) {
                $entity = $this->find('G2\Entity\MensagemCobranca', $data['id_mensagemcobranca']);
                if (!$entity) {
                    throw new \Exception("Mensagem cobrança não encontrada.");
                }
            }

            //bl_antecedencia setado na tela como select
            $entity->setBl_antecedencia($data['bl_antecedencia']);
            $entity->setBl_ativo($data['bl_ativo']);
            $entity->setId_entidade($this->getReference('G2\Entity\Entidade', $data['id_entidade'] ? $data['id_entidade'] : $this->getId_entidade()));
            $entity->setId_textosistema($this->getReference('G2\Entity\TextoSistema', $data['id_textosistema']));
            $entity->setNu_diasatraso($data['nu_diasatraso']);

            if (!$entity->getId_mensagemcobranca())
                $entity->setDt_cadastro(new \DateTime());

            return parent::save($entity);
        } catch (\PDOException $exception) {
            throw $exception;
            return $exception;
        }
    }

    public function delete($id)
    {
        try {
            $entity = $this->em->getRepository('G2\Entity\MensagemCobranca')->find($id);
            $this->em->remove($entity);
            $this->em->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function registrarMensagensCobranca($dados = array())
    {
        ini_set('display_errors', 1);

        $mensagemCobrancaoDAO = new \MensagemCobrancaDAO();
        $params = array();

        if (!empty($dados['id_lancamento'])) {
            $params['id_lancamento'] = $dados['id_lancamento'];
        }

        //$vwsAvisoBoleto = $this->findAll('\G2\Entity\VwAvisoBoleto');// $negocio->getRepository('\G2\Entity\VwAvisoBoleto')->findBy(array());
        $vwsAvisoBoleto = $this->findBy('\G2\Entity\VwAvisoBoleto', $params, null,600,null);

        echo 'Iniciando envio de cobranças.<br/>';
        foreach ($vwsAvisoBoleto as $vw) {
            try {

                $this->beginTransaction();

                $mensagemBO = new \MensagemBO();

                $mensageiro = $mensagemBO->enviarNotificacaoMensagemCobranca($vw);

                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception('Erro ao salvar mensagem para o lançamento ' . $vw->getId_lancamento()->getIdLancamento() . '.', '', '');
                }

                echo 'Mensagem para o lançamento ' . $vw->getId_lancamento()->getIdLancamento() . ' criada com sucesso. Aguardando o envio.</br>';
                $this->commit();
            } catch (\Exception $e) {
                $this->rollback();
                echo 'Erro lançamento '.$vw->getId_lancamento()->getIdLancamento().': ' . $e->getMessage() . '<br/>';
            }
        }
        echo 'Finalizando envio de cobranças.<br/>';
    }

}
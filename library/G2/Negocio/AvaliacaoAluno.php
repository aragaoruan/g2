<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Avaliação Aluno (lançamento de nota para provas presenciais - final e recuperação)
 * @author Débora Castro <deboracastro.pm@gmail.com>
 */
class AvaliacaoAluno extends Negocio
{

    private $repositorio = 'G2\Entity\AvaliacaoAluno';

    /**
     * Retorna pesquisa dos alunos para o lançamento de nota por aluno
     * @param array $params
     * @return bool
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisaAlunoLancamento(array $params = array())
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAlunosLancamentoNota');
            $array_search = array('vw.bl_provaglobal', 'vw.id_tipodeavaliacao', 'vw.id_avaliacaoagendamento', 'vw.id_matricula', 'vw.st_nomecompleto', 'vw.st_projetopedagogico', 'vw.id_projetopedagogico', 'vw.st_evolucao', 'vw.st_situacao', 'vw.st_areaconhecimento', 'vw.id_aplicadorprova', 'vw.st_aplicadorprova', 'vw.st_aplicadorprova', 'vw.dt_aplicacao', 'vw.st_email', 'vw.st_cpf', 'vw.sg_uf', 'vw.st_situacao', 'vw.st_evolucao', 'vw.nu_valor');
            if (isset($params['id_aplicadorprova']) && !empty($params['id_aplicadorprova']) && isset($params['dt_aplicacao']) && !empty($params['dt_aplicacao'])) {
                $array_search[] = 'vw.id_avaliacaoagendamento';
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->where('1=1')
                ->orderBy('vw.st_nomecompleto');

            $where = array();

            if (isset($params['st_str']) && !empty($params['st_str'])) {
                $where[] = "vw.st_nomecompleto LIKE '%" . $params['st_str'] . "%'";
                $where[] = "vw.st_cpf LIKE '%" . $params['st_str'] . "%'";
                $where[] = "vw.st_email LIKE '%" . $params['st_str'] . "%'";
                $query->andWhere(implode(" OR ", $where));
            }

            if (isset($params['sg_uf']) && !empty($params['sg_uf'])) {
                $query->andWhere('vw.sg_uf = :sg_uf')
                    ->setParameter('sg_uf', $params['sg_uf']);
            }

            if (isset($params['id_aplicadorprova']) && !empty($params['id_aplicadorprova'])) {
                if (isset($params['dt_aplicacao']) && !empty($params['dt_aplicacao'])):
                    $dataEntrada = new \Zend_Date(new \Zend_Date($params['dt_aplicacao']), \Zend_Date::ISO_8601);
                    $dtEntrada = $dataEntrada->toString('yyyy-MM-dd');

                    $query->andWhere('vw.id_aplicadorprova = :id_aplicadorprova')
                        ->setParameter('id_aplicadorprova', $params['id_aplicadorprova']);
                    $query->andWhere("vw.dt_aplicacao like '" . $dtEntrada . "'");
                    $query->andWhere("vw.id_avaliacaoagendamento IS NOT NULL");
                else:
                    return new \Ead1_Mensageiro('Para pesquisar pelo aplicador é necessário selecionar a data!', \Ead1_IMensageiro::AVISO);
                endif;
            }

            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);
            $result = $query->getQuery()->getResult();
            if (count($result) > 0) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum aluno encontrado', \Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    public function findByAlunoAvaliacao($params = null)
    {
        try {
            $params['id_tipoavaliacao'] = 4;
            $repositoryName = 'G2\Entity\VwAvaliacaoAluno';
            return $this->findBy($repositoryName, $params, array('st_tituloexibicaodisciplina' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function findByHistoricoAlunoAvaliacao(array $params)
    {
        try {
            $repositoryName = 'G2\Entity\VwHistoricoAvaliacaoAluno';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function findByProvasCadastradas(array $params)
    {
        try {
            $params['id_situacao'] = 68;
            $params['nu_presenca'] = 1;
            $repositoryName = 'G2\Entity\VwAvaliacaoAgendamento';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function findVwMatriculaNota(array $params)
    {
        $params['id_tipoavaliacao'] = 4;
        $result = $this->findBy('G2\Entity\VwMatriculaNota', $params, array('st_disciplinafinal' => 'ASC'));
        return $result;
    }

    /**
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvarNotaAvaliacaoPresencial(array $data, $verificarConcluite = true)
    {
        try {
            $params['id_matricula'] = $data['id_matricula'];
            $params['id_avaliacao'] = $data['id_avaliacao'];

            if (isset($data['id_avaliacaoconjuntoreferencia']) && !empty($data['id_avaliacaoconjuntoreferencia'])) {
                $params['id_avaliacaoconjuntoreferencia'] = $data['id_avaliacaoconjuntoreferencia'];
            }

            $dadosAluno = $this->findByAlunoAvaliacao($params);
            $arrAvaliacaoAlunos = array();
            $bo = new \AvaliacaoBO();
            foreach ($dadosAluno as $row) {
                $to = new \AvaliacaoAlunoTO();
                $to->setId_avaliacao($data['id_avaliacao']);
                $to->setId_matricula($data['id_matricula']);
                $to->setSt_nota($data['st_nota']);
                $to->setId_avaliacaoconjuntoreferencia($row->getId_avaliacaoconjuntoreferencia());
                $to->setDt_avaliacao($row->getDt_avaliacao());
                $arrAvaliacaoAlunos[] = $to;
            }
            $retorno = $bo->salvarAvaliacoesAluno($arrAvaliacaoAlunos, $verificarConcluite);
            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Notas cadastradas com sucesso!', \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro($retorno->getMensagem(), \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @param array $params
     * @return array
     */
    public function findVwAvaliacaoAlunoDefesaTcc(array $params = array())
    {
        $query = $this->em->getRepository('G2\Entity\VwAvaliacaoAluno')
            ->createQueryBuilder('vw')
            ->select()
            ->andWhere('vw.bl_ativo = :bl_ativo')
            ->setParameter('bl_ativo', TRUE)
            ->andWhere('vw.id_tipodisciplina = 2')
            ->andWhere('vw.id_avaliacaoaluno is not null');

        if ($params['id_projetopedagogico']) {
            $query->andWhere('vw.id_projetopedagogico = :id_projetopedagogico')
                ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
        }

        if (array_key_exists('id_situacao', $params)) {
            if ($params['id_situacao'] == TRUE) {
                $query->andWhere('vw.dt_defesa is not null');
            } else {
                $query->andWhere('vw.dt_defesa is null');
            }
        }

        if ($params['st_nomecompleto']) {
            $query->andWhere("vw.st_nomecompleto LIKE :st_nomecompleto")
                ->setParameter('st_nomecompleto', '%' . $params['st_nomecompleto'] . '%');
        }

        return $query->getQuery()->getResult();
    }

    public function salvarDefesaTcc(array $data)
    {
        $avaliacaoBO = new \AvaliacaoBO();
        $arrAvaliacoes = array();

        foreach ($data as $value) {
            if ($value['dt_cadastrotcc']) {
                $to = new \AvaliacaoAlunoTO();
                $to->setId_matricula((int)$value['id_matricula']);
                $to->setId_avaliacaoconjuntoreferencia((int)$value['id_avaliacaoconjuntoreferencia']);
                $to->setId_avaliacaoaluno($value['id_avaliacaoaluno']);
                $to->setDt_defesa($value['dt_defesa']);
                $to->setId_avaliacao($value['id_avaliacao']);
                array_push($arrAvaliacoes, $to);
            }
        }

        return $avaliacaoBO->entregarDefesasTCC($arrAvaliacoes);
    }

    /**
     * GII-9268
     * Função responsável por enviar um e-mail para o aluno quando sua nota for lançada
     *
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function notificarAluno($idMatricula, $idEntidade = null)
    {
        $idEntidade ? $entidade = $idEntidade : $entidade = $this->sessao->id_entidade;

        $emailMsgEntity = $this->findOneBy(\G2\Entity\EmailEntidadeMensagem::class, array(
            'id_mensagempadrao' => \G2\Constante\MensagemPadrao::LANCAMENTO_NOTA_PROVA_FINAL_POS,
            'id_entidade' => $entidade,
            'bl_ativo' => 1
        ));

        if (!$emailMsgEntity) {
            return $mensageiro = new \Ead1_Mensageiro("Não encontramos uma mensagem padrão cadastrada no sistema.",
                \Ead1_IMensageiro::ERRO);
        }

        $textoSistema = $this->findOneBy(\G2\Entity\TextoSistema::class, array(
            'id_textosistema' => $emailMsgEntity->getId_textosistema()->getId_textosistema()
        ));

        if (!$textoSistema) {
            return $mensageiro = new \Ead1_Mensageiro("Não encontramos o texto de sistema cadastrado no sistema.",
                \Ead1_IMensageiro::ERRO);
        }

        $aluno = $this->findOneBy(\G2\Entity\Matricula::class, array(
            'id_matricula' => $idMatricula
        ));

        if (!$aluno) {
            return $mensageiro = new \Ead1_Mensageiro("Não encontramos aluno cadastrado no sistema.",
                \Ead1_IMensageiro::ERRO);
        }

        $textoSistemaTO = new \TextoSistemaTO();
        $textoSistemaTO->montaToDinamico($this->toArrayEntity($textoSistema));

        $mensagemBO = new \MensagemBO();

        return $mensagemBO->enviarNotificacaoTextoSistema(
            $textoSistemaTO,
            array('id_matricula' => $idMatricula),
            $emailMsgEntity->getId_emailconfig()->getId_emailconfig(),
            $aluno->getId_usuario()->getId_usuario(),
            $textoSistema->getId_entidade()->getId_entidade(),
            true
        );

    }
}

<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 25-03-2015
 * Time: 15:15
 */

namespace G2\Negocio;


use Doctrine\Common\Util\Debug;
use Doctrine\ORM\ORMException;

class VwAtendente extends Negocio
{
    private $repositoryName;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = 'G2\Entity\VwAtendente';
    }

    public function findByAtendenteId($id)
    {
        try {
            $result = $this->findBy($this->repositoryName, array('id_nucleofuncionariotm' => $id));
            if ($result) {
                $arrReturn = $this->entitySerialize($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function retornarAtendentes(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            //pega os parametros e remove algumas chaves que não precisaria
            unset($params['to'], $params['txtSearch'], $params['grid']);

            if (!isset($params['id_nucleotm'])) {
                $params['id_entidadematriz'] = $this->sessao->id_entidade;
            }

            $result = $this->em->getRepository('\G2\Entity\VwAtendente')
                ->retornarAtendente($params, $orderBy, $limit, $offset);

            $retorno = array();
            if (!empty($result) && $result->rows) {
                foreach ($result->rows as $key => $values) {
                    $retorno[$key]['id_nucleofuncionariotm'] = $values->getId_nucleofuncionariotm();
                    $retorno[$key]['st_nomecompleto'] = $values->getSt_nomecompleto();
                    $retorno[$key]['st_situacao'] = $values->getSt_situacao();
                    $retorno[$key]['st_nucleotm'] = $values->getSt_nucleotm();
                }

                $result->rows = $retorno;
            }

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Salva Atendente
     */
    public function salvarAtendente(array $data)
    {
        try {
            $id_usuariocadastro = \Ead1_Sessao::getSessaoGeral()->id_usuario;

            /* Pesquisa se o usuário já exite no banco*/
            $verifica = $this->findBy($this->repositoryName, array('id_usuario' => $data['usuario']['id_usuario']));

            /* Verifica se o usuário já foi cadastrado naquele nucleo e o elemina do array de cadastro */
            if (isset($data['ids_nucleotm'])) {
                foreach ($verifica as $value) {
                    foreach ($data['ids_nucleotm'] as $key => $nucleo) {
                        if ($nucleo == $value->getId_nucleotm()) {
                            unset($data['ids_nucleotm'][$key]);
                        }
                    }
                }
            }

            $result = array();
            $this->beginTransaction(); //abre transação
            /* Verifica se é uma alteração ou cadastro*/
            if (isset($data['ids_nucleotm'])) {
                /* Faz um laço em cima de todos os nucleos recebidos e inserindo no banco*/
                foreach ($data['ids_nucleotm'] as $value) {
                    $entity = new \G2\Entity\NucleoFuncionarioTm();
                    $entity->setId_funcao(1);
                    $entity->setId_usuario($data['usuario']['id_usuario']);
                    $entity->setDt_cadastro(new \DateTime());
                    $entity->setId_nucleotm($value);
                    $entity->setBl_ativo(true);
                    $entity->setId_usuariocadastro($id_usuariocadastro);
                    $entity->setId_situacao($data['id_situacao']);
                    $result[] = $this->save($entity);
                }
            } else {
                $edit = $this->findOneBy('G2\Entity\NucleoFuncionarioTm', array('id_nucleofuncionariotm' => $data['id_nucleofuncionariotm']));
                $edit->setId_nucleofuncionariotm($data['id_nucleofuncionariotm']);
                $edit->setId_situacao($data['id_situacao']);
                $result = $this->save($edit);
            }
            $this->commit(); //commit

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /*
     * Busca nucleos cadastradas para aquele usuário
     */
    public function pegaNucleosUsuario($param)
    {
        try {
            if ($param['id']) {
                unset($param['id']);
            }
            $result = $this->findBy($this->repositoryName, array('id_nucleofuncionariotm' => $param));
            if ($result) {
                $arrReturn = $this->entitySerialize($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function findByAtendentePorEntidade()
    {
        try {
            $return = $this->findBy('\G2\Entity\VwAtendente',
                array('id_entidade' => $this->sessao->id_entidade, 'bl_ativo' => 1),
                array('st_nomecompleto' => 'ASC')
            );
            $mensageiro = new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}

<?php

namespace G2\Negocio;

class Notas extends Negocio {

    private $repositoryName = 'G2\Entity\VwGradeNota';

    /**
     * Função responsável por buscar na VwGradeNota apenas os campos necessários para
     * o preenchimento da tela de notas no aplicativo do portal.
     *
     * @param $idMatricula
     * @return array
     */
    public function getNotasFormatoApp($idMatricula){

        $repo = $this->em->getRepository($this->repositoryName);
        $array_search = array('gn.st_tituloexibicaodisciplina', 'gn.st_notafinal', 'gn.st_notaead', 'gn.st_notatcc', 'gn.nu_notafinal', 'gn.st_status');

        $query = $repo->createQueryBuilder("gn")
            ->select($array_search)
            ->where('gn.id_matricula = '.$idMatricula);

        return $results = $query->getQuery()->getResult();
    }
}
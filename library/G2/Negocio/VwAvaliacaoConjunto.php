<?php

namespace G2\Negocio;

/**
 */
class VwAvaliacaoConjunto extends Negocio {

    private $repositoryName = 'G2\Entity\VwAvaliacaoConjunto';

    public function __construct() {
        parent::__construct();
    }

    public function findByVw(array $where = NULL, array $order = NULL) {
//        \Zend_Debug::dump($where);die;
        $result = parent::findBy($this->repositoryName,$where,$order);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

 public function findVwAvaliacaoConjuntoReferencia(array $where = NULL, array $order = NULL) {
//        \Zend_Debug::dump($where);die;

     $result = parent::findBy('G2\Entity\VwAvaliacaoConjuntoReferencia',$where,$order);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Nucleo de Telemarketing
 *
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class NucleoTelemarketing extends Negocio
{

    private $repositoryName = '\G2\Entity\NucleoTelemarketing';

    /**
     * Metodo responsavel por gravar os nucleos de telemarketing
     * @param $dadosNucleoTelemarketing
     * @return \Ead1_Mensageiro
     */
    public function salvaNucleoTelemarketing($dadosNucleoTelemarketing)
    {
        $mensageiro = new \Ead1_Mensageiro();

        try {

            // Pesquisar para verificar se o registro tb_nucleotelemarketing existe
            $entityNucleoTelemarketing = $this->find($this->repositoryName, $dadosNucleoTelemarketing['id_nucleotelemarketing']);

            if (!$entityNucleoTelemarketing instanceof \G2\Entity\NucleoTelemarketing) {
                $entityNucleoTelemarketing = new \G2\Entity\NucleoTelemarketing();
            }

            if (empty($dadosNucleoTelemarketing['bl_ativo'])) {
                $dadosNucleoTelemarketing['bl_ativo'] = 0;
            }

            $this->entitySerialize->arrayToEntity($dadosNucleoTelemarketing, $entityNucleoTelemarketing);

            if ($entityNucleoTelemarketing instanceof \G2\Entity\NucleoTelemarketing) {
                //Atualiza o nucleo de telemarketing existente
                $entityNucleoTelemarketing = $this->merge($entityNucleoTelemarketing);
            } else {
                //Salva un novo nucleo de telemarketing
                $entityNucleoTelemarketing = $this->persist($entityNucleoTelemarketing);
            }

            $mensageiro->setTipo(\Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Registro salvo com sucesso');
        } catch (\Exception $ex) {
            $mensageiro->setTipo(\Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Erro ao salvar o registro');
        }
        return $mensageiro->setMensagem($dadosNucleoTelemarketing);
    }
}
<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Constante\Sistema;
use G2\Entity\AcordoPagarme;
use G2\Negocio\Negocio;

use Doctrine\ORM\Mapping\ClassMetadata,
    Doctrine\Common\Util\Inflector,
    Doctrine\ORM\EntityManager,
    Exception,
    Ead1\Financeiro\Fluxus\Integracao as Integracao,
    G2\Negocio\Erro as ErroNegocio;
use G2\Utils\Helper;

class TotvsVwIntegraGestorFluxus extends Negocio
{
    const PARAM_STR_INVALID_PARAMETER = 'PARAMETROS INVALIDOS';

    private $repositoryName = 'G2\Entity\TotvsVwIntegraGestorFluxus';
    /** @var \Ead1\Doctrine\EntitySerializer $serialize */
    private $serialize;
    /** @var Integracao $ws */
    private $ws;

    public function __construct (){
        parent::__construct();

        $this->criarPastaLog();
        $this->serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
        $this->ws = new Integracao();
    }

    public function integrar(array $params){

        try{
            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $ws = new Integracao();
            $vw = $serialize->arrayToEntity($params, new \G2\Entity\TotvsVwIntegraGestorFluxus());
            $lancamentos = $this->getRepository($this->repositoryName)->getLancamentosVendaIntegracao($vw);

            $arrayRetorno = array();
            $arrayRetornoErro = array();
            if (!empty($lancamentos)){
                foreach ($lancamentos as $lancamento){
                    $this->beginTransaction();
                    try{
                        $arrayLancamento = $this->toArrayEntity($lancamento);

                        if (!empty($arrayLancamento['dataemissao'])) {
                            $dt = new \DateTime($arrayLancamento['dataemissao']['date']);
                            $arrayLancamento['dataemissao'] = $dt->format('Y-m-d H:i:s');
                        }

                        if (!empty($arrayLancamento['databaixa'])) {
                            $dt = new \DateTime($arrayLancamento['databaixa']['date']);
                            $arrayLancamento['databaixa'] = $dt->format('Y-m-d H:i:s');
                        }

                        if (!empty($arrayLancamento['datavencimento'])) {
                            $dt = new \DateTime($arrayLancamento['datavencimento']['date']);
                            $arrayLancamento['datavencimento'] = $dt->format('Y-m-d H:i:s');
                        }

                        if (!empty($arrayLancamento['dataop2'])) {
                            $dt = new \DateTime($arrayLancamento['dataop2']['date']);
                            $arrayLancamento['dataop2'] = $dt->format('Y-m-d H:i:s');
                        }

                        //retira os caracteres especiais dos nomes
                        if(!empty($arrayLancamento['nomefantasia_fcfo'])){
                            $arrayLancamento['nomefantasia_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['nomefantasia_fcfo'])));
                        }

                        if(!empty($arrayLancamento['nome_fcfo'])){
                            $arrayLancamento['nome_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['nome_fcfo'])));
                        }

                        if(!empty($arrayLancamento['rua_fcfo'])){
                            $arrayLancamento['rua_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['rua_fcfo'])));
                        }

                        if(!empty($arrayLancamento['complemento_fcfo'])){
                            $arrayLancamento['complemento_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['complemento_fcfo'])));
                        }

                        if(!empty($arrayLancamento['numero_fcfo'])){
                            $arrayLancamento['numero_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['numero_fcfo'])));
                        }

                        if(!empty($arrayLancamento['bairro_fcfo'])){
                            $arrayLancamento['bairro_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['bairro_fcfo'])));
                        }

                        if(!empty($arrayLancamento['cidade_fcfo'])){
                            $arrayLancamento['cidade_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['cidade_fcfo'])));
                        }

                        if(!empty($arrayLancamento['pais_fcfo'])){
                            $arrayLancamento['pais_fcfo'] =
                                trim(strtoupper(Helper::removeSpecialChars($arrayLancamento['pais_fcfo'])));
                        }

                        $return = $ws->integrarParcelaGestorFluxus($arrayLancamento);
                        $dados = $return->getFirstMensagem();
                        $arrayRetorno[] = $dados;

                        if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO){
                            if (isset($dados['idlancamento']) && isset($dados['codcfo'])){

                                $lancamentoIntegracao = new \G2\Entity\LancamentoIntegracao();
                                $lancamentoIntegracao->setIdUsuariocadastro($this->sessao->id_usuario);
                                $lancamentoIntegracao->setIdEntidade($this->sessao->id_entidade);
                                $lancamentoIntegracao->setIdSistema($this->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::FLUXUS));
                                $lancamentoIntegracao->setIdLancamento($this->getReference('G2\Entity\Lancamento', $arrayLancamento['id_lancamento']));
                                $lancamentoIntegracao->setStCodlancamento($dados['idlancamento']);
                                $lancamentoIntegracao->setStCodresponsavel($dados['codcfo']);

                                $this->save($lancamentoIntegracao);
                            }

//                            $lancamentoBanco = $this->em->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $arrayLancamento['id_lancamento']));
//                            $lancamentoBanco->setBlOriginal(1);
//
//                            $this->merge($lancamentoBanco);

                        }else{
                            $erroNegorio = new ErroNegocio();
                            if (isset($dados['result'])) {
                                if (is_array($dados['result']['error'])) {
                                    foreach ($dados['result']['error'] as $key => $error) {
                                        $erroNegorio->saveError(array(  'st_mensagem' =>
                                            !empty($error) ? $error : 'Erro padrão: nenhum retorno via WebServices',
                                            'st_codigo' =>  $dados['result']['assinatura'][$key],
                                            'st_origem' => __CLASS__.'('.__LINE__.')'));

                                        $arrayRetornoErro[] = 'Venda: '.$arrayLancamento['id_venda'].
                                            ' - Erro: '. $error;
                                    }
                                } else {
                                    $erroNegorio->saveError(array(  'st_mensagem' =>
                                        !empty($dados['result']['error']) ?
                                            $dados['result']['error'] :
                                            'Erro padrão: nenhum retorno via WebServices',
                                        'st_codigo' =>  $dados['result']['assinatura'],
                                        'st_origem' => __CLASS__.'('.__LINE__.')'));

                                    $arrayRetornoErro[] = 'Venda: '.$arrayLancamento['id_venda'].
                                        ' - Erro: '.$dados['result']['error'];
                                }
                            } else {
                                $erroNegorio->saveError(array(  'st_mensagem' =>
                                        !empty($dados['error']) ? serialize($dados['error']) :
                                            'Erro padrão: nenhum retorno via WebServices',
                                    'st_codigo' => $arrayLancamento['id_lancamento'],
                                    'st_origem' => __CLASS__.'('.__LINE__.')'));

                                $arrayRetornoErro[] = 'Venda: '.$arrayLancamento['id_venda'].
                                    ' - Erro: '.$dados['error'];
                            }
                        }

                        $this->commit();
                    }catch (\Exception $e){
                        echo $e->getMessage();
                        $this->rollback();
                    }
                }
            }
            if (!empty($arrayRetornoErro)){
                return new \Ead1_Mensageiro($arrayRetornoErro, \Ead1_IMensageiro::ERRO);
            }else{
                return new \Ead1_Mensageiro(empty($arrayRetorno) ? 'Lançamentos não encontrados, não foi possível integrar com o Fluxus.' : $arrayRetorno,
                    empty($arrayRetorno) ? \Ead1_IMensageiro::AVISO : \Ead1_IMensageiro::SUCESSO);
            }
        }catch (\Exception $e){
            $erroNegorio = new ErroNegocio();
            $erroNegorio->saveError(array(  'st_mensagem' => $e->getMessage(),
                'st_codigo' => $e->getCode(),
                'st_origem' => __CLASS__.'('.__LINE__.')'));
            throw $e;
        }
    }

    public function handleError($errno, $errstr, $errfile, $errline, array $errcontext)
    {
        // error was suppressed with the @-operator
        if (0 === error_reporting()) {
            return false;
        }

        throw new \ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    public function sincronizarDadosFluxusGestorWS(array $dados){
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        set_error_handler(array($this, 'handleError'));

        try{

            if (!is_dir("upload".DIRECTORY_SEPARATOR."log")){
                mkdir("upload".DIRECTORY_SEPARATOR."log", 0777);
            }

            if(!file_exists("upload".DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR."log_integracao_".date("d_m_Y").".txt")) {
                // Criamos o arquivo do usuário com w+
                $fp = fopen("upload".DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR."log_integracao_".date("d_m_Y").".txt", "w+");
            }else{
                $fp = fopen("upload".DIRECTORY_SEPARATOR."log".DIRECTORY_SEPARATOR."log_integracao_".date("d_m_Y").".txt", "a+");
            }

            if(empty($dados['contrato'])){
                $dados['status_atualizacao'] = 1;
            }

            $ws = new Integracao();
            $return = $ws->listarParcelaFluxusG2($dados);

            if(array_key_exists('lancamentos', $return) && !is_null($return['lancamentos']) && array_key_exists('contrato', $return['lancamentos']) &&!is_null($return['lancamentos']['contrato'])){
                $parcela = $return['lancamentos'];
                unset($return['lancamentos']);
                $return['lancamentos'][] = $parcela;
            }

            if(!array_key_exists('lancamentos', $return) || is_null($return['lancamentos'])){
                //throw new Exception("Nenhuma parcela foi encontrada para atualização");
                fwrite($fp, 'Nenhuma parcela foi encontrada para atualização '.date("Y-m-d H:i:s")."\r\n");
                fclose($fp);
                return (array('type' => 'alert', 'data' => array(), 'message' => 'Nenhuma parcela foi encontrada para atualização'));
            }

            $arrayRetorno = array();
            $chargeSkipedPagarMe = array();

            $nu_cartao = array('id_acordo'=>"",'nu_cartao'=>"");

            foreach ($return['lancamentos'] as $lancamentoWS){
                try{
                    $this->beginTransaction();

                    $vwRetornolancamentos = $this->getRepository('\G2\Entity\TotvsVwSincronizaListaLancamentoFluxusG2')->findOneBy(array( 'id_venda' => str_replace('g2u', '', str_replace('G2U', '', $lancamentoWS['contrato'])),'st_codlancamento' => $lancamentoWS['idlan']));
                    $vwSincronizaAcordoFluxusG2 = $ws->retornaAcordo(array('codcoligada' => $lancamentoWS['codcoligada'], 'idlan' => $lancamentoWS['idlan']));
                    /** @var \G2\Entity\Venda $venda */
                    $venda = $this->em->getRepository('\G2\Entity\Venda')->findOneBy(array('id_venda' => str_replace('g2u', '', str_replace('G2U', '', $lancamentoWS['contrato']))));

                    if (is_null($venda)){
                        fwrite($fp, "Venda ".$lancamentoWS['contrato']." não foi encontrada. \r\n");
                        $arrayRetorno[] = "Venda ".$lancamentoWS['contrato']." não foi encontrada.";
                        $this->rollback();
                        continue;
                    }

                    if (!is_null($vwRetornolancamentos)){
                        $lancamento = $this->getRepository('\G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $vwRetornolancamentos->getIdLancamento()));
                    }else{
                        $lancamento = null;
                    }

                    if (is_null($lancamento))
                        $lancamento = new \G2\Entity\Lancamento();

                    $id_lancamento = $lancamento->getId_lancamento();

                    if(empty($id_lancamento)){
                        $lancamento->setId_tipolancamento($this->getReference('\G2\Entity\TipoLancamento', \G2\Constante\TipoLancamento::CREDITO));
                        $lancamento->setId_usuariolancamento($venda->getIdUsuario()->getId_usuario());
                        $lancamento->setId_usuariocadastro($venda->getIdUsuariocadastro());
                    }else{
                        $lancamento->setId_tipolancamento( $this->em->getReference('\G2\Entity\TipoLancamento', $lancamentoWS['tipo_lancamento']));
                    }

                    $meiopagamento = $this->getRepository('\G2\Entity\MeioPagamentoIntegracao')->findOneBy(array('id_entidade' => $venda->getIdEntidade(), 'st_codsistema' => $lancamentoWS['codmeiopagamento'], 'id_sistema' => \G2\Constante\Sistema::FLUXUS));

                    $lancamento->setBl_quitado(in_array($lancamentoWS['statuslan'], array(1, 3)) ? 1 : 0);
                    $lancamento->setBl_ativo(in_array($lancamentoWS['statuslan'], array(0, 1)) ? 1 :0);
                    $lancamento->setNu_valor($lancamentoWS['valor_original']);
                    $lancamento->setDt_cadastro(new \DateTime($lancamentoWS['dt_emissao']));
                    /*  01 SIM
                        02 nao*/
                    $lancamento->setBl_chequedevolvido($lancamentoWS['cheque_devolvido'] === '01' ? 1 : 0);

                    if (is_null($meiopagamento)){
                        fwrite($fp, "Meio de Pagamento ".$lancamentoWS['codmeiopagamento']." para a entidade ".$venda->getIdEntidade()." não foi encontrado.\r\n");
                        $arrayRetorno[] = "Meio de Pagamento ".$lancamentoWS['codmeiopagamento']." para a entidade ".$venda->getIdEntidade()." não foi encontrado.";
                        $this->rollback();
                        continue;
                    }

                    $lancamento->setId_meiopagamento($this->getReference('\G2\Entity\MeioPagamento',
                        $meiopagamento->getId_meiopagamento()));

                    if (!is_null($lancamentoWS['codigo_banco_extrato'])){
                        $banco = $this->em->getReference('\G2\Entity\Banco', $lancamentoWS['codigo_banco_extrato']);
                        if (!is_null($banco))
                            $lancamento->setSt_banco($banco);
                    }

                    $lancamento->setId_entidade($venda->getIdEntidade());
                    $lancamento->setDt_vencimento(new \DateTime($lancamentoWS['dt_vencimento']));
                    $lancamento->setDt_emissao(new \DateTime($lancamentoWS['dt_emissao']));
                    $lancamento->setNu_quitado( $lancamentoWS['valor_baixado'] );
                    $lancamento->setDt_atualizado( new \DateTime($lancamentoWS['dt_alteracao']));
                    $lancamento->setSt_agencia($lancamentoWS['codigo_agencia_extrato']);
                    $lancamento->setSt_numcheque($lancamentoWS['numerochequeextrato']);
                    $lancamento->setSt_nossonumero($lancamentoWS['nosso_numero']);
                    $lancamento->setSt_codconta($lancamentoWS['conta_caixa']);
                    $lancamento->setId_codcoligada($lancamentoWS['codcoligada']);
                    $lancamento->setSt_statuslan($lancamentoWS['statuslan']);
                    $lancamento->setNu_verificacao(0);
                    $lancamento->setNu_juros($lancamentoWS['valor_juros']);
                    $lancamento->setNu_desconto($lancamentoWS['valor_desconto']);

                    if($meiopagamento->getId_meiopagamento() == \G2\Constante\MeioPagamento::BOLETO) {
                        $lancamento->setnu_cartao(null);
                    }

                    if (array_key_exists('acordo', $vwSincronizaAcordoFluxusG2) && array_key_exists('idacordo', $vwSincronizaAcordoFluxusG2['acordo'])){
                        // caso a forma de pagamento seja cartao de credito, ele executa a lógica para conferir se o nu_cartao já foi utilizado. Ele deve ser diferente para cada lancamento
                        if($meiopagamento->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTAO_CREDITO) {
                            if(empty($nu_cartao['nu_cartao']) || (!empty($nu_cartao['id_acordo']) ? $lancamento->getId_acordo() != $nu_cartao['id_acordo'] : false)) {
                                $query = $this->getem()->getConnection()->prepare(
                                    "SELECT distinct top 1 nu_cartao, id_acordo FROM vw_vendalancamento WHERE nu_cartao = ( select max(nu_cartao) from vw_vendalancamento where id_venda = ".$venda->getId_venda().") and id_venda = ".$venda->getId_venda()." order by id_acordo desc");
                                $query->execute();
                                $resultado = $query->fetchAll();

                                if (empty($resultado[0]['nu_cartao'])) {
                                    $nu_cartao['nu_cartao'] = 1;
                                    $nu_cartao['id_acordo'] = $lancamento->getId_acordo();
                                }else{
                                    $nu_cartao['id_acordo'] = $resultado[0]['id_acordo'];
                                    $nu_cartao['nu_cartao'] =  $resultado[0]['nu_cartao'];
                                }
                            }
                            if ($lancamento->getId_acordo() != $nu_cartao['id_acordo']) {
                                if(!empty($lancamento->getnu_cartao())){
                                    $lancamento->setnu_cartao($lancamento->getnu_cartao());
                                }else{
                                    $lancamento->setnu_cartao($nu_cartao['nu_cartao'] + 1);
                                }
                            }

                            if($lancamento->getId_acordo() == $nu_cartao['id_acordo']) {
                                $lancamento->setnu_cartao($nu_cartao['nu_cartao']);
                            }
                        }
                        $lancamento->setId_acordo($vwSincronizaAcordoFluxusG2['acordo']['idacordo']);
                    }elseif(array_key_exists('acordo', $vwSincronizaAcordoFluxusG2) && array_key_exists(0, $vwSincronizaAcordoFluxusG2['acordo'])){
                        $lancamento->setId_acordo($vwSincronizaAcordoFluxusG2['acordo'][count($vwSincronizaAcordoFluxusG2['acordo'])-1]['idacordo']);
                    }

                    if(!empty($lancamentoWS['dt_vencimento_cheque_extrato']))
                        $lancamento->setDt_vencimentocheque( new \DateTime($lancamentoWS['dt_vencimento_cheque_extrato']));

                    if(!empty($lancamentoWS['dt_baixa'])){
                        $lancamento->setDt_quitado( new \DateTime($lancamentoWS['dt_baixa']));
                    }

                    if (!$lancamento->getBl_quitado()){
                        $lancamento->setDt_quitado( null);
                        $lancamento->setDt_prevquitado(null);
                        $lancamento->setNu_quitado(0);
                    }
                    $this->save($lancamento);

                    /**
                     * Efetua a alteração do boleto na pagar-me se houver lançamento e se for boleto.
                     */
                    if (
                        !is_null($id_lancamento)
                        && $lancamento->getId_meiopagamento()->getId_meiopagamento() ==
                        \G2\Constante\MeioPagamento::BOLETO
                    ) {
                        $vendaNegocio = new \G2\Negocio\Venda();
                        $resultado = $vendaNegocio->atualizarBoleto(
                            $lancamento,
                            !empty($vwSincronizaAcordoFluxusG2['acordo'])
                        );

                        if ($resultado->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $arrayRetorno[] = "Venda: " . $venda->getIdvenda() .
                                " Lancamento: " . $lancamento->getId_lancamento() .
                                " - Boleto atualizado com sucesso na pagar-me!";
                            fwrite($fp, "Venda: " . $venda->getIdvenda() .
                                " Lancamento: " . $lancamento->getId_lancamento() .
                                " - Boleto atualizado com sucesso na pagar-me!");
                        }
                    }


                    if(is_null($id_lancamento)){
                        $lancamentoVenda = new \G2\Entity\LancamentoVenda();

                        $lancamentoVenda->setIdLancamento($lancamento);
                        $lancamentoVenda->setIdVenda($venda);
                        $lancamentoVenda->setBlEntrada(false);
                        $nuOrdem = $this->getRepository('\G2\Entity\LancamentoVenda')->retornaUltimoNuOrdemLancamentoVenda($venda->getIdVenda());
                        $lancamentoVenda->setNuOrdem($nuOrdem['nu_ordem']);
                        $this->save($lancamentoVenda);
                    }

                    $lancamentoIntegracao = $this->getRepository('\G2\Entity\LancamentoIntegracao')->findOneBy(array( 'id_lancamento' => $lancamento->getId_lancamento(), 'st_codlancamento' => $lancamentoWS['idlan']));
                    if (is_null($lancamentoIntegracao)){
                        $lancamentoIntegracao = new \G2\Entity\LancamentoIntegracao();
                    }

                    $lancamentoIntegracao->setStCodresponsavel($lancamentoWS['codcfo']);
                    $lancamentoIntegracao->setDtCadastro( new \DateTime($lancamentoWS['dt_emissao']) );
                    $lancamentoIntegracao->setDtSincronizado( new \DateTime(date("Y-m-d H:i:s")) );

                    if (!$lancamentoIntegracao->getIdLancamentointegracao()) {
                        $lancamentoIntegracao->setIdEntidade($venda->getIdEntidade());
                        $lancamentoIntegracao->setIdLancamento($this->getReference('G2\Entity\Lancamento', $lancamento->getId_lancamento()));
                        $lancamentoIntegracao->setIdSistema($this->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::FLUXUS));
                        $lancamentoIntegracao->setIdUsuariocadastro($venda->getIdUsuariocadastro());
                        $lancamentoIntegracao->setStCodlancamento($lancamentoWS['idlan']);
                    }

                    $lancamentoIntegracao = $this->save($lancamentoIntegracao);

                    if ( array_key_exists('acordo', $vwSincronizaAcordoFluxusG2) && !is_null($vwSincronizaAcordoFluxusG2['acordo'])){

                        if(!empty ($vwSincronizaAcordoFluxusG2['acordo']['idacordo'])) {
                            $acordoAux = $vwSincronizaAcordoFluxusG2['acordo'];
                            unset($vwSincronizaAcordoFluxusG2['acordo']);
                            $vwSincronizaAcordoFluxusG2['acordo'][0] = $acordoAux;
                        }

                        if (!empty($vwSincronizaAcordoFluxusG2['acordo'][0]['idacordo'])) {
                            $setleCharges = 0;
                            foreach ($vwSincronizaAcordoFluxusG2['acordo'] as $acordo){
                                $acordoRel = $this->em->getRepository('\G2\Entity\AcordoRel')->findOneBy(array('id_acordo' => $acordo['idacordo'], 'id_lan' => $acordo['idlan']));
                                //GII-6855
                                if (is_null($acordoRel)){
                                    $acordoRel = new \G2\Entity\AcordoRel();
                                }

                                $acordoRel->setClassificacao($acordo['classificacao']);
                                $acordoRel->setCodcoligada($acordo['codcoligada']);
                                $acordoRel->setIdAcordo($acordo['idacordo']);
                                $acordoRel->setIdLan($acordo['idlan']);

                                $acordoRel = $this->save($acordoRel);

                                $lancamento->setId_acordo($acordoRel->getIdAcordo());
                                $this->save($lancamento);

                                if(
                                    $lancamento->getId_meiopagamento()->getId_meiopagamento() ==
                                        \G2\Constante\MeioPagamento::CARTAO_RECORRENTE
                                    && !empty($venda->getRecorreteOrderid())
                                    && substr($venda->getRecorreteOrderid(), -3) != 'G1U'
                                    && substr_count($venda->getRecorreteOrderid(), '-') != 4
                                    && !empty($lancamento->getId_acordo())
                                    && $lancamento->getSt_statuslan() == 3
                                ) {
                                    $chargeSkipedPagarMe[$venda->getId_venda()][] = $lancamento->getId_lancamento();
                                }
                            }
                        }
                    }

                    $valorVendaFluxusG2 = $this->getRepository('\G2\Entity\TotvsVwSincronizaValorVendaFluxusG2')->findOneBy(array('id_venda' => $venda->getIdVenda()));
                    if (!empty($valorVendaFluxusG2)){
                        $venda->setNuValoratualizado($valorVendaFluxusG2->getNuValor());
                    }
                    $this->save($venda);
                    $this->commit();

                    $ws->atualizarFlagFluxus(array('idlan' => $lancamentoWS['idlan'], 'contrato' => $lancamentoWS['contrato']));
                    $arrayRetorno[] = "Venda: ".$venda->getIdVenda()." Lancamento: ".$lancamento->getId_lancamento() ." Cadastrado/Alterado com sucesso." ;
                    fwrite($fp, "Venda: ".$venda->getIdVenda()." Lancamento: ".$lancamento->getId_lancamento() ." Cadastrado/Alterado com sucesso. ".date("Y-m-d H:i:s")."\r\n");

                } catch (Exception $messagem) {
                    $arrayRetorno[] =  $messagem->getMessage();
                    fwrite($fp, $messagem->getMessage()." ".date("Y-m-d H:i:s")."\r\n");
                    $this->rollback();
                } catch(ErrorException $messagem) {
                    $arrayRetorno[] =  $messagem->getMessage();
                    fwrite($fp, $messagem->getMessage()." ".date("Y-m-d H:i:s")."\r\n");
                    $this->rollback();
                }
            }
            $pagarme = new Pagarme();

            foreach($chargeSkipedPagarMe as $key => $value) {
                $this->beginTransaction();
                try {
                    $venda = $this->getRepository('\G2\Entity\Venda')->findOneBy(array('id_venda' =>$key));

                    $settleCharges = 0;
                    foreach ($value as $id_lancamento) {
                        /** @var \G2\Entity\Lancamento $lancamento */
                        $lancamento = $this->getRepository('\G2\Entity\Lancamento')
                            ->findOneBy(array('id_lancamento' => $id_lancamento));

                        if (empty($lancamento)) {
                            continue;
                        }

                        /** @var \G2\Entity\AcordoPagarme $acordoLancamento */
                        $acordoLancamento = $this->getRepository('\G2\Entity\AcordoPagarme')
                            ->findOneBy(array('id_lancamento' => $lancamento->getId_lancamento(),
                                'id_assinatura' => $venda->getRecorreteOrderid()));

                        if(!empty($acordoLancamento)) {
                            continue;
                        }

                        $acordoPagarme = new AcordoPagarme();

                        $acordoPagarme->setId_acordo($lancamento->getId_acordo());
                        $acordoPagarme->setId_assinatura($venda->getRecorreteOrderid());
                        $acordoPagarme->setId_lancamento($lancamento->getId_lancamento());
                        $acordoPagarme->setDt_cadastro(new \DateTime());

                        $this->save($acordoPagarme);
                        $setleCharges++;
                    }

                    if ($setleCharges === 0) {
                        $this->commit();
                        continue;
                    }

                    $pagarmeMensageiro = $pagarme->settleCharge(
                        $venda->getRecorreteOrderid(), $venda->getId_entidade(), $setleCharges);

                    if ($pagarmeMensageiro->getTipo() == \Ead1_IMensageiro::ERRO) {
                        $arrayRetorno[] = "Venda: " . $venda->getId_venda() .
                            " Lancamento(s): " . join(', ', $value) .
                            " - Erro: ".$pagarmeMensageiro->getFirstMensagem();
                        fwrite($fp, "Venda: " . $venda->getId_venda() .
                            " Lancamento(s): " . join(', ', $value) .
                            " - Erro: ".$pagarmeMensageiro->getFirstMensagem());

                        $this->rollback();
                        continue;
                    }

                    $this->commit();

                } catch (\Exception $error) {
                    var_dump($error);
                    $arrayRetorno[] = "Venda: " . $venda->getId_venda() .
                        " Lancamento(s): " . join(', ', $value) .
                        " - Erro: ".$error->getMessage();
                    fwrite($fp, "Venda: " . $venda->getId_venda() .
                        " Lancamento(s): " . join(', ', $value) .
                        " - Erro: ".$error->getMessage());

                    $this->rollback();
                }
            }

            /**
             * array(1) {
            [0]=>
            object(PagarMe_Object)#1741 (3) {
            ["_attributes":protected]=>
            array(20) {
            ["object"]=>
            string(12) "subscription"
            ["plan"]=>
            object(PagarMe_Plan)#1767 (3) {
            ["_attributes":protected]=>
            array(13) {
            ["object"]=>
            string(4) "plan"
            ["id"]=>
            int(116514)
            ["amount"]=>
            int(100)
            ["days"]=>
            int(1)
            ["name"]=>
            string(37) "Plano G1:54622072 - Cristiano Ronaldo"
            ["trial_days"]=>
            int(0)
            ["date_created"]=>
            string(24) "2017-12-06T12:01:39.329Z"
            ["payment_methods"]=>
            array(2) {
            [0]=>
            string(6) "boleto"
            [1]=>
            string(11) "credit_card"
            }
            ["color"]=>
            NULL
            ["charges"]=>
            int(9)
            ["installments"]=>
            int(1)
            ["invoice_reminder"]=>
            NULL
            ["payment_deadline_charges_interval"]=>
            int(1)
            }
            ["_unsavedAttributes":protected]=>
            object(PagarMe_Set)#1768 (3) {
            ["_values":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_orderedValues":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_position":"PagarMe_Set":private]=>
            int(0)
            }
            ["_position":"PagarMe_Object":private]=>
            int(0)
            }
            ["id"]=>
            int(591330)
            ["current_transaction"]=>
            NULL
            ["postback_url"]=>
            NULL
            ["payment_method"]=>
            string(6) "boleto"
            ["card_brand"]=>
            NULL
            ["card_last_digits"]=>
            NULL
            ["current_period_start"]=>
            string(24) "2017-12-12T13:00:10.708Z"
            ["current_period_end"]=>
            string(24) "2017-12-13T13:00:10.708Z"
            ["charges"]=>
            int(3)
            ["status"]=>
            string(4) "paid"
            ["date_created"]=>
            string(24) "2017-12-06T12:01:41.752Z"
            ["date_updated"]=>
            string(24) "2017-12-12T13:00:10.709Z"
            ["phone"]=>
            NULL
            ["address"]=>
            NULL
            ["customer"]=>
            object(PagarMe_Customer)#1769 (3) {
            ["_attributes":protected]=>
            array(15) {
            ["object"]=>
            string(8) "customer"
            ["id"]=>
            int(11312286)
            ["external_id"]=>
            NULL
            ["type"]=>
            NULL
            ["country"]=>
            NULL
            ["document_number"]=>
            NULL
            ["document_type"]=>
            string(3) "cpf"
            ["name"]=>
            NULL
            ["email"]=>
            string(20) "ronaldo@homologa.com"
            ["phone_numbers"]=>
            NULL
            ["born_at"]=>
            NULL
            ["birthday"]=>
            NULL
            ["gender"]=>
            NULL
            ["date_created"]=>
            string(24) "2017-12-06T12:01:39.915Z"
            ["documents"]=>
            array(0) {
            }
            }
            ["_unsavedAttributes":protected]=>
            object(PagarMe_Set)#1770 (3) {
            ["_values":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_orderedValues":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_position":"PagarMe_Set":private]=>
            int(0)
            }
            ["_position":"PagarMe_Object":private]=>
            int(0)
            }
            ["card"]=>
            object(PagarMe_Object)#1771 (3) {
            ["_attributes":protected]=>
            array(12) {
            ["object"]=>
            string(4) "card"
            ["id"]=>
            string(30) "card_cjav06l4100t5t961gb4j22yh"
            ["date_created"]=>
            string(24) "2017-12-06T12:01:39.937Z"
            ["date_updated"]=>
            string(24) "2017-12-06T12:01:41.747Z"
            ["brand"]=>
            string(10) "mastercard"
            ["holder_name"]=>
            string(18) "Marcondes Gorgonho"
            ["first_digits"]=>
            string(6) "516230"
            ["last_digits"]=>
            string(4) "3777"
            ["country"]=>
            string(6) "BRAZIL"
            ["fingerprint"]=>
            string(25) "cj7jelnzcahxk0g10rmu8xk36"
            ["valid"]=>
            bool(true)
            ["expiration_date"]=>
            string(4) "1118"
            }
            ["_unsavedAttributes":protected]=>
            object(PagarMe_Set)#1772 (3) {
            ["_values":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_orderedValues":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_position":"PagarMe_Set":private]=>
            int(0)
            }
            ["_position":"PagarMe_Object":private]=>
            int(0)
            }
            ["metadata"]=>
            object(PagarMe_Object)#1773 (3) {
            ["_attributes":protected]=>
            array(1) {
            ["codboleto"]=>
            string(8) "54622072"
            }
            ["_unsavedAttributes":protected]=>
            object(PagarMe_Set)#1774 (3) {
            ["_values":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_orderedValues":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_position":"PagarMe_Set":private]=>
            int(0)
            }
            ["_position":"PagarMe_Object":private]=>
            int(0)
            }
            ["settled_charges"]=>
            array(3) {
            [0]=>
            int(1)
            [1]=>
            int(2)
            [2]=>
            int(3)
            }
            }
            ["_unsavedAttributes":protected]=>
            object(PagarMe_Set)#1766 (3) {
            ["_values":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_orderedValues":"PagarMe_Set":private]=>
            array(0) {
            }
            ["_position":"PagarMe_Set":private]=>
            int(0)
            }
            ["_position":"PagarMe_Object":private]=>
            int(0)
            }
            }
             */

            fclose($fp);
            return (array('type' => 'success', 'data' => $arrayRetorno, 'message' => 'Parcelas atualizadas com sucesso!'));

        }catch (Exception $messagem){
            fwrite($fp, $messagem->getMessage()." ".date("Y-m-d H:i:s")."\r\n");
            fclose($fp);
            return array('type' => 'error', 'data' => array(), 'message' => $messagem->getMessage());
        }
    }

    /**
     * @desc Método para dar baixa em lançamentos no fluxus
     * @param array $dados
     * @return array
     * @throws \Zend_Exception
     */
    public function baixarParcelaFluxus(array $dados = array())
    {
        $dados = $this->getRepository("\G2\Entity\LancamentoIntegracao")
            ->listarLancamentosBaixarFluxus($dados);

        $arrayReturn = array();

        $fp = $this->criarArquivoLog('log_baixa_parcelas_' . date('d_m_Y') . '.txt');

        foreach ($dados as $lancamentointegracao) {
            try {
                $WSResult = $this->ws->baixarParcelaFluxus(array(
                    'idlancamento' => $lancamentointegracao['st_codlancamento'],
                    'codcoligada' => $lancamentointegracao['st_codsistema'],
                    'nubaixado' => $lancamentointegracao['nu_quitado'],
                    'dtbaixa' => $lancamentointegracao['dt_quitado']
                    )
                );

                $this->escreverArquivo($fp, serialize($WSResult));

                $result = isset($WSResult[0])
                        ? $WSResult[0]['retorno']
                        : $WSResult['retorno'];

                if (isset($result['RETORNO']) && $result['RETORNO'] != self::PARAM_STR_INVALID_PARAMETER) {
                    $lancamento = $this->getRepository('\G2\Entity\Lancamento')
                                       ->findOneBy(array('id_lancamento' => $lancamentointegracao['id_lancamento']));
                    $lancamento->setBlBaixadofluxus(false);

                    $this->save($lancamento);
                }

                $arrayReturn[] = $result;

            }catch (Exception $messagem){
                $arrayReturn[] =  $messagem->getMessage();
                $this->escreverArquivo($fp, $messagem->getMessage());
            }
        }

        fclose($fp);

        return $arrayReturn;
    }

    /**
     * @desc Método para criar/abrir um arquivo de log para a integração
     * @param $file
     * @param string $path
     * @return resource
     */
    protected function criarArquivoLog($file, $path = null)
    {
        $path = $path ?: "upload" . DIRECTORY_SEPARATOR . "log" . DIRECTORY_SEPARATOR;
        $this->criarPastaLog($path);
        return ! file_exists($path . $file)
               ? fopen($path . $file, "w+")
               : fopen($path . $file, "a+");
    }

    /**
     * @desc Método para criar uma pasta no servidor caso ela ja não exista com permissões de leitura e escrita
     * @param string $path
     * @param int $permissions
     */
    protected function criarPastaLog($path = null , $permissions = 0777)
    {
        $path = $path ?: "upload" . DIRECTORY_SEPARATOR . "log" . DIRECTORY_SEPARATOR;
        if ( !is_dir( $path )){
            mkdir( $path , $permissions);
        }
    }

    /**
     * @Desc Método para escrever em um arquivo de log da integração
     * @param $file
     * @param $message
     */
    protected function escreverArquivo($file, $message){
        //escrevendo no arquivo informado.
        fwrite($file, $message." ".date("Y-m-d H:i:s")."\r\n");
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function tratarArray($data){
        foreach($data as $key => $value){
            if(empty($value)) unset($data[$key]);
        }

        return $data;
    }

    /**
     * @Desc Método para integrar uma venda ao fluxus
     * @param array $dados
     * @return array
     */
    public function integrarVendaFluxus(array $params = array(), $limit  = null, array $order = array()) {
        try {

            $fp = $this->criarArquivoLog("log_integracao_venda_fluxus_".date("d_m_Y").".txt");
            $params = $this->tratarArray($params);
            $order  = $this->tratarArray($order);
            $data = $this->getRepository('G2\Entity\TotvsVwIntegraVendaGestorFluxus')->findBy($params, $order, $limit, 0);
            $arrayRetorno = array();

            /** @var \G2\Entity\TotvsVwIntegraVendaGestorFluxus $vwVenda */
            foreach($data as $vwVenda){

                $this->beginTransaction();
                try{

                    $arrayVenda = $this->toArrayEntity($vwVenda);

                    $return = $this->ws->inserirVenda($arrayVenda);
                    $dados = $return->getFirstMensagem();
                    $arrayRetorno[] = $dados;

                    $this->escreverArquivo($fp, serialize($dados));

                    if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO){
                        //\Zend_Debug::dump($dados,__CLASS__.'('.__LINE__.')');exit;
                        if (!empty($dados['idvenda']) && !empty($dados['idmov'])){
                            $vendaIntegracao = new \G2\Entity\VendaIntegracao();

                            $vendaIntegracao->setId_sistema($this->getReference('\G2\Entity\Sistema', Sistema::ALUMNUS));
                            $vendaIntegracao->setId_usuariocadastro(1);
                            $vendaIntegracao->setId_venda($this->getReference('\G2\Entity\Venda', $dados['idvenda']));
                            $vendaIntegracao->setSt_codvenda($dados['idmov']);
                            $vendaIntegracao->setSt_codresponsavel($dados['codcfo']);

                            $this->save($vendaIntegracao);
                        }
                    }else{
                        $erroNegorio = new ErroNegocio();

                        if (is_array($dados['error'])){
                            foreach ($dados['error'] as $key => $error) {
                                $erroNegorio->saveError(array(  'st_mensagem' => $error,
                                    'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'][$key]) ? $dados['assinatura'][$key] : '',
                                    'st_origem' => __CLASS__.'('.__LINE__.')'));
                                $arrayRetornoErro[] = $error;
                            }
                        }else{
                            $erroNegorio->saveError(array(  'st_mensagem' => $dados['error'],
                                'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'] ) ? $dados['assinatura'] : '',
                                'st_origem' => __CLASS__.'('.__LINE__.')'));
                            $arrayRetornoErro[] = $dados['error'];
                        }
                    }

                    $this->commit();

                }catch (\Exception $e){
                    $arrayRetorno[] = $e->getMessage();
                    $this->escreverArquivo($fp, $e->getMessage());
                    $this->rollback();
                }
            }

            fclose($fp);
            return (array('type' => 'success', 'data' => $arrayRetorno, 'message' => 'Venda integrada com sucesso!'));

        }catch (Exception $messagem){
            $this->escreverArquivo($fp, $messagem->getMessage());
            fclose($fp);
            return array('type' => 'error', 'data' => array(), 'message' => $messagem->getMessage());
        }
    }

    /**
     * @param array $dados
     * @return array
     */
    public function integrarProdutoVenda(array $params = array(), $limit  = null, array $order = array()){
        try {

            $fp = $this->criarArquivoLog("log_integracao_venda_fluxus_".date("d_m_Y").".txt");
            $params = $this->tratarArray($params);
            $order  = $this->tratarArray($order);

            $data = $this->getRepository('G2\Entity\TotvsVwIntegraProdutoVendaGestorFluxus')->findBy($params, $order, $limit, 0);
            $arrayRetorno = array();
            $nseqitmov_titmmov = 1;
            $numerosequencial_titmmov = 1;

            /** @var \G2\Entity\TotvsVwIntegraProdutoVendaGestorFluxus $vwItem */
            foreach($data as $vwItem){

                $this->beginTransaction();
                try{

                    $arrayItem = $this->toArrayEntity($vwItem);


                    if(empty($arrayItem['nseqitmov_titmmov'])){
                        $arrayItem['nseqitmov_titmmov'] = $nseqitmov_titmmov;
                        $nseqitmov_titmmov++;
                    }

                    if(empty($arrayItem['numerosequencial_titmmov'])){
                        $arrayItem['numerosequencial_titmmov'] = $numerosequencial_titmmov;
                        $numerosequencial_titmmov++;
                    }
                    /*echo '<pre>';
                    Debug::dump($arrayItem);die;*/
                    $return = $this->ws->inserirProdutoVenda($arrayItem);
                    $dados = $return->getFirstMensagem();
                    $arrayRetorno[] = $dados;
                    //\Zend_Debug::dump($return,__CLASS__.'('.__LINE__.')');exit;
                    if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO){
                        //
                        if (!empty($dados['idprd']) && !empty($dados['idmov'])){
                            $vendaIntegracao = new \G2\Entity\VendaProdutoIntegracao();

                            $vendaIntegracao->setId_sistema($this->getReference('\G2\Entity\Sistema', Sistema::ALUMNUS));
                            $vendaIntegracao->setId_usuariocadastro(1);
                            $vendaIntegracao->setId_venda($this->getReference('\G2\Entity\Venda', $vwItem->getIdVenda() ));
                            $vendaIntegracao->setId_entidade($this->getReference('\G2\Entity\Entidade', $vwItem->getIdEntidade() ));
                            $vendaIntegracao->setId_produto($this->getReference('\G2\Entity\Produto', $vwItem->getIdProduto() ));
                            $vendaIntegracao->setSt_codvenda($dados['idmov']);
                            $vendaIntegracao->setSt_codigoprodutoexterno($dados['idprd']);

                            $this->save($vendaIntegracao);
                        }
                    }else{
                        $erroNegorio = new ErroNegocio();

                        if (is_array($dados['error'])){
                            foreach ($dados['error'] as $key => $error) {
                                $erroNegorio->saveError(array(  'st_mensagem' => $error,
                                    'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'][$key]) ? $dados['assinatura'][$key] : '',
                                    'st_origem' => __CLASS__.'('.__LINE__.')'));
                                $arrayRetornoErro[] = $error;
                            }
                        }else{
                            $erroNegorio->saveError(array(  'st_mensagem' => $dados['error'],
                                'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'] ) ? $dados['assinatura'] : '',
                                'st_origem' => __CLASS__.'('.__LINE__.')'));
                            $arrayRetornoErro[] = $dados['error'];
                        }
                    }

                    $this->escreverArquivo($fp, serialize($dados));
                    $this->commit();

                }catch (\Exception $e){
                    $arrayRetorno[] = $e->getMessage();
                    $this->escreverArquivo($fp, $e->getMessage());
                    $this->rollback();
                }
            }

            fclose($fp);
            return (array('type' => 'success', 'data' => $arrayRetorno, 'message' => 'Item integrado com sucesso!'));

        }catch (Exception $messagem){
            $this->escreverArquivo($fp, $messagem->getMessage());
            fclose($fp);
            return array('type' => 'error', 'data' => array(), 'message' => $messagem->getMessage());
        }
    }

    /**
     * @return array
     */
    public function integrarLancamentoVenda(array $params = array(), $limit  = null, array $order = array()) {
        try {

            $fp = $this->criarArquivoLog("log_integracao_lancamento_movimento_fluxus_".date("d_m_Y").".txt");
            $params = $this->tratarArray($params);
            $order  = $this->tratarArray($order);

            $data = $this->getRepository('G2\Entity\VwIntegraLancamentoMovimentoFluxus')->findBy($params, $order, $limit, $order);
            $arrayRetorno = array();

            /** @var \G2\Entity\VwIntegraLancamentoMovimentoFluxus $vwLancamento */
            foreach($data as $vwLancamento){

                $this->beginTransaction();
                try{

                    $arrayLancamento = $this->toArrayEntity($vwLancamento);

                    $return = $this->ws->inserirVendaLancamento($arrayLancamento);
                    $dados = $return->getFirstMensagem();
                    $arrayRetorno[] = $dados;

                    if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO){
                        /** @var \G2\Entity\LancamentoIntegracao $lancamentoIntegracao */
                        $lancamentoIntegracao = $this->getRepository('\G2\Entity\LancamentoIntegracao')->findOneBy(array('id_lancamento' => $vwLancamento->getIdLancamento()));
                        $lancamentoIntegracao->setStCodvenda($dados['idmov']);

                        $this->save($lancamentoIntegracao);

                    }else{
                        $erroNegorio = new ErroNegocio();

                        if (is_array($dados['error'])){
                            foreach ($dados['error'] as $key => $error) {
                                $erroNegorio->saveError(array(  'st_mensagem' => $error,
                                    'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'][$key]) ? $dados['assinatura'][$key] : '',
                                    'st_origem' => __CLASS__.'('.__LINE__.')'));
                                $arrayRetornoErro[] = $error;
                            }
                        }else{
                            $erroNegorio->saveError(array(  'st_mensagem' => $dados['error'],
                                'st_codigo' =>  ( isset($dados['assinatura']) && $dados['assinatura'] ) ? $dados['assinatura'] : '',
                                'st_origem' => __CLASS__.'('.__LINE__.')'));
                            $arrayRetornoErro[] = $dados['error'];
                        }
                    }

                    $this->escreverArquivo($fp, serialize($dados));
                    $this->commit();

                }catch (\Exception $e){
                    $arrayRetorno[] = $e->getMessage();
                    $this->escreverArquivo($fp, $e->getMessage());
                    $this->rollback();
                }
            }

            fclose($fp);
            return (array('type' => 'success', 'data' => $arrayRetorno, 'message' => 'Lançamento movimento integrado com sucesso!'));

        }catch (Exception $messagem){
            $this->escreverArquivo($fp, $messagem->getMessage());
            fclose($fp);
            return array('type' => 'error', 'data' => array(), 'message' => $messagem->getMessage());
        }
    }
}

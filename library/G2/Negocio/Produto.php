<?php

namespace G2\Negocio;

use G2\Entity\ProdutoProjetoPedagogico;
use G2\Entity\TipoProduto;

/**
 * Classe de negócio para Produto
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-29-01
 */
class Produto extends Negocio
{

    private $repositoryProduto = 'G2\Entity\Produto';

    /**
     * Retorna todas os Produtos da Entidade
     * @return \Ead1_Mensageiro
     */
    public function findAllProduto()
    {

        try {

            $result = $this->em->getRepository($this->repositoryProduto)
                ->findBy(array('id_entidade' => $this->sessao->id_entidade), array('st_produto' => 'ASC'));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     *
     * @param array $params
     * @throws \Exception
     */
    public function findByVwProduto(array $params = array())
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            return $this->findBy('\G2\Entity\VwProduto', $params, array("st_produto" => "ASC"));
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public function findByProdutoCategoria(array $params = array())
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            $result = $this->em->getRepository('\G2\Entity\VwCategoriaProduto')->retornaProdutosCategorias($params);
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    if (!isset($arrReturn[$row['id_produto']])) {
                        $arrReturn[$row['id_produto']] = $row;
                        $arrReturn[$row['id_produto']]['categorias'] = array(0 => array(
                            'id_categoria' => $row['id_categoria'],
                            'st_nomeexibicao' => $row['st_nomeexibicao'],
                            'st_categoria' => $row['st_categoria']
                        ));
                    } else {
                        array_push($arrReturn[$row['id_produto']]['categorias'], array(
                                'id_categoria' => $row['id_categoria'],
                                'st_nomeexibicao' => $row['st_nomeexibicao'],
                                'st_categoria' => $row['st_categoria']
                            )
                        );
                    }

                    unset($arrReturn[$row['id_produto']]['id_categoria'], $arrReturn[$row['id_produto']]['st_nomeexibicao'], $arrReturn[$row['id_produto']]['st_categoria']);
                }
            }
            return $arrReturn;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * @param array $params
     * @throws \Exception
     */
    public function findByModeloVenda(array $params = array())
    {
        try {
            return $this->findBy('\G2\Entity\ModeloVenda', $params, array("st_modelovenda" => "ASC"));
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     * Retorna o Produto pelo ID
     * @param type $id_produto
     * @return \Ead1_Mensageiro
     */
    public function findProdutoById($id_produto)
    {

        try {

            $result = $this->em->getRepository($this->repositoryProduto)->findBy(array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_produto' => $id_produto));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn = $this->toArrayEntity($row);
            }

            $arrReturn['id_usuariocadastro'] = $arrReturn['id_usuariocadastro']['id_usuario'];
            $arrReturn['id_tipoproduto'] = $arrReturn['id_tipoproduto']['id_tipoproduto'];
            $arrReturn['id_situacao'] = $arrReturn['id_situacao']['id_situacao'];
            $arrReturn['id_entidade'] = $arrReturn['id_entidade']['id_entidade'];


            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o Plano de Pagamento pelo ID
     * @param type $id_planopagamento
     * @return \Ead1_Mensageiro
     */
    public function findPlanoPagamentoById($id_planopagamento)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\PlanoPagamento')->find($id_planopagamento);
            if ($result) {
                $arrReturn = $this->toArrayEntity($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o Valor ID
     * @param type $id_produtovalor
     * @return \Ead1_Mensageiro
     */
    public function findProdutoValorById($id_produtovalor)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProdutoValor')->find($id_produtovalor);
            if ($result) {
                $arrReturn = $this->toArrayEntity($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o Produto valor
     * @param array $dados
     * @return \Ead1_Mensageiro
     */
    public function findProdutoValorByArray(array $dados, $valido = false)
    {
        try {
            $result = null;
            $arsel = null;

            if ($valido) {

                $sql = $this->em->getRepository('G2\Entity\ProdutoValor')->createQueryBuilder('p')->select(array(
                    'IDENTITY(p.id_tipoprodutovalor) AS id_tipoprodutovalor',
                    'IDENTITY(p.id_usuariocadastro) AS id_usuariocadastro',
                    'p.id_produtovalor',
                    'IDENTITY(p.id_produto) AS id_produto',
                    'p.dt_cadastro',
                    'p.dt_termino',
                    'p.dt_inicio',
                    'p.nu_valor',
                    'p.nu_valormensal',
                    'p.nu_basepropor'
                ));

                foreach ($dados as $par => $value) {
                    if ($value) {
                        $t = "p." . $par . " = " . $value;
                        $sql->andWhere($t); //->setParameter($par, $value);
                    }
                }

                //$sql->andWhere("p.dt_inicio <= :HOJE");
                //$sql->andWhere("p.dt_termino >= :HOJE");
                //$sql->setParameter("HOJE", date("Y-m-d H:i:s"));

                $sql->orderBy('p.dt_inicio', 'ASC');

                $array = $sql->getQuery()->getArrayResult();

                if ($array) {
                    foreach ($array as $entity) {
                        $et = new \G2\Entity\ProdutoValor();
                        $et->montarEntity($entity);
                        $result[] = $et;
                    }
                }
            }

            if (!$result) {
                $result = $this->em->getRepository('G2\Entity\ProdutoValor')->findBy($dados, array('dt_cadastro' => 'DESC'));
            }

            if ($result) {
                $mensageiro = new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o Livro ID
     * @param type $id_produtolivro
     * @return \Ead1_Mensageiro
     */
    public function findProdutoLivroById($id_produtolivro)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProdutoLivro')->find($id_produtolivro);
            if ($result) {
                $arrReturn = array(
                    'id_produto' => $result->getId_produto(),
                    'id_livro' => $result->getId_livro()->getId_livro(),
                    'id_entidade' => $result->getId_entidade()->getId_entidade()
                );
//                $arrReturn = $this->toArrayEntity($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Find Produto Livro By Id Produto
     * @param integer $idProduto
     * @return \Ead1_Mensageiro
     */
    public function findProdutoLivroByProduto($idProduto)
    {
        try {
            $result = $this->findBy('G2\Entity\ProdutoLivro', array('id_produto' => $idProduto));
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
            }
        } catch (\Exception $ex) {
            return new \Ead1_Mensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * @param int $id
     * @return \Ead1_Mensageiro
     */
    public function salvarProduto(array $array)
    {

        try {

            if (!isset($array['id_entidade']) || !$array['id_entidade']) {
                $array['id_entidade'] = $this->sessao->id_entidade;
            }
            if (!isset($array['id_usuariocadastro']) || !$array['id_usuariocadastro']) {
                $array['id_usuariocadastro'] = $this->sessao->id_usuario;
            }
            if (!isset($array['dt_cadastro']) || !$array['dt_cadastro']) {
                $array['dt_cadastro'] = new \Zend_Date();
            }
            if (!isset($array['dt_atualizado']) || !$array['dt_atualizado']) {
                $array['dt_atualizado'] = new \Zend_Date();
            }
            $produto = new \G2\Entity\Produto(); //instancia a entity
            //se o id_produto vier setado faz um find do produto
            if (isset($array['id_produto']) && $array['id_produto']) {
                $produto = $this->find('\G2\Entity\Produto', $array['id_produto']);
            } else {
                $produto = new \G2\Entity\Produto();
            }
            //usa o serialize pra setar os valores
            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $produto = $serialize->arrayToEntity($array, $produto); //pegou a instancia da entity e passou no lugar do new

            if ($produto instanceof \G2\Entity\Produto) {

                $this->verificarIntegracao($produto->getId_tipoproduto());

                if (!$produto->getBl_todasformas()) {
                    $produto->setBl_todasformas(0);
                }
                if (!$produto->getBl_todascampanhas()) {
                    $produto->setBl_todascampanhas(0);
                }
                if (!$produto->getNu_gratuito()) {
                    $produto->setNu_gratuito(0);
                }
                if (!$produto->getBl_unico()) {
                    $produto->setBl_unico(0);
                }
                if (!$produto->getNu_pontospromocional()) {
                    $produto->setNu_pontospromocional(0);
                }
                if (!$produto->getNu_pontos()) {
                    $produto->setNu_pontos(0);
                }
                if (!$produto->getid_produtoimagempadrao()) {
                    $produto->setid_produtoimagempadrao(null);
                }


                if ($produto->getDt_fimpontosprom() instanceof \DateTime && $produto->getDt_iniciopontosprom() instanceof \DateTime) {
                    $df = $produto->getDt_iniciopontosprom()->diff($produto->getDt_fimpontosprom());
                    if ($df->invert) {
                        throw new \Exception("A Data de Início dos Pontos Promocionais não pode ser maior que a Data de Término!");
                    }
                }


                if (!$produto->getbl_destaque()) {
                    $produto->setBl_destaque(0);
                }

                if (!$produto->getId_produto()) {
//                    $this->merge($produto);
//                } else {
                    $produto->setBl_ativo(true);
//                    $this->save($produto);
                }

                $this->save($produto);

                $ba = new \G2\Negocio\Baiao();
                $ei = $ba->atualizaProduto($produto->getId_produto());
            }
            //if($produto instanceof \G2\Entity\Produto){


            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($produto->getId_produto());
        } catch (\Exception $exc) {

            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Verifica a Integração com os WS do tipo de produto quando existirem
     * @param $id_tipoproduto
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @see \Produto\salvarProduto
     */
    public function verificarIntegracao(\G2\Entity\TipoProduto $tp)
    {

        try {

            $eiEntity = null;
            $int = new  \G2\Negocio\EntidadeIntegracao();
            $st_sistema = null;
            switch ($tp->getId_tipoproduto()) {
                /**
                 * e-Book
                 */
                case \TipoProdutoTO::LIVRO:
                    $sistema = $int->find('\G2\Entity\Sistema', \SistemaTO::LEYA_BOOKSTORE);
                    $st_sistema = $sistema->getSt_sistema();
                    $ei = new \G2\Negocio\EntidadeIntegracao();
                    $eiEntity = $ei->retornaEntidadeIntegracao($this->sessao->id_entidade, \SistemaTO::LEYA_BOOKSTORE);
                    if ($eiEntity) {
                        return new \Ead1_Mensageiro("Integração encontrada entre o Tipo de Produto " . $tp->getSt_tipoproduto() . " e a " . $st_sistema . ".", \Ead1_IMensageiro::SUCESSO);
                    }
                    break;
                default:
                    return new \Ead1_Mensageiro("Validação ainda não implementada para este tipo de produto.", \Ead1_IMensageiro::SUCESSO);
                    break;

            }

            if (!$eiEntity)
                throw new \Zend_Exception("A Integração para o Tipo de Produto " . $tp->getSt_tipoproduto() . " não existe, favor solicitar a configuração da Integração com a " . $st_sistema . " ao setor responsável.");

        } catch (\Exception $e) {
            throw $e;
            return false;
        }

    }


    /**
     * Salva o Valor de um Produto!
     * Usando a RO por não ser necessário recriar código
     * @param array $array
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoValor(array $array)
    {
        try {

            $ro = new \ProdutoRO();

            if (isset($array["id_tipoprodutovalor"]["id_tipoprodutovalor"])) {
                $array["id_tipoprodutovalor"] = $array["id_tipoprodutovalor"]["id_tipoprodutovalor"];
            }
            if (isset($array["id_usuariocadastro"]["id_usuario"])) {
                $array["id_usuariocadastro"] = $array["id_usuariocadastro"]["id_usuario"];
            }

            $to = new \ProdutoValorTO();
            $to->montaToDinamico($array);

            $to->setNu_valor($to->getNu_valor() ? $to->getNu_valor() : 0);

            if (!$to->getDt_inicio())
                $to->setDt_inicio(new \Zend_Date(date('Y-m-d')));
            else
                $to->setDt_inicio(new \Zend_Date($to->getDt_inicio()));

//            if ($to->getDt_termino() == false) {
//                $to->setDt_termino(new \Zend_Date());
//                $to->getDt_termino()->add('30', \Zend_Date::YEAR);
//            } else {
//                $to->setDt_termino(new \Zend_Date($to->getDt_termino()));
//            }
            if (!$to->getDt_termino())
                $to->setDt_termino(null);
            else
                $to->setDt_termino(new \Zend_Date($to->getDt_termino()));

            if ($to->getDt_inicio() instanceof \Zend_Date && $to->getDt_termino() instanceof \Zend_Date) {
                $df = $to->getDt_inicio()->isLater($to->getDt_termino());
                if ($df) {
                    throw new \Exception("A Data de Início não pode ser maior que a Data de Término!");
                }
            }

            $to->setId_produtovalor($to->getId_produtovalor() ? $to->getId_produtovalor() : null);

            $mensageiro = $ro->salvarProdutoValor($to);
            if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro->setMensagem($to);
            } else {
                throw new \Exception($mensageiro->getFirstMensagem());
            }
            $mensageiro->setId($to->getId_produtovalor());
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Salva o Plano de Pagamento de um produto
     * @param array $array
     * @return \Ead1_Mensageiro
     */
    public function salvarPlanoPagamento(array $array)
    {

        try {

            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $entity = $serialize->arrayToEntity($array, new \G2\Entity\PlanoPagamento());

            if ($entity->getId_planopagamento()) {
                $this->em->merge($entity);
            } else {
                $this->em->persist($entity);
            }
            $this->em->flush();

            $mensageiro = new \Ead1_Mensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Retorna o ProdutoProjetoPedagogico
     * @param array $param
     * @return \Ead1_Mensageiro
     */
    public function findProdutoProjetoPedagogicoByArray(array $param = null)
    {
        try {
            $result = $this->findBy('G2\Entity\ProdutoProjetoPedagogico', $param);
//            $result = $this->em->getRepository('G2\Entity\ProdutoProjetoPedagogico')
//                ->findBy($param);
            $arrReturn = array();
            foreach ($result as $row) {
//                $arrReturn[] = $this->toArrayEntity($row);
                //Verificar o funcionamento do toBackBoneArray() pois ele não retorna relacionamentos.
                if ($row->getId_contratoregra() instanceof \G2\Entity\ContratoRegra)
                    $row->setId_contratoregra($row->getId_contratoregra()->getId_contratoregra());

                $arrReturn[] = $row->toBackBoneArray();
            }
            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Funcionalidade para salvar o projeto pedagógico nos produtos
     * @param array $array
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoProjetoPedagogico(array $array)
    {
        try {
            $array['id_entidade'] = $this->sessao->id_entidade;

            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $findP = new \G2\Entity\ProdutoProjetoPedagogico();

            if (array_key_exists('id_produtoprojetopedagogico', $array) && $array['id_produtoprojetopedagogico']) {
                $findP = $this->find('G2\Entity\ProdutoProjetoPedagogico', $array['id_produtoprojetopedagogico']);
            } else
                /**
                 * Verifica se ja existe um projeto vinculado ao projeto, caso exista e feito um update no registro, com o mesmo ou outro projeto pedagogico
                 */
                if (array_key_exists('id_produto', $array) && $array['id_produto']) {
                    $findOneBy = $this->findOneBy('G2\Entity\ProdutoProjetoPedagogico', array('id_produto' => $array['id_produto']));
                    if ($findOneBy) {
                        $findP = $findOneBy;
                    }
                }

            $produtoprojeto = $serialize->arrayToEntity($array, $findP);

            //Verifica se o contrato é null, pois pode existir a possibilidade de remover o contrato.
            if (array_key_exists('id_contratoregra', $array) && !$array['id_contratoregra'])
                $produtoprojeto->setId_contratoregra($array['id_contratoregra']);

            if ($produtoprojeto instanceof ProdutoProjetoPedagogico) {
                if (!$produtoprojeto->getNu_tempoacesso() && !$produtoprojeto->getbl_indeterminado()) {
                    throw new \Zend_Exception("O tempo de acesso deve ser indeterminado ou conter algum valor maior que zero!");
                }

                $verificaEntityProduto = $this->em->getRepository('G2\Entity\ProdutoProjetoPedagogico')
                    ->findOneBy(
                        array(
                            'id_projetopedagogico' => $produtoprojeto->getId_projetopedagogico(),
                            'id_entidade' => $produtoprojeto->getid_entidade(),
                            'id_turma' => $produtoprojeto->getId_turma()
                        )
                    );

                if ($verificaEntityProduto && $produtoprojeto->getId_produto() != $verificaEntityProduto->getId_produto()) {
                    throw new \Zend_Exception('Já existe um produto cadastrado com o mesmo projeto pedagógico e a mesma turma');
                }

                try {
                    $return = $this->save($produtoprojeto);
                } catch (Exception $ex) {
                    throw new \Exception("Erro ao salvar o produto projeto pedagógico");
                }

                $mensageiro = new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($return->getId_produtoprojetopedagogico());
            } else {
                throw new \Exception("Ocorreu um erro ao criar a Entity ProdutoProjetoPedagogico");
            }
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Zend_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Salva o Tipo de Produto Livro
     * @param array $array
     * @throws \Zend_Validate_Exception
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoLivro(array $array)
    {

        try {

            if (!$array['id_entidade']) {
                $array['id_entidade'] = $this->sessao->id_entidade;
            }


            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $produtoprojeto = $serialize->arrayToEntity($array, new \G2\Entity\ProdutoLivro());

            $entityVerifyProduto = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_produto' => $produtoprojeto->getid_produto()));

            if ($entityVerifyProduto) {
                throw new \Zend_Validate_Exception("Livro do Produto não alterado.");
            }

            $entityVerify = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_entidade' => $produtoprojeto->getid_entidade()));

            if ($entityVerify) {
                throw new \Zend_Exception("Já existe um Produto cadastrado com este Livro");
            }

            $entity = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_produto' => $produtoprojeto->getId_produto(), 'id_entidade' => $produtoprojeto->getid_entidade()));
            if ($entity) {
                $this->em->merge($produtoprojeto);
            } else {
                $this->em->persist($produtoprojeto);
            }
            $this->em->flush();

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($produtoprojeto->getId_produto());
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function salvarProdutoDeclaracao(array $array)
    {

        try {

            if (!$array['id_entidade']) {
                $array['id_entidade'] = $this->sessao->id_entidade;
            }


            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $produtoprojeto = $serialize->arrayToEntity($array, new \G2\Entity\ProdutoLivro());

            $entityVerifyProduto = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_produto' => $produtoprojeto->getid_produto()));

            if ($entityVerifyProduto) {
                throw new \Zend_Validate_Exception("Livro do Produto não alterado.");
            }

            $entityVerify = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_entidade' => $produtoprojeto->getid_entidade()));

            if ($entityVerify) {
                throw new \Zend_Exception("Já existe um Produto cadastrado com este Livro");
            }

            $entity = $this->em->getRepository('G2\Entity\ProdutoLivro')
                ->findOneBy(array('id_produto' => $produtoprojeto->getId_produto(), 'id_entidade' => $produtoprojeto->getid_entidade()));
            if ($entity) {
                $this->em->merge($produtoprojeto);
            } else {
                $this->em->persist($produtoprojeto);
            }
            $this->em->flush();

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($produtoprojeto->getId_produto());
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

//     public function salvarProdutoArea(array $array)
//     {
//         try {
//             $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
//             $entity = new \G2\Entity\ProdutoArea();
//             $entity->montarEntity($array);
//             Zend_Debug::dump($entity,__CLASS__.'('.__LINE__.')');exit;
//             $produtoprojeto = $serialize->arrayToEntity($array, new \G2\Entity\ProdutoLivro());
//             $entityVerifyProduto = $this->em->getRepository('G2\Entity\ProdutoLivro')
//                     ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_produto' => $produtoprojeto->getid_produto()));
//             if ($entityVerifyProduto) {
//                 throw new \Zend_Validate_Exception("Livro do Produto não alterado.");
//             }
//             $entityVerify = $this->em->getRepository('G2\Entity\ProdutoLivro')
//                     ->findOneBy(array('id_livro' => $produtoprojeto->getId_livro(), 'id_entidade' => $produtoprojeto->getid_entidade()));
//             if ($entityVerify) {
//                 throw new \Zend_Exception("Já existe um Produto cadastrado com este Livro");
//             }
//             $entity = $this->em->getRepository('G2\Entity\ProdutoLivro')
//                     ->findOneBy(array('id_produto' => $produtoprojeto->getId_produto(), 'id_entidade' => $produtoprojeto->getid_entidade()));
//             if ($entity) {
//                 $this->em->merge($produtoprojeto);
//             } else {
//                 $this->em->persist($produtoprojeto);
//             }
//             $this->em->flush();
//             $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
//             $mensageiro->setId($produtoprojeto->getId_produto());
//         } catch (\Zend_Validate_Exception $exc) {
//             $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
//         } catch (\Exception $exc) {
//             $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
//         }
//         return $mensageiro;
//     }

    /**
     * Retorna o Produto Combo por ID
     * @param type $id_produtocombo
     * @return \Ead1_Mensageiro
     */
    public function findProdutoComboById($id_produtocombo)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProdutoCombo')->find($id_produtocombo);
            if ($result) {
                $arrReturn = $this->toArrayEntity($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o ProdutoCombo
     * @param array $param
     * @return \Ead1_Mensageiro
     */
    public function findProdutoComboByArray(array $param = null, $returnArray = true)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProdutoCombo')
                ->findBy($param);

            if ($result) {

                $arrReturn = array();
                if ($returnArray) {
                    foreach ($result as $row) {
                        $arrReturn[] = $this->toArrayEntity($row);
                    }
                } else {
                    $arrReturn = $result;
                }
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna o VwProdutoCombo
     * @param array $param
     * @return \Ead1_Mensageiro
     */
    public function findVwProdutoComboByArray(array $param = null)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\VwProdutoCombo')->findBy($param);

            if ($result) {

                $arrReturn = array();
                foreach ($result as $row) {
                    $arrReturn[] = $this->toArrayEntity($row);
                }

                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Salva o Produto Combo
     * @param array $array
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoCombo(array $array)
    {

        try {

            $array['id_produtocombo'] = isset($array['id_produtocombo']) ? $array['id_produtocombo'] : null;

            if ($array['id_produtocombo']) {
                $produtocombo = $this->em->getRepository('G2\Entity\ProdutoCombo')->find($array['id_produtocombo']);
                $produtocombo->setNu_descontoporcentagem($array['nu_descontoporcentagem']);
                $this->em->persist($produtocombo);
            } else {

                $array['dt_cadastro'] = new \DateTime();
                $array['id_usuario'] = $this->sessao->id_usuario;
                $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
                $produtocombo = $serialize->arrayToEntity($array, new \G2\Entity\ProdutoCombo());

                $this->em->persist($produtocombo);
            }
            $this->em->flush();

            $mensageiro = new \Ead1_Mensageiro($this->toArrayEntity($produtocombo), \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Excluí um Produto Combo
     * @param array $array
     * @throws Zend_Validate_Exception
     * @return \Ead1_Mensageiro
     */
    public function excluirProdutoCombo($id_produtocombo)
    {

        try {

            $entity = $this->em->getRepository('G2\Entity\ProdutoCombo')->find($id_produtocombo);
            if ($entity) {
                $this->em->remove($entity);
                $this->em->flush();
                $mensageiro = new \Ead1_Mensageiro("Produto excluído com sucesso do Combo", \Ead1_IMensageiro::SUCESSO);
            } else {
                throw new \Zend_Validate_Exception("Produto não encontrado no Combo!");
            }
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Método que exclúi as áreas de um Produto
     * @param unknown_type $id_produto
     * @throws \Zend_Validate_Exception
     * @return \Ead1_Mensageiro
     */
    public function excluirProdutoArea($id_produto)
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProdutoArea')->findBy(array('id_produto' => $id_produto));
            if ($result) {
                foreach ($result as $row) {
                    $this->em->remove($row);
                    $this->em->flush();
                }
                $mensageiro = new \Ead1_Mensageiro("Áreas excluídas com sucesso", \Ead1_IMensageiro::SUCESSO);
            } else {
                throw new \Zend_Validate_Exception("Áreas não encontradas!");
            }
        } catch (\Zend_Validate_Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Retorna o Produto pelo ID da entidade que está sendo passado pelo WS
     * @param array $entidade_id
     * @param array $parametros
     * @return type
     * @author Kayo Silva <kayo.silva@unyleya.com.br> Alterado por mim em 2014-12-12,
     * a pedido do Toffolo, para optimizar a consulta.
     * Foi trocado de lugar o local de fazer a query mandando os parametros para a
     * camada de repository e lá feito a consulta
     */
    public function listarProdutoWs(array $parametros, array $entidade_id = [])
    {
        try {
            $arrReturn = array();

            $result = $this->em->getRepository('G2\Entity\VwConsultaProduto')->listarProdutoWs($parametros, $entidade_id);

            if ($result) {
                foreach ($result as $key => $row) {

                    if (!array_key_exists($row['id_produto'], $arrReturn)) {
                        $arrReturn[$row['id_produto']] = array(
                            'id_produto' => $row['id_produto'],
                            'st_produto' => $row['st_produto'],
                            'bl_destaque' => $row['bl_destaque'],
                            'id_entidade' => $row['id_entidade'],
                            'st_nomeentidade' => $row['st_nomeentidade'],
                            'st_descricao' => $row['st_descricao'],
                            'dt_cadastro' => $row['dt_cadastro'],
                            'st_evolucao' => $row['st_evolucao'],
                            'id_evolucao' => $row['id_evolucao'],
                            'nu_valor' => $row['nu_valor'],
                            'bl_mostrarpreco' => $row['bl_mostrarpreco'],
                            'projeto' => array()
                        );


                        array_push($arrReturn[$row['id_produto']]['projeto'], array(
                            'id_produto' => $row['id_produto'],
                            'id_projetopedagogico' => $row['id_projetopedagogico'],
                            'nu_cargahoraria' => $row['nu_cargahoraria'],
                            'dt_inicio' => $row['dt_inicio'],
                            'st_turno' => $row['st_turno'],
                            'st_disciplina' => $row['st_disciplina'],
                            'st_diassemana' => $row['st_diassemana']
                        ));
                    } else {
                        array_push($arrReturn[$row['id_produto']]['projeto'], array(
                            'id_produto' => $row['id_produto'],
                            'id_projetopedagogico' => $row['id_projetopedagogico'],
                            'nu_cargahoraria' => $row['nu_cargahoraria'],
                            'dt_inicio' => $row['dt_inicio'],
                            'st_turno' => $row['st_turno'],
                            'st_disciplina' => $row['st_disciplina'],
                            'st_diassemana' => $row['st_diassemana']
                        ));
                    }
                }
            }

            return $arrReturn;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna o Produto pelo ID
     * @param type $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarProdutoWs($parametros)
    {
        try {
            //Recebendo as informações da requisição
            $id = $parametros['id_produto'];

            //Definição de variáveis:
            //$arrReturn: array de retorno das informações
            $arrReturn = array();

            //Query para retornar as informações básicas do curso
            $this->repositoryProduto = 'G2\Entity\VwConsultaProduto';
            $result = $this->em->getRepository($this->repositoryProduto)
                ->findBy(array('id_produto' => $id));


            if (!empty($result)) {
                //Transformando o resultado em uma entidade
                foreach ($result as $row) {
                    $arrReturn = $this->toArrayEntity($row);
                }

                //TODO: Procurar uma forma mais elegante de verificar se o produto se encaixa nas entidades corretas
                if ($arrReturn['id_entidade'] == 21 || $arrReturn['id_entidade'] == 22 || $arrReturn['id_entidade'] == 176 || $arrReturn['id_entidade'] == 177 || $arrReturn['id_entidade'] == 178 || $arrReturn['id_entidade'] == 115 || $arrReturn['id_entidade'] == 116) {
                    //Query para retornar as disciplinas relacionadas ao curso
                    //Concatenação do produto com as disciplinas
                    $arrDisciplinas = $this->findProdutoDisciplinaByProduto($id);
                    if ($arrDisciplinas->getType() == 'success') {
                        $arrReturn['disciplinas'] = $arrDisciplinas->getMensagem();
                    }

                    //Query para retornar as informações do projeto
                    $repo = $this->em->getRepository('G2\Entity\VwProdutoProjeto');
                    $query = $repo->createQueryBuilder('vw')
                        ->select('vw')
                        ->where('vw.id_produto = :id_produto')
                        ->setParameter('id_produto', $id)
                        ->orderBy('vw.id_produto', 'ASC');

                    //Concatenação do produto com as informações do projeto
                    $arrReturn['projeto'] = $query->getQuery()->getArrayResult();
                } else {
                    return $arrReturn = "Produto inexistente, por favor procure por outro ou entre em contato com o administrador na área 'Fale Conosco'";
                }
            } else {
                return $arrReturn = "Produto inexistente, por favor procure por outro ou entre em contato com o administrador na área 'Fale Conosco'";
            }


            return $arrReturn;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find Produto Disciplina By Id Produto
     * @param integer $idProduto
     * @return mixed G2\Entity\VwProdutoDisciplina
     */
    public function findProdutoDisciplinaByProduto($idProduto)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwProdutoDisciplina');
            $query = $repo->createQueryBuilder('vw')
                ->select('vw')
                ->where('vw.id_produto = :id_produto')
                ->setParameter('id_produto', $idProduto)
                ->orderBy('vw.id_produto', 'ASC');

            //Concatenação do produto com as disciplinas
            $result = $query->getQuery()->getArrayResult();
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function buscarCategoria(array $parametros = null)
    {
        try {
            $id = $parametros['id_entidadecadastro'];

            $repo = $this->em->getRepository('G2\Entity\Categoria');
            $query = $repo->createQueryBuilder('vw')
                ->select('vw')
                ->where('vw.id_entidadecadastro = :id_entidadecadastro')
                ->setParameter('id_entidadecadastro', $id)
                ->orderBy('vw.st_nomeexibicao', 'ASC');

            return $query->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function buscarProdutos(array $parametros = null)
    {
        $q = $parametros['q'];
        $t = $parametros['t'];
        try {
            switch ($t) {
                case 'cursos':
                    $repo = $this->em->getRepository('G2\Entity\VwProduto');
                    $query = $repo->createQueryBuilder('vw')
                        ->select('vw')
                        ->where('vw.id_entidade IN(21, 22)')
                        ->andWhere('vw.st_produto LIKE :query')
                        ->setParameter('query', '%' . $q . '%')
                        ->orderBy('vw.dt_cadastro', 'DESC');
                    return $query->getQuery()->getArrayResult();
                    break;
                case 'concursos':
                    $repo = $this->em->getRepository('G2\Entity\Concurso');
                    $query = $repo->createQueryBuilder('con')
                        ->select('con, sg')
                        ->join('con.sg_uf', 'sg')
//                        ->where('vw.id_entidade IN(21, 22)')
                        ->andWhere('con.st_concurso LIKE :query')
                        ->orWhere('con.st_orgao LIKE :query')
                        ->setParameter('query', '%' . $q . '%')
                        ->orderBy('con.dt_cadastro', 'DESC');
                    return $query->getQuery()->getArrayResult();
                    break;
                default:
                    return "Produto inexistente, por favor procure por outro ou entre em contato com o administrador na área 'Fale Conosco'";
                    break;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $data
     * @throws \Zend_Exception
     * @return \G2\Negocio\Ead1_Mensageiro
     */
    public function salvarProdutoCarreira(array $data)
    {
        try {

            if (isset($data['id_produtocarreira'])) {
                $entity = $this->find('G2\Entity\ProdutoCarreira', $data['id_produtocarreira']);
            } else {
                $entity = $this->em->getRepository('G2\Entity\ProdutoCarreira')->findOneBy($data);
            }
            if (!$entity) {
                $entity = new \G2\Entity\ProdutoCarreira();
            } else {
                return new \Ead1_Mensageiro("Vínculo já cadastrado!", \Ead1_IMensageiro::AVISO);
            }

            if (!isset($data['id_carreira']) || !isset($data['id_produto'])) {
                throw new \Zend_Exception("id_carreira e id_produto são de preenchimento obrigatório!");
            } else {
                $entity->montarEntity($data);
            }
            $return = $this->save($entity); //salva
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os registros da tb_produtocarreira e tb_carreira
     * @param integer $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarCarreirasProduto($id_produto)
    {
        try {

            $query = $this->em->createQueryBuilder('pc')->select('pc, c')->from('\G2\Entity\ProdutoCarreira', 'pc')->innerJoin('pc.id_carreira', 'c')->where('pc.id_produto = :id_produto')->setParameter('id_produto', $id_produto)->getQuery();

            $result = $query->getResult();
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function listarConcursoWs(array $parametros = null)
    {
        try {
            $limit = $parametros['limit'];
//            $id = $parametros['id_entidade'];

            $repo = $this->em->getRepository('G2\Entity\Concurso');
            $query = $repo->createQueryBuilder('vw')
                ->select('vw')
                ->where('vw.id_entidade IN (:id_as,:id_ag)')
                ->setParameter('id_as', 21)
                ->setParameter('id_ag', 22)
                ->setMaxResults($limit)
                ->orderBy('vw.dt_cadastro', 'DESC');

//            var_dump($query->getQuery()->getSQL());
//            die;

            return $query->getQuery()->getArrayResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva Produto Integração
     * @param array $data
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvaProdutoIntegracao(array $data)
    {
        try {
            if (!$data) {
                throw new \Zend_Exception("No data to persist.");
            } else {
                $entity = $this->findProdutoIntegracao($data); //Procura produto integracao
                if (!$entity) {
                    $entity = new \G2\Entity\ProdutoIntegracao();
                }
                if (isset($data['id_produto'])) {
                    $produto = $this->getReference($this->repositoryProduto, $data['id_produto']);
                    $entity->setId_produto($produto);
                }

                if (isset($data['id_sistema'])) {
                    $sistema = $this->getReference('\G2\Entity\Sistema', $data['id_sistema']);
                    $entity->setId_sistema($sistema);
                }

                $entity->setNu_codprodutosistema($entity->getNu_codprodutosistema() ? $entity->getNu_codprodutosistema() : $data['nu_codprodutosistema']);
                $result = $this->save($entity);
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Procura Produto Integracao e verifica se existe
     * @param array $data
     * @return mixed boolean or \G2\Entity\ProdutoIntegracao
     */
    private function findProdutoIntegracao(array $data)
    {
        try {
            if (isset($data['id_produtointegracao'])) {
                $result = $this->getReference('G2\Entity\ProdutoIntegracao', $data['id_produtointegracao']);
            } else {
                $param = array();
                foreach ($data as $key => $value) {
                    if ($value)
                        $param[$key] = $value;
                }
                $result = $this->em->getRepository('G2\Entity\ProdutoIntegracao')->findOneBy($param);
            }
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva o Produto Área
     * @param array $data
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoArea(array $data)
    {
        try {

            if (isset($data['id_produtoarea']) && $data['id_produtoarea']) {
                $entity = $this->find('G2\Entity\ProdutoArea', $data['id_produtoarea']);
            } else {
                $entity = $this->em->getRepository('G2\Entity\ProdutoArea')->findOneBy($data);
            }
            if (!$entity) {
                $entity = new \G2\Entity\ProdutoArea();
            } else {
                return new \Ead1_Mensageiro("Vínculo já cadastrado!", \Ead1_IMensageiro::AVISO);
            }

            if (!isset($data['id_areaconhecimento']) || !isset($data['id_produto'])) {
                throw new \Zend_Exception("id_areaconhecimento e id_produto são de preenchimento obrigatório!");
            } else {
                $entity->montarEntity($data);
            }
            $return = $this->save($entity); //salva
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna as Áreas de um Produto
     * @param unknown_type $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarAreasProduto($id_produto)
    {
        try {

            $query = $this->em->createQueryBuilder('pc')->select('pc, c')->from('\G2\Entity\ProdutoArea', 'pc')->innerJoin('pc.id_areaconhecimento', 'c')->where('pc.id_produto = :id_produto')->setParameter('id_produto', $id_produto)->getQuery();

            $result = $query->getResult();
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva o Produto UF
     * @param array $data
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function salvarProdutoUf(array $data)
    {
        try {

            if (isset($data['id_produtouf'])) {
                $entity = $this->find('G2\Entity\ProdutoUf', $data['id_produtouf']);
            } else {
                $entity = $this->em->getRepository('G2\Entity\ProdutoUf')->findOneBy($data);
            }
            if (!$entity) {
                $entity = new \G2\Entity\ProdutoUf();
            } else {
                return new \Ead1_Mensageiro("Vínculo já cadastrado!", \Ead1_IMensageiro::AVISO);
            }

            $entity->montarEntity($data);
            if (!$entity->getSg_uf() || !$entity->getId_produto()) {
                throw new \Zend_Exception("sg_uf e id_produto são de preenchimento obrigatório!");
            }

            $return = $this->save($entity); //salva
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna as UFs de um Produto
     * @param integer $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarUfsProduto($id_produto)
    {
        try {

            $query = $this->em->createQueryBuilder('pc')->select('pc, c')->from('\G2\Entity\ProdutoUf', 'pc')->innerJoin('pc.sg_uf', 'c')->where('pc.id_produto = :id_produto')->setParameter('id_produto', $id_produto)->getQuery();

            $result = $query->getResult();
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna as Categorias de um Produto
     * @param int $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarCategoriasProduto($id_produto = null)
    {

        try {

            if ($id_produto) {
                $result = $entity = $this->em->getRepository('G2\Entity\VwCategoriaProduto')->findBy(array('id_produto' => $id_produto, 'id_entidadecadastro' => $this->sessao->id_entidade));
            } else {
                $result = $entity = $this->em->getRepository('G2\Entity\VwCategoria')->findBy(array('id_entidadecadastro' => $this->sessao->id_entidade, 'st_situacao' => 'Ativa'));
            }

            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna as ÁREAS DE UM PRODUTO
     * @param unknown_type $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarVwAreasProduto($id_produto)
    {

        try {

            $result = $entity = $this->em->getRepository('G2\Entity\VwProdutoArea')->findBy(array('id_produto' => $id_produto, 'id_entidade' => $this->sessao->id_entidade));

            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna dados de VwProdutoArea
     * @param array $where
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwProdutoArea(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VwProdutoArea', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao retornar dados de VwProdutoArea" . $e->getMessage());
        }
    }

    /**
     * Retorna as Entidades de um Produto
     * @param unknown_type $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarEntidadesProduto($id_produto)
    {
        try {

            $query = $this->em->createQueryBuilder('pc')->select('pc, c')->from('\G2\Entity\ProdutoEntidade', 'pc')->innerJoin('pc.id_entidade', 'c')->where('pc.id_produto = :id_produto')->setParameter('id_produto', $id_produto)->getQuery();

            $result = $query->getResult();
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * @param array $arrEntidades
     * @return \Ead1_Mensageiro
     */
    public function vinculaEntidadeProdutoWS(array $arrEntidades, $idProduto)
    {
        $mensageiro = new \Ead1_Mensageiro();
        $this->em->getConnection()->beginTransaction();
        $arrReturn = array();
        try {
            if ($idProduto) {
                $produto = $this->getReference('\G2\Entity\Produto', $idProduto);
                $delete = $this->deleteEntidadeProdutoByProduto($idProduto);
                if ($delete->getType() != 'success') {
                    throw new \Zend_Exception("Erro ao desvincular Produtos Disciplina - " . $delete->getText());
                }

                foreach ($arrEntidades as $entidade) {
                    $entidade = $this->getReference('\G2\Entity\Entidade', $entidade);
                    $entity = new \G2\Entity\ProdutoEntidade();
                    $entity->setId_entidade($entidade)
                        ->setId_produto($produto);
                    $result = $this->save($entity);
                    if ($result) {
                        $arrReturn['entidades'][] = array(
                            'id_produtoentidade' => $result->getId_produtoentidade(),
                            'id_entidade' => $result->getId_entidade()->getId_entidade(),
                            'st_nomeentidade' => $result->getId_entidade()->getSt_nomeentidade(),
                        );
//                        $this->toArrayEntity($result);
                    }
                }
            }
            $this->em->getConnection()->commit();
            return $mensageiro->setMensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->em->getConnection()->rollback();
            $this->em->close();
            return $mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Deleta Entides Produto By id_produto
     * @param integer $idProduto
     * @return \Ead1_Mensageiro
     */
    public function deleteEntidadeProdutoByProduto($idProduto)
    {
        $result = true;
        $mensageiro = new \Ead1_Mensageiro();
        try {
            $resultEntidaeProduto = $this->findBy('\G2\Entity\ProdutoEntidade', array('id_produto' => $idProduto));
            if ($resultEntidaeProduto) {
                foreach ($resultEntidaeProduto as $row) {
                    $entidade = $this->find('\G2\Entity\ProdutoEntidade', $row->getId_produtoentidade());
                    if (!$this->delete($entidade))
                        $result = false;
                }
            }
            if ($result) {
                return $mensageiro->setMensageiro('Produtos Entidade Desvinculadas', \Ead1_IMensageiro::SUCESSO);
            } else {
                return $mensageiro->setMensageiro('Erro ao desvincular Produtos Entidade', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            return $mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * @param array $dados
     * @return type
     */
    public function salvarSalaPorDisciplinaWs(array $dados)
    {
        try {
            if (isset($dados['modulodisciplina'])) {
                $mensageiro = new \Ead1_Mensageiro();
                $arrSaladeAula = array();
                $arrReturn = array();
                $disciplinaNegocio = new Disciplina(); //Instancia a disciplina negocio
                $gerencia = new GerenciaSalas();
//                $count = count($dados['modulodisciplina']);
//                $arrSaladeAula['nu_linhas'] = $count;
                foreach ($dados['modulodisciplina'] as $i => $disciplina) { // percorre o array de disciplina
                    $index = ($i > 0 ? $i : null);
                    $resDisc = $disciplinaNegocio->getReference($disciplina['id_disciplina']); //Procura a disciplina por id
                    //verifica se a disciplina existe
                    if ($resDisc) {
                        $arrSaladeAula['st_saladeaula' . $index] = $resDisc->getSt_disciplina();
                        $arrSaladeAula['dt_inicioinscricao' . $index] = date('d/m/Y');
                        $arrSaladeAula['dt_fiminscricao' . $index] = null;
                        $arrSaladeAula['dt_encerramento' . $index] = null;
                        $arrSaladeAula['dt_abertura' . $index] = date('d/m/Y');
                        $arrSaladeAula['id_avaliacaoconjunto' . $index] = 11;
                        $arrSaladeAula['id_categoriasala' . $index] = 1;
                        $arrSaladeAula['id_usuario' . $index] = "1#1";
                        $arrSaladeAula['salaDeAulaDisciplina'][] = $resDisc->getId_disciplina();
                        $arrSaladeAula['nu_diasextensao'] = null;
                        $arrSaladeAula['id_periodoletivo'] = null;
                        $arrSaladeAula['nu_diasencerramento'] = null;
                        $arrSaladeAula['nu_diasaluno'] = null;
                        $arrSaladeAula['id_modalidadesaladeaula'] = 2;
                        $arrSaladeAula['id_situacao'] = 8;
                        $arrSaladeAula['nu_maxalunos'] = 999;
                        $arrSaladeAula['id_tiposaladeaula'] = 1;
                        $arrSaladeAula['bl_usardoperiodoletivo'] = true;
                        $arrSaladeAula['salaDeAulaEntidade'] = $resDisc->getId_entidade();
                        $arrSaladeAula['salaDeAulaProjeto'] = $dados['projetopedagogico']['id_projetopedagogico'];
                        $arrSaladeAula['bl_semencerramento'] = 1;
                        $arrSaladeAula['bl_semdiasaluno'] = 1;
                        $arrSaladeAula['entidades'] = array($resDisc->getId_entidade());
                        $arrSaladeAula['nu_linhas'] = 0;
                        $return[] = $gerencia->salvaSalaDeAula($arrSaladeAula);
                    }
                }
            }
            $arrReturn = array();
            if ($return) {
                foreach ($return as $objMensageiro) {
                    $mensagem = $objMensageiro->getMensagem();
                    foreach ($mensagem as $mens) {
                        $arrReturn[] = $mens->toArray();
                    }
                }
            }
            return $mensageiro->setMensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return $mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna coordenador do curso by id do produto
     * @param integer $idProduto
     */
    public function retornaCoordenadorCursoByIdProduto($idProduto)
    {
        try {
            $pessoaNegocio = new Pessoa();
            $resultProjeto = $this->findBy('\G2\Entity\ProdutoProjetoPedagogico', array('id_produto' => $idProduto));
            $arrReturn = array();
            if ($resultProjeto) {
                foreach ($resultProjeto as $i => $projeto) {
                    $arrResult = $pessoaNegocio->retornaVwUsuarioPerfilEntidadeReferencia(array(
                        'id_projetopedagogico' => $projeto->getId_projetopedagogico(),
                        'bl_ativo' => TRUE
                    ));
                    $arrReturn = array_merge($arrReturn, $arrResult);
                }
            }

            return $arrReturn;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Retorna Produto por parametros
     * @param array $params
     * @return \G2\Entity\VwProduto
     * @throws \Zend_Exception
     */
    public function retornarProduto(array $params = array())
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade; //força o id_entidade
            return $this->em->getRepository('\G2\Entity\VwProduto')->pesquisarProduto($params);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna os produtos de VwProdutoCompartilhado
     * @param array $where
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaVwProdutoCompartilhado(array $where = array())
    {
        try {
            $where['id_entidade'] = !empty($where['id_entidade']) ? $where['id_entidade'] : $this->sessao->id_entidade;

            /** @var \G2\Repository\Produto $rep */
            $rep = $this->em->getRepository('\G2\Entity\VwProdutoCompartilhado');

            return $rep->retornarVwProdutoCompartilhado($where);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna produto consulta por ID
     * @param integer $idProduto Id do produto
     * @return mixed | \G2\Entity\Produto
     * @throws \Zend_Exception
     */
    public function findProduto($idProduto)
    {
        try {
            return $this->find($this->repositoryProduto, $idProduto);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar produto. " . $e->getMessage());
        }
    }

    /**
     * Retorna Forma Disponibilizacao para cadastro do produto declaração
     * @throws \Zend_Exception
     */
    public function findAllFormaDisponibilizacao()
    {
        try {
            $resultado = $this->findAll('\G2\Entity\FormaDisponibilizacao');
            foreach ($resultado as &$value) {
                $value = $this->toArrayEntity($value);
            }
            return $resultado;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao Forma de Disponibilizacao." . $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @param \DateTime|\Zend_Date|string $dt_vigencia
     * @return \G2\Entity\VwProdutoValorTransferencia|null
     * @throws \Exception
     */
    public function retornarValorPorData($params = array(), $dt_vigencia = null)
    {
        if (empty($params['id_produto']) && empty($params['id_projetopedagogico'])) {
            throw new \Exception('É necessário informar o Produto OU o Projeto Pedagógico.');
        }

        // Se não informada, a data de vigência será a data atual
        $dt_vigencia = $dt_vigencia ? \G2\Utils\Helper::converterData($dt_vigencia, 'Y-m-d') : date('Y-m-d');

        // Buscar valor do produto ATIVO NA DATA INFORMADA
        /** @var \G2\Entity\VwProdutoValorTransferencia[] $dadosProduto */
        $produtoValor = $this->findCustom(
            '\G2\Entity\VwProdutoValorTransferencia',
            $params,
            array(
                "tb.dt_inicio <= '{$dt_vigencia}' AND " .
                "(tb.dt_termino >= '{$dt_vigencia}' OR tb.dt_termino IS NULL)"
            ),
            array('dt_inicio' => 'ASC'),
            1
        );

        // O findCustom traz um array, retornar o primeiro item
        return $produtoValor ? array_shift($produtoValor) : null;
    }

}

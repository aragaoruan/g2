<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Constante\CategoriaSala;
use G2\Constante\TipoDisciplina;
use G2\Constante\TipoNota;
use G2\Entity\Turma;
use G2\Entity\VwAvaliacaoAluno;

/**
 * Classe de negócio para Transferencia de projeto pedagogico
 * @author Denise Xavier <denise.xavier@unyleya.com.br>;
 */
class Transferencia extends Negocio
{

    public function findByVwAreaConhecimento()
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwAreaConhecimento';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByPesquisaProjetoModal($parametros)
    {
        try {

            $repositoryName = 'G2\Entity\VwProdutoProjetoTipoValor';
            $result = $this->em->createQuery("SELECT 	p
							   FROM " . $repositoryName . " p
			         			    WHERE p.bl_ativo = 1
							    " . (($parametros['id_entidade'] != '' && !empty($parametros['id_entidade']) ? ' AND p.id_entidade = ' . $parametros['id_entidade'] : '')) . "
							    " . (($parametros['id_areaconhecimento'] != '' && !empty($parametros['id_areaconhecimento']) ? ' AND p.id_areaconhecimento = ' . $parametros['id_areaconhecimento'] : '')) . "
							    " . (($parametros['id_nivelensino'] != '' && !empty($parametros['id_nivelensino']) ? ' AND p.id_nivelensino = ' . $parametros['id_nivelensino'] : '')) . "
							    " . (($parametros['st_projetopedagogico'] != '' && !empty($parametros['st_projetopedagogico']) ? " AND p.st_projetopedagogico  like '%" . $parametros['st_projetopedagogico'] . "%'" : '')) . "
					    ORDER BY p.st_projetopedagogico ASC")->getResult();
            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function retornarGridOrigem($parametros)
    {
        try {
            $repositoryName = 'G2\Entity\VwSalaDisciplinaParaTransferencia';
            $result = $this->em->createQuery("SELECT 	vw
							   FROM " . $repositoryName . " vw
			         			    WHERE vw.id_entidadetransfere = " . $this->sessao->id_entidade . "
			         			AND vw.id_tipodisciplina <>  " . TipoDisciplina::AMBIENTACAO . "   
							    " . (($parametros['id_projetotransfere'] != '' && !empty($parametros['id_projetotransfere']) ? ' AND vw.id_projetotransfere = ' . $parametros['id_projetotransfere'] : '')) . "
							    " . (($parametros['id_matricula'] != '' && !empty($parametros['id_matricula']) ? ' AND vw.id_matricula = ' . $parametros['id_matricula'] : '')) . "
							    " . ((array_key_exists('id_turma', $parametros) && $parametros['id_turma'] != '' && !empty($parametros['id_turma']) ? ' AND vw.id_turmatransfere = ' . $parametros['id_turma'] : '')) . "
					    ORDER BY vw.st_disciplina ASC")->getResult();
            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function retornarGridDestino($parametros)
    {
        try {

            $repositoryName = 'G2\Entity\VwSalaDisciplinaTransferencia';
            $result = $this->em->createQuery("SELECT 	vw
							   FROM " . $repositoryName . " vw
			         			    WHERE vw.id_entidade = " . $this->sessao->id_entidade . "
			         			    AND vw.nu_maxalunos > vw.nu_alocados
			         			    AND vw.id_tipodisciplina <>  " . TipoDisciplina::AMBIENTACAO . "   
			         			    AND vw.nu_maxalunos > vw.nu_alocados 
							    " . (($parametros['id_projetotransfere'] != '' && !empty($parametros['id_projetotransfere']) ? ' AND vw.id_projetopedagogico = ' . $parametros['id_projetotransfere'] : '')) . "
							    " . ((array_key_exists('id_turma', $parametros) && $parametros['id_turma'] != '' && !empty($parametros['id_turma']) ? ' AND vw.id_turma = ' . $parametros['id_turma'] : '')) . "
					    ORDER BY vw.st_disciplina ASC
					    , vw.dt_abertura ASC")->getResult();

            $array_return = array();
            foreach ($result as $key => $row) {
                if (!$key || $row->getId_disciplina() !== $result[$key - 1]->getId_disciplina()) {
                    $array_return[$key] = $row->_toArray();
                }
            }

            return array_values($array_return);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    public function transferirAluno($dados)
    {

        $dao = new \MatriculaDAO();
        $bo = new \MatriculaBO();
        $mensageiroRetorno = new \Ead1_Mensageiro();

        try {
            if (!empty($dados['disciplinas_aproveitar'])) {
                $id_disciplinas = $dados['disciplinas_aproveitar'];
            } else {
                $id_disciplinas = false;
            }

            $dao->beginTransaction();

            $entityMatricula = $this->getReference('\G2\Entity\Matricula', $dados['id_matriculaorigem']);
            $entityProjetoDestino = $this->getReference('\G2\Entity\ProjetoPedagogico', $dados['id_projetodestino']);

            $entityTurma = null;

            if (!empty($dados['id_turma'])) {
                $entityTurma = $this->find('\G2\Entity\Turma', $dados['id_turma']);
                if (!($entityTurma instanceof \G2\Entity\Turma)) {
                    throw new \Exception('Erro ao pesquisar os dados da turma de destino.');
                }
            }

            $objContratoMatricula = $this->findOneBy('\G2\Entity\ContratoMatricula', array('id_matricula' => $dados['id_matriculaorigem']));

            if ($objContratoMatricula instanceof \G2\Entity\ContratoMatricula) {
                $matriculaTOrigem = new \MatriculaTO();
                $matriculaTOrigem->setId_matricula($dados['id_matriculaorigem']);
                $matriculaTOrigem->fetch(false, true, true);

                if ($matriculaTOrigem->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULA_TRANSFERIDO) {
                    throw new \Exception('Matrícula já foi transferida!');
                }

                //Inserindo via doctrine pois estava acontecendo erro utilizando a TO para inativar o registro.
                $objContratoMatricula->setBl_ativo(false);
                $result = $this->save($objContratoMatricula);

                if (!($result instanceof \G2\Entity\ContratoMatricula)) {
                    throw new \Exception('Erro ao inativar Contrato Matricula antigo.');
                }

                $contratoMatriculaTO = $this->entityToTO($objContratoMatricula);

                $matriculaTO = new \MatriculaTO();
                $matriculaTO->setId_matriculaorigem($dados['id_matriculaorigem']);
                $matriculaTO->setId_usuario($matriculaTOrigem->getId_usuario());
                $matriculaTO->setId_projetopedagogico($dados['id_projetodestino']);
                $matriculaTO->setId_turma(!empty($dados['id_turma']) ? (int)$dados['id_turma'] : null);
                $matriculaTO->setId_entidadeatendimento($matriculaTOrigem->getId_entidadeatendimento());
                $matriculaTO->setId_entidadematricula($matriculaTOrigem->getId_entidadematricula());
                $matriculaTO->setId_vendaproduto($matriculaTOrigem->getId_vendaproduto());
                $mensageiro = $bo->matricularDireto($matriculaTO, true, false, $id_disciplinas);

                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception('Erro ao matricular direto: ' . $mensageiro->getFirstMensagem());
                }

                $matriculaTO = $mensageiro->getFirstMensagem();
                $contratoMatriculaTO->setId_matricula($matriculaTO->getId_matricula());
                $contratoMatriculaTO->setBl_ativo(1);

                $mensageiroContrato = $bo->cadastrarContratoMatricula($contratoMatriculaTO);

                if ($mensageiroContrato->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception($mensageiroContrato->getFirstMensagem());
                }
            } else {
                throw new \Exception('Matrícula de Origem não tem contrato!');
            }

            $matriculaTOrigem = new \MatriculaTO();
            $matriculaTOrigem->setId_matricula($dados['id_matriculaorigem']);
            $matriculaTOrigem->setId_situacao(\MatriculaTO::SITUACAO_NAO_ATIVADA);
            $matriculaTOrigem->setId_evolucao(\MatriculaTO::EVOLUCAO_TRANSFERIDO);

            $mensageiro = $bo->editarMatricula($matriculaTOrigem);

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($mensageiro->getFirstMensagem());
            }

            $salaBO = new \SalaDeAulaBO();

            /** Desaloca as salas de aula que nao foram encerradas da matricula de origem
             * Independente do aproveitamento, as salas não encerradas serão desalocadas
             */
            $mensageiro = $salaBO->desalocarMatriculaDisciplinaTransferenciaCurso($matriculaTOrigem->getId_matricula());

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($mensageiro->getFirstMensagem());
            }

            // Salvar trâmite de transferência
            $tramiteBO = new \TramiteBO();
            $tramiteTO = new \TramiteTO();
            $tramiteTO->setId_tipotramite(\G2\Constante\TipoTramite::MATRICULA_TRANSFERENCIA);
            $tramiteTO->setBl_visivel(1);
            $tramiteTO->setId_entidade($this->sessao->id_entidade);
            $tramiteTO->setId_usuario($this->sessao->id_usuario);

            $tramiteTO->setSt_tramite('Aluno transferido do curso [' .
                $entityMatricula->getId_projetopedagogico()->getSt_projetopedagogico() .
                '] da turma [' .
                ($entityMatricula->getId_turma() instanceof \G2\Entity\Turma ?
                    $entityMatricula->getId_turma()->getSt_turma() : ' - ') .
                '] para o curso [' .
                $entityProjetoDestino->getSt_projetopedagogico() .
                '] turma [' .
                ($entityTurma instanceof \G2\Entity\Turma ? $entityTurma->getSt_turma() : ' - ') .
                ']. Motivo: [' . $dados['st_tramite'] . ']');

            $tramiteBO->cadastrarMatriculaTramite($matriculaTO, $tramiteTO);

            unset($tramiteTO->id_tramite);
            $tramiteBO->cadastrarMatriculaTramite($matriculaTOrigem, $tramiteTO);

            //recupera o id da venda
            $id_venda = null;
            $vendaProduto = $this->findOneBy('\G2\Entity\VendaProduto', array(
                'id_matricula' => $dados['id_matriculaorigem']
            ));

            if ($vendaProduto instanceof \G2\Entity\VendaProduto) {
                $id_venda = $vendaProduto->getId_venda()->getId_venda();
            } else {
                //se não encontrar na a venda na tb_vendaproduto, pesquisa na tb_vendaaditivo
                // (o aluno pode ter uma trasnferência)
                $vendaAditivo = $this->findOneBy('\G2\Entity\VendaAditivo', array(
                    'id_matricula' => $dados['id_matriculaorigem']
                ));
                if ($vendaAditivo instanceof \G2\Entity\VendaAditivo) {
                    if ($vendaAditivo->getId_venda()->getId_venda()) {
                        $id_venda = $vendaAditivo->getId_venda()->getId_venda();
                    } else {
                        //Se ele encontrar a venda na tb_vendaaditivo, mas não encontrar os valores da
                        // negociação, pesquisa na tb_vendaproduto agora com o id_venda
                        if ($vendaAditivo->getId_venda()) {
                            $vendaProdutoByIdVenda = $this->findOneBy('\G2\Entity\VendaProduto', array(
                                'id_venda' => $vendaAditivo->getId_venda()
                            ));
                            if ($vendaProdutoByIdVenda instanceof \G2\Entity\VendaProduto) {
                                $id_venda = $vendaProdutoByIdVenda->getId_venda()->getId_venda();
                            }
                        }
                    }
                } else {

                    $id_matriculaorigem = $dados['id_matriculaorigem'];
                    do {
                        //A venda pode ter sido transferida pelo método antigo
                        //  então pesquisa na tb_matrícula a matrícula que será feita a transferência
                        $enMatricula = $this->findOneBy('\G2\Entity\Matricula',
                            array('id_matricula' => $id_matriculaorigem));
                        if (!($enMatricula instanceof \G2\Entity\Matricula)) {
                            throw new \Exception('Erro ao pesquisa a matricula de da transferência.');
                        }

                        $id_matriculaorigem = $enMatricula->getId_matriculaorigem()->getId_matricula();

                        //Pesquisa na tb_vendaproduto com a matrícula de origem retornada da pesquisa acima
                        $enVendaProdutoOrigem = $this->findOneBy('\G2\Entity\VendaProduto',
                            array('id_matricula' => $id_matriculaorigem));

                    } while (!($enVendaProdutoOrigem instanceof \G2\Entity\VendaProduto));

                    $id_venda = $enVendaProdutoOrigem->getId_venda()->getId_venda();
                }
            }

            //Salvar o a venda aditivo e o aditivo de transferência na pasta "minha pasta" do aluno
            $vendaAditivo = $dados['venda_aditivo'];
            if (!$vendaAditivo) {
                throw new \Exception('Dados do aditivo da venda não encontrados.');
            }

            $vendaBO = new \VendaBO;
            $id_vendaaditivo = $vendaBO->salvarAditivoTransferencia($objContratoMatricula
                , $dados['id_matriculaorigem']
                , $matriculaTO->getId_matricula()
                , $vendaAditivo
                , $id_venda);

            if (!$id_vendaaditivo) {
                throw new \Exception('Erro ao salvar o aditivo da venda.');
            }

            //verifica se o valor para o aluno pagar, para gerar os lançamentos para o aluno
            if ($vendaAditivo['nu_valorapagar'] > 0) {
                //Salva os lançamentos e as  suas referencias na tb_lancamentovenda
                if (!empty($dados['lancamentos'])) {
                    $arrLancamentos = $dados['lancamentos'];
                    $mensageiroLancamentos = $this->salvarLancamentosTransferencia(
                        $arrLancamentos, $id_venda, $id_vendaaditivo
                    );

                    if ($mensageiroLancamentos->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                        throw new \Exception('Erro ao salvar os lançamentos.');
                    }
                } else {
                    throw new \Exception('Lançamentos não encontrados.');
                }
            }

            //Gera o aditivo da transferencia
            if ($id_vendaaditivo) {
                //salva na pasta do aluno se for criada a venda aditivo
                $this->geraArquivoAditivo($objContratoMatricula
                    , $dados['id_matriculaorigem']
                    , $matriculaTO->getId_matricula());
            }
            $dao->commit();

            //se tiver disciplinas para aproveitar
            if (is_array($id_disciplinas)) {
                $this->lancaNotasAproveitamentoTransferencia(
                    $matriculaTO->getId_matricula(), $id_disciplinas, $dados['id_matriculaorigem']
                );
            }

            return $mensageiroRetorno
                ->setMensageiro('Transferência efetuada com sucesso.', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $dao->rollBack();
            return $mensageiroRetorno
                ->setMensageiro('Erro ao transferir alunos: ' . $e->getMessage(),
                    \Ead1_IMensageiro::ERRO, $e->getMessage());
        }

    }

    /**
     * @param $id_matricula
     * @param $id_disciplinas
     * @param $id_matriculaorigem
     */
    public function lancaNotasAproveitamentoTransferencia($id_matricula, $id_disciplinas, $id_matriculaorigem)
    {
        $avaliacaoBO = new \AvaliacaoBO();
        foreach ($id_disciplinas as $id_disciplina) {
            $vw_avaliacaoaluno = $this->trataNotasParaAproveitamento($id_matriculaorigem, $id_disciplina);
            $vw_avaliacaoalunoNew = $this->findOneBy('G2\Entity\VwAvaliacaoAluno', array(
                'id_matricula' => $id_matricula, 'id_disciplina' => $id_disciplina
            ));

            if (count($vw_avaliacaoaluno) > 0
                && ($vw_avaliacaoalunoNew instanceof VwAvaliacaoAluno
                    && $vw_avaliacaoalunoNew->getId_avaliacaoconjuntoreferencia())
            ) {
                foreach ($vw_avaliacaoaluno as $vw) {
                    if (!is_null($vw->getSt_nota())) {
                        $to = new \AvaliacaoAlunoTO();
                        $to->setId_matricula($id_matricula);
                        $to->setId_avaliacaoconjuntoreferencia($vw_avaliacaoalunoNew
                            ->getId_avaliacaoconjuntoreferencia());

                        $to->setId_avaliacao($vw->getId_avaliacao());
                        $to->setId_avaliacaoagendamento($vw->getId_avaliacaoagendamento());
                        $to->setSt_nota($vw->getSt_nota());
                        $to->setDt_avaliacao($vw->getDt_avaliacao());
                        $to->setBl_ativo(1);
                        $to->setId_usuariocadastro($this->sessao->id_usuario);
                        $to->setId_tiponota(TipoNota::APROVEITAMENTO);
                        $to->setId_situacao(\AvaliacaoAlunoTO::ATIVO);
                        $to->setId_avaliacaoalunoorigem($vw->getId_avaliacaoaluno());
                        $to->setId_upload($vw->getId_upload());
                        $to->setSt_tituloavaliacao($vw->getSt_tituloavaliacao());
                        $to->setDt_defesa($vw->getDt_defesa());
                        $to->setSt_justificativa($vw->getSt_justificativa());
                        $retorno = $avaliacaoBO->salvarAvaliacaoAluno($to);
                    }
                }
            }
        }
    }

    /**
     * @param $array_params
     * @return array|string
     */
    public function retornaTurmas($array_params)
    {
        try {
            if (empty($array_params['id_projetopedagogico'])) {
                throw new \Exception("É necessário informar o ID do Projeto Pedagógico.");
            }

            $id_entidade = empty($array_params['id_entidade']) ? $this->getId_entidade() : $array_params['id_entidade'];

            $sql = "SELECT 
                        tp.id_turmaprojeto,
                        tp.id_turma,
                        tp.id_projetopedagogico,
                        tm.id_entidadecadastro,
                        tm.st_turma,
                        tm.st_tituloexibicao
                    FROM 
                        tb_turmaprojeto tp
                        OUTER APPLY (
                            SELECT COUNT(id_matricula) AS nu_alunos
                            FROM dbo.tb_matricula
                            WHERE id_turma = tp.id_turma
                        ) AS mt
                        JOIN tb_turma AS tm ON tm.id_turma = tp.id_turma AND tm.bl_ativo = 1 
                        AND mt.nu_alunos < tm.nu_maxalunos
                        JOIN tb_turmaentidade AS te ON te.id_turma = tm.id_turma
                    WHERE
                        tp.bl_ativo = 1
                        AND id_projetopedagogico = " . $array_params['id_projetopedagogico'] . "
                        AND te.id_entidade =  " . $id_entidade;

            if (!empty($array_params['id_situacao'])) {
                $sql .= ' AND tm.id_situacao = ' . $array_params['id_situacao'];
            }

            $sql .= " ORDER BY tm.dt_inicio DESC, tm.st_turma ASC";

            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $data
     * @return array|\Ead1_Mensageiro|string
     */
    public function transferirAlunoTurma($data)
    {
        $response = [];

        try {
            /**
             * @var \G2\Entity\Matricula $en_matricula
             */
            $en_matricula = $this->find('\G2\Entity\Matricula', $data['id_matricula']);

            /**
             * @var \G2\Entity\Turma $en_turma
             */
            $en_turma = $this->getReference('\G2\Entity\Turma', $data['id_turma']);

            $ng_alocacao = new \G2\Negocio\Alocacao();


            // Desalocar das salas que não foram selecionadas para fazer a realocação
            if (!empty($data['id_disciplina'])) {
                foreach ($data['id_disciplina'] as $id_disciplina) {
                    //Se a disciplina tiver PRR ele deve desalocar das 2 salas, por isso ele procura com findBy
                    // e não com o findOneby
                    $array_result = $this->findBy('\G2\Entity\VwSalaDisciplinaAlocacao', array(
                        'id_matricula' => $data['id_matricula'],
                        'id_disciplina' => $id_disciplina,
                        'bl_ativo' => true
                    ));
                    if (is_array($array_result)) {
                        foreach ($array_result as $result) {
                            if ($result instanceof \G2\Entity\VwSalaDisciplinaAlocacao) {
                                $ng_alocacao->desalocarAluno($this->toArrayEntity($result));
                            }
                        }
                    }

                }
            }

            // Alterar a Turma de Origem somente se a nova turma for diferente
            if (!$en_matricula->getId_turmaorigem()
                || ($en_matricula->getId_turmaorigem()->getId_turma() != $en_turma->getId_turma())
            ) {
                $en_matricula->setId_turmaorigem($en_matricula->getId_turma());
            }

            // Alterar a turma da Matrícula do Aluno
            $en_matricula->setId_turma($en_turma);
            $this->save($en_matricula);

            $ng_matricula = new \G2\Negocio\Matricula();

            // Gerar o Trâmite
            $ng_matricula->gerarTramite($en_matricula,
                array(
                    'id_tipotramite' => \G2\Constante\TipoTramite::MATRICULA_TRANSFERENCIA,
                    'st_tramite' => "Transferido da turma {$en_matricula->getId_turmaorigem()
                    ->getSt_tituloexibicao()} para turma {$en_matricula->getId_turma()
                    ->getSt_tituloexibicao()}"
                ));
            if ($en_matricula->getId_turmaorigem()) {
                $ng_matricula->gerarTramite($en_matricula, array(
                    'id_tipotramite' => \G2\Constante\TipoTramite::MATRICULA_TRANSFERENCIA_TURMA,
                    'st_tramite' => "Transferido da turma {$en_matricula->getId_turmaorigem()
                    ->getSt_tituloexibicao()} para turma {$en_matricula->getId_turma()->getSt_tituloexibicao()}"
                ));
            }

            // Realiza a Auto Alocação
            $to_matricula = new \MatriculaTO();
            $to_matricula->setId_matricula($en_matricula->getId_matricula());
            $to_matricula->fetch(true, true, true);

            if (!empty($data['id_disciplina'])) {

                $response = $ng_matricula->alocarAutomatico($to_matricula);
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return $response;
    }

    /**
     * Retorna grid concatenando as disciplinas de PRR para exibição na grade de origem da transferencia de turma
     * @param $parametros
     * @return array|string
     */
    public function retornarGridOrigemOrdenada($parametros)
    {
        try {
            $repositoryName = 'G2\Entity\VwSalaDisciplinaParaTransferencia';
            $result = $this->em->createQuery("SELECT 	vw
							   FROM " . $repositoryName . " vw
			         			    WHERE vw.id_entidadetransfere = " . $this->sessao->id_entidade . "
			         			AND vw.id_tipodisciplina <>  " . TipoDisciplina::AMBIENTACAO . "   
							    " . (($parametros['id_projetotransfere'] != '' && !empty($parametros['id_projetotransfere']) ? ' AND vw.id_projetotransfere = ' . $parametros['id_projetotransfere'] : '')) . "
							    " . (($parametros['id_matricula'] != '' && !empty($parametros['id_matricula']) ? ' AND vw.id_matricula = ' . $parametros['id_matricula'] : '')) . "
							    " . ((array_key_exists('id_turma', $parametros) && $parametros['id_turma'] != '' && !empty($parametros['id_turma']) ? ' AND vw.id_turmatransfere = ' . $parametros['id_turma'] : '')) . "
					    ORDER BY vw.st_disciplina ASC")->getResult();

            $arrReturn = array();

            if (is_array($result)) {
                foreach ($result as $key => $row) {
                    if ($row->getId_categoriasala() == CategoriaSala::NORMAL || !$row->getId_categoriasala()) {
                        $arrReturn[$key] = $row->_toArray();
                        $arrReturn[$key]['id_disciplina_prr'] = '';
                        $arrReturn[$key]['st_disciplina_prr'] = '';
                        $arrReturn[$key]['st_saladeaula_prr'] = '';
                        $arrReturn[$key]['dt_abertura_prr'] = '';
                        $arrReturn[$key]['dt_encerramento_prr'] = '';
                        $arrReturn[$key]['nu_notafinal_prr'] = '';
                        $arrReturn[$key]['st_notatcc_prr'] = '';
                        $arrReturn[$key]['st_notaead_prr'] = '';
                        $arrReturn[$key]['st_notatotal_prr'] = '';

                    }
                }
                ksort($arrReturn);
                foreach ($result as $key2 => $vw2) {

                    if ($vw2->getId_categoriasala() == CategoriaSala::PRR) {

                        foreach ($arrReturn as $keyret => $ret) {
                            //varre o array procurando a disciplina igual a do PRR para adicionar os dados da PRR.
                            if ((isset($ret['id_disciplina']) && !empty($ret['id_disciplina'])) && $vw2->getId_disciplina() == $ret['id_disciplina']) {
                                $arrReturn[$keyret]['id_disciplina_prr'] = $vw2->getId_disciplina();
                                $arrReturn[$keyret]['st_disciplina_prr'] = $vw2->getSt_disciplina();
                                $arrReturn[$keyret]['st_saladeaula_prr'] = $vw2->getSt_saladeaula();
                                $arrReturn[$keyret]['dt_abertura_prr'] = self::dateRender($vw2, 'getDt_abertura');
                                $arrReturn[$keyret]['dt_encerramento_prr'] = self::dateRender($vw2, 'getDt_encerramento');
                                $arrReturn[$keyret]['st_notafinal_prr'] = $vw2->getSt_notafinal();;
                                $arrReturn[$keyret]['st_notatcc_prr'] = is_null($vw2->getSt_notatcc()) ? '' : (int)$vw2->getSt_notatcc();
                                $arrReturn[$keyret]['st_notaead_prr'] = is_null($vw2->getSt_notaead()) ? '-' : (int)$vw2->getSt_notaead();
                                $arrReturn[$keyret]['st_notatotal_prr'] = is_null($vw2->getSt_notatotal()) ? '-' : (int)$vw2->getSt_notatotal();
                            }
                        }
                    }
                }
            }

            return array_values($arrReturn);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id_matriculaorigem
     * @param $id_disciplina
     * @return array|Object
     */
    public function trataNotasParaAproveitamento($id_matriculaorigem, $id_disciplina)
    {
        try {
            $vw_avaliacaoaluno = $this->findBy('G2\Entity\VwAvaliacaoAluno',
                array('id_matricula' => $id_matriculaorigem
                , 'id_disciplina' => $id_disciplina
                , 'id_categoriasala' => \G2\Constante\CategoriaSala::NORMAL));

            $vw_avaliacaoalunoPrr = $this->findBy('G2\Entity\VwAvaliacaoAluno'
                , array('id_matricula' => $id_matriculaorigem
                , 'id_disciplina' => $id_disciplina
                , 'id_categoriasala' => \G2\Constante\CategoriaSala::PRR));


            //se o aluno tem nota de salas de PRR verifica qual é a maior para lançar o aproveitamento
            if (is_array($vw_avaliacaoalunoPrr) && !empty($vw_avaliacaoalunoPrr)) {
                foreach ($vw_avaliacaoaluno as $key => $vwSalaNormal) {
                    foreach ($vw_avaliacaoalunoPrr as $keyPrr => $vwSalaPrr) {
                        //Verifica se a avaliacao da PRR é igual a sala normal (id_avaliacao)
                        // se tem nota lançada
                        // se a nota da PRR é maior que a nota da sala normal
                        if (($vwSalaPrr->getId_avaliacao() == $vwSalaNormal->getId_avaliacao())
                            && (!is_null($vwSalaPrr->getSt_nota()))
                            && ((int)$vwSalaPrr->getSt_nota() > (int)$vwSalaNormal->getSt_nota())
                        ) {
                            //se a nota de PRR for maior que a nota da sala normal, ela será aproveitada
                            $vw_avaliacaoaluno[$key] = $vwSalaPrr;
                        }
                    }
                }
            }
            //Se o aluno nao tem nota de prr, retorna o array de avaliações de sala normal
            return $vw_avaliacaoaluno;

        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Verifica se as disciplinas selecionadas para aproveitamento tem sala disponivel para alocação.
     * @param $params
     * @return \Ead1_Mensageiro
     */
    public function verificaSalaDisponivelAlocacaoTransferencia($params)
    {
        try {
            $array_disciplinas = $params['id_disciplina'];
            $parametros['id_projetopedagogico'] = $params['id_projetopedagogico'];
            $parametros['id_turma'] = $params['id_turma'];

            $mensageiro = new \Ead1_Mensageiro();
            if (is_array($array_disciplinas) && !empty($array_disciplinas)) {
                foreach ($array_disciplinas as $id_disciplina) {
                    $parametros['id_disciplina'] = $id_disciplina;
                    $sala = $this->findOneBy('\G2\Entity\VwSalaDisciplinaTransferencia', $parametros);
                    if (!$sala instanceof \G2\Entity\VwSalaDisciplinaTransferencia) {
                        $mensageiro->setMensageiro("A disciplina " . $id_disciplina . " não possui sala de aula disponível para alocação", \Ead1_IMensageiro::ERRO);
                        continue;
                    } else if ($sala->getNu_alocados() > $sala->getNu_maxalunos()) {
                        $mensageiro->setMensageiro("Sala de aula " . $sala->getSt_saladeaula() . " atingiu limite de alunos alocados.", \Ead1_IMensageiro::AVISO);
                        continue;
                    } else {
                        $mensageiro->setMensageiro("Sala de aula encontrada para alocação.", \Ead1_IMensageiro::SUCESSO, "Sala de aula encontrada para alocação");
                    }
                }
            }
            return $mensageiro;

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e->getCode());
        }
    }

    /**
     * @param $objContratoMatricula
     * @param $matriculadestino
     * @param $matriculaorigem
     * @throws \Exception
     */
    public function salvarAditivoTransferencia($objContratoMatricula, $matriculadestino, $matriculaorigem)
    {
        try {

            $contratoTo = new \ContratoTO();
            $contratoTo->setId_contrato($objContratoMatricula->getId_contrato());
            $contratoTo->fetch(false, true, true);

            $matriculaBO = new \MatriculaBO();
            $matriculaBO->gerarAditivo($contratoTo, array('id_matricula' => $matriculadestino), \G2\Constante\TipoAditivo::TRANSFERENCIA, $matriculaorigem);

            $vendaAditivo = new \G2\Entity\VendaAditivo();
            $vendaAditivo->setId_venda($this->getReference('\G2\Entity\Venda', $contratoTo->id_venda));
            $vendaAditivo->setId_matricula($this->getReference('\G2\Entity\Matricula', $matriculadestino));
            $vendaAditivo->setId_usuario($this->getReference('\G2\Entity\Usuario', $contratoTo->id_usuario));
            $vendaAditivo->setDt_cadastro(new \DateTime());
            $result = $this->save($vendaAditivo);

            if (!($result instanceof \G2\Entity\VendaAditivo)) {
                throw new \Exception('Erro ao salvar o aditivo de venda.');
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function calcularValorNovoContrato($dados, $id_entidade)
    {
        $mensageiro = new \Ead1_Mensageiro();

        try {

            /** @var \G2\Entity\Matricula $entityMatriculaOrigem */
            $entityMatriculaOrigem = $this->find('\G2\Entity\Matricula', $dados['id_matriculaorigem']);

            if (!($entityMatriculaOrigem instanceof \G2\Entity\Matricula)) {
                throw new \Exception("Erro ao pesquisar a matricula de origem.");
            }

            $id_projetopedagogico = $entityMatriculaOrigem->getId_projetopedagogico()->getId_projetopedagogico();

            //Soma as cargas horárias das disciplinas selecionadas
            $idDiscipinasOrigem = array();
            $cargaHorariaDisciplinasSelecionadas = 0;
            if (!empty($dados['disciplinas_calcular'])) {
                $disciplinas = $dados['disciplinas_calcular'];
                foreach ($disciplinas as $k => $id_disciplinaC) {
                    $whereModuloDisciplina = array(
                        'id_disciplina' => $id_disciplinaC,
                        'id_projetopedagogico' => $id_projetopedagogico
                    );

                    $vwModuloDisciplina = $this->findOneBy('\G2\Entity\VwModuloDisciplina', $whereModuloDisciplina);

                    if (!($vwModuloDisciplina instanceof \G2\Entity\VwModuloDisciplina)) {
                        throw new \Exception("Erro ao pesquisar a(s) disciplina(s) que será(ão) aproveitada(s).");
                    }

                    $cargaHorariaDisciplinasSelecionadas += $vwModuloDisciplina->getNu_cargahoraria();
                    $idDiscipinasOrigem[] = $vwModuloDisciplina->getId_disciplina();
                }
            }

            //Pega o valor liquido do produto do contrato
            $semVenda = false;
            $valorLiquidoProdutoOrigem = 0;

            $vendaProduto = $this->findOneBy('\G2\Entity\VendaProduto',
                array('id_matricula' => $dados['id_matriculaorigem']));
            if ($vendaProduto instanceof \G2\Entity\VendaProduto) {
                $valorLiquidoProdutoOrigem = $vendaProduto->getNu_valorliquido();
                $id_venda = $vendaProduto->getId_venda()->getId_venda();
            } else {

                //se não encontrar a venda na tb_vendaproduto, pesquisa na tb_vendaaditivo
                //  (o aluno pode ter uma transferência)
                $vendaAditivo = $this->findOneBy('\G2\Entity\VendaAditivo',
                    array('id_matricula' => $dados['id_matriculaorigem']));
                if ($vendaAditivo instanceOf \G2\Entity\VendaAditivo) {
                    $id_venda = $vendaAditivo->getId_venda()->getId_venda()
                        ? $vendaAditivo->getId_venda()->getId_venda() : $vendaAditivo->getId_venda();

                    //Se ele encontrar a venda na tb_vendaaditivo, mas não encontrar os valores da negociação,
                    // pesquisa na tb_vendaproduto agora com o id_venda
                    if ($id_venda) {
                        $vendaProdutoByIdVenda = $this->findOneBy('\G2\Entity\VendaProduto',
                            array('id_venda' => $id_venda));
                        if ($vendaProdutoByIdVenda instanceof \G2\Entity\VendaProduto) {
                            $valorLiquidoProdutoOrigem = $vendaProdutoByIdVenda->getNu_valorliquido();
                        } else {
                            $semVenda = true;
                        }
                    }
                } else {

                    $id_matriculaorigem = $dados['id_matriculaorigem'];
                    do {
                        //A venda pode ter sido transferida pelo método antigo
                        //  então pesquisa na tb_matrícula a matrícula que será feita a transferência
                        $enMatricula = $this->findOneBy('\G2\Entity\Matricula',
                            array('id_matricula' => $id_matriculaorigem));
                        if (!($enMatricula instanceof \G2\Entity\Matricula)) {
                            $semVenda = true;
                            throw new \Exception('Erro ao pesquisa a matricula de da transferência.');
                        }

                        $id_matriculaorigem = $enMatricula->getId_matriculaorigem()->getId_matricula();

                        //Pesquisa na tb_vendaproduto com a matrícula de origem retornada da pesquisa acima
                        $enVendaProdutoOrigem = $this->findOneBy('\G2\Entity\VendaProduto',
                            array('id_matricula' => $id_matriculaorigem));

                    } while (!($enVendaProdutoOrigem instanceof \G2\Entity\VendaProduto));

                    $valorLiquidoProdutoOrigem = $enVendaProdutoOrigem->getNu_valorliquido();
                    $id_venda = $enVendaProdutoOrigem->getId_venda()->getId_venda();
                }
            }

            //Pega a carga horaria do projeto pedagogico
            $entityProjetoOrigem = $this->find('\G2\Entity\ProjetoPedagogico', $id_projetopedagogico);

            if (!($entityProjetoOrigem instanceof \G2\Entity\ProjetoPedagogico)) {
                throw new \Exception("Erro ao pesquisar o projeto pedagógico de origem.");
            }

            $cargaHorariaProjetoPedagogico = $entityProjetoOrigem->getNu_cargahoraria();

            //Pega a carga horaria da disciplina de TCC desse Matricula
            $vwModuloDisciplinaOrigem = $this->findBy('\G2\Entity\VwModuloDisciplina', array(
                'id_projetopedagogico' => $id_projetopedagogico
            ));

            if (empty($vwModuloDisciplinaOrigem)) {
                throw new \Exception("Erro ao pesquisar a disciplina de TCC da matrícula de origem.");
            }

            $cargaHorariaTcc = 0;

            foreach ($vwModuloDisciplinaOrigem as $disciplina) {
                if ($disciplina->getId_tipodisciplina() == \G2\Constante\TipoDisciplina::TCC) {
                    $cargaHorariaTcc = $disciplina->getNu_cargahoraria();
                }
            }

            //Adiciona os valores do aditivo no $valorLiquidoProdutoOrigem
            //Pesquisa se existe venda aditivo pelo id_venda para somar os valores de liquidos
            if (!empty($id_venda)) {
                $valorLiquidoVendaAditivo = 0;

                //verifica se existe aditivo para essa venda
                /** @var \G2\Entity\VendaAditivo[] $arrVendaAditivo */
                $arrVendaAditivo = $this->findBy('\G2\Entity\VendaAditivo', array('id_venda' => $id_venda));

                // se existir, pega todas as vendas aditivadas e soma os valores de transferência (valor líquido)
                if (!empty($arrVendaAditivo)) {
                    foreach ($arrVendaAditivo as $vendaAditivo) {
                        //se o valor proporcional da venda aditivo for null recebe 0
                        $valorProporcionalAditivo = $vendaAditivo->getNu_valorProporcional() ?: 0;

                        $valorLiquidoVendaAditivo +=
                            ($valorLiquidoProdutoOrigem - $valorProporcionalAditivo)
                            + $vendaAditivo->getNu_valortransferencia();
                    }

                    $valorLiquidoProdutoOrigem = $valorLiquidoVendaAditivo;
                }
            }

            //Cálculo para chegar no Valor Proporcional:
            // (Somatorio C.H. Disciplinas selecionadas X Valor líquido do produto de origem)
            //                      ____________________________________________
            //                      (C.H. P.P. - C.H. Disciplina de TCC do P.P.)

            //faz o calculo do valor proporcional
            if ($semVenda == false) {
                $x = $cargaHorariaProjetoPedagogico - $cargaHorariaTcc;
                $y = ($x == 0) ? 1 : $x;
                $valorProporcional = (($cargaHorariaDisciplinasSelecionadas * $valorLiquidoProdutoOrigem) / $y);
            } else {
                $valorProporcional = 0;
            }

            $ng_produto = new \G2\Negocio\Produto();

            //Pega o valor no novo produto Destino
            $dtCadastroMatricula = \G2\Utils\Helper::converterData($entityMatriculaOrigem->getDt_cadastro(), 'Y-m-d');

            // @history GII-8905
            // Buscar valor do produto de destino na DATA ATUAL
            $dadosProdutoDestino = $ng_produto->retornarValorPorData(array(
                'id_projetopedagogico' => (int)$dados['id_projetodestino'],
                'id_entidade' => $id_entidade,
                'id_situacao' => \G2\Constante\Situacao::TB_PRODUTO_ATIVO
            ), date('Y-m-d'));

            // Verificar se encontrou os dados com o valor do produto de DESTINO
            if (!($dadosProdutoDestino instanceof \G2\Entity\VwProdutoValorTransferencia)) {
                throw new \Exception("Nenhum valor encontrado no projeto de destino, baseado na data atual.");
            }

            $valorNovoCurso = $dadosProdutoDestino->getNu_valor();

            // @history GII-8905
            // Buscar valor do produto de origem na DATA DA MATRÍCULA
            $dadosProdutoOrigem = $ng_produto->retornarValorPorData(array(
                'id_projetopedagogico' => $id_projetopedagogico,
                'id_entidade' => $id_entidade
            ), $dtCadastroMatricula);

            // Verificar se encontrou os dados com o valor do produto de DESTINO
            if (!($dadosProdutoOrigem instanceof \G2\Entity\VwProdutoValorTransferencia)) {
                throw new \Exception(
                    "Nenhum valor encontrado no projeto de origem, baseado na data de cadastro da matrícula."
                );
            }

            $valorCursoOrigem = $dadosProdutoOrigem->getNu_valor();

            // Calcular o valor do desconto
            $valorDescontoPorcentagem = number_format(
                100 - ($valorLiquidoProdutoOrigem / $valorCursoOrigem * 100),
                2,
                '.',
                ''
            );
            $valorDesconto = $valorNovoCurso * $valorDescontoPorcentagem / 100;

            //Para chegar no valor a pagar:
            //Valor do novo curso - valor do desconto -(Valor líquido do produto de origem - valor calculo proporcional)
            $valorAPagar = $valorNovoCurso - $valorDesconto - ($valorLiquidoProdutoOrigem - $valorProporcional);
            if ($valorAPagar > -5 && $valorAPagar < 5) {
                $valorAPagar = 0;
            }

            //Recuperando o TID caso a transação tenha sido realizada com cartão de crédito
            /** @var \G2\Entity\TransacaoFinanceira $idTransacao */
            $idTransacao = null;
            if ($semVenda == false && $valorAPagar < 0) {
                $transacaoFinanceira = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                    array('id_venda' => $id_venda));
                if ($transacaoFinanceira instanceof \G2\Entity\VwResumoFinanceiro) {
                    if ($transacaoFinanceira->getId_meiopagamento() == \MeioPagamentoTO::RECORRENTE) {
                        $idTransacao = $transacaoFinanceira->getRecorrenteOrderid() ?
                            $transacaoFinanceira->getRecorrenteOrderid() : $id_venda . 'G2U';
                    } else {
                        $idTransacao = $transacaoFinanceira->getSt_codtransacaooperadora() ?
                            $transacaoFinanceira->getSt_codtransacaooperadora() : null;
                    }
                }
            }

            //Pega as disciplinas que serão aproveitadas
            $disciplinasAproveitar = array();
            $idDisciplinasAproveitar = array();
            if (!empty($dados['disciplinas_aproveitar'])) {
                $idDisciplinasAproveitadas = $dados['disciplinas_aproveitar'];
                foreach ($idDisciplinasAproveitadas as $k => $id_disciplinaA) {
                    $whereModuloDisciplina = array('id_disciplina' => $id_disciplinaA, 'id_projetopedagogico' => $dados['id_projetodestino']);

                    $vwModuloDisciplina = $this->findOneBy('\G2\Entity\VwModuloDisciplina', $whereModuloDisciplina);

                    if (!($vwModuloDisciplina instanceof \G2\Entity\VwModuloDisciplina)) {
                        throw new \Exception("Erro ao pesquisar a(s) disciplina(s) que será(ão) aproveitada(s).");
                    }

                    $disciplinasAproveitar[] = $vwModuloDisciplina->getSt_disciplina();
                    $idDisciplinasAproveitar[] = $vwModuloDisciplina->getId_disciplina();
                }
            }

            //Retorna um array com os valores dos cálculos anteriores
            $retorno = array(
                'id_matriculaorigem' => $dados['id_matriculaorigem'],
                'id_projetopedagogicoorigem' => $entityProjetoOrigem->getId_projetopedagogico(),
                'st_projetopedagogicoorigem' => $entityProjetoOrigem->getSt_projetopedagogico(),
                'nu_valorcursoorigem' => number_format($valorLiquidoProdutoOrigem, 2, ',', '.'),
                'dt_cadastroorigem' => date('d-m-Y', strtotime($entityMatriculaOrigem->getDt_cadastro())),
                'id_turmaorigem' => ($entityMatriculaOrigem->getId_turma() != null) ? $entityMatriculaOrigem->getId_turma()->getId_turma() : null,
                'st_projetopedagogicodestino' => $dadosProdutoDestino->getSt_projetopedagogico(),
                'st_documentacao' => ($entityMatriculaOrigem->getBl_documentacao() === null ||
                    $entityMatriculaOrigem->getBl_documentacao() === '') ? 'Não Analisado' : ($entityMatriculaOrigem->getBl_documentacao() ? 'Ok' : 'Pendente'),
                'id_venda' => $semVenda == false ? $id_venda : null,
                'id_usuario' => ($entityMatriculaOrigem->getId_usuario() != null) ? $entityMatriculaOrigem->getId_usuario()->getId_usuario() : null,
                'cargaHorariaProjetoPedagogico' => $cargaHorariaProjetoPedagogico,
                'nu_valorproporcional' => number_format($valorProporcional, 2, ',', '.'),
                'nu_valornovocurso' => number_format($valorNovoCurso, 2, ',', '.'),
                'nu_valorapagar' => number_format($valorAPagar, 2, ',', '.'),
                'nu_desconto' => number_format($valorDesconto, 2, ',', '.'),
                'st_tid' => $idTransacao,
                'idDiscipinasOrigem' => $idDiscipinasOrigem,
                'discipinasAproveitar' => $disciplinasAproveitar,
                'idsDiscipinasAproveitar' => $idDisciplinasAproveitar,
                'cargaHorariaDiscSelecionadas' => $cargaHorariaDisciplinasSelecionadas,
                'nu_valor' => '0.00',
                'id_entidade' => ($id_entidade) ? $id_entidade : $this->getId_entidade()
            );

            return $mensageiro->setMensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e->getCode());
        }

    }

    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function imprimirSimulacao($dados)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            //Pega o valor pago
            $valorPago = 0;
            $lancamentoVenda = $this->em->getRepository('\G2\Entity\LancamentoVenda')
                ->retornaLancamentosPorIdVenda(array('id_venda' => $dados['id_venda'],
                    'bl_ativo' => 1,
                    'bl_quitado' => 1));
            if ($lancamentoVenda) {
                $valorPago = round($lancamentoVenda, 2);
            }

            //Retorna os dados do usuário
            $vwPessoa = $this->findOneBy('\G2\Entity\VwPessoa',
                array('id_usuario' => $dados['id_usuario'],'id_entidade' => $dados['id_entidade']));
            if (!($vwPessoa instanceof \G2\Entity\VwPessoa)) {
                throw new \Exception('Erro ao pesquisar os dados do usuário.');
            }

            //Pega as disciplina do projeto pedagogico de destino
            $disciplinaDestino = array();
            $vwModuloDisciplinaOrigem = $this->retornarGridDestino(array('id_turma' => $dados['id_turma'], 'id_projetotransfere' => $dados['id_projetotransfere']));
            if (empty($vwModuloDisciplinaOrigem)) {
                throw new \Exception("Erro ao pesquisar as disciplinas de destino");
            }

            foreach ($vwModuloDisciplinaOrigem as $disciplina) {
                if (!in_array($disciplina['id_disciplina'], $dados['idsDiscipinasAproveitar'])) {
                    $disciplinaDestino[] = $disciplina['st_disciplina'];
                }
            }

            //passa as informações que serão salvas no tramite
            $informacoesTramite = 'Dados adicionais da Simulação de trasferência de Curso:';
            if ($dados['nu_valorapagar'] < 0) {
                if ($dados['st_tid']) {
                    $informacoesTramite .= ' TID = ' . $dados['st_tid']
                        . ', Observação=' . $dados['st_observacao'];
                } else {
                    $informacoesTramite .= ' Agência=' . $dados['nu_agencia']
                        . ', Conta=' . $dados['nu_conta']
                        . ', Operacao=' . $dados['nu_operacao']
                        . ', Observação=' . $dados['st_observacao'];
                }
            } else {
                $informacoesTramite = ' Observação=' . $dados['st_observacao'];
            }

            //Gera o Trâmite
            $ng_matricula = new \G2\Negocio\Matricula();
            $ng_matricula->gerarTramite($this->getReference('\G2\Entity\Matricula', $dados['id_matriculaorigem']),
                array(
                    'id_tipotramite' => \G2\Constante\TipoTramite::MATRICULA_TRANSFERENCIA,
                    'st_tramite' => $informacoesTramite
                )
            );

            //pagar o banco selecionado
            if (isset($dados['id_banco'])) {
                $ngBanco = new \G2\Negocio\Banco();
                $entityBanco = $ngBanco->findOneByBanco(array('id_banco' => $dados['id_banco']));
                $nu_banco = $entityBanco->getSt_banco() . " - " . $entityBanco->getSt_nomebanco();
            } else {
                $nu_banco = '';
            }

            $entityTurmaOrigem = '';
            //Retorna as turmas de origem e destino
            if ($dados['id_turmaorigem']) {
                $entityTurmaOrigem = $this->find('\G2\Entity\Turma', $dados['id_turmaorigem']);
                if (!($entityTurmaOrigem instanceof \G2\Entity\Turma)) {
                    throw new \Exception("Erro ao pesquisar a a turma de origem.");
                }
            }

            $entityTurmaDestino = '';
            if ($dados['id_turma']) {
                $entityTurmaDestino = $this->find('\G2\Entity\Turma', $dados['id_turma']);
                if (!($entityTurmaDestino instanceof \G2\Entity\Turma)) {
                    throw new \Exception("Erro ao pesquisar a a turma de destino.");
                }
            }

            $discipinasOrigem = array();
            //pegar o nome das disciplinas
            foreach($dados['idDiscipinasOrigem'] as $idDisciplinaO){

                $disciplinaEN = $this->find('\G2\Entity\Disciplina', $idDisciplinaO);
                if(!empty($disciplinaEN)){
                    $discipinasOrigem[] = $disciplinaEN->getSt_disciplina();
                }
            }

            //Retorna um array com os valores dos cálculos anteriores
            $retorno = array(
                'st_nomecompleto' => $vwPessoa->getSt_nomecompleto(),
                'st_cpf' => $this->mask($vwPessoa->getSt_cpf(), '###.###.###-##'),
                'st_email' => $vwPessoa->getSt_email(),
                'st_projetopedagogicoorigem' => $dados['st_projetopedagogicoorigem'],
                'nu_valorcursoorigem' => $dados['nu_valorcursoorigem'],
                'dt_cadastroorigem' => date('d/m/Y', strtotime($dados['dt_cadastroorigem'])),
                'st_turmaorigem' => (!empty($entityTurmaOrigem) && $entityTurmaOrigem->getSt_turma()) ?
                    $entityTurmaOrigem->getSt_turma() : 'Sem turma',
                'st_projetopedagogicodestino' => $dados['st_projetopedagogicodestino'],
                'st_turmadestino' => $entityTurmaDestino->getSt_turma(),
                'st_documentacao' => $dados['st_documentacao'],
                'id_venda' => $dados['id_venda'],
                'nu_valorproporcional' => $dados['nu_valorproporcional'],
                'nu_valornovocurso' => $dados['nu_valornovocurso'],
                'nu_valorpago' => number_format($valorPago, 2, ',', '.'),
                'nu_desconto' => $dados['nu_desconto'],
                'nu_valorapagar' => $dados['nu_valorapagar'],
                'discipinasOrigem' => $discipinasOrigem,
                'discipinasAproveitar' => $dados['discipinasAproveitar'],
                'discipinasDestino' => $disciplinaDestino,
                'nu_cargahorariaorigem' => $dados['cargaHorariaProjetoPedagogico'],
                'nu_cargahorariadiscselect' => $dados['cargaHorariaDiscSelecionadas'],
                'st_tid' => $dados['st_tid'],
                'nu_banco' => $nu_banco,
                'nu_agencia' => $dados['nu_agencia'],
                'nu_conta' => $dados['nu_conta'],
                'nu_operacao' => $dados['nu_operacao'],
                'st_observacao' => $dados['st_observacao']
            );

            return $mensageiro->setMensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e->getCode());
        }
    }

    /**
     * @param $arrLancamentos
     * @param $id_venda
     * @param $id_vendaaditivo
     * @return \Ead1_Mensageiro
     */
    public function salvarLancamentosTransferencia($arrLancamentos, $id_venda, $id_vendaaditivo, $idAcordo = null)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();

            //Salva os lancamentos
            if ($arrLancamentos) {
                $ultimoNuOrdemExistente = 0;
                $vendaTO = new \VendaTO;
                $vendaTO->setId_venda($id_venda);
                $lancamentoVendaORM = new \LancamentoVendaORM();
                $lancamentoVendaTO = new \LancamentoVendaTO();
                $lancamentoVendaTO->setId_venda($vendaTO->getId_venda());
                $lancamentosExistentes = $lancamentoVendaORM->consulta($lancamentoVendaTO);

                //pega o último número da ordem do lançamento da venda
                if ($lancamentosExistentes) {
                    foreach ($lancamentosExistentes AS $lancamExist) {
                        $ultimoNuOrdemExistente = $lancamExist->nu_ordem;
                    }
                }

                $ultimoNuOrdemExistente++;

                //reorganiza os nu_ordem dos novos lançamentos
                foreach ($arrLancamentos as $key => $lan) {
                    $arrLancamentos[$key]['nu_ordem'] = (int)$ultimoNuOrdemExistente;
                    $ultimoNuOrdemExistente++;
                }

                foreach ($arrLancamentos as $index => $lancamento) {

                    //Monta TO de lancamento
                    $lancamentoTO = new \LancamentoTO();
                    $lancamentoTO->setId_meiopagamento($lancamento['id_meiopagamento']);
                    $lancamentoTO->setId_tipolancamento($lancamento['id_tipolancamento']);
                    $lancamentoTO->setId_usuariolancamento($lancamento['id_usuariolancamento']);
                    $lancamentoTO->setNu_valor($lancamento['nu_valor']);
                    $lancamentoTO->setnu_cartao($lancamento['nu_cartao']);
                    $lancamentoTO->setBl_quitado(false);
                    $lancamentoTO->setNu_quitado((isset($lancamento['nu_quitado']) ? $lancamento['nu_quitado'] : null));
                    $lancamentoTO->setId_usuariocadastro($lancamento['id_usuariocadastro']);
                    $lancamentoTO->setId_entidade($this->sessao->id_entidade);
                    $lancamentoTO->setId_entidadelancamento((isset($lancamento['id_entidadelancamento']) ? $lancamento['id_entidadelancamento'] : $this->sessao->id_entidade));
                    $lancamentoTO->setIdSistemacobranca(\G2\Constante\Sistema::PAGARME);
                    $lancamentoTO->setNu_cobranca((!empty($lancamento['nu_cobranca']) ? $lancamento['nu_cobranca'] : null));
                    $lancamentoTO->setNu_assinatura((!empty($lancamento['nu_assinatura']) ? $lancamento['nu_assinatura'] : null));

                    if (isset($lancamento['dt_vencimento']) && !empty($lancamento['dt_vencimento'])) {
                        $lancamentoTO->setDt_vencimento($this->converterData($lancamento['dt_vencimento']));
                    } else {
                        throw new \Exception("Sem data de vencimento.");
                    }

                    if ($idAcordo) {
                        $lancamentoTO->setId_acordo($idAcordo);
                    }

                    //Instancia a VendaDao para salvar os lancamentos
                    $vendaDao = new \VendaDAO();
                    //Cadastra Lançamento
                    $id_lancamento = $vendaDao->cadastrarLancamento($lancamentoTO);

                    if (!$id_lancamento) {
                        throw new \Exception("Erro ao salvar o Lançamento.");
                    }

                    //Monta o TO  de lançamento venda
                    $lancamentoVendaTO->setId_lancamento($id_lancamento);
                    $lancamentoVendaTO->setId_venda($id_venda);
                    $lancamentoVendaTO->setNu_ordem($lancamento['nu_ordem']);
                    $lancamentoVendaTO->setBl_entrada(false);
                    $lancamentoVendaTO->setId_vendaaditivo($id_vendaaditivo);

                    //Cadastro relacionamento entre lançamentos da venda
                    if (!$id_lancamentovenda[] = $vendaDao->cadastrarLancamentoVenda($lancamentoVendaTO)) {
                        throw new \Exception("Erro ao salvar vínculo de Lançamento com Venda.");
                    }

                }

                return $mensageiro->setMensageiro('Lancamentos salvos com sucesso.', \Ead1_IMensageiro::SUCESSO);

            } else {
                throw new \Exception('Não existem lançamentos para serem adicionados.');
            }
        } catch (\Exception $e) {
            return $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO, $e->getCode());
        }
    }

    /**
     * Gerar arquivo do aditivo
     * @param $objContratoMatricula
     * @param $matriculaorigem
     * @param $matriculadestino
     * @return array
     */
    public function geraArquivoAditivo($objContratoMatricula, $matriculaorigem, $matriculadestino)
    {

        $contratoTo = new \ContratoTO();
        $contratoTo->setId_contrato($objContratoMatricula->getId_contrato());
        $contratoTo->fetch(false, true, true);

        $matriculaBO = new \MatriculaBO();
        return $matriculaBO->gerarAditivo($contratoTo, array('id_matricula' => $matriculadestino), \G2\Constante\TipoAditivo::TRANSFERENCIA, $matriculaorigem);
    }

    /**
     * @param array $data
     * @return bool
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarTransferenciaPolo($data = array())
    {
        $this->beginTransaction();

        try {
            $required = array('id_matricula', 'id_venda', 'id_vendaproduto', 'id_entidadedestino', 'id_turmadestino', 'id_contratoregradestino');

            foreach ($required as $attr) {
                if (empty($data[$attr])) {
                    throw new \Zend_Exception('É necessário informar o parâmetro ' . $attr);
                    break;
                }
            }

            // Verificar se o aluno está em processo de renovação
            $ng_gradedisciplina = new \G2\Negocio\GradeDisciplina();
            $bl_renovacao = $ng_gradedisciplina->verificaMensagemRenovacao($data['id_matricula']);

            if ($bl_renovacao) {
                throw new \Zend_Exception('O aluno não pode ser transferido porque está em processo de renovação.');
            }

            if (empty($data['lancamentos'])) {
                throw new \Zend_Exception('É necessário possuir pelo menos um lançamento.');
            }

            /** @var \G2\Entity\Matricula $matricula */
            $matricula = $this->find('\G2\Entity\Matricula', $data['id_matricula']);

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->find('\G2\Entity\Venda', $data['id_venda']);

            if (!($venda instanceof \G2\Entity\Venda)) {
                throw new \Zend_Exception('Venda não encontrada.');
            }

            /** @var \G2\Entity\VendaProduto $vendaProduto */
            $vendaProduto = $this->find('\G2\Entity\VendaProduto', $data['id_vendaproduto']);

            if (!($vendaProduto instanceof \G2\Entity\VendaProduto)) {
                throw new \Zend_Exception('VendaProduto não encontrada.');
            }

            $data['id_entidadeorigem'] = $matricula->getId_entidadematricula();

            $nu_valorbruto = $vendaProduto->getNu_desconto() ?: 0;
            $nu_valorliquido = 0;

            $lancamentos = array();

            if (!empty($data['lancamentos'])) {
                foreach ($data['lancamentos'] as $i => $lancamento) {
                    // Desativar o lançamento na venda original, para criar na nova venda
                    /** @var \G2\Entity\Lancamento $original */
                    $original = $this->find('\G2\Entity\Lancamento', $lancamento['id_lancamento']);

                    if ($original instanceof \G2\Entity\Lancamento) {
                        $original->setBl_ativo(false);
                        $this->save($original);
                    }

                    $lancamentos[$i] = array_merge($lancamento, array(
                        'id_lancamento' => null,
                        'id_venda' => null,
                        'id_entidade' => $data['id_entidadedestino'],
                        'dt_cadastro' => date('Y-m-d H:i:s'),
                        'nu_ordem' => $i + 1,
                        'nu_valorliquido' => &$nu_valorliquido
                    ));

                    $nu_valor = (float)$lancamento['nu_valor'];

                    $nu_valorbruto += $nu_valor;
                    $nu_valorliquido += $nu_valor;
                }
            }


            $arrVenda = $this->toArrayEntity($venda);

            foreach ($arrVenda as $attr => $value) {
                if (substr($attr, 0, 3) === 'id_' && is_array($value)) {
                    $arrVenda[$attr] = array_shift($value);
                }
            }

            $today = new \DateTime();

            $novaVenda = array_merge($arrVenda, array(
                'id_venda' => '',
                'id_vendaproduto' => '',
                'id_evolucao' => '',
                'id_situacao' => '',
                'id_matricula' => $data['id_matricula'],
                'id_produto' => $vendaProduto->getId_produto() ? $vendaProduto->getId_produto()->getId_produto() : null,
                'nu_valorbruto' => $nu_valorbruto,
                'nu_valorliquido' => $nu_valorliquido,
                'dt_cadastro' => $today,
                'dt_confirmacao' => $today,
                'dt_ingresso' => $vendaProduto->getDt_ingresso(),
                'bl_transferidoentidade' => true,
                'id_campanhapontualidade' => !empty($data['id_campanhapontualidadedestino'])
                    ? $data['id_campanhapontualidadedestino']
                    : null,
                'id_campanhacomercial' => !empty($data['id_campanhacomercialdestino'])
                    ? $data['id_campanhacomercialdestino']
                    : null,
                'id_tiposelecao' => $vendaProduto->getId_tiposelecao()
                    ? $vendaProduto->getId_tiposelecao()->getId_tiposelecao()
                    : null,
                'id_contratoregra' => !empty($data['id_contratoregradestino'])
                    ? $data['id_contratoregradestino']
                    : null,
                'id_turma' => $data['id_turmadestino'],
                'id_entidade' => $data['id_entidadedestino'],
                'id_usuariocadastro' => $this->sessao->id_usuario
            ));

            // Retornar pessoa da entidade de destino, se não existir, o método clona a pessoa da entidade origem
            $ng_pessoa = new \G2\Negocio\Pessoa();
            $pessoa = $ng_pessoa->retornarPessoaClone(
                $venda->getId_usuario(), $data['id_entidadedestino'], $data['id_entidadeorigem']
            );

            $arraySalvar = array(
                'pessoa' => array(
                    'id_usuario' => $pessoa->getId_usuario(),
                    'id_entidade' => $pessoa->getId_entidade() ? $pessoa->getId_entidade()->getId_entidade() : null
                ),
                'venda' => $novaVenda,
                //'disciplinas' => $disciplinas
            );

            if ($lancamentos) {
                $arraySalvar['lancamentos'] = $lancamentos;
            }

            $ng_venda = new \G2\Negocio\Venda();
            $ng_vendagraduacao = new \G2\Negocio\VendaGraduacao();
            $params = $ng_vendagraduacao->trataParametros($arraySalvar);

            $mensageiro = $ng_venda->procedimentoSalvarVenda($params,
                $venda->getId_situacao()
                    ? $venda->getId_situacao()->getId_situacao()
                    : null,
                $venda->getId_evolucao()
                    ? $venda->getId_evolucao()->getId_evolucao()
                    : null
            );

            if ($mensageiro->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                $this->rollback();
                throw new \Zend_Exception($mensageiro->getFirstMensagem());
            }

            // Refernciar a nova venda
            $en_venda = $mensageiro->getFirstMensagem();

            $ng_usperfilentidade = new \G2\Negocio\UsuarioPerfilEntidade();
            $ng_usperfilentidade->clonarAcessoAluno(
                $matricula->getId_usuario()->getId_usuario(), $data['id_entidadedestino'], $data['id_entidadeorigem']
            );

            // Salvar trâmite
            $ng_matricula = new \G2\Negocio\Matricula();

            // Salvar histórico de transferência de polo
            $en_transfpolo = $this->preencherEntity('\G2\Entity\TransferenciaPolo', 'id_transferenciapolo', array(
                'dt_cadastro' => date('Y-m-d H:i:s'),
                'bl_ativo' => 1,
                'id_usuariocadastro' => $this->sessao->id_usuario,
                'id_matricula' => $data['id_matricula'],
                'id_entidadeorigem' => $data['id_entidadeorigem'],
                'id_entidadedestino' => $data['id_entidadedestino'],
                'id_vendaorigem' => $data['id_venda'],
                'id_vendadestino' => $en_venda->getId_venda()
            ));

            /** @var \G2\Entity\TransferenciaPolo $en_transfpolo */
            $en_transfpolo = $this->save($en_transfpolo);

            // Gerar o Trâmite
            $ng_matricula->gerarTramite($matricula,
                array(
                    'id_tipotramite' => \G2\Constante\TipoTramite::MATRICULA_TRANSFERENCIA,
                    'st_tramite' => "Transferido de \"{$en_transfpolo->getId_entidadeorigem()->getSt_nomeentidade()}\" " .
                        "para \"{$en_transfpolo->getId_entidadedestino()->getSt_nomeentidade()}\""
                ));

            // Salvar nova turma e entidade na matrícula
            $matricula->setId_entidadematriz($data['id_entidadeorigem']);
            $matricula->setId_entidadematricula($data['id_entidadedestino']);
            $matricula->setId_entidadeatendimento($data['id_entidadedestino']);
            $matricula->setId_turma($this->getReference('\G2\Entity\Turma', $data['id_turmadestino']));
            $this->save($matricula);

            $this->commit();
        } catch (\Zend_Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }

        return true;
    }

}

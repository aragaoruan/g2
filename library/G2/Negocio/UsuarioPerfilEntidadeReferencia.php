<?php

namespace G2\Negocio;
use G2\Entity\Titulacao;

/**
 * Classe de negócio para UsuarioPerfilEntidadeReferencia (tb_usuarioperfilentidadereferencia)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class UsuarioPerfilEntidadeReferencia extends Negocio
{

    public $repositoryName = '\G2\Entity\UsuarioPerfilEntidadeReferencia';

    public function __construct() {
        parent::__construct();
    }

    public function findAllBy(array $where, array $order = null, $limit = null, $offset = null) {

        $dql = "SELECT tb FROM {$this->repositoryName} tb JOIN \\G2\\Entity\\Perfil AS p WITH p.id_perfil = tb.id_perfil AND p.id_perfilpedagogico = 9 WHERE 1 = 1";
        if (is_array($where) && $where) {
            foreach ($where as $field => $value) {
                $dql .= " AND tb.{$field} = '{$value}'";
            }
        }

        $mensageiro = $this->em->createQuery($dql)->getResult();
        //$mensageiro = parent::findBy($this->repositoryName, $where, $order, $limit, $offset);

        $results = array();
        foreach ($mensageiro as $entity) {
            $model = array(
                'id_perfilreferencia' => $entity->getId_perfilreferencia(),
                'bl_ativo' => $entity->getBl_ativo(),
                'id_usuario' => array(
                    'id_usuario' => $entity->getId_usuario() ? $entity->getId_usuario()->getId_usuario() : 0,
                    'st_nomecompleto' => $entity->getId_usuario() ? $entity->getId_usuario()->getSt_nomecompleto() : '',
                ),
                'id_areaconhecimento' => array(
                    'id_areaconhecimento' => $entity->getId_areaconhecimento() ? $entity->getId_areaconhecimento()->getId_areaconhecimento() : 0,
                    'st_areaconhecimento' => $entity->getId_areaconhecimento() ? $entity->getId_areaconhecimento()->getSt_areaconhecimento() : ''
                ),
                'id_projetopedagogico' => array(
                    'id_projetopedagogico' => $entity->getId_projetopedagogico() ? $entity->getId_projetopedagogico()->getId_projetopedagogico() : 0,
                    'st_projetopedagogico' => $entity->getId_projetopedagogico() ? $entity->getId_projetopedagogico()->getSt_projetopedagogico() : ''
                ),
                'id_disciplina' => array(
                    'id_disciplina' => $entity->getId_disciplina() ? $entity->getId_disciplina()->getId_disciplina() : 0,
                    'st_disciplina' => $entity->getId_disciplina() ? $entity->getId_disciplina()->getSt_disciplina() : ''
                ),
                'dt_inicio' => $entity->getDt_inicio() ? implode('/', array_reverse(explode('-', substr($entity->getDt_inicio(), 0, 10)))) : '',
                'dt_fim' => $entity->getDt_fim() ? implode('/', array_reverse(explode('-', substr($entity->getDt_fim(), 0, 10)))) : '',
                'id_titulacao' => array (
                    'id_titulacao' => (($entity->getId_titulacao() instanceof Titulacao) ) ? $entity->getId_titulacao()->getId_titulacao() : '',
                    'st_titulacao' => (($entity->getId_titulacao() instanceof Titulacao) ) ? $entity->getId_titulacao()->getSt_titulacao() : '',
                )
            );
            $results[] = $model;
        }

        return $results;
    }

    public function findByDisciplinaAreaProjetoPeriodoEntidade($params) {
        $dql  = "SELECT tb FROM G2\Entity\UsuarioPerfilEntidadeReferencia tb JOIN \\G2\\Entity\\Perfil AS p WITH p.id_perfil = tb.id_perfil AND p.id_perfilpedagogico = 9 WHERE 1 = 1 ";

        if (!isset($params['id_entidade']))
            $params['id_entidade'] = $this->sessao->id_entidade;

        $dataIni = implode('-', array_reverse(explode('/', substr($params['dt_inicio'], 0, 10))));
        $dataFim = implode('-', array_reverse(explode('/', substr($params['dt_fim'], 0, 10))));

        // Verifica se possui um registro com os memos dados, no mesmo periodo de data
        // Simular BETWEEN, porque nao funciona no createQuery
        $dql .= "AND (
            ('{$dataIni}' >= tb.dt_inicio AND ('{$dataIni}' <= tb.dt_fim OR tb.dt_fim IS NULL))
            OR
            ('{$dataFim}' >= tb.dt_inicio AND ('{$dataFim}' <= tb.dt_fim OR tb.dt_fim IS NULL))
            )";

        if (isset($params['id_perfilreferencia']) && $params['id_perfilreferencia'])
            $dql .= "AND tb.id_perfilreferencia != {$params['id_perfilreferencia']} ";

        unset($params['dt_inicio'], $params['dt_fim'], $params['id_perfilreferencia']);
        foreach ($params as $attribute => $value) {
            $dql .= "AND tb.{$attribute} = '{$value}' ";
        }

        $query = $this->em->createQuery($dql);
        return $query->getResult();
    }

    public function save($data) {
        $idAttribute = 'id_perfilreferencia';

        if (isset($data[$idAttribute]) && !empty($data[$idAttribute])) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$idAttribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }

        if (isset($data['id_entidade']) && !empty($data['id_entidade'])) {
            $entity->setId_entidade($this->find('\G2\Entity\Entidade', $data['id_entidade']));
        }else{
            $entity->setId_entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));
        }

        if (isset($data['id_disciplina']) && !empty($data['id_disciplina']))
            $entity->setId_disciplina($this->find('\G2\Entity\Disciplina', $data['id_disciplina']));

        if (isset($data['id_usuario']) && !empty($data['id_usuario']))
            $entity->setId_usuario($this->find('\G2\Entity\Usuario', $data['id_usuario']));

        if (isset($data['id_perfil']) && !empty($data['id_perfil']))
            $entity->setId_perfil($this->find('\G2\Entity\Perfil', $data['id_perfil']));

        if (isset($data['id_areaconhecimento']) && !empty($data['id_areaconhecimento']))
            $entity->setId_areaconhecimento($this->find('\G2\Entity\AreaConhecimento', $data['id_areaconhecimento']));

        if (isset($data['id_projetopedagogico']) && !empty($data['id_projetopedagogico']))
            $entity->setId_projetopedagogico($this->find('\G2\Entity\ProjetoPedagogico', $data['id_projetopedagogico']));

        if (isset($data['bl_titular']) && !empty($data['bl_titular']))
            $entity->setBl_titular($data['bl_titular']);
        else
            $entity->setBl_titular(1);

        if (array_key_exists('bl_ativo', $data))
            $entity->setBl_ativo($data['bl_ativo']);
        else
            $entity->setBl_ativo(1);

        if (isset($data['dt_inicio']) && !empty($data['dt_inicio']))
            $entity->setDt_inicio((new \Zend_Date($data['dt_inicio'], 'pt_BR'))->toString('yyyy-MM-dd'));
        else
            $entity->setDt_inicio(date('Y-m-d'));

        if (isset($data['dt_fim']) && !empty($data['dt_fim']))
            $entity->setDt_fim((new \Zend_Date($data['dt_fim'], 'pt_BR'))->toString('yyyy-MM-dd'));
        else
            $entity->setDt_fim(null);


       if (isset($data['id_titulacao']) && !empty($data['id_titulacao']))
            $entity->setId_titulacao($this->find('\G2\Entity\Titulacao', $data['id_titulacao']));

        $getMethod = 'get' . ucfirst($idAttribute);

        if ($entity->$getMethod()) {
            $entity = $this->merge($entity);
        } else {
            $entity = $this->persist($entity);
        }

        $mensageiro = new \Ead1_Mensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $entity->$getMethod());
        
        return $mensageiro;
    }

}
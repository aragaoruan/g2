<?php
namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Entity\PessoaEndereco;


/**
 * Classe Negocio para Endereco
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-18
 */
class Endereco extends Negocio
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Salva endereco Pessoa
     * @param array $data
     * @param $returnMensageiro - Retorna o mensageiro completo
     * @return mixed
     * @return \Ead1_Mensageiro
     * @update Denise Xavier - 12/01/2017
     */
    public function salvarEnderecoPessoa(array $data, $returnMensageiro = false)
    {
        try {
            //atribuiu id_usuario a variavel
            $id_usuario = $data['id_usuario'];
            unset($data['id_usuario']); //remove do array o id_usuario

            //instacia as TO
            $uTO = new \UsuarioTO();
            $uTO->setId_usuario($id_usuario);

            $eTO = new \EnderecoTO($data);

            $peTO = new \PessoaEnderecoTO();
            $peTO->setId_usuario($id_usuario);
            $peTO->setBl_padrao(true);

            $bo = new \PessoaBO();
            $result = $bo->cadastrarEndereco($uTO, $eTO, $peTO);
            if ($result->getType() == 'success') {
                return !$returnMensageiro ? $result->getMensagem() : $result;
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }
    /**
     * Retorna Dados do UF
     * @param array $params
     * @return \G2\Negocio\Zend_Exception
     */
    public function retornaDadosUf(array $params)
    {
        try {
            $return = $this->findBy('G2\Entity\Uf', $params);
            if ($return) {
                return $return;
            }
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }
    public function retornaEndereco($id_entidade)
    {
    }
    public function retornaEnderecos($id_entidade, $unico = false)
    {
        try {
            $return = $this->findBy('G2\Entity\EntidadedeEndereco', array('id_entidade' => $id_entidade));
            $arResult = array();
            foreach ($return as $r) {
                $arResult[] = array(
                    'id_entidadeendereco' => $r->getId_entidadeendereco(),
                    'id_entidade'         => $r->getId_entidade(),
                    'id_endereco'         => $r->getId_endereco()->getId_endereco(),
                    'id_tipoendereco'     => $r->getId_endereco()->getId_tipoendereco()->getId_tipoendereco(),
                    'id_municipio'        => $r->getId_endereco()->getId_municipio()->getId_municipio(),
                    'st_nomemunicipio'    => $r->getId_endereco()->getId_municipio()->getSt_nomemunicipio(),
                    'st_tipoendereco'     => $r->getId_endereco()->getId_tipoendereco()->getSt_tipoendereco(),
                    'st_endereco'         => $r->getId_endereco()->getSt_endereco(),
                    'st_cep'              => $r->getId_endereco()->getSt_cep(),
                    'st_bairro'           => $r->getId_endereco()->getSt_bairro(),
                    'st_complemento'      => $r->getId_endereco()->getSt_complemento(),
                    'nu_numero'           => $r->getId_endereco()->getNu_numero(),
                    'st_cidade'           => $r->getId_endereco()->getSt_cidade(),
                    'sg_uf'               => $r->getId_endereco()->getSg_uf()
                );
            }
            if ($arResult)
                return $arResult[0];
            else
                return $arResult;
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }


    public function salvaEnderecoLoja($params)
    {
        try {
            $this->beginTransaction();
            $objPessoaEndereco = $this->findOneBy('\G2\Entity\PessoaEndereco', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));
            if ($objPessoaEndereco) {
                $objEndereco = $this->findOneBy('\G2\Entity\Endereco', array('id_endereco' => $objPessoaEndereco->getId_endereco()->getId_endereco()));
            } else {
                $objEndereco = new \G2\Entity\Endereco();
                $objEndereco->setId_pais($this->getReference('\G2\Entity\Pais', $params['id_pais']));
                $objEndereco->setSg_uf($params['sg_uf_correspondencia']);
                $objEndereco->setId_municipio($this->getReference('\G2\Entity\Municipio', $params['id_municipio_correspondencia']));
                $objEndereco->setId_tipoendereco($this->getReference('\G2\Entity\TipoEndereco', 1));
                $objEndereco->setNu_numero($params['nu_numero']);
                $objEndereco->setBl_ativo(true);
            }
            $objEndereco->setSt_cep($params['st_cep']);
            $objEndereco->setSt_endereco($params['st_endereco']);
            $objEndereco->setSt_bairro($params['st_bairro']);
            $objEndereco->setSt_complemento($params['st_complemento']);
            $objEndereco->setNu_numero($params['nu_numero']);
            $objEndereco->setId_municipio($this->getReference('\G2\Entity\Municipio', $params['id_municipio']));
            $objEndereco->setSt_cidade($this->getReference('\G2\Entity\Municipio', $params['id_municipio'])->getSt_nomemunicipio());
            $enderecoResidencial = $this->save($objEndereco);
            if (!$objPessoaEndereco) {
                $objR = new \G2\Entity\PessoaEndereco();
                $objR->setBl_padrao(true);
                $objR->setId_endereco($enderecoResidencial);
                $objR->setId_entidade($this->getReference('\G2\Entity\Entidade', $params['id_entidade']));
                $objR->setId_usuario($params['id_usuario']);
                $this->save($objR);
            }
            if ($params['correspondencia']) {
                $objEnderecoCorresp = new \G2\Entity\Endereco();
                $objEnderecoCorresp->setId_pais($this->getReference('\G2\Entity\Pais', $params['id_pais_correspondencia']));
                $objEnderecoCorresp->setSg_uf($params['sg_uf_correspondencia']);
                $objEnderecoCorresp->setId_municipio($this->getReference('\G2\Entity\Municipio', $params['id_municipio_correspondencia']));
                $objEnderecoCorresp->setId_tipoendereco($this->getReference('\G2\Entity\TipoEndereco', 5));
                $objEnderecoCorresp->setSt_cep($params['st_cep_correspondencia']);
                $objEnderecoCorresp->setSt_endereco($params['st_endereco_correspondencia']);
                $objEnderecoCorresp->setSt_bairro($params['st_bairro_correspondencia']);
                $objEnderecoCorresp->setSt_complemento($params['st_complemento_correspondencia']);
                $objEnderecoCorresp->setNu_numero($params['nu_numero_correspondencia']);
                $objEnderecoCorresp->setBl_ativo(true);
                $objEnderecoCorresp->setSt_cidade($this->getReference('\G2\Entity\Municipio', $params['id_municipio_correspondencia'])->getSt_nomemunicipio());
                $endereco = $this->save($objEnderecoCorresp);
                if ($endereco) {
                    $obj = new \G2\Entity\PessoaEndereco();
                    $obj->setBl_padrao(true);
                    $obj->setId_endereco($endereco);
                    $obj->setId_entidade($this->getReference('\G2\Entity\Entidade', $params['id_entidade']));
                    $obj->setId_usuario($params['id_usuario']);
                    $this->save($obj);
                }
            }
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception('Erro ao salvar endereço na loja. ' . $e->getMessage());
        }
    }


    /**
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function editarEnderecoPessoa(array $data)
    {
        try {
            if(array_key_exists('telefone', $data) && !empty($data['telefone'])){
                $this->editarTelefone($data);
            }
            $endereco = $this->preencherEntity('\G2\Entity\Endereco', 'id_endereco', $data, array('bl_ativo' => true));
            $pessoaEndereco = $this->preencherEntity('\G2\Entity\PessoaEndereco', null, $data);
            return $this->editarEndereco($endereco, $pessoaEndereco);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Valida endereço pessoa para salvar/ editar
     * @param \G2\Entity\Endereco $endereco
     * @param \G2\Entity\PessoaEndereco $pessoaEndereco
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Validate_Exception
     */
    public function editarEndereco($endereco, $pessoaEndereco)
    {
        $bo = new \PessoaBO();
        $mensageiro = new  \Ead1_Mensageiro();
        try {
            if ($endereco->getNu_numero()) {
                $numeroLimpo = $bo->somenteNumero($endereco->getNu_numero());
                if (empty($numeroLimpo)) {
                    throw new \Zend_Validate_Exception('É necessário haver ao menos 1 caracter numérico no número!');
                }
            }

            if (!$pessoaEndereco->getId_entidade()->getId_entidade()) {
                throw new \Exception('É necessário o parâmetro da entidade para continuar');
            }
            if ($pessoaEndereco->getBl_padrao() === true) {
                $this->editarEnderecoPadrao($endereco, $pessoaEndereco);
            }
            $mensageiro = $this->salvarEndereco($endereco, $pessoaEndereco);
        } catch (\Zend_Validate_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
            THROW $e;
        } catch (\Exception $e) {
            $mensageiro->setMensageiro('Erro ao Editar Endereço!', \Ead1_IMensageiro::ERRO, $e->getMessage());
            THROW $e;
        }
        return $mensageiro;
    }


    /**
     * Se o endereço a ser cadastrado ou editado for padrão, marca o anterior com bl_padrao = 0
     * @param \G2\Entity\Endereco $enderecoEntity
     * @param \G2\Entity\PessoaEndereco $pessoaEnderecoEntity
     * @return bool
     */
    private function editarEnderecoPadrao($enderecoEntity, $pessoaEnderecoEntity)
    {
        try {
            $enderecos = $this->findBy('\G2\Entity\VwPessoaEndereco', array(
                'id_entidade'     => $pessoaEnderecoEntity->getId_entidade()->getId_entidade(),
                'id_usuario'      => $pessoaEnderecoEntity->getId_usuario(),
                'id_tipoendereco' => $enderecoEntity->getId_tipoendereco()->getId_tipoendereco()
            ));

            //Busca os endereços do usuario, do mesmo tipo para saber se existe algum padrão. Edita o mesmo caso exista
            if (is_array($enderecos)) {
                foreach ($enderecos as $endereco) {
                    if ($endereco instanceof \G2\Entity\VwPessoaEndereco && $endereco->getId_endereco()) {
                        $entityUpdate = $this->findOneBy('\G2\Entity\PessoaEndereco', array('id_endereco' => $endereco->getId_endereco()));
                        if ($entityUpdate instanceof \G2\Entity\PessoaEndereco) {
                            $entityUpdate->setBl_padrao(false);
                            $this->save($entityUpdate);
                        }
                    }
                }
            }
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Salva /  Edita endereço
     * @param $endereco
     * @param $pessoaEndereco
     * @return \Ead1_Mensageiro
     */
    private function salvarEndereco($endereco, $pessoaEndereco)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            $this->beginTransaction();
            $endereco = $this->save($endereco);
            if ($endereco instanceof \G2\Entity\Endereco && $endereco->getId_endereco()) {
                $entityAlter = $this->findOneBy('\G2\Entity\PessoaEndereco', array('id_endereco' => $endereco->getId_endereco()));
                if (!$entityAlter instanceof \G2\Entity\PessoaEndereco) {
                    $entityAlter = new \G2\Entity\PessoaEndereco();
                    $entityAlter->setId_entidade($pessoaEndereco->getId_entidade());
                    $entityAlter->setId_usuario($pessoaEndereco->getId_usuario());
                    $entityAlter->setId_endereco($endereco);
                }
                $entityAlter->setBl_padrao($pessoaEndereco->getBl_padrao());
                if (!$this->save($entityAlter)) {
                    throw new \Exception('Erro ao salvar');
                }
            }
            $this->commit();
            $mensageiro->setMensageiro($endereco, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Endereço salvo com sucesso');
        } catch (\Exception $e) {
            $this->rollback();
            $mensageiro->setMensageiro('Erro ao salvar endereço: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * Edita/ Salva um Telefone
     * @param array $dados
     * @return \Ead1_Mensageiro|\Exception
     */
    public function editarTelefone(array  $dados) {
        $negocio = new \G2\Negocio\Pessoa();
        try{
            $ctTO = $this->preencherEntity('\G2\Entity\ContatosTelefone', 'id_telefone', $dados);
            $ctpTO = $this->findOneBy('\G2\Entity\ContatosTelefonePessoa', array(
                                    'id_telefone' => $dados['id_telefone'],
                                    'id_usuario' => $dados['id_usuario'],
                                    'id_entidade' => $dados['id_entidade']
                                ));


            if (!$ctpTO) {
                $ctpTO = new \G2\Entity\ContatosTelefonePessoa();
            }

            $ctpTO->setId_entidade($dados['id_entidade'])
                ->setId_usuario($dados['id_usuario']);

            //set bl_padrao
            if (isset($data['bl_padrao'])) {
                $ctpTO->setBl_padrao($data['bl_padrao']);
            }

            if ($ctTO instanceof \G2\Entity\ContatosTelefone ) {
              $ctTO->setNu_ddd(substr($dados['telefone'], 0, 2));
              $ctTO->setNu_telefone(trim(substr($dados['telefone'], 2)));
            }

            return $negocio->salvaContatoTelefonePessoa($ctTO, $ctpTO);

        }catch (\Exception $e){
           return new \Exception($e->getMessage());
        }
    }
}
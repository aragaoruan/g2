<?php

namespace G2\Negocio;

/**
 * Classe de negócio para ProjetoPedagogico
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 * @author Kayo Silva <kayo.silva@unyleya.com> 2014-29-01
 */
class ProjetoPedagogico extends Negocio
{

    private $repositoryProjetoPedagogico = 'G2\Entity\ProjetoPedagogico';
    /**
     * Atributo privado para armazenar a soma da % Curso
     * @var float
     */
    private $somaPorcentagemCurso = 0;

    /**
     * Retorna todas os ProjetoPedagogicos da Entidade
     * @return \Ead1_Mensageiro
     */
    public function findAllProjetoPedagogico()
    {

        try {

            $result = $this->em->getRepository($this->repositoryProjetoPedagogico)
                ->findBy(array('id_entidadecadastro' => $this->sessao->id_entidade), array('st_projetopedagogico' => 'ASC'));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Find Projeto Pedagogico
     * @param array $params
     * @return \G2\Entity\ProjetoPedagogico
     * @throws \Ead1_Mensageiro
     */
    public function findOneProjetoPedagogico(array $params)
    {
        try {
            if (array_key_exists('id_projetopedagogico', $params) && isset($params['id_projetopedagogico'])) {
                return $this->find($this->repositoryProjetoPedagogico, $params['id_projetopedagogico']);
            }
            $param = array();
            foreach ($params as $key => $value) {
                if ($value) {
                    if (substr($key, 0, 3) == 'dt_') {
                        $date = $this->converteDataBanco($value);
                        $param[$key] = $date->format('Y-m-d H:i:s');
                    } else {
                        $param[$key] = $value;
                    }
                }
            }

            return $this->em->getRepository($this->repositoryProjetoPedagogico)->findOneBy($param);
        } catch (\Exception $exc) {
            throw new \Exception($exc->getMessage());
        }
    }

    /**
     * Retorna o ProjetoPedagogico pelo ID
     * @param type $id_projetopedagogico
     * @return \Ead1_Mensageiro
     */
    public function findProjetoPedagogicoById($id_projetopedagogico)
    {

        try {

            $result = $this->em->getRepository($this->repositoryProjetoPedagogico)->findBy(array('id_projetopedagogico' => $id_projetopedagogico));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Find Projeto Entidade
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findProjetoEntidade(array $params = array())
    {
        try {
            $params['id_entidade'] = (isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade);
            $result = $this->findBy('G2\Entity\ProjetoEntidade', $params);
            if ($result) {
                $mensageiro = new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro("Sem Resultado", \Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Find VwProjetoPedagogico
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findVwProjetoEntidade(array $params = array())
    {
        try {
            $params['id_entidadecadastro'] = (isset($params['id_entidadecadastro']) ? $params['id_entidadecadastro'] : $this->sessao->id_entidade);
            $resultFind = $this->findBy('G2\Entity\VwProjetoEntidade', $params, array('st_projetopedagogico' => 'ASC'));

            $result = array();
            foreach ($resultFind as $key => $value) {
                $result[$key]['id_projetopedagogico'] = $value->getId_projetopedagogico();
                $result[$key]['st_projetopedagogico'] = $value->getSt_projetopedagogico();
            }
            return $result;
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Salva modulo disciplina para cadastro completo
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaModuloParaCadastroCompleto(array $arrProjeto, array $arrayDisciplina)
    {
        try {
            $arrRetorno = array();
            $disciplinas = array();
            $ro = new \ProjetoPedagogicoRO();
            $mTO = new \ModuloTO();
            $mTO->setSt_descricao($arrProjeto['projetopedagogico']['st_descricao']);
            $mTO->setSt_modulo($arrProjeto['projetopedagogico']['st_projetopedagogico']);
            $mTO->setSt_tituloexibicao($arrProjeto['projetopedagogico']['st_projetopedagogico']);
            $mTO->setBl_ativo(true);
            $mTO->setId_projetopedagogico($arrProjeto['projetopedagogico']['id_projetopedagogico']);

//percorre o array de disciplina
            if ($arrayDisciplina) {
                foreach ($arrayDisciplina as $rowDisciplina) {
//Seta os ids das disciplinas em um novo array
                    $disciplinas[] = $rowDisciplina['id_disciplina'];
                    $mTO->setArrDisciplinas($rowDisciplina['id_disciplina']);
                }
            }
//Verifica modulo existe
            $findModulo = $this->findOneByModulo($mTO->toArray());
            if ($findModulo->getType() == 'success') {
                foreach ($findModulo->getMensagem() as $row) {
                    $mTO->setId_modulo($row->getId_modulo());
                }
            }
//Salva Módulo
            $resultModulo = $ro->salvarModulo($mTO);

            if ($resultModulo->getType() == 'success') {
                $resFindModulo = $ro->retornarModulo($mTO); //Procura modulo
                if ($resFindModulo->getType() == 'success') {
                    foreach ($resFindModulo->getMensagem() as $modulo) {
                        $arrRetorno['modulo'] = $modulo->toArray();
                    }
                }
            }
//Salva Modulo Disciplina
            if (isset($arrRetorno['modulo'])) {
                if ($arrayDisciplina) {
                    $resultModDisc = $this->salvarModuloDisciplinaParaCadastroCompleto($arrRetorno['modulo'], $arrayDisciplina);
                    if ($resultModDisc->getType() == 'success') {
                        $arrRetorno['modulodisciplina'] = $resultModDisc->getMensagem();
                    }
                }
            }
            return new \Ead1_Mensageiro($arrRetorno, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao cadastrar Modulo To Cadastro Completo. Entre em contato com a EAD1.' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Find One Modulo
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findOneByModulo(array $params)
    {
        try {
            $param = array();
            if ($params) {
                foreach ($params as $key => $value) {
                    if ($value)
                        $param[$key] = $value;
                }
            }
            unset($param['arrDisciplinas']);
            $result = $this->em->getRepository('\G2\Entity\Modulo')->findOneBy($param);
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva Modulos Disciplina
     * @param array $modulo array com modulo a ser vinculado
     * @param array $arrDisciplina array de disciplinas a serem vinculadas
     * @return \Ead1_Mensageiro
     */
    public function salvarModuloDisciplinaParaCadastroCompleto(array $modulo, array $arrDisciplina)
    {
        try {

            $disciplinaNegocio = new Disciplina();
            $arrReturn = array();
            if ($arrDisciplina) {
//percorre o array de disciplina
                foreach ($arrDisciplina as $disciplina) {
//Procura pela disciplina no banco
                    $resultDisciplina = $disciplinaNegocio->getReference($disciplina['id_disciplina']);
//se achou a disciplina
                    if ($resultDisciplina) {
//Pesquisa se o modulo disciplina já existe
                        $modulosDisciplina = $this->em->getRepository('\G2\Entity\ModuloDisciplina')->findOneBy(array(
                            'id_modulo' => $modulo['id_modulo'],
                            'id_disciplina' => $resultDisciplina->getId_disciplina(),
                        ));

                        if (!$modulosDisciplina) {
                            $disciplinaSerieNivelEnsino = $disciplinaNegocio->findDisciplinaSerieNivel(array(
                                'id_serie' => 11,
                                'id_disciplina' => $resultDisciplina->getId_disciplina(),
                                'id_nivelensino' => 7
                            ));

                            if ($disciplinaSerieNivelEnsino->getType() != 'success') {
                                $disciplinaNegocio->salvaDisciplinaSerieNivelEnsino(array(
                                    'id_serie' => array(11),
                                    'id_disciplina' => $resultDisciplina->getId_disciplina(),
                                    'id_nivelensino' => 7
                                ));
                            }

                            $entity = new \G2\Entity\ModuloDisciplina();
                            $moduloObj = $this->getReference('\G2\Entity\Modulo', $modulo['id_modulo']);
                            $serie = $this->getReference('\G2\Entity\Serie', 11);
                            $nivelEnsino = $this->getReference('\G2\Entity\NivelEnsino', 7);


                            $entity->setId_modulo($moduloObj)
                                ->setId_disciplina($resultDisciplina)
                                ->setId_serie($serie)
                                ->setId_nivelensino($nivelEnsino)
                                ->setBl_ativo(true)
                                ->setBl_obrigatoria(false);

                            $resultModulodisciplina = $this->save($entity);
                        }
                    }
                }
            }

            $resultModulodisciplina = $this->findBy('\G2\Entity\ModuloDisciplina', array('id_modulo' => $modulo['id_modulo']));
            if ($resultModulodisciplina) {
                foreach ($resultModulodisciplina as $row) {
                    $arrReturn[] = array(
                        "id_modulodisciplina" => $row->getId_modulodisciplina(),
                        "nu_ordem" => $row->getNu_ordem(),
                        "nu_importancia" => $row->getNu_importancia(),
                        "bl_ativo" => $row->getBl_ativo(),
                        "bl_obrigatoria" => $row->getBl_obrigatoria(),
                        "nu_ponderacaocalculada" => $row->getNu_ponderacaocalculada(),
                        "nu_ponderacaoaplicada" => $row->getNu_ponderacaoaplicada(),
                        "dt_cadastroponderacao" => $row->getDt_cadastroponderacao(),
                        "id_modulo" => $row->getId_modulo() ? $row->getId_modulo()->getId_modulo() : null,
                        "id_disciplina" => $row->getId_disciplina() ? $row->getId_disciplina()->getId_disciplina() : null,
                        "id_serie" => $row->getId_serie() ? $row->getId_serie()->getId_serie() : null,
                        "id_nivelensino" => $row->getId_nivelensino() ? $row->getId_nivelensino()->getId_nivelensino() : null,
                        "id_areaconhecimento" => $row->getId_areaconhecimento() ? $row->getId_areaconhecimento()->getId_areaconhecimento() : null,
                        "id_usuarioponderacao" => $row->getId_usuarioponderacao() ? $row->getId_usuarioponderacao()->getId_usuarioponderacao() : null,
                    );
                }
            }
//retorna o mensageiro
            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao cadastrar Modulo Disciplina. Entre em contato com a EAD1.' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna Parametros de TCC do projeto pedagogico
     * @return \Ead1_Mensageiro
     */
    public function findParametrosTccProjetoPedagogico(array $find = array())
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ParametroTcc')
                ->findBy($find);
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna Modulo disciplina
     * @return \Ead1_Mensageiro
     */
    public function findVwModuloDisciplina(array $find = array())
    {

        try {
            $modulosDisciplinas = $this->findBy('\G2\Entity\VwModuloDisciplina', $find);

            $arrReturn = array();
            foreach ($modulosDisciplinas as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Atualiza a coluna de ponderação calculada para os registros de um projeto pedagogico
     * @param integer $idProjeto
     * @return \Ead1_Mensageiro
     */
    public function atualizaPonderacaoCalculada($idProjeto)
    {
        try {
            $arrReturn = array(); //cria uma variavel com array vazio para retorno
            //Busca os Modulos Disciplina
            $modulosDisciplinas = $this->findBy('\G2\Entity\VwModuloDisciplina', array('id_projetopedagogico' => $idProjeto));
            if ($modulosDisciplinas) {
                $totalCh = $this->recuperaSomatorioPonderacao($idProjeto, 'nu_cargahoraria'); //recupera o somatorio de carga horaria
                $totalImportancia = $this->recuperaSomatorioPonderacao($idProjeto, 'nu_importancia'); //recupera o somatorio de importancia
                $totalRepeticao = $this->recuperaSomatorioPonderacao($idProjeto, 'nu_repeticao'); //somatorio de repetição

                //percorre os modulos disciplinas para calcular a porcentagem de cada curso e calcular o somatorio da porcentagem do curso
                foreach ($modulosDisciplinas as $modulo) {
                    //atribui a porcentagem do curso atual em uma posição do array
                    $arrPercCurso[] = $this->calculaPorcentagemCurso($modulo->getNu_cargahoraria(), $modulo->getNu_importancia(), $modulo->getNu_repeticao(), $totalCh, $totalImportancia, $totalRepeticao);
                }
                //percorre o array do valor do curso e calcula a porcentagem da disciplina
                foreach ($arrPercCurso as $percCurso) {
                    //atribui cada porcentagem da disciplina numa posição do array
                    $percDisc[] = $this->calculaPorcetagemDisciplina($percCurso);
                }

                $this->beginTransaction(); //inicia a transação
                //percorre novamente os modulos disciplinas
                foreach ($modulosDisciplinas as $key => $modulo) {
                    //faz um find na entity de ModuloDisciplina
                    $moduloDisciplina = $this->find('\G2\Entity\ModuloDisciplina', $modulo->getId_modulodisciplina());
                    //Seta o valor da ponderação calculada
                    $moduloDisciplina->setNu_ponderacaocalculada(($percDisc[$key] * 100)); //multiplica por 100 para ficar na escala de 0 a 100%
                    if (!$this->save($moduloDisciplina)) {
                        throw new \Exception("Erro ao tentar salvar a ponderação calculada.");
                    } //salva

                    $arrReturn[] = $moduloDisciplina; //atribui o retorno no array
                }

                $return = $this->findBy('\G2\Entity\VwModuloDisciplina', array('id_projetopedagogico' => $idProjeto), array('st_disciplina' => 'ASC'));

                $this->commit(); //commit
                return $return; //retorna
            }

        } catch (\Exception $ex) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao tentar atualizar Ponderação Calculada. Entre em contato com a EAD1.' . $ex->getMessage());
        }
    }

    /**
     * Recupera o somatorio do modulo disciplina de um determinado projeto pedagogico
     * @param integer $idProjeto
     * @param string $stColuna
     * @return mixed
     * @throws \Zend_Exception
     */
    public function recuperaSomatorioPonderacao($idProjeto, $stColuna)
    {
        try {
            $result = $this->em->getRepository('\G2\Entity\VwModuloDisciplina')
                ->recuperaSomatorioPonderacaoByProjeto($idProjeto, $stColuna);
            if (isset($result[$stColuna])) {
                return (int)$result[$stColuna];
            }
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro não esperado. Entre em contato com a EAD1.' . $ex->getMessage());
        }
    }

    /**
     * Efetua o calculo da porcentagem do curso baseado no calculo disponibilizado em documento COM-252
     * @param integer $idProjeto
     * @param float $cargaHoraria
     * @param float $importancia
     * @param float $repeticao
     * @return float
     * @throws \Zend_Exception
     */
    private function calculaPorcentagemCurso($cargaHoraria, $importancia, $repeticao, $totalCh, $totalImportancia, $totalRepeticao)
    {
        try {
            $total = 0; //assume o valor de zero
            if (!$repeticao) $repeticao = 1;
            if (!$cargaHoraria) $cargaHoraria = 1;
            if (!$importancia) $importancia = 1;
            if (!$totalCh) $totalCh = 1;
            if (!$totalImportancia) $totalImportancia = 1;
            if (!$totalRepeticao) $totalRepeticao = 1;

            if ($totalCh && $totalImportancia && $totalRepeticao) {//verifica se os somatorios retornaram valor para evitar erro em divisão por zero
                $total = ((($cargaHoraria * $importancia) / $repeticao) / $totalCh) * 100; //efetua o calculo
            }
            $this->somaPorcentagemCurso += $total;
            return $total;
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao tentar calcular % de Curso' . $ex->getMessage());
        }
    }

    /**
     * Calcula a porcentagem da Disciplina baseado no calculo disponibilizado em documento COM-252
     * @param float $porcetagemCurso
     * @return float
     * @throws \Zend_Exception
     */
    private function calculaPorcetagemDisciplina($porcetagemCurso)
    {
        try {
            $total = 0; // inicia a variavel zerada
            if (!$porcetagemCurso) {
                $porcetagemCurso = 1;
            }
            if ($this->somaPorcentagemCurso) { // verifica se a soma das porcentagens do curso não é zero
                $total = $porcetagemCurso / $this->somaPorcentagemCurso;
            }
            return $total;
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro ao tentar calcular % de Curso' . $ex->getMessage());
        }
    }

    public function retornarProjetosRegraPagamento($id_area, $id_entidade = null)
    {
        if (!$id_entidade) {
            $id_entidade = $this->sessao->id_entidade;
        }
        try {
            $dql = "SELECT pj 
                    FROM G2\Entity\Entidade AS e
                    JOIN G2\Entity\ProjetoEntidade AS pe WITH pe.id_entidade = ?1 AND pe.id_entidade = e.id_entidade
                    JOIN G2\Entity\ProjetoPedagogico AS pj WITH pj.id_projetopedagogico = pe.id_projetopedagogico
                    JOIN G2\Entity\AreaProjetoPedagogico AS tap WITH tap.id_projetopedagogico = pj.id_projetopedagogico
                    JOIN G2\Entity\AreaConhecimento AS ac WITH ac.id_areaconhecimento = tap.id_areaconhecimento AND tap.id_areaconhecimento = ?2
                    ORDER BY ac.st_areaconhecimento, pj.st_projetopedagogico";

            $query = $this->em->createQuery($dql);
            $query->setParameter(1, $id_entidade);
            $query->setParameter(2, $id_area);

            return $query->getArrayResult();
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro não esperado. Entre em contato com a EAD1.' . $ex->getMessage());
        }
    }

    public function verificaNomeProjetoPedagogico(array $array)
    {

        if (count($array) > 0) {

            $st_projetopedagogico = $array['st_projetopedagogico'];
            $id_projetopedagogico = $array['id_projetopedagogico'];

            if ($id_projetopedagogico != null) {

                $query = "SELECT pp.st_projetopedagogico
                      FROM G2\Entity\ProjetoPedagogico AS pp
                      WHERE pp.st_projetopedagogico = ?1
                      AND pp.id_projetopedagogico != ?2";

                $query = $this->em->createQuery($query);
                $query->setParameter(1, $st_projetopedagogico);
                $query->setParameter(2, $id_projetopedagogico);

            } else {

                $query = "SELECT pp.st_projetopedagogico
                      FROM G2\Entity\ProjetoPedagogico AS pp
                      WHERE pp.st_projetopedagogico = ?1";

                $query = $this->em->createQuery($query);
                $query->setParameter(1, $st_projetopedagogico);
            }

            $result = $query->getArrayResult();


            if (count($result) > 0) {
                return true;
            } else {
                return false;
            }

        } else {

            return new \Zend_Exception('O parâmetro passado não está configurado.');
        }
    }


    public function findByAreaConhecimentoEntidade($idAreaconhecimento)
    {
        if ($idAreaconhecimento) {
            $dql = "SELECT pp ";
            $dql .= "FROM G2\Entity\VwProjetoEntidade pp ";
            $dql .= "JOIN G2\Entity\VwAreaProjetoPedagogico AS ap WITH pp.id_projetopedagogico = ap.id_projetopedagogico ";
            $dql .= "WHERE ap.id_areaconhecimento = $idAreaconhecimento AND pp.id_entidade = {$this->sessao->id_entidade} ";
            $dql .= "ORDER BY pp.st_projetopedagogico ASC ";

            $query = $this->em->createQuery($dql);
            return $query->getResult();
        }

        return array();

    }

    /**
     * Retorna os projetos pedagogicos de acordodo com a entidade recursiva
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornaProjetoPedagogicoEntidadeRecursiva()
    {
        try {
            //Instancias
            $mensageiro = new \Ead1_Mensageiro();
            $entidadeNegocio = new Entidade();
            $id_entidade = $this->sessao->id_entidade;//seta o id_entidade pegando da sessão

            //busca as entidades e suas filhas
            $resultEntidadeRecursiva = $entidadeNegocio->retornarVwEntidadeRecursaId($id_entidade);
            //verifica se tem resultado
            if ($resultEntidadeRecursiva) {

                //array de entidades
                $arrIdEntidades = array();
                foreach ($resultEntidadeRecursiva as $entidade) {
                    $arrIdEntidades[] = $entidade['id_entidade'];
                }

                //verifica se o array de entidades esta preenchido
                if ($arrIdEntidades) {

                    //pega o repositorio da vw_turmaprojetopedagogico e chama a função
                    $result = $this->em->getRepository('\G2\Entity\VwTurmaProjetoPedagogico')
                        ->retornaTurmaProjetoPedagogicoEntidadeRecursiva($arrIdEntidades);

                    //verifica se tem resultado
                    if ($result) {
                        $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
                    } else {
                        $mensageiro->setMensageiro('Nenhuma projeto encontrado.', \Ead1_IMensageiro::AVISO);
                    }
                }

            } else {
                $mensageiro->setMensageiro('Nenhuma entidade encontrada.', \Ead1_IMensageiro::AVISO);
            }

            //retorna os dados
            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados do Projeto. " . $e->getMessage());
        }
    }

    /**
     * Retorna entidades que contenham turma.
     * @param integer $idProjeto
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornaEntidadeComTurmaByProjeto($idProjeto)
    {
        try {
            //Instancias
            $mensageiro = new \Ead1_Mensageiro();
            $entidadeNegocio = new Entidade();
            $id_entidade = $this->sessao->id_entidade;//seta o id_entidade pegando da sessão

            //busca as entidades e suas filhas
            $resultEntidadeRecursiva = $entidadeNegocio->retornarVwEntidadeRecursaId($id_entidade);
            //verifica se tem resultado
            if ($resultEntidadeRecursiva) {

                //array de entidades
                $arrIdEntidades = array();
                foreach ($resultEntidadeRecursiva as $entidade) {
                    $arrIdEntidades[] = $entidade['id_entidade'];
                }

                //verifica se o array de entidades esta preenchido
                if ($arrIdEntidades) {
                    //pega o repositorio da VwProjetoEntidade e chama a função
                    $result = $this->em->getRepository('\G2\Entity\VwProjetoEntidade')
                        ->retornarEntidadeComTurmaByProjeto($arrIdEntidades, $idProjeto);

                    //verifica se tem resultado
                    if ($result) {
                        $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
                    } else {
                        $mensageiro->setMensageiro('Nenhuma Unidade com Turma vinculada encontrado.', \Ead1_IMensageiro::AVISO);
                    }
                }
            } else {
                $mensageiro->setMensageiro('Nenhuma entidade encontrada.', \Ead1_IMensageiro::AVISO);
            }
            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar entidade com turma. " . $e->getMessage());
        }
    }

    /**
     * Retorna turmas de acordo com a uniddade e projeto pedagogico
     * @param integer $idProjeto ID do projeto pedagogico
     * @param integer $idUnidade ID da entidade
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornarTurmaByProjetoUnidade($idProjeto, $idUnidade, $idTurno)
    {
        try {

            //Instancias
            $mensageiro = new \Ead1_Mensageiro();

            //pega o repositorio da VwProjetoEntidade e chama a função
            $result = $this->em->getRepository('\G2\Entity\VwProjetoEntidade')
                ->retornarTurmaByProjetoUnidade($idProjeto, $idUnidade, $idTurno);

            //verifica se tem resultado
            if ($result) {
                $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro->setMensageiro('Nenhuma Turma vinculada ao Projeto e Unidade encontrado.', \Ead1_IMensageiro::AVISO);
            }

            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar turmas vinculadas ao projeto e unidade. " . $e->getMessage());
        }
    }

    /**
     * Retorna disciplinas que tenham encontros a realizar de acordo com o projeto e unidade
     * @param integer $idProjeto
     * @param integer $idUnidade
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornarDisciplinasProjetoUnidade($idProjeto, $idUnidade, $idTurma)
    {
        try {

            //Instancias
            $mensageiro = new \Ead1_Mensageiro();

            //pega o repositorio da VwProjetoEntidade e chama a função
            $result = $this->em->getRepository('\G2\Entity\VwEntidadeProjetoDisciplina')
                ->retornarDisciplinasProjetoUnidade($idProjeto, $idUnidade, $idTurma);

            //verifica se tem resultado
            if ($result) {
                $arrReturn = array();
                foreach ($result as $key => $row) {
                    //verififca se o numero de encontros é menor que o numero permitido
                    if (($row['nu_encontros'] < $row['nu_totalencontros']) || $row['nu_cargahoraria'] == 0) {
                        $arrReturn[] = $row;
                    }
                }
                unset($result);
                $mensageiro->setMensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro->setMensageiro('Nenhuma Disciplina vinculada ao Projeto e Unidade encontrado.', \Ead1_IMensageiro::AVISO);
            }

            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar Disciplina vinculadas ao projeto e unidade. " . $e->getMessage());
        }
    }

    /**
     * Retorna turnos de um projeto pedagogico com turma
     * @param integer $idProjeto
     * @return \Ead1_Mensageiro
     */
    public function retornarTurnoByProjeto($idProjeto)
    {
        //Instancias
        $mensageiro = new \Ead1_Mensageiro();
        $result = $this->em->getRepository('\G2\Entity\TurmaTurno')->retornarTurnoByProjeto($idProjeto);
        if ($result) {
            $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            $mensageiro->setMensageiro("Nenhum resultado", \Ead1_IMensageiro::AVISO);
        }
        return $mensageiro;
    }


    /**
     * Cordenadores para ter o papel retirado do MOODLE.
     * Usado no ROBO - removerPapelCoordenadorMoodle
     * @param array $params
     * @return array
     */
    public function retornaVwDesativarCoordenadoresMoodle(array $params = [])
    {
        return $this->findBy('\G2\Entity\VwDesativarCoordenadoresMoodle', $params, null, 100);
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function retornaProjetosEmComumEntreSalas(array $params = array())
    {

        $id_saladeaula1 = $params['ids_saladeaula'][0];
        $id_saladeaula2 = $params['ids_saladeaula'][1];

        return $this->getRepository('\G2\Entity\ProjetoPedagogico')
            ->retornaProjetosEmComumEntreSalas($params, $id_saladeaula1, $id_saladeaula2);
    }


    /**
     * @param $id_projetopedagogico
     * @param $arquivo
     * @return \Ead1_Mensageiro
     */
    public function salvarAnexoEmenta($id_projetopedagogico, $arquivo)
    {
        $negocio = new \G2\Negocio\Upload();
        $mensageiro = $negocio->upload($arquivo);

        // Se fazer o upload, alterar no projeto
        if ($mensageiro->type == 'success') {
            $pp = $this->find($this->repositoryProjetoPedagogico, $id_projetopedagogico);
            $pp->setId_anexoementa($this->find('\G2\Entity\Upload', $mensageiro->codigo));
            $this->save($pp);
        }

        return $mensageiro;
    }


    /**
     * @param int $id
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object
     * @throws \Zend_Exception
     */
    public function removeModuloProjeto($id)
    {
        try {
            $this->beginTransaction();

            $modulo = $this->find('\G2\Entity\Modulo', $id);
            $modulosDisciplina = $this->findBy('G2\Entity\ModuloDisciplina', array('id_modulo' => $modulo->getId_modulo()));

            if (is_array($modulosDisciplina)) {
                foreach ($modulosDisciplina as $md) {
                    $md->setBl_ativo(false);
                    $this->save($md);
                }
            }

            $modulo->setBl_ativo(false);
            $modulo->setId_situacao($this->find('\G2\Entity\Situacao', \G2\Constante\Modulo::SITUACAO_INATIVA));
            $this->save($modulo);

            $this->commit();

            return $modulo;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return null|object
     * @throws \Exception
     */
    public function findOneByVwProjetoArea(array $params = array())
    {
        try {
            $params['id_entidade'] = (isset($params['id_entidadecadastro']) ? $params['id_entidadecadastro'] : $this->sessao->id_entidade);
            $result = $this->findOneBy('G2\Entity\VwProjetoArea', $params);

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $arrEntidade
     * @param $pp
     * @return \Ead1_Mensageiro
     */
    public function vincularProjetoEntidade(array $arrEntidade, $pp)
    {
        try {
            if ($pp) {
                $objDelete = $this->findBy('\G2\Entity\ProjetoEntidade', array('id_projetopedagogico' => $pp));
                if ($objDelete) {
                    foreach ($objDelete as $value) {
                        $this->delete($value);
                    }
                }
            }

            foreach ($arrEntidade as $entidade) {

                $objProjEntidade = new \G2\Entity\ProjetoEntidade();
                $objProjEntidade->setDt_inicio(new \DateTime());
                $objProjEntidade->setDt_cadastro(new \DateTime());
                $objProjEntidade->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_PROJETOENTIDADE_ATIVO));
                $objProjEntidade->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_entidade));
                $objProjEntidade->setBl_ativo(true);
                $objProjEntidade->setId_entidade($this->getReference('\G2\Entity\Entidade', $entidade));
                $objProjEntidade->setId_projetopedagogico($this->getReference('\G2\Entity\ProjetoPedagogico', $pp));
                $result = $this->save($objProjEntidade);

                if (!$result) {
                    throw new Zend_Exception('Erro ao cadastrar Projeto na Instituição');
                }
            }
            return new \Ead1_Mensageiro('Projeto cadastrado com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (Zend_Exception $e) {
            return new \Ead1_Mensageiro('Erro ao Cadastrar Relação de entidade!', \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

    /**
     * Faz upload da imagem do projeto pedagogico
     *
     * @param $imagem
     * @param $id_projetopedagogico
     * @return array
     * @throws \Zend_Exception
     */
    public function uploadImagemProjeto($imagem, $id_projetopedagogico)
    {
        try {
            if (is_array($imagem)) {
                $path = 'upload/projetopedagogico/';
                $type = explode('/', $imagem['type']);
                $hash = uniqid($id_projetopedagogico); // cria um id diferente para o nome da imagem
                $fileName = 'projeto_pedagogico_' . $hash;
                $fileNameType = 'projeto_pedagogico_' . $hash . '.' . $type['1'];
                //renomeando o nome do arquivo para que a extensão fique realmente a extensão
                //do arquivo
                $imagem['name'] = $fileName;

                //verifica o tamanho da imagem
                list($width, $height) = getimagesize($imagem['tmp_name']);

                if ($width != '1024' || $height != '1024') {
                    $arrReturn = array(
                        "type" => 'error',
                        'title' => 'Atenção',
                        'text' => "A imagem não está no tamanho solicitado. Tamanho da imagem deve ser igual a 1024x1024."
                    );
                    return $arrReturn;
                }

                if (!preg_match("/^image\/(jpg|jpeg)$/", $imagem["type"])) {
                    $arrReturn = array(
                        "type" => 'warning',
                        'title' => 'Arquivo inválido!',
                        'text' => "O formato da imagem não é válido. O formato aceito é JPG."
                    );
                    return $arrReturn;
                }

                // pesquisa pelo arquivo previamente cadastrado e o exclui
                $caminhoImagem = $this->findOneBy('G2\Entity\ProjetoPedagogico', array('id_projetopedagogico' => $id_projetopedagogico));
                if (!empty($caminhoImagem->getSt_imagem()) && file_exists($caminhoImagem->getSt_imagem())) {
                    unlink($caminhoImagem->getSt_imagem());
                }

                $bo = new \Ead1_BO();
                $arquivoTO = new \ArquivoTO();
                $arquivoTO->setSt_caminhodiretorio('projetopedagogico');
                $arquivoTO->setAr_arquivo($imagem);
                $arquivoTO->setSt_nomearquivo($fileName);
                $arquivoTO->setSt_extensaoarquivo($type[1]);
                $img = $bo->uploadBasico($arquivoTO);

                //monta o array de retorno com os dados da imagem
                $arrReturn = array(
                    "type" => $img->getType(),
                    'title' => $img->getTitle(),
                    'text' => $img->getText(),
                    'file' => 'upload/projetopedagogico/' . $fileNameType
                );
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception('ERRO: ' . $e->getMessage());
        }
    }

    /**
     * Calcula e seta o nu_percentualsemestre da tb_modulodisciplina
     * @param $id_projetopedagogico
     * @param $nu_obrigatorioalocacao
     * @return decimal/null
     */
    public function setNuPercentualSemestre($id_projetopedagogico, $nu_obrigatorioalocacao)
    {
        try {
            $pp = $this->find('\G2\Entity\ProjetoPedagogico', (int)$id_projetopedagogico);
            if ($pp instanceof \G2\Entity\ProjetoPedagogico && $pp->getId_projetopedagogico()) {
                if (is_null($pp->getNu_semestre()) || !$pp->getNu_semestre())
                    $pp->setNu_semestre(1);

                $nu_percentualsemestre = $nu_obrigatorioalocacao / $pp->getNu_semestre();
            }

            return str_replace(",", ".", $nu_percentualsemestre);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * Retorna os projetos pedagógicos do coordenador em uma determinada entidade
     * @param $id_projetopedagogico
     * @param $nu_entidade
     * @return Object
     */
    public function retornarProjetoPedagogicoCoordenador($id_coordenador = null, $id_entidade = null){
        if(empty($id_coordenador)){
            throw new \Exception('Informe o id do coordenador.');
        }

        if(empty($id_entidade)){
            throw new \Exception('Informe o id da entidade.');
        }

        try {
            $sql = "SELECT  distinct pe.id_projetopedagogico, pe.st_projetopedagogico, pe.id_entidade FROM vw_usuarioperfilentidadereferencia  uper
                  	  INNER JOIN vw_projetoentidade pe on uper.id_projetopedagogico = pe.id_projetopedagogico
                      WHERE id_usuario = $id_coordenador and 
                            uper.bl_ativo = 1 and 
                            bl_titular = 1  and 
                            id_perfilpedagogico = 2 and 
                            (pe.id_entidade in (select id_entidadepai from tb_entidaderelacao where id_entidade = $id_entidade) AND 
                            pe.id_entidade = $id_entidade)";
            $stmt = $this->getem()->getConnection()->prepare($sql);
            $stmt->execute();
            $resultado = $stmt->fetchAll();
            return $resultado;
        } catch (\Exception $e){
            return 'Houve um erro ao recuperar os dados do projeto pedagógico para o coordenador';
        }

    }

    // A flag recursiva permite buscar os coordenadores de projeto pedagógico nas entidades superiores (pais)
    public function retornaCoordenadoresDeProjetoPedagogico($id_entidade, $recursivo = null){
        if(empty($id_entidade)){
            throw new \Exception('Informe o id da entidade para buscar os coordenadores');
        }

        try {
//            $coordenadores = $this->findBy('G2\Entity\VwUsuarioPerfilEntidadeReferencia', array(
//                'id_entidade' => $this->sessao->id_entidade,
//                'id_perfilpedagogico' => 2,
//                'bl_ativo' => 1,
//                'bl_titular' => 1));
//            return $this->toArrayEntity($coordenadores);

            $sql = "select distinct id_entidade, id_usuario, st_nomecompleto from vw_usuarioperfilentidadereferencia as vw
	                where
            vw.id_perfilpedagogico = 2 and
            vw.bl_ativo = 1 and
            vw.bl_titular = 1 and
			vw.id_entidade  in (select id_entidadepai from tb_entidaderelacao where id_entidade = $id_entidade)
			order by st_nomecompleto asc";
            $stmt = $this->getem()->getConnection()->prepare($sql);
            $stmt->execute();
            $resultado = $stmt->fetchAll();
        } catch (\Exception $e){
            throw new \Exception('Não foi possível buscar os coordenadores');
        }

        return $resultado;
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para TipoDocumento (tb_tipodocumento)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class TipoDocumento extends Negocio
{

    private $repositoryName = 'G2\Entity\TipoDocumento';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_tipodocumento' => $row->getId_tipodocumento(),
                    'st_tipodocumento' => $row->getSt_tipodocumento(),
                    'bl_ativo' => $row->getBl_ativo()
                );
            }

            return $arrReturn;
        }
    }

}
<?php

namespace G2\Negocio;

use Doctrine\Common\Collections\Criteria;
use G2\Constante\TipodeConta;
use G2\Entity\ContaPessoa;
use G2\Entity\VwPesquisarPessoa;

/**
 * Classe de Negocio para Pessoa
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-10-17
 */
class Pessoa extends Negocio
{

    /**
     * @var BO PessoaBO
     */
    public $_pessoaBo;

    /**
     * @var TO PessoaTO
     */
    public $_pessoaTo;

    /**
     * @var String Repository Name
     */
    public $repositoryName = 'G2\Entity\Pessoa';

    public function __construct()
    {
        parent::__construct();
        $this->_pessoaBo = new \PessoaBO();
    }

    /**
     * Retorna VwPesquisarPessoa de acordo com os parametros passados
     * @param array $params Parametros da busca
     * @return mixed array
     */
    public function retornaPesquisarPessoa(array $params, $orderBy = null, $limit = 0, $offset = 0)
    {
        /** @var $repo \G2\Repository\VwPesquisarPessoa */
        $repo = $this->em->getRepository('G2\Entity\VwPesquisarPessoa');
        $to = new \Ead1_TO_Pesquisar();
        //Instancia a PerfilBo
        $perfilBo = new \PerfilBO();
        $arrColumns = array();
        if (isset($params['grid'])) {
            //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
            $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "vw");
        }

        if (isset($params['entidadesMesmaHolding'])) {
            $holdingNegocio = new \G2\Negocio\Holding();

            /** @var $holdings \G2\Entity\VwEntidadesMesmaHolding */
            $holdings = $holdingNegocio->retornarEntidadesHolding();

            $entidadesArr = [];

            foreach ($holdings as $holding) {
                $id_entidade = $holding->getId_entidadeparceira();
                if (!in_array($id_entidade, $entidadesArr)) {
                    $entidadesArr[] = $id_entidade;
                }
            }

            if (empty($entidadesArr)) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            } else {
                $params['id_entidade'] = $entidadesArr;
            }

        } else if (!isset($params['allEntidades']) || !$params['allEntidades']) {

            //Verifica se não veio nos parametros o id da entidade, e seta o id vindo da sessão
            if (!isset($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }
            //Busca as entidades filhas
            $retornoEntidades = $perfilBo->retornaEntidadeRecursivaPerfil($params);
            //Trata o retorno
            if ($retornoEntidades) {
                $retornoEntidades = $retornoEntidades->toArray(); //transforma em um array
                //Percorre o array e busca somente o index passado via parametro
                $entidadesFilhas = ", " . $to->montaDadosIN($retornoEntidades['mensagem'], 'id_entidade');
            } else {
                $entidadesFilhas = NULL;
            }
            //concatena as entidades
            $params['id_entidade'] = $params['id_entidade'] . $entidadesFilhas;
            //Comunica com o repository e executa o querybuilder

        }

        $result = $repo->pesquisaPessoa($params, $arrColumns, $orderBy, $limit, $offset);
        //Verifica o resultado
        if ($result) {
            //Retorna o mensageiro e os dados Serializado
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
     * @param string $prefix
     * @param json string $grid
     * @return array
     */
    private function getColumnsFromPesquisaDinamica($grid, $prefix = '')
    {
        $grids = json_decode($grid);
        $arrGrid = array();
        $prefix = (!empty($prefix) ? $prefix . "." : null);
        if (is_object($grids)) {
            foreach ($grids->grid as $key => $grid) {
                array_push($arrGrid, $prefix . $grid->name);
            }
        }
        return $arrGrid;
    }

    /**
     * Busca usuário por parametros
     * @param array $params
     * @return \G2\Negocio\Zend_Exception
     */
    public function verificaUsuario(array $params = array())
    {
        try {
            $entidadeNegocio = new Entidade();
            $mensageiro = new \Ead1_Mensageiro();
            $uTO = new \UsuarioTO();
            $uTO->montaToDinamico($params);

            if ($uTO->getSt_cpf()) {
                $uTO->setSt_cpf(str_replace('.', null, str_replace('-', null, $uTO->getSt_cpf())));

                if (!$this->_pessoaBo->validaCPF($uTO->getSt_cpf())) {
                    return $mensageiro->setMensageiro('CPF inválido! Digite o CPF corretamente.', \Ead1_IMensageiro::AVISO);
                }
            }
            //Busca os dados da pessoa
            $return = $this->_pessoaBo->verificaUsuario($uTO);
            //verifica se os dados de pessoa foi encontrado
            if ($return->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $retorno = $return->getFirstMensagem(); //pega a primeira mensagem do retorno
                if ($retorno->getId_entidade()) {
                    $entidade = $entidadeNegocio->findEntidade($retorno->getId_entidade());
                } else {
                    $entidade = $entidadeNegocio->retornaEntidadePai($this->sessao->id_entidade);
                }

                $pessoa = $this->findOneBy('\G2\Entity\Pessoa', array(
                    'id_usuario' => $retorno->getId_usuario(),
                    'id_entidade' => $entidade->getId_entidade()
                )); //recupera dados de pessoa
                //var_dump($retorno->getId_usuario());die;
                $usuario = $this->retornaUsuarioById($retorno->getId_usuario()); //recupera os dados de usuario
                //seta os dados no array para retorno
                if ($usuario && $pessoa) {
                    $responsavelLegal = $this->findOneBy('\G2\Entity\ResponsavelLegal', array('id_usuario' => $retorno->getId_usuario(), 'bl_ativo' => true));
                    $arrResult = array(
                        'id' => $retorno->getId_usuario(),
                        'id_usuario' => $retorno->getId_usuario(),
                        'st_login' => $usuario->getSt_login(),
                        'st_senha' => $usuario->getSt_senha(),
                        'st_nomecompleto' => $usuario->getSt_nomecompleto(),
                        'id_registropessoa' => $usuario->getId_registropessoa() ? $usuario->getId_registropessoa()->getId_registropessoa() : NULL,
                        'st_cpf' => $usuario->getSt_cpf(),
                        'bl_ativo' => (boolean)$pessoa->getBl_ativo(),
                        'bl_redefinicaosenha' => (boolean)$usuario->getBl_redefinicaosenha(),
                        'id_usuariopai' => $usuario->getId_usuariopai() ? $usuario->getId_usuariopai()->getId_usuario() : NULL,
                        'dt_nascimento' => $pessoa->getDt_nascimento(),
                        'id_entidade' => $entidade->getId_entidade(),
                        'st_nomeentidade' => $entidade->getSt_nomeentidade(),
                        'st_nomeResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getSt_nomecompleto() : null,
                        'nu_dddResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getNu_ddd() : null,
                        'nu_telefoneResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getNu_telefone() : null,
                        'st_cpfResponsavelLegal' => $responsavelLegal ? $responsavelLegal->getSt_cpf() : null,

                    );
                    return $mensageiro->setMensageiro(array($arrResult), \Ead1_IMensageiro::SUCESSO); //retorna os dados

                } else {
                    if ($usuario) {
                        $arrResult = array(
                            'id' => $usuario->getId_usuario(),
                            'id_usuario' => $usuario->getId_usuario(),
                            'st_login' => $usuario->getSt_login(),
                            'st_senha' => $usuario->getSt_senha(),
                            'st_nomecompleto' => $usuario->getSt_nomecompleto(),
                            'id_registropessoa' => $usuario->getId_registropessoa() ? $usuario->getId_registropessoa()->getId_registropessoa() : NULL,
                            'st_cpf' => $usuario->getSt_cpf(),
                            'bl_ativo' => (boolean)$usuario->getBl_ativo(),
                            'bl_redefinicaosenha' => (boolean)$usuario->getBl_redefinicaosenha(),
                            'st_nomeentidade' => null,
                            'id_entidade' => null,
                            'id_usuariopai' => $usuario->getId_usuariopai() ? $usuario->getId_usuariopai()->getId_usuario() : NULL);

                        return $mensageiro->setMensageiro(array($arrResult), \Ead1_IMensageiro::SUCESSO); //retorna os dados

                    };

                };
            }
            //retorna o mensageiro caos não tenha entrado no if acima
            return $return;
        } catch (\Exception $e) {
            return $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna Dados de Usuario pelo ID
     * @param integer $idUsuario
     * @return \G2\Entity\Usuario
     */
    public function retornaUsuarioById($idUsuario)
    {
        return $this->find('\G2\Entity\Usuario', $idUsuario);
    }

    /**
     * Get Enderecço pessoa
     * @param array $params
     * @return array PessoaBo\retornaEndereco
     * @return \G2\Negocio\Zend_Exception
     */
    public function getEnderecoPessoa(array $params)
    {
        try {
            $uTo = new \UsuarioTO();
            $uTo->setId_usuario($params['id_usuario']);
            $result = $this->_pessoaBo->retornaEnderecos($uTo, true, $params['id_entidade']);
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    if ($row->getBl_padrao()) {

                        if ($row->getId_tipoendereco() == 5)
                            $sufix = "_corresp";
                        else
                            $sufix = "";

                        $arrReturn = array_merge($arrReturn, array(
                            "id_usuario{$sufix}" => $row->getId_usuario(),
                            "id_entidade{$sufix}" => $row->getId_entidade(),
                            "bl_padrao{$sufix}" => $row->getBl_padrao(),
                            "st_categoriaendereco{$sufix}" => $row->getSt_categoriaendereco(),
                            "id_tipoendereco{$sufix}" => $row->getId_tipoendereco(),
                            "st_tipoendereco{$sufix}" => $row->getSt_tipoendereco(),
                            "id_endereco{$sufix}" => $row->getId_endereco(),
                            "st_endereco{$sufix}" => $row->getSt_endereco(),
                            "st_complemento{$sufix}" => $row->getSt_complemento(),
                            "nu_numero{$sufix}" => $row->getNu_numero(),
                            "st_bairro{$sufix}" => $row->getSt_bairro(),
                            "st_cidade{$sufix}" => $row->getSt_cidade(),
                            "st_estadoprovincia{$sufix}" => $row->getSt_estadoprovincia(),
                            "st_cep{$sufix}" => $row->getSt_cep(),
                            "sg_uf{$sufix}" => $row->getSg_uf(),
                            "id_categoriaendereco{$sufix}" => $row->getId_categoriaendereco(),
                            "id_municipio{$sufix}" => $row->getId_municipio(),
                            "st_nomemunicipio{$sufix}" => $row->getSt_nomemunicipio(),
                            "st_uf{$sufix}" => $row->getSt_uf(),
                            "id_pais{$sufix}" => $row->getId_pais(),
                            "st_nomepais{$sufix}" => $row->getSt_nomepais(),
                            "bl_ativo{$sufix}" => $row->getBl_ativo()
                        ));
                    }
                }
                return $arrReturn;
            }
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    /**
     * Get Endereço By CEP
     * @param string $st_cep Cep string
     * @return EnderecoTO
     */

    public function getEnderecoBYCep($st_cep, $primeiro = false)
    {
        $eTo = new \EnderecoTO();
        $eTo->setSt_cep($st_cep);
        $result = $this->_pessoaBo->cepService($eTo);

        if ($result->getType() == 'success') {
            if ($primeiro) {
                $resultado = $result->getFirstMensagem();
                $munic = $this->findOneBy('G2\Entity\Municipio', array('st_nomemunicipio' => $resultado->getSt_cidade()));
                if ($munic) {
                    $arrayMuni = $this->toArrayEntity($munic);
                    if (array_key_exists('id_municipio', $arrayMuni)) {
                        $resultado->setId_municipio($arrayMuni['id_municipio']);
                        $resultado->setSt_nomemunicipio($arrayMuni['st_nomemunicipio']);
                    }
                }
                return $resultado;
            } else {
                $resultado = $result->getMensagem();
                foreach ($resultado as &$endereco) {
                    $munic = $this->findOneBy('G2\Entity\Municipio', array('st_nomemunicipio' => $endereco->getSt_cidade()));
                    if (isset($munic->id_municipio)) {
                        $endereco->setId_municipio($munic->getId_municipio());
                        $endereco->setSt_nomemunicipio($munic->getSt_nomemunicipio());
                    }
                }
                return $resultado;
            }
        } else if ($result->getTipo() == \Ead1_IMensageiro::AVISO) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * get conta pessoa
     *
     * @param $id_usuario
     * @param bool $returnArray
     * @return array|null|object
     * @throws \Zend_Exception
     */
    public function getContaPessoa($id_usuario, $returnArray = true)
    {
        $result = $this->findOneBy('G2\Entity\ContaPessoa', array('id_usuario' => $id_usuario, 'id_entidade' => $this->sessao->id_entidade));
        if (isset($result) && !empty($result)) {
            if ($returnArray) {
                $result = $this->toArrayEntity($result);
            }
            return $result;
        }
    }

    /**
     * Get Municipios by UF
     * @param string $sg_uf
     * @return mixed/array Municipios Array
     */
    public function getMunicipioByUf($sg_uf)
    {
        $result = $this->findBy('G2\Entity\Municipio', array('sg_uf' => $sg_uf), array('st_nomemunicipio' => 'ASC'));
        $arrReturn = array();
        if ($result) {
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_municipio' => $row->getId_municipio(),
                    'st_nomemunicipio' => $row->getSt_nomemunicipio(),
                    'nu_codigoibge' => $row->getNu_codigoibge(),
                    'sg_uf' => $row->getSg_uf()->getSg_uf()
                );
            }
            return $arrReturn;
        }
    }

    /**
     * Salva Dados Usuário
     * @param array $data
     * @return \UsuarioTO
     */
    public function salvarUsuario(array $data)
    {
        $dataTo = array();
        if ($data) {
            foreach ($data as $key => $value) {
                if (!empty($value)) {
                    $dataTo[$key] = $value;
//limpa a string de cpf
                    if ($key == 'st_cpf') {
                        $dataTo[$key] = str_replace('.', null, str_replace('-', null, $value));
                    }
                }
            }
        }
        $uTo = new \UsuarioTO($dataTo);
        $result = $this->_pessoaBo->cadastrarUsuario($uTo, $this->sessao->id_entidade);
        if ($result->getType() == 'success') {
            return $result->getMensagem();
        }
    }

    /**
     * Salva Dados Pessoa
     * @param array $data
     * @return PessoaTO
     */
    public function salvarPessoa(array $data)
    {
        $uTO = new \UsuarioTO();
        $uTO->setId_usuario($data['id_usuario']);
        $pTO = new \PessoaTO($data);

        $result = $this->_pessoaBo->cadastrarPessoa($uTO, $pTO);
        if ($result->getType() == 'success') {
            return $result->getMensagem();
        }
    }

    public function savePessoa($pTO)
    {
        try {

            $pTO->setBl_ativo(true);
            $pTO->setId_situacao(1);
            $pTO->setId_entidade($pTO->getId_entidade() ? $pTO->getId_entidade() : $pTO->getSessao()->id_entidade);


            $this->beginTransaction();

            $var = new \Ead1\Doctrine\EntitySerializer($this->em);

            $objPessoa = new \G2\Entity\Pessoa();
            if ($pTO->getId_usuario()) {
                $objPessoa = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $pTO->getId_usuario()
                , 'id_entidade' => ($pTO->getId_entidade() ? $pTO->getId_entidade() : $pTO->getSessao()->id_entidade)));

                // necessário pois se não encontrar registros no find anterior, o arrayToEntity recebe uma entity vazia
                if (empty($objPessoa)) {
                    $objPessoa = new \G2\Entity\Pessoa();
                }
            }
            $toArray = $pTO->toArray();
            $toArray['id_usuario'] = $toArray['id_usuario'] ? (int)$toArray['id_usuario'] : null;

            if (isset($toArray['dt_nascimento'])) {
                if ($toArray['dt_nascimento'] instanceof \Zend_Date) {
                    $toArray['dt_nascimento'] = new \DateTime($toArray['dt_nascimento']->toString('Y-MM-d'));
                }
            }

            //verifica se a data esta vazia e seta null na entity para casos de edição
            if (empty($toArray['dt_expedicaocertificadoreservista'])) {
                $objPessoa->setDt_expedicaocertificadoreservista(null);
            }

            if (isset($toArray['sg_fatorrh']) && empty($toArray['sg_fatorrh'])) {
                $objPessoa->setSg_fatorrh(null);
            }

            if (isset($toArray['id_tiposanguineo']) && empty($toArray['id_tiposanguineo'])) {
                $objPessoa->setId_tiposanguineo(null);
            }


            if (isset($toArray['dt_cadastro'])) {
                if ($toArray['dt_cadastro'] instanceof \Zend_Date) {
                    $toArray['dt_cadastro'] = new \DateTime($toArray['dt_cadastro']->toString('Y-MM-d'));
                }
            }

            if (empty($toArray['bl_alterado'])) {
                $toArray['bl_alterado'] = 0;
            }

            $objPessoa = $var->arrayToEntity($toArray, $objPessoa);

            $data = new \DateTime('today');
            $objPessoa->setDt_cadastro($data->format('Y-m-d'));
            $objPessoa = $this->save($objPessoa);

            $this->commit();

            return $objPessoa;
        } catch (\Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new \Zend_Db_Exception($e->getMessage());
        }
    }


    /**
     * Salva a descrição do coordenador na tb_pessoa.
     * @param int $idUsuario
     * @param string $stDescricao
     * @return \G2\Entity\Pessoa
     * @throws \Zend_Db_Exception
     * @internal param array $data
     */

    public function salvarPessoaCadastroResumidoDescricao($idUsuario, $stDescricao)
    {

        try {
            $this->beginTransaction();

            $pessoaEntity = $this->findOneBy('\G2\Entity\Pessoa', array(
                'id_usuario' => $idUsuario,
                'id_entidade' => $this->sessao->id_entidade
            ));

            $pessoaEntity->setSt_descricaocoordenador($stDescricao);
            $this->save($pessoaEntity);
            $this->commit();

            return $pessoaEntity;

        } catch (\Exception $e) {
            $this->rollBack();
            $this->excecao = $e->getMessage();
            THROW new \Zend_Db_Exception($e->getMessage());
        }
    }

    /**
     * @description Salvar cadastro resumido para Pessoa
     * @author Kayo Sival <kayo.silva@unyleya.com.br>
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvarPessoaCadastroResumido(array $data)
    {
        try {
            if ($data) {

                // Se possuir o ID
                if (!empty($data['id'])) {
                    $data['id_usuario'] = $data['id'];
                }

                //unset id
                unset($data['id']);

                // Preencher os dados
                $uTO = $this->preencheUsuarioTO($data); //Preenche usuario


                // Validar CPF
                if (!\G2\Utils\FormValidator::cpf($uTO->getSt_cpf())) {
                    return new \Ead1_Mensageiro('O CPF preenchido é inválido.', \Ead1_IMensageiro::ERRO);
                }

                // Validar Sexo
                if (empty($data['st_sexo'])) {
                    return new \Ead1_Mensageiro('O campo "Sexo" não foi preenchido.', \Ead1_IMensageiro::ERRO);
                }

                $pTO = new \PessoaTO();
                $pTO->setId_usuario($uTO->getId_usuario());
                $pTO->setSt_nomeexibicao($data['st_nomecompleto'] ? $data['st_nomecompleto'] : null);
                $pTO->setId_estadocivil($data['id_estadocivil']);
                $pTO->setDt_nascimento(new \Zend_Date($data['dt_nascimento']));
                $pTO->setSt_sexo($data['st_sexo'] ? $data['st_sexo'] : null);
                $pTO->setSt_nomemae($data['st_nomemae'] ? $data['st_nomemae'] : null);
                $pTO->setSt_nomepai($data['st_nomepai'] ? $data['st_nomepai'] : null);
                $pTO->setId_pais($data['id_nacionalidade'] ? $data['id_nacionalidade'] : null);
                $pTO->setSt_passaporte(array_key_exists('st_passaporte', $data) ? $data['st_passaporte'] : null);
                if (!empty($data['id_municipionascimento'])) {
                    $pTO->setId_municipionascimento($data['id_municipionascimento']);
                }
                if (!empty($data['sg_ufnascimento'])) {
                    $pTO->setSg_ufnascimento($data['sg_ufnascimento']);
                }

                $pTO->setSt_tituloeleitor(isset($data['st_tituloeleitor']) ? $data['st_tituloeleitor'] : null);
                $pTO->setSt_zonaeleitoral(isset($data['st_zonaeleitoral']) ? $data['st_zonaeleitoral'] : null);
                $pTO->setSt_secaoeleitoral(isset($data['st_secaoeleitoral']) ? $data['st_secaoeleitoral'] : null);
                $pTO->setSt_municipioeleitoral(isset($data['st_municipioeleitoral']) ? $data['st_municipioeleitoral'] : null);

                $pTO->setSt_certificadoreservista(array_key_exists('st_certificadoreservista', $data) ? $data['st_certificadoreservista'] : null);
                $pTO->setDt_expedicaocertificadoreservista(array_key_exists('dt_expedicaocertificadoreservista', $data) && !empty($data['dt_expedicaocertificadoreservista']) ? new \Zend_Date($data['dt_expedicaocertificadoreservista']) : null);
                $pTO->setId_categoriaservicomilitar(array_key_exists('id_categoriaservicomilitar', $data) ? $data['id_categoriaservicomilitar'] : null);
                $pTO->setSt_reparticaoexpedidora(array_key_exists('st_reparticaoexpedidora', $data) ? $data['st_reparticaoexpedidora'] : null);

                // Informações sanguíneas
                $pTO->setId_tiposanguineo(array_key_exists('id_tiposanguineo', $data) ? $data['id_tiposanguineo'] : null);
                $pTO->setSg_fatorrh(array_key_exists('sg_fatorrh', $data) ? $data['sg_fatorrh'] : null);

                $st_identificacao = isset($data['st_identificacao']) ? $data['st_identificacao'] : null;

                if ($st_identificacao != $pTO->getSt_identificacao()) {
                    $pTO->setbl_alterado(1);
                }

                $pTO->setSt_identificacao($st_identificacao);

                //Salva dados bancarios
                if (!empty($data['st_agencia']) && !empty($data['st_conta']) && !empty($data['id_banco'])) {
                    //Busca se já existe conta para esse usuario para essa entidade
                    $contaPessoa = $this->findOneBy('\G2\Entity\ContaPessoa', array(
                        'id_usuario' => $data['id_usuario'],
                        'id_entidade' => $this->sessao->id_entidade
                    ));

                    //Verifica e se não existir nada aplica a entity
                    if (!isset($contaPessoa) && empty($contaPessoa)) {
                        $contaPessoa = new ContaPessoa();
                    }

                    //remove caracteres
                    if (isset($data['st_cnpj'])) {
                        $data['st_cnpj'] = str_replace('.', '', str_replace('/', '', str_replace('-', '', $data['st_cnpj'])));
                    }
                    $this->beginTransaction();
                    $contaPessoa->setId_contapessoa(($contaPessoa->getId_contapessoa()) ? $contaPessoa->getId_contapessoa() : NULL);
                    $contaPessoa->setBl_padrao(true);
                    $contaPessoa->setId_tipopessoa($this->getReference('\G2\Entity\TipoPessoa', $data['id_tipopessoa']));
                    $contaPessoa->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
                    $contaPessoa->setSt_banco($data['id_banco']);
                    $contaPessoa->setSt_agencia($data['st_agencia']);
                    $contaPessoa->setSt_digitoagencia($data['st_digitoagencia']);
                    $contaPessoa->setId_usuario($this->getReference('\G2\Entity\Usuario', $data['id_usuario']));
                    $contaPessoa->setSt_conta($data['st_conta']);
                    $contaPessoa->setSt_digitoconta($data['st_digitoconta']);
                    $contaPessoa->setSt_cnpj(isset($data['st_cnpj']) ? $data['st_cnpj'] : null);
                    $contaPessoa->setId_tipodeconta(TipodeConta::CONTACORRENTE);
                    $this->save($contaPessoa);
                    $this->commit();
                }

                //Documento identidade
                $dTO = new \DocumentoIdentidadeTO();
                $dTO->montaToDinamico($data);
                if ($dTO->getId_entidade() != $this->sessao->id_entidade) {
                    $dTO->setId_entidade($this->sessao->id_entidade);
                }

                //Verifica alterações nos telefones e nos endereços.
                $attrDiferentes = array();

                //Contatos Telefone
                $tTO = $this->preencheContatosTelefoneTO($data);

                $tpTO = new \ContatosTelefonePessoaTO();
                $tpTO->montaToDinamico(array(
                    'id_usuario' => $uTO->getId_usuario(),
                    'bl_padrao' => true,
                    'id_telefone' => $tTO->getId_telefone(),
                    'id_entidade' => $this->sessao->id_entidade));

                $phoneAtual = $tTO->fetch(true, true);
                if ($phoneAtual) {
                    foreach ($tTO as $attr => $val) {
                        if ($val != $phoneAtual->$attr)
                            $attrDiferentes[] = $attr;
                    }
                }

                //Email
                $eTO = $this->preencheContatosEmailTO($data);
                $epTO = new \ContatosEmailPessoaPerfilTO(array(
                    'bl_padrao' => 1,
                    'bl_ativo' => 1,
                    'id_usuario' => $uTO->getId_usuario(),
                    'id_email' => $eTO->getId_email()
                ));

                //Endereço
                $endTO = $this->preencheEnderecoTO($data);
                $pendTO = new \PessoaEnderecoTO(array(
                    'id_usuario' => $uTO->getId_usuario(),
                    'id_endereco' => $endTO->getId_endereco()
                ));

                // Verifica se algum campo do Endereco Residencial foi alterado
                $enderecoAtual = $endTO->fetch(true, true);
                if ($enderecoAtual) {
                    foreach ($endTO as $attr => $val) {
                        if ($attr == 'st_cep') {
                            $val = preg_replace('/\D/', '', $val);
                            $enderecoAtual->$attr = preg_replace('/\D/', '', $enderecoAtual->$attr);
                        }
                        if ($val != $enderecoAtual->$attr) {
                            $attrDiferentes[] = $attr . '_corresp';
                        }
                    }
                }

                $end_corresp = new \EnderecoTO();
                $pend_corresp = new \PessoaEnderecoTO();

                if (isset($data['id_tipoendereco_corresp']) && $data['id_tipoendereco_corresp']) {

                    $end_corresp = array(
                        'id_endereco' => $data['id_endereco_corresp'],
                        'id_pais' => $data['id_pais_corresp'],
                        'st_cep' => $data['st_cep_corresp'],
                        'st_endereco' => $data['st_endereco_corresp'],
                        'nu_numero' => $data['nu_numero_corresp'],
                        'st_bairro' => $data['st_bairro_corresp'],
                        'st_complemento' => $data['st_complemento_corresp'],
                        'sg_uf' => $data['sg_uf_corresp'],
                        'id_tipoendereco' => $data['id_tipoendereco_corresp'],
                        'id_municipio' => $data['id_municipio_corresp']
                    );

                    $end_corresp = $this->preencheEnderecoTO($end_corresp);

                    $pend_corresp = new \PessoaEnderecoTO(array(
                        'id_usuario' => $uTO->getId_usuario(),
                        'id_endereco' => $end_corresp->getId_endereco(),
                        'bl_padrao' => 1
                    ));

                    // Verifica se algum campo do Endereco Residencial foi alterado
                    $enderecoCoAtual = $end_corresp->fetch(true, true);
                    if ($enderecoCoAtual) {
                        foreach ($end_corresp as $attr => $val) {
                            if ($attr == 'st_cep') {
                                $val = preg_replace('/\D/', '', $val);
                                $enderecoCoAtual->$attr = preg_replace('/\D/', '', $enderecoCoAtual->$attr);
                            }
                            if ($val != $enderecoCoAtual->$attr) {
                                $attrDiferentes[] = $attr . '_corresp';
                            }
                        }
                    }
                }


                //seta os atributos de Informações Academicas
                $academicoTO = new \InformacaoAcademicaPessoaTO();
                $academicoTO->montaToDinamico($data);

                //envia para BO reutilizando metodo
                $result = $this->_pessoaBo->procedimentoSalvarPessoaCadastroResumido($uTO, $pTO, $dTO, $tTO, $tpTO, $eTO, $epTO, $pendTO, $endTO, $academicoTO, $pend_corresp, $end_corresp);
                if ($result->getType() == 'success') { //if success

                    $d1 = $this->converteDataBanco($data['dt_nascimento']);
                    $d2 = new \DateTime("now");
                    $diff = $d2->diff($d1);
                    if (isset($data['st_cpfResponsavelLegal']) && !empty($data['st_cpfResponsavelLegal']) && $diff->y < 18) {
                        //insere responsável legal
                        $objResponsavelLegal = $this->findOneBy('\G2\Entity\ResponsavelLegal', array('id_usuario' => $result->mensagem[0]->getId_usuario()));

                        if (!$objResponsavelLegal)
                            $objResponsavelLegal = new \G2\Entity\ResponsavelLegal();

                        $objResponsavelLegal->setId_usuario($this->getReference('\G2\Entity\Usuario', $result->mensagem[0]->getId_usuario()));
                        $objResponsavelLegal->setSt_cpf($this->limpaCpfCnpj($data['st_cpfResponsavelLegal']));
                        $objResponsavelLegal->setSt_nomecompleto($data['st_nomeResponsavelLegal']);
                        $objResponsavelLegal->setNu_ddd($data['nu_dddResponsavelLegal']);
                        $objResponsavelLegal->setNu_telefone($data['nu_telResponsavelLegal']);
                        $objResponsavelLegal->setDt_cadastro(new \DateTime());
                        $objResponsavelLegal->setBl_ativo(true);
                        $this->save($objResponsavelLegal);
                    } else {
                        $objResponsavelLegal = $this->findOneBy('\G2\Entity\ResponsavelLegal', array('id_usuario' => $result->mensagem[0]->getId_usuario()));
                        if ($objResponsavelLegal) {
                            $objResponsavelLegal->setBl_ativo(false);
                            $this->save($objResponsavelLegal);
                        }
                    }
                    // Se algum dado ja cadastrado, tiver sido alterado, enviar o email para o usuario
                    if ($attrDiferentes)
                        $result->enviaEmail = $this->enviarEmailAlteracaoDados($uTO);

                    //salava telefone secundario
                    if ($data['id_tipotelefone2']) {
                        //Salva ContatosTelefone
                        $resultTel = $this->salvaContatosTelefone(array(
                            'id_tipotelefone' => $data['id_tipotelefone2'],
                            'nu_ddi' => $data['nu_ddi2'],
                            'nu_ddd' => $data['nu_ddd2'],
                            'nu_telefone' => $data['nu_telefone2'],
                            'id_telefone' => (isset($data['id_telefone2']) ? $data['id_telefone2'] : null),
                        ));

                        if ($resultTel) {
                            $result->addMensagem($resultTel); //Adiciona no mensageiro
                            //salva ContatosTelefonePessoa
                            $resultPessoaTel = $this->salvaContatosTelefonePessoa(array(
                                'id_telefone' => $resultTel->getId_telefone(),
                                'id_usuario' => $result->mensagem[0]->getId_usuario(),
                                'bl_padrao' => false
                            ));

                            if (!$resultPessoaTel) {
                                return new \Ead1_Mensageiro('Erro ao tentar vincular telefone secundário ao usuário.', \Ead1_IMensageiro::ERRO);
                            } else {
                                //adiciona no mensageiro
                                $result->addMensagem($resultPessoaTel);
                            }
                        } else {
                            return new \Ead1_Mensageiro('Erro ao tentar salvar telefone secundário.', \Ead1_IMensageiro::ERRO);
                        }
                    }
                }

                //Verifica se existe o usuário no G1 para Criar o JOB e Sincronizar.

                $entidadeIntegracaoG1 = $this->findOneBy('\G2\Entity\EntidadeIntegracao', array('id_entidade' => $result->mensagem[1]->getId_entidade(), 'id_sistema' => \G2\Constante\Sistema::ACTOR));
                if ($entidadeIntegracaoG1) {
                    $ws = new \actor_Pessoa($entidadeIntegracaoG1->getSt_codsistema(), $entidadeIntegracaoG1->getSt_codchave(), $entidadeIntegracaoG1->getSt_caminho());
                    $usuarioG1 = $ws->retornarPessoaSincronizacaoG1($result->mensagem[0]->getId_usuario());
                    if ($usuarioG1) {
                        $this->criarJobPessoa(
                            array(
                                'id_usuario' => $result->mensagem[0]->getId_usuario(),
                                'id_entidade' => $result->mensagem[1]->getId_entidade(),
                                'id_sistema' => array(\G2\Constante\Sistema::ACTOR, \G2\Constante\Sistema::FLUXUS)
                            )
                        );
                    }

                }

                //Verifica se existe integração com o fluxus para Criar o JOB e Sincronizar.
                $entidadeIntegracaoFluxus = $this->findOneBy('\G2\Entity\EntidadeIntegracao', array('id_entidade' => $result->mensagem[1]->getId_entidade(), 'id_sistema' => \G2\Constante\Sistema::FLUXUS));
                if ($entidadeIntegracaoFluxus) {
                    $this->criarJobPessoa(
                        array(
                            'id_usuario' => $result->mensagem[0]->getId_usuario(),
                            'id_entidade' => $result->mensagem[1]->getId_entidade(),
                            'id_sistema' => array(\G2\Constante\Sistema::FLUXUS)
                        )
                    );

                }
                return $result;
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao tentar salvar dados, verifique os campos obrigatórios.' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $depois
     * @param array $antes
     * @throws \Zend_Exception
     */
    private function criarJobPessoa(Array $params)
    {
        try {

            if ($params) {

                //cria o job
                $negocio = new \G2\Negocio\Negocio();
                $schedule_time = date('Y-m-d H:i:s', time() + 10);
                $job = $negocio->createHttpJob(
                    '/robo/zend-job-sincroniza-pessoa', $params, array(
                    'name' => 'Sincronizacao de Pessoa.',
                    'schedule_time' => $schedule_time,
                    'queue_name' => 'SincronizarPessoa'
                ),
                    \G2\Constante\Sistema::GESTOR
                );
                return $job;
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar criar JOB no servidor. " . $e->getMessage());
        }
    }

    /**
     * Preenche atributos de UsuarioTO a partir do array
     * @param array $data
     * @return \UsuarioTO
     */
    public function preencheUsuarioTO(array $data)
    {
        $usuarioTO = new \UsuarioTO();
        if (isset($data['id_usuario']))
            $usuarioTO->setId_usuario($data['id_usuario']);

        if (isset($data['st_cpf']))
            $usuarioTO->setSt_cpf(str_replace('.', null, str_replace('-', null, $data['st_cpf'])));

        if (isset($data['st_login']))
            $usuarioTO->setSt_login(strtolower($data['st_login']));

        if (isset($data['st_senha']))
            $usuarioTO->setSt_senha($data['st_senha'], true);

        if (isset($data['st_nomecompleto']))
            $usuarioTO->setSt_nomecompleto($data['st_nomecompleto']);

        if (isset($data['id_usuariopai']))
            $usuarioTO->setId_usuariopai($data['id_usuariopai']);

        if (isset($data['bl_ativo']))
            $usuarioTO->setBl_ativo($data['bl_ativo']);

        if (isset($data['id_registropessoa']))
            $usuarioTO->setId_registropessoa($data['id_registropessoa']);

        if (isset($data['bl_redefinicaosenha']))
            $usuarioTO->setBl_redefinicaosenha($data['bl_redefinicaosenha']);

        if (isset($data['id_titulacao'])) {
            $usuarioTO->setId_titulacao($data['id_titulacao']);
        }

        return $usuarioTO;
    }

    /**
     * Preenche atributos de ContatosTelefoneTO a partir de um array
     * @param array $data
     * @return \ContatosTelefoneTO
     */
    public function preencheContatosTelefoneTO(array $data)
    {
        $contatosTelefoneTO = new \ContatosTelefoneTO();

        if (isset($data['id_telefone']))
            $contatosTelefoneTO->setId_telefone($data['id_telefone']);

        if (isset($data['nu_telefone']))
            $contatosTelefoneTO->setNu_telefone($data['nu_telefone']);

        if (isset($data['nu_ddd']))
            $contatosTelefoneTO->setNu_ddd($data['nu_ddd']);

        if (isset($data['nu_ddi']))
            $contatosTelefoneTO->setNu_ddi($data['nu_ddi']);

        if (isset($data['id_tipotelefone']))
            $contatosTelefoneTO->setId_tipotelefone($data['id_tipotelefone']);

        if (isset($data['st_descricao']))
            $contatosTelefoneTO->setSt_descricao($data['st_descricao']);

        return $contatosTelefoneTO;
    }

    /**
     * Preenche CotnatosEmailTO by array
     * @param array $data
     * @return \ContatosEmailTO
     */
    private function preencheContatosEmailTO(array $data)
    {
        $emailTO = new \ContatosEmailTO();
        if (isset($data['st_email']))
            $emailTO->setSt_email($data['st_email']);

        if (isset($data['id_email']))
            $emailTO->setId_email($data['id_email']);

        return $emailTO;
    }

    /**
     * Preenche EnderecoTO by array
     * @param array $data
     * @return \EnderecoTO
     */
    public function preencheEnderecoTO(array $data)
    {
        $enderecoTO = new \EnderecoTO();
        if (isset($data['id_pais']))
            $enderecoTO->setId_pais($data['id_pais']);

        if (isset($data['sg_uf']))
            $enderecoTO->setSg_uf($data['sg_uf']);

        if (isset($data['id_municipio']))
            $enderecoTO->setId_municipio($data['id_municipio']);

        if (isset($data['id_endereco']))
            $enderecoTO->setId_endereco($data['id_endereco']);

        if (isset($data['id_tipoendereco']))
            $enderecoTO->setId_tipoendereco($data['id_tipoendereco']);

        if (isset($data['st_cep']))
            $enderecoTO->setSt_cep($data['st_cep']);

        if (isset($data['st_endereco']))
            $enderecoTO->setSt_endereco($data['st_endereco']);

        if (isset($data['st_bairro']))
            $enderecoTO->setSt_bairro($data['st_bairro']);

        if (isset($data['st_complemento']))
            $enderecoTO->setSt_complemento($data['st_complemento']);

        if (isset($data['nu_numero']))
            $enderecoTO->setNu_numero($data['nu_numero']);

        if (isset($data['st_estadoprovincia']))
            $enderecoTO->setSt_estadoprovincia($data['st_estadoprovincia']);

        if (isset($data['st_cidade'])) {
            $enderecoTO->setSt_cidade($data['st_cidade']);
        } elseif (isset($data['id_municipio'])) {
            $mun = new \MunicipioTO();
            $mun->setId_municipio($data['id_municipio']);
            $mun->fetch(true, true, true);

            $enderecoTO->setSt_cidade($mun->getSt_nomemunicipio());
        }

        if (isset($data['bl_ativo']))
            $enderecoTO->setBl_ativo($data['bl_ativo']);
        else
            $enderecoTO->setBl_ativo(1);


        return $enderecoTO;
    }

    /**
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @date 2015-04-30
     * @description Função para envio do email de
     * alerta de alteração no cadastro.
     */

    public function enviarEmailAlteracaoDados($usuarioTO)
    {
        $bo = new \MensagemBO();

        $pessoa = $this->findOneBy($this->repositoryName, array('id_usuario' => $usuarioTO->getId_usuario(), 'id_entidade' => $this->sessao->id_entidade));

        $result = array();
        if ($pessoa) {
            $pessoaTo = new \PessoaTO();
            $pessoaTo->montaToDinamico(array(
                'id_usuario' => $pessoa->getId_usuario(),
                'id_entidade' => $pessoa->getId_entidade() ? $pessoa->getId_entidade()->getId_entidade() : NULL,
                'st_sexo' => $pessoa->getSt_sexo(),
                'st_nomeexibicao' => $pessoa->getSt_nomeexibicao(),
                'dt_nascimento' => $pessoa->getDt_nascimento() ? $pessoa->getDt_nascimento()->format('Y-m-d H:i:s') : NULL,
                'st_nomepai' => $pessoa->getSt_nomepai(),
                'st_nomemae' => $pessoa->getSt_nomemae(),
                'id_pais' => $pessoa->getId_pais() ? $pessoa->getId_pais()->getId_pais() : NULL,
                'sg_uf' => $pessoa->getSg_uf(),
                'id_municipio' => $pessoa->getId_municipio() ? $pessoa->getId_municipio()->getId_municipio() : NULL,
                'dt_cadastro' => $pessoa->getDt_cadastro(),
                'id_usuariocadastro' => $pessoa->getId_usuariocadastro(),
                'st_passaporte' => $pessoa->getSt_passaporte(),
                'id_situacao' => $pessoa->getId_situacao() ? $pessoa->getId_situacao()->getId_situacao() : NULL,
                'bl_ativo' => $pessoa->getBl_ativo(),
                'id_estadocivil' => $pessoa->getId_estadocivil() ? $pessoa->getId_estadocivil()->getId_estadocivil() : NULL,
                'st_urlavatar' => $pessoa->getSt_urlavatar(),
                'st_cidade' => $pessoa->getSt_cidade(),
            ));
            $mensagemBo = new \MensagemBO();
            $result = $mensagemBo->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::ALTERACAO_CADASTRO_USUARIO, $pessoaTo, \TipoEnvioTO::EMAIL);
        }
        return $result;
    }

    /**
     * Salva ContatosTelefone
     * @param array $data
     * @return \G2\Entity\ContatosTelefone Entity or Exception
     */
    public function salvaContatosTelefone(array $data)
    {
        try {
            if ($data) { //if $data
                $entity = new \G2\Entity\ContatosTelefone(); //Get instance entity
                //Verifica se é um novo telefone
                if (isset($data['id_telefone'])) { //if primary key set
                    $telefone = $this->find('\G2\Entity\ContatosTelefone', $data['id_telefone']); //Find telefone
                    if ($telefone)
                        $entity = $telefone;
                }
                $tipoTelefone = $this->getReference('\G2\Entity\TipoTelefone', $data['id_tipotelefone']); //get reference of the tipo telefone
                //Set data on entity
                $entity->setNu_ddi($data['nu_ddi'])
                    ->setNu_ddd($data['nu_ddd'])
                    ->setNu_telefone($data['nu_telefone'])
                    ->setId_tipotelefone($tipoTelefone);
                //save
                return $this->save($entity);
            }
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva Contatos Telefone Pessoa
     * @param array $data
     * @return \G2\Entity\ContatosTelefonePessoa Entity or Exception
     */
    public function salvaContatosTelefonePessoa(array $data)
    {
        try {
            if ($data) {
                $data['id_entidade'] = (isset($data['id_entidade']) ? $data['id_entidade'] : $this->sessao->id_entidade);

                $resultTelPessoa = $this->findOneBy('\G2\Entity\ContatosTelefonePessoa', array(
                    'id_telefone' => $data['id_telefone'],
                    'id_usuario' => $data['id_usuario'],
                    'id_entidade' => $data['id_entidade']
                ));

                if ($resultTelPessoa) {
                    $entity = $resultTelPessoa;
                } else {
                    $entity = new \G2\Entity\ContatosTelefonePessoa();
                }

                $entity->setId_entidade($data['id_entidade'])
                    ->setId_usuario($data['id_usuario'])
                    ->setId_telefone($data['id_telefone']);

                //set bl_padrao
                if (isset($data['bl_padrao'])) {
                    $entity->setBl_padrao($data['bl_padrao']);
                }
                return $this->save($entity);
            }
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva Caadastro resumido dados Bancarios para pessoa
     * @param array $data Dados pessoa
     * @return array
     * @author Kayo Sival <kayo.silva@unyleya.com.br>
     */
    public function salvarPessoaCadastroResumidoDadosBancarios(array $data)
    {
        try {
            if ($data) {

                if (isset($data['id'])) { //if set id
                    $data['id_usuario'] = $data['id']; //set id_usuario
                    unset($data['id']); //unset id
                }

                //Salva dados bancarios
                if (!empty($data['st_agencia']) && !empty($data['st_conta']) && !empty($data['id_banco'])) {
                    //Busca se já existe conta para esse usuario para essa entidade
                    $contaPessoa = $this->findOneBy('\G2\Entity\ContaPessoa', array(
                        'id_usuario' => $data['id_usuario'],
                        'id_entidade' => $this->sessao->id_entidade
                    ));

                    //Verifica e se não existir nada aplica a entity
                    if (!isset($contaPessoa) && empty($contaPessoa)) {
                        $contaPessoa = new ContaPessoa();
                    }

                    //remove caracteres
                    if (isset($data['st_cnpj'])) {
                        $data['st_cnpj'] = str_replace('.', '', str_replace('/', '', str_replace('-', '', $data['st_cnpj'])));
                    }
                    $this->beginTransaction();
                    $contaPessoa->setId_contapessoa(($contaPessoa->getId_contapessoa()) ? $contaPessoa->getId_contapessoa() : NULL);
                    $contaPessoa->setBl_padrao(true);
                    $contaPessoa->setId_tipopessoa($this->getReference('\G2\Entity\TipoPessoa', $data['id_tipopessoa']));
                    $contaPessoa->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
                    $contaPessoa->setSt_banco($data['id_banco']);
                    $contaPessoa->setSt_agencia($data['st_agencia']);
                    $contaPessoa->setSt_digitoagencia(($data['st_digitoagencia']) ? $data['st_digitoagencia'] : null);
                    $contaPessoa->setId_usuario($this->getReference('\G2\Entity\Usuario', $data['id_usuario']));
                    $contaPessoa->setSt_conta($data['st_conta']);
                    $contaPessoa->setSt_digitoconta(($data['st_digitoconta']) ? $data['st_digitoconta'] : null);
                    $contaPessoa->setSt_cnpj(isset($data['st_cnpj']) ? $data['st_cnpj'] : null);
                    $contaPessoa->setId_tipodeconta(TipodeConta::CONTACORRENTE);
                    $this->save($contaPessoa);
                    $this->commit();

                    $mensageiro = new \Ead1_Mensageiro();
                    $mensageiro->setMensageiro('Os dados bancarios foram salvos com sucesso.', \Ead1_IMensageiro::SUCESSO);
                } else {
                    $mensageiro = new \Ead1_Mensageiro();
                    $mensageiro->setMensageiro('Verifique se todos os campos foram preenchidos corretamente.', \Ead1_IMensageiro::AVISO);
                }

                return $mensageiro;
                //Return
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao tentar salvar dados bancários. ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Recupera os dados do usuário e retorna array com chaves para formulário de cadastro rapido
     * @param integer $idUsuario Id do usuário
     * @return array Dados do usuário para formulário
     */
    public function getDadosCadastroRapidoUsuario($idUsuario)
    {
        $arrData = array();
        $usuarioEntidadeTO = new \UsuarioEntidadeTO();
        $usuarioEntidadeTO->setId_usuario($idUsuario);
        $result = $this->_pessoaBo->retornarDadosUsuarioEntidade($usuarioEntidadeTO);
        if ($result->getType() == 'success') {
            foreach ($result->getMensagem() as $dados) {
                //Seta os dados de usuário
                if ($dados instanceof \UsuarioTO) { // if instanceof UsuarioTO
                    $arrData['id'] = $dados->getId_usuario();
                    $arrData['st_cpf'] = $dados->getSt_cpf();
                    $arrData['st_nomecompleto'] = $dados->getSt_nomecompleto();
                    $arrData['st_login'] = $dados->getSt_login();
                    $arrData["id_titulacao"] = $dados->getId_titulacao();
                    $respLegal = $this->findOneBy('\G2\Entity\ResponsavelLegal', array('id_usuario' => $dados->getId_usuario(), 'bl_ativo' => true));
                    if ($respLegal) {
                        $arrData['st_cpfResponsavelLegal'] = $respLegal->getSt_cpf();
                        $arrData['st_nomeResponsavelLegal'] = $respLegal->getSt_nomecompleto();
                        $arrData['nu_dddResponsavelLegal'] = $respLegal->getNu_ddd();
                        $arrData['nu_telefoneResponsavelLegal'] = $respLegal->getNu_telefone();
                    }
                }

                //Seta os dados de pessoa
                if ($dados instanceof \PessoaTO) {
                    $dtNascimento = NULL;
                    //Recupera a data de nascimento da pessoa
                    if ($dados->getDt_nascimento() instanceof \Zend_Date) { //verifica se é uma instancia de zend_date
                        $dtNascimento = $dados->getDt_nascimento()->toString('dd/MM/Y'); //formata a data para padrão brasil
                    } else { //se não for um zend_date
                        $dtNasicmento = new \DateTime($dados->getDt_nascimento()); //cria um datetime
                        $dtNasicmento = $dtNasicmento->format('dd/MM/Y'); //formata o valor para padrão brasil
                    }
                    $arrData['st_urlavatar'] = $dados->getSt_urlavatar(); //passa o url_avatar
                    $arrData['dt_nascimento'] = $dtNascimento; //passa a dt_nascimento
                    $arrData['id_pais'] = $dados->getId_pais();
                    $arrData['st_nomepai'] = $dados->getSt_nomepai();
                    $arrData['st_nomemae'] = $dados->getSt_nomemae();
                    $arrData['st_sexo'] = $dados->getSt_sexo();
                    $arrData['st_identificacao'] = $dados->getSt_identificacao();//busca dados da identificação da carteirinha
                    $arrData['st_passaporte'] = $dados->getSt_passaporte();
                    $arrData['st_tituloeleitor'] = preg_replace("/[^0-9\s]/", "", $dados->getSt_tituloeleitor());
                    $arrData['st_secaoeleitoral'] = $dados->getSt_secaoeleitoral();
                    $arrData['st_zonaeleitoral'] = $dados->getSt_zonaeleitoral();
                    $arrData['st_municipioeleitoral'] = $dados->getSt_municipioeleitoral();
                    $arrData['st_certificadoreservista'] = $dados->getSt_certificadoreservista();
                    $arrData['id_categoriaservicomilitar'] = $dados->getId_categoriaservicomilitar();
                    $arrData['st_reparticaoexpedidora'] = $dados->getSt_reparticaoexpedidora();
                    $arrData['dt_expedicaocertificadoreservista'] = $dados->getDt_expedicaocertificadoreservista() ?
                        $dados->getDt_expedicaocertificadoreservista()->toString('dd/MM/Y') : '';
                    $arrData['sg_ufnascimento'] = $dados->getSg_ufnascimento();
                    $arrData['id_municipionascimento'] = $dados->getId_municipionascimento();
                    $arrData['id_tiposanguineo'] = $dados->getId_tiposanguineo();
                    $arrData['sg_fatorrh'] = $dados->getSg_fatorrh();
                }

                //identidade
                if ($dados instanceof \DocumentoIdentidadeTO) {
                    $arrData['st_rg'] = $dados->getSt_rg();
                    $arrData['st_orgaoexpeditor'] = $dados->getSt_orgaoexpeditor();

                    $dtExpedicao = NULL;
                    if ($dados->getDt_dataexpedicao() instanceof \Zend_Date) { //verifica se é uma instancia de zend_date
                        $dtExpedicao = $dados->getDt_dataexpedicao()->toString('dd/MM/Y'); //formata a data para padrão brasil
                    } else { //se não for um zend_date
                        $dtExpedicao = new \DateTime($dados->getDt_dataexpedicao()); //cria um datetime
                        $dtExpedicao = $dtExpedicao->format('dd/MM/Y'); //formata o valor para padrão brasil
                    }
                    $arrData['dt_dataexpedicao'] = $dtExpedicao;
                }


                if (is_array($dados)) { //is array percorre o array e verifica os objets

                    foreach ($dados as $row) {
                        if ($row instanceof \EnderecoTO) {
                            if ($row->getBl_padrao()) {

                                if ($row->getId_tipoendereco() == 5) {
                                    $sufix = "_corresp";

                                } else {
                                    $sufix = "";
                                }

                                $arrData['id_paisendereco' . $sufix] = $row->getId_pais();
                                $arrData['id_endereco' . $sufix] = $row->getId_endereco();
                                $arrData['sg_uf' . $sufix] = $row->getSg_uf();
                                $arrData['st_cep' . $sufix] = $row->getSt_cep();
                                $arrData['id_municipio' . $sufix] = $row->getId_municipio();
                                $arrData['id_tipoendereco' . $sufix] = $row->getId_tipoendereco();
                                $arrData['st_endereco' . $sufix] = $row->getSt_endereco();
                                $arrData['st_bairro' . $sufix] = $row->getSt_bairro();
                                $arrData['st_cidade' . $sufix] = $row->getSt_cidade();
                                $arrData['st_complemento' . $sufix] = $row->getSt_complemento();
                                $arrData['nu_numero' . $sufix] = $row->getNu_numero();

                            }
                        }
                        if ($row instanceof \VwPessoaEmailTO) {
                            if ($row->getBl_padrao()) {
                                $arrData['id_email'] = $row->getId_email();
                                $arrData['st_email'] = $row->getSt_email();
                            }
                        }
                        if ($row instanceof \VwPessoaTelefoneTO) {
                            if ($row->getBl_padrao()) {
                                $arrData['id_telefone'] = $row->getId_telefone();
                                $arrData['id_tipotelefone'] = $row->getId_tipotelefone();
                                $arrData['nu_ddi'] = $row->getNu_ddi();
                                $arrData['nu_ddd'] = $row->getNu_ddd();
                                $arrData['nu_telefone'] = $row->getNu_telefone();
                            } else {
                                $arrData['id_telefone2'] = $row->getId_telefone();
                                $arrData['id_tipotelefone2'] = $row->getId_tipotelefone();
                                $arrData['nu_ddi2'] = $row->getNu_ddi();
                                $arrData['nu_ddd2'] = $row->getNu_ddd();
                                $arrData['nu_telefone2'] = $row->getNu_telefone();
                            }
                        }

                        if ($row instanceof \VwPessoaInformacaoAcademicaTO) {
                            $arrData['id_informacaoacademicapessoa'] = $row->getId_informacaoacademicapessoa();
                            $arrData['id_nivelensino'] = $row->getId_nivelensino();
                            $arrData['st_curso'] = $row->getSt_curso();
                            $arrData['st_nomeinstituicao'] = $row->getSt_nomeinstituicao();
                        }

                    }
                }
            }

            if (!isset($arrData['id_tipoendereco_corresp'])) {
                $arrData['id_paisendereco_corresp'] = '';
                $arrData['id_endereco_corresp'] = '';
                $arrData['sg_uf_corresp'] = '';
                $arrData['st_cep_corresp'] = '';
                $arrData['id_municipio_corresp'] = '';
                $arrData['id_tipoendereco_corresp'] = '';
                $arrData['st_endereco_corresp'] = '';
                $arrData['st_bairro_corresp'] = '';
                $arrData['st_cidade_corresp'] = '';
                $arrData['st_complemento_corresp'] = '';
                $arrData['nu_numero_corresp'] = '';
            }
        }
        return $arrData;
    }

    /**
     * Salva avatar da pessoa na tabela
     * @param array $data
     * @return Object Entity
     */
    public function salvaAvatarPessoa(array $data)
    {
        try {
            if ($data) {
                $id_entidade = isset($data['id_entidade']) ? $data['id_entidade'] : $this->sessao->id_entidade;
                $entity = $this->findOneBy($this->repositoryName, array(
                    'id_usuario' => $data['id_usuario'],
                    'id_entidade' => $id_entidade
                ));

                if ($entity) {
                    $entity->setSt_urlavatar($data['st_urlavatar']);
                    return $this->save($entity);
                }

                return false;

            }
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna telefone da pessoa buscando da vw_pessoatelefone
     * @param array $params
     * @return array
     */
    public function retornaTelefonePessoa(array $params)
    {
        try {
            $result = $this->findBy('G2\Entity\VwPessoaTelefone', $params);
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    $arrReturn[] = $row->_toArray();
                }
            }
            return $arrReturn;
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna o email da pessoa
     * @param array $params
     * @return array
     */
    public function retornaEmailPessoa(array $params)
    {
        try {
            $result = $this->findBy('G2\Entity\VwPessoaEmail', $params);
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {
                    $arrReturn[] = $row->_toArray();
                }
            }
            return $arrReturn;
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna os dados da VwPessoa e mais alguns
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornaDadosPessoa(array $params)
    {
        try {
            $result = $this->findBy('G2\Entity\VwPessoa', $params);
            $arrReturn = array();
            if ($result) {
                foreach ($result as $row) {

                    $arpt = null;
                    $pt = $this->findBy('G2\Entity\VwPessoaTelefone',
                        array('id_usuario' => $row->getId_usuario(), 'id_entidade' => $row->getid_entidade())
                    );
                    if ($pt) {
                        foreach ($pt as $ept) {
                            switch ($ept->getid_tipotelefone()) {
                                case \TipoTelefoneTO::CELULAR:
                                    $arpt['celular'] = $ept->toBackBoneArray();
                                    break;
                                case \TipoTelefoneTO::COMERCIAL:
                                    $arpt['comercial'] = $ept->toBackBoneArray();
                                    break;
                                case \TipoTelefoneTO::RESIDENCIAL:
                                    $arpt['residencial'] = $ept->toBackBoneArray();
                                    break;
                            }
                        }
                    }


                    $ic = new \VwPessoaInformacaoAcademicaTO();
                    $ic->setId_usuario($row->getId_usuario());
                    $ic->setId_entidade($row->getid_entidade());
                    $ic->fetch(false, true, true);


                    $pt = new \VwPessoaInformacaoProfissionalTO();
                    $pt->setId_usuario($row->getId_usuario());
                    $pt->setId_entidade($row->getid_entidade());
                    $pt->fetch(false, true, true);

                    $arrdados = array_merge($row->toBackBoneArray(), array('telefones' => $arpt), $ic->toBackboneArray(), $pt->toBackboneArray());
                    $arrReturn[] = $arrdados;

                }
            }
            return $arrReturn;
        } catch (\Doctrine\DBAL\DBALException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $params
     * @return array|bool|string
     */
    public function importarPessoaEntidade(array $params)
    {
        try {
            //set params
            if ($params['id_usuario']) {
                $idUsuario = $params['id_usuario'];
            }

            if ($params['id_entidade']) {
                if ($params['id_entidade'] == $this->sessao->id_entidade) {
                    return array(
                        'type' => 'warning',
                        'title' => 'Atenção!',
                        'text' => 'Usuário já esta vinculado a esta entidade!'
                    );
                } else {
                    $idEntidadeOrigem = $params['id_entidade'];
                }
            }

            $idEntidadeDestino = $this->sessao->id_entidade;
            $idUsuarioCadastro = $this->sessao->id_usuario;

            $result = $this->_pessoaBo
                ->importarUsuarioEntidade($idUsuario, $idEntidadeOrigem, $idEntidadeDestino, $idUsuarioCadastro);

            if ($result) {
                return array(
                    'type' => 'success',
                    'title' => 'Sucesso!',
                    'text' => 'Importação realizada com sucesso!'
                );
            } else {
                return false;
            }
        } catch (\Zend_exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Executa exclusão logica de pessoa, dados de acesso e usuario
     * @param integer $id
     * @return \Ead_Mensageiro
     */
    public function exclusaoLogicaDadosUsuario($id)
    {
        $mensageiro = new \Ead1_Mensageiro();
        if ($id) {
            $pessoa = $this->deletePessoa($id);
            $dadosAcesso = $this->deleteDadosAcesso($id);
            $usuario = $this->deleteUsuario($id);
            if ($pessoa && $dadosAcesso && $usuario) {
                $mensageiro->setMensageiro('Sucesso!', \Ead1_IMensageiro::SUCESSO);
                if ($pessoa) {
                    $mensageiro->addMensagem($pessoa);
                }
                if ($dadosAcesso) {
                    $mensageiro->addMensagem($dadosAcesso);
                }
                if ($usuario) {
                    $mensageiro->addMensagem($usuario);
                }
            }
        }
        return $mensageiro;
    }

    /**
     * Deleção lógica de Pessoa
     * @param integer $id
     * @return \G2\Entity\Pessoa
     */
    public function deletePessoa($id)
    {
        try {
            $pessoa = $this->find($this->repositoryName, $id);
            $pessoa->setBl_ativo(false);
            return $this->save($pessoa);
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Deleção lógica de Dados de Acesso
     * @param integer $id
     * @return '\G2\Entity\DadosAcesso'
     */
    public function deleteDadosAcesso($id)
    {
        try {
            $pessoa = $this->find('\G2\Entity\DadosAcesso', $id);
            $pessoa->setBl_ativo(false);
            return $this->save($pessoa);
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Deleção lógica de Usuario
     * @param integer $id
     * @return \G2\Entity\Usuario
     */
    public function deleteUsuario($id)
    {
        try {
            $pessoa = $this->find('\G2\Entity\Usuario', $id);
            $pessoa->setBl_ativo(false);
            return $this->save($pessoa);
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Envia dados de acesso do usuario
     * @param $idPessoa
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function enviaDadosAcesso($idPessoa)
    {
        try {
            $pessoa = $this->findOneBy($this->repositoryName, array('id_usuario' => $idPessoa, 'id_entidade' => $this->sessao->id_entidade));
            if (!$pessoa) {
                throw new \Exception("Usuário não encontrado.", 404);
            }
            $pessoaTo = new \PessoaTO();
            $pessoaTo->montaToDinamico(array(
                'id_usuario' => $pessoa->getId_usuario(),
                'id_entidade' => $pessoa->getId_entidade() ? $pessoa->getId_entidade()->getId_entidade() : NULL,
                'st_sexo' => $pessoa->getSt_sexo(),
                'st_nomeexibicao' => $pessoa->getSt_nomeexibicao(),
                'dt_nascimento' => $pessoa->getDt_nascimento() ? $pessoa->getDt_nascimento()->format('Y-m-d H:i:s') : NULL,
                'st_nomepai' => $pessoa->getSt_nomepai(),
                'st_nomemae' => $pessoa->getSt_nomemae(),
                'id_pais' => $pessoa->getId_pais() ? $pessoa->getId_pais()->getId_pais() : NULL,
                'sg_uf' => $pessoa->getSg_uf(),
                'id_municipio' => $pessoa->getId_municipio() ? $pessoa->getId_municipio()->getId_municipio() : NULL,
                'dt_cadastro' => $pessoa->getDt_cadastro(),
                'id_usuariocadastro' => $pessoa->getId_usuariocadastro(),
                'st_passaporte' => $pessoa->getSt_passaporte(),
                'id_situacao' => $pessoa->getId_situacao() ? $pessoa->getId_situacao()->getId_situacao() : NULL,
                'bl_ativo' => $pessoa->getBl_ativo(),
                'id_estadocivil' => $pessoa->getId_estadocivil() ? $pessoa->getId_estadocivil()->getId_estadocivil() : NULL,
                'st_urlavatar' => $pessoa->getSt_urlavatar(),
                'st_cidade' => $pessoa->getSt_cidade(),
            ));
            $mensagemBo = new \MensagemBO();
            return $mensagemBo->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::DADOS_ACESSO, $pessoaTo, \TipoEnvioTO::EMAIL, true);

        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Retorna Dados de pessoa por id
     * @param integer | array $idPessoa pode ser passado o id_usuario como valor ou um array com
     * @param boolean $toArray
     * @return array|null|object /G2/Entity/Pessoa or array
     * @throws \Exception
     */
    public function retornaPessoaById($idPessoa, $toArray = false)
    {
        if (is_array($idPessoa) && $idPessoa) {
            if (!array_key_exists('id_usuario', $idPessoa) || empty($idPessoa['id_usuario'])) {
                throw new \Exception('Id da pessoa não informado');
            }

            $pessoa = $this->findOneBy($this->repositoryName, $idPessoa);
        } else {
            $pessoa = $this->find($this->repositoryName, $idPessoa);
        }
        if ($pessoa) {
            if ($toArray) {
                $pessoa = array(
                    'id_usuario' => $pessoa->getId_usuario(),
                    'id_entidade' => $pessoa->getId_entidade() ? $pessoa->getId_entidade()->getId_entidade() : NULL,
                    'st_sexo' => $pessoa->getSt_sexo(),
                    'st_nomeexibicao' => $pessoa->getSt_nomeexibicao(),
                    'dt_nascimento' => $pessoa->getDt_nascimento() ? $pessoa->getDt_nascimento()->format('Y-m-d H:i:s') : NULL,
                    'st_nomepai' => $pessoa->getSt_nomepai(),
                    'st_nomemae' => $pessoa->getSt_nomemae(),
                    'id_pais' => $pessoa->getId_pais() ? $pessoa->getId_pais()->getId_pais() : NULL,
                    'sg_uf' => $pessoa->getSg_uf(),
                    'id_municipio' => $pessoa->getId_municipio() ? $pessoa->getId_municipio()->getId_municipio() : NULL,
                    'dt_cadastro' => $pessoa->getDt_cadastro(),
                    'id_usuariocadastro' => $pessoa->getId_usuariocadastro(),
                    'st_passaporte' => $pessoa->getSt_passaporte(),
                    'id_situacao' => $pessoa->getId_situacao() ? $pessoa->getId_situacao()->getId_situacao() : NULL,
                    'bl_ativo' => $pessoa->getBl_ativo(),
                    'id_estadocivil' => $pessoa->getId_estadocivil() ? $pessoa->getId_estadocivil()->getId_estadocivil() : NULL,
                    'st_urlavatar' => $pessoa->getSt_urlavatar(),
                    'st_cidade' => $pessoa->getSt_cidade(),
                );
            }
        }
        return $pessoa;
    }

    /**
     *   * Retorna dados de Vw Usuario Perfil Entidade Referencia
     * @param array $where
     * @return \G2\Entity\VwUsuarioPerfilEntidadeReferencia
     */
    public function retornaVwUsuarioPerfilEntidadeReferencia(array $where = array())
    {
        return $this->findBy('\G2\Entity\VwUsuarioPerfilEntidadeReferencia', $where);
    }

    /**
     * Retorna Dados de Usuario pelo CPF
     * @param string $cpf
     * @return \G2\Entity\Usuario
     */
    public function retornaUsuarioByCpf($cpf)
    {
        return $this->findBy('\G2\Entity\Usuario', array('st_cpf' => $cpf));
    }

    /**
     * Retorna perfis da pessoa na entidade
     * @param array $params
     * @return array
     */
    public function retornaVwPerfilPessoa(array $params)
    {
        try {
            $params['nu_entidadepai'] = $this->sessao->id_entidade;

            $result = $this->findBy('G2\Entity\VwListarPerfilUsuario', $params, array('st_nomeentidade' => 'ASC'));
            $return = '';
            foreach ($result as $key => $vw) {
                $dt_cadastro = new \DateTime($vw->getDt_cadastro());
                $return[$key]['id_usuario'] = $vw->getId_usuario();
                $return[$key]['dt_termino'] = $vw->getDt_termino() instanceof \DateTime ? $vw->getDt_termino()->format("d/m/Y") : $vw->getDt_termino();
                $return[$key]['dt_inicio'] = $vw->getDt_inicio() instanceof \DateTime ? $vw->getDt_inicio()->format("d/m/Y") : $vw->getDt_inicio();
                $return[$key]['dt_cadastro'] = $dt_cadastro->format("d/m/Y");
                $return[$key]['id_situacao'] = $vw->getId_situacao();
                $return[$key]['id_perfil'] = $vw->getId_perfil();
                $return[$key]['st_nomeperfil'] = $vw->getSt_nomeperfil();
                $return[$key]['st_nomeentidade'] = $vw->getSt_nomeentidade();
                $return[$key]['id_entidade'] = $vw->getId_entidade();
                $return[$key]['st_situacaoperfil'] = $vw->getSt_situacaoperfil();
//                $return[$key]['st_responsavel'] = ' - ';
//                $return[$key]['st_sistema'] = ' - ';
            }

            return $return;
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna Dados de UsuarioPerfilEntidadeReferencia
     * @param array $where
     * @return \G2\Entity\UsuarioPerfilEntidadeReferencia
     */
    public function retornarUsuarioPerfilEntidadeReferencia(array $where = array())
    {
        return $this->findBy('\G2\Entity\UsuarioPerfilEntidadeReferencia', $where);
    }

    /**
     * Retorna as disciplinas de um colaborador
     * @param $idUsuario
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaDisciplinasColaboradores($idUsuario)
    {
        try {
            $result = $this->em->getRepository("\G2\Entity\UsuarioPerfilEntidadeReferencia")
                ->retornaDisciplinas(array('id_usuario' => $idUsuario, 'id_entidade' => $this->sessao->id_entidade));
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar disciplinas do colaborador " . $e->getMessage());
        }
    }

    /**
     * Metodo responsavel por retornar os dados de uma pessoa, da vw_pesquisarpessoa
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param type $id
     * @return \G2\Entity\VwPesquisarPessoa
     */
    public function retornaVwPesquisarPessoaById($id)
    {
        return $this->findOneBy('\G2\Entity\VwPesquisarPessoa', array(
            'id_usuario' => $id,
            'id_entidade' => $this->getId_entidade()
        ));
    }

    /**
     * /**
     * Metodo responsavel por retornar os dados de uma pessoa, da vw_pessoa
     * Caso tenha necessidade de retornar os dados em array basta enviar o param true,
     * além disso ele vai retonar os objetos convertidos para facili
     *
     * @author Kayo
     * @update Neemias Santos
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|null|object
     * @throws \Zend_Exception
     */
    public function retornaVwPessoaOne($dados, $array = false)
    {
        $result = $this->findOneBy('\G2\Entity\VwPessoa', $dados);
        if ($result) {
            if ($array) {
                $result = $this->toArrayEntity($result);

                $result['st_urlavatar'] = ($result['st_urlavatar'] != '' ? \Ead1_Ambiente::geral()->st_url_gestor2 . $result['st_urlavatar'] : '');
                $result['st_sexo'] = ($result['st_sexo'] == 'M' ? 'Masculino' : 'Feminino');
                $result['dt_dataexpedicao'] = (isset($result['dt_dataexpedicao'])) ? $this->converterData(new \Zend_Date($result['dt_dataexpedicao']), 'd/m/Y') : '';

                return $result;
            } else {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Salva dados de usuário integração
     * @param array $data
     * @return \G2\Entity\UsuarioIntegracao
     * @throws \Zend_Exception
     */
    public function salvaUsuarioIntegracao(array $data)
    {
        try {
            $entity = new \G2\Entity\UsuarioIntegracao();

            $result = $this->findUsuarioIntegracao($data);
            if ($result) {
                $entity = $result;
            }
            $entity->setDt_cadastro(isset($data['dt_cadastro']) ? $data['dt_cadastro'] : new \DateTime())
                ->setId_sistema(isset($data['id_sistema']) ? $data['id_sistema'] : NULL)
                ->setId_usuariocadastro(isset($data['id_usuariocadastro']) ? $data['id_usuariocadastro'] : NULL)
                ->setId_usuario(isset($data['id_usuario']) ? $data['id_usuario'] : NULL)
                ->setId_entidade(isset($data['id_entidade']) ? $data['id_entidade'] : $this->sessao->id_entidade)
                ->setSt_codusuario(isset($data['st_codusuario']) ? $data['st_codusuario'] : NULL)
                ->setSt_senhaintegrada(isset($data['st_senhaintegrada']) ? $data['st_senhaintegrada'] : NULL)
                ->setSt_loginintegrado(isset($data['st_loginintegrado']) ? $data['st_loginintegrado'] : NULL);
            return $this->save($entity);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados de Usuário Integração " . $e->getMessage());
        }
    }

    /**
     * Retorna dados de usuario integração
     * @param array $where
     * @return mixed G2\Entity\UsuarioIntegracao | NULL
     * @throws \Zend_Exception
     */
    public function findUsuarioIntegracao(array $where)
    {
        try {
            $arrWhere = array();
            foreach ($where as $key => $value) {
                if ($value)
                    $arrWhere[$key] = $value;
            }
            unset($arrWhere['dt_cadastro'], $arrWhere['st_senhaintegrada'], $arrWhere['st_codusuario']);
            $result = $this->findOneBy('\G2\Entity\UsuarioIntegracao', $arrWhere);
            return $result;
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao consultar dados de Usuário Integração " . $ex->getMessage());
        }
    }

    /**
     * Retorna dados de Documento Identidade
     * @param array $where
     * @return mixed
     */
    public function retornarDadosIdentidade(array $where = array())
    {
        $where['id_entidade'] = array_key_exists('id_entidade', $where) ? $where['id_entidade'] : $this->sessao->id_entidade;
        return $this->findBy('\G2\Entity\DocumentoIdentidade', $where);
    }

    /**
     * Retorna dados de InformacaoAcademicaPessoa
     * @param array $where
     * @return mixed
     */
    public function retornarInformacaoAcademicaPessoa(array $where = array())
    {
        $where['id_entidade'] = array_key_exists('id_entidade', $where) ? $where['id_entidade'] : $this->sessao->id_entidade;
        return $this->findBy('\G2\Entity\InformacaoAcademicaPessoa', $where);
    }

    /**
     * Método para retornar dados para gerar carteirinha
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarDadosCarteirinha(array $params)
    {
        try {
            $arrReturn = array();
            if ($params) {
                // Buscar dados da pessoa
                /** @var \G2\Entity\VwPessoa $pessoa */
                $pessoa = $this->findOneBy('\G2\Entity\VwPessoa', array(
                    'id_usuario' => $params['id_usuario'],
                    'id_entidade' => $params['id_entidade']
                ));

                // Verificar o resultado
                if ($pessoa instanceof \G2\Entity\VwPessoa) {
                    $arrReturn['id_usuario'] = $pessoa->getId_usuario();
                    $arrReturn['st_nomecompleto'] = $pessoa->getst_nomecompleto();
                    $arrReturn['st_cpf'] = $this->_pessoaBo->setaMascaraCPF($pessoa->getst_cpf());
                    $arrReturn['dt_nascimento'] = $pessoa->getdt_nascimento() ? $pessoa->getdt_nascimento()->format('d/m/Y') : null;
                    $arrReturn['st_identificacao'] = $pessoa->getSt_identificacao();
                    $arrReturn['st_urlavatar'] = $pessoa->getSt_urlavatar();
                    $arrReturn['st_sexo'] = $pessoa->getSt_sexo();
                    $arrReturn['id_entidade'] = $pessoa->getId_entidade();
                }

                // Buscar a turma
                /** @var \G2\Entity\Turma $turma */
                $turma = $this->find('\G2\Entity\Turma', $params['id_turma']);
                if ($turma instanceof \G2\Entity\Turma) {
                    $arrReturn['st_turma'] = $turma->getSt_turma();
                }

                // Buscar a entidade
                /** @var \G2\Entity\Entidade $entidade */
                $entidade = $this->find('\G2\Entity\Entidade', $params['id_entidade']);
                if ($entidade instanceof \G2\Entity\Entidade) {
                    $arrReturn['st_entidade'] = $entidade->getSt_siglaentidade();
                }

                // Buscar modelo da carteirinha
                /** @var \G2\Entity\ModeloCarteirinha $modelo */
                $modelo = $this->find('\G2\Entity\ModeloCarteirinha', $params['id_modelocarteirinha']);
                if ($modelo instanceof \G2\Entity\ModeloCarteirinha) {
                    $arrReturn['id_modelocarteirinha'] = $modelo->getId_modelocarteirinha();
                    $arrReturn['st_modelocarteirinha'] = $modelo->getSt_modelocarteirinha();
                }

                // Buscar Matrícula
                /** @var \G2\Entity\VwMatricula $matricula */
                $matricula = $this->find('\G2\Entity\VwMatricula', $params['id_matricula']);

                if ($matricula instanceof \G2\Entity\VwMatricula) {
                    $dt_termino = $matricula->getDt_terminomatricula();
                    if ($dt_termino instanceof \DateTime) {
                        $dt_termino = $dt_termino->format('Y-m-d');
                    }
                    $arrReturn['dt_termino'] = $dt_termino;

                    // Se for graduação
                    if ($entidade->getId_esquemaconfiguracao()->getid_esquemaconfiguracao() == \G2\Constante\EsquemaConfiguracao::Graduacao_AVM) {
                        // Buscar os dados da venda
                        $venda = $this->find('\G2\Entity\Venda', $matricula->getId_venda());

                        // A data de referência para a validade da carteirinha será a data de confirmação da venda
                        $dt_referencia = $venda->getDt_confirmacao();
                    } else {
                        // A data de referência para a validade da carteirinha será a data início da matrícula
                        $dt_referencia = $matricula->getDt_inicio();
                    }

                    if ($dt_referencia) {
                        $dt_referencia = new \DateTime($this->converterData($dt_referencia, 'Y-m-d'));
                        // Somar 1 ano à data de referência
                        $arrReturn['dt_validade'] = $dt_referencia->add(new \DateInterval("P1Y"))->format('m/Y');
                    }

                    $arrReturn['st_projetopedagogico'] = $matricula->getSt_projetopedagogico();
                    $arrReturn['st_tituloexibicao'] = $matricula->getSt_tituloexibicao();
                    $arrReturn['id_matricula'] = $matricula->getId_matricula();
                }
            }

            //retorna
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da carteirinha do aluno. " . $e->getMessage());
        }
    }

    /**
     * Salva vários códigos de identificação
     * @param array $data array com os dados do aluno,
     * deve ser um chave valor ex. array([0]=>array('id_usuario'=>1,'st_identificacao'=>1231231),...,n)
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarArrayIdentificacao(array $data)
    {
        try {
            if ($data) {
                $pessoaDAO = new \PessoaDAO();

                $pessoaDAO->beginTransaction();
                foreach ($data as $usuario) {
                    $verifica = $this->findOneBy('\G2\Entity\Pessoa', array('st_identificacao' => $usuario['st_identificacao']));
                    if (isset($usuario) && !empty($usuario) && $verifica == null) {
                        $res = $this->editarPessoa($usuario);
                        if (!is_array($res)) {
                            throw new \Exception("Erro ao tentar salvar código de identificação do aluno. ");
                        }
                    } else {
                        return new \Ead1_Mensageiro("O código de identificação " . $usuario['st_identificacao'] . " já foi cadastrado, favor crie outro .", \Ead1_IMensageiro::ERRO);
                    }
                }
                $pessoaDAO->commit();


                return new \Ead1_Mensageiro("Códigos de Identificação salvos com sucesso.", \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro("Nenhum dado informado para ser salvo.", \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $pessoaDAO->rollBack();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Edita dados de pessoa
     * @param array $data
     * @throws \Zend_Exception
     * @return array
     */
    public function editarPessoa(array $data)
    {
        try {
            //instancia a to
            $uTO = new \UsuarioTO();
            $uTO->setId_usuario($data['id_usuario']);

            //instancia a pessoa to
            $pTO = new \PessoaTO();
            $pTO->setId_usuario($uTO->getId_usuario());
            $pTO->montaToDinamico($data);
            //chama o metodo da bo
            $result = $this->_pessoaBo->editarPessoa($uTO, $pTO);

            //pega o resultado
            if ($result->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $pTO->setId_entidade($this->sessao->id_entidade);
                $pTO->fetch(false, true, true);
                return $pTO->toArray();
            } else {
                throw new \Exception($result->getText());
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao editar pessoa." . $e->getMessage());
        }
    }

    /**
     * Salva dados de cancelamento
     * @author: Helder Silva <helder.silva@unyleya.com.br>
     */
    public function salvaCancelamento($params)
    {
        try {
            $this->beginTransaction();

            $objeto = $this->findOneBy('\G2\Entity\EntidadeFinanceiro', array('id_entidade' => $params['id_entidade']));
            $objeto->setNu_multacomcarta($params['nu_multacomcarta']);
            $objeto->setNu_multasemcarta($params['nu_multasemcarta']);
            $result = $this->save($objeto);

            $this->commit();

            return $result;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function retornaCancelamento($params)
    {
        return $this->findOneBy('\G2\Entity\EntidadeFinanceiro', $params);
    }

    public function gerarCss()
    {
        $params['id_entidade'] = $_GET["id_entidade"];
        $params['rgbPrimario'] = $_GET["rgbPrimario"];
        $params['rgbSecundario'] = $_GET["rgbSecundario"];
        $params['rgbTerciario'] = $_GET["rgbTerciario"];

        $texto = "
            .preenchimento {
              color: #FFFFFF;
              font-weight: bold;
              padding: 10px 0px 10px;
              background: #" . $params['rgbPrimario'] . " !important;
              border-bottom: 4px solid #" . $params['rgbTerciario'] . ";
            }

            /* line 83, ../sass/styles.scss */
            .preenchimento-menor {
              color: #FFFFFF;
              font-weight: bold;
              padding: 10px 0px 10px;
              background: #13A096;
              border-bottom: 4px solid #" . $params['rgbTerciario'] . ";
              font-size: 9px;
            }

            /* line 92, ../sass/styles.scss */
            .preenchimento-active {
              background: #" . $params['rgbSecundario'] . ";
              color: #FFFFFF;
              font-weight: bold;
              padding: 10px 0px 10px;
              border-bottom: 4px solid #" . $params['rgbTerciario'] . ";
            }

            .barra-meio {
              border-left: 1px solid #" . $params['rgbTerciario'] . ";
              border-right: 1px solid #" . $params['rgbTerciario'] . ";
            }

            /* line 60, ../sass/styles.scss */
            .btnpadrao {
            font-size: 12px;
             padding: 7px 15px 5px !important;
}
        ";

        $uploadPathLojaCss = realpath(APPLICATION_PATH . "/../public/loja/assets/stylesheets/");
        $arquivo = $uploadPathLojaCss . '/' . $params['id_entidade'] . '.css';
        $handle = fopen($arquivo, 'w+');
        $ler = fwrite($handle, $texto);
        fclose($handle);
        if (file_exists($arquivo)) {
            //se existir a configuracao, busca
            $objetoEntidadeConfiguracao = $this->findOneBy('\G2\Entity\ConfiguracaoEntidade', array('id_entidade' => $params['id_entidade']));
            $x = $params['rgbPrimario'];
            $y = $params['rgbSecundario'];
            $w = $params['rgbTerciario'];
            if (!$objetoEntidadeConfiguracao) {
                //se não existir o id cria uma nova configuracao
                throw new \Zend_Exception("Erro ao salvar css. Entidade não tem configuração existente.");
            }
            $objetoEntidadeConfiguracao->setId_entidade($params['id_entidade']);
            $objetoEntidadeConfiguracao->setSt_corprimarialoja($x);
            $objetoEntidadeConfiguracao->setSt_corsecundarialoja($y);
            $objetoEntidadeConfiguracao->setSt_corterciarialoja($w);
            return $this->save($objetoEntidadeConfiguracao);
        } else {
            throw new \Zend_Exception("Erro ao salvar o css.");
        }
    }

    public function salvarPessoaLojaContrato($params)
    {
        try {
            /** @var \G2\Entity\Pessoa $objPessoa */
            $objPessoa = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $params['id_usuario']));
            if (!($objPessoa instanceof \G2\Entity\Pessoa)) {
                throw new \Exception("Dados da pessoa não encontrado.");
            }
            $objPessoa->setSt_sexo($params['st_sexo']);
            $objPessoa->setDt_nascimento($this->converteDataBanco($params['dt_nascimento']));
            return $this->save($objPessoa);
        } catch (\Exception $e) {
            throw new \Exception('Erro ao salvar usuario ao cadastrar contrato na loja. ' . $e->getMessage());
        }
    }

    /**
     * Verifica se o usuario é maior de idade
     * @param \Zend_Date $dt_nascimento
     * @return boolean
     * @throws \Zend_Excpetion
     */
    private function verificaMaiorIdade(\Zend_Date $dt_nascimento)
    {
        try {
            if ($this->_pessoaBo->retornaIdade($dt_nascimento, new \Zend_Date()) < 18) {
                return false;
            }
            return true;
        } catch (\Exception $ex) {
            throw new \Zend_Excpetion("Erro - " . $ex->getMessage());
        }
    }

    /**
     *  Metodo responsavel por retornar os dados do perfil de uma pessoa, da VwUsuarioPerfilEntidade.
     * Caso tenha necessidade de retornar os dados em array basta enviar o param true,
     * além disso ele vai retonar os objetos convertidos para facilitar
     *
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|null|object
     * @throws \Zend_Exception
     */
    public function retornaVwUsuarioPerfilEntidade($dados, $array = false)
    {
        $result = $this->findOneBy('\G2\Entity\VwUsuarioPerfilEntidade', $dados);
        if ($result) {
            if ($array) {
                $result = $this->toArrayEntity($result);
                return $result;
            } else {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
        }
    }


    /**
     * @param \G2\Entity\ContatosTelefone $ctTO
     * @param \G2\Entity\ContatosTelefonePessoa $ctpTO
     * @return \Ead1_Mensageiro
     */
    public function salvaContatoTelefonePessoa($ctTO, $ctpTO)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            $this->beginTransaction();
            $ctTO = $this->save($ctTO);
            if ($ctTO instanceof \G2\Entity\ContatosTelefone && $ctTO->getId_telefone()) {
                $entityAlter = $this->findOneBy('\G2\Entity\ContatosTelefonePessoa', array('id_telefone' => $ctTO->getId_telefone()));
                if (!$entityAlter instanceof \G2\Entity\ContatosTelefonePessoa) {
                    $entityAlter = new \G2\Entity\ContatosTelefonePessoa();
                    $entityAlter->setId_entidade($ctpTO->getId_entidade());
                    $entityAlter->setId_usuario($ctpTO->getId_usuario());
                    $entityAlter->setId_telefone($ctTO->getId_telefone());
                }
                $entityAlter->setBl_padrao($ctpTO->getBl_padrao());
                if (!$this->save($entityAlter)) {
                    throw new \Exception('Erro ao salvar');
                }
            }
            $this->commit();
            $mensageiro->setMensageiro($ctpTO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Telefone salvo com sucesso');
        } catch (\Exception $e) {
            $this->rollback();
            $mensageiro->setMensageiro('Erro ao salvar telefone: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Salva o Avatar o Usuário
     * @param $avatar - arquivo
     * @param $id - id_usuario
     * @return array
     * @throws Exception
     * @throws \Exception
     */
    public function uploadAvatar($avatar, $id, $url_imagem = null)
    {
        try {

            $uploadPath = realpath(APPLICATION_PATH . "/../public");
            $hash = uniqid($id);
            if (is_array($avatar)) {
                $type = explode('/', $avatar['type']);
                $fileName = 'usuario_foto_' . $hash . '.' . $type[1];

                //renomeando o nome do arquivo para que a extensão fique realmente a extensão
                //do arquivo
                $avatar['name'] = $fileName;

                if (!empty($url_imagem) && file_exists($uploadPath . $url_imagem)) {
                    unlink($uploadPath . $url_imagem);
                }

                if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $avatar["type"])) {
                    $arrReturn = array(
                        "type" => 'warning',
                        'title' => 'Arquivo inválido!',
                        'text' => "Isso não é uma imagem."
                    );
                    return $arrReturn;
                }
                $bo = new \Ead1_BO();
                $arquivoTO = new \ArquivoTO();
                $arquivoTO->setSt_caminhodiretorio('/usuario');
                $arquivoTO->setAr_arquivo($avatar);
                $arquivoTO->setSt_nomearquivo('usuario_foto_' . $hash);
                $arquivoTO->setSt_extensaoarquivo(substr($avatar['name'], (strrpos($avatar['name'], '.') + 1)));
                $img = $bo->uploadBasico($arquivoTO);
                $arrReturn = array(
                    "type" => $img->getType(),
                    'title' => $img->getTitle(),
                    'text' => $img->getText(),
                    'file' => $fileName
                );
            } else if ($avatar) {
                $image = base64_decode(str_replace('data:image/png;base64,', '', $avatar)); // decode string with base64 and replace desnecessary header to write to file
                $fileName = 'usuario_foto_' . $hash . '.png';

                if (!empty($url_imagem) && file_exists($uploadPath . $url_imagem)) {
                    unlink($uploadPath . $url_imagem);
                }

                if (file_put_contents($uploadPath . '/' . $fileName, $image)) { //save picture to disk
                    $arrReturn = array(
                        "type" => 'success',
                        'title' => 'Sucesso',
                        'text' => 'Arquivo Salvo com sucesso!',
                        'file' => $fileName
                    );
                } else {
                    $arrReturn = array(
                        "type" => 'error',
                        'title' => 'Erro',
                        'text' => 'Erro ao tentar salvar imagem capturada!',
                        'file' => $fileName
                    );
                }
            }


            /**
             * Tenta atualizar o Avatar do Usuário no Moodle
             * Caso não consiga, deixa o processo continuar
             */
            $ui = new \UsuarioIntegracaoTO();
            $ui->setId_usuario($id);
            $ui->setId_sistema(\G2\Constante\Sistema::MOODLE);
            $uis = $ui->fetch(false, false, false);

            try {
                if (file_exists($uploadPath . '/' . $fileName) && $uis) {
                    foreach ($uis as $ui) {
                        if ($ui instanceof \UsuarioIntegracaoTO) {
                            $integracao = new \MoodleUnyleyaWebServices($ui->getId_entidade());
                            $integracao->uploadUserProfileImage($ui->getId_usuario(), $ui->getId_entidade(), $uploadPath . '/' . $fileName);
                        }
                    }
                }
            } catch (\Exception $e) {
            }

            return $arrReturn;
        } catch (Exception $e) {
            \Zend_Debug::dump($e, __FILE__ . ' - ' . __line__);
            exit;
            throw $e;
        }
    }

    /**
     * Executa a Sincronização
     * @param array $params
     * @return array|string
     */
    public function processaSincronizacao(Array $params)
    {
        $atualizacaoOk = array();

        /**
         * Verifica se existe algum id_sistema para atualizar.
         * Caso não exista, vai consultar os usuários que devem ser atualizados em algum sistema.
         */

        if (array_key_exists('id_sistema', $params) && $params['id_sistema']) {

            foreach ($params['id_sistema'] as $sistema) {

                if ($sistema == \G2\Constante\Sistema::ACTOR) {
                    $result = self::sincronizacaoG1($params);
                    array_push($atualizacaoOk, $result);
                }

                if ($sistema == \G2\Constante\Sistema::FLUXUS) {
                    $result = self::sincronizacaoFluxus($params);
                    array_push($atualizacaoOk, $result);
                }

            }

            return $atualizacaoOk;

        } else {

            //Retorna as pessoas que precisam ser sincronizadas baseadas nos parâmetros passados.

            $objSincronizar = $this->em->getRepository('\G2\Entity\Pessoa')->returnPessoasSync(array('bl_ativo' => true), array('bl_sincronizarg1' => true, 'bl_sincronizarfluxus' => true));
            if ($objSincronizar) {
                if (array_key_exists('mensagem', $objSincronizar)) {
                    return $objSincronizar['mensagem'];
                } else {
                    foreach ($objSincronizar as $item) {

                        if ($item->getBl_sincronizarg1()) {
                            $result = self::sincronizacaoG1(array(
                                'id_usuario' => $item->getId_usuario(),
                                'id_entidade' => $item->getId_entidade(),
                                'id_sistema' => array(\G2\Constante\Sistema::ACTOR)
                            ));
                            array_push($atualizacaoOk, $result);
                        }

                        if ($item->getBl_sincronizarfluxus()) {
                            $result = self::sincronizacaoFluxus(array(
                                'id_usuario' => $item->getId_usuario(),
                                'id_entidade' => $item->getId_entidade(),
                                'id_sistema' => array(\G2\Constante\Sistema::FLUXUS)
                            ));
                            array_push($atualizacaoOk, $result);
                        }

                    }
                    return $atualizacaoOk;
                }
            } else {
                return 'Nenhuma pessoa para sincronizar.';
            }

        }
    }

    /**
     * @author: helder Silva <helder.silva@unyleya.com.br>
     * Método para sincronizar pessoas do G2 no G1
     * @param $params
     * @return array
     */
    public function sincronizacaoG1($params)
    {

        $entidadeIntegracao = $this->findOneBy('\G2\Entity\EntidadeIntegracao', array('id_entidade' => $params['id_entidade'], 'id_sistema' => \G2\Constante\Sistema::ACTOR));

        if ($entidadeIntegracao) {
            $ws = new \actor_Pessoa($entidadeIntegracao->getSt_codsistema(), $entidadeIntegracao->getSt_codchave(), $entidadeIntegracao->getSt_caminho());
            $usuarioG1 = $ws->retornarPessoaSincronizacaoG1($params['id_usuario']);
            if ($usuarioG1['pessoa']) {

                $pessoa = $this->findOneBy('\G2\Entity\VwPessoa', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));

                if ($pessoa) {

                    $correspondencia = $this->findOneBy('\G2\Entity\VwPessoaEndereco', array(
                        'id_usuario' => $params['id_usuario'],
                        'id_entidade' => $params['id_entidade'],
                        'id_tipoendereco' => \G2\Constante\TipoEndereco::Correspondência), array('id_endereco' => 'desc'));

                    $telefoneCelular = $this->findOneBy('\G2\Entity\VwPessoaTelefone', array(
                        'id_usuario' => $params['id_usuario'],
                        'id_entidade' => $params['id_entidade'],
                        'id_tipotelefone' => \G2\Constante\TipoTelefone::Celular));

                    $telefoneResidencial = $this->findOneBy('\G2\Entity\VwPessoaTelefone', array(
                        'id_usuario' => $params['id_usuario'],
                        'id_entidade' => $params['id_entidade'],
                        'id_tipotelefone' => \G2\Constante\TipoTelefone::Residencial));

                    $telefoneComercial = $this->findOneBy('\G2\Entity\VwPessoaTelefone', array(
                        'id_usuario' => $params['id_usuario'],
                        'id_entidade' => $params['id_entidade'],
                        'id_tipotelefone' => \G2\Constante\TipoTelefone::Comercial));

                    $dados = array(
                        'codpessoa' => $usuarioG1['pessoa']['codpessoa'],
                        'nompessoa' => $pessoa->getSt_nomecompleto(),
                        'numcpf' => $pessoa->getSt_cpf(),
                        'nommae' => $pessoa->getSt_nomemae(),
                        'nompai' => $pessoa->getSt_nomepai(),
                        'numidentidade' => $pessoa->getSt_rg(),
                        'datnascimento' => $pessoa->getDt_nascimento() ? $this->converterData($pessoa->getDt_nascimento()) : null,
                        'flasexo' => $pessoa->getSt_sexo(),
                        'idepais' => $pessoa->getId_paisnascimento(),

                        'idemunicipionaturalidade' => $pessoa->getId_municipionascimento(),
                        'nomcidadenaturalidade' => $pessoa->getSt_municipionascimento(),
                        'codufnaturalidade' => $pessoa->getSg_ufnascimento(),

                        'idemunicipioendereco' => $pessoa->getId_municipio(),
                        'desendereco' => $pessoa->getSt_endereco(),
                        'numendereco' => $pessoa->getNu_numero(),
                        'nombairro' => $pessoa->getSt_bairro(),
                        'descompendereco' => $pessoa->getSt_complemento(),
                        'nomcidade' => $pessoa->getSt_cidade(),
                        'numcep' => $pessoa->getSt_cep(),

                        'idemunicipiococorrespondencia' => $correspondencia->getId_municipio(),
                        'desenderecocorrespondencia' => $correspondencia->getSt_endereco(),
                        'numenderecococorrespondencia' => $correspondencia->getNu_numero(),
                        'descompenderecocorrespondencia' => $correspondencia->getSt_complemento(),
                        'nombairrococorrespondencia' => $correspondencia->getSt_bairro(),
                        'codufcocorrespondencia' => $correspondencia->getSg_uf(),
                        'numcepcorrespondencia' => $correspondencia->getSt_cep(),

                        'numdddtelefone' => $telefoneResidencial ? $telefoneResidencial->getNu_ddd() : null,
                        'numtelefone' => $telefoneResidencial ? $telefoneResidencial->getNu_telefone() : null,

                        'numdddtelefonecomercial' => $telefoneComercial ? $telefoneComercial->getNu_ddd() : null,
                        'numtelefonecomercial' => $telefoneComercial ? $telefoneComercial->getNu_telefone() : null,

                        'numdddcelular' => $telefoneCelular ? $telefoneCelular->getNu_ddd() : null,
                        'numcelular' => $telefoneCelular ? $telefoneCelular->getNu_telefone() : null,

                        'desemail' => $pessoa->getSt_email(),
                    );
                    $dados = array_filter($dados);

                    try {
                        $objPessoa = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));
                        $objPessoa->setBl_sincronizarG1(true);
                        $this->save($objPessoa);

                        $result = $ws->atualizar($dados);

                        if ($result['status'] == 1) {
                            $objPessoa = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));
                            $objPessoa->setBl_sincronizarG1(false);
                            $this->save($objPessoa);
                            return array('sistema' => 'G1', 'status' => 'OK', 'id_usuario' => $params['id_usuario']);
                        }
                    } catch (\Exception $e) {
                        $objPessoa = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));
                        $objPessoa->setBl_sincronizarG1(true);
                        $this->save($objPessoa);
                        return array('sistema' => 'G1', 'status' => 'FALHA', 'id_usuario' => $params['id_usuario'], 'ERRO' => $e->getMessage());
                    }
                }
            }
        } else {
            return 'Verifique a configuração da entidade na tabela tb_entidadeintegracao!';
        }
    }

    /**
     * Método para sincronizar G2 no fluxo.
     * @param $params
     * @author: helder Silva <helder.silva@unyleya.com.br>
     * @return array
     */
    public function sincronizacaoFluxus($params)
    {
        if (array_key_exists('id_usuario', $params) && array_key_exists('id_entidade', $params)
            && $params['id_usuario'] && $params['id_entidade']
        ) {

            $entidadeIntegracao = $this->findOneBy('\G2\Entity\EntidadeIntegracao', array('id_entidade' => $params['id_entidade'], 'id_sistema' => \G2\Constante\Sistema::FLUXUS));

            $pessoa = $this->findOneBy('\G2\Entity\VwPessoa', array('id_usuario' => $params['id_usuario'], 'id_entidade' => $params['id_entidade']));

            if ($entidadeIntegracao && $pessoa) {


                $telefoneResidencial = $this->findOneBy('\G2\Entity\VwPessoaTelefone', array(
                    'id_usuario' => $params['id_usuario'],
                    'id_entidade' => $params['id_entidade'],
                    'bl_padrao' => true), array('id_telefone' => 'DESC'));

                if ($telefoneResidencial) {
                    $telefoneResidencial = $telefoneResidencial->getNu_ddd() . $telefoneResidencial->getNu_telefone();
                } else {
                    $telefoneResidencial = null;
                }

                $pess = $this->findOneBy('\G2\Entity\Pessoa', array('id_usuario' => $pessoa->getId_usuario(),
                    'id_entidade' => $params['id_entidade']));

                $pais = $this->find('\G2\Entity\Pais', $pessoa->getId_pais());

                switch ($pais->getId_pais()):
                    CASE (22):
                        $id_pais = 1;
                        BREAK;
                    CASE (116):
                        $id_pais = 2;
                        BREAK;
                    CASE (94):
                        $id_pais = 3;
                        BREAK;
                    CASE (43):
                        $id_pais = 4;
                        BREAK;
                    CASE (49):
                        $id_pais = 5;
                        BREAK;
                    CASE (4):
                        $id_pais = 6;
                        BREAK;
                    CASE (76):
                        $id_pais = 7;
                        BREAK;
                    CASE (10):
                        $id_pais = 8;
                        BREAK;
                    CASE (44):
                        $id_pais = 9;
                        BREAK;
                    CASE (73):
                        $id_pais = 10;
                        BREAK;
                    DEFAULT:
                        $id_pais = 99;
                        BREAK;
                ENDSWITCH;

                $dados = array(
                    'CODCOLIGADA_FCFO' => $entidadeIntegracao->getSt_codsistema(),                //CODIGO DA COLIGADA
                    'NOMEFANTASIA_FCFO' => $pessoa->getSt_nomecompleto(),                         //NOME FANTASIA
                    'NOME_FCFO' => $pessoa->getSt_nomecompleto(),                                 //NOME COMPLETO
                    'CGCCFO_FCFO' => $pessoa->getSt_cpf(),// inserir masscara,                    //CPF
                    'INSCRESTADUAL_FCFO' => null,                                                 //INSCRIÇÃO ESTADUAL
                    'PAGREC_FCFO' => 1,                                                           //FORNECEDOR
                    'TIPORUA_FCFO' => 1,                                                          //TIPO DO ENDERECO
                    'RUA_FCFO' => $pessoa->getSt_endereco(),                                      //ENDERECO
                    'NUMERO_FCFO' => $pessoa->getNu_numero(),                                     //NUMERO DO ENDERECO
                    'COMPLEMENTO_FCFO' => $pessoa->getSt_complemento(),                           //COMPLEMENTO DO END.
                    'TIPOBAIRRO_FCFO' => 1,                                                       //TIPO DO BAIRRO
                    'BAIRRO_FCFO' => $pessoa->getSt_bairro(),                                     //NOME DO BAIRRO
                    'CIDADE_FCFO' => $pessoa->getSt_cidade(),                                     //NOME DA CIDADE
                    'CODETD_FCFO' => $pessoa->getSg_uf(),                                         //NOME DO ESTADO
                    'CEP_FCFO' => $this->limpaCpfCnpj($pessoa->getSt_cep()),                      //CEP
                    'CODMUNICIPIO_FCFO' => substr($pessoa->getId_municipio(), 2, 6),            //CODIGO DO MUNICIPIO
                    'TELEFONE_FCFO' => $telefoneResidencial,                                      //TELEFONE RESIDENCIAL
                    'EMAIL_FCFO' => $pessoa->getSt_email(),                                       //EMAIL
                    'CONTATO_FCFO' => NULL,                                                       //CONTATO NA EMPRESA.
                    'ATIVO_FCFO' => $pess->getBl_ativo(),
                    'PESSOAFISOUJUR_FCFO' => \G2\Constante\TipoPessoa::FISICA,                    //TIPO DA PESSOA
                    'IDPAIS_FCFO' => $id_pais,                                                    //PAIS
                    'PAIS_FCFO' => $pais->getSt_nomepais(),                                       //PAIS
                    'NACIONALIDADE_FCFO' =>
                        $pessoa->getId_paisnascimento() == \G2\Constante\Pais::BRASIL ? 1 : 0,    //PAIS DE NASCIMENTO
                    'CEI_FCFO' => NULL,                                                           //CEI
                    'TIPOCONTRIBUINTEINSS_FCFO' => 0                                              //TIPO DE CONTRIBUINTE

                );

                $objPessoa = $this->findOneBy('\G2\Entity\Pessoa',
                    array('id_usuario' => $params['id_usuario'],
                        'id_entidade' => $params['id_entidade'])
                );
                $objPessoa->setBl_sincronizarfluxus(true);
                $this->save($objPessoa);

                $sincronizacao = new \Ead1\Financeiro\Fluxus\Integracao();
                $result = $sincronizacao->sincronizaCadastroPessoa($dados);

                if ($result->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                    $objPessoa->setBl_sincronizarfluxus(false);
                    $this->save($objPessoa);

                    return array('
                    sistema' => 'FLUXUS',
                        'status' => 'OK',
                        'id_usuario' => $params['id_usuario']
                    );
                } else {
                    return array(
                        'sistema' => 'FLUXUS',
                        'status' => 'FALHA',
                        'id_usuario' => $params['id_usuario'],
                        'ERRO' => $result->getText()
                    );
                }


            } else {
                return array(
                    'sistema' => 'FLUXUS',
                    'status' => 'FALHA',
                    'id_usuario' => $params['id_usuario'],
                    'Não existe integração para a entidade ' . $params['id_entidade']);
            }

        } else {
            return array('sistema' => 'FLUXUS',
                'status' => 'FALHA',
                'id_usuario' => $params['id_usuario'],
                'Existem dados faltantes necessários para consulta da pessoa');
        }

    }

    /**
     * Esta função permite conferir se o usuário já está cadastrado na entidade.
     * @access public
     * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
     */
    public function checaSeUsuarioEstaIncluidoNaEntidade($id_usuario)
    {
        $return = $this->findOneBy('\G2\Entity\Pessoa', array(
            'id_usuario' => $id_usuario,
            'id_entidade' => $this->sessao->id_entidade
        ));

        return $return;
    }

    /**
     * Esta função permite inserir o usuário na tabela tb_pessoa com a entidade da sessão de onde
     * veio a solicitação de cadastro.
     * @access public
     * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
     * @author Kayo Silva <kayo.silva@unyleya.com.br> 2018-03-28
     * @param $id_usuario
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function incluirAlunoNaEntidade($id_usuario)
    {
        try {

            /** @var \G2\Entity\Pessoa $pessoa */
            $pessoa = $this->findOneBy(\G2\Entity\Pessoa::class, [
                "id_usuario" => $id_usuario
            ], ["dt_cadastro" => Criteria::DESC]);


            if (!$pessoa) {
                throw new \Exception("Dados de pessoa não encontrado.");
            }

            $result = $this->importarPessoaEntidade([
                "id_usuario" => $id_usuario,
                "id_entidade" => $pessoa->getId_entidade()->getId_entidade()
            ]);

            if (!is_array($result)) {
                throw new \Exception($result);
            }

            return new \Ead1_Mensageiro("Usuario integrado na entidade com sucesso", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao tentar integrar o usuário na entidade. '
                . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retornar a pessoa da entidade informada, caso não exista, ele clona a pessoa da entidade de origem informada
     * @param int $id_usuario
     * @param int $id_entidade
     * @param int $id_entidadeorigem
     * @return \G2\Entity\Pessoa
     * @throws \Zend_Exception
     */
    public function retornarPessoaClone($id_usuario, $id_entidade, $id_entidadeorigem = null)
    {
        $id_entidadeorigem = $id_entidadeorigem ?: $this->sessao->id_entidade;

        $pessoa = $this->findOneBy('\G2\Entity\Pessoa', array(
            'id_usuario' => $id_usuario,
            'id_entidade' => $id_entidade
        ));

        // Se não possuir cadastro de pessoa para a entidade informada, então realizar o clone da entidade de origem
        if (!($pessoa instanceof \G2\Entity\Pessoa)) {
            /** @var \G2\Entity\Pessoa $pessoaOrigem */
            $pessoaOrigem = $this->findOneBy('\G2\Entity\Pessoa', array(
                'id_usuario' => $id_usuario,
                'id_entidade' => $id_entidadeorigem
            ));

            $pessoa = clone $pessoaOrigem;
            unset($pessoa->_entityPersister, $pessoa->_identifier);
            $pessoa->setId_entidade($this->getReference('\G2\Entity\Entidade', $id_entidade));

            $pessoa = $this->save($pessoa);
        }

        return $pessoa;
    }

    public function retornaPessoaSecretariado($st_nomeexibicao, $id_entidade)
    {
        $id_entidade = $id_entidade ?: $this->sessao->id_entidade;

        $qb = $this->em->createQueryBuilder()
            ->select('pe.id_usuario', 'pe.st_nomeexibicao')
            ->distinct()
            ->from('\G2\Entity\Pessoa', 'pe')
            ->where('pe.bl_ativo = 1')
            ->andWhere('pe.id_situacao = :id_situacao')
            ->andWhere('pe.id_entidade = :id_entidade')
            ->andWhere('pe.st_nomeexibicao LIKE :st_nomeexibicao')
            ->setParameters(array(
                'id_situacao' => \G2\Constante\Situacao::TB_PESSOA_ATIVO,
                'id_entidade' => $id_entidade,
                'st_nomeexibicao' => $st_nomeexibicao . '%',
            ));

        return $qb->getQuery()->getResult();
    }

}

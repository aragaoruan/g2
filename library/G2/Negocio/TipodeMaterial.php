<?php
namespace G2\Negocio;

/**
 * Classe de negócio para TipoDeMaterial (tb_tipodematerial)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class TipodeMaterial extends Negocio
{

    private $repositoryName;

    public function __construct() {
        parent::__construct();
        $this->repositoryName = '\G2\Entity\TipodeMaterial';
    }

    public function save($data)
    {
        $id_attribute = 'id_tipodematerial';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }

        /**********************
         * Preparar atributos FK
         **********************/
        if (isset($data['id_entidade']['id_entidade']) && $data['id_entidade']['id_entidade'])
            $data['id_entidade'] = $this->find('\G2\Entity\Entidade', $data['id_entidade']['id_entidade']);
        else
            $data['id_entidade'] = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

        if (isset($data['id_situacao']['id_situacao']))
            $data['id_situacao'] = $this->find('\G2\Entity\Situacao', $data['id_situacao']['id_situacao']);


        /**********************
         * Definindo atributos
         **********************/
        $this->arrayToEntity($entity, $data);


        /**********************
         * Acao de salvar
         **********************/
        if (isset($data[$id_attribute]) && $data[$id_attribute])
            $entity = $this->merge($entity);
        else
            $entity = $this->persist($entity);

        $getId = 'get' . ucfirst($id_attribute);
        $data[$id_attribute] = $entity->$getId();

        $mensageiro = new \Ead1_Mensageiro();
        return $mensageiro->setMensageiro($entity->toArray(), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);

    }


}
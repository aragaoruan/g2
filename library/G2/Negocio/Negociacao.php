<?php

namespace G2\Negocio;

use G2\Constante\MeioPagamento;
use G2\Constante\Sistema;
use G2\Entity\CartaoBandeira;
use G2\Entity\Entidade;
use G2\Entity\Venda;
use G2\Entity\VendaProduto;
use SessaoLojaBO;
use stdClass;

/**
 * @author J. Augusto <augustowebd@gmail.com>
 * @since 2017-03-22
 */
class Negociacao extends Negocio
{
    /**
     * efetua negociacao de uma venda
     *
     * @param  object $data {
     *         id_venda,
     *         id_acrodo,
     *         id_lancamento
     *  }
     * @return stdClass
     * @throws \Exception
     */
    public function cartao(stdClass $data)
    {
        try {
            # recupera referencia da venda
            $eVenda = new Venda();
            $eVenda->find($data->id_venda);

            # recupera referencia da entidade para personalizar a página de pgto
            $eEntidade = new Entidade();
            $eEntidade = $this->findOneBy(
                get_class($eEntidade),
                array('id_entidade' => $eVenda->getId_entidade())
            );

            # configura a sessão
            $entSession = array();
            $entSession['id_sistema'] = Sistema::WORDPRESS_ECOMMERCE;
            $entSession['id_entidade'] = $eEntidade->getId_entidade();
            $entSession['st_nomeentidade'] = $eEntidade->getSt_nomeentidade();
            $entSession['st_urlimglogo'] = $eEntidade->getSt_urlimglogo();
            SessaoLojaBO::setSessaoEntidadeLoja($entSession);

            # recupera os dados do acordo
            $acordo = (object)$this->em->getRepository(get_class($eVenda))
                ->acordo($data->id_venda, $data->id_acordo);

            //verifica se retornou algo do método.
            if (!$acordo) {
                throw new \Exception("Dados do acordo não encontrado.");
            }

            # recupera os dados do lancamento
            /** @var \G2\Entity\Lancamento $lancamento */
        $lancamento = $this->find('\G2\Entity\Lancamento', $data->id_lancamento);

        # configura o objeto de venda
            $venda = new stdClass;

            # define o id do acordo
            $venda->idAcordo = $data->id_acordo;

            # define o meio de pagamento como sendo cartão
            $venda->idMeioPagamento = MeioPagamento::CARTAO_CREDITO;

            $venda->aluno = new stdClass;
            $venda->aluno->stNomeCompleto = $eVenda->getId_usuario()->getSt_nomecompleto();

            $venda->idVenda = $eVenda->getId_venda();
            $venda->dtCadastro = $eVenda->getDtCadastro()->format('d/m/Y');
            $venda->nuValorTotal = $acordo->vl_pagamento;
            $venda->nuValorTotalExibicao = self::formatCurrencyToBr($acordo->vl_pagamento);

            # recupera as bandeiras disponíveis para pagamento
            $venda->bandeiras = $this->listBandeiraCartao();

            $eProduto = new VendaProduto();
            $eProduto = $this->findOneBy(
                get_class($eProduto),
                array('id_venda' => $data->id_venda)
            );

            # optei por um array de produtos para facilitar o reuso desta estrutura no futuro
            $venda->produtos = array(
                (object)array(
                    'noProduto' => $eProduto->getId_produto()->getSt_produto(),
                    'nuProduto' => 1,
                    # note que este não é o valor do produto em si e sim o valor da renegociacao
                    'nuValorLiquido' => self::formatCurrencyToBr($venda->nuValorTotal),
                    'blRenegociado' => true
                )
            );

        $nuValorLiquidoExibicao = ($lancamento->getNu_valor() + $lancamento->getNu_juros()) - $lancamento->getNu_desconto();# define o número de parcelas e o valor do lancamento
        $venda->lancamento = new stdClass;
        $venda->lancamento->idLancamento = $data->id_lancamento;
        $venda->lancamento->nuParcelas = $acordo->nu_parcela;
        $venda->lancamento->nuValorLiquido = self::formatToSendToPayment($acordo->vl_pagamento);
        $venda->lancamento->nuValorLiquidoExibicao = self::formatCurrencyToBr($nuValorLiquidoExibicao);
        $venda->lancamento->nu_cartaoUtilizado = $lancamento->getnu_cartao();

            return $venda;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function renegociacao($data, $transaction)
    {
        # recupera referencia da venda
        $eVenda = new Venda();
        // $eVenda->find($data->id_venda);

        # recupera os dados do acordo
        $lancamentos = (object) $this->em->getRepository(get_class($eVenda))
                                         ->recuperaLancamentoNegociado($data->id_venda, $data->id_acordo);

        $sentinela = false;
        $date = new \DateTime();

        foreach ($lancamentos as $curr) {

            # recupera referencia da entidade para personalizar a página de pgto
            $lancamento = $this->findOneBy(
                '\G2\Entity\Lancamento',
                array('id_lancamento' => $curr['id_lancamento'])
            );

            # quita a primeira parcela
            if (! $sentinela) {
                $lancamento->setBl_quitado(1);
                $lancamento->setSt_statuslan(1);
                $lancamento->setdt_quitado($date);
                $lancamento->setNu_quitado(($lancamento->getNu_valor() + $lancamento->getNu_juros()) - $lancamento->getNu_desconto());
                $lancamento->setSt_idtransacaoexterna($transaction->tid);

                # marca o lacamento como hapto a ser enviado ao Fluxus
                $lancamento->setBl_baixadofluxus(1);

            } else {
                $lancamento->setBl_baixadofluxus(0);
                $date->add(new \DateInterval('P1M'));
            }

            $lancamento->setDt_prevquitado($date);


            $this->save($lancamento);

            $sentinela = true;
        }
    }

    /**
     * registra os dados necessários para que os "lançamentos" sejam aptos
     * a serem reconhecidos pelo robô como válidos para baixa no Fluxus
     *
     * @param  object $data {
     *         id_venda,
     *         id_acrodo,
     *         id_lancamento
     *  }
     * @return void
     * @throws Exception
     */
    public function registraBaixaLancamentoFluxos(stdClass $data)
    {
        # recupera referencia da venda
        $eVenda = new Venda();
        $eVenda->find($data->id_venda);

        # recupera os dados do acordo
        $lancamentos = (object)$this->em->getRepository(get_class($eVenda))
            ->lancamentosAcordo($data->id_venda, $data->id_acordo);

        $dt_prevquitado = new \DateTime();

        foreach ($lancamentos as $lancamento) {
            $lanRefer = $this->getReference('\G2\Entity\Lancamento', $lancamento['id_lancamento']);

            # define a previsãod e recebimento da próxima parcela
            $dt_prevquitado->add(new \DateInterval('P1M'));
            $lanRefer->setDt_prevquitado($dt_prevquitado);

            # define o id do pagamento informado pela operadora do cartão
            $lanRefer->setSt_idtransacaoexterna($data->id_transacaoexterna);

            # sinaliza que o lancamento deve ser sincronizada com o Fluxus
            $lanRefer->setBl_baixadofluxus(1);

            $this->save($lanRefer);
        }
    }

    /**
     * @return string[]
     */
    private function listBandeiraCartao()
    {
        $data = array();

        foreach ($this->findAll(new CartaoBandeira) as $bandeira) {
            $strname = $bandeira->getSt_cartaobandeira();
            $data[$strname] = (object)array('stCartaoBandeira' => $strname);
        }

        return array_values($data);
    }

    private static function formatToSendToPayment($amount)
    {
        return number_format($amount, 2, '', '');
    }

    private static function formatCurrencyToBr($amount)
    {
        return number_format($amount, 2, ',', '.');
    }
}

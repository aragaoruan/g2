<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 20/05/14
 * Time: 10:00
 */

namespace G2\Negocio;


class VerificarPagamentoRecorrente extends Negocio
{

    private $repositoryName = 'G2\Entity\VwVendaLancamento';

    public function __construct()
    {
        parent::__construct();
    }

    public function verificaPagamentocartaoRecorrente(array $params = array())
    {

        $resultado = false;
        $simulando = (isset($params['simulando']) ? $params['simulando'] : false);

        try {
            $lancamentos = $this->getRepository($this->repositoryName)->listarlancamentosCartaoRecorrentePendente($params);
            if ($lancamentos) {
                foreach ($lancamentos as $vw) {

                    $retorno = null;

                    if ($vw instanceof \G2\Entity\VwVendaLancamento) {

                        if (
                            $vw->getid_sistemacobranca() == \G2\Constante\Sistema::PAGARME
                            &&
                            substr($vw->getRecorrenteOrderid(), -3) != 'G1U'
                            &&
                            substr_count($vw->getRecorrenteOrderid(), '-') != 4
                        ) {
                            $recorrente = new Pagarme($vw->getIdEntidade());
                            $retorno = $recorrente->verificarRecorrente($vw, $simulando);
                        } else {
                            if (substr_count($vw->getRecorrenteOrderid(), '-') == 4) { // RecurrentPaymentId e2b7db92-eee6-48a7-8360-dac3864e120c
                                $recorrente = new \BraspagRecorrenteV2BO();
                            } else {
                                $recorrente = new \BraspagRecorrenteBO($vw->getIdEntidade());
                            }
                            $retorno = $recorrente->verificar($vw, $simulando);
                        }

                        if($retorno instanceof \Ead1_Mensageiro){
                            $resultado[] = $retorno->getFirstMensagem();
                        } else {
                            $resultado[] = $retorno;
                        }

                    }

                }

                return new \Ead1_Mensageiro($resultado, \Ead1_IMensageiro::SUCESSO);
            }

            return new \Ead1_Mensageiro("Nenhum lançamento encontrado!", \Ead1_IMensageiro::AVISO);
        } catch (\Exception $exception) {
            return new \Ead1_Mensageiro($exception->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }
}

<?php

namespace G2\Negocio;

use Ead1_IMensageiro;
use Ead1_Mensageiro;
use G2\Entity\Banco;
use \G2\Entity\ContaEntidade as ContaEntidadeEntity;
use \G2\Entity\Entidade as EntidadeEntity;
use \G2\Entity\TipoConta;

class ContaEntidade extends Negocio
{
    private $entidade;
    private $tipoConta;
    private $banco;
    private $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->entidade = new EntidadeEntity();
        $this->tipoConta = new TipoConta();
        $this->banco = new Banco();
        $this->mensageiro = new Ead1_Mensageiro();
    }

    public function salvarContaEntidade(array $dadosConta)
    {
        try {
            $entidadeConta = new ContaEntidadeEntity();

            $dadosConta['id_entidade'] = $this->getReference('\G2\Entity\Entidade', $dadosConta['id_entidade']);
            $dadosConta['id_tipodeconta'] = $this->getReference('\G2\Entity\TipoConta', $dadosConta['id_tipodeconta']);
            $dadosConta['st_banco'] = $this->getReference('\G2\Entity\Banco', $dadosConta['st_banco']);

            foreach ($dadosConta as $key => $value) {
                $entidadeConta->$key = $value;
            }
            $entidadeConta->setBl_padrao(true);

            if ($this->save($entidadeConta)) {
                return $this->mensageiro->setMensageiro(array($this->toArrayEntity($entidadeConta)), Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro ao Atualizar Conta Bancaria!', Ead1_IMensageiro::ERRO, $e->getMessage());
        }
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para PaDadosContato
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class PaDadosContato extends Negocio
{
  /**
   * Busca os dados de contato de acordo com os parametros enviados.
   * Caso necessario que o retorno seja em array, envie true no segundo parametros
   *
   * @param $dados
   * @param bool $array
   * @return array|\Ead1_Mensageiro|null|object
   * @throws \Exception
   */
  public function retornaPaDadosContatoIdEntidade($dados, $array = false)
  {
    $result = $this->findOneBy('G2\Entity\PADadosContato', $dados);

    if (!is_null($result)) {
      if ($array) {
        $result = $this->toArrayEntity($result);
        return $result;
      }
      return $result;
    }
    return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
  }
}

<?php

/**
 * Classe Negocio para ContratoRegra
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @since 2014-10-31
 */

namespace G2\Negocio;

class ContratoRegra extends Negocio
{

    private $repositoryName = 'G2\Entity\ContratoRegra';

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $id_attribute = 'id_contratoregra';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }


        /*********************
         * Definindo atributos
         *********************/
        $defaults = array(
            'id_entidade' => $this->sessao->id_entidade,
            'id_usuariocadastro' => $this->sessao->id_usuario,
            'bl_ativo' => true,
            'dt_cadastro' => date('Y-m-d H:i:s')
        );

        // Nao permite campos NULLS que estejam no DEFAULTS
        foreach ($data as $attr => $val) {
            if (array_key_exists($attr, $defaults) && $val === null) {
                $data[$attr] = $defaults[$attr];
            }
        }

        $data = array_merge($defaults, $data);
        $this->entitySerialize->arrayToEntity($data, $entity);


        /**********************
         * Acao de salvar
         **********************/
        $mensageiro = new \Ead1_Mensageiro();

        try {
            if (isset($data[$id_attribute]) && $data[$id_attribute]) {
                $entity = $this->merge($entity);
            } else {
                $entity = $this->persist($entity);
            }

            $data[$id_attribute] = $entity->getId_contratoregra();

            return $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);
        } catch (Exception $e) {
            $mensageiro->setMensageiro(array(
                __FILE__ . ':' . __LINE__,
                $e->getMessage(),
            ), \Ead1_IMensageiro::ERRO, $data[$id_attribute]);
            $mensageiro->setText('Não foi possível salvar o registro.');
            return $mensageiro;
        }

    }

    /**
     * @param int $id_modelocarteirinha
     * @return string
     */
    public function getModeloCarteirinha($id_modelocarteirinha) {
        if ($id_modelocarteirinha instanceof \G2\Entity\ModeloCarteirinha) {
            $id_modelocarteirinha->getId_modelocarteirinha();
        }

        switch ($id_modelocarteirinha) {
            case \G2\Constante\ModeloCarteirinha::GRADUACAO:
                $modelo = 'graduacao.phtml';
                break;
            default:
                $modelo = 'padrao.phtml';
        }

        return $modelo;
    }


}

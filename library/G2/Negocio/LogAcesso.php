<?php

namespace G2\Negocio;

/**
 * Classe de negocio para Log de acesso
 */
class LogAcesso extends Negocio
{

    public $repositoryName = '\G2\Entity\LogAcesso';

    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Salva dados de Log de Acesso, caso seja necessario retorna um mensageiro
     *
     * @param array $data
     * @param bool $mensageiro
     * @return bool|\Ead1_Mensageiro|mixed
     * @throws \Zend_Exception
     */
    public function salvaLogAcesso(array $data, $mensageiro = false)
    {
        try {
            $entity = new \G2\Entity\LogAcesso();

            //verifica se o id_loacesso veio
            if (array_key_exists('id_logacesso', $data) && !empty($data['id_logacesso'])) {
                $entity = $this->find($this->repositoryName, $data['id_logacesso']); //busca o log
                if (!$entity) { //se não encontrou cria um exception
                    throw new \Exception('id_logacesso não encontrado.');
                }
            }

            if (isset($data['id_entidade']) && !empty($data['id_entidade'])) {
                $id_entidade = $data['id_entidade'];
                $entity->setId_entidade($id_entidade);
            }

            if (isset($data['id_funcionalidade']) && !empty($data['id_entidade'])) {
                $id_funcionalidade = $data['id_funcionalidade'];
                $entity->setId_funcionalidade($id_funcionalidade);
                $this->setaFuncionalidadeNaSessao($id_funcionalidade);//seta a funcionalidade na sessão
            }

            if (isset($data['id_perfil']) && !empty($data['id_entidade'])) {
                $id_perfil = $data['id_perfil'];
                $entity->setId_perfil($id_perfil);
            }

            if (isset($data['id_usuario']) && !empty($data['id_entidade'])) {
                $id_usuario = $data['id_usuario'];
                $entity->setId_usuario($id_usuario);
            }

            if (isset($data['id_saladeaula']) && !empty($data['id_entidade'])) {
                $id_saladeaula = $data['id_saladeaula'];
                $entity->setId_saladeaula($id_saladeaula);
            }

            if (isset($data['id_matricula']) && !empty($data['id_matricula'])) {
                $entity->setId_matricula($data['id_matricula']);
            } else {
                $entity->setId_matricula($this->sessao->id_matricula);
            }

            if (isset($data['st_parametros']) && !empty($data['st_parametros'])) {
                $entity->setSt_parametros($data['st_parametros']);
            }

            if (isset($data['id_sistema']) && !empty($data['id_sistema'])) {
                $entity->setId_sistema($data['id_sistema']);
            }

            if (isset($data['st_urlcompleta']) && !empty($data['st_urlcompleta'])) {
                $entity->setSt_urlcompleta($data['st_urlcompleta']);
            }

            $entity->setDt_cadastro(new \DateTime());

            if ($mensageiro) {
                if (!$this->save($entity)) {
                    throw new \Zend_Exception('Erro ao salvar o log.');
                };
                $mensageiro = new \Ead1_Mensageiro();
                $mensageiro->setMensageiro('Log Salvo com Sucesso', \Ead1_IMensageiro::SUCESSO);
                return $mensageiro;
            } else {
                return $this->save($entity);
            }
        } catch (\Exception $ex) {
            throw new \Zend_Exception('Erro - ' . $ex->getMessage());
        }
    }

    /**
     * Retorna logs
     * @param array $where
     * @param array $order
     * @return \G2\Entity\LogAcesso
     */
    public function retornaLogs(array $where = array(), array $order = array())
    {
        return $this->findBy($this->repositoryName, $where, $order);
    }

    /**
     * Retorna log por id
     * @param integer $id
     * @return \G2\Entity\LogAcesso
     */
    public function findLogs($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /**
     * Cria um epaço na sessão com o id_funcionalidade
     * @param $funcionalidade
     */
    private function setaFuncionalidadeNaSessao($funcionalidade)
    {
        //cria a sessão
        $sessao = new \Zend_Session_Namespace('funcionalidade_atual');
        $sessao->id_funcionalidade = $funcionalidade;
    }
}

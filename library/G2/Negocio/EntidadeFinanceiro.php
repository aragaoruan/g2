<?php

namespace G2\Negocio;
use Doctrine\Common\Util\Debug;
use Ead1\Doctrine\EntitySerializer;

/**
 * Classe de negócio para Recebimento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class EntidadeFinanceiro extends Negocio
{

    private $repositoryName = '\G2\Entity\EntidadeFinanceiro';

    public function __construct()
    {
        set_time_limit(0);
        parent::__construct();
    }

    /**
     * @param mixed $data
     * @return type
     */
    public function save($data)
    {
        if ($data instanceof \G2\Entity\EntidadeFinanceiro) {
            if ($data->getId_entidadefinanceiro()) {
                $retorno = $this->merge($data);
            } else {
                $retorno = $this->persist($data);
            }
            return $retorno;
        } else {
            try {
                //             ["id_entidadefinanceiro"] => NULL
                if (isset($data['id_entidadefinanceiro'])) {
                    //se existir o id, busca o registro
                    $entity = $this->findOneBy($this->repositoryName, array('id_entidadefinanceiro' => $data['id_entidadefinanceiro']));
                } else {
                    //se não existir o id cria um novo registro
                    $entity = new \G2\Entity\EntidadeFinanceiro();
                }

                //       ["id_reciboconsolidado"] => string(3) "113"
                if ($data['id_reciboconsolidado']) {
                    $entity->setId_reciboconsolidado($this->getReference('\G2\Entity\TextoSistema', $data['id_reciboconsolidado']));
                }
                //       ["id_textochequesdevolvidos"] => string(3) "617"
                if ($data['id_textochequesdevolvidos']) {
                    $entity->setId_textochequesdevolvidos($this->getReference('\G2\Entity\TextoSistema', $data['id_textochequesdevolvidos']));
                }
                //       ["id_textosistemarecibo"] => string(3) "112"
                if ($data['id_textosistemarecibo']) {
                    $entity->setId_textosistemarecibo($this->getReference('\G2\Entity\TextoSistema', $data['id_textosistemarecibo']));
                }
                //       ["id_textoimpostorenda"] => string(3) "652"
                if ($data['id_textoimpostorenda']) {
                    $entity->setId_textoimpostorenda($this->getReference('\G2\Entity\TextoSistema', $data['id_textoimpostorenda']));
                }

                if(!empty($data['bl_protocolo'])){
                    $entity->setBl_protocolo($data['bl_protocolo']);
                }elseif(empty($data['bl_protocolo']) && empty($entity->getBl_protocolo()) ){
                    $entity->setBl_protocolo(0);
                }
                //              ["id_entidade"] => int(24)
                if (!empty($data['id_entidade'])) {
                    $entity->setId_entidade($data['id_entidade']);
                } else {
                    $entity->setId_entidade($this->sessao->id_entidade);
                }

                $retorno = $this->save($entity);

                $mensageiro = new \Ead1_Mensageiro();
                return $mensageiro->setMensageiro('Salvo com sucesso', \Ead1_IMensageiro::SUCESSO, $retorno->getId_entidadefinanceiro());
            } catch (Exception $e) {
                return new \Ead1_Mensageiro('Erro ao salvar EntidadeFinanceiro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
            }
        }
    }

    public function findByEntidade($id_entidade)
    {
        $retorno = $this->em->getRepository($this->repositoryName)->findOneBy(array('id_entidade' => $id_entidade));
        if ($retorno && $retorno->getId_entidadefinanceiro()) {
            return new \Ead1_Mensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro('Nenhum registro encontrado', \Ead1_IMensageiro::AVISO);
        }
    }
}
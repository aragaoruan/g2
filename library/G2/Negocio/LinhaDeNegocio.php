<?php
/**
 * Created by PhpStorm.
 * User: Denise Xavier
 * Date: 16/12/2015
 * Time: 09:04
 */

namespace G2\Negocio;


class LinhaDeNegocio extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function findAllLinhaNegocio()
    {
        try {
            $result = $this->em->getRepository('\G2\Entity\LinhaDeNegocio')->findAll();
            if ($result) {
                $arrReturn = array();
                foreach ($result as $row) {
                    $arrReturn[] = $this->toArrayEntity($row);
                }

                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }



}
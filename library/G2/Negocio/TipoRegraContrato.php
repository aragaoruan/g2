<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Entity TipoRegraContrato
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class TipoRegraContrato extends Negocio
{


    public function __construct ()
    {
        parent::__construct();
    }

    public function retornarTipoRegraContrato()
    {
        return $this->findAll('\G2\Entity\TipoRegraContrato');
    }

}

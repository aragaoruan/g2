<?php

namespace G2\Negocio;

/**
 * Classe de negócio para RegraPagamento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class RegraPagamento extends Negocio {

    private $repositoryName = 'G2\Entity\RegraPagamento';

    public function __construct() {
        set_time_limit(0);
        parent::__construct();
    }

    public function findById($id) {
        $result = parent::find($this->repositoryName, $id);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * @param mixed $data
     * @return type
     */
    public function save($data) {
        if ($data instanceof \G2\Entity\RegraPagamento) {
            if ($data->getId_regrapagamento()) {
                $retorno = $this->merge($data);
            } else {
                $retorno = $this->persist($data);
            }
            return $retorno;
        } else {
            try {
                if (isset($data['id_regrapagamento'])) {
                    //se existir o id, busca o registro
                    $entity = $this->find($this->repositoryName, $data['id_regrapagamento']);
                } else {
                    //se não existir o id cria um novo registro
                    $entity = new \G2\Entity\RegraPagamento();
                    $entity->setDt_cadastro(new \DateTime());
                }

                if ($data['id_tiporegrapagamento']) {
                    $entity->setId_tiporegrapagamento($this->find('\G2\Entity\TipoRegraPagamento', $data['id_tiporegrapagamento']));
                }

                //seta os atributos
                if ($data['id_areaconhecimento']) {
                    $entity->setId_areaconhecimento($this->find('\G2\Entity\AreaConhecimento', $data['id_areaconhecimento']));
                }

                $entity->setId_cargahoraria($this->find('\G2\Entity\CargaHoraria', $data['id_cargahoraria']));

                if ($data['id_entidade']) {
                    $entity->setId_entidade($this->find('\G2\Entity\Entidade', $data['id_entidade']));
                } else {
                    $entity->setId_entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));
                }

                if ($data['id_professor']) {
                    $entity->setId_professor($this->find('\G2\Entity\Usuario', $data['id_professor']));
                }

                if ($data['id_projetopedagogico']) {
                    $entity->setId_projetopedagogico($this->find('\G2\Entity\ProjetoPedagogico', $data['id_projetopedagogico']));
                }

                $entity->setId_tipodisciplina($this->find('G2\Entity\TipoDisciplina', $data['id_tipodisciplina']));
                $entity->setNu_valor($data['nu_valor']);

                if ($data['id_regrapagamento']) {
                    $entity->setBl_ativo($data['bl_ativo']);
                    $retorno = $this->merge($entity);
                } else {
                    $entity->setBl_ativo(1);
                    $retorno = $this->persist($entity);
                }

                $mensageiro = new \Ead1_Mensageiro();
                return $mensageiro->setMensageiro('Salvo com sucesso', \Ead1_IMensageiro::SUCESSO, $retorno->getId_regrapagamento());
            } catch (Exception $e) {
                return new \Ead1_Mensageiro('Erro ao salvar RegraPagamento: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
            }
        }
    }

    public function findRegraPagamento($id_tiporegrapagamento, array $filtro = null) {
        $dql = 'SELECT vw FROM G2\Entity\VwRegraPagamento vw '
                . 'WHERE vw.id_tiporegrapagamento = :id_regra AND vw.id_entidade = :id_entidade AND vw.bl_ativo = 1';

        if ($filtro) {
            foreach ($filtro as $key => $value) {
                if ($key == 'nu_valor') {
                    if (((float) $value) > 0) {
                        $dql .= ' AND vw.' . $key . ' = ' . $value;
                    }
                } else if ((int) $value > 0) {
                    $dql .= ' AND vw.' . $key . ' = ' . $value;
                }
            }
        }

        $query = $this->em->createQuery($dql);
        $query->setParameter('id_entidade', \Ead1_Sessao::getSessaoGeral()->id_entidade);
        $query->setParameter('id_regra', $id_tiporegrapagamento);

        return $query->getResult();
    }

    public function findByEntidade(array $filtro = null) {
        return $this->findRegraPagamento(\G2\Constante\TipoRegraPagamento::ENTIDADE, $filtro);
    }

    public function findByAreaConhecimento(array $filtro = null) {
        return $this->findRegraPagamento(\G2\Constante\TipoRegraPagamento::AREA_CONHECIMENTO, $filtro);
    }

    public function findByProjetoPedagogico(array $filtro = null) {
        return $this->findRegraPagamento(\G2\Constante\TipoRegraPagamento::PROJETO_PEDAGOGICO, $filtro);
    }

    public function findByProfessor(array $filtro = null) {
        return $this->findRegraPagamento(\G2\Constante\TipoRegraPagamento::PROFESSOR, $filtro);
    }

    public function findByProfessorProjeto(array $filtro = null) {
        return $this->findRegraPagamento(\G2\Constante\TipoRegraPagamento::PROFESSOR_PROJETO, $filtro);
    }

    public function delete($entity) {
        try {
            $entity->setBl_ativo(0);
            $this->save($entity);
            $this->em->flush();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}

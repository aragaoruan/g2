<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Atualização na Baião CMS
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class Baiao extends Negocio {

	public function atualizaProduto($id_produto){
		
		try {
			
			$produto  = $this->em->getRepository('\G2\Entity\VwProduto')->find($id_produto);
			if($produto instanceof \G2\Entity\VwProduto){
				$ei = $this->em->getRepository('\G2\Entity\EntidadeIntegracao')->findOneBy(array('id_sistema'=>\SistemaTO::BAIAOCMS, 'id_entidade'=>$produto->getId_entidade()));
				if(!$ei){
					return false;
				}
				
				
				$product = new \BaiaoCMS_ProductVO();
				$product->setId($produto->getId_produto());
				$product->setTitle($produto->getSt_produto());
				$product->setSubtitle($produto->getSt_subtitulo());
				$product->setPrice($produto->getNu_valor());
				$product->setPrice_promotional($produto->getNu_valorpromocional());
				$product->setPromotional_start(substr($produto->getDt_iniciopontosprom(),0,19));
				$product->setPromotional_end(substr($produto->getDt_fimpontosprom(),0,19));
				$product->setPartner_id($produto->getId_entidade());
				$product->setDot($produto->getNu_pontos());
				$product->setDot_promotional($produto->getNu_pontospromocional());
				$product->setDot_start((substr($produto->getDt_iniciopontosprom(),0,19)));
				$product->setDot_end((substr($produto->getDt_fimpontosprom(),0,19)));
				
				$key 	= json_decode($ei->getSt_codchave());
				$baiao 	= new \BaiaoCMS($ei->getSt_caminho(), $key->ApiKey, $key->ApiSecret);
				$save 	= $baiao->saveProduct($product);
				
				$return = json_decode($save);
				if($return->status=="error"){
					throw new \Exception(get_class($this)."(".__LINE__.")"." ".$return->status."(".$return->code."): ".$return->message);
				} elseif($return->status=="success"){
					return true;
				} else {
					throw new \Exception(get_class($this)."(".__LINE__.")"." ".$save);
				}
				
			} else {
				throw new \Exception(get_class($this)."(".__LINE__.")"." O Produto não foi encontrado na VwProduto");
			}
			
		} catch (Exception $e) {
			throw $e;
			return false;
		}
		
	}

}

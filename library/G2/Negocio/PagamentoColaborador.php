<?php

namespace G2\Negocio;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-05-13
 */
class PagamentoColaborador extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Returns data \G2\Entity\VwUsuariosComissionados based on parameters
     * @param array $where
     * @param boolean $toArray
     * @return mixed Returns an array of object or an array of array
     * @throws \Zend_Exception
     */
    public function retornarUsuariosComissionados(array $where = array(), $toArray = false)
    {
        try {
            $repositoryName = '\G2\Entity\VwUsuariosComissionados'; //atribui o repository name a variavel
            if (!array_key_exists('id_entidade', $where) || empty($where['id_entidade'])) { //verifica se o id da entidade não foi passado
                $where['id_entidade'] = $this->sessao->id_entidade; //seta o id da entidade na chave do array
            }
            $result = $this->em->getRepository($repositoryName)->retornarUsuariosComissionados($where, array('st_nomecompleto' => 'asc', 'id_usuario' => 'desc'));
            //busca os dados
            if ($toArray) { //verifica se o $toArray veio como true
                $arrResult = array(); //cria uma variavel com array vazio
                foreach ($result as $i => $row) { //percorre os dados
                    $arrResult[$i] = $row; //deserializa a entity e atribui o array no array
                }
                return $arrResult; //retorna o array
            }
            return $result; //retorna o objeto
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Houve um erro ao tentar consultar os Usuarios Comissionados. " . $ex->getMessage());
        }
    }

    /**
     * Formata o valor passado no padrão de 2 casas decimais separados por virgula e com sinal de positivo para maior que 0 (zero) e negativo para menor
     * @param float $nu_valor
     * @return string
     */
    private function formataValorParaExibicao($nu_valor)
    {
        $bo = new \Ead1_BO();
        if ($nu_valor < 0) {
            return $bo->formataValor($nu_valor, '#,##0.00');
        } elseif ($nu_valor > 0) {
//            return '+' . $bo->formataValor($nu_valor, '#,##0.00');
            return '+' . $bo->toFloat($nu_valor);
        } else {
            return $bo->formataValor($nu_valor, '#,##0.00');
        }
    }

    /**
     * Recupera as informações da vw_comissaoreceber e salva os lancamentos e as comissões lancamento
     * @param array $data Array com chave valor para buscar dados em vw_comissaoreceber
     * @return \G2\Entity\VwComissaoReceber
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function salvarComissaoReceber(array $data)
    {
        try {

            //busca os dados da vw_comissaoreceber
            $resultComissao = $this->em->getRepository('\G2\Entity\VwComissaoReceber')
                ->retornarDadosVwComissaoReceber($data);

            if ($resultComissao) { //verifica se achou algo
                $this->beginTransaction(); //inicia transação
                //percore os resultados
                foreach ($resultComissao as $row) {
                    if ($row['nu_comissaoreceber']) {
                        //salva o lancamento
                        $lancamento = $this->salvarLancamento(array('nu_valor' => $row['nu_comissaoreceber']));
                        if (!$lancamento) { //se não retornar um lancamento cria uma excessão
                            throw new \Exception("Erro ao salvar Lançamento." . $lancamento);
                        }
                        //salva a comissao lancamento
                        $comissao = $this->salvarComissaoLancamento(array(
                            'id_usuario' => $row['id_usuario'],
                            'id_vendaproduto' => $row['id_vendaproduto'],
                            'id_lancamento' => $lancamento->getIdLancamento(),
                            'id_disciplina' => $row['id_disciplina'],
                            'id_perfilpedagogico' => $row['id_funcao']
                        ));
                        //se não retornar um objeto cria uma excessao
                        if (!$comissao) {
                            throw new \Exception("Erro ao salvar Comissao Lançamento." . $comissao);
                        }
                    }
                }
                //commit
                $this->commit();
                return $resultComissao; //retorna o array de dados encontrado
            } else {
                return 'Nenhum lancamento encontrado.';
            }
        } catch (\Exception $ex) {
            $this->rollback();
            throw new \Zend_Exception("Houve um erro ao tentar salvar os lancamentos de comissão. " . $ex->getMessage());
        }
    }

    /**
     * Busca dados e vw_comissaoreceber e retorna um array de objetos
     * @param array $where
     * @return array de objeto \G2\Entity\VwComissaoReceber
     * @throws \Zend_Exception
     */
    public function buscarComissoesReceber(array $where = array())
    {
        try {
            $result = $this->findBy('\G2\Entity\VwComissaoReceber', $where);
            return $result;
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Houve um erro ao tentar recuperar comissao receber. " . $ex->getMessage());
        }
    }

    /**
     * Seta os atributos do lancamento e salva
     * @param array $data
     * @return \G2\Entity\Lancamento
     * @throws \Zend_Exception
     */
    private function salvarLancamento(array $data)
    {
        try {
            $vendaNegocio = new Venda(); //instancia a negocio de venda
            //seta os atributos fixos
            $data['id_tipolancamento'] = 2;
            $data['id_meiopagamento'] = 8;
            $data['id_usuariocadastro'] = $this->sessao->id_usuario;
            $data['dt_vencimento'] = date('d/m/Y');
            return $vendaNegocio->salvarLancamento($data); //chama o metodo da negocio para salvar e retorna
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Houve um erro ao tentar salvar o lancamento. " . $ex->getMessage());
        }
    }

    /**
     * Salva Comissao Lançamento
     * @param array $params
     * @return \G2\Entity\ComissaoLancamento
     * @throws \Zend_Exception
     */
    public function salvarComissaoLancamento(array $params, $batch = false)
    {
        try {
            //verifica os valores passados pelo paramentro
            $data = $this->verificaDadosComissaoLancamento($params);
            $entity = new \G2\Entity\ComissaoLancamento(); //instancia a entity
            if ($data['id_comissaolancamento']) { //verifica se o id ca comissao foi passado
                $result = $this->find('\G2\Entity\ComissaoLancamento', $data['id_comissaolancamento']); //busca a comissao
                if ($result) //verifica se achou
                    $entity = $result; //reescreve o valor da variavel
            }

            //seta os valores na entity
            $entity->setBl_adiantamento($data['bl_adiantamento'])
                ->setDt_cadastro($data['dt_cadastro'])
                ->setBl_autorizado($data['bl_autorizado']);

            if ($data['id_lancamentopagamento'])
                $entity->setId_lancamentopagamento($data['id_lancamentopagamento']);

            if ($data['id_lancamento'])
                $entity->setId_lancamento($data['id_lancamento']);

            if ($data['id_vendaproduto'])
                $entity->setId_vendaproduto($data['id_vendaproduto']);

            if ($data['id_usuario']) {
                $entity->setId_usuario($data['id_usuario']);
            } else {
                throw new \Exception("Não é permitido salvar um lançamento de comissão sem informar o id do usuário");
            }

            if ($data['id_disciplina'])
                $entity->setId_disciplina($data['id_disciplina']);

            if ($data['id_entidade'])
                $entity->setId_entidade($data['id_entidade']);

            if ($data['id_perfilpedagogico'])
                $entity->setId_perfilpedagogico($data['id_perfilpedagogico']);


            //salva
            if (!$batch) {
                $entity = $this->save($entity);
            } else {
                $entity = $this->em->persist($entity);
            }
            return $entity;
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Houve um erro ao tentar salvar a comissao lancamento. " . $ex->getMessage());
        }


    }

    /**
     * Verifica os arrays e seta os valores nos mesmos
     * @param array $data
     * @return array
     */
    private function verificaDadosComissaoLancamento(array $data)
    {
        $data['id_comissaolancamento'] = isset($data['id_comissaolancamento']) ? $data['id_comissaolancamento'] : null;
        $data['id_usuario'] = isset($data['id_usuario']) ? $this->getReference('\G2\Entity\Usuario', $data['id_usuario']) : null;
        $data['id_lancamento'] = isset($data['id_lancamento']) ? $this->getReference('\G2\Entity\Lancamento', $data['id_lancamento']) : null;
        $data['id_vendaproduto'] = isset($data['id_vendaproduto']) ? $this->getReference('\G2\Entity\VendaProduto', $data['id_vendaproduto']) : null;
        $data['id_disciplina'] = isset($data['id_disciplina']) ? $this->getReference('\G2\Entity\Disciplina', $data['id_disciplina']) : null;
        $data['id_lancamentopagamento'] = isset($data['id_lancamentopagamento']) ? $this->getReference('\G2\Entity\Lancamento', $data['id_lancamentopagamento']) : null;
        $data['dt_cadastro'] = isset($data['dt_cadastro']) ? new \DateTime($data['dt_cadastro']) : new \DateTime();
        $data['bl_adiantamento'] = isset($data['bl_adiantamento']) ? $data['bl_adiantamento'] : false;
        $data['bl_autorizado'] = isset($data['bl_autorizado']) ? $data['bl_autorizado'] : false;
        $data['id_entidade'] = isset($data['id_entidade']) ? $this->getReference('\G2\Entity\Entidade', $data['id_entidade']) : $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
        $data['id_perfilpedagogico'] = isset($data['id_perfilpedagogico']) ? $data['id_perfilpedagogico'] : null;

        return $data;
    }

    /**
     * Retorna as disciplinas do colaborador
     * @param $idUsuario
     * @return array
     */
    public function retornaDisciplinasColaborador($idUsuario)
    {
        $pessoaNegocio = new Pessoa();
        $result = $pessoaNegocio->retornaDisciplinasColaboradores($idUsuario);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $disciplina) {
                $arrReturn[] = array(
                    'id' => $disciplina['id_disciplina'],
                    'text' => $disciplina['st_disciplina']
                );
            }
        }
        return $arrReturn;
    }

    /**
     * Salva Adiantamento do Colaborador
     * @param array $data
     * @return array
     * @throws \Zend_Exception
     */
    public function salvarAdiantamento(array $data)
    {
        $bo = new \Ead1_BO();
        try {
            if (isset($data['nu_valor'])) {
                $data['nu_valor'] = str_replace(",", ".", $bo->converteMoeda($data['nu_valor']));
            }
            $this->beginTransaction();
            $lancamento = $this->salvarLancamento(array('nu_valor' => $data['nu_valor']));
            if (!$lancamento) { //se não retornar um lancamento cria uma excessão
                throw new \Exception("Erro ao salvar Lançamento." . $lancamento);
            }

            //salva a comissao lancamento
            $comissao = $this->salvarComissaoLancamento(array(
                'id_usuario' => $data['id_usuario'],
                'id_disciplina' => $data['id_disciplina'],
                'id_lancamento' => $lancamento->getIdLancamento(),
                'bl_adiantamento' => true,
                'id_perfilpedagogico' => $data['id_funcao']
            ));
            //se não retornar um objeto cria uma excessao
            if (!$comissao) {
                throw new \Exception("Erro ao salvar Comissao Lançamento.");
            }

            $this->commit();
            return $data;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao salvar adiantamento do colaborador. " . $e->getMessage());
        }
    }

    /**
     * Retorna os dados do extrato detalhado do colaborador com perfil de coordenador de projeto
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarExtratoDetalhadoCoordenadorProjeto(array $params = [])
    {
        try {

            //calcula o periodo das datas
            $periodo = $this->getPeriodoExtratoColaborador($params['ano'], $params['mes']);
            //seta os parametros
            $params['dt_inicio'] = $periodo['dt_inicio'];
            $params['dt_termino'] = $periodo['dt_fim'];
            $params['id_entidade'] = array_key_exists('id_entidade', $params) ? $params['id_entidade'] : $this->sessao->id_entidade;

            //chama a repository para buscar os dados
            $result = $this->em->getRepository('\G2\Entity\ComissaoLancamento')
                ->retornaExtratoCoordenadorProjeto($params['id_usuario'], $params['id_entidade'], $params['dt_inicio'], $params['dt_termino']);
            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar extrato de coordenadores de projeto. " . $e->getMessage());
        }
    }


    /**
     * Retorna os dados do extrato detalhado do colaborador com perfil de coordenador de disciplina
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarExtratoDetalhadoCoordenadorDisciplina(array $params = [])
    {
        try {
            //verifica se foi passado os parametros de ano e mês, se não foram retorna o dia e mês atual
            $ano = array_key_exists('ano', $params) && !empty($params['ano']) ? $params['ano'] : date('Y');
            $mes = array_key_exists('mes', $params) && !empty($params['mes']) ? $params['mes'] : date('m');

            //calcula o periodo das datas
            $periodo = $this->getPeriodoExtratoColaborador($ano, $mes);

            //seta os parametros
            $params['dt_inicio'] = $periodo['dt_inicio'];
            $params['dt_termino'] = $periodo['dt_fim'];
            $params['id_entidade'] = array_key_exists('id_entidade', $params) ? $params['id_entidade'] : $this->sessao->id_entidade;

            //chama a repository para buscar os dados
            $result = $this->em->getRepository('\G2\Entity\ComissaoLancamento')
                ->retornaExtratoCoordenadorDisciplina($params['id_usuario'], $params['id_entidade'], $params['dt_inicio'], $params['dt_termino']);

            //monta o array vazio para armazenar os dados
            $arrReturn = array();
            if ($result) {
                //percorre os dados
                foreach ($result as $i => $disciplina) {
                    //atribui o valor da disciplina na variavel para ser utilizada como index do array para o retorno
                    $idDisciplina = $disciplina['id_disciplina'];

                    //verifica se não foi criado um index com o id_disciplina e cria um array concatenando
                    if (!isset($arrReturn[$idDisciplina])) {
                        $arrReturn[$idDisciplina] = array(
                            'id_disciplina' => $idDisciplina,
                            'st_disciplina' => $disciplina['st_disciplina'],
                        );
                    }

                    /*
                     * verifica o tipo se for Adiantamento transforma o valor em negativo
                     * pois o Adiantamento é um emprestimo feito ao autor ou professor
                     */
                    if ($disciplina['st_tipo'] == 'Adiantamento') {
                        $nu_valor = $disciplina['nu_valor'] * (-1);
                    } else {
                        $nu_valor = $disciplina['nu_valor'];
                    }

                    //cria uma posição no array de retorno com os detalhes
                    $arrReturn[$idDisciplina]['detalhes'][$i] = array(
                        'st_tipo' => $disciplina['st_tipo'],
                        'nu_valor' => $nu_valor,
                        'nu_valorvenda' => $disciplina['nu_valorvenda']
                    );
                    //busca os projetos vinculados a disciplina
                    $resultProjeto = $result = $this->em->getRepository('\G2\Entity\ComissaoLancamento')
                        ->retornaProjetoDetalheExtrato($idDisciplina, $disciplina['id_usuario'], $params['id_entidade'], $params['dt_inicio'], $params['dt_termino']);
                    $arrReturn[$idDisciplina]['projetos'] = $resultProjeto;
                }
            }
            sort($arrReturn);
            unset($result);
            return $arrReturn;
        } catch
        (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar extrado do colaborador. " . $e->getMessage());
        }
    }

    /**
     * Retorna os Projetos e Disciplinas vinculados ao pagamento
     * @param integer $id_lancamentopagamento
     * @return array
     * @throws \Zend_Exception
     */
    private function retornaProjetoDisciplinaExtratoPagamento($id_lancamentopagamento)
    {
        try {
            $result = $this->findBy('\G2\Entity\ComissaoLancamento', array('id_lancamentopagamento' => $id_lancamentopagamento));

            $arrReturn = array();
            if ($result) {
                foreach ($result as $i => $lancamento) {
                    //Procura o Produto no relacionamento do lancamento
                    $idProduto = $lancamento->getId_vendaproduto() ? $lancamento->getId_vendaproduto()->getId_produto()->getId_produto() : NULL;
                    //cria uma variavel
                    $stProjeto = NULL;

                    if ($idProduto) { //verifica se tem id do Produto
                        //Busca o relacionamento do produto com o projeto pedagogico
                        $prodProjeto = $this->findOneBy('\G2\Entity\ProdutoProjetoPedagogico', array('id_produto' => $idProduto));
                        if ($prodProjeto) { //verifica se achou
                            $stProjeto = $prodProjeto->getId_projetopedagogico() ? $prodProjeto->getId_projetopedagogico()->getSt_projetopedagogico() : NULL; //atribui o nome do projeto a variavel
                        }
                    }
                    //preenche o array de retorno
                    $arrReturn[$i]['st_disciplina'] = $lancamento->getId_disciplina() ? $lancamento->getId_disciplina()->getSt_disciplina() : NULL;
                    $arrReturn[$i]['st_projetopedagogico'] = $stProjeto; //preenche a posição do array
                }
            }
            return $arrReturn;
        } catch
        (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar extrado do colaborador. " . $e->getMessage());
        }
    }

    /**
     * Retorna um array com data inicial e data final para o periodo do filtro do extrato
     * @param $ano integer valor do ano
     * @param $mes integer valor do mes
     * @return array Par chave => valor com a data de inicio e fim
     */
    private function getPeriodoExtratoColaborador($ano, $mes)
    {
        //cria a primeira data sempre colocando o dia 1 concatenando com o mes e ano
        $dataInicio = date("Y-m-d H:i:s", strtotime($ano . "-" . $mes . "-01"));
        //recupera o numero de dias do mes, baseado na data inicial
        $totalDias = date('t', strtotime($dataInicio));
        //cria a data final baseada no mes e ano e o numero total de dias recuperado, fixando o valor da hora
        $dataFim = date("Y-m-d H:i:s", strtotime($ano . "-" . $mes . "-" . $totalDias . " 23:59:59"));

        //retornar um array com par de chave valor
        return array(
            'dt_inicio' => $dataInicio,
            'dt_fim' => $dataFim
        );
    }

    /**
     * Salva um array de lançamentos vinculados ao um colaborador, definindo que as comissões que foram quitadas.
     * Basicamente cria um novo lançamento com o valor total de todos os lançamentos do colaborador, data definida,
     * após isso, altera todos os lançamentos da comissão setando o id do lançamento criado na coluna id_lancamentopagamento
     * @param array $dados
     * @return array
     * @throws \Zend_Exception
     */
    public function salvarArrayLancamentosPagamentoColaborador(array $dados)
    {
        try {
            //inicia a transação
            $this->beginTransaction();
            $arrReturn = array();
            if ($dados) {
                //percorre os arrays
                foreach ($dados as $lancamento) {

                    $valor = str_replace(",", ".", str_replace(".", "", str_replace("+", "", $lancamento['nu_valortotal'])));
                    //salva o lançamento com o valor total dos lançamentos e as datas
                    $resultLancamentoQuitado = $this->salvarLancamento(array(
                        'id_lancamento' => $lancamento['id_lancamento'],
                        'nu_valor' => $valor,
                        'dt_quitado' => $lancamento['dt_quitado'],
                        'bl_quitado' => $lancamento['bl_quitado']
                    ));

                    //se não retornar um lancamento cria uma excessão
                    if (!$resultLancamentoQuitado) {
                        throw new \Exception("Erro ao salvar Lançamento quitado.");
                    }

                    //atribui o objeto do lançamento ao array de retorno, se chegou até aqui é porque não morreu em nenhuma exceção
                    $arrReturn[] = $this->toArrayEntity($resultLancamentoQuitado);

                }
            }
            $this->commit();//commit
            return $arrReturn;//retorna o array

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Houve um erro ao tentar salvar array de lançamentos. " . $e->getMessage());
        }
    }

    /**
     * Executa uma consulta para retornar os usuários que estão aptos a serem autorizados para pagamento.
     * @param $params
     * @return array
     */
    public function retornaUsuariosAutorizacao($params)
    {
        //pega o primeiro e ultimo dia do mes.
        $month_end = strtotime('last day of -1 month', time());

        //Busca no repository os laçamentos a serem autorizados.
        $autorizacoes = $this->em->getRepository('\G2\Entity\ComissaoLancamento')
            ->retornaAutorizacoesPagamento(array(
                'id_entidade' => $this->sessao->id_entidade,
                ////Descomentar
                'dt_inicial' => date('Y-m-d') . ' 00:00:00',
                'dt_final' => date('Y-m-d', $month_end) . ' 23:59:59',
                'st_cpf' => $params['st_cpf'] ? $this->limpaCpfCnpj($params['st_cpf']) : null,
                'st_nomecompleto' => $params['st_nomecompleto'] ? $params['st_nomecompleto'] : null,


                //PARA TESTE
//                'dt_inicial' => '2013-10-01 00:00:00',
//                'dt_final' => '2015-10-31 23:59:59'
                //---------------
            ));

        //Para cada autorizacao retorna um valor formatado para a tabela.
        $arrayReturn = array();
        foreach ($autorizacoes as $key => $value) {
            $arrayReturn[$key] = $value;
            //para teste
//            $arrayReturn[$key]['nu_valor'] = (float)'33.02';
//            $arrayReturn[$key]['nu_valorMostrar'] = number_format((float)'33.02', 2, ",", ".");
            //--------------
            //Descomentar
            $arrayReturn[$key]['nu_valorMostrar'] = number_format((float)$value['nu_valor'], 2, ",", ".");
        }
        return $arrayReturn;
    }

    /**
     * Efetua a Autorização de Pagamento, salva o lançamento na tb_lancamento e salva na tb_comissaolancamento
     * para cada id_vendaproduto que gera aquele lançamento um registro contendo o id_venda e o id_lançamento
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function efetuaAutorizacaoPagamento($params)
    {
        try {
            //Pega o primeiro dia do mes e o ultimo
            $month_end = strtotime('last day of  -1 month', time());
            //Descomentar
            $data['dt_final'] = date('Y-m-d', $month_end) . ' 23:59:59';
            $mensageiro = new \Ead1_Mensageiro();
            $arrayResult = array();
            $this->beginTransaction();
            foreach ($params as $value) {
                //cria o registro para cada usuário selecionado na autorizacao de pagamento
                $lancamentoResult = $this->salvarLancamento($value);

                if (!$lancamentoResult) {
                    throw new \Exception("Erro ao salvar lançamento para autorização.");
                } else {
                    $mensageiro->addMensagem("Lançamento salvo com sucesso.", \Ead1_IMensageiro::SUCESSO);
                }

                $resultado = $this->toArrayEntity($lancamentoResult);

                $resultProcesso = $this->em->getRepository('\G2\Entity\ComissaoLancamento')
                    ->processarAutorizacoesdePagamentoLote(array(
                        'id_usuario' => $value['id_usuario'],
                        'id_entidade' => $this->sessao->id_entidade,
                        'dt_final' => $data['dt_final']
                    ), $resultado['id_lancamento']);

                if (!$resultProcesso) {
                    throw new \Exception("Erro ao inserir dados referentes aos lançamentos da comissão.");
                } else {
                    $mensageiro->addMensagem("Lançamento da comissao salvos com sucesso.", \Ead1_IMensageiro::SUCESSO);
                }

            }
            $this->commit();
            return $mensageiro;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Houve um erro ao tentar salvar a comissao lancamento. " . $e->getMessage());
        }
    }

    /**
     * Retorna lista de pagamentos efetuados ao colaborador.
     * @param array $params Parametros para busca, chave valor, esperado até 4 parametros
     * ano, mes, id_usuario, id_entidade, sendo que id_entidade não é obrigatório, pois pode ser o da sessao.
     * @return mixed
     */
    public function retornaListaPagamentosEfetuados(array $params = [])
    {

        $periodo = $this->getPeriodoExtratoColaborador($params['ano'], $params['mes']);
        $where['dt_inicio'] = $periodo['dt_inicio'];
        $where['dt_termino'] = $periodo['dt_fim'];
        $where['id_usuario'] = $params['id_usuario'];
        $where['id_entidade'] = array_key_exists('id_entidade', $params) && !empty($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;

        $result = $this->em->getRepository('\G2\Entity\ComissaoLancamento')->retornaListaPagamentosEfetuados($where);

        //verifica se tem registro
        if ($result) {
            //percorre os registros
            foreach ($result as $key => $row) {
                //reescreve a posição da data do array e insere o valor formatado
                $result[$key]['dt_quitado'] = $row['dt_quitado']->format('d/m/Y');
            }
        }

        return $result;

    }

    /**
     * Retornar dados dos lançamentos autorizados
     * @param array $params chave=>valor
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarLancamentosAutorizados(array $params = [])
    {
        try {
            //verifica se foi passado parametros
            if ($params) {
                //percorre os parametros
                foreach ($params as $key => $value) {
                    //verifica se não tem valor e remove a posição do array
                    if (!$value)
                        unset($params[$key]);
                }
            }

            //verifica se existe uma chave chamada id_entidade e pega o valor da sessao se não existir
            $params['id_entidade'] = array_key_exists('id_entidade', $params) ? $params['id_entidade'] : $this->sessao->id_entidade;

            //chama o metodo da camada de repository
            $result = $this->em->getRepository('\G2\Entity\ComissaoLancamento')->retornarLancamentosAutorizados($params);
            $arrReturn = array();
            if ($result) {//verifica o resultado da repository e percorre os dados
                foreach ($result as $row) {
                    //cria uma posição no array com os dados basicos que se repetem
                    if (!isset($arrReturn[$row['id_lancamento']])) {
                        $arrReturn[$row['id_lancamento']] = array(
                            'id_usuario' => $row['id_usuario'],
                            'st_nomecompleto' => $row['st_nomecompleto'],
                            'st_cpf' => $row['st_cpf'],
                            'id_lancamento' => $row['id_lancamento'],
                            'nu_valortotal' => $row['nu_valor'],
                            'dt_quitado' => NULL,
                            'bl_quitado' => false
                        );
                    }
                }
            }

            //re-ordena o array
            sort($arrReturn);

            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar montar dados lançamentos autorizados. " . $e->getMessage());
        }
    }
}

<?php

namespace G2\Negocio;

use G2\Constante\Sistema;
//use G2\Entity\EntidadeIntegracao; // VERIFICAR SE AO RETIRAR ISTO, HÁ ALGUM IMPACTO EM OUTRAS FUNCIONALIDADES
use G2\Entity\EntidadeRelacao;
use G2\Entity\Usuario;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;

/**
 * Classe de negócio para Entidade
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Entidade extends Negocio
{

    private $repositoryName = 'G2\Entity\Entidade';
    private $entidadeBo;

    public function __construct()
    {
        parent::__construct();
        $this->entidadeBo = new \EntidadeBO();
    }

    public function getReference($idEntidade, $repositoryName = null)
    {
        return parent::getReference($this->repositoryName, $idEntidade);
    }

    /**
     * Find Entidade
     * @param integer $id
     */
    public function findEntidade($id, $retornoArray = false)
    {
        $result = $this->find($this->repositoryName, $id);


        if ($retornoArray) {
            $result = $this->toArrayEntity($result);
        }
        return $result;
    }

    /**
     * Find Entidade Relação
     * @param integer $id
     */
    public function findEntidadeRelacao($id)
    {
        $result = $this->findOneBy('G2\Entity\EntidadeRelacao', array('id_entidade' => $id));

        if ($result) {
            return $this->toArrayEntity($result);
        }
    }

    /**
     * Retorna Entidade de acordo com os parametros passados
     * @param array $params
     * @author Denise Xavier <denise.xavier@unyleya.com.br>
     * @return mixed array
     */
    public function retornaPesquisarEntidade(array $params)
    {
        $repo = $this->em->getRepository($this->repositoryName);
        //se existir um parametro chamado str ele reescreve
        if (isset($params['str']) && !empty($params['str'])) {
            $params['st_razaosocial'] = $params['str'];
            $params['st_cnpj'] = $params['str'];
        }
        //Comunica com o repository e executa o querybuilder
        $result = $repo->pesquisaEntidade($params);
        //Verifica o resultado
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Listar Entidades Filhas
     * @param array $params
     * @throws \Zend_Exception
     */
    public function listarUnidades(array $params)
    {

        try {
            $resultEntidade = $this->findBy($this->repositoryName, $params);
            $arrReturn = array();

            if ($resultEntidade) {
                foreach ($resultEntidade as $i => $entidade) {
                    $arrReturn[$i]['id_entidadematriz'] = $entidade->getId_entidadecadastro()->getId_entidade();
                    $arrReturn[$i]['id_entidade'] = $entidade->getId_entidade();
                    $arrReturn[$i]['st_nomeentidade'] = $entidade->getSt_nomeentidade();
                    $arrReturn[$i]['st_siglaentidade'] = $entidade->getSt_siglaentidade();
                    $arrReturn[$i]['st_razaosocial'] = $entidade->getSt_razaosocial();
                    $arrReturn[$i]['st_urlsite'] = $entidade->getSt_urlsite();
                    //Find Endereço
                    $resultEndEntidade = $this->findOneEntidadeEndereco(array('id_entidade' => $entidade->getId_entidade()));
                    if ($resultEndEntidade) {
                        foreach ($resultEndEntidade as $endereco) {
                            $arrReturn[$i]['st_endereco'] = $endereco->getSt_endereco();
                            $arrReturn[$i]['st_complemento'] = $endereco->getSt_complemento();
                            $arrReturn[$i]['nu_numero'] = $endereco->getNu_numero();
                            $arrReturn[$i]['st_cep'] = $endereco->getSt_cep();
                            $arrReturn[$i]['st_bairro'] = $endereco->getSt_bairro();
                            $arrReturn[$i]['st_nomemunicipio'] = $endereco->getSt_nomemunicipio();
                            $arrReturn[$i]['sg_uf'] = $endereco->getSg_uf();
                            $arrReturn[$i]['st_nomepais'] = $endereco->getSt_nomepais();
                        }
                    }

                    //Find Telefone
                    $resultTelefone = $this->findOneTelefoneEntidade(array(
                        'id_entidade' => $entidade->getId_entidade(),
                        'bl_padrao' => true
                    ));
                    if ($resultTelefone) {
                        foreach ($resultTelefone as $telefone) {
                            $arrReturn[$i]['nu_ddi'] = $telefone->getNu_ddi();
                            $arrReturn[$i]['nu_ddd'] = $telefone->getNu_ddd();
                            $arrReturn[$i]['nu_telefone'] = $telefone->getNu_telefone();
                        }
                    }
                }
            }
            return $arrReturn;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Find Entidade Endereço
     * @param array $params
     * @return \G2\Entity\EntidadedeEndereco
     * @throws \Zend_Exception
     */
    public function findOneEntidadeEndereco(array $params = array())
    {
        try {
            //Find Endereço Entidade
            return $this->em->getRepository('\G2\Entity\VwEntidadeEndereco')->findOneBy($params);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * @param array $params
     * @return '\G2\Entity\VwEntidadeTelefone'
     * @throws \Zend_Exception
     */
    public function findOneTelefoneEntidade(array $params = array())
    {
        try {
            //Find Telefone Entidade
            return $this->em->getRepository('\G2\Entity\VwEntidadeTelefone')->findOneBy($params);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Retorna dados da entidade pai passando o id da entidade
     * @param $idEntidade integer Id da entidade
     * @return \G2\Entity\Entidade
     * @throws \Zend_Exception
     */
    public function retornaEntidadePai($idEntidade)
    {
        try {
            $result = $this->find($this->repositoryName, $idEntidade);
            if ($result) {
                return $result->getId_entidadecadastro();
            }
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Salva os dados da entidade em pessoa jurídica com log.
     * @param $dataEntidade - Dados da tb_entidade.
     * @param $dataEndereco - Dados da tb_endereco.
     * @param $bl_relacionarentidade
     * @return array
     * @throws \Zend_Exception
     */

    public function salvarEntidadePessoaJuridica(array $dataEntidade, array $dataEndereco, $bl_relacionarentidade = null)
    {
        try {
            $this->beginTransaction();

            if ($dataEntidade['id_entidade']) {
                $entidade = $this->find('\G2\Entity\Entidade', $dataEntidade['id_entidade']);
            } elseif (!isset($entidade)) {
                $entidade = new \G2\Entity\Entidade();
                $entidade->setDt_cadastro(new \DateTime());
                $entidade->setId_usuariocadastro(parent::getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $entidade->setId_entidadecadastro($this->getReference($this->getId_entidade()));
            }

            try {
                if (isset($dataEntidade['id_esquemaconfiguracao']) && $dataEntidade['id_esquemaconfiguracao'] != '') {
                    $entidade->setId_esquemaconfiguracao(parent::getReference('\G2\Entity\EsquemaConfiguracao',
                        $dataEntidade['id_esquemaconfiguracao']));
                } else {
                    $entidade->setId_esquemaconfiguracao(null);
                }

                if (isset($dataEntidade['id_esquemapergunta']) && !empty($dataEntidade['id_esquemapergunta'])) {
                    $entidade->setId_esquemapergunta(parent::getReference('\G2\Entity\EsquemaPergunta',
                        $dataEntidade['id_esquemapergunta']));
                } elseif (empty($dataEntidade['id_esquemapergunta'])) {
                    $entidade->setId_esquemapergunta(null);
                }

                $entidade->setBl_ativo(array_key_exists('bl_ativo',
                    $dataEntidade) && !empty($dataEntidade['bl_ativo']) ? $dataEntidade['bl_ativo'] : true);
                $entidade->setSt_nomeentidade($dataEntidade['st_nomeentidade']);
                $entidade->setSt_wschave($this->newid($dataEntidade['st_nomeentidade']));
                $entidade->setSt_urlimglogo($dataEntidade['st_urlimglogo']);
                $entidade->setBl_acessasistema($dataEntidade['bl_acessasistema']);
                $entidade->setNu_cnpj($dataEntidade['nu_cnpj']);
                $entidade->setSt_razaosocial($dataEntidade['st_razaosocial']);
                $entidade->setNu_inscricaoestadual($dataEntidade['nu_inscricaoestadual']);
                $entidade->setSt_urlsite($dataEntidade['st_urlsite']);
                $entidade->setSt_cnpj($dataEntidade['st_cnpj']);

                if (array_key_exists('id_situacao', $dataEntidade) && !empty($dataEntidade['id_situacao'])) {
                    $entidade->setId_situacao(parent::getReference('\G2\Entity\Situacao', $dataEntidade['id_situacao']));
                } else {
                    throw new \Exception("Informe a situação da entidade.");
                }

                $entidade->setSt_siglaentidade($dataEntidade['st_siglaentidade']);
                $entidade->setSt_urlportal((isset($dataEntidade['st_urlportal'])) ?
                    $dataEntidade['st_urlportal'] : null);
                $entidade->setSt_urlnovoportal((isset($dataEntidade['st_urlnovoportal'])) ?
                    $dataEntidade['st_urlnovoportal'] : null);
                $entidade->setSt_apelido($dataEntidade['st_apelido']);
                $entidade->setSt_urllogoutportal($dataEntidade['st_urllogoutportal']);
                $entidade->setStr_urlimglogoentidade(isset($dataEntidade['str_urlimglogoentidade']) ?
                    $dataEntidade['str_urlimglogoentidade'] : null);
                $entidade->setId_uploadloja(isset($dataEntidade['id_uploadloja']) ?
                    $dataEntidade['id_uploadloja'] : null);
                $entidade->setSt_urlimgapp(isset($dataEntidade['st_urlimgapp']) ?
                    $dataEntidade['st_urlimgapp'] : null);

                $dataEntidade["id_usuariosecretariado"]
                    ? $entidade->setId_usuariosecretariado(parent::getReference("G2\Entity\Usuario",
                        $dataEntidade["id_usuariosecretariado"]))
                    : $entidade->setId_usuariosecretariado(null);

                // Salva a entidade.
                $retornoEntidade = $this->save($entidade);

            } catch (\Exception $e) {
                THROW new \Zend_Exception('Erro ao salvar a entidade. ' . $e->getMessage());
            }

            try {
                if ($dataEndereco['id_endereco']) {
                    $endereco = $this->find('\G2\Entity\Endereco', $dataEndereco['id_endereco']);
                } elseif (!isset($endereco)) {
                    $endereco = new \G2\Entity\Endereco();
                }

                $endereco->setSg_uf($dataEndereco['sg_uf']);
                $endereco->setSt_cep($dataEndereco['st_cep']);
                $endereco->setSt_endereco($dataEndereco['st_endereco']);
                $endereco->setSt_bairro($dataEndereco['st_bairro']);
                $endereco->setSt_complemento($dataEndereco['st_complemento']);
                $endereco->setNu_numero($dataEndereco['nu_numero']);
                $endereco->setSt_cidade($dataEndereco['st_cidade']);
                $endereco->setBl_ativo($dataEndereco['bl_ativo']);
                $endereco->setId_pais($this->find('\G2\Entity\Pais', $dataEndereco['id_pais']));
                $endereco->setId_municipio($this->find('G2\Entity\Municipio', $dataEndereco['id_municipio']));
                $endereco->setId_tipoendereco($this->find('G2\Entity\TipoEndereco', $dataEndereco['id_tipoendereco']));

                //Salva o endereço.
                $retornoEndereco = $this->save($endereco);

            } catch (\Exception $e) {
                THROW new \Zend_Exception('Erro ao salvar o endereço da entidade. ' . $e->getMessage());
            }

            $retornoEntidadeEndereco = $this->findOneBy('\G2\Entity\EntidadedeEndereco', array(
                    'id_endereco' => $retornoEndereco->getId_endereco(),
                    'id_entidade' => $retornoEntidade->getId_entidade()
                )
            );

            if ($retornoEndereco != null && $retornoEntidadeEndereco == null) {

                $entidadeEndereco = new \G2\Entity\EntidadedeEndereco();

                try {
                    $entidadeEndereco->setBl_padrao(true);
                    $entidadeEndereco->setId_endereco(parent::getReference('\G2\Entity\Endereco',
                        $retornoEndereco->getId_endereco()));
                    $entidadeEndereco->setId_entidade($retornoEntidade->getId_entidade());
                    $retornoEntidadeEndereco = $this->save($entidadeEndereco);
                } catch (\Exception $e) {
                    THROW new \Zend_Exception('Erro ao relacionar a entidade ao endereço. ' . $e->getMessage());
                }
            }

            $relacao = $this->findBy('G2\Entity\EntidadeRelacao', array(
                'id_entidade' => $retornoEntidade->getId_entidade(),
                'id_entidadepai' => $this->sessao->id_entidade
            ));
            $resultRelacao = $this->entitySerialize($relacao);

            if ($bl_relacionarentidade != 0 && sizeof($resultRelacao) == 0) {
                $entidadeClasse = parent::getReference('G2\Entity\EntidadeClasse', '1');

                $relacaoEntidade = new EntidadeRelacao();
                $relacaoEntidade->setId_entidade($retornoEntidade->getId_entidade())
                    ->setId_entidadepai($this->sessao->id_entidade)
                    ->setId_usuariocadastro($this->sessao->id_usuario)
                    ->setId_entidadeclasse($entidadeClasse);

                $this->save($relacaoEntidade);
            }

            $resultArr = array(
                'tipo' => \Ead1_IMensageiro::SUCESSO,
                'retorno' => array(
                    'entidade' => $retornoEntidade->toArray(),
                    'endereco' => $this->toArrayEntity($retornoEndereco),
                    'entidadeendereco' => $retornoEntidadeEndereco
                )
            );

            $this->commit();

            return $resultArr;
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function salvarContaEntidade(array $dadosConta)
    {
        $entidadeConta = new \ContaEntidadeTO();

        foreach ($dadosConta as $k => $v) {
            $entidadeConta->$k = $v;
        }
        $entidadeConta->setBl_padrao(true);

        return $this->entidadeBo->procedimentoSalvarContaEntidade($entidadeConta);
    }

    public function processarEntidadeFinanceiro(array $dadosEntidadeFinanceiro = null)
    {
        try {
            $this->beginTransaction();

            if (isset($dadosEntidadeFinanceiro['id_entidadefinanceiro']) && $dadosEntidadeFinanceiro['id_entidadefinanceiro'] != null) {
                $entidadeFinanceiro = $this->find('\G2\Entity\EntidadeFinanceiro',
                    $dadosEntidadeFinanceiro['id_entidadefinanceiro']);
            } else {
                $entidadeFinanceiro = new \G2\Entity\EntidadeFinanceiro();
            }

            $entidadeFinanceiro->setNu_diaspagamentoentrada($dadosEntidadeFinanceiro['nu_diaspagamentoentrada'])
                ->setBl_protocolo(0)
                ->setId_entidade($dadosEntidadeFinanceiro['id_entidade']);

            $result = $this->save($entidadeFinanceiro);

            $arrayResult = array();
            if ($result) {
                $arrayResult = array(
                    'id_entidadefinanceiro' => $result->getId_entidadefinanceiro(),
                    'id_entidade' => $result->getId_entidade(),
                    'bl_automatricula' => $result->getBl_automatricula(),
                    'id_textosistemarecibo' => $result->getId_textosistemarecibo(),
                    'bl_protocolo' => $result->getBl_protocolo(),
                    'st_linkloja' => $result->getSt_linkloja(),
                    'nu_avisoatraso' => $result->getNu_avisoatraso(),
                    'id_textoavisoatraso' => $result->getId_textoavisoatraso(),
                    'nu_diaspagamentoentrada' => $result->getNu_diaspagamentoentrada(),
                    'id_reciboconsolidado' => $result->getId_reciboconsolidado()
                );
            }
            $this->commit();
            return $arrayResult;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Salva dados de cartão entidade
     * @param integer $id_entidade
     * @param integer $id_entidadefinanceiro
     * @param array $dadosCartao
     * @param array $dadosEntidadeIntegracao
     * @return boolean
     * @throws \Exception
     */
    public function processarCartaoEntidade($id_entidade, $id_entidadefinanceiro, array $dadosCartao = null)
    {
        try {
            $this->beginTransaction();

            $cartaoOperadora = parent::getReference('G2\Entity\CartaoOperadora', 1);

            /*
             * Salvando os dados do cartão
             * */
            if ($dadosCartao != null) {
                foreach ($dadosCartao as $dC) {
                    $cartao = new \G2\Entity\CartaoConfig();

                    if ($dC['id_cartaoconfig'] != null) {
                        $cartao = $this->findOneBy('G2\Entity\CartaoConfig',
                            array('id_cartaoconfig' => $dC['id_cartaoconfig']));
                    }
                    $sistema = parent::getReference('G2\Entity\Sistema',
                        ($dC['id_sistema'] ? $dC['id_sistema'] : Sistema::CARTAO));
                    $cartaoBandeira = parent::getReference('G2\Entity\CartaoBandeira', $dC['id_cartaobandeira']);
                    $cartao->setId_cartaobandeira($cartaoBandeira)
                        ->setId_sistema($sistema)
                        ->setId_cartaooperadora($cartaoOperadora)
                        ->setId_contaentidade($dC['id_contaentidade']);

                    $resultCartao = $this->save($cartao);
                    if ($resultCartao) {
                        $id_cartaoconfig = $resultCartao->getId_cartaoconfig();

                        $findObj = $this->findBy('\G2\Entity\EntidadeCartaoConfig', array(
                            'id_cartaoconfig' => $id_cartaoconfig,
                            'id_entidadefinanceiro' => $id_entidadefinanceiro
                        ));

                        if (sizeof($findObj) == 0) {
                            $entidadeCartao = new \G2\Entity\EntidadeCartaoConfig();
                            $entidadeCartao->setId_cartaoconfig($id_cartaoconfig)
                                ->setId_entidadefinanceiro($id_entidadefinanceiro);

                            $this->save($entidadeCartao);
                        }
                    }
                }

                $this->commit();

                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Processa o boleto, salva na tb_entidadeintegracao caso não exista.
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function processarBoleto($params)
    {
        try {
            parent::beginTransaction();

            $result = null;
            $entidadeIntegracaoObj = new \G2\Entity\EntidadeIntegracao();
            if (array_key_exists('id_entidadeintegracao', $params) && $params['id_entidadeintegracao']) {
                $entidadeIntegracaoObj = parent::find('\G2\Entity\EntidadeIntegracao',
                    $params['id_entidadeintegracao']);
            } else {
                $entidadeIntegracaoObj->setDt_cadastro(new \DateTime());
                $entidadeIntegracaoObj->setId_usuariocadastro($this->sessao->id_usuario);
                $entidadeIntegracaoObj->setId_entidade($params['id_entidade']);
                $entidadeIntegracaoObj->setId_sistema(\G2\Constante\Sistema::PAGARME);

            }
            $entidadeIntegracaoObj->setSt_codsistema($params['st_codsistema']); //recomendação do felipe
            $entidadeIntegracaoObj->setSt_codchave($params['st_codchave']);
            $result = parent::save($entidadeIntegracaoObj);

            parent::commit();

            if ($result) {
                return new \Ead1_Mensageiro(parent::toArrayEntity($result), \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro("Não foi possível salvar os dados do boleto na tb_entidadeintegracao",
                    \Ead1_IMensageiro::ERRO);
            }

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }
    }

    public function retornaEntidadeUsuario($params)
    {

        try {
            $result = $this->find('G2\Entity\VwEntidadeUsuarioTutorial', $params);
            if ($result) {
                $arrayResult = array(
                    'id_usuario' => $result->getId_usuario(),
                    'id_entidade' => $result->getId_entidade(),
                    'bl_tutorialaluno' => $result->getBl_tutorialaluno(),
                    'bl_tutorialentidade' => $result->getBl_tutorialentidade()
                );
                return $arrayResult;
            }
            return false;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    public function salvarLayoutG2SPortal($arquivo, $id_entidade, $pathPortal)
    {

        try {
            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);


            if ($tipo == 'png') {
                $novo_nome = $id_entidade . '.' . $tipo;
                $objEntidade = $this->findOneBy($this->repositoryName, array('id_entidade' => $id_entidade));
                $objEntidade->setSt_urlimglogo('/upload/entidade/' . $novo_nome);

                $result = $this->merge($objEntidade);

                /*
                 * Após salvar no banco iremos mover o arquivo de lugar para a pasta correta
                 * */
                if ($result) {
                    $uploadPathG2S = realpath(APPLICATION_PATH . "/../public/upload/entidade");

                    if (move_uploaded_file($arquivo['tmp_name'], $uploadPathG2S . '/' . $novo_nome)) {
                        /*
                         * Sobrescrevo a imagem adicionada para o tamanho correto
                         * */
                        $imagine = new Imagine();
                        $imagine->open($uploadPathG2S . '/' . $novo_nome)
                            ->resize(new Box(130, 54))
                            ->save($uploadPathG2S . '/' . $novo_nome)
                            ->save($pathPortal . '/logo_padrao.png');

                        chmod($pathPortal . '/logo_padrao.png', 0777);

                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function salvarLayoutG2SPortalEntidade($arquivo, $id_entidade, $pathPortal)
    {

        try {
            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);

            if ($tipo == 'png') {
                $novo_nome = $id_entidade . '_2.' . $tipo;
                $objEntidade = $this->findOneBy($this->repositoryName, array('id_entidade' => $id_entidade));
                $objEntidade->setStr_urlimglogoentidade('/upload/entidade/' . $novo_nome);

                $result = $this->merge($objEntidade);

                /*
                 * Após salvar no banco iremos mover o arquivo de lugar para a pasta correta
                 * */
                if ($result) {
                    $uploadPathG2S = realpath(APPLICATION_PATH . "/../public/upload/entidade");

                    if (move_uploaded_file($arquivo['tmp_name'], $uploadPathG2S . '/' . $novo_nome)) {
                        /*
                         * Sobrescrevo a imagem adicionada para o tamanho correto
                         * */
                        $imagine = new Imagine();
                        $imagine->open($uploadPathG2S . '/' . $novo_nome)
                            ->resize(new Box(130, 54))
                            ->save($uploadPathG2S . '/' . $novo_nome)
                            ->save($pathPortal . '/logo_topo.png');

                        chmod($pathPortal . '/logo_topo.png', 0777);

                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function salvarLayoutPortalLoja($arquivo, $id_entidade, $pathPortal)
    {
        try {
            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);

            if ($tipo == 'png') {
                $novo_nome = $id_entidade . '.' . $tipo;

                $uploadPathLoja = realpath(APPLICATION_PATH . "/../public/loja/layout/padrao/img/layout");

                if (move_uploaded_file($arquivo['tmp_name'], $uploadPathLoja . '/' . $novo_nome)) {
                    /*
                     * Sobrescrevo a imagem adicionada para o tamanho correto
                     * */
                    $imagine = new Imagine();
                    $imagine->open($uploadPathLoja . '/' . $novo_nome)
                        ->resize(new Box(460, 90))
                        ->save($uploadPathLoja . '/' . $novo_nome)
                        ->save($pathPortal . '/logo_grande.png');

                    chmod($pathPortal . '/logo_grande.png', 0777);

                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Exception $e) {
            THROW new \Exception($e->getMessage());
        }
    }

    public function salvarLayoutPortalCss($arquivo, $id_entidade)
    {
        try {

            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);

            if ($tipo == 'css') {
                $novo_nome = $id_entidade . '.css';

                $uploadPathCssPortal = realpath(APPLICATION_PATH . "/../public/portaldoaluno/css/cssnovoportal/skins");

                if (move_uploaded_file($arquivo['tmp_name'], $uploadPathCssPortal . '/' . $novo_nome)) {
                    chmod($uploadPathCssPortal . '/' . $novo_nome, 0777);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function salvarLayoutLojaCss($arquivo, $id_entidade)
    {
        try {

            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);

            if ($tipo == 'css') {
                $novo_nome = $id_entidade . '.css';

                $objetoEntidade = $this->find('\G2\Entity\Entidade', $id_entidade);
                if (!$objetoEntidade->getId_uploadCssLoja()) {
                    $objeto = new \G2\Entity\Upload();
                    $objeto->setSt_upload($novo_nome);
                    $this->save($objeto);
                    $objetoEntidade->setId_uploadCssLoja($objeto->getId_upload());
                    $this->save($objetoEntidade);
                }

                $uploadPathLojaCss = realpath(APPLICATION_PATH . "/../public/loja/assets/stylesheets");

                if (move_uploaded_file($arquivo['tmp_name'], $uploadPathLojaCss . '/' . $novo_nome)) {
                    chmod($uploadPathLojaCss . '/' . $novo_nome, 0777);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function salvarFaveIcon($arquivo, $id_entidade)
    {
        try {

            $nome = $arquivo['name'];
            $tipo = pathinfo($nome, PATHINFO_EXTENSION);

            if ($tipo == 'ico') {
                $novo_nome = $id_entidade . '.ico';

                $objetoEntidade = $this->find('\G2\Entity\Entidade', $id_entidade);
                if (!$objetoEntidade->getId_uploadloja()) {
                    $objeto = new \G2\Entity\Upload();
                    $objeto->setSt_upload($novo_nome);
                    $this->save($objeto);
                    $objetoEntidade->setId_uploadloja($objeto->getId_upload());
                    $this->save($objetoEntidade);
                }

                $uploadPathCssPortal = realpath(APPLICATION_PATH . "/../public/loja/layout/padrao/img/layout/favicons");

                if (move_uploaded_file($arquivo['tmp_name'], $uploadPathCssPortal . '/' . $novo_nome)) {
                    chmod($uploadPathCssPortal . '/' . $novo_nome, 0777);
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function verificarLayoutEntidade($id_entidade, $uploadImagem)
    {
        try {
            $retorno = array(
                'bl_cssportal' => '',
                'bl_logopadrao' => '',
                'bl_logogrande' => '',
                'bl_urlimglogoentidade' => '',
                'bl_favicon' => '',
                'st_urlimgapp' => ''
            );

            $uploadPathLoja = realpath(APPLICATION_PATH . "/../public/loja/layout/padrao/img/layout");
            $uploadPathLogoEntidade = realpath(APPLICATION_PATH . "/../public");

            $uploadPathFaviconLoja = realpath(APPLICATION_PATH . "/../public/loja/layout/padrao/img/layout/favicons");

            $objEntidade = $this->findOneBy($this->repositoryName, array('id_entidade' => $id_entidade));

            if ($objEntidade) {
                if ($objEntidade->getSt_urlimglogo() != null && file_exists($uploadImagem . '/logo_padrao.png')) {
                    $retorno['bl_logopadrao'] = true;
                }

                if ($objEntidade->getStr_urlimglogoentidade() != null && file_exists($uploadPathLogoEntidade . $objEntidade->getStr_urlimglogoentidade())) {
                    $retorno['bl_urlimglogoentidade'] = true;
                }

                if (file_exists($uploadImagem . '/logo_grande.png') && (file_exists($uploadPathLoja . '/' . $id_entidade . '.jpg') || file_exists($uploadPathLoja . '/' . $id_entidade . '.png'))) {
                    $retorno['bl_logogrande'] = true;
                }

                if (file_exists($uploadPathFaviconLoja . '/' . $id_entidade . '.ico')) {
                    $retorno['bl_favicon'] = true;
                }

                if (!is_null($objEntidade->getSt_urlimgapp()) && file_exists(realpath(APPLICATION_PATH . "/../public") . $objEntidade->getSt_urlimgapp())) {
                    $retorno['st_urlimgapp'] = $objEntidade->getSt_urlimgapp();
                }
            }

            return $retorno;
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }

    public function verificarCssLayout($id_entidade)
    {
        try {
            $retorno = array(
                'bl_cssportal' => ''
            );

            $uploadPathCssPortal = realpath(APPLICATION_PATH . "/../public/portaldoaluno/css/cssnovoportal/skins");
            if (file_exists($uploadPathCssPortal . '/' . $id_entidade . '.css')) {
                $retorno['bl_cssportal'] = true;
            }

            $objetoEntidadeConfiguracao = $this->findOneBy('\G2\Entity\ConfiguracaoEntidade',
                array('id_entidade' => $id_entidade));
            if ($objetoEntidadeConfiguracao) {
                $retorno['st_corprimaria'] = $objetoEntidadeConfiguracao->getSt_corprimarialoja();
                $retorno['st_corsecundaria'] = $objetoEntidadeConfiguracao->getSt_corsecundarialoja();
                $retorno['st_corterciaria'] = $objetoEntidadeConfiguracao->getSt_corterciarialoja();
            }

            return $retorno;
        } catch (\Zend_Exception $e) {
            THROW new \Zend_Exception($e->getMessage());
        }
    }


    /**
     * Retorna array com dados das entidades
     * @param $st_consulta string para consutar
     * @return array retorna array de array com dados da entidade
     * @throws \Zend_Exception
     */
    public function retornaEntidadeAutocomplete($st_consulta)
    {
        try {
            $arrayParams = array();
            if ($st_consulta) {
                $arrayParams['grid'] = json_encode(" * ");
                $arrayParams['st_nomeentidade'] = $st_consulta;
                $arrayParams['st_razaosocial'] = $st_consulta;
                $arrayParams['st_cnpj'] = $st_consulta;

                $bo = new \PesquisarBO();
                $result = $bo->retornaPesquisaPessoaJuridica($arrayParams);
                if ($result) {
                    foreach ($result as $i => $row) {
                        $result[$i] = $this->toArrayEntity($row);
                        $result[$i]['id'] = $row->getId_entidade();
                        $result[$i]['id_situacao'] = $row->getId_situacao() ? $row->getId_situacao()->getId_situacao() : null;
                        $result[$i]['id_entidadecadastro'] = $row->getId_entidadecadastro() ? $row->getId_entidadecadastro()->getId_entidade() : null;
                        $result[$i]['id_usuariocadastro'] = $row->getId_usuariocadastro() ? $row->getId_usuariocadastro()->getId_usuario() : null;
                    }
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar entidade . " . $e->getMessage());
        }
    }

    /**
     * Método para retornar dados dos cartões configurados
     * @param array $where
     * @throws \Zend_Exception
     */
    public function retornaVwEntidadeCartao(array $where = array())
    {
        try {
            $where['id_entidade'] = (array_key_exists('id_entidade',
                    $where) && !empty($where['id_entidade'])) ? $where['id_entidade'] : $this->sessao->id_entidade;
            return $this->findBy('\G2\Entity\VwEntidadeCartao', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados . " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_entidaderecursivaid
     * @param integer $id passsar como parametro o id_entidade que deseja buscar a sua recursividade,
     * se esse valor não for passado ele assumidar o valor da entidade da sessão
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarVwEntidadeRecursaId($id = null)
    {
        try {
            $id = $id ? $id : $this->sessao->id_entidade;
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($id);
            return $result;
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da entidade recursiva . " . $ex->getMessage());
        }
    }


    public function retornaUrlLogoutPortal($entidade)
    {
        return $this->find('\G2\Entity\Entidade', $entidade);
    }

    /**
     * Salva imagem do aplicativo no servidor e no banco de dados
     * @param array $file
     * @param integer $idEntidade
     * @return bool
     * @throws \Zend_Exception
     */
    public function salvarImagemAplicativo(array $file, $idEntidade)
    {
        try {
            if ($file) {
                $entidade = $this->find($this->repositoryName, $idEntidade);

                //apaga a imagem antiga do diretorio
                if (!empty($entidade->getSt_urlimgapp()) && file_exists(realpath(APPLICATION_PATH . "/../public") . $entidade->getSt_urlimgapp())) {
                    unlink(realpath(APPLICATION_PATH . "/../public") . $entidade->getSt_urlimgapp());
                }

                $uploadPath = realpath(APPLICATION_PATH . "/../public/upload/entidade");
                $hashImg = md5(uniqid() . $idEntidade);
                $fileName = $hashImg . '.png';//cria um md5 com o nome da imagem

                //verifica o tipo da imagem
                if (!preg_match("/^image\/(png)$/", $file["type"])) {
                    throw new \Exception("O formato da imagem enviada não é suportado.");
                }

                if (move_uploaded_file($file['tmp_name'], $uploadPath . '/' . $fileName)) {
                    // Sobrescrevo a imagem adicionada para o tamanho correto
                    $imagine = new Imagine();
                    $imagine->open($uploadPath . '/' . $fileName)
                        ->resize(new Box(100, 100))
                        ->save($uploadPath . '/' . $fileName);

                    chmod($uploadPath . '/' . $fileName, 0777);

                    //seta o novo nome da imagem na entity
                    $entidade->setSt_urlimgapp('/upload/entidade/' . $fileName);
                    //tenta salvar
                    if (!$this->save($entidade)) {
                        throw new \Exception("Erro ao tentar salvar nome da imagem no banco de dados.");
                    }

                    return $entidade->getSt_urlimgapp();

                } else {
                    throw new \Exception("Houve um erro ao tentar salvar imagem no servidor.");
                }
            }
            return false;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao fazer upload da imagem do aplicativo. " . $e->getMessage());
        }
    }

    public function newid($nomeentidade)
    {
        try {
            if (empty($nomeentidade)) {
                throw new \Exception('É necessário o nome da entidade para gerar a nova chave do ws.');
            }
            return sha1($nomeentidade, false);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro: " . $e->getMessage());
        }


    }

    public function retornaEsquemaConfiguracao($identidade)
    {
        $result = $this->findOneBy($this->repositoryName, array('id_entidade' => $identidade));
        $result = $this->toArrayEntity($result)['id_esquemaconfiguracao']['id_esquemaconfiguracao'];

        return $result;
    }

    /**
     * Retorna dados da vw_entidadeesquemaconfiguracao
     * @param integer $idEntidade passsar como parametro o id_entidade que deseja buscar a sua configuração,
     * se esse valor não for passado ele assumidar o valor da entidade da sessão
     * @param array $params passar como parâmetro um array de parâmetros para pesquisa, se o ficar vazio, a consulta
     * será por padrão apenas pelo id da entidade
     * @return mixed
     * @throws \Zend_Exception
     * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
     */
    public function retornarVwEntidadeEsquemaConfiguracao($idEntidade = null, $params = array())
    {
        try {
            //Verifica se foi passado por parâmtro um ID de Entidade
            $idEntidade = $idEntidade ? $idEntidade : $this->sessao->id_entidade;

            //Une em um array só e guarda em uma variável para facilitar a consulta no FindBy
            $params = array_merge(['id_entidade' => $idEntidade], $params);

            //Faz a consulta baseada nos parâmetros passados anteriormente
            return $this->findBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', $params);

        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao tentar consultar dados da entidade recursiva . " . $ex->getMessage());
        }
    }

    /**
     * Retorna a linha de negócio da entidade
     * @param string $id_entidade
     * @return string
     * @throws \Exception
     */
    public function retornaLinhaDeNegocio($id_entidade){
        try{
            if(empty($id_entidade)){
                throw new \Exception("Informe a entidade para recuperar a linha de negócio.");
            }

            $linhas = array(2 => 'Graduação', 3 => 'Pós');
            //busca os dados da linha de negócio da entidade logada
            $linhaNegocio = (new \G2\Negocio\EsquemaConfiguracao())
                ->retornaItemPorEntidade(\G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO, $id_entidade);
            if ($linhaNegocio) {
                return $linhas[$linhaNegocio["st_valor"]];
            }else{
                return false;
            }
        }catch (\Exception $e){
            throw  new \Exception($e->getMessage());
        }
    }
}

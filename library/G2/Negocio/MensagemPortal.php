<?php

namespace G2\Negocio;

use G2\Entity\AreaConhecimento;
use G2\Entity\EnvioMensagem;
use G2\Entity\ProjetoPedagogico;
use G2\Entity\TipoDestinatario;
use G2\Entity\TipoEnvio;
use G2\Entity\Turma;
use G2\Entity\VwEnviarMensagemAluno;
use G2\Utils\Helper;

/**
 * Classe de negocio para funcionalidade de Mensagens de agendamento no portal
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2013-09-17
 */
class MensagemPortal extends Negocio
{
    /**
     * titulo(st_mensagem), mensagem(st_texto), id_matricula, dt_envio
     */

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Recupera os dados para Envio de Tcc
     * @param $params
     * @param bool|false $appResponse
     * @return array
     * @throws \Zend_Exception
     */
    public function findVwEnvioTcc($params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwEnvioTcc');
            $query = $repo->createQueryBuilder("vw")
                ->select();

            if (isset($params['id_usuario'])) {
                $query->andWhere('vw.id_usuario = :id_usuario')
                    ->setParameter('id_usuario', $params['id_usuario']);
            }

            if (isset($params['id_matricula'])) {
                $query->andWhere('vw.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            $query->orderBy('vw.id_matricula', 'desc');

            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw $e;
        }

    }


    /**
     * Método que retorna vwEnviarMensagemAluno
     * @param $params
     * @param $appResponse
     * @throws \Zend_Exception
     * @return boolean
     */
    public function findVwEnviarMensagemAluno($params, $appResponse = false)
    {
        //alterar para buscar por entidade e por matricula - Matricula apenas pelo aluno.
        try {
            $repo = $this->em->getRepository('G2\Entity\VwEnviarMensagemAluno');
            $arrColumns = array(
                'vw.id_enviomensagem',
                'vw.id_usuario',
                'vw.st_endereco',
                'vw.id_matricula',
                'vw.id_mensagem',
                'vw.bl_importante',
                'vw.id_tipoenvio',
                'vw.id_evolucao',
                'vw.dt_cadastro',
                'vw.st_mensagem',
                'vw.dt_enviar',
                'vw.st_texto',
                'vw.dt_cadastromsg',
                'vw.id_entidade',
                'vw.id_sistema',
                'vw.id_enviodestinatario'
            );
            $query = $repo->createQueryBuilder("vw")
                ->select($arrColumns);

            if (isset($params['id_enviomensagem'])) {
                $query->andWhere('vw.id_enviomensagem = :id_enviomensagem')
                    ->setParameter('id_enviomensagem', $params['id_enviomensagem']);
            }

            if (isset($params['id_usuario'])) {
                $query->andWhere('vw.id_usuario = :id_usuario')
                    ->setParameter('id_usuario', $params['id_usuario']);
            }

            if (isset($params['id_matricula'])) {
                $query->andWhere('vw.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            if (isset($params['id_mensagem'])) {
                $query->andWhere('vw.id_mensagem = :id_mensagem')
                    ->setParameter('id_mensagem', $params['id_mensagem']);
            }

            if (isset($params['st_mensagem'])) {
                $query->andWhere('vw.st_mensagem LIKE :st_mensagem')
                    ->setParameter('st_mensagem', '%' . $params['st_mensagem'] . '%');
            }

            if (isset($params['st_texto'])) {
                $query->andWhere('vw.st_texto LIKE :st_texto')
                    ->setParameter('st_texto', '%' . $params['st_texto'] . '%');
            }

            if (isset($params['id_matricula'])) {
                $query->andWhere('vw.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            if (isset($params['id_evolucao'])) {
                $query->andWhere('vw.id_evolucao = :id_evolucao')
                    ->setParameter('id_evolucao', $params['id_evolucao']);
            }

            if (isset($params['bl_importante'])) {
                $query->andWhere('vw.bl_importante = :bl_importante')
                    ->setParameter('bl_importante', $params['bl_importante']);
            }

            if (isset($params['dt_enviar'])) {
                $query->andWhere('vw.dt_enviar = :dt_enviar')
                    ->setParameter('dt_enviar', $this->converteDataBanco($params['dt_enviar']));
            }

            if (isset($params['id_sistema'])) {
                $query->andWhere('vw.id_sistema = :id_sistema')
                    ->setParameter('id_sistema', $params['id_sistema']);
            }

            if (isset($params['bl_entrega'])) {
                $query->andWhere('vw.bl_entrega = :bl_entrega')
                    ->setParameter('bl_entrega', $params['bl_entrega']);
            }

            if (!$appResponse) {
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $this->sessao->id_entidade);
            }

            if (\Ead1_Sessao::getSessaoPerfil()->getId_perfil() == \PerfilPedagogicoTO::ALUNO) {
                $query->andWhere('vw.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', \Ead1_Sessao::getSessaoPerfil()->getId_perfil());
            }


            $query->orderBy('vw.id_mensagem', 'desc');

            $result = $query->getQuery()->getResult();
        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            throw new \Zend_Exception($e->getMessage());
        }

        return $result;

    }

    public function findMensagensAluno($params)
    {
        return $this->findBy('G2\Entity\VwEnviarMensagemAluno', $params);
    }

    /**
     * Atualiza a mensagem do portal
     * @param array $dados
     */

    public function atualizarMensagem(array $dados)
    {
        try {

            if (!isset($dados['id_enviomensagem']) || !$dados['id_enviomensagem'])
                throw new \Exception('O parãmetro id_enviomensagem é obrigatório');

            if (!isset($dados['id_matricula']) || !$dados['id_matricula'])
                throw new \Exception('O parãmetro id_matricula é obrigatório');

            if (!isset($dados['id_evolucao']) || !$dados['id_evolucao'])
                throw new \Exception('O parãmetro id_evolucao é obrigatório');

            if (!isset($dados['id_enviodestinatario']) || !$dados['id_enviodestinatario']) {
                throw new \Exception('O parâmetro id_enviodestinatario é obrigatório');
            }

            $mensagem = $this->findOneBy('\G2\Entity\EnvioDestinatario',
                array(
                    'id_enviomensagem' => $dados['id_enviomensagem'],
                    'id_matricula' => $dados['id_matricula'],
                    'id_enviodestinatario' => $dados['id_enviodestinatario']
                ));

            if (!$mensagem)
                throw new \Exception('EnvioDestinatario não encontrado');

            $mensagem->setId_evolucao($dados['id_evolucao']);

            $result = $this->save($mensagem);

            if ($result) {
                return true;
            }
        } catch (\Zend_Exception $e) {
            return $e;
        }

        return false;
    }

    /**
     * Salva mensagem portal
     * @param array $dados (keys obrigatórios: st_mensagem, st_texto, id_matricula, dt_enviar
     */
    public function salvarMensagemPortal(array $dados)
    {
        try {
            if (!array_key_exists('st_mensagem', $dados) || empty($dados['st_mensagem'])) {
                throw new \Exception('O título da mensagem é obrigatório e não foi passado!');
            }

            if (!array_key_exists('st_texto', $dados) || empty($dados['st_texto'])) {
                throw new \Exception('O texto da mensagem é obrigatório e não foi passado!');
            }

            if (!array_key_exists('id_matricula', $dados) || empty($dados['id_matricula'])) {
                throw new \Exception('O id_matricula é obrigatório e não foi passado!');
            }

            if (!array_key_exists('dt_enviar', $dados) || empty($dados['dt_enviar'])) {
                throw new \Exception('A data de envio é obrigatória!');
            }

            //tb_mensagem (st_mensagem, st_texto)
            $tb_mensagem = new \G2\Entity\Mensagem();
            $tb_mensagem->setId_entidade($this->getReference('G2\Entity\Entidade', $this->sessao->id_entidade));
            $tb_mensagem->setId_usuariocadastro($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));
            $tb_mensagem->setDt_cadastro(new \DateTime());
            $tb_mensagem->setSt_mensagem($dados['st_mensagem']);
            $tb_mensagem->setSt_texto($dados['st_texto']);
            $tb_mensagem->setId_situacao($this->find('G2\Entity\Situacao', 147));
            $tb_mensagem->setBl_importante(true);

            $tb_mensagem = $this->save($tb_mensagem);


            if ($tb_mensagem instanceof \G2\Entity\Mensagem) {

                $emailNegocio = new \G2\Negocio\EmailConfig();
                $arrayEmail = $emailNegocio->findByEntidade();
                if (empty($arrayEmail)) {
                    throw new \Exception('Não foi encontrado nenhum e-mail válido para envio nessa entidade!');
                }

                $email = $arrayEmail[0];
                $id_evolucao = $this->getReference('\G2\Entity\Evolucao', VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA);
                $id_tipoenvio = $this->getReference('\G2\Entity\TipoEnvio', TipoEnvio::HTML);

                $tbEnvioMsg = new \G2\Entity\EnvioMensagem();
                $tbEnvioMsg->setDt_enviar(Helper::converterData($dados['dt_enviar']));
                $tbEnvioMsg->setDt_cadastro(new \DateTime());
                $tbEnvioMsg->setId_emailconfig($email);
                $tbEnvioMsg->setId_mensagem($tb_mensagem);
                $tbEnvioMsg->setId_evolucao($id_evolucao);
                $tbEnvioMsg->setId_tipoenvio($id_tipoenvio);

                //save tb_enviomensagem
                $tbEnvioMsg = $this->save($tbEnvioMsg);

                if ($tbEnvioMsg instanceof EnvioMensagem) {
                    $id_matricula = $this->find('\G2\Entity\Matricula', $dados['id_matricula']);

                    $tbEnvioDestinatario = new \G2\Entity\EnvioDestinatario();
                    $tbEnvioDestinatario->setId_enviomensagem($tbEnvioMsg);
                    $tbEnvioDestinatario->setId_matricula($id_matricula);
                    $tbEnvioDestinatario->setId_tipodestinatario($this->find('\G2\Entity\TipoDestinatario', TipoDestinatario::DESTINATARIO));
                    $tbEnvioDestinatario->setId_usuario($id_matricula->getId_usuario());
                    $tbEnvioDestinatario->setId_evolucao(VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA);

                    // save tb_enviodestinatario
                    $tbEnvioDestinatario = $this->save($tbEnvioDestinatario);


                } else {
                    throw new \Exception('Erro ao salvar EnvioMensagem');
                }
            } else {
                throw new \Exception('Erro ao salvar mensagem');
            }

            return array(
                'type' => 'success',
                'title' => 'Sucesso :)',
                'text' => 'Mensagem cadastrada com sucesso!, Job GCM',
                'obj' => $tb_mensagem
            );

        } catch (Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao cadastrar mensagem: ' . $e->getMessage()
            );
        }


    }

    /**
     * Método que salva a mensagem de agendamento para o aluno
     * @param array $params (id_matricula obrigatório)
     */
    public function salvaMensagemAgendamento(array $params)
    {
        try {
            if (!array_key_exists('id_matricula', $params) || $params['id_matricula'] == '') {
                throw new \Zend_Exception('O id_matricula é obrigatório e não foi passado');
            }

            $matriculaNegocio = new Matricula();
            $matricula = $matriculaNegocio->retornaMatricula(array(
                'id_matricula' => $params['id_matricula']
            ), true);

            if ($matricula['id_evolucao']['id_evolucao'] == \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO) {
                $sistemaEntidadeMensagem = $this->findBy('\G2\Entity\SistemaEntidadeMensagem', array(
                    'id_mensagempadrao' => \MensagemPadraoTO::AVISO_AGENDAMENTO
                , 'id_entidade' => $this->sessao->id_entidade
                , 'bl_ativo' => 1
                ));

                if (count($sistemaEntidadeMensagem) > 0) {
                    $tb_sistemaEntidadeMensagem = $sistemaEntidadeMensagem[0];
                    $arParametros = array('id_matricula' => $params['id_matricula'], 'id_entidade' => \Ead1_Sessao::getSessaoEntidade()->id_entidade);
                    $textoSistemaBO = new \TextoSistemaBO ();
                    $html_mensagem = $textoSistemaBO->gerarHTML(new \TextoSistemaTO(array('id_textosistema' => $tb_sistemaEntidadeMensagem->getId_textosistema()->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                    //array $dados (keys obrigatórios: st_mensagem, st_texto, id_matricula, dt_enviar
                    $dados_send = array('st_mensagem' => 'Aviso de agendamento de prova'
                    , 'st_texto' => $html_mensagem
                    , 'id_matricula' => $params['id_matricula']
                    , 'dt_enviar' => date('d/m/Y'));
                    $salvar = $this->salvarMensagemPortal($dados_send);
                }
            } else {
                throw new \Zend_Exception('A evolução da matricula informada não é CURSANDO');
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que salva a mensagem de agendamento de recuperação para o aluno
     * @param array $params (id_matricula obrigatório)
     */
    public function salvaMensagemAgendamentoRecuperacao(array $params)
    {
        try {
            if (!array_key_exists('id_matricula', $params) || $params['id_matricula'] == '') {
                throw new \Zend_Exception('O id_matricula é obrigatório e não foi passado');
            }
            $sistemaEntidadeMensagem = $this->findBy('\G2\Entity\SistemaEntidadeMensagem', array('id_mensagempadrao' => \MensagemPadraoTO::AVISO_AGENDAMENTO_RECUPERACAO
            , 'id_entidade' => $this->sessao->id_entidade
            , 'bl_ativo' => 1));

            if (count($sistemaEntidadeMensagem) > 0) {
                $tb_sistemaEntidadeMensagem = $sistemaEntidadeMensagem[0];
                $arParametros = array('id_matricula' => $params['id_matricula'], 'id_entidade' => \Ead1_Sessao::getSessaoEntidade()->id_entidade);
                $textoSistemaBO = new \TextoSistemaBO ();
                $html_mensagem = $textoSistemaBO->gerarHTML(new \TextoSistemaTO(array('id_textosistema' => $tb_sistemaEntidadeMensagem->getId_textosistema()->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                //array $dados (keys obrigatórios: st_mensagem, st_texto, id_matricula, dt_enviar
                $dados_send = array('st_mensagem' => 'Aviso de agendamento de prova de recuperação'
                , 'st_texto' => $html_mensagem
                , 'id_matricula' => $params['id_matricula']
                , 'dt_enviar' => date('d/m/Y'));
                $salvar = $this->salvarMensagemPortal($dados_send);
                return $salvar;
            } else {
                throw new \Zend_Exception('Nenhum texto cadastrado para a instituição para agendamento de prova de recuperação');
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


    public function findVwMensagemPortal($id_mensagem)
    {
        $result = $this->find('G2\Entity\VwPesquisaMensagemPortal', (int)$id_mensagem);
        if ($result->getSt_mensagem() != null) {
            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }


    public function findByVwTurmaProjeto($id_projetopedagogico)
    {
        try {
            $params['id_projetopedagogico'] = $id_projetopedagogico;
            $repositoryName = 'G2\Entity\VwTurmaProjetoPedagogico';
            return $this->findBy($repositoryName, $params, array('st_turma' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Salva ou edita mensagem portal para todos os alunos com matricula ativa na entidade
     * @param array $dados (keys obrigatórios: st_mensagem, st_texto,  dt_enviar
     */
    public function salvarMensagemPortalPorEntidade(array $dados)
    {
        $this->beginTransaction();

        try {
            if (array_key_exists('id_mensagem', $dados) && $dados['id_mensagem']) {
                $tb_mensagem = $this->findOneBy('\G2\Entity\Mensagem', array('id_mensagem' => $dados['id_mensagem']));
                $tb_enviomensagem = $this->findOneBy('\G2\Entity\EnvioMensagem', array('id_mensagem' => $dados['id_mensagem']));
            } else {
                $tb_mensagem = new \G2\Entity\Mensagem();
            }

            /**
             * Se for uma edição, as unicas alterações feitas serão na situação da mensagem, que passa para ativa ou inativa e na data de término
             */
            if ($tb_mensagem->getId_mensagem()) {
                $tb_mensagem->setId_situacao($this->getReference('\G2\Entity\Situacao', $dados['id_situacao']));
                $tb_enviomensagem->setDt_saida(empty($dados['dt_saida']) ? null : \G2\Utils\Helper::converterData($dados['dt_saida']));
                $this->merge($tb_mensagem);
                $this->merge($tb_enviomensagem);
            } else {

                if (!array_key_exists('st_mensagem', $dados) || empty($dados['st_mensagem'])) {
                    throw new \Exception('O título da mensagem é obrigatório e não foi passado!');
                }

                if (!array_key_exists('st_texto', $dados) || empty($dados['st_texto'])) {
                    throw new \Exception('O texto da mensagem é obrigatório e não foi passado!');
                }

                if (!array_key_exists('dt_enviar', $dados) || empty($dados['dt_enviar'])) {
                    throw new \Exception('A data de início é obrigatória!');
                }

                $tb_mensagem->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade))
                    ->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario))
                    ->setDt_cadastro(new \DateTime())
                    ->setSt_mensagem($dados['st_mensagem'])
                    ->setSt_texto($dados['st_texto'])
                    ->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Entity\Mensagem::ATIVA));

                if (array_key_exists('id_areaconhecimento', $dados) && $dados['id_areaconhecimento']) {
                    $tb_mensagem->setId_areaconhecimento($this->getReference('\G2\Entity\AreaConhecimento', (int)$dados['id_areaconhecimento']));
                } else {
                    $tb_mensagem->setId_areaconhecimento(null);
                }

                if (array_key_exists('id_projetopedagogico', $dados) && $dados['id_projetopedagogico']) {
                    $tb_mensagem->setId_projetopedagogico($this->getReference('\G2\Entity\ProjetoPedagogico', (int)$dados['id_projetopedagogico']));
                } else {
                    $tb_mensagem->setId_projetopedagogico(null);
                }

                if (array_key_exists('id_turma', $dados) && $dados['id_turma']) {
                    $tb_mensagem->setId_turma($this->getReference('\G2\Entity\Turma', (int)$dados['id_turma']));
                } else {
                    $tb_mensagem->setId_turma(null);
                }

                if (array_key_exists('bl_importante', $dados)) {
                    $tb_mensagem->setBl_importante(1);
                } else {
                    $tb_mensagem->setBl_importante(0);
                }

                if ($tb_mensagem->getId_mensagem()) {
                    $tb_mensagem = $this->merge($tb_mensagem);
                } else {
                    $tb_mensagem = $this->save($tb_mensagem);
                }

                if ($tb_mensagem instanceof \G2\Entity\Mensagem && $tb_mensagem->getId_mensagem()) {

                    if (array_key_exists('id_mensagem', $dados) && $dados['id_mensagem']) {
                        $tbEnvioMsg = $this->findOneBy('\G2\Entity\EnvioMensagem', array('id_mensagem' => (int)$dados['id_mensagem']));
                        $tbEnvioMsg = $this->updateEnvioMensagem($tbEnvioMsg, $dados);
                        $envioDestinatario = $this->updateEnvioDestinatario($tbEnvioMsg);
                    } else {
                        $tbEnvioMsg = $this->saveEnvioMensagem($tb_mensagem, $dados);
                        if ($tbEnvioMsg instanceof EnvioMensagem) {
                            $envioDestinatario = $this->saveEnvioDestinatario($tbEnvioMsg, $tb_mensagem, null, false);
                        } else {
                            throw new \Exception('Erro ao salvar EnvioMensagem');
                        }
                    }

                } else {
                    throw new \Exception('Erro ao salvar mensagem');
                }
            }

            $this->commit();

            if (array_key_exists('id_mensagem', $dados) && $dados['id_mensagem']) {
                return array(
                    'type' => 'success',
                    'title' => 'Sucesso :)',
                    'text' => 'Mensagem alterada com sucesso!',
                    'obj' => $tb_mensagem->getId_mensagem()
                );
            } else {
                return array(
                    'type' => 'success',
                    'title' => 'Sucesso :)',
                    'text' => 'Mensagem agendada com sucesso! <br/> <b>Data do envio: ' . $dados['dt_enviar'] . '</b> ',
                    'obj' => $tb_mensagem->getId_mensagem()
                );
            }
        } catch (Exception $e) {
            $this->rollback();
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao cadastrar mensagem: ' . $e->getMessage()
            );
        }
    }

    /**
     * Atualiza data de envio de uma mensagem na hora da edição da mesmo
     * Altera a evolução da mensagem alterada para não lida
     * @param $tbEnvioMsg
     * @param $dados
     * @return array|\Exception|type|\PDOException
     */
    public function updateEnvioMensagem($tbEnvioMsg, $dados)
    {
        try {
            $id_evolucao = $this->getReference('\G2\Entity\Evolucao', VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA);

            $tbEnvioMsg->setDt_enviar($this->converteDataBanco($dados['dt_enviar'] . ' 00:00:00'))
                ->setDt_cadastro(new \DateTime())
                ->setId_evolucao($id_evolucao);

            $tbEnvioMsg = $this->merge($tbEnvioMsg);
            return $tbEnvioMsg;
        } catch (\Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao editar envio de mensagem: ' . $e->getMessage()
            );
        }
    }

    /**
     * Salva envio de mensagem para serem exibidas para o aluno no portal
     * @param $tb_mensagem
     * @param $dados
     * @return array|EnvioMensagem
     */
    public function saveEnvioMensagem($tb_mensagem, $dados)
    {
        try {
            $id_evolucao = $this->getReference('\G2\Entity\Evolucao', VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA);
            $id_tipoenvio = $this->getReference('\G2\Entity\TipoEnvio', TipoEnvio::HTML);

            $tbEnvioMsg = new EnvioMensagem();
            $tbEnvioMsg->setDt_enviar(\G2\Utils\Helper::converterData($dados['dt_enviar']))
                ->setDt_cadastro(new \DateTime())
                ->setId_mensagem($tb_mensagem)
                ->setId_evolucao($id_evolucao)
                ->setId_tipoenvio($id_tipoenvio)
                ->setDt_saida(empty($dados['dt_saida']) ? null : \G2\Utils\Helper::converterData($dados['dt_saida']));

            //save tb_enviomensagem
            $tbEnvioMsg = $this->save($tbEnvioMsg);
            return $tbEnvioMsg;
        } catch (\Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao salvar envio de mensagem: ' . $e->getMessage()
            );
        }
    }

    /**
     * Salva envio de mensagem para todos os alunos da entidade com matricula ativa na entidade
     * @param $tbEnvioMsg
     * @param $tb_mensagem
     * @return array|bool
     */
    public function saveEnvioDestinatario
    (
        $tbEnvioMsg
        , $tb_mensagem
        , \G2\Entity\VwMatriculaLancamento $matriculaLancamento = null
        , $autoTransaction = true
    )
    {
        $autoTransaction
            ? $this->beginTransaction()
            : null;

        try {

            $retorno = array();
            $where = '';

            if ($tb_mensagem->getId_areaconhecimento() instanceof AreaConhecimento &&
                $tb_mensagem->getId_areaconhecimento()->getId_areaconhecimento()
            ) {
                $where .= ' AND id_areaconhecimento = ' . $tb_mensagem->getId_areaconhecimento()->getId_areaconhecimento();
            }
            if ($tb_mensagem->getId_projetopedagogico() instanceof ProjetoPedagogico &&
                $tb_mensagem->getId_projetopedagogico()->getId_projetopedagogico()
            ) {
                $where .= ' AND id_projetopedagogico = ' . $tb_mensagem->getId_projetopedagogico()->getId_projetopedagogico();
            }

            if ($tb_mensagem->getId_turma() instanceof Turma &&
                $tb_mensagem->getId_turma()->getId_turma()
            ) {
                $where .= ' AND id_turma = ' . $tb_mensagem->getId_turma()->getId_turma();
            }

            if (!(is_null($matriculaLancamento)) && $matriculaLancamento->getId_matricula()) {
                $where .= ' AND id_matricula = ' . $matriculaLancamento->getId_matricula();
            }

            if (!$this->sessao->id_entidade && !$matriculaLancamento) {
                throw new \Exception("Não foi possível encontrar a Entidade");
            }

            $id_entidade = $this->sessao->id_entidade ? $this->sessao->id_entidade : $matriculaLancamento->getId_entidade();

            $sql = "SELECT 1,
                        " . $tbEnvioMsg->getId_enviomensagem() . " ,
                        id_usuario ,
                        null ,
                        st_nomecompleto ,
                        id_matricula ,
                        null ,
                        " . VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA . "
                        FROM dbo.vw_matricula as m
                            WHERE (m.id_entidadematricula = " . $id_entidade . " AND bl_ativo = 1 " . $where . "
                        )";

            $res = $this->em->getConnection()->fetchAll($sql);
            //verifica se a consulta retornou resultado
            if ($res) {
                /*
                 * se a consulta tiver retornado dados, significa que há destinatários, então passa a consulta para o
                 * método no repository persistir os dados
                 */
                $retorno = $this->em->getRepository('\G2\Entity\Mensagem')
                    ->inserirMensagemDestinatario($sql);

                //Registrando JOB de push de notificacoes ao aplicativo portal do aluno
                $this->createHttpJob("/robo/zend-job-push-notificacao-app",
                    array('array_alunos' => \Zend_Json::encode($res)),
                    array(
                        'name' => 'Envio de mensagems do aplicativo Portal do Aluno',
                        'schedule_time' => strtotime('now')
                    ),
                    \G2\Constante\Sistema::PORTAL_ALUNO
                );
            }

            $autoTransaction
                ? $this->commit()
                : null;

            return $retorno;

        } catch (\Exception $e) {
            $autoTransaction
                ? $this->rollback()
                : null;

            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao salvar ENVIO DESTINATARIO: ' . $e->getMessage()
            );
        }
    }

    public function updateEnvioDestinatario($tbEnvioMsg)
    {
        try {
            $sql = "UPDATE dbo.tb_enviodestinatario
            SET id_evolucao = " . VwEnviarMensagemAluno::EVOLUCAO_NAO_LIDA . "
            WHERE id_enviomensagem = " . $tbEnvioMsg->getId_enviomensagem();
            //\Zend_Debug::dump($sql);die;
            $stmt = $this->em->getConnection()->prepare($sql);
            $retorno = $stmt->execute();
            return $retorno;
            //return $stmt->fetchAll();
        } catch (\Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao editar ENVIO DESTINATARIO: ' . $e->getMessage()
            );
        }
    }

    /**
     * Faz update do atributo bl_entregue de um conjunto de ids_enviodestinatario
     * em uma unica vez utilizando native query.
     *
     * @use Aplicativo Portal do Aluno
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $arrEnvioMensagensEntregues
     * @return \Ead1_Mensageiro
     */
    public function updateEnvioDestinatarioEntregueEmLote($arrEnvioMensagensEntregues)
    {
        if (!is_array($arrEnvioMensagensEntregues) || empty($arrEnvioMensagensEntregues)) {
            return new \Ead1_Mensageiro("Nenhum id_enviomensagem informado para a atualização da entrega.",
                \Ead1_IMensageiro::ERRO);
        }

        $arrIdsMsgsNaoEntregues = array();

        foreach ($arrEnvioMensagensEntregues as $envioMensagemEntregue) {
            try {
                $sql = 'UPDATE dbo.tb_enviodestinatario 
                          SET bl_entrega = 1
                          WHERE id_enviomensagem = ' . $envioMensagemEntregue .
                    ' AND bl_entrega = 0';
                $stmt = $this->em->getConnection()->prepare($sql);
                $stmt->execute();
            } catch (\Exception $e) {
                $arrIdsMsgsNaoEntregues[] = $envioMensagemEntregue;
            }
        }

        if (!empty($arrIdsMsgsNaoEntregues)) {
            $texto = 'Não foi possível editar o envio destinatário dos seguintes id_enviomensagem: '
                . implode(", ", $arrIdsMsgsNaoEntregues);
            return new \Ead1_Mensageiro($texto, \Ead1_IMensageiro::ERRO);
        }

        return new \Ead1_Mensageiro("Mensagens atualizadas com sucesso.", \Ead1_IMensageiro::SUCESSO);
    }

    /**
     * Metodo requisitado pelo robo para envio de mensagens ainda nao entregues aos dispositivos
     * registrados de alunos de de entidades que usam o aplicativo portal do aluno.
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @throws \Zend_Exception
     */
    public function pushMensagensNotificacaoAppRegistrarEntrega(array $whereAnd = [], array $whereLike = [], array $whereIn = [])
    {

        try {

            //pegando hash configuracao app gcm do aplicativo
            $front = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $apiKeyGcm = $front->getOption('apiKeyGcm');

            if (!$apiKeyGcm) {
                throw new \Zend_Exception('Nao encontrada a key GCM no arquivo de configuracao principal', 500);
            }

            $repo = $this->getRepository('\G2\Entity\VwEnviarMensagemAluno');

            $mensagens = $repo->retornaMensagensEnvioNotificacaoApp($whereAnd, $whereLike, $whereIn);
            $retorno = array();
            $arrayMsgEntregues = array();

            $gcm = new \Zend_Mobile_Push_Gcm();
            $gcm->setApiKey($apiKeyGcm);

            foreach ($mensagens as $mensagem) {

                $gcmm = new \Zend_Mobile_Push_Message_Gcm();

                $gcmm->addToken($mensagem['st_key']);
                $gcmm->setData(array(
                    'title' => 'Portal do Aluno',
                    'message' => $mensagem['st_mensagem'],
                    'router' => 'app.mensagem',
                    'icon' => 'android/ic_action_monolog.png',
                    'smallIcon' => 'android/ic_action_monolog.png'
                ));

                try {
                    $result = $gcm->send($gcmm);
                    $retorno[] = $result;
                    $arrayMsgEntregues[] = $mensagem;

                } catch (\Zend_Mobile_Push_Exception $e) {
                    $retorno[] = $e->getMessage();
                }
            }

            //registra mensagens ja entreges aos dispositivos
            if ($arrayMsgEntregues) {
                $arrIdEnviomensagens = array_unique(array_column($arrayMsgEntregues, 'id_enviomensagem'));
                $this->updateEnvioDestinatarioEntregueEmLote($arrIdEnviomensagens);
            }

            return $arrayMsgEntregues;

        } catch (\Zend_Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }

    /**
     * Retorna os avisos (diversos) importantes
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornarAvisosPortal($params = [])
    {
        try {

            //busca as mensagens comuns
            $mensagensComuns = $this->findVwEnviarMensagemAluno(array(
                'id_usuario' => $params['id_usuario'],
                'id_evolucao' => \G2\Constante\Evolucao::TB_ENVIODESTINATARIO_NAO_LIDA,
                'bl_importante' => true,
//                'bl_entrega'=>false
            ), true);

            $arrMensagens = [];

            //verifica se retornou alguma mensagem
            if (count($mensagensComuns)) {
                //percorre o array e seta somente os valores necessários
                $arrMensagens = array_map(function ($mensagem) {
                    return array(
                        'id' => $mensagem['id_mensagem'],
                        'title' => $mensagem['st_mensagem'],
                        'text' => $mensagem['st_texto'],
                        'send_date' => $mensagem['dt_enviar'],
                        'type' => \G2\Constante\Utils::TIPO_AVISO_MENSAGEM,
                        'object' => $mensagem
                    );
                }, $mensagensComuns);
            }

            if (isset($params['id_matricula']) && $params['id_matricula']) {
                /**
                 * @var \G2\Entity\Matricula $matricula
                 */
                $matricula = $this->find('\G2\Entity\Matricula', $params['id_matricula']);
                $id_entidade = $matricula->getId_entidadematricula();

                /**
                 * Mensagem personalizada: Aviso Agendamento de PROVA FINAL
                 */
                // Verificar se a configuracao da entidade (Organizacao > Cadastros > Pessoa Juridíca :: Configurações) esta HABILITADO para mostrar mensagem de agendamento no portal.
                $cf_entidade = $this->findOneBy('\G2\Entity\ConfiguracaoEntidade', array('id_entidade' => $id_entidade));

                if ($cf_entidade instanceof \G2\Entity\ConfiguracaoEntidade && $cf_entidade->getBl_msgagendamentoportal()) {
                    $ng_gerenciaprova = new \G2\Negocio\GerenciaProva();
                    $status = $ng_gerenciaprova->statusAgendamentoAluno($matricula->getId_matricula(), $id_entidade);

                    // Verificar se o aluno esta APTO
                    if ($status) {
                        switch ($status) {
                            case 2:
                                $title = 'Agende sua prova de recuperação';
                                $text = 'Agende sua prova com antecedência e não perca o prazo!';
                                break;
                            case 3:
                                $title = 'Reagendar prova presencial';
                                $text = 'Você já possui um agendamento, clique na mensagem para alterar a data do agendamento.';
                                break;
                            case 4:
                                $title = 'Reagendar prova de recuperação';
                                $text = 'Você já possui um agendamento, clique na mensagem para alterar a data do agendamento.';
                                break;
                            default:
                                $title = 'Agende sua prova presencial';
                                $text = 'Agende sua prova com antecedência e não perca o prazo!';
                                break;
                        }

                        array_unshift($arrMensagens, array(
                            'id' => null,
                            'title' => $title,
                            'text' => $text,
                            'send_date' => date("Y-m-d H:i:s"),
                            'type' => \G2\Constante\Utils::TIPO_AVISO_AGENDAMENTO,
                            'object' => array()
                        ));
                    }
                }

            }

            return $arrMensagens;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao consultar avisos. " . $e->getMessage());
        }
    }


}

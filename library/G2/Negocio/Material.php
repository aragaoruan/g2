<?php

namespace G2\Negocio;

use G2\Constante\Situacao;

/**
 * Classe de negócio para Material
 * @author Débora Castro <debora.castro@unyleya.com.br>
 */
class Material extends Negocio
{


    public function __construct()
    {
        parent::__construct();
    }

    public function gerarPacoteAlunos($parametros)
    {
        try {
            $where = array();

            $where['id_entidade'] = $this->sessao->id_entidade;

            if (isset($parametros['id_areaconhecimento']) && $parametros['id_areaconhecimento'] != '') {
                $where['id_areaconhecimento'] = $parametros['id_areaconhecimento'];
            }

            if (isset($parametros['id_projetopedagogico']) && $parametros['id_projetopedagogico'] != '') {
                $where['id_projetopedagogico'] = $parametros['id_projetopedagogico'];
            }

            $buscaVw = $this->findBy('\G2\Entity\VwGerarPacoteAlunos', $where);

            if (is_array($buscaVw) && !empty($buscaVw)) {

                $idLoteMerial = $this->salvarLoteMaterial();

                if ($idLoteMerial->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                    $id_pacote = null;

                    foreach ($buscaVw as $alunos) {

                        $findPacote = $this->findOneBy('\G2\Entity\Pacote', array('id_matricula' => $alunos->getId_matricula()));
                        if (empty($findPacote)) {
                            $entityPacote = new \G2\Entity\Pacote();
                            $entityPacote->setId_lotematerial($idLoteMerial->getId());
                            $entityPacote->setId_usuariocadastro($this->sessao->id_usuario);
                            $entityPacote->setId_matricula($alunos->getId_matricula());
                            $entityPacote->setId_situacao(\G2\Constante\Situacao::TB_PACOTE_PENDENTE);
                            $entityPacote->setDt_cadastro(new \DateTime());
                            $this->save($entityPacote);
                            $id_pacote = $entityPacote->getId_pacote();
                        } else {
                            $id_pacote = $findPacote->getId_pacote();
                        }


                        $retornoPacoteMD = $this->salvarPacoteMatriculaDisciplina(array('id_pacote' => $id_pacote, 'id_matriculadisciplina' => $alunos->getId_matriculadisciplina()));
                        if ($retornoPacoteMD->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            return $retornoPacoteMD;
                        }
                        if (!empty($id_pacote)) {
                            $retornoEM = $this->salvarEntregaMaterial(array('id_pacote' => $id_pacote, 'id_matriculadisciplina' => $alunos->getId_matriculadisciplina(), 'id_itemdematerial' => $alunos->getId_itemdematerial(), 'id_matricula' => $alunos->getId_matricula()));
                            if ($retornoEM->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                                return $retornoEM;
                            }
                        }
                    }

                    $mensageiro = new \Ead1_Mensageiro('Pacote gerado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                    $mensageiro->setId($idLoteMerial->getId());
                }

            } else {
                $mensageiro = new \Ead1_Mensageiro('Não foram encontrados dados para gerar o pacote!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);

        }
        return $mensageiro;

    }

    public function salvarLoteMaterial()
    {
        try {
            $entityLoteMaterial = new \G2\Entity\LoteMaterial();
            $entityLoteMaterial->setDt_cadastro(new \DateTime());
            $this->save($entityLoteMaterial);

            $mensageiro = new \Ead1_Mensageiro();

            if ($entityLoteMaterial->getId_lotematerial()) {
                $mensageiro->setText('Lote de Material salvo com sucesso!');
                $mensageiro->setTipo(\Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($entityLoteMaterial->getId_lotematerial());
            } else {
                $mensageiro->setText('Lote de Material não foi criado!');
                $mensageiro->setTipo(\Ead1_IMensageiro::ERRO);
            }

        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    public function salvarPacoteMatriculaDisciplina($dados)
    {
        try {

            $entityMatricula = $this->findBy('\G2\Entity\MatriculaDisciplina', array('id_matriculadisciplina' => (int)$dados['id_matriculadisciplina']));

            if (is_array($entityMatricula) && !empty($entityMatricula)) {
                foreach ($entityMatricula as $entityUnic) {
                    $id_pacote = $entityUnic->getId_pacote();
                    if (empty($id_pacote)) {
                        $entityUnic->setId_pacote((int)$dados['id_pacote']);
                        $this->merge($entityUnic);
                    }
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Matricula Disciplina atualizada com sucesso!', \Ead1_IMensageiro::SUCESSO);

        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    public function salvarEntregaMaterial($dados)
    {
        try {
            $entityMatricula = $this->findBy('\G2\Entity\EntregaMaterial', array('id_matricula' => (int)$dados['id_matricula'], 'id_itemdematerial' => (int)$dados['id_itemdematerial']));

            if (empty($entityMatricula)) {
                $entityEntregaMaterial = new \G2\Entity\EntregaMaterial();
                $entityEntregaMaterial->setId_situacao(Situacao::TB_ENTREGAMATERIAL_PREPARANDO_PARA_ENVIO);
                $entityEntregaMaterial->setId_matricula((int)$dados['id_matricula']);
                $entityEntregaMaterial->setId_usuariocadastro((int)$this->sessao->id_usuario);
                $entityEntregaMaterial->setId_itemdematerial((int)$dados['id_itemdematerial']);
                $entityEntregaMaterial->setId_pacote((int)$dados['id_pacote']);
                $entityEntregaMaterial->setBl_ativo(1);
                $entityEntregaMaterial->setId_matriculadisciplina((int)$dados['id_matriculadisciplina']);
                $entityEntregaMaterial->setDt_devolucao(null);
                $entityEntregaMaterial->setDt_entrega(null);
                $this->save($entityEntregaMaterial);

                if ($entityEntregaMaterial->getId_entregamaterial()) {
                    $mensageiro = new \Ead1_Mensageiro('Cadastro de entrega de material registrado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                    $mensageiro->setId($entityEntregaMaterial->getId_entregamaterial());
                } else {
                    $mensageiro = new \Ead1_Mensageiro('Não foi possível cadastrar a entrega de material!', \Ead1_IMensageiro::ERRO);
                }
            } else {
                $mensageiro = new \Ead1_Mensageiro('Já existe este material para está matrícula!', \Ead1_IMensageiro::SUCESSO);
            }

        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    public function pesquisarVwEntregaMaterial($params)
    {
        try {

            $repo = $this->em->getRepository('G2\Entity\VwEntregaMaterial');

            $query = $repo->createQueryBuilder("vw")
                ->select('vw.st_nomecompleto', 'vw.id_pacote', 'vw.id_itemdematerial', 'vw.st_itemdematerial', 'vw.id_lotematerial', 'vw.st_projetopedagogico', 'vw.id_matricula', 'vw.dt_criacaopacote', 'vw.st_situacaopacote', 'vw.id_entregamaterial', 'vw.dt_entrega')
                ->where('1=1')
                ->orderBy('vw.id_lotematerial, vw.st_nomecompleto');

            if (isset($params['st_nomecompleto']) && !empty($params['st_nomecompleto'])) {
                $query->andWhere("vw.st_nomecompleto like '%" . $params['st_nomecompleto'] . "%'");
            }

            //dt_criacaofim
            if ((isset($params['dt_criacaoinicio']) && !empty($params['dt_criacaoinicio'])) && (isset($params['dt_criacaofim']) && !empty($params['dt_criacaofim']))) {
                $dtIn =$this->converteDataBanco($params['dt_criacaoinicio']);
                $dtFim =$this->converteDataBanco($params['dt_criacaofim']);
                $query->andWhere('vw.dt_criacaopacote BETWEEN :dtinicio AND :dtfim')
                    ->setParameter('dtinicio', $dtIn->format('Y-m-d'))
                    ->setParameter('dtfim', $dtFim->format('Y-m-d'));
            }

            if (isset($params['id_areaconhecimentopesquisa']) && !empty($params['id_areaconhecimentopesquisa'])) {
                $query->andWhere('vw.id_areaconhecimento = :id_areaconhecimento')
                    ->setParameter('id_areaconhecimento', $params['id_areaconhecimentopesquisa']);
            }

            if (isset($params['id_projetopedagogicopesquisa']) && !empty($params['id_projetopedagogicopesquisa'])) {
                $query->andWhere('vw.id_projetopedagogico = :id_projetopedagogico')
                    ->setParameter('id_projetopedagogico', $params['id_projetopedagogicopesquisa']);
            }

            if ((isset($params['dt_postageminicio']) && !empty($params['dt_postageminicio'])) && (isset($params['dt_postagemfim']) && !empty($params['dt_postagemfim']))) {
                    $dataIni =$this->converteDataBanco($params['dt_postageminicio']);
                    $dataF =$this->converteDataBanco($params['dt_postagemfim']);
                $query->andWhere('vw.dt_entrega BETWEEN :inicio AND :fim')
                    ->setParameter('inicio', $dataIni->format('Y-m-d'))
                    ->setParameter('fim', $dataF->format('Y-m-d'));
//                \Zend_Debug::dump($query->getParameters());die;
            }
            if (isset($params['id_situacaopesquisa']) && !empty($params['id_situacaopesquisa'])) {
                $query->andWhere('vw.id_situacaopacote = :id_situacaopacote')
                    ->setParameter('id_situacaopacote', $params['id_situacaopesquisa']);
            }

            $result = $query->getQuery()->getResult();

            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }

    }

    public function gerarArquivoDeEnvio($param)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwGerarArquivoDeEnvio');

            $query = $repo->createQueryBuilder("vw")
                ->select('vw.st_nomecompleto', 'vw.id_pacote', 'vw.id_itemdematerial', 'vw.st_cpf', 'vw.st_endereco', 'vw.nu_numero', 'vw.st_bairro', 'vw.st_complemento', 'vw.st_nomemunicipio', 'vw.sg_uf', 'vw.st_cep', 'vw.st_disciplina', 'vw.st_projetopedagogico', 'vw.st_areaconhecimento', 'vw.st_nomeentidade')
                ->where('1=1')
                ->orderBy('vw.id_pacote, vw.st_nomecompleto');

            if (isset($params['id_lotematerial']) && !empty($params['id_lotematerial'])) {
                $query->andWhere('vw.id_lotematerial = :id_lotematerial')
                    ->setParameter('id_lotematerial', $params['id_lotematerial']);
            }
            if ((isset($param['dt_inicio']) && !empty($param['dt_inicio'])) && (isset($param['dt_fim']) && !empty($param['dt_fim']))) {

                $query->andWhere('vw.dt_criacaopacote BETWEEN :inicio AND :fim')
                    ->setParameter('inicio', $this->converteDataBanco($param['dt_inicio']))
                    ->setParameter('fim', $this->converteDataBanco($param['dt_fim']));
            }
                $result = $query->getQuery()->getResult();
            if (count($result) > 0) {
                return $result;
            } else {
                return array();
            }

        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
            return $mensageiro;
        }
    }

    /**
     * Metodo responsavel por retornar registros da vw_entregamaterial de acordo com os parametros
     * repassados.
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $where
     * @param array $order
     * @param array $limit
     * @return array G2\Entity\VwEntregaMaterial
     */
    public function findByVwEntregaMaterial(array $where = NULL, array $order = NULL, array $limit = NULL){
        return $this->findBy('\G2\Entity\VwEntregaMaterial', $where, $order, $limit);
    }

}

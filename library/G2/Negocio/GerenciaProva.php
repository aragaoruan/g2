<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Constante\MensagemPadrao;
use G2\Constante\TipoAvaliacao;
use G2\Entity\AvaliacaoAgendamento;
use G2\Utils\Helper;


/**
 * Classe de negócio para Gerenciar Provas
 * @author Debora Castro <debora.castro@unyleya.com.br>;
 */
class GerenciaProva extends Negocio
{

    private $entidadeNegocio;
    private $repositorio = '\G2\Entity\AvaliacaoAgendamento';

    public function __construct()
    {
        parent::__construct();
        $this->entidadeNegocio = new \G2\Negocio\Entidade();
    }

    /**
     * entity VwAlunosAgendamento
     * @param array $params
     * @return array|string
     */
    public function findByVwAlunosAgendamento($params = array())
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwAlunosAgendamento';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método que pesquisa a entity AvaliacaoAgendamento
     * parametros padrão: bl_ativo=1 e id_entidade = (sessao)
     * @param $params
     * @return array|string
     */
    public function findByAvaliacaoAgendamento(array $params)
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            //$params['bl_ativo'] = 1;
            $repositoryName = 'G2\Entity\AvaliacaoAgendamento';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function findOneByPresencaAgendamento(array $params)
    {
        try {
            $retorno = array();

            $params['id_entidade'] = $this->sessao->id_entidade;
            $params['id_situacao'] = AvaliacaoAgendamento::AGENDADO;
            $repositoryName = 'G2\Entity\VwAvaliacaoAgendamento';

            // Primeiramento procura o que tenha nu_presenca 1
            $params['nu_presenca'] = 1;
            $objetoEntity = $this->findOneBy($repositoryName, $params);

            // Se nao encontrar, procurar qualquer outro valor de nu_presenca
            if (!$objetoEntity) {
                unset($params['nu_presenca']);
                $objetoEntity = $this->findOneBy($repositoryName, $params);
            }

            if (!empty($objetoEntity)) {
                $retorno = $this->toArrayEntity($objetoEntity);
                $retorno['hr_inicio'] = $this->converteHoraTextoAgendamento($retorno['hr_inicio']);
                $retorno['hr_fim'] = $this->converteHoraTextoAgendamento($retorno['hr_fim']);
            }

            return $retorno;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Metodo que pesquisa a entity AvaliacaoAgendamento
     * @param array $params
     * @return array|string
     */
    public function findByAvaliacaoAluno(array $params)
    {
        try {
            $repositoryName = 'G2\Entity\AvaliacaoAluno';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo que pesquisa a entity VwAvaliacaoConjuntoRelacao
     * Parametros padrão: id_entidade = sessao
     * id_tipoavaliacao = 4 (referente a provas presenciais - final)
     */

    public function findByProvasCadastradas($params = array())
    {
        try {
            $repositoryName = '\G2\Entity\VwAvaliacaoConjuntoRelacao';
            return $this->findBy($repositoryName, array_merge(array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL
            ), $params));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo que pesquisa a entity VwAvaliacaoAluno
     * @param $params
     * @return array|string
     */
    public function findByVwAvaliacaoAluno($params)
    {
        try {
            $repositoryName = 'G2\Entity\VwAvaliacaoAluno';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Metodo que pesquisa a entity VwAvaliacaoAgendamento
     * @param $params
     * @return array|string
     */
    public function findByVwAvaliacaoAgendamento($where, $order_by = array())
    {
        try {
            $repositoryName = '\G2\Entity\VwAvaliacaoAgendamento';

            if (!$order_by) {
                $order_by = array('st_nomecompleto' => 'ASC', 'dt_aplicacao' => 'ASC');
            }

            if (isset($where['st_nomecompleto']) && $where['st_nomecompleto']) {
                $whereLike = array('st_nomecompleto' => $where['st_nomecompleto']);
                unset($where['st_nomecompleto']);
                return $this->findBySearch($repositoryName, $whereLike, $where, $order_by);
            } else {
                return $this->findBy($repositoryName, $where, $order_by);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Metodo que pesquisa a entity VwHistoricoAgendamento
     * Parametro padrao: id_entidade = sessao
     * @param $params
     * @return array|string
     */
    public function findByVwHistoricoAgendamento($params)
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwHistoricoAgendamento';
            return $this->findBy($repositoryName, $params, array(
                'dt_cadastro' => 'DESC',
                'id_tramite' => 'DESC'
            ));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna pesquisa dos alunos para o agendamento
     * @param array $params
     * @return bool
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisaAlunos(array $params = array())
    {
        try {

            $repo = $this->em->getRepository('G2\Entity\VwAlunosAgendamento');
            $array_search = array('vw.id_usuario', 'vw.st_nomecompleto', 'vw.st_cpf', 'vw.st_email', 'vw.id_situacaoagendamento', 'vw.id_matricula', 'vw.id_aplicadorprova', 'vw.st_aplicadorprova', 'vw.bl_ativo', 'vw.dt_cadastro', 'vw.bl_unica', 'vw.dt_antecedenciaminima', 'vw.dt_alteracaolimite', 'vw.dt_aplicacao', 'vw.id_usuariocadastro', 'vw.id_horarioaula', 'vw.nu_maxaplicacao', 'vw.id_endereco', 'vw.id_avaliacaoaplicacao', 'vw.st_projetopedagogico', 'vw.id_projetopedagogico', 'vw.id_entidade', 'vw.sg_uf');
            if (isset($params['id_aplicadorprova']) && !empty($params['id_aplicadorprova']) && isset($params['dt_aplicacao']) && !empty($params['dt_aplicacao'])) {
                $array_search[] = 'vw.id_avaliacaoagendamento';
                $array_search[] = 'vw.bl_ativoavagendamento';
            }
            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->where('1=1')
                ->orderBy('vw.st_nomecompleto')
                ->distinct();

            if (isset($params['st_nomecompleto']) && $params['st_nomecompleto']) {
                $query->andWhere("vw.st_nomecompleto LIKE '%" . $params['st_nomecompleto'] . "%'");
            }

            if (isset($params['st_email']) && $params['st_email']) {
                $query->andWhere("vw.st_email = '" . $params['st_email'] . "'");
            }

            if (isset($params['st_cpf']) && $params['st_cpf']) {
                $query->andWhere("vw.st_cpf = '" . $params['st_cpf'] . "'");
            }

            $where = array();
            if (isset($params['st_str']) && !empty($params['st_str'])) {
                $where[] = "vw.st_nomecompleto LIKE '%" . $params['st_str'] . "%'";
                $where[] = "vw.st_cpf LIKE '%" . $params['st_str'] . "%'";
                $where[] = "vw.st_email LIKE '%" . $params['st_str'] . "%'";
                $query->andWhere(implode(" OR ", $where));
            }

            if (isset($params['sg_uf']) && !empty($params['sg_uf'])) {
                $query->andWhere('vw.sg_uf = :sg_uf')
                    ->setParameter('sg_uf', $params['sg_uf']);
            }

            if (isset($params['id_matricula']) && !empty($params['id_matricula'])) {
                $query->andWhere('vw.id_matricula = :id_matricula')
                    ->setParameter('id_matricula', $params['id_matricula']);
            }

            if (isset($params['id_aplicadorprova']) && !empty($params['id_aplicadorprova'])) {
                if (isset($params['dt_aplicacao']) && !empty($params['dt_aplicacao'])):
                    $dataEntrada = new \Zend_Date(new \Zend_Date($params['dt_aplicacao']), \Zend_Date::ISO_8601);
                    $dtEntrada = $dataEntrada->toString('yyyy-MM-dd');

                    $query->andWhere('vw.id_aplicadorprova = :id_aplicadorprova')
                        ->setParameter('id_aplicadorprova', $params['id_aplicadorprova']);
                    $query->andWhere("vw.dt_aplicacao like '" . $dtEntrada . "'");
                    $query->andWhere('vw.bl_ativoavagendamento = :bl_ativoavagendamento')
                        ->setParameter('bl_ativoavagendamento', 1);
                    $query->andWhere("vw.id_avaliacaoagendamento IS NOT NULL");
                else:
                    return new \Ead1_Mensageiro('Para pesquisar pelo aplicador é necessário selecionar a data!', \Ead1_IMensageiro::AVISO);
                endif;
            }

            $query->andWhere("vw.id_entidade = :id_entidade")
                ->setParameter("id_entidade", $this->sessao->id_entidade);
            $result = $query->getQuery()->getResult();
            if (count($result) > 0) {
                $ar_return = array();
                foreach ($result as $item) {
                    $ar_return[$item['id_matricula']] = $item;
                }
                return new \Ead1_Mensageiro(array_values($ar_return), \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * entity VwAplicador
     * @param array $params
     * @return array|string
     */
    public function findVwAplicador(array $params = array())
    {
        try {
            if (empty($params['sg_uf']))
                unset($params['sg_uf']);
            $params['id_entidade'] = $this->sessao->id_entidade;
            $params['bl_ativo'] = 1;
            $repositoryName = 'G2\Entity\VwAplicador';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * findBy VwAvaliacaoAplicacao
     * parametros inicializados : id_entidade (sessao), bl_ativo (true), bl_datativa (true) indica se a data da aplicação é válida
     * bl_limitealunos (false) indica se o limite de alunos agendados não foi excedido
     * @param array $params
     * @return array|string
     */
    public function findVwAplicacoes(array $params = array())
    {
        try {
            $repository = '\G2\Entity\VwAvaliacaoAplicacao';

            $params = array_merge(array(
                'id_entidade' => $this->sessao->id_entidade,
                'bl_ativo' => 1,
                'bl_dataativa' => 1, // data valida, provas que ainda nao aconteceram e estao no prazo limite de alteração
                'bl_limitealunos' => 0, //verifica o limite de alunos por aplicacao
            ), $params);

            $results = $this->findBy($repository, $params, array('dt_aplicacao' => 'ASC', 'hr_inicioprova' => 'ASC'));

            return $results ? $results : array();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array
     */
    public function findVwAplicacoesPorProva(array $params = array())
    {
        try {
            if (!isset($params['id_avaliacao']) || !$params['id_avaliacao']) {
                throw new \Exception('O paramatro id_avaliacao é obrigatório');
            }

            $params = array_merge(array(
                'id_entidade' => $this->sessao->id_entidade,
                'bl_ativo' => 1,
                'bl_dataativa' => 1,
                'bl_limitealunos' => 0,
                'aar.id_avaliacao' => $params['id_avaliacao']
            ), $params);

            unset($params['id_avaliacao']);

            $dql = "SELECT aa FROM \G2\Entity\VwAvaliacaoAplicacao aa JOIN \G2\Entity\VwAvaliacaoAplicacaoRelacao aar WITH aar.id_avaliacaoaplicacao = aa.id_avaliacaoaplicacao";
            $dql .= ' WHERE 1 = 1';

            foreach ($params as $attr => $val) {
                if (!strstr($attr, '.')) {
                    $attr = 'aa.' . $attr;
                }
                $dql .= " AND {$attr} = '{$val}'";
            }

            $dql .= ' ORDER BY aa.dt_aplicacao ASC, aa.hr_inicioprova ASC, aa.dt_cadastro ASC';

            $results = $this->em->createQuery($dql)->getResult();

            return $results ? $results : array();
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * findBY entity VwDisciplinasAgendamento
     * @param array $params
     * @return array|string
     */
    public function findByVwDisciplinasAgendamento($params = array(), $order = null)
    {
        try {
            //$params['bl_status'] = 0;
            return $this->findBy('G2\Entity\VwDisciplinasAgendamento', $params, $order);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * entity VwMunicipioAplicacao
     * parametros inicializado : id_entidade (via sessao)
     * @param array $params
     * @return array|string
     */
    public function findByVwMunicipioAplicacao($params = array())
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwMunicipioAplicacao';
            return $this->findBy($repositoryName, $params);
//            \Zend_Debug::dump($this->findBy($repositoryName, $params));die;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * retorna o id_usuario, a partir da data de matrícula (em algumas situações não vem setado no formulário de agendamento)
     * @param array $data
     * @return \the
     * @throws \Zend_Exception
     */
    public function retornaIdUsuario(array $data)
    {
        if (!(isset($data['id_usuario'])) || $data['id_usuario'] == '') {
            $to = new \MatriculaTO();
            $to->setId_matricula($data['id_matricula']);
            $to->fetch(true, true, true);

            if ($to->getId_usuario()) {
                $usuario = $to->getId_usuario();
            }
        } else {
            $usuario = $data['id_usuario'];
        }

        return $usuario;
    }


    /**
     * Método responsável pelas atividades: agendamento, liberação e abono
     * @param array $data
     * @return \Ead1_Mensageiro|mensageiro
     */
    public function salvaAvaliacaoAgendamento(array $data)
    {

        if (!(isset($data['id_matricula'])) || $data['id_matricula'] == '') {
            $mensageiro = new \Ead1_Mensageiro('Nenhuma matrícula informada!', \Ead1_IMensageiro::ERRO);
            return $mensageiro;
        }

        switch ($data['id_situacao']) {
            case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO:
                return $this->salvarAgendamento($data);
            case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_LIBERADO:
                return $this->salvarLiberacaoAgendamento($data);
            case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_ABONADO:
                return $this->salvarAbonoAgendamento($data);
            case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_CANCELADO:
                return $this->cancelarAgendamento($data);
            case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO:
                return $this->salvarReagendamento($data);
            default:
                return new \Ead1_Mensageiro('Nenhuma situação válida foi informada!', \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Método para salvar agendamento com sitação liberado (129) - menu -> Secretaria Gerencia Prova
     * @param array
     * @return \Ead1_Mensageiro
     */
    public function salvarLiberacaoAgendamento($data)
    {
        $id_usuariocadastro = !empty($data['id_usuariocadastro']) ? $data['id_usuariocadastro'] : $this->sessao->id_usuario;

        if (!$id_usuariocadastro) {
            throw new \Exception("É necessário informar o usuário de cadastro");
        }

        $entidadeCadastro = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
        $usuarioCadastro = $this->getReference('\G2\Entity\Usuario', $id_usuariocadastro);
        $entityMatricula = $this->find('\G2\Entity\Matricula', (int)$data['id_matricula']);
        $idUsuarioAluno = $entityMatricula->getId_usuario()->getId_usuario();

        $entity = new \G2\Entity\AvaliacaoAgendamento();

        $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

        /**********************
         * Definindo atributos (PADRAO)
         **********************/
        if (!array_key_exists('id_situacao', $data) || !$data['id_situacao']) {
            $data['id_situacao'] = \G2\Entity\AvaliacaoAgendamento::LIBERADO;
        }
        if (!array_key_exists('bl_provaglobal', $data) || $data['bl_provaglobal'] === null) {
            // Se a Configuracao da Entidade no bl_provaspordisciplina for TRUE, entao o bl_provaglobal sera FALSE
            $data['bl_provaglobal'] = !$bl_provapordisciplina;
        }
        if (!isset($data['bl_automatico'])) {
            $data['bl_automatico'] = false;
        }

        //seta os atribustos
        $entity->setBl_ativo(1)
            ->setId_avaliacao($this->getReference('\G2\Entity\Avaliacao', (int)$data['id_avaliacao']))
            ->setId_matricula($entityMatricula)
            ->setId_situacao($this->getReference('\G2\Entity\Situacao', $data['id_situacao']))
            ->setId_usuario($entityMatricula->getId_usuario())
            ->setDt_cadastro(new \DateTime())
            ->setId_tipodeavaliacao((int)$data['id_tipodeavaliacao'])
            ->setId_usuariocadastro($usuarioCadastro)
            ->setId_entidade($entidadeCadastro)
            ->setBl_provaglobal((int)$data['bl_provaglobal'])
            ->setBl_automatico($data['bl_automatico']);

        $retorno = $this->save($entity);

        if ($entity->getId_avaliacaoagendamento()) {
            $mensageiro = new \Ead1_Mensageiro('Agendamento alterado com Sucesso!', \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($retorno->getId_avaliacaoagendamento());

            // Gerar email de agendamento quando ele for LIBERADO
            if ($data['id_situacao'] == \G2\Entity\AvaliacaoAgendamento::LIBERADO) {
                $data['id_usuario'] = $idUsuarioAluno;
                $this->enviarEmailAgendamentoLiberado($data);
            }
        } else {
            $mensageiro = new \Ead1_Mensageiro('Não foi possível alterar este agendamento!', \Ead1_IMensageiro::ERRO);
            return $mensageiro;
        }

        // SALVAR A RELACAO DO AGENDAMENTO COM AS DISCIPLINAS
        if (isset($data['bl_provaglobal']) && $data['bl_provaglobal']) {
            //Se for prova global, libera o agendamento para TODAS as disciplinas
            $referenciaDisciplina = $this->salvarAgendamentoRef($mensageiro->getId(), $data['id_matricula'], $data['id_avaliacao'], $data['id_tipodeavaliacao']);
        } else {
            // Se nao for prova global, libera o agendamento somente para as disciplinas enviadas no parametro (array id_avaliacaoconjuntoreferencia)
            $referenciaDisciplina = $this->salvarAgendamentoRef($mensageiro->getId(), $data['id_matricula'], $data['id_avaliacao'], $data['id_tipodeavaliacao'], $data['id_avaliacaoconjuntoreferencia']);
        }

        // Gerar TRAMITE do Agendamento
        if ($referenciaDisciplina->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $tramite = $this->salvarTramite($data['st_justificativa']);
            if ($tramite->getId()) {
                $this->salvarTramiteAgendamento($mensageiro->getId(), $tramite->getId(), $data['id_situacao']);
            }
        } else {
            return $referenciaDisciplina;
        }

        return $mensageiro;
    }

    /**
     * @param array $dados
     * @return \Ead1_Mensageiro|Mensageiro
     */
    public function enviarEmailAgendamentoLiberado(array $dados)
    {

        try {
            $negocioMensagem = new \G2\Negocio\Mensagem();
            if ($dados['id_tipodeavaliacao'] == 0) {
                $dados['id_mensagempadrao'] = MensagemPadrao::AGENDAMENTO_LIBERADO_FINAL;
            } else {
                $dados['id_mensagempadrao'] = MensagemPadrao::AGENDAMENTO_LIBERADO_RECUPERACAO;
            }
            $dados['st_mensagem'] = 'Agendamento Liberado';

            return $negocioMensagem->gerarEmailMensagemTextoSitema($dados);

        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);

        }

    }

    /**
     * Método para salvar agendamento com sitação abonado (130) - menu -> Secretaria Gerencia Prova
     * @param array
     * @return \Ead1_Mensageiro
     */
    public function salvarAbonoAgendamento(array $data)
    {
        $data['id_situacao'] = \G2\Entity\AvaliacaoAgendamento::ABONADO;
        return $this->salvarLiberacaoAgendamento($data);

        //A FUNCAO ESTA REDUNDANTE, ENTAO SO REDIRECIONEI
        /*
        try {
            $entidadeCadastro = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
            $usuarioCadastro = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);

            $entity = new \G2\Entity\AvaliacaoAgendamento();
            $entityMatricula = $this->find('\G2\Entity\Matricula', (int)$data['id_matricula']);
            //seta os atribustos
            $entity->setBl_ativo(1)
                ->setId_avaliacao($this->getReference('\G2\Entity\Avaliacao', (int)$data['id_avaliacao']))
                ->setId_matricula($entityMatricula)
                ->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::ABONADO))
                ->setId_usuario($entityMatricula->getId_usuario())
                ->setDt_cadastro(new \DateTime())
                ->setId_tipodeavaliacao((int)$data['id_tipodeavaliacao'])
                ->setId_usuariocadastro($usuarioCadastro)
                ->setId_entidade($entidadeCadastro);

            if ($data['id_tipodeavaliacao'] == 0) {
                $entity->setBl_provaglobal(1);
            } else {
                $entity->setBl_provaglobal((int)$data['bl_provaglobal']); //valida apenas para provas de recuperação
            }

            $retorno = $this->save($entity);

            if ($entity->getId_avaliacaoagendamento()) {
                $mensageiro = new \Ead1_Mensageiro('Agendamento Abonado com Sucesso!', \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($retorno->getId_avaliacaoagendamento());
            } else {
                $mensageiro = new \Ead1_Mensageiro('NÃ£o foi possÃ­vel realizar o Abono!', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }

            if ((isset($data['bl_provaglobal']) && $data['bl_provaglobal'] == 1) || $data['id_tipodeavaliacao'] == 0) {
                //se for prova global ou prova final salva avaliação para todas disciplinas
                $referenciaDisciplina = $this->salvarAgendamentoRef($mensageiro->getId(), $data['id_matricula'], $data['id_avaliacao'], $data['id_tipodeavaliacao']);
            } else {
                //salva disciplinas relacionadas
                $referenciaDisciplina = $this->salvarAgendamentoRef($mensageiro->getId(), $data['id_matricula'], $data['id_avaliacao'], $data['id_tipodeavaliacao'], $data['id_avaliacaoconjuntoreferencia']);
            }

            if ($referenciaDisciplina->getTipo() == \Ead1_IMensageiro::MENSAGEM_SUCESSO) {
                $tramite = $this->salvarTramite($data['st_justificativa']);
                if ($tramite->getId()) {
                    $this->salvarTramiteAgendamento($mensageiro->getId(), $tramite->getId(), \G2\Entity\AvaliacaoAgendamento::ABONADO);
                }
            } else {
                return $referenciaDisciplina;
            }

        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
        */
    }

    /**
     * Método para salvar agendamento pelo G2, menu -> Secretaria Gerencia Prova
     * @param array
     * @return \Ead1_Mensageiro
     */
    public function salvarAgendamento(array $params)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();

            // E necessario que antes o agendamento ja tenha sido LIBERADO ou ABONADO, entao obrigatorio o id_avaliacaoagendamento
            if (!isset($params['id_avaliacaoagendamento']) || !$params['id_avaliacaoagendamento']) {
                $mensageiro->setMensageiro('Parâmetros insuficientes para realizar o agendamento.', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }

            /**
             * @var \G2\Entity\AvaliacaoAgendamento $entity
             */
            $entity = $this->find($this->repositorio, $params['id_avaliacaoagendamento']);

            // Verificar se ja o mesmo aluno possui algum agendamento no mesmo horario do id_avaliacaoaplicacao.
            // Se possuir, nao pode permitir que ele use o mesmo
            $aplicacao_usada = $this->findAlunoAplicacoesUsadas(array(
                'id_matricula' => $entity->getId_matricula()->getId_matricula(),
                'id_avaliacaoaplicacao' => $params['id_avaliacaoaplicacao']
            ));

            if ($aplicacao_usada) {
                $mensageiro->setMensageiro('Este aluno já está usando a Aplicação selecionada em outro agendamento.', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }

            $id_usuariocadastro = isset($params['id_usuariocadastro']) && $params['id_usuariocadastro'] ? $params['id_usuariocadastro'] : $params['id_usuario'];

            // Setar os atributos
            $entity
                ->setId_avaliacaoaplicacao($this->getReference('\G2\Entity\AvaliacaoAplicacao', $params['id_avaliacaoaplicacao']))
                ->setDt_agendamento($this->converteDataBanco(date('d/m/Y')))
                ->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::AGENDADO))
                ->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $id_usuariocadastro));

            $this->save($entity);

            // Gerar um tramite com a justificativa, para constar no historico
            if ($entity->getId_avaliacaoagendamento()) {
                $tramite = $this->salvarTramite($params['st_justificativa']);
            }

            if ($tramite->getId()) {
                $tramiteAgendamento = $this->salvarTramiteAgendamento($entity->getId_avaliacaoagendamento(), $tramite->getId(), \G2\Entity\AvaliacaoAgendamento::AGENDADO);
            }

            if ($tramiteAgendamento->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Agendamento realizado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($entity->getId_avaliacaoagendamento());
                $this->gerarEmailAgendamentoparaAluno($entity->getId_avaliacaoagendamento());
                return $mensageiro;

            } else {
                return $tramiteAgendamento;
            }
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Método específico para reagendamento de provas (tb_avaliacaoagendamento)
     * @param array
     * @return \Ead1_Mensageiro
     */
    public function salvarReagendamento(array $params)
    {
        try {

            if (!(isset($params['id_avaliacaoagendamento'])) || $params['id_avaliacaoagendamento'] == '') {
                $mensageiro = new \Ead1_Mensageiro('Parametros insuficientes!', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }

            $entity = $this->find($this->repositorio, $params['id_avaliacaoagendamento']);

            // Verificar se ja o mesmo aluno possui algum agendamento no mesmo horario do id_avaliacaoaplicacao.
            // Se possuir, nao pode permitir que ele use o mesmo
            $aplicacao_usada = $this->findAlunoAplicacoesUsadas(array(
                'id_matricula' => $entity->getId_matricula()->getId_matricula(),
                'id_avaliacaoaplicacao' => $params['id_avaliacaoaplicacao']
            ));

            if ($aplicacao_usada) {
                return new \Ead1_Mensageiro('Este aluno já está usando a Aplicação selecionada em outro agendamento.', \Ead1_IMensageiro::AVISO);
            }

            if (isset($this->sessao->id_usuario) && $this->sessao->id_usuario) {
                $id_usuariocadastro = $this->sessao->id_usuario;
            } else if (isset($params['id_usuariocadastro']) && $params['id_usuariocadastro']) {
                $id_usuariocadastro = $params['id_usuariocadastro'];
            } else {
                $id_usuariocadastro = $params['id_usuario'];
            }

            //seta os atributos necessários para atualização
            $entity->setId_avaliacaoaplicacao($this->getReference('\G2\Entity\AvaliacaoAplicacao', $params['id_avaliacaoaplicacao']))
                ->setDt_agendamento($this->converteDataBanco(date('d/m/Y')))
                ->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::AGENDADO))
                ->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $id_usuariocadastro))
                ->setBl_automatico(false);

            $retorno = $this->save($entity);

            //gera um tramite com justificativa, para constar no historico
            if ($entity->getId_avaliacaoagendamento()) {
                $tramite = $this->salvarTramite($params['st_justificativa']);
            }

            if ($tramite->getId()) {
                $tramiteAgendamento = $this->salvarTramiteAgendamento($entity->getId_avaliacaoagendamento(), $tramite->getId(), \G2\Entity\AvaliacaoAgendamento::AGENDADO);
                $tramiteAgendamento = $this->salvarTramiteAgendamento($entity->getId_avaliacaoagendamento(), $tramite->getId(), \G2\Entity\AvaliacaoAgendamento::REAGENDADO);
            }

            if ($tramiteAgendamento->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Agendamento realizado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($entity->getId_avaliacaoagendamento());
                $enviarEmail = $this->gerarEmailAgendamentoparaAluno($entity->getId_avaliacaoagendamento());

                return $mensageiro;

            } else {
                return $tramiteAgendamento;
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Método responsável pela atividade: cancelar agendamento
     * @param array
     * @return mensageiro
     */

    public function cancelarAgendamento(array $data)
    {
        try {

            $params = array(
                'id_matricula' => $data['id_matricula'],
                'id_tipodeavaliacao' => $data['id_tipodeavaliacao'],
            );

            // Se for prova por disciplina, cancelar SOMENTE o agendamento em questao
            //if ($this->sessao->bl_provapordisciplina) {
            $params['id_avaliacaoagendamento'] = $data['id_avaliacaoagendamento'];
            //}

            $id = $this->findByVwAvaliacaoAgendamento($params);

            if (!$id) {
                $mensageiro = new \Ead1_Mensageiro('Aluno sem agendamento ativo!', \Ead1_IMensageiro::ERRO);
            } else {
                if (is_array($id)) {
                    foreach ($id as $row) {
                        $id_aval = $row->getId_avaliacaoagendamento();
                        $entity = $this->find($this->repositorio, $id_aval);
                        $entity->setBl_ativo(0);
                        $entity->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::CANCELADO));
                        $retorno = $this->save($entity);
                        $tramite = $this->salvarTramite($data['st_justificativa']);
                        if ($tramite->getId()) {
                            $this->salvarTramiteAgendamento($id_aval, $tramite->getId(), \G2\Entity\AvaliacaoAgendamento::CANCELADO);
                        }
                    }
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Agendamento Cancelado com Sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @param int $id_avaliacaoagendamento
     * @param int $id_matricula
     * @param int $id_avaliacao
     * @param int $id_avaliacaorecupera
     * @param array|null $ar_id_avaliacaoconjuntoreferencia
     * @return \Ead1_Mensageiro
     */
    public function salvarAgendamentoRef($id_avaliacaoagendamento, $id_matricula, $id_avaliacao, $id_avaliacaorecupera, $ar_id_avaliacaoconjuntoreferencia = array())
    {
        try {
            $params = array(
                'id_matricula' => $id_matricula,
                'id_avaliacao' => $id_avaliacao,
                'id_avaliacaorecupera' => $id_avaliacaorecupera,
                'id_entidadeatendimento' => $this->sessao->id_entidade
            );

            if (is_int($ar_id_avaliacaoconjuntoreferencia) && $ar_id_avaliacaoconjuntoreferencia > 0) {
                $ar_id_avaliacaoconjuntoreferencia = array($ar_id_avaliacaoconjuntoreferencia);
            }

            //verifica se vem disciplinas setadas (id_avaliacaoconjuntoreferencia, se não pega todas as disciplinas que aceitam prova presencial (final)
            if (is_array($ar_id_avaliacaoconjuntoreferencia) && $ar_id_avaliacaoconjuntoreferencia) {
                foreach ($ar_id_avaliacaoconjuntoreferencia as $row) {
                    $entity = new \G2\Entity\AvalAgendamentoRef();
                    $entity
                        ->setId_avaliacaoagendamento($id_avaliacaoagendamento)
                        ->setId_avaliacaoconjuntoreferencia($row);
                    $this->save($entity);
                }
            } else {
                $retorno = $this->findByVwAvaliacaoAluno($params);
                if ($retorno) {
                    foreach ($retorno as $row) {
                        $entity = new \G2\Entity\AvalAgendamentoRef();
                        $entity
                            ->setId_avaliacaoagendamento($id_avaliacaoagendamento)
                            ->setId_avaliacaoconjuntoreferencia($row->getId_avaliacaoconjuntoreferencia());
                        $this->save($entity);
                    }
                }
            }

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * Rotina que salva os tramites de todas as atividades do gerencia prova
     * Responsável pelo historioco.
     * @param $st_tramite
     * @param $id_categoriatramite
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarTramite($st_tramite, $id_categoriatramite = 14)
    {
        try {

            $entity = new \G2\Entity\Tramite();
            $entity->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', $id_categoriatramite))
                ->setId_usuario($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario))
                ->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade))
                ->setSt_tramite($st_tramite)
                ->setDt_cadastro(new \DateTime())
                ->setBl_visivel(1);
            $retorno = $this->save($entity);

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($entity->getId_tramite());
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @param $id_avaliacaoagendamento
     * @param $id_tramite
     * @param $id_situacao
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarTramiteAgendamento($id_avaliacaoagendamento, $id_tramite, $id_situacao)
    {

        try {
            $entity = new \G2\Entity\TramiteAgendamento();
            $entity->setId_avaliacaoagendamento($id_avaliacaoagendamento)
                ->setId_tramite($id_tramite)
                ->setId_situacao($id_situacao);
            $retorno = $this->save($entity);

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($entity->getId_tramiteagendamento());
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Salva frequencia e grava um tramite para a marcação
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvarFrequencia($data = array())
    {
        try {
            $entity = $this->find('\G2\Entity\AvaliacaoAgendamento', $data['id_avaliacaoagendamento']);

            if (!($entity instanceof \G2\Entity\AvaliacaoAgendamento)) {
                throw new \Exception('Agendamento não encontrado!');
            }

            $entity->setBl_ativo(0);
            $entity->setNu_presenca($data['nu_presenca']);
            $entity->setId_usuariolancamento($this->sessao->id_usuario);

            if ($this->save($entity)) {
                $tramite = 'Presença lançada para: ' . $data['st_exibe'] . '. Marcado como [' . ($data['nu_presenca'] ? 'Presente' : 'Ausente') . '].';
                $tramite = $this->salvarTramite($tramite, 23);

                $id_matricula = $entity->getId_matricula()->getId_matricula();

                // Verificar se a linha de negócio da entidade da matrícula é "Graduação" e se é "Prova Por Disciplina"
                $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();
                $bl_matriculagrad = $this->getRepository('\G2\Entity\VwMatricula')->isMatriculaGraduacao($id_matricula);

                if ($bl_matriculagrad && $bl_provapordisciplina) {
                    // Buscar a referência da view que possui os dados necessários
                    /** @var \G2\Entity\VwAvaliacaoAgendamento $agendamento */
                    $agendamento = $this->find('\G2\Entity\VwAvaliacaoAgendamento', $entity->getId_avaliacaoagendamento());

                    // Verificar se o lançamento é de uma "Prova Presencial"
                    if ($agendamento->getId_tipoavaliacao() == \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL) {
                        // Registrar referência
                        $id_disciplina = $agendamento->getId_disciplina();

                        // Buscar avaliação do aluno
                        /** @var \G2\Entity\VwAvaliacaoAluno $avaliacaoaluno */
                        $avaliacaoaluno = $this->findOneBy('\G2\Entity\VwAvaliacaoAluno', array(
                            'id_matricula' => $id_matricula,
                            'id_disciplina' => $id_disciplina,
                            'id_avaliacao' => $agendamento->getId_avaliacao()
                        ));

                        // Verificar se lançou frequência como "Presente"
                        if ($data['nu_presenca']) {
                            // Buscar o agendamento automático da recuperação
                            $agendamento_rec = $this->findOneBy('\G2\Entity\VwAvaliacaoAgendamento', array(
                                'bl_ativo' => true,
                                'id_matricula' => $id_matricula,
                                'id_disciplina' => $id_disciplina,
                                'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_RECUPERACAO,
                                'id_situacao' => array(
                                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_LIBERADO,
                                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                                    \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                                )
                            ), array(
                                'dt_agendamento' => 'DESC'
                            ));

                            // Cancelar a liberação do agendamento de prova final (recuperação)
                            if ($agendamento_rec instanceof \G2\Entity\VwAvaliacaoAgendamento) {
                                $this->cancelarAgendamento(array_merge($this->toArrayEntity($agendamento_rec), array(
                                    'st_justificativa' => 'Agendamento cancelado automaticamente'
                                )));
                            }
                        } else {
                            // Salvar a nota do aluno como 0 (zero)
                            $ng_avaliacao = new \G2\Negocio\Avaliacao();
                            $ng_avaliacao->cadastrarAvaliacaoAluno(array_merge($this->toArrayEntity($avaliacaoaluno), array(
                                'dt_avaliacao' => \G2\Utils\Helper::converterData($avaliacaoaluno->getdt_avaliacao() ? $avaliacaoaluno->getdt_avaliacao() : date('Y-m-d')),
                                'id_tiponota' => \G2\Constante\TipoNota::NORMAL,
                                'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOALUNO_ATIVA,
                                'st_nota' => '0'
                            )));

                            // Alterar a situação do agendamento do aluno
                            $this->tornarAlunosAptosAgendamentoPorDisciplina(array(
                                'id_matricula' => $id_matricula,
                                'id_disciplina' => $id_disciplina
                            ));

                            // Realizar o agendamento automaticamente
                            $this->agendarAlunosAptosPrimeiroAgendamento(array(
                                'id_matricula' => $id_matricula,
                                'id_disciplina' => $id_disciplina
                            ), 'Agendamento liberado automaticamente', 'Agendamento realizado automaticamente');
                        }
                    }
                }

                if ($tramite->getId()) {
                    $this->salvarTramiteAgendamento($data['id_avaliacaoagendamento'], $tramite->getId(), ($data['nu_presenca'] ? \G2\Constante\AvaliacaoAgendamento::PRESENCA_PRESENTE : \G2\Constante\AvaliacaoAgendamento::PRESENCA_AUSENTE));
                }
            }

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Presença lançada para: ' . $data['st_exibe']);
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarAgendamentoPortal(array $params)
    {
        try {
            // Se não for informado o tipo de prova (se é recuperação ou não), então marcar como NÃO RECUPERAÇÃO (zero)
            if (empty($params['id_tipodeavaliacao'])) {
                $params['id_tipodeavaliacao'] = 0;
            }

            // Descobrir o id_avaliacao caso não seja enviado por parâmetro
            if (empty($params['id_avaliacao'])) {
                /** @var \G2\Entity\VwAvaliacaoConjuntoRelacao[] $provas */
                $provas = $this->findByProvasCadastradas(array('id_entidade' => $params['id_entidade']));
                foreach ($provas as $prova) {
                    if ((int)$params['id_tipodeavaliacao'] == (int)$prova->getId_avaliacaorecupera()) {
                        $params['id_avaliacao'] = $prova->getId_avaliacao();
                    }
                }
            }

            // Se mesmo após tentar descobrir o id_avaliacao ele ainda for vazio, não permitir agendar
            if (empty($params['id_avaliacao'])) {
                throw new \Exception("Nenhuma avaliação está disponível para agendamento");
            }

            $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

            $entity = null;

            if (!empty($params['id_avaliacaoagendamento'])) {
                $entity = $this->find('\G2\Entity\AvaliacaoAgendamento', $params['id_avaliacaoagendamento']);
            }

            // Verificar se ja o mesmo aluno possui algum agendamento no mesmo horario do id_avaliacaoaplicacao.
            // Se possuir, nao pode permitir que ele use o mesmo
            $aplicacao_usada = $this->findAlunoAplicacoesUsadas(array(
                'id_matricula' => $params['id_matricula'],
                'id_avaliacaoaplicacao' => $params['id_avaliacaoaplicacao']
            ));

            if ($aplicacao_usada) {
                return new \Ead1_Mensageiro('Você já está usando a aplicação selecionada em outro agendamento.', \Ead1_IMensageiro::AVISO);
            }

            if ($entity) {
                $entity->setBl_ativo(1);
                $entity->setId_avaliacaoaplicacao($this->find('\G2\Entity\AvaliacaoAplicacao', $params['id_avaliacaoaplicacao']));
                $entity->setDt_agendamento($this->converteDataBanco(date('d/m/Y')));
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::AGENDADO));
                $save = $this->merge($entity);
            } else {
                $entity = new \G2\Entity\AvaliacaoAgendamento();

                $id_usuario = $this->find('\G2\Entity\Usuario', $params['id_usuario']);

                $entity->setBl_ativo(1)
                    ->setId_avaliacao($this->find('\G2\Entity\Avaliacao', $params['id_avaliacao']))
                    ->setId_entidade($this->find('\G2\Entity\Entidade', $params['id_entidade']))
                    ->setId_matricula($this->find('\G2\Entity\Matricula', $params['id_matricula']))
                    ->setId_avaliacaoaplicacao($this->find('\G2\Entity\AvaliacaoAplicacao', $params['id_avaliacaoaplicacao']))
                    ->setDt_agendamento($this->converteDataBanco(date('d/m/Y')))
                    ->setId_situacao($this->find('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::AGENDADO))
                    ->setId_usuario($id_usuario)
                    ->setId_usuariocadastro($id_usuario)
                    ->setDt_cadastro(new \DateTime())
                    ->setId_tipodeavaliacao($params['id_tipodeavaliacao'])
                    ->setBl_provaglobal(0);

                if (!$bl_provapordisciplina) {
                    $entity->setBl_provaglobal(1); //valida apenas para provas de recuperação
                }

                $save = $this->persist($entity);
            }

            if ($save instanceof \G2\Entity\AvaliacaoAgendamento) {
                $entity = $save;
            }

            if ($entity->getId_avaliacaoagendamento()) {
                if (!$entity->getBl_provaglobal() && !empty($params['id_avaliacaoconjuntoreferencia'])) {
                    $id_avaliacaoconjuntoreferencia = $params['id_avaliacaoconjuntoreferencia'];
                } else {
                    $id_avaliacaoconjuntoreferencia = array();
                }

                $this->salvarAgendamentoRef($entity->getId_avaliacaoagendamento(), $params['id_matricula'], $params['id_avaliacao'], $params['id_tipodeavaliacao'], $id_avaliacaoconjuntoreferencia);

                $tramite = $this->salvarTramite('Agendamento realizado pelo aluno.');
                if ($tramite->getId()) {
                    $this->salvarTramiteAgendamento($entity->getId_avaliacaoagendamento(), $tramite->getId(), 68);
                }
            }

            $mensageiro = new \Ead1_Mensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($entity->getId_avaliacaoagendamento());
            $mensageiro->setText('Agendamento realizado com sucesso!');

            $this->gerarEmailAgendamentoparaAluno($entity->getId_avaliacaoagendamento());

        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function salvarReagendamentoPortal(array $params)
    {
        try {

            // Verificar se ja o mesmo aluno possui algum agendamento no mesmo horario do id_avaliacaoaplicacao.
            // Se possuir, nao pode permitir que ele use o mesmo
            $aplicacao_usada = $this->findAlunoAplicacoesUsadas(array(
                'id_matricula' => $params['id_matricula'],
                'id_avaliacaoaplicacao' => $params['id_avaliacaoaplicacao']
            ));

            if ($aplicacao_usada) {
                return new \Ead1_Mensageiro('Você já está usando a aplicação selecionada em outro agendamento.', \Ead1_IMensageiro::AVISO);
            }

            /**
             * @var \G2\Entity\AvaliacaoAgendamento $entity
             */
            $entity = $this->find($this->repositorio, $params['id_avaliacaoagendamento']);
            $situacaoAtual = $entity->getId_situacao()->getId_situacao();

            $entity->setId_avaliacaoaplicacao($this->find('\G2\Entity\AvaliacaoAplicacao', $params['id_avaliacaoaplicacao']));
            $entity->setDt_agendamento($this->converteDataBanco(date('d/m/Y')));
            $entity->setId_situacao($this->find('\G2\Entity\Situacao', \G2\Entity\AvaliacaoAgendamento::AGENDADO));
            $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $params['id_usuario']));

            $retorno = $this->save($entity);

            if ($entity->getId_avaliacaoagendamento()) {
                $tramite = $this->salvarTramite($situacaoAtual == \G2\Entity\AvaliacaoAgendamento::AGENDADO ? 'Reagendamento realizado pelo aluno.' : 'Agendamento realizado pelo aluno.');

                if ($tramite->getId()) {
                    $tramiteAgendamento = $this->salvarTramiteAgendamento($entity->getId_avaliacaoagendamento(), $tramite->getId(), ($situacaoAtual == \G2\Entity\AvaliacaoAgendamento::AGENDADO ? \G2\Entity\AvaliacaoAgendamento::REAGENDADO : \G2\Entity\AvaliacaoAgendamento::AGENDADO));

                    if ($tramiteAgendamento) {
                        $mensageiro = new \Ead1_Mensageiro('Reagendamento realizado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                        //salva tramite
                        $mensageiro->setId($entity->getId_avaliacaoagendamento());
                        $enviarEmail = $this->gerarEmailAgendamentoparaAluno($entity->getId_avaliacaoagendamento());
                    }
                }
            }

        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }


    /**
     * Verifica o dado e formata para Hora:minutos
     * @param $hora
     * @return \DateTime|string|\Zend_Date
     */
    private function converteHoraTextoAgendamento($hora)
    {
        if (!empty($hora)) {
            return Helper::converterData($hora, "H:i");
        }
        return "-";
    }


    /**
     * Monta o texto
     * @param array $dados
     * @return string
     */
    public function retornaTextoMensagemAgendamento(array $dados)
    {
        $htmlTextoMensagem = '';

        $cpf = Helper::mask($dados['st_cpf'], '###.###.###-##');

        $configuracao = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
            'id_entidade' => $dados['id_entidade'],
            'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO
        ));

        $hr_inicio = $this->converteHoraTextoAgendamento($dados['hr_inicio']);
        $hr_fim = $this->converteHoraTextoAgendamento($dados['hr_fim']);
        $dt_aplicacao = (!empty($dados['dt_aplicacao']) && !empty($dados['dt_aplicacao']['date'])
            ? Helper::converterData($dados['dt_aplicacao']['date'], "d/m/Y") : '-');


        if ($configuracao instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao
            && $configuracao->getSt_valor() == \G2\Constante\LinhaDeNegocio::GRADUACAO
        ) {
            $htmlTextoMensagem .= '' .
                '<p>' .
                '    <strong>DATA:</strong> ' . $dt_aplicacao .
                '</p>' .
                '<p>' .
                '    <strong>HORÁRIO:</strong> ' . $hr_inicio . ' as ' . $hr_fim .
                '</p>' .
                '<p>' .
                '    <strong>Nome:</strong> ' . $dados['st_nomecompleto'] . '<br />' .
                '    <strong>CPF:</strong> ' . $cpf . '<br />' .
                '    <strong>E-mail:</strong> ' . $dados['st_email'] . '<br />' .
                '    <strong>Matrícula:</strong> ' . $dados['id_matricula'] .
                '</p>' .
                '<p>' .
                '    <strong>Curso:</strong> ' . $dados['st_projetopedagogico'] . '<br />' .
                '    <strong>Disciplina:</strong> ' . $dados['st_disciplina'] . '<br />' .
                '    <strong>Tipo de Prova:</strong> ' . $dados['st_avaliacao'] . '<br />' .
                '    <strong>Endereço:</strong> ' . ($dados['st_endereco'] ? $dados['st_endereco'] : '-') . '<br />' .
                '';

            if ($dados['nu_numero']) {
                $htmlTextoMensagem .= '<strong>Número:</strong> ' . $dados['nu_numero'] . '<br />';
            }

            if ($dados['st_complemento']) {
                $htmlTextoMensagem .= '<strong>Complemento:</strong> ' . $dados['st_complemento'] . '<br />';
            }

            if ($dados['st_bairro']) {
                $htmlTextoMensagem .= '<strong>Bairro:</strong> ' . $dados['st_bairro'] . '<br />';
            }

            if ($dados['st_cidade']) {
                $htmlTextoMensagem .= '<strong>Cidade:</strong> ' . $dados['st_cidade'];

                if ($dados['sg_uf']) {
                    $htmlTextoMensagem .= ' / ' . $dados['sg_uf'];
                }

                $htmlTextoMensagem .= '<br />';
            }

            $htmlTextoMensagem .= '' .
                '</p>' .
                '<p>' .
                '    • É obrigatório a apresentação do documento de identificação com foto para a realização da prova.<br />' .
                '    • Após o início da prova, você terá 15 minutos de tolerância para comparecer ao local de realização.<br />' .
                '    • A prova será composta por questões objetivas e/ou subjetivas, onde versarão os assuntos abordados na disciplina.<br />' .
                '    • Não será permitida a consulta de nenhum material (apostilas, cadernos, doutrinas, códigos, monografias).<br />' .
                '    • Não será permitido o uso de folha de rascunho.<br />' .
                '    • Não será permitido o uso de aparelhos eletrônicos, incluído celulares e smartphones. Estes devem estar desligados.<br />' .
                '    • Após a realização da prova, teremos um prazo de até 14 dias úteis para o lançamento das notas na plataforma.<br />' .
                '    • O aluno que não comparecer à prova receberá zero. Sendo assim, estará automaticamente em prova final.<br />' .
                '    • A solicitação para vistas e revisão de prova deverá ser realizada até 10 dias úteis após o lançamento da nota na plataforma virtual.<br />' .
                '    • Caso não concorde com a data agendada para a realização da prova, você poderá escolher uma nova data no Portal do Aluno, até a data limite de agendamento estabelecida pela Faculdade.' .
                '</p>' .
                '<p>' .
                '    Em caso de dúvida, entre em contato conosco através do Serviço de Atenção ao Aluno – SAA.' .
                '</p>' .
                '<p>' .
                '    Atenciosamente,<br />' .
                '    Faculdade UnYLeYa ' .
                '</p>' .
                '';
        } else {
            $htmlTextoMensagem .= '<p><strong>DATA: </strong>';
            $htmlTextoMensagem .= $dt_aplicacao;
            $htmlTextoMensagem .= ' </p><p><strong>HORÁRIO: </strong>';
            $htmlTextoMensagem .= $hr_inicio;
            $htmlTextoMensagem .= '&nbsp;as&nbsp;' . $hr_fim;

            $htmlTextoMensagem .= '</p><p><strong>Nome: </strong>' . $dados['st_nomecompleto'];
            $htmlTextoMensagem .= '<br /><strong>CPF: </strong>' . $cpf;
            $htmlTextoMensagem .= '<br /><strong>E-mail: </strong>' . $dados['st_email'];
            $htmlTextoMensagem .= '<br /><strong>Matrícula: </strong>' . $dados['id_matricula'];

            $htmlTextoMensagem .= '</p><p><strong>Curso: </strong>' . $dados['st_projetopedagogico'];
            $htmlTextoMensagem .= '<br /><strong>&nbsp</strong>';
            $htmlTextoMensagem .= '<br /><strong>Tipo de Prova: </strong>' . $dados['st_avaliacao'];


            $htmlTextoMensagem .= '<br /><strong>Endereço: </strong>';
            $htmlTextoMensagem .= ($dados['st_endereco'] == '') ? '-' : $dados['st_endereco'];
            $htmlTextoMensagem .= ($dados['nu_numero'] == '') ? '' : '<br /><strong>Número:</strong> ' . $dados['nu_numero'];
            $htmlTextoMensagem .= ($dados['st_complemento'] == '') ? '' : '<br /><strong>Complemento:</strong> ' . $dados['st_complemento'];
            $htmlTextoMensagem .= ($dados['st_bairro'] == '') ? '' : '<br /><strong>Bairro:</strong> ' . $dados['st_bairro'];
            $htmlTextoMensagem .= ($dados['st_cidade'] == '') ? '' : '<br /><strong>Cidade:</strong> ' . $dados['st_cidade'] . (($dados['sg_uf'] == '') ? '' : ' / ' . $dados['sg_uf']);
            $htmlTextoMensagem .= ($dados['st_telefoneaplicador'] == '') ? '' : '<br /><strong>Telefone do Aplicador:</strong> ' . $dados['st_telefoneaplicador'] . '</p>';
            $htmlTextoMensagem .= '</p>
                        <p>
                        • É necessária a apresentação do documento de identidade para a realização da prova. <br />
                        • Após o início da prova, você terá 30 minutos de tolerância para comparecer ao local de realização.<br />
                        • A prova será composta por questões objetivas e/ou subjetivas, onde versarão os assuntos abordados
                        nos módulos do curso e/ou nos conteúdos disponibilizados pelo tutor na plataforma virtual.<br />
                        • Não será permitida a consulta de nenhum material (apostilas, cadernos, doutrinas, códigos,
                        monografias).<br />
                        • A defesa do TCC será realizada através de um questionário anexado no final da prova.<br />
                        • Após a realização da prova, teremos um prazo de até 20 dias úteis para o lançamento das notas na
                        plataforma.<br />
                        • O aluno que não comparecer à prova ou não desmarcá-la, com até uma semana de antecedência, deverá
                        apresentar atestado, caso contrário, pagará R$ 70,00 pela segunda chamada.<br />
                        • A solicitação para vistas e revisão de prova deverá ser realizada até 05 dias após o lançamento da
                        nota na plataforma virtual.<br />
                         </p>
                        <p>
                            Em caso de dúvida, entre em contato conosco através do Serviço de Atenção ao Aluno – SAA.
                        </p>';
        }

        return utf8_encode($htmlTextoMensagem);
    }

    /**
     * @param int $id_avaliacaoagendamento
     * @return \Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function gerarEmailAgendamentoparaAluno($id_avaliacaoagendamento)
    {
        try {

            $retornaParametrosAgendamento = $this->findOneBy('\G2\Entity\VwAvaliacaoAgendamento', array('id_avaliacaoagendamento' => $id_avaliacaoagendamento));
            if (!$retornaParametrosAgendamento) {
                return new \Ead1_Mensageiro('Não foi encontrado um agendamento válido', \Ead1_IMensageiro::AVISO);
            }

            $arrayDados = $this->toArrayEntity($retornaParametrosAgendamento);

            $textoMensagem = utf8_decode($this->retornaTextoMensagemAgendamento($arrayDados));
            $assuntoEmail = 'CARTA DE CONFIRMAÇÃO DE AGENDAMENTO DE PROVA';
            return $this->gerarEmailGeralAgendamento($textoMensagem, $assuntoEmail, $arrayDados['id_usuario']);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }


    }

    /**
     * @param string $textoMensagem
     * @param string $tituloEmail
     * @param int $id_usuario
     * @return \Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function gerarEmailGeralAgendamento($textoMensagem, $tituloEmail, $id_usuario)
    {
        try {
            $mensagemTO = new \MensagemTO();
            $mensagemTO->setSt_mensagem($tituloEmail);
            $mensagemTO->setSt_texto($textoMensagem);
            $mensagemTO->setId_usuariocadastro((int)$this->sessao->id_usuario);
            $mensagemTO->setId_entidade((int)$this->sessao->id_entidade);


            $emailConfig = $this->findOneBy('\G2\Entity\EmailConfig', array('id_entidade' => $this->sessao->id_entidade));
            if (!$emailConfig) {
                return new \Ead1_Mensageiro('Não foi encontrado nenhuma configuraçao de e-mail válida', \Ead1_IMensageiro::AVISO);
            }

            $envioMensagemTO = new \EnvioMensagemTO();
            $envioMensagemTO->setId_tipoenvio(\TipoEnvioTO::EMAIL);
            $envioMensagemTO->setId_emailconfig($emailConfig->getId_emailconfig());

            $vwPessoaTO = new \VwPessoaTO();
            $vwPessoaTO->setId_usuario($id_usuario);
            $vwPessoaTO->setId_entidade((int)$this->sessao->id_entidade);
            $pessoaBO = new \PessoaBO();
            if ($pessoa = $pessoaBO->retornarVwPessoa($vwPessoaTO)) {
                $vwPessoaTO = \Ead1_TO_Dinamico::encapsularTo($pessoa, new \VwPessoaTO());
                $vwPessoaTO = $vwPessoaTO [0];
            } else {
                throw new \Zend_Exception('Erro ao Retornar Pessoa.');
            }
            $envioDestinatarioTO = new \EnvioDestinatarioTO();
            $envioDestinatarioTO->setId_usuario($vwPessoaTO->getId_usuario());
            $envioDestinatarioTO->setId_tipodestinatario(\TipoDestinatarioTO::DESTINATARIO);
            $envioDestinatarioTO->setSt_nome($vwPessoaTO->getSt_nomecompleto());
            $envioDestinatarioTO->setSt_endereco($vwPessoaTO->getSt_email());
            $arrEnvioDestinatario = array($envioDestinatarioTO);

            $mensagemBO = new \MensagemBO();

            //echo '<pre>' . print_r($mensagemTO, true);exit;
            return $mensagemBO->procedimentoCadastrarMensagemEnvioDestinatarios($envioMensagemTO, $mensagemTO, $arrEnvioDestinatario, false);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo que pesquisa a entity VwAvaliacaoConjuntoRelacao
     * Parametros padrão: id_entidade = sessao
     * id_tipoavaliacao = 4 (referente a provas presenciais - final)
     * Exclui o id_
     */

    public function findProvasCadastradas($params = null)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoConjuntoRelacao');
            $array_search = array('vw.id_avaliacao', 'vw.st_avaliacao', 'vw.id_avaliacaorecupera'
            , 'vw.st_avaliacaorecupera', 'vw.id_tipoavaliacao', 'vw.nu_valor'
            , 'vw.bl_recuperacao', 'vw.st_tipoavaliacao', 'vw.id_entidade', 'vw.id_situacao');
            $query = $repo->createQueryBuilder("vw")
                ->distinct(true)
                ->select($array_search)
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade)
                ->andWhere('vw.id_tipoavaliacao = :id_tipoavaliacao')
                ->setParameter('id_tipoavaliacao', 4)
                ->orderBy('vw.st_avaliacao');
            $result = $query->getQuery()->getResult();
            return $result;
        } catch (\Exception $e) {
            // \Zend_Debug::dump($e->getMessage());die;
            return $e->getMessage();
        }
    }

    public function tornarAlunosAptosAgendamentoPorDisciplina($params = array(), $id_usuariocadastro = null)
    {
        $mensageiro = new \Ead1_Mensageiro();

        $this->beginTransaction();
        try {
            $results = $this->findCustom('G2\Entity\VwAlunosAptosAgendamentoPorDisciplina', $params);

            if ($results && is_array($results)) {
                foreach ($results as $result) {
                    /**
                     * @var \G2\Entity\MatriculaDisciplina $entity
                     */
                    $entity = $this->find('\G2\Entity\MatriculaDisciplina', $result->getId_matriculadisciplina());

                    $id_situacao = \G2\Constante\Situacao::TB_MATRICULA_APTO;

                    // Verificar se está apto para a avaliação de recuperação
                    // Se possuir as notas lancadas, verificar se possui média. Se não possuir, liberar agendamento da recuperacão.
                    if (!is_null($result->getSt_notaatividade()) && !is_null($result->getSt_notaead()) && !is_null($result->getSt_notafinal()) && !$result->getBl_aprovado()) {
                        // Verificar presença no último agendamento
                        $ultimo_agendamento = $this->findOneBy('\G2\Entity\VwAvaliacaoAgendamento', array(
                            'id_matricula' => $result->getId_matricula(),
                            'id_disciplina' => $result->getId_disciplina(),
                            'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL,
                            'id_situacao' => array(
                                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                            )
                        ), array(
                            'dt_agendamento' => 'DESC'
                        ));

                        if ($ultimo_agendamento instanceof \G2\Entity\VwAvaliacaoAgendamento && !is_null($ultimo_agendamento->getNu_presenca())) {
                            $id_situacao = \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_APTO_RECUPERACAO;
                        }
                    }

                    // Atualizar os dados somente se estiver definindo uma situação diferente
                    if ($id_situacao != $entity->getId_situacaoagendamento()->getId_situacao()) {
                        $entity->setId_situacaoagendamento($this->getReference('\G2\Entity\Situacao', $id_situacao));
                        $entity->setDt_aptoagendamento(date('Y-m-d H:i:s'));
                        $this->save($entity);

                        // Salvar tramite aluno apto/não apto
                        $ng_tramite = new \G2\Negocio\Tramite();
                        $ng_tramite->salvarTramiteAptoAgendamento($result, $id_situacao, $id_usuariocadastro, $result->getId_entidade());
                    }
                }

                $mensageiro->setMensageiro([], \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setText('A Situação do Agendamento das Disciplinas foi alterada para Apto');
            } else {
                $mensageiro->setMensageiro([], \Ead1_IMensageiro::AVISO);
                $mensageiro->setText('Não foram encontrados alunos que possuam uma sala de aula iniciada e que ainda não estejam aptos');
            }
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            $mensageiro->setMensageiro(array(
                __FILE__ . ':' . __LINE__,
                $e->getMessage(),
            ), \Ead1_IMensageiro::ERRO);
            $mensageiro->setText('Houve um erro ao tentar alterar a Situação do Agendamento dos alunos.');
        }

        return $mensageiro;
    }

    /**
     * @param int $id_matricula
     * @return array
     * @throws \Exception
     */
    public function findDisciplinasAptoAgendamento($id_matricula)
    {
        $arr_return = array();

        $en_matricula = $this->find('\G2\Entity\Matricula', $id_matricula);

        if (!$en_matricula instanceof \G2\Entity\Matricula) {
            throw new \Exception('Matrícula não encontrada.');
        }

        $en_confg = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
            'id_entidade' => $en_matricula->getId_entidadematricula(),
            'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::AVALIACAO_POR_DISCIPLINA,
            'st_valor' => true
        ));

        if ($en_confg instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao) {
            $disciplinas = $this->findBy('\G2\Entity\VwMatriculaAlocacao', array(
                'id_matricula' => $id_matricula,
                'id_situacaoagendamento' => array(
                    \G2\Constante\Situacao::TB_MATRICULA_APTO,
                    \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_APTO_RECUPERACAO
                )
            ), array(
                'dt_inicio' => 'DESC'
            ));

            if ($disciplinas && is_array($disciplinas)) {
                foreach ($disciplinas as $disciplina) {
                    $arr_return[$disciplina->getId_disciplina()] = array(
                        'id_disciplina' => $disciplina->getId_disciplina(),
                        'st_disciplina' => $disciplina->getSt_disciplina(),
                        'id_situacaoagendamento' => $disciplina->getId_situacaoagendamento()
                    );
                }
            }
        }

        return array_values($arr_return);
    }

    public function findAvaliacoesDisciplinasAgendamento($params)
    {
        if (!isset($params['id_matricula']) || !$params['id_matricula']) {
            throw new \Exception('O parâmetro Matrícula é necessário.');
        }

        $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

        if ($bl_provapordisciplina && (!isset($params['id_disciplina']) || !$params['id_disciplina'])) {
            throw new \Exception('O parâmetro Disciplina é necessário.');
        }

        $linhadenegocio = (new \G2\Negocio\EsquemaConfiguracao())
            ->retornaItemPorEntidade(\G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO, $this->sessao->id_entidade);

        $ar_return = array();

        $ng_avaliacao = new \G2\Negocio\Avaliacao();
        $avaliacoes = $ng_avaliacao->findVwAvaliacaoAluno($params, array(
            'bl_recuperacao' => 'ASC',
            'id_avaliacaorecupera' => 'ASC',
            'id_tipoavaliacao' => 'DESC',
            'id_avaliacao' => 'ASC',
            'st_avaliacao' => 'ASC'
        ));

        $ignorar_recuperacao = true;
        $colunas_chamadas = 0;
        $id_tipodeavaliacao = 0;

        foreach ($avaliacoes as $key => $avaliacao) {
            $id_avaliacao = $avaliacao->getId_avaliacao();
            $is_recuperacao = $avaliacao->getBl_recuperacao()
                || $avaliacao->getId_avaliacaorecupera()
                || $avaliacao->getId_tipoavaliacao() == \G2\Constante\TipoAvaliacao::AVALIACAO_RECUPERACAO;

            //$index = $id_avaliacao . $row['id_tipodeavaliacao'];
            $index = $id_avaliacao;

            if (isset($ar_return[$index])) {
                continue;
            }

            $row = array(
                'id_avaliacao' => $avaliacao->getId_avaliacao(),
                'st_avaliacao' => $avaliacao->getSt_avaliacao(),
                'id_evolucao' => $avaliacao->getId_evolucao(),
                'st_evolucao' => $avaliacao->getSt_evolucao(),
                'id_tipoavaliacao' => $avaliacao->getId_tipoavaliacao(),
                'id_avaliacaoagendamento' => null,
                'id_tipodeavaliacao' => $is_recuperacao ? ++$id_tipodeavaliacao : 0,
                'id_situacaoagendamento' => $avaliacao->getId_situacaoagendamento(),
                'st_situacaomatricula' => $avaliacao->getSt_situacaomatricula(),
                'bl_aplicacaodisponivel' => true,
                'id_avaliacaoconjuntoreferencia' => array(),
                'disciplinas' => array(),
                'agendamentos' => array(),
                'id_linhadenegocio' => ($linhadenegocio && isset($linhadenegocio['st_valor'])) ?
                    (int)$linhadenegocio['st_valor'] : null
            );

            // BUSCAR AGENDAMENTOS
            $filter = array(
                'id_avaliacao' => $id_avaliacao,
                'id_matricula' => $params['id_matricula'],
            );

            // Se a configuracao da Entidade for Prova por Disciplina, entao filtrar o AGENDAMENTO por DISCIPLINA
            if ($bl_provapordisciplina) {
                $filter['id_disciplina'] = $params['id_disciplina'];
            }

            $agendamentos = $this->findByVwAvaliacaoAgendamento($filter, array(
                'id_tipodeavaliacao' => 'ASC',
                'id_chamada' => 'ASC',
                'bl_ativo' => 'ASC',
                'id_avaliacaoagendamento' => 'ASC'
            ));

            // Se a avalação for Recuperação E a flag ignorar_recuperacao for true
            // E não possuir agendamentos nesta avaliação, então ir para a proxima iteracao
            if ($is_recuperacao && $ignorar_recuperacao && !$agendamentos) {
                continue;
            }

            // Definir uma variável boolean que mostra se possui aplicação disponível
            if (!$this->findVwAplicacoesPorProva(array(
                'id_entidade' => $avaliacao->getId_entidadeatendimento(),
                'id_avaliacao' => $id_avaliacao
            ))
            ) {
                $row['bl_aplicacaodisponivel'] = false;
            }

            if (is_array($agendamentos) && $agendamentos) {
                foreach ($agendamentos as $agendamento) {
                    $item = $this->toArrayEntity($agendamento);

                    $item['dt_agendamento'] = $agendamento->getDt_agendamento()
                        ? Helper::converterData($agendamento->getDt_agendamento(), 'd/m/Y') : 'Não agendado';
                    $item['dt_aplicacao'] = $agendamento->getDt_aplicacao()
                        ? Helper::converterData($agendamento->getDt_aplicacao(), 'd/m/Y') : 'Não agendado';

                    $item['hr_inicio'] = $agendamento->getHr_inicio()
                        ? Helper::converterData($agendamento->getHr_inicio(), 'H:i') : null;
                    $item['hr_fim'] = $agendamento->getHr_fim()
                        ? Helper::converterData($agendamento->getHr_fim(), 'H:i') : null;

                    $item['bl_dataativa'] = 0;

                    // O id_avaliacaoagendamento DO ULTIMO ITEM definira o do OBJETO PAI
                    $row['id_avaliacaoagendamento'] = $item['id_avaliacaoagendamento'];

                    // Buscar a Data de Alteração Limite
                    if (!empty($item['id_avaliacaoaplicacao'])) {
                        $aplicacao = $this->find('\G2\Entity\AvaliacaoAplicacao', $item['id_avaliacaoaplicacao']);
                        $dt_alteracaolimite = $aplicacao->getDt_alteracaolimite();

                        $today = new \DateTime(date('Y-m-d'));

                        if ($dt_alteracaolimite && $today <= $dt_alteracaolimite) {
                            $item['bl_dataativa'] = 1;
                        }
                    }

                    $row['agendamentos'][] = $item;
                }
            }

            // Caso nao possua nenhum agendamento, criar um DEFAULT apenas para mostrar na TELA
            $ultimo_agendamento = end($row['agendamentos']);

            if (!$ultimo_agendamento || ($ultimo_agendamento['nu_presenca'] === 0 && !$bl_provapordisciplina)) {
                $new_ag = new \G2\Entity\VwAvaliacaoAgendamento();
                $new_ag->setId_tipodeavaliacao($row['id_tipodeavaliacao']);
                $new_ag->setBl_provaglobal(!$bl_provapordisciplina);
                $row['agendamentos'][] = $this->toArrayEntity($new_ag);
                $ultimo_agendamento = end($row['agendamentos']);
            }

            // Verificar se o aluno esta insatisfatorio EM ALGUMA disciplina do agendamento referente a esta Avaliacao
            $bl_insatisfatorio = array_filter($avaliacoes, function ($item) use (
                $ultimo_agendamento,
                $bl_provapordisciplina
            ) {
                $condicao = $bl_provapordisciplina ?
                    $item->getId_disciplina() &&
                    $item->getId_disciplina() == $ultimo_agendamento['id_disciplina']
                    : $item->getId_avaliacaoagendamento()
                    && $item->getId_avaliacaoagendamento() == $ultimo_agendamento['id_avaliacaoagendamento'];

                return (
                    $condicao
                    && $item->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO
                );
            });

            if ($is_recuperacao && $ultimo_agendamento['nu_presenca'] === null) {
                // Se nao tiver presenca do ultimo agendamento lancada entao IGNORAR a recuperacao (NAO ADD NO ARRAY)
                $ignorar_recuperacao = true;
            } else if ($bl_insatisfatorio) {
                // Se tiver insatisfatorio, NAO DEVERA ignorar a recuperacao
                $ignorar_recuperacao = false;
            }

            // Pegar quantidade de Colunas para Agendamento
            $colunas_chamadas = max($colunas_chamadas, count($row['agendamentos']));

            // BUSCAR DISCIPLINAS E REFERENCIAS
            if ($row['id_avaliacaoagendamento'] && !$ultimo_agendamento['bl_provaglobal'] && !$bl_provapordisciplina) {
                $disciplinas = $this->findByVwDisciplinasAgendamento(array(
                    'id_matricula' => $params['id_matricula'],
                    'id_avaliacao' => $id_avaliacao,
                    'id_avaliacaoagendamento' => $row['id_avaliacaoagendamento'],
                    'bl_agendado' => 1
                ), array(
                    // ORDER BY
                    'st_disciplina' => 'ASC'
                ));
                $method = 'getSt_disciplina';
            } else {
                $disciplinas = $ng_avaliacao->findVwAvaliacaoAluno(array_merge(array(
                    'id_avaliacao' => $id_avaliacao,
                ), $params), array(
                    // ORDER BY
                    'st_tituloexibicaodisciplina' => 'ASC'
                ));
                $method = 'getSt_tituloexibicaodisciplina';
            }

            if (is_array($disciplinas) && $disciplinas) {
                foreach ($disciplinas as $disciplina) {
                    $row['disciplinas'][$disciplina->getId_disciplina()] = array(
                        'id_disciplina' => $disciplina->getId_disciplina(),
                        'st_disciplina' => $disciplina->$method()
                    );
                    $row['id_avaliacaoconjuntoreferencia'][] = $disciplina->getId_avaliacaoconjuntoreferencia();
                }
                $row['disciplinas'] = array_values($row['disciplinas']);
            }

            $ar_return[$index] = $row;
        }

        return array(
            'colunas_chamadas' => $colunas_chamadas,
            'avaliacoes' => array_values($ar_return)
        );
    }

    /**
     * @param array $params
     * @param string $st_just_liberacao
     * @param string $st_just_agendamento
     * @return array|string
     */
    public function agendarAlunosAptosPrimeiroAgendamento(
        $params = array(),
        $st_just_liberacao = 'Primeiro agendamento liberado automaticamente',
        $st_just_agendamento = 'Primeiro agendamento realizado automaticamente'
    )
    {
        $ar_return = array();

        // Buscar disciplinas que os alunos estao APTOS e que ele esteja alocado e que a entidade esteja configurada para PRIMEIRO AGENDAMENTO AUTOMATICO
        $disciplinas = $this->findBy('\G2\Entity\VwAvaliacaoPrimeiroAgendamento', $params, array(
            'id_matricula' => 'ASC',
            'id_tipoavaliacao' => 'ASC',
            'id_avaliacao' => 'ASC'
        ));

        // Se nao encontrar nenhuma disciplina que o aluno esta apto, retornar mensagem
        if (!is_array($disciplinas) || !$disciplinas) {
            return 'Nenhum aluno apto para o primeiro agendamento';
        }

        // O dados da sessão atual são salvos numa variável, porque, caso a ação seja executada por um usuário usando o LINK,
        // A sessão será redefinida (no final) para os dados da sessão que ele estava
        $id_usuario_sessao = $this->sessao->id_usuario;
        $id_entidade_sessao = $this->sessao->id_entidade;

        /**
         * @var \G2\Entity\VwAvaliacaoPrimeiroAgendamento $disciplina
         */
        foreach ($disciplinas as $disciplina) {
            $id_usuario = $disciplina->getId_usuario();
            $id_entidade = $disciplina->getId_entidadeatendimento();
            $id_matricula = $disciplina->getId_matricula();
            $id_disciplina = $disciplina->getId_disciplina();
            $id_avaliacao = $disciplina->getId_avaliacao();
            $id_avaliacaoconjuntoreferencia = $disciplina->getId_avaliacaoconjuntoreferencia();

            // É necessario setar as variáveis da sessão de acordo com as variáveis acima porque
            // várias funções que serão usadas durante o processo, usam o id_usuario e id_entidade da sessão. (Robô).
            $this->sessao->id_usuario = $id_usuario;
            $this->sessao->id_entidade = $id_entidade;

            // Buscar o primeiro Aplicador de Prova
            // Buscando somente aplicadores que o aluno nao esta agendado em alguma disciplina
            $aplicadores_usados = $this->findAlunoAplicacoesUsadas(array(
                'id_matricula' => $id_matricula
            ));

            $aplicadores = $this->findVwAplicacoesPorProva(array(
                'id_entidade' => $id_entidade,
                'id_avaliacao' => $id_avaliacao
            ));

            // Ignorar aplicadores que ja foram usados
            if (is_array($aplicadores_usados) && $aplicadores_usados) {
                $ar_aplicadores = array_map(function ($item) {
                    return $item->getId_avaliacaoaplicacao() ? $item->getId_avaliacaoaplicacao()->getId_avaliacaoaplicacao() : null;
                }, $aplicadores_usados);

                $aplicadores = array_filter($aplicadores, function ($item) use ($ar_aplicadores) {
                    return !in_array($item->getId_avaliacaoaplicacao(), $ar_aplicadores);
                });
            }
            $aplicador = array_shift($aplicadores);

            // Antes de prosseguir, verificar se tem aplicador
            if (!$aplicador instanceof \G2\Entity\VwAvaliacaoAplicacao) {
                $ar_return[] = 'Não foi encontrado um aplicador disponível. ' .
                    'Matrícula: ' . $id_matricula .
                    ' | Disciplina: ' . $id_disciplina .
                    ' | Avaliação: ' . $id_avaliacao;
                continue;
            }

            // Na GRADUAÇÃO não é permitido várias chamadas, então bloquear mais de um agendamento para a mesma disciplina
            /** @var \G2\Entity\VwAvaliacaoAgendamentoDisciplina $agendamento */
            $agendamento = $this->findOneBy('\G2\Entity\VwAvaliacaoAgendamentoDisciplina', array(
                'id_matricula' => $id_matricula,
                'id_disciplina' => $id_disciplina,
                'id_avaliacao' => $id_avaliacao
            ));

            if ($agendamento instanceof \G2\Entity\VwAvaliacaoAgendamentoDisciplina) {
                $ar_return[] = 'O aluno já possui um agendamento.' .
                    ' Matrícula: ' . $id_matricula .
                    ' | Disciplina: ' . $id_disciplina .
                    ' | Avaliação: ' . $id_avaliacao;
                continue;
            }

            // Preparar parametros para agendar
            $data = array(
                'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_LIBERADO,
                'id_matricula' => $id_matricula,
                'id_avaliacao' => $id_avaliacao,
                'id_usuariocadastro' => \G2\Constante\Usuario::SISTEMA_G2,
                'id_tipodeavaliacao' => 0,
                'bl_provaglobal' => 0,
                'bl_automatico' => 1,
                'st_justificativa' => $st_just_liberacao,
                'id_avaliacaoconjuntoreferencia' => array($id_avaliacaoconjuntoreferencia)
            );

            // Criar transacao, porque caso a aplicacao ja tiver sido usada pelo aluno, entao deve cancelar tambem a liberacao do agendamento
            $this->beginTransaction();

            // Primeiro é necessario LIBERAR pra depois AGENDAR (o metodo salvarAgendamento pede isso)
            $agendamento = $this->salvarLiberacaoAgendamento($data);

            // Depois salva com situacao AGENDADO
            if ($agendamento instanceof \Ead1_Mensageiro && $agendamento->getId()) {
                $agendamento = $this->salvarAgendamento(array(
                    'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                    'id_avaliacaoagendamento' => $agendamento->getId(),
                    'id_avaliacaoaplicacao' => $aplicador->getId_avaliacaoaplicacao(),
                    'id_usuario' => $id_usuario,
                    'id_usuariocadastro' => \G2\Constante\Usuario::SISTEMA_G2,
                    'st_justificativa' => $st_just_agendamento
                ));

                $ar_return[] = $agendamento->getFirstMensagem() .
                    ' Matrícula: ' . $id_matricula .
                    ' | Disciplina: ' . $id_disciplina .
                    ' | Avaliação: ' . $id_avaliacao;

                if ($agendamento->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $this->commit();
                } else {
                    $this->rollback();
                }
            } else {
                $this->rollback();
            }
        }

        // Redefinir os dados originais da sessão
        $this->sessao->id_usuario = $id_usuario_sessao;
        $this->sessao->id_entidade = $id_entidade_sessao;

        if (!$ar_return) {
            $ar_return = 'Nenhum aluno apto para o primeiro agendamento';
        }

        return $ar_return;
    }

    /**
     * @param array $params
     * @return array
     */
    public function findAlunoAplicacoesUsadas($params = array())
    {
        $params = array_merge(array(
            'id_situacao' => array(\G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO, \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO),
            //'id_matricula'          => $id_matricula,
            //'bl_ativo'              => true,
            //'nu_presenca'           => null
        ), $params);

        // Verificar se ja o mesmo aluno possui algum agendamento no mesmo horario do id_avaliacaoaplicacao.
        // Se possuir, nao pode permitir que ele use o mesmo
        $results = $this->findBy('\G2\Entity\AvaliacaoAgendamento', $params);

        return $results ? $results : array();
    }


    /**
     * Retorna trâmites do lançamento de frequencia
     * @param $id_avaliacaoagendamento
     * @return array|bool
     */
    public function retornaHistoricoLancamentoFrequencia($id_avaliacaoagendamento)
    {
        try {
            $tramites = $this->em->getRepository('G2\Entity\AvaliacaoAgendamento')->findHistoricoLancamentoPresenca($id_avaliacaoagendamento);
            if (is_array($tramites) && !empty($tramites)) {
                return $tramites;
            } else
                return array();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param int $id_matricula
     * @return bool
     */
    public function alunoEstaAptoAgendamento($id_matricula)
    {
        $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

        if ($bl_provapordisciplina) {
            $entity = '\G2\Entity\MatriculaDisciplina';
        } else {
            $entity = '\G2\Entity\Matricula';
        }

        $result = $this->findOneBy($entity, array(
            'id_matricula' => $id_matricula,
            'id_situacaoagendamento' => array(
                \G2\Constante\Situacao::TB_MATRICULA_APTO,
                \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_APTO_RECUPERACAO
            )
        ));

        // Converter para boolean usando !!
        return !!$result;
    }

    /**
     * Retorna o tipo de prova que o aluno está apto a agendar, baseando-se
     * nos resultados encontrados tb_avaliacaoagendamento.
     * @param $id_matricula
     * @return int
     */
    public function statusAgendamentoAluno($id_matricula, $id_entidade = null)
    {
        $currentDate = new \DateTime(date('Y-m-d'));
        $id_entidade = $id_entidade ? $id_entidade : $this->sessao->id_entidade;

        /* Verifica se o aluno está apto para o agendamento com o mesmo método
        * usado na tela Gerência Provas do G2. */

        //$alunoApto = $this->pesquisaAlunos(array('id_matricula' => $id_matricula));
        $alunoApto = $this->findOneBy('\G2\Entity\VwAlunosAgendamento', array(
            'id_matricula' => $id_matricula,
            'id_entidade' => $id_entidade
        ));

        // Se o aluno estiver apto, verificar a situação da prova.
        if ($alunoApto instanceof \G2\Entity\VwAlunosAgendamento) {

            // Verificar se possui prova presencial no conjunto de avaliação
            $presencial = $this->findOneBy('\G2\Entity\VwAvaliacaoAluno', array(
                'id_matricula' => $id_matricula,
                'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL
            ));

            // @history GII-8276
            // Se não possuir prova presencial, então não deve ser exibida mensagem de agendamento
            if (!($presencial instanceof \G2\Entity\VwAvaliacaoAluno)) {
                return 0;
            }

            $statusAluno = $this->findOneBy('\G2\Entity\AvaliacaoAgendamento', array(
                'id_matricula' => $id_matricula
            ), array('id_avaliacaoagendamento' => 'DESC'));

            /*
             * 0 - Nenhuma mensagem será exibida.
             * 1 - Agendamento Prova Presencial
             * 2 - Agendamento Prova Final (Recuperação)
             * 3 - Reagendamento Prova Presencial
             * 4 - Reagendamento Prova Final (Recuperação)
             */

            if ($statusAluno) {
                $nu_presenca = $statusAluno->getNu_presenca();
                $id_situacao = $statusAluno->getId_situacao()->getId_situacao();
                $id_tipodeavaliacao = $statusAluno->getId_tipodeavaliacao();

                if (is_null($nu_presenca)) {
                    if ($id_situacao != \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO
                        && $id_situacao != \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                        && $id_situacao != \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_CANCELADO
                    ) {
                        return $id_tipodeavaliacao ? 2 : 1;
                    } else if ($id_situacao == \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO
                        || $id_situacao == \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
                    ) {
                        if ($currentDate <= $statusAluno->getId_avaliacaoaplicacao()->getDt_alteracaolimite()) {
                            return $id_tipodeavaliacao ? 4 : 3;
                        } //o aluno pode ver o historico ate o dia da aplicação
                        else if ($currentDate <= $statusAluno->getId_avaliacaoaplicacao()->getDt_aplicacao()) {
                            return 5;
                        }
                    }
                }
            } else if ($this->pesquisaAlunos(array('id_matricula' => $id_matricula))) {
                return 1;
            }
        }

        return 0;
    }

    public function provaAgendada($params)
    {
        $data = array();
        $resultSet = $this->findAvaliacoesDisciplinasAgendamento($params);

        foreach ($resultSet['avaliacoes'] as $avaliacao) {

            foreach ($avaliacao['agendamentos'] as $agendamento) {

                if (!$agendamento['id_avaliacaoagendamento'] &&
                    1 == count($avaliacao['agendamentos']) &&
                    \G2\Constante\Situacao::TB_MATRICULA_APTO != $avaliacao['id_situacaoagendamento']
                ) {
                    continue;
                }

                $historico = $this->historicoAgendamento($agendamento);

                $aplicadorProva = 'Sem agendamento';
                if (trim($agendamento['st_cidade'])) {
                    $aplicadorProva = 'CIDADE: ' . $agendamento['st_cidade'];
                    $aplicadorProva .= '<br/ >APLICADOR: ' . $agendamento['st_aplicadorprova'];
                }

                $data[] = array(
                    'st_avaliacao' => $agendamento['st_avaliacao'] ?: $avaliacao['st_avaliacao'],
                    'dt_agendamento' => $agendamento['dt_agendamento'] ?: 'Sem data',
                    'dt_aplicacao' => $agendamento['dt_aplicacao'] ?: 'Sem data',
                    'st_usuariolancamento' => $agendamento['st_usuariolancamento'],
                    'st_aplicadorprova' => $aplicadorProva,
                    'st_disciplina' => self::defineDisciplinaNome($agendamento, $avaliacao['disciplinas']),
                    'statusProva' => self::defineSituacaoStatus($agendamento),
                    'statusPresenca' => self::definePresencaStatus($agendamento),
                    'hr_inicio' => $agendamento['hr_inicio'] ? $agendamento['hr_inicio'] : ' - ',
                    'hr_fim' => $agendamento['hr_fim'] ? $agendamento['hr_fim'] : ' - '
                );
            }
        }

        return $data;
    }

    /**
     * @param  string[] $agendamento
     * @return string
     */
    private function historicoAgendamento($agendamento)
    {
        $gerProva = new \G2\Negocio\GerenciaProva();
        $respAgendamento = $gerProva->findByVwHistoricoAgendamento(array(
            'id_situacao' => (integer)$agendamento['id_situacao'],
            'id_matricula' => (integer)$agendamento['id_matricula'],
            'id_avaliacaoagendamento' => (integer)$agendamento['id_avaliacaoagendamento'],
        ));

        return current($respAgendamento)
            ? $this->toArrayEntity(current($respAgendamento))
            : array('st_nomecompleto' => null, 'st_situacao' => null);
    }

    /***
     * @param string $agendamento
     * @param string $disciplinas
     * @return string
     */
    private static function defineDisciplinaNome($agendamento, $disciplinas)
    {
        if ((boolean)$agendamento['bl_provaglobal']) {
            return '<b>PROVA GLOBAL</b>';
        }

        $strDisciplina = '<ul>';

        foreach ($disciplinas as $disciplina) {
            $strDisciplina .= sprintf('<li>%s</li>', $disciplina['st_disciplina']);
        }

        return ($strDisciplina .= '</ul>');
    }

    /***
     * @param string $agendamento
     * @return string[]
     */
    private static function defineSituacaoStatus($agendamento)
    {
        $status = array(
            'text' => 'Prova realizada',
            'color' => '#fff',
        );

        if (false == ((boolean)$agendamento['nu_presenca'])) {
            $status['text'] = $agendamento['st_situacao'] ?: 'Aluno Apto';
        }

        if (null !== $agendamento['nu_presenca']) {
            $status['color'] = 'rgb(255,123,0)';
        } else {
            switch ($agendamento['id_situacao']) {
                case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_LIBERADO:
                    $status['color'] = 'rgb(0,102,255)';
                    break;

                case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_ABONADO:
                    $status['color'] = 'rgb(0,102,255)';
                    break;

                case \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO:
                    $status['color'] = 'rgb(51,153,0)';
                    break;

                case  \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO:
                    $status['color'] = 'rgb(51,153,0)';
                    break;

                default:
                    $status['color'] = 'rgb(0,51,255)';
                    break;
            }
        }

        return $status;
    }

    /***
     * @param string $agendamento
     * @return string[]
     */
    private static function definePresencaStatus($agendamento)
    {
        $status = array(
            'text' => null,
            'label' => null,
        );

        if (1 === $agendamento['nu_presenca']) {
            $status['text'] = 'Presente';
            $status['label'] = 'success';
        } elseif (0 === $agendamento['nu_presenca']) {
            $status['text'] = 'Ausente';
            $status['label'] = 'error';
        } else {
            $status['text'] = 'Não lançada';
            $status['label'] = 'no-record';
        }

        return $status;
    }

    /**
     * @description Verificar se o aluno está apto para o agendamento, sendo ele da PÓS ou da GRA, Avaliação FINAL e/ou RECUPERAÇÃO
     * @param int|\G2\Entity\Matricula $matricula
     * @return int
     */
    public function verificarAlunoAptoAgendamento($matricula)
    {
        if (!($matricula instanceof \G2\Entity\Matricula)) {
            $matricula = $this->find('\G2\Entity\Matricula', $matricula);
        }

        // Verificar se a configuracao da entidade (Organizacao > Cadastros > Pessoa Juridíca :: Configurações) está HABILITADO para mostrar mensagem de agendamento no portal.
        $msg_portal = $this->findOneBy('\G2\Entity\ConfiguracaoEntidade', array(
            'id_entidade' => $matricula->getId_entidadematricula(),
            'bl_msgagendamentoportal' => true
        ));

        if (!($msg_portal instanceof \G2\Entity\ConfiguracaoEntidade)) {
            return 0;
        }

        $status = $this->statusAgendamentoAluno($matricula->getId_matricula(), $matricula->getId_entidadematricula());

        return $status;
    }

    /**
     * @param int $id_matricula
     * @return \G2\Entity\VwAvaliacaoAgendamento|null
     */
    public function retornarAgendamentoComprovanteDisponivel($id_matricula)
    {
        /**
         * @var \G2\Repository\AvaliacaoAgendamento $repo
         */
        $repo = $this->em->getRepository('\G2\Entity\VwAvaliacaoAgendamento');
        $result = $repo->retornarUltimoAgendamento($id_matricula);

        if (!($result instanceof \G2\Entity\VwAvaliacaoAgendamento)) {
            return null;
        }

        // Se a presença lançada for "Ausente" então não deverá retornar o agendamento, porque o comprovanete não deverá ficar disponível
        return $result->getNu_presenca() === 0 ? null : $result;
    }

    /**
     * Envia mensagem de aviso para o aluno
     * Parametrizado na entidade o número de dias antes do envio
     * @return array
     */
    public function enviaMensagemAvisoAgendamento()
    {
        try {
            $alunos = $this->retornaAlunosEnvioMensagemAgendamento();

            $success = array();
            $error = array();

            if (is_array($alunos) && $alunos) {

                $mensagemBO = new \MensagemBO();
                foreach ($alunos as $key => $entity) {
                    $en_to_array = $this->toArrayEntity($entity);
                    $mensageiro_aux = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::AVISO_SOBRE_AGENDAMENTO, $this->entityToTO($entity), \TipoEnvioTO::EMAIL);
                    if ($mensageiro_aux->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                        $this->marcaDataEnvioMensagemAvisoPrevio($entity->getId_avaliacaoagendamento());
                        $success[] = $en_to_array;
                    } else {
                        $en_to_array['message'] = $mensageiro_aux->getText();
                        $error[] = $en_to_array;
                    }
                }
            }
            return array(
                'results' => $alunos,
                'error' => $error,
                'success' => $success
            );

        } catch (\Exception $e) {
            return array('results' => false);
        }
    }

    /**
     * Marca data de envio de mensagem de aviso do agendamento
     * @param $id_avaliacaoagendamento
     * @return \Ead1_Mensageiro
     */
    public function marcaDataEnvioMensagemAvisoPrevio($id_avaliacaoagendamento)
    {
        try {
            /** @var \G2\Entity\AvaliacaoAgendamento $avaliacao_agendamento */
            $avaliacao_agendamento = $this->find('\G2\Entity\AvaliacaoAgendamento', (int)$id_avaliacaoagendamento);

            if ($avaliacao_agendamento instanceof \G2\Entity\AvaliacaoAgendamento && $avaliacao_agendamento->getId_avaliacaoagendamento()) {
                $avaliacao_agendamento->setDt_envioaviso(new \DateTime());
                $this->save($avaliacao_agendamento);
            }
            return new \Ead1_Mensageiro('Data de envio de aviso de agendamento prévio salva com sucesso.');
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('ERRO ao salvar a data de envio de aviso de agendamento prévio.', \Ead1_IMensageiro::ERRO);
        }
    }

    public function getDisciplinasAgendamento($id_avaliacaoagendamento)
    {
        /** @var \G2\Entity\VwAvaliacaoAgendamento $agendamento */
        $agendamento = $this->findOneBy('\G2\Entity\VwAvaliacaoAgendamento', array('id_avaliacaoagendamento' => $id_avaliacaoagendamento));

        if (!($agendamento instanceof \G2\Entity\VwAvaliacaoAgendamento)) {
            throw new \Zend_Exception('Agendamento não encontrado.');
        }

        $bl_provapordisciplina = (new \G2\Negocio\EsquemaConfiguracao())->isProvaPorDisciplina();

        $response = array();
        $id_matricula = $agendamento->getId_matricula();
        $id_avaliacao = $agendamento->getId_avaliacao();

        if (!$agendamento->getBl_provaglobal() && !$bl_provapordisciplina) {
            $disciplinas = $this->findByVwDisciplinasAgendamento(array(
                'id_matricula' => $id_matricula,
                'id_avaliacao' => $id_avaliacao,
                'id_avaliacaoagendamento' => $agendamento->getId_avaliacaoagendamento(),
                'bl_agendado' => 1
            ), array(
                // ORDER BY
                'st_disciplina' => 'ASC'
            ));

            $method = 'getSt_disciplina';
        } else {
            $disciplinas = $this->findBy('\G2\Entity\VwAvaliacaoAluno', array(
                'id_matricula' => $id_matricula,
                'id_avaliacao' => $id_avaliacao,
                'id_disciplina' => $agendamento->getId_disciplina()
            ), array(
                // ORDER BY
                'st_tituloexibicaodisciplina' => 'ASC'
            ));

            $method = 'getSt_tituloexibicaodisciplina';
        }

        if (is_array($disciplinas) && $disciplinas) {
            foreach ($disciplinas as $disciplina) {
                $response[$disciplina->getId_disciplina()] = array(
                    'id_disciplina' => $disciplina->getId_disciplina(),
                    'st_disciplina' => $disciplina->$method()
                );
            }
        }

        return array_values($response);
    }

    /**
     * Retorna alunos com agendamento previsto para os proximos (n) dias
     * (n) Parametrizado no esquema de configuração da entidade
     * Aptos ao envio de e-mail de aviso de agendamento
     * @return array|\G2\Entity\VwAvaliacaoAgendamento[]
     */
    public function retornaAlunosEnvioMensagemAgendamento()
    {
        try {
            $retorno = array();

            /**Busca o número de dias parametrizado para envio das mensagens de aviso de agendamento por entidade
             * Retorna apenas aquelas que tem valor  **/
            /**  @var \G2\Entity\VwEntidadeEsquemaConfiguracao[] $entidades_esquema */
            $entidades_esquema = $this->findCustom('\G2\Entity\VwEntidadeEsquemaConfiguracao',
                array("id_itemconfiguracao" => \G2\Constante\ItemConfiguracao::DIAS_ENVIAR_AVISO_AGENDAMENTO,
                    "st_valor" => " != '' ")
            );

            $nu_dias = array();
            if (empty($entidades_esquema)) {
                throw new \Exception('Nenhuma entidade parametrizada para envio de mensagens de aviso de agendamento.');
            }

            /** Organiza o array pelo número de dias parametrizado agrupando as entidades */
            foreach ($entidades_esquema as $result) {
                $nu_dias[$result->getSt_valor()][] = $result->getId_entidade();
            }

            //Busca os agendamentos , validando o número de dias de intervalo e as entidades parametrizadas
            foreach ($nu_dias as $key => $nu_dia) {
                $today = new \Zend_Date();

                $where = array('dt_aplicacao' => "BETWEEN  '" . \G2\Utils\Helper::converterData($today, 'Y-m-d') . "' AND '" . \G2\Utils\Helper::converterData($today->addDay((int)$key), 'Y-m-d') . "'"
                , 'bl_ativo' => 1
                , 'id_situacao' => \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO
                , 'dt_envioaviso' => ' IS NULL'
                , 'id_entidade' => ' IN ( ' . implode(' , ', $nu_dia) . ' )'
                , 'id_tipoavaliacao' => \G2\Constante\TipoAvaliacao::AVALIACAO_PRESENCIAL
                );

                /**  @var \G2\Entity\VwAvaliacaoAgendamento[] $en_vwavaliacaoenvio */
                $en_vwavaliacaoenvio = $this->findCustom('\G2\Entity\VwAvaliacaoAgendamento', $where);

                /** @var $retorno []
                 *  Agrupa os resultados - para quantidades de dias diferentes - para envio do e-mail */
                $retorno = array_merge($retorno, $en_vwavaliacaoenvio);
            }

            return $retorno;
        } catch (\Exception $e) {
            return array();
        }
    }
}

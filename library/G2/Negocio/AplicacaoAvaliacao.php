<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Aplicacao de Avaliações (locais de prova para agendamento)
 * @author Débora Castro <deboracastro.pm@gmail.com>
 */
class AplicacaoAvaliacao extends Negocio
{

    private $repositorio = 'G2\Entity\AvaliacaoAplicacao';

    /**
     * @return array|string
     */
    public function findAvaliacao($params, $order_by = null)
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            $repositoryName = 'G2\Entity\VwPesquisaAvaliacao';
            return $this->em->getRepository($repositoryName)->findBy($params, $order_by);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function findEnderecoAplicador(array $params)
    {
        try {
            $params['id_entidaderelacionada'] = $this->sessao->id_entidade;
            
            $repositoryName = 'G2\Entity\VwAplicadorEndereco';
            return $this->em->getRepository($repositoryName)->findBy($params, array('st_aplicadorprova' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $params
     * @return array|string
     */
    public function findByAvaliacoes(array $params)
    {
        try {
            $repositoryName = 'G2\Entity\VwAvaliacaoAplicacaoRelacao';
            return $this->em->getRepository($repositoryName)->findBy($params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $data
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvaAplicacaoAvaliacao(array $data)
    {
        try {

            if (!isset($data['id_avaliacao']) || empty($data['id_avaliacao'])) {
                return new \Ead1_Mensageiro('Selecione pelo menos uma avaliação!', \Ead1_IMensageiro::AVISO);
            }
            if ((!empty($data['dt_aplicacao'])) && $data['bl_unica'] == 1) {
                return new \Ead1_Mensageiro('O campo Data e Sem Data estão preenchidos, selecione apenas um deles!', \Ead1_IMensageiro::AVISO);
            }

            if ($data['id_avaliacaoaplicacao'] != '') {
//                //se existir o id, busca o registro
                $entity = $this->find($this->repositorio, $data['id_avaliacaoaplicacao']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\AvaliacaoAplicacao();
            }
            //seta os atributos
            $entity->setBl_ativo(1)
                ->setBl_unica($data['bl_unica'])
                ->setDt_alteracaolimite($this->converteDataBanco($data['dt_alteracaolimite']))
                ->setDt_antecedenciaminima($this->converteDataBanco($data['dt_antecedenciaminima']))
                ->setDt_cadastro(new \DateTime())
                ->setId_aplicadorprova($data['id_aplicadorprova'])
                ->setId_endereco($data['id_endereco'])
                ->setId_horarioaula($data['id_horarioaula'])
                ->setId_usuariocadastro($this->sessao->id_usuario)
                ->setNu_maxaplicacao($data['nu_maxaplicacao'])
                ->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->getId_entidade()))
            ;

            if (!empty($data['dt_aplicacao'])) {
                $entity->setDt_aplicacao($this->converteDataBanco($data['dt_aplicacao']));
            }


            $salvar = $this->save($entity);
            if ($salvar->getId_avaliacaoaplicacao()) {

                $this->salvarAvaliacaoAplicacaoRelacao($data['id_avaliacao'], $salvar->getId_avaliacaoaplicacao());
                $mensageiro = new \Ead1_Mensageiro('Avaliações salvas com sucesso', \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($salvar->getId_avaliacaoaplicacao());
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @param $data
     * @param $id_avaliacaoaplicacao
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarAvaliacaoAplicacaoRelacao($data, $id_avaliacaoaplicacao)
    {
        try {
            $deleta_anteriores = $this->deletarAvaliacaoAplicacaoRelacao($id_avaliacaoaplicacao);

            if ($deleta_anteriores->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                $mensageiro = new \Ead1_Mensageiro('Erro ao Deletar avaliações relacionadas', \Ead1_IMensageiro::SUCESSO);
                return $mensageiro;
            }
            if (is_array($data)) {
                foreach ($data as $avaliacao) {
                    $entity = new \G2\Entity\AvaliacaoAplicacaoRelacao();
                    $entity->setId_avaliacao($avaliacao)
                        ->setId_avaliacaoaplicacao($id_avaliacaoaplicacao);
                    $this->save($entity);
                }
            } else {
                $entity = new \G2\Entity\AvaliacaoAplicacaoRelacao();
                $entity->setId_avaliacao($data)
                    ->setId_avaliacaoaplicacao($id_avaliacaoaplicacao);
                $this->save($entity);
            }
            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @param $id
     * @return \Ead1_Mensageiro
     */
    public function deletarAvaliacaoAplicacaoRelacao($id)
    {

        try {
            $result = $this->findBy('\G2\Entity\AvaliacaoAplicacaoRelacao', array('id_avaliacaoaplicacao' => $id));
            if (is_array($result)) {
                foreach ($result as $ent) {
                    $this->delete($ent);
                }
            }
            $mensageiro = new \Ead1_Mensageiro('Avaliações desvinculadas com sucesso', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function deletarAvaliacaoAplicacao($id)
    {
//        try {
//            $result = $this->find('\G2\Entity\AvaliacaoAplicacao', $id);
//            if ($result) {
//                $this->delete($result);
//            }
//            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
//        } catch (Exception $exc) {
//            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
//        }
//        return $mensageiro;
    }


    /**
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function retornarAplicadores(array $params)
    {
        try {
            $array = array();
            $params['id_entidaderelacionada'] = $this->sessao->id_entidade;
            $result = $this->findBy('G2\Entity\VwAplicadorEndereco', $params, array('st_aplicadorprova' => 'ASC'));

            if (is_array($result)) {
                foreach ($result as $ind => $row) {
                    $array[$ind] = $this->toArrayEntity($row);
                }
            }
            return new \Ead1_Mensageiro($array, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }


    /**
     * @param array $params
     * @return bool|\Ead1_Mensageiro
     * @throws Zend_Exception
     */
    public function retornaDatasProvaDeAplicador(array $params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\AvaliacaoAplicacao');

            $query = $repo->createQueryBuilder("vw")
                ->select('vw.id_avaliacaoaplicacao', 'vw.dt_aplicacao')
                ->where('1=1');

            $query->andWhere('vw.id_aplicadorprova = :id_aplicadorprova')
                ->setParameter('id_aplicadorprova', $params['id_aplicadorprova']);

            $query->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', 1);

            if (array_key_exists('id_entidade', $params)){
                $query->andWhere('vw.id_entidade = :id_entidade')
                    ->setParameter('id_entidade', $params['id_entidade']);
            }

            $query->andWhere('vw.dt_aplicacao is not null');

            $query->andWhere('vw.dt_aplicacao >= :dt_aplicacao')
                ->setParameter('dt_aplicacao', date('Y-m-d'));
            $result = $query->getQuery()->getResult();
            if (count($result) > 0) {
                foreach ($result as $index => $objeto) {
                    $result[$index]['dt_aplicacao'] = date_format($objeto['dt_aplicacao'], 'd/m/Y');
                }
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna dados da VwAvaliacaoAplicacao filtrando pelos atributos informados
     *
     * @param array $where
     * @param array $order
     * @return Object
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwAvaliacaoAplicacao(array $where, array $order = array()){
        return $this->findBy('\G2\Entity\VwAvaliacaoAplicacao', $where, $order);
    }


    /**
     * Retorna locais de prova de acordo com UF e entidade (apenas locais de prova da propria entidade)
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaLocalProva(array $params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAvaliacaoAplicacao');

            $query = $repo->createQueryBuilder("vw")
                ->select('vw.id_aplicadorprova', 'vw.st_aplicadorprova')
                ->distinct(true);

            $query->where("vw.sg_uf like '".$params['sg_uf']."'");

            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);

            $query->orderBy('vw.st_aplicadorprova');
            return $query->getQuery()->getResult();

        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Exception($e->getMessage());
        }
    }

}

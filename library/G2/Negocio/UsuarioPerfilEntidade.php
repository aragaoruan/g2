<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Entidades relacionadas ao UsuarioPerfilEntidade
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2017-08-29
 */
class UsuarioPerfilEntidade extends Negocio
{

    private $repositoryName = 'G2\Entity\UsuarioPerfilEntidade';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param bool $bl_tutorial
     * @return bool|mixed
     * @throws \Exception
     */
    public function saveTutorialPortal($bl_tutorial = false)
    {
        try {
            if ($bl_tutorial) {
                $objUPE = $this->findOneBy($this->repositoryName, array(
                    'id_usuario' => \Ead1_Sessao::getSessaoUsuario()->id_usuario,
                    'id_entidade' => \Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_perfil' => \Ead1_Sessao::getSessaoPerfil()->id_perfil
                ));

                if ($objUPE) {
                    $objUPE->setBl_tutorial($bl_tutorial);
                    return $this->save($objUPE);
                } else {
                    return false;
                }

            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return false;
    }

    /**
     * Verificar se o usuário possui acesso à entidade de origem como ALUNO,
     * se não possuir, o método vai clonar os dados da entidade de origem
     * @param int $id_usuario
     * @param int $id_entidadedestino
     * @param int $id_entidadeorigem
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function clonarAcessoAluno($id_usuario, $id_entidadedestino, $id_entidadeorigem = null)
    {
        $id_entidadeorigem = $id_entidadeorigem ?: $this->sessao->id_entidade;

        $en_entidadeorigem = $this->getReference('\G2\Entity\Entidade', $id_entidadeorigem);
        $en_entidadedestino = $this->getReference('\G2\Entity\Entidade', $id_entidadedestino);

        // Buscar perfil de aluno da entidade de ORIGEM
        /** @var \G2\Entity\Perfil $perfilaluno */
        $perfilAlunoOrigem = $this->findOneBy('\G2\Entity\Perfil', array(
            'bl_ativo' => true,
            'id_situacao' => \G2\Constante\Situacao::TB_PERFIL_ATIVO,
            'id_entidade' => $en_entidadeorigem,
            'id_sistema' => \G2\Constante\Sistema::PORTAL_ALUNO,
            'id_perfilpedagogico' => \G2\Constante\PerfilPedagogico::ALUNO
        ));

        // Buscar perfil de aluno da entidade de DESTINO
        /** @var \G2\Entity\Perfil $perfilaluno */
        $perfilAlunoDestino = $this->findOneBy('\G2\Entity\Perfil', array(
            'bl_ativo' => true,
            'id_situacao' => \G2\Constante\Situacao::TB_PERFIL_ATIVO,
            'id_entidade' => $en_entidadedestino,
            'id_sistema' => \G2\Constante\Sistema::PORTAL_ALUNO,
            'id_perfilpedagogico' => \G2\Constante\PerfilPedagogico::ALUNO
        ));

        if (!($perfilAlunoDestino instanceof \G2\Entity\Perfil)) {
            throw new \Exception('Perfil pedagógico "Aluno" não foi encontrado na entidade de destino');
        }

        // Verificar se já existe perfil de aluno na entidade de destino
        /** @var \G2\Entity\UsuarioPerfilEntidade $usuarioPerfilEntidade */
        $usuarioPerfilEntidade = $this->findOneBy('\G2\Entity\UsuarioPerfilEntidade', array(
            'id_usuario' => $id_usuario,
            'id_entidade' => $id_entidadedestino,
            'bl_ativo' => true,
            'id_perfil' => $perfilAlunoDestino->getId_perfil()
        ));

        // Se não existir, clonar da entidade de origem
        if (!($usuarioPerfilEntidade instanceof \G2\Entity\UsuarioPerfilEntidade)) {
            $usuarioPerfilEntidade = $this->findOneBy('\G2\Entity\UsuarioPerfilEntidade', array(
                'id_usuario' => $id_usuario,
                'id_entidade' => $id_entidadeorigem,
                'bl_ativo' => true,
                'id_perfil' => $perfilAlunoOrigem->getId_perfil()
            ));

            $usuarioPerfilEntidade = clone $usuarioPerfilEntidade;
            unset($usuarioPerfilEntidade->_entityPersister, $usuarioPerfilEntidade->_identifier);

            $usuarioPerfilEntidade->setId_entidade($id_entidadedestino);
            $usuarioPerfilEntidade->setId_perfil($perfilAlunoDestino->getId_perfil());
            $usuarioPerfilEntidade->setDt_cadastro(date('Y-m-d'));

            $this->save($usuarioPerfilEntidade);
        }
    }

}

<?php

/**
 * Classe Negocio para Folha de Pagamento
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2015-07-16
 */

namespace G2\Negocio;

class FolhaDeNumerosTurmaXMes extends Negocio
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $params
     * @return mixed
     * Busca no repositorio VwTurmaProjetoVenda as vendas vinculadas a entidade, ao projeto pedagogico ou a turma, ou
     * seja, aos filtros passados na consulta.
     */
    public function getRelatorio($params){
        return $this->em->getRepository('\G2\Entity\VwTurmaProjetoVenda')->getVwTurmaProjetoVenda($params);
    }

    /**
     * @param $params
     * @return mixed
     * Retorna todos os projetos pedagogicos da entidade selecionada.
     */
    public function findVwProjetoEntidade($params){
        return $this->em->getRepository('\G2\Entity\VwProjetoEntidade')->retornaProjetosEntidades($params);
    }

    /**
     * @param $params
     * @return array
     * Retorna todas as turmas vinculadas aquele projeto pedagogico selecionado.
     */
    public function findVwTurmaProjetoPedagogico($params){
        return $this->findBy('\G2\Entity\VwTurmaProjetoPedagogico', $params);
    }

    /**
     * @param $params
     * @return array
     * Retorna todas as holdings vinculadas a entidade atual.
     */
    public function findHolding($params){
        $objetoHoldingFiliada = $this->findBy('\G2\Entity\HoldingFiliada',$params);
        $arrHoldings = array();
        foreach($objetoHoldingFiliada as $value){
            array_push($arrHoldings, $this->find('\G2\Entity\Holding', $value->getId_holding()));
        }
        return $arrHoldings;
    }

    /**
     * @param $params
     * @return array
     * Retorna todas as entidades selecionadas na holding.
     */
    public function findEntidadeHolding($params){
        $objetoEntidadeHolding = $this->findBy('\G2\Entity\HoldingFiliada', $params);
        $arrHoldings = array();
        foreach($objetoEntidadeHolding as $value){
            array_push($arrHoldings, $this->find('\G2\Entity\Entidade', $value->getId_entidade()));
        }
        return $arrHoldings;
    }

}
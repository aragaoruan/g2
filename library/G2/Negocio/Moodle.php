<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Moodle
 */
class Moodle extends Negocio
{


    public function __construct()
    {
        parent::__construct();

    }


    /**
     * Verifica se o aluno está integrado em uma sala de TCC
     * Utilizando os parametros enviados do moodle para validar a integração
     * @param $params
     * @return bool|\G2\Entity\VwAlunoGradeIntegracao
     */
    public function verificaIntegracaoAlunoSalaMoodle($params)
    {
        try {
            $return = false;
            /** @var \G2\Entity\VwAlunoGradeIntegracao[] $vw_array */
            $vw_array = $this->findCustom('\G2\Entity\VwAlunoGradeIntegracao',
                array('st_codsistemacurso' => " LIKE '" . trim($params['course']) . "'",
                    'st_codusuario' => " LIKE '" . trim($params['coduser']) . "'",
                    'id_matricula' => \Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula,
                    'id_entidade' => \Ead1_Sessao::getSessaoEntidade()->id_entidade,
                    'id_tipodisciplina' => \G2\Constante\TipoDisciplina::TCC,
                    //'bl_encerrado' => 0,
                    'id_sistema' => \G2\Constante\Sistema::MOODLE)
               );
            if ($vw_array && is_array($vw_array) && $vw_array[0] instanceof \G2\Entity\VwAlunoGradeIntegracao) {
                $return =  $vw_array[0];
            }
            return $return;
        } catch (\Exception $e) {
            return false;
        }

    }

}

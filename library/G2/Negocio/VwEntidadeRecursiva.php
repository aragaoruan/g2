<?php

namespace G2\Negocio;

use G2\Entity\VwEntidadeRecursiva as VwEntidadeRecursivaEntity;

/**
 * Classe de negócio para Vw Entidade Recursiva
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VwEntidadeRecursiva extends Negocio
{

    protected $repositoryName;
    private $mensageiro;

    public function __construct ()
    {
        parent::__construct();
        $this->repositoryName = 'G2\Entity\VwEntidadeRecursiva';
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Find By Entidade
     * @param string $cadeiaentidades
     * @return object G2\Entity\VwEntidadeRecursiva
     */
    public function findByCadeiaEntidades ($params)
    {
        $result = $this->em->createQuery("SELECT p 
            FROM " . $this->repositoryName . " p 
                WHERE p.cadeiaentidades like '%|" . (string) $this->sessao->id_entidade . "|%' 
                    ORDER BY p.nu_nivelhierarquia")
                ->getResult();
        if (empty($result)) {
            return $this->mensageiro->setMensageiro('Nenhum Registro Encontrado!', \Ead1_IMensageiro::AVISO);
        }

        $arrEntidades = $result;
//        $entidadesPai = array();
        $arrAux = array();
        $primeiroNivel = true;

        //Foreach para ajeitar as entidades em um formato que a tree do flex entenda.
        foreach ($arrEntidades as $entidadeTO) {
            if ($primeiroNivel) {
                $entidadesPai[$entidadeTO->getId_entidade()]['to'] = $entidadeTO;
                $entidadesPai[$entidadeTO->getId_entidade()]['data'] = $entidadeTO->getSt_nomeentidade();
                $entidadesPai[$entidadeTO->getId_entidade()]['children'] = null;
                $entidadesPai[$entidadeTO->getId_entidade()]['tipo'] = 'Entidade';
                $entidadesPai[$entidadeTO->getId_entidade()]['metadata'] = $entidadeTO->getId_entidade();
                $primeiroNivel = false;
                continue;
            }
            $arrAux[$entidadeTO->getId_entidade()] = $entidadeTO;
        }
//        var_dump($arrAux);die;

        if (!empty($arrAux)) {
            foreach ($entidadesPai as $index => $pai) {
                $entidadesPai[$index]['children'] = $this->entidadeRecursiva($pai['to'], $arrAux);
            }
        }
        return $entidadesPai;
    }

    private function entidadeRecursiva ($eTO, $arrAux)
    {
        $arrRecurssivo = array();
        if ($arrAux) {
            foreach ($arrAux as $index => $entidadeTO) {
                if ($entidadeTO->getId_entidadepai() == $eTO->getId_entidade()) {
                    $arrRecurssivo[$index]['to'] = $entidadeTO;
                    $arrRecurssivo[$index]['data'] = $entidadeTO->getSt_nomeentidade();
                    $arrRecurssivo[$index]['tipo'] = 'Entidade';
                    $arrRecurssivo[$index]['metadata'] = $entidadeTO->getId_entidade();
                    $arrRecurssivo[$index]['children'] = $this->entidadeRecursiva($entidadeTO, $arrAux);
                }
            }
        }
        return (empty($arrRecurssivo) ? null : $arrRecurssivo);
    }

}
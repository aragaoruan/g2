<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Entrega de documentos
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @size 15/01/2015
 */
class EntregaDeclaracao extends Negocio
{

    private $repositoryName = 'G2\Entity\EntregaDeclaracao';
    private $repositoryNameVwEntregaDeclaracao = 'G2\Entity\VwEntregaDeclaracao';

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Find VwEntregaDeclaracao
     * @param array $where
     * @param array $order
     * @return mixed
     */
    public function findIdMatricula($idMatricula)
    {
        $result = $this->findBy($this->repositoryNameVwEntregaDeclaracao, array('id_matricula' => $idMatricula));

        $arrReturn = array();
        foreach ($result as $key => $row) {
            $arrReturn[$key] = array(
                'id_entregadeclaracao' => $row->getId_entregadeclaracao(),
                'id_venda' => $row->getId_venda(),
                'id_matricula' => $row->getId_matricula(),
                'st_situacao' => $row->getSt_situacao(),
                'dt_envio' => date('d/m/Y', strtotime($row->getDt_envio())),
                'id_situacao' => $row->getId_situacao(),
                'id_textosistema' => $row->getId_textosistema(),
                'st_textosistema' => $row->getSt_textosistema(),
                'dt_entrega' => $row->getDt_entrega() ? date('d/m/Y', strtotime($row->getDt_entrega())): '',
                'dt_cadastro' => date('d/m/Y', strtotime($row->getDt_cadastro())),
                'dt_cadastro' => date('d/m/Y', strtotime($row->getDt_cadastro())),
                'dt_solicitacao' => date('d/m/Y', strtotime($row->getDt_solicitacao()))
            );
        }
        return $arrReturn;
    }

    /**
     * Salva entrega de documentos
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function salvarEntregaDeclaracao($data)
    {
        try {
            $entity = new \EntregaDeclaracaoTO();
            $entity->montaToDinamico($data);
            $entity->setId_usuariocadastro($this->sessao->id_usuario);

            $decalaracaoBO = new \DeclaracaoBO();
            $retorno = $decalaracaoBO->salvarEntregaDeclaracao($entity);

            return $retorno;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar entrega de declaracao: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }
}
<?php
namespace G2\Negocio;

/**
 * Negocio PreMatriculaDisciplina (tb_prematriculadisciplina)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class PreMatriculaDisciplina extends Negocio
{

    private $repositoryName = '\G2\Entity\PreMatriculaDisciplina';

    /**
     * @description Verifica o array $disciplinas que recebeu e entao remove todos q nao estao nele, depois insere os novos que sobraram (faltaram inserir) do array
     * @param $id_venda
     * @param $disciplinas
     * @param int $id_usuariocadastro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function sincronizarRegistros($id_venda, $disciplinas, $id_usuariocadastro)
    {

        $negocio = new \G2\Negocio\Negocio();

        // Primeiramente remover as disciplinas antigas (em caso de edicao da grade)
        $disc_ja_adicionadas = $this->findBy('\G2\Entity\PreMatriculaDisciplina', array(
            'id_venda' => $id_venda
        ));

        $objVenda = $this->find('\G2\Entity\Venda', $id_venda);

        if ($id_usuariocadastro)
            $objUsuario = $this->find('\G2\Entity\Usuario', $id_usuariocadastro);

        if ($disc_ja_adicionadas) {
            foreach ($disc_ja_adicionadas as $disc_added) {
                $delete = true;

                foreach ($disciplinas as $key => $disciplina) {
                    // Caso o registro possua o mesmo id_disciplina e id_saladeaula, entao ele ja ESTA ADICIONADO, nao precisando nem REMOVER nem ADICIONAR
                    if ($disc_added->getId_disciplina()->getId_disciplina() == $disciplina['id_disciplina'] && $disc_added->getId_saladeaula()->getId_saladeaula() == $disciplina['id_saladeaula']) {
                        $delete = false;
                        unset($disciplinas[$key]);
                        break;
                    }
                }

                if ($delete) {
                    $tramite = new \G2\Entity\Tramite();

                    $tramite->setDt_cadastro(new \DateTime())
                        ->setId_entidade($this->getReference('\G2\Entity\Entidade', $objVenda->getId_entidade()))
                        ->setBl_visivel(true)
                        ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::ALTERANDO_DISCIPLINA_GRADE));

                    if ($id_usuariocadastro) {
                        $tramite->setId_usuario($objUsuario);
                        $tramite->setSt_tramite('Sala de aula ' . $disc_added->getId_saladeaula()->getSt_saladeaula() . ' removida pelo usuário '
                            . $objUsuario->getSt_nomecompleto() . ' na seleção de disciplinas.');
                    } else {
                        $tramite->setSt_tramite('Sala de aula ' . $disc_added->getId_saladeaula()->getSt_saladeaula() . ' removida pela renovação automatica.');
                    };

                    $objTramite = $negocio->save($tramite);

                    $objTramiteVenda = new \G2\Entity\TramiteVenda();
                    $objTramiteVenda->setId_tramite($objTramite->getId_tramite());
                    $objTramiteVenda->setId_venda($id_venda);

                    $negocio->save($objTramiteVenda);

                    $this->delete($disc_added);
                }
            }
        }

        // Salva as disciplinas que faltaram (NOVAS)
        foreach ($disciplinas as $disciplina) {
            $this->save(array(
                'id_venda' => $id_venda,
                'id_disciplina' => $disciplina['id_disciplina'],
                'id_saladeaula' => $disciplina['id_saladeaula'],
                'dt_cadastro' => new \DateTime(),
                'id_usuariocadastro' => $id_usuariocadastro
            ));

            $tramite = new \G2\Entity\Tramite();

            $tramite->setDt_cadastro(new \DateTime())
                ->setId_entidade($this->getReference('\G2\Entity\Entidade', $objVenda->getId_entidade()))
                ->setBl_visivel(true)
                ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::ALTERANDO_DISCIPLINA_GRADE));

            $objSalaDeAula = $this->find('\G2\Entity\SalaDeAula', $disciplina['id_saladeaula']);

            if ($id_usuariocadastro) {
                $tramite->setId_usuario($objUsuario);
                $tramite->setSt_tramite('Sala de aula ' . $objSalaDeAula->getSt_saladeaula()
                    . ' adicionada pelo usuário ' . $objUsuario->getSt_nomecompleto() . ' na seleção de disciplinas.');
            } else {
                $tramite->setSt_tramite('Sala de aula ' . $objSalaDeAula->getSt_saladeaula()
                    . ' adicionada pela renovação automatica.');
            };

            $objTramite = $negocio->save($tramite);

            $objTramiteVenda = new \G2\Entity\TramiteVenda();
            $objTramiteVenda->setId_tramite($objTramite->getId_tramite());
            $objTramiteVenda->setId_venda($id_venda);
            $negocio->save($objTramiteVenda);

        }


    }

    /**
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function save($data)
    {

        $id_attribute = 'id_prematriculadisciplina';

        /**
         * @var \G2\Entity\PreMatriculaDisciplina $entity
         */
        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }


        /**********************
         * Definindo atributos
         **********************/
        $this->entitySerialize->arrayToEntity($data, $entity);


        /**********************
         * Acao de salvar
         **********************/
        $mensageiro = new \Ead1_Mensageiro();

        try {
            if ($entity->getId_prematriculadisciplina()) {
                $entity = $this->merge($entity);
            } else {
                $entity = $this->persist($entity);
            }

            $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $entity->getId_prematriculadisciplina());
        } catch (\Exception $e) {
            $mensageiro->setMensageiro(array(
                __FILE__ . ':' . __LINE__,
                $e->getMessage(),
            ), \Ead1_IMensageiro::ERRO, $entity->getId_prematriculadisciplina());
            $mensageiro->setText('Não foi possível salvar o registro.');
        }

        return $mensageiro;
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de neg�cio para Upload (tb_upload)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 * @date 2015-07-31
 */
class Upload extends Negocio
{

    private $repositoryName;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = str_replace('\Negocio', '\Entity', __CLASS__);
    }

    /**
     * Salva os dados na tabela Upload
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function save($data)
    {

        try {

            $id_attribute = 'id_upload';

            if (isset($data[$id_attribute]) && $data[$id_attribute]) {
                // Se existir o ID, buscar registro
                $entity = $this->find($this->repositoryName, $data[$id_attribute]);
            } else {
                // Se não existir o ID, instanciar novo registro
                $entity = new $this->repositoryName;
            }


            /**********************
             * Definindo atributos
             **********************/
            $this->arrayToEntity($entity, $data);


            /**********************
             * Acao de salvar
             **********************/
            if (isset($data[$id_attribute]) && $data[$id_attribute]) {
                $entity = $this->merge($entity);
            } else {
                $entity = $this->persist($entity);
            }

            $getId = 'get' . ucfirst($id_attribute);
            $data[$id_attribute] = $entity->$getId();

            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Valida os tipos de arquivo permitido e retorna a extesão do arquivo se estiver dentro do permitido
     * @param array $_file
     * @param array $permitidos
     * @return string
     * @throws \Exception
     */
    private function validaTipos(array $_file, array $permitidos = [])
    {
        $ararq = explode('.', $_file['name']);
        $ext = strtolower($ararq[(count($ararq) - 1)]);

        if ($permitidos) {
            $permitidos = array_map('strtolower', $permitidos);
            if (!in_array($ext, $permitidos)) {
                throw new \Exception("Formato inválido do arquivo. Os formatos  permitidos são: "
                    . implode(', ', $permitidos) . ".");
            }
        }
        return $ext;
    }

    /**
     * @param $dir
     * @param $destination
     * @return bool
     * @throws \Exception
     */
    private function criarDiretorio($dir, $destination)
    {
        if (!realpath($dir)) {
            if (@mkdir($dir, true)) {
                if (!@chmod($dir, 0777))
                    throw new \Exception("Não foi possível dar a permissão correta ao diretório $destination!");

            } else {
                throw new \Exception("Não foi possível criar o diretório $destination!");
            };
        }
        return true;
    }

    /**
     * Faz o Upload do Arquivo
     * @param $_file
     * @param string $destination
     * @param bool $insert
     * @param array $permitidos
     * @param null $fileName
     * @param null $maxFileSize
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function upload($_file, $destination = 'upload', $insert = true, $permitidos = array(), $fileName = null, $maxFileSize = null)
    {
        try {
            $_file['name'] = self::limpaCaracteresEspeciais($_file['name'], false, '_');

            //valida o tipo de arquivo e pega a extensão do mesmo
            $ext = $this->validaTipos($_file, $permitidos);

            $dir = realpath('.') . DIRECTORY_SEPARATOR . $destination;
            //tenta criar e dar permissão ao diretório
            $this->criarDiretorio($dir, $destination);

            $fileName = !is_null($fileName) ? $fileName . "." . $ext : $_file['name'];

            $adapter = new \Zend_File_Transfer_Adapter_Http();
            $adapter->addFilter('Rename', $fileName);
            $adapter->setDestination($dir);

            if (!is_null($maxFileSize))
                $adapter->addValidator("Size", false, $maxFileSize);

            //verifica se o upload falhou
            if (!$adapter->receive())
                throw new \Exception(implode("\n", $adapter->getMessages()));

            $upload = new \Zend_File_Transfer();
            $status = $upload->receive();

            //verifica se é para salvar o upload no banco
            if ($insert)
                $status = $this->save(array('st_upload' => $fileName));

        } catch (\Exception $e) {
            throw $e;
        }
        return $status;

    }


    /**
     * Subistitui caracteres especiais
     * @param $string
     * @return mixed
     */
    public static function limpaCaracteresEspeciais($string, $lower = false, $trocaespaco = false)
    {

        $nstring = str_replace(str_split('áàãâäÁÀÃÂÄéèêëÉÈÊËíìîïÍÌÎÏóòõôöÓÒÕÔÖúùûüÚÙÛÜñÑçÇÿýÝ[]', 2), str_split('aaaaaAAAAAeeeeEEEEiiiiIIIIoooooOOOOOuuuuUUUUnNcCyyY--'), $string);
        if ($trocaespaco) {
            $nstring = str_replace(str_split(' ', 2), str_split($trocaespaco), $nstring);
        }
        if ($lower) {
            $nstring = strtolower($nstring);
        }

        return $nstring;


    }

    /**
     * Retorna um unico upload de acordo com o id enviado
     * @author Neemias Santos <neemias.santos@unyleya.com.br>
     *
     * @param $id_upload
     * @return array
     * @throws \Zend_Exception
     */
    public function findUpload($id_upload)
    {
        $result = $this->find('\G2\Entity\Upload', $id_upload);
        return $result;
    }
}

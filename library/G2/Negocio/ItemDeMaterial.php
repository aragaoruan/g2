<?php

namespace G2\Negocio;

/**
 * Classe de negócio para ItemDeMaterial (tb_itemdematerial)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class ItemDeMaterial extends Negocio
{

    private $repositoryName = 'G2\Entity\ItemDeMaterial';

    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {

        try {
            if (isset($data['id_itemdematerial'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id_itemdematerial']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\ItemDeMaterial();
            }


            /*********************
             * Definindo atributos
             *********************/

            if (isset($data['st_itemdematerial']))
                $entity->setSt_itemdematerial($data['st_itemdematerial']);

            if (isset($data['nu_peso']))
                $entity->setNu_peso($data['nu_peso']);


            if (isset($data['nu_qtdepaginas']))
                $entity->setNu_qtdepaginas($data['nu_qtdepaginas']);

            // FK id_situacao (tb_situacao)
            if (isset($data['id_situacao']))
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', $data['id_situacao']));

            // FK id_situacao (tb_situacao)
            if (isset($data['id_upload']))
                $entity->setId_upload($this->find('\G2\Entity\Upload', $data['id_upload']));

            // FK id_tipodematerial (tb_tipodematerial)
            if (isset($data['id_tipodematerial']))
                $entity->setId_tipodematerial($this->find('\G2\Entity\TipodeMaterial', $data['id_tipodematerial']));

            $entity->setDt_cadastro(date('Y-m-d H:i:s'));

            if (isset($data['id_usuariocadastro']))
                $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $data['id_usuariocadastro']));
            else
                $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));


            if (isset($data['id_itemdematerial'])) {
                $retorno = $this->merge($entity);
            } else {
                $retorno = $this->persist($entity);
            }

            $data['id_itemdematerial'] = $retorno->getId_itemdematerial();

            /**
             * ItemDeMaterialDisciplina
             */
            // Buscar ItemDeMaterialDisciplina e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\ItemDeMaterialDisciplina', array(
                'id_itemdematerial' => $data['id_itemdematerial']
            ));

            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                parent::delete($ItemEntity);
            }

            // Adicionar os selecionados
            foreach ($data['disciplinas'] as $key => $item) {
                $novo = new \G2\Entity\ItemDeMaterialDisciplina();
                $novo->setId_itemdematerial($data['id_itemdematerial']);
                $novo->setId_disciplina($this->find('\G2\Entity\Disciplina', (int)$item));

                parent::save($novo);
            }



            /**
             * MaterialProjeto
             */
            // Buscar MaterialProjeto e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\MaterialProjeto', array(
                'id_itemdematerial' => $data['id_itemdematerial'],
                'id_usuariocadastro' => $this->sessao->id_usuario
            ));

            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                parent::delete($ItemEntity);
            }

            // Adicionar os selecionados
            foreach ($data['projetosPedagogicos'] as $key => $item) {
                $novo = new \G2\Entity\MaterialProjeto();
                $novo->setId_projetopedagogico($this->find('\G2\Entity\ProjetoPedagogico', (int)$item));
                $novo->setId_usuariocadastro($this->sessao->id_usuario);
                $novo->setId_itemdematerial($data['id_itemdematerial']);
                $novo->setDt_cadastro(date('Y-m-d H:i:s'));

                parent::save($novo);
            }



            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro('O registro foi salvo', \Ead1_IMensageiro::SUCESSO, $retorno->getId_itemdematerial());
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro ao o registro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }



}
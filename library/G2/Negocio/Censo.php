<?php

namespace G2\Negocio;

use G2\Utils\Helper;

/**
 * Classe Negocio para Censo
 *
 * @author Stéfan Amaral <stefan.amaral@unyleya.com.br>
 */
class Censo extends Negocio
{
    /**
     * @param array $params
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function gerarDadosCenso($params = array())
    {
        $success = array();
        $error = array();

        $camposObrigatorios = $this->getCamposObrigatorios();

        $query = $this->getRepository('\G2\Entity\VwCenso')
            ->createQueryBuilder("vw")
            ->select('vw');

        foreach ($params as $attr => $val) {
            $val = trim($val);
            $query->andWhere("vw.{$attr} = '{$val}'");
        }

        // Buscar matrículas já adicionadas na tb_censo
        $subq = $this->em->createQueryBuilder()->select('c.id_matricula')->from('\G2\Entity\Censo', 'c');

        // Filtrar somente matrículas que não estão na tb_censo
        $query->andWhere($query->expr()->notIn('vw.id_matricula', $subq->getDQL()));

        $query->orderBy('vw.nu_codigopolocursodistancia', 'ASC');
        $query->addOrderBy('vw.nu_codigocurso', 'ASC');
        $query->addOrderBy('vw.st_nome', 'ASC');

        /** @var \G2\Entity\VwCenso[] $results */
        $results = $query->getQuery()->getResult();

        if (is_array($results)) {
            foreach ($results as $result) {
                $data = $this->toArrayEntity($result);

                try {
                    // Remover caracteres especiais dos NOMES
                    $data['st_nome'] = strtoupper(Helper::removeSpecialChars($data['st_nome']));
                    $data['st_nomecompletomae'] = strtoupper(Helper::removeSpecialChars($data['st_nomecompletomae']));

                    // Verificar campos obrigatórios
                    foreach ($camposObrigatorios as $attr) {
                        if (is_null($data[$attr])) {
                            throw new \Exception('É necessário informar o campo ' . $attr);
                        }
                    }

                    // Verificar se especificou a deficiência
                    if (!$this->verificarDeficiencia($data)) {
                        throw new \Exception('É necessário especificar a deficiência');
                    }

                    // Verificar se informou pelo menos uma forma de ingresso
                    if (!$this->verificarFormaIngresso($data)) {
                        throw new \Exception('É necessário informar ao menos uma forma de ingresso');
                    }

                    // Verificar idade do aluno no ano de ingresso
                    $ano_nascimento = (int)substr($data['dt_nascimento'], 4);
                    $ano_ingresso = (int)substr($data['nu_semestreingcurso'], 2);
                    $diff = $ano_ingresso - $ano_nascimento;

                    if ($diff <= 13 || $diff >= 110) {
                        throw new \Exception('Verifique a data de nascimento do aluno');
                    }

                    $entity = new \G2\Entity\Censo();
                    $this->entitySerialize->arrayToEntity($data, $entity);

                    $this->save($entity);
                    $success[] = $data;
                } catch (\Exception $e) {
                    /** @var \G2\Entity\Entidade $entidade */
                    $entidade = $this->find('\G2\Entity\Entidade', $data['id_entidade']);

                    $data['st_nomeentidade'] = $entidade->getSt_nomeentidade();

                    $data['message'] = strlen($e->getMessage()) <= 200
                        ? $e->getMessage()
                        : 'Não foi possível salvar os dados do censo para este aluno';

                    $error[] = $data;
                }
            }
        }

        return array(
            'results' => $results,
            'error' => $error,
            'success' => $success
        );
    }

    /**
     * Gerar arquivo para importação do CENSO
     * @param array $params
     * @throws \Doctrine\ORM\Mapping\MappingException
     * @throws \Doctrine\ORM\Query\QueryException
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function gerarArquivoAluno($params)
    {
        $results = $this->contagemDadosPesquisa($params, true);

        $camposArquivo = $this->getCamposArquivo();
        $camposObrigatorios = $this->getCamposObrigatorios();
        $camposFixos = $this->getCamposFixos();

        $linhas = [];

        if (is_array($results) && $results) {
            // Criar cabeçalho APENAS UMA VEZ
            $linhas[] = $results[0]['tp_registrocabecalho']
                . '|' . $results[0]['id_iesinep']
                . '|' . $results[0]['tp_arquivocabecalho'];

            foreach ($results as $result) {
                $colunas = [];

                // Percorrer campos na ordem correta para adicionar ao arquivo
                foreach ($camposArquivo as $attr) {
                    $value = array_key_exists($attr, $result) ? $result[$attr] : null;

                    $tamanhoMapeado = $this->em->getClassMetadata('\G2\Entity\Censo')->getFieldMapping($attr)['length'];

                    // Manter o tamanho máximo o definido na entity
                    $value = substr($value, 0, $tamanhoMapeado);

                    // Se estiver no array de campos com tamanho FIXO E
                    // Possuir algum valor OU for campo obrigátorio
                    if (in_array($attr, $camposFixos) && ($value || in_array($attr, $camposObrigatorios))) {
                        $value = str_pad($value, $tamanhoMapeado, '0', STR_PAD_LEFT);
                    }

                    if (in_array($attr, array('tp_registroalunocurso'))) {
                        $value = "\n" . $value;
                    }

                    $colunas[] = $value;
                }

                $str = implode('|', $colunas);

                // Remover pipes sobrando
                $str = implode("\n", explode("|\n", $str));

                $linhas[] = $str;
            }
        }

        $strFinal = trim(implode("\n", $linhas));

        $txtName = array('CENSO');

        if (!empty($params['produto'])) {
            /** @var \G2\Entity\ProjetoPedagogico $projeto */
            $projeto = $this->find('\G2\Entity\ProjetoPedagogico', $params['produto']);
            $str = str_replace(' ', '_', Helper::removeSpecialChars($projeto->getSt_projetopedagogico()));
            $txtName[] = strtoupper($str);
        }

        if (!empty($params['anoReferencia'])) {
            $txtName[] = $params['anoReferencia'];
        }

        $txtName[] = time();

        $tmpTxtNameFile = tempnam(APPLICATION_PATH . "/../public/tmp/", "CENSO_");

        $handle = fopen($tmpTxtNameFile, "w");

        fwrite($handle, $strFinal);
        fclose($handle);

        header('Content-Type: application/txt');
        header('Content-disposition: attachment; filename=' . implode('_', $txtName) . '.txt');

        readfile($tmpTxtNameFile);
    }

    /**
     * pega os nomes de todos os polos abaixo da graduação
     *
     * @return array
     */
    public function getNomesPolos()
    {
        $sql = $this->em->getRepository('G2\Entity\VwEntidadeRecursiva')
            ->createQueryBuilder('e')
            ->select(
                array(
                    'e.id_entidade',
                    'e.st_nomeentidade'
                )
            )
            ->distinct()
            ->where('e.id_entidade = :entidade')
            ->orWhere('e.id_entidadepai = :entidade')
            ->orderBy('e.id_entidade')
            ->setParameter('entidade', $this->getId_entidade());

        return $sql->getQuery()->getArrayResult();
    }

    /**
     * pega os nomes de todos os polos
     *
     * @return array
     */
    public function getNomesCursos()
    {
        $sql = $this->em->getRepository('G2\Entity\Produto')
            ->createQueryBuilder('c')
            ->select(
                array(
                    'c.id_produto',
                    'c.st_produto'
                )
            )
            ->orderBy('c.id_produto');

        return $sql->getQuery()->getArrayResult();
    }

    /**
     * pega todos os anos que possuem dados do CENSO para migração
     *
     * @return array
     */
    public function getAnosCensoPopulados()
    {
        $sql = $this->em->getRepository('G2\Entity\Censo')
            ->createQueryBuilder('c')
            ->select(
                array(
                    'DISTINCT c.nu_anocenso'
                )
            )
            ->orderBy('c.nu_anocenso');

        return $sql->getQuery()->getArrayResult();
    }

    /**
     * pega todos os anos que possuem alguma matricula
     *
     * @return array
     */
    public function getAnosComMatricula()
    {
        $sql = $this->em->getRepository('G2\Entity\VwMatricula')
            ->createQueryBuilder('m')
            ->select(
                array(
                    'DISTINCT (m.nu_anomatricula)'
                )
            )
            ->where('m.bl_ativo = 1')
            ->orderBy('m.nu_anomatricula');

        return $sql->getQuery()->getArrayResult();
    }

    /**
     * realiza contagem de dados para popular tabela em tela de download do arquivo faso fullData = false
     * Caso fullData = true, retorna todos os dados, não comente a contagem
     *
     * @param $params
     * @param bool $fullData
     * @return array
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function contagemDadosPesquisa($params, $fullData = false)
    {
        if ($fullData) {
            return $this->getData($params);
        }

        return array(
            'total' => $this->getData($params, null, true),
            'enem' => $this->getData($params, 'nu_formaingselecaoenem', true),
            'simplificada' => $this->getData($params, 'nu_formaingselecaoselecaosimplificada', true),
            'remanescentes' => $this->getData($params, 'nu_formaingselecaovagasremanescentes', true)
        );
    }

    /**
     * @param $params
     * @param string $forma_ingresso
     * @param bool $return_count
     * @return array|int
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function getData($params, $forma_ingresso = null, $return_count = false)
    {
        $dql = $this->em->getRepository('\G2\Entity\Censo')
            ->createQueryBuilder('c')
            ->leftJoin('\G2\Entity\Matricula', 'mt', 'WITH', 'mt.id_matricula = c.id_matricula')
            ->select('c');

        /**
         * @history GII-9361
         */
        $dql->where('mt.bl_institucional = 0');

        if ($forma_ingresso) {
            $dql->andWhere("c.{$forma_ingresso} = 1");
        }

        if (!empty($params['polo'])) {
            $dql->andWhere('mt.id_entidadematricula = :id_entidadematricula')
                ->setParameter('id_entidadematricula', $params['polo']);
        }

        if (!empty($params['produto'])) {
            //$dql->andWhere('c.nu_codigocurso = :curso')
            $dql->andWhere('mt.id_projetopedagogico = :id_projetopedagogico')
                ->setParameter('id_projetopedagogico', $params['produto']);
        }

        $dql->andWhere('c.nu_anomatricula >= :anoInicio')
            ->setParameter('anoInicio', $params['anoInicio'])
            ->andWhere('c.nu_anomatricula <= :anoFim')
            ->setParameter('anoFim', $params['anoFim'])
            ->andWhere('c.nu_anocenso = :anocenso')
            ->setParameter('anocenso', $params['anoReferencia']);

        if ($return_count) {
            $dql->select('count(c.nu_anocenso)');
            return $dql->getQuery()->getSingleScalarResult();
        }

        return $dql->getQuery()->getArrayResult();
    }

    /**
     * Verificar se foi marcado como "POSSUI DEFICIENCIA"
     * @param array $data
     * @return bool
     */
    public function verificarDeficiencia($data)
    {
        if (!empty($data['nu_alunodeftranstsuperdotacao'])
            && empty($data['nu_tipodeficienciacegueira'])
            && empty($data['nu_tipodeficienciabaixavisao'])
            && empty($data['nu_tipodeficienciasurdez'])
            && empty($data['nu_tipodeficienciaauditiva'])
            && empty($data['nu_tipodeficienciafisica'])
            && empty($data['nu_tipodeficienciasurdocegueira'])
            && empty($data['nu_tipodeficienciamultipla'])
            && empty($data['nu_tipodeficienciaintelectcual'])
            && empty($data['nu_tipodeficienciaautismo'])
            && empty($data['nu_tipodeficienciaasperger'])
            && empty($data['nu_tipodeficienciarett'])
            && empty($data['nu_tipodeficienciatranstdesinfancia'])
            && empty($data['nu_tipodeficienciasuperdotacao'])
        ) {
            return false;
        }

        return true;
    }

    /**
     * Verificar se possui uma forma de ingresso informada
     * @param array $data
     * @return bool
     */
    public function verificarFormaIngresso($data)
    {
        return !empty($data['nu_formaingselecaovestibular'])
            || !empty($data['nu_formaingselecaoenem'])
            || !empty($data['nu_formaingselecaoavaliacaoseriada'])
            || !empty($data['nu_formaingselecaoselecaosimplificada'])
            || !empty($data['nu_formaingselecaoegressobili'])
            || !empty($data['nu_formaingselecaoegressopecg'])
            || !empty($data['nu_formaingselecaotrasnfexofficio'])
            || !empty($data['nu_formaingselecaodecisaojudicial'])
            || !empty($data['nu_formaingselecaovagasremanescentes'])
            || !empty($data['nu_formaingselecaovagasprogespeciais']);
    }

    public function getCamposArquivo()
    {
        return array(
            'tp_registroaluno',
            'id_alunoinep',
            'st_nome',
            'st_cpf',
            'st_docuestrangeiropassaporte',
            'dt_nascimento',
            'nu_sexo',
            'nu_corraca',
            'st_nomecompletomae',
            'nu_nacionalidade',
            'nu_ufnascimento',
            'nu_municipionascimento',
            'st_paisorigem',
            'nu_alunodeftranstsuperdotacao',
            'nu_tipodeficienciacegueira',
            'nu_tipodeficienciabaixavisao',
            'nu_tipodeficienciasurdez',
            'nu_tipodeficienciaauditiva',
            'nu_tipodeficienciafisica',
            'nu_tipodeficienciasurdocegueira',
            'nu_tipodeficienciamultipla',
            'nu_tipodeficienciaintelectcual',
            'nu_tipodeficienciaautismo',
            'nu_tipodeficienciaasperger',
            'nu_tipodeficienciarett',
            'nu_tipodeficienciatranstdesinfancia',
            'nu_tipodeficienciasuperdotacao',
            'tp_registroalunocurso',
            'nu_semestrereferencia',
            'nu_codigocurso',
            'nu_codigopolocursodistancia',
            'st_idnaies',
            'nu_turnoaluno',
            'nu_situacaovinculoalunocurso',
            'nu_cursoorigem',
            'nu_semestreconclusao',
            'nu_alunoparfor',
            'nu_semestreingcurso',
            'nu_tipoescolaconclusaoensinomedio',
            'nu_formaingselecaovestibular',
            'nu_formaingselecaoenem',
            'nu_formaingselecaoavaliacaoseriada',
            'nu_formaingselecaoselecaosimplificada',
            'nu_formaingselecaoegressobili',
            'nu_formaingselecaoegressopecg',
            'nu_formaingselecaotrasnfexofficio',
            'nu_formaingselecaodecisaojudicial',
            'nu_formaingselecaovagasremanescentes',
            'nu_formaingselecaovagasprogespeciais',
            'nu_mobilidadeacademica',
            'nu_tipomodalidadeacademica',
            'st_iesdestino',
            'nu_tipomodalidadeacademicainternacional',
            'st_paisdestino',
            'nu_programareservavagas',
            'nu_programareservavagasetnico',
            'nu_programareservavagaspessoadeficiencia',
            'nu_programareservavagasestudanteescolapublica',
            'nu_programareservavagassocialrendafamiliar',
            'nu_programareservavagasoutros',
            'nu_finestud',
            'nu_finestudreembfies',
            'nu_finestudreembgovest',
            'nu_finestudreembgovmun',
            'nu_finestudreembies',
            'nu_finestudreembentidadesexternas',
            'nu_finestudreembprouniintegral',
            'nu_finestudreembprouniparcial',
            'nu_finestudreembentidadesexternas_2',
            'nu_finestudreembgovest_2',
            'nu_finestudreembies_2',
            'nu_finestudreembgovmun_2',
            'nu_apoiosocial',
            'nu_tipoapoiosocialalimentacao',
            'nu_tipoapoiosocialmoradia',
            'nu_tipoapoiosocialtransporte',
            'nu_tipoapoiosocialmaterialdidatico',
            'nu_tipoapoiosocialbolsatrabalho',
            'nu_tipoapoiosocialbolsapermanencia',
            'nu_atvextcurr',
            'nu_atvextcurrpesquisa',
            'nu_bolsaremuneracaoatvextcurrpesquisa',
            'nu_atvextcurrextensao',
            'nu_bolsaremuneracaoatvextcurrextensao',
            'nu_atvextcurrmonitoria',
            'nu_bolsaremuneracaoatvextcurrmonitoria',
            'nu_atvextcurrestnaoobg',
            'nu_bolsaremuneracaoatvextcurrestnaoobg',
            'nu_cargahorariototalporaluno',
            'nu_cargahorariointegralizadapeloaluno'
        );
    }

    public function getCamposObrigatorios()
    {
        return array(
            'tp_registrocabecalho',
            'tp_arquivocabecalho',
            'tp_registroaluno',
            'nu_alunodeftranstsuperdotacao',
            'nu_corraca',
            'nu_nacionalidade',
            'dt_nascimento',
            'st_nome',
            'st_nomecompletomae',
            'st_paisorigem',
            'nu_sexo',
            'nu_codigocurso',
            'st_cpf',
            'nu_cargahorariototalporaluno',
            'nu_cargahorariointegralizadapeloaluno',
            'nu_tipoescolaconclusaoensinomedio',
            'tp_registroalunocurso',
            'nu_situacaovinculoalunocurso',
            'nu_formaingselecaoavaliacaoseriada',
            'nu_formaingselecaodecisaojudicial',
            'nu_formaingselecaoegressobili',
            'nu_formaingselecaoenem',
            'nu_formaingselecaoselecaosimplificada',
            'nu_formaingselecaovagasprogespeciais',
            'nu_formaingselecaovagasremanescentes',
            'nu_formaingselecaotrasnfexofficio',
            'nu_formaingselecaovestibular',
            'nu_apoiosocial',
            'nu_atvextcurr',
            'nu_programareservavagas',
            'nu_semestreingcurso'
        );
    }

    public function getCamposFixos()
    {
        return array(
            'tp_registrocabecalho',
            'tp_arquivocabecalho',
            'tp_registroaluno',
            'id_alunoinep',
            'st_cpf',
            'dt_nascimento',
            'nu_sexo',
            'nu_corraca',
            'nu_nacionalidade',
            'nu_ufnascimento',
            'nu_municipionascimento',
            'st_paisorigem',
            'nu_alunodeftranstsuperdotacao',
            'nu_tipodeficienciacegueira',
            'nu_tipodeficienciabaixavisao',
            'nu_tipodeficienciasurdez',
            'nu_tipodeficienciaauditiva',
            'nu_tipodeficienciafisica',
            'nu_tipodeficienciasurdocegueira',
            'nu_tipodeficienciamultipla',
            'nu_tipodeficienciaintelectcual',
            'nu_tipodeficienciaautismo',
            'nu_tipodeficienciaasperger',
            'nu_tipodeficienciarett',
            'nu_tipodeficienciatranstdesinfancia',
            'nu_tipodeficienciasuperdotacao',
            'tp_registroalunocurso',
            'nu_semestrereferencia',
            'nu_turnoaluno',
            'nu_situacaovinculoalunocurso',
            'nu_semestreconclusao',
            'nu_alunoparfor',
            'nu_semestreingcurso',
            'nu_tipoescolaconclusaoensinomedio',
            'nu_formaingselecaovestibular',
            'nu_formaingselecaoenem',
            'nu_formaingselecaoavaliacaoseriada',
            'nu_formaingselecaoselecaosimplificada',
            'nu_formaingselecaoegressobili',
            'nu_formaingselecaoegressopecg',
            'nu_formaingselecaotrasnfexofficio',
            'nu_formaingselecaodecisaojudicial',
            'nu_formaingselecaovagasremanescentes',
            'nu_formaingselecaovagasprogespeciais',
            'nu_mobilidadeacademica',
            'nu_tipomodalidadeacademica',
            'nu_tipomodalidadeacademicainternacional',
            'st_paisdestino',
            'nu_programareservavagas',
            'nu_programareservavagasetnico',
            'nu_programareservavagaspessoadeficiencia',
            'nu_programareservavagasestudanteescolapublica',
            'nu_programareservavagassocialrendafamiliar',
            'nu_programareservavagasoutros',
            'nu_finestud',
            'nu_finestudreembfies',
            'nu_finestudreembgovest',
            'nu_finestudreembgovmun',
            'nu_finestudreembies',
            'nu_finestudreembentidadesexternas',
            'nu_finestudreembprouniintegral',
            'nu_finestudreembprouniparcial',
            'nu_finestudreembentidadesexternas_2',
            'nu_finestudreembgovest_2',
            'nu_finestudreembies_2',
            'nu_finestudreembgovmun_2',
            'nu_apoiosocial',
            'nu_tipoapoiosocialalimentacao',
            'nu_tipoapoiosocialmoradia',
            'nu_tipoapoiosocialtransporte',
            'nu_tipoapoiosocialmaterialdidatico',
            'nu_tipoapoiosocialbolsatrabalho',
            'nu_tipoapoiosocialbolsapermanencia',
            'nu_atvextcurr',
            'nu_atvextcurrpesquisa',
            'nu_bolsaremuneracaoatvextcurrpesquisa',
            'nu_atvextcurrextensao',
            'nu_bolsaremuneracaoatvextcurrextensao',
            'nu_atvextcurrmonitoria',
            'nu_bolsaremuneracaoatvextcurrmonitoria',
            'nu_atvextcurrestnaoobg',
            'nu_bolsaremuneracaoatvextcurrestnaoobg'
        );
    }

}

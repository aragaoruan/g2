<?php

namespace G2\Negocio;

use G2\Utils\Helper;

/**
 * Classe Negocio responsável pelas lógica da geração de etiquetas
 * Class Etiqueta
 * @author João Marcos Bizarro Lopes <joao.lopes@unyleya.com.br>
 * @since 2017-08-25
 * @package G2\Negocio
 */
class Etiqueta extends Negocio
{

    /**
     * Constantes referentes aos repositórios
     */
    const DECLARACAO_REPOSITORY = 'G2\Entity\VwEtiquetaDeclaracao';
    const CERTIFICACAO_REPOSITORY = 'G2\Entity\VwEtiquetaCertificacao';
    const USUARIO_REPOSITORY = 'G2\Entity\VwEtiquetaUsuario';

    /**
     * Retorna a clásula where comum a todas as consultas da classe
     * @param $params
     * @return array
     */
    private function getCommomWhere($params)
    {
        return [
            'id_entidade' => $params['idEntidade'],
        ];
    }

    /**
     * Retorna o custom where referentes às datas (período)
     * @param $params
     * @return string
     */
    private function getCommomWhereDate($params)
    {
        $custom = "";
        if ($params['situacao'] == 'todos') {
            $custom = " AND (tb.dt_solicitacao BETWEEN '" . Helper::converterData($params['dtInicio'], 'Y-m-d') . "' AND '" . Helper::converterData($params['dtFim'], 'Y-m-d') . "' 
                        OR tb.dt_entrega BETWEEN '" . Helper::converterData($params['dtInicio'], 'Y-m-d') . "' AND '" . Helper::converterData($params['dtFim'], 'Y-m-d') . "') ";
        } else if ($params['situacao'] == 'enviados') {
            $custom = " AND tb.dt_entrega BETWEEN '" . Helper::converterData($params['dtInicio'], 'Y-m-d') . "' AND '" . Helper::converterData($params['dtFim'], 'Y-m-d') . "' ";
        } else if ($params['situacao'] == 'naoenviados') {
            $custom = " AND tb.dt_solicitacao BETWEEN '" . Helper::converterData($params['dtInicio'], 'Y-m-d') . "' AND '" . Helper::converterData($params['dtFim'], 'Y-m-d') . "' 
                        AND tb.dt_entrega IS NULL";
        }

        return $custom;
    }

    /**
     * Retorna o custom where referente a pessoa (busca por nome, cpf ou email)
     * @param $params
     * @return string
     */
    private function getCommomWherePessoa($params)
    {
        return "(tb.st_nomecompleto LIKE '%" . trim($params['pessoa']) . "%'
            OR tb.st_cpf LIKE '%" . trim($params['pessoa']) . "%'
            OR tb.st_email LIKE '%" . trim($params['pessoa']) . "%')";
    }

    /**
     * Retorna o custom where concatenado para pesquisas em comum
     * @param $params
     * @return array
     */
    private function getCommomCustomWhere($params)
    {
        return [
            $this->getCommomWherePessoa($params) . $this->getCommomWhereDate($params)
        ];
    }

    private function getSemData($params)
    {
        return [
            $this->getCommomWherePessoa($params)
        ];
    }
    /**
     * Retorna a consulta da VW_ETIQUETADECLARACAO
     * @param $params
     * @return array|null
     */
    public function findDeclaracao($params)
    {
        return $this->findCustom(
            self::DECLARACAO_REPOSITORY,
            $this->getCommomWhere($params),
            $this->getCommomCustomWhere($params)
        );
    }

    /**
     * Retorna a consulta da VW_ETIQUETACERTIFICACAO
     * @param $params
     * @return array|null
     */
    public function findCertificacao($params)
    {
        return $this->findCustom(
            self::CERTIFICACAO_REPOSITORY,
            $this->getCommomWhere($params),
            $this->getCommomCustomWhere($params)
        );
    }

    public function findSemParametro($params)
    {
        return $this->findCustom(
            self::USUARIO_REPOSITORY,
            $this->getCommomWhere($params),
            $this->getSemData($params)
        );
    }


}

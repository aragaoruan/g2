<?php

namespace G2\Negocio;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\OCI8\OCI8Exception;
use Doctrine\DBAL\Event\Listeners\OracleSessionInit;
use G2\Entity\Integracao\PontoSoft\ControlesPessoas;
use G2\Entity\Integracao\PontoSoft\PerfisAcessosPessoas;
use G2\Entity\Integracao\PontoSoft\TiposControlesPessoas;
use G2\Entity\Integracao\PontoSoft\TipoTreinamentosPessoas;
use G2\Entity\Integracao\PontoSoft\TurmasAcessos;
use G2\Entity\Integracao\PontoSoft\TurmasPessoasAcessos;
use G2\Entity\MatriculaIntegracao;
use G2\Entity\ProjetoPedagogico;
use G2\Entity\ProjetoPedagogicoIntegracao;
use G2\Entity\Turma as TurmaG2;
use G2\Entity\TurmaIntegracao;
use G2\Entity\UsuarioIntegracao;
use G2\Entity\VwMatriculaIntegracaoPontoSoft;

/**
 * Classe de negócio para Integração
 *
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class IntegracaoExterna extends Negocio
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    private $con;

    private $bo;

    /**
     * @param null $idEntidade
     * @param bool $nlsTimeFormat
     */
    public function __construct($idEntidade = null, $nlsTimeFormat = false)
    {
        parent::__construct();

        $this->bo = new \Ead1_BO();

        if ($idEntidade != null) {
            $this->con = $this->getConnectionIntegracao($idEntidade);

            //Configuração necessária para inserir as datas corretamente no oracle.
            if ($nlsTimeFormat) {
                $this->nlsTimeFormat();
            }
        }
    }

    /**
     * Configuração necessária para inserir as datas corretamente no oracle.
     *
     * Alteração feita para implementar toda a lógica dentro do arquivo
     */
    private function nlsTimeFormat()
    {
        $this->con->getEventManager()->addEventSubscriber(
            new OracleSessionInit()
        );
    }


    /**
     * Retorna os horários de acesso da PontoSoft
     *
     * @return \Ead1_Mensageiro
     */
    public function retornarHorariosAcessos()
    {

        try {
            $return = array();
            $acessos = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\HorariosAcessos')->findAll();
            if ($acessos) {
                foreach ($acessos as $acesso) {
                    if ($acesso instanceof \G2\Entity\Integracao\PontoSoft\HorariosAcessos) {
                        $return[] = array(
                            'cod_horario_acesso' => $acesso->getcod_horario_acesso(),
                            'descr_horario_acesso' => $acesso->getdescr_horario_acesso(),
                            'limite_horario_movel' => $acesso->getlimite_horario_movel(),
                            'padrao_hora_extra' => $acesso->getpadrao_hora_extra(),
                            'vetor_horarios' => $acesso->getvetor_horarios()
                        );

                    }
                }
            }
            if ($return) {
                return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro("Nenhum registro encontrado", \Ead1_IMensageiro::AVISO);
            }

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * @param ProjetoPedagogico $pp
     * @param $id_sistema
     * @return int
     * @throws \Exception
     */
    public function salvarDadosProjetoIntegracao(ProjetoPedagogico $pp, $id_sistema)
    {
        try {

            $this->beginTransaction();
            $this->con->beginTransaction();
            /*
             * Verificando se o projeto já existe na tabela de projetopedagogicointegracao
             * */
            $projetoIntegracao = $this->findOneBy('\G2\Entity\ProjetoPedagogicoIntegracao', array(
                'id_projetopedagogico' => $pp->getId_projetopedagogico(),
                'id_sistema' => $id_sistema,
                'id_entidade' => $this->sessao->id_entidade
            ));
            $cadastrarProjeto = true;
            if ($projetoIntegracao != null) {
                /*
                 * Se já existir iremos fazer um find na modelo da pontosoft para atualizar o item
                 * */
                $cadastrarProjeto = false;
                $tpTreinamento = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TipoTreinamentosPessoas')->find($projetoIntegracao->getId_projetopedagogicosistema());
                if (!$tpTreinamento) {
                    $this->delete($projetoIntegracao);
                    $cadastrarProjeto = true;
                }

            }

            if ($cadastrarProjeto) {

                /*
                 * Se não existir iremos fazer uma pesquisa para pegar o ultimo inserido na tabela
                 * */
                $lastItem = $this->getLastItemTable('\G2\Entity\Integracao\PontoSoft\TipoTreinamentosPessoas', 'tipo_treinamento');
                $nextId = $lastItem != 0 ? $lastItem['tipo_treinamento'] + 1 : 1;

                $tpTreinamento = new TipoTreinamentosPessoas();
                $tpTreinamento->setTipoTreinamento($nextId);

                //Setando e salvando os dados na tabela de projetopedagogicointegracao
                $projpedintegracao = new ProjetoPedagogicoIntegracao();
                $projpedintegracao->setId_projetopedagogico($pp);
                $projpedintegracao->setId_projetopedagogicosistema($nextId);
                $projpedintegracao->setId_sistema($this->getReference('\G2\Entity\Sistema', $id_sistema));
                $projpedintegracao->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));

                $this->save($projpedintegracao);
            }

            $tpTreinamento->setDescrTreinamento($this->bo->substituirCaracteresEspeciais(strtoupper($pp->getSt_projetopedagogico())));
            $tpTreinamento->setConteudo($this->bo->substituirCaracteresEspeciais(strtoupper($pp->getSt_projetopedagogico())));
            $tpTreinamento->setCargaHorariaPadrao($pp->getNu_cargahoraria());

            //Salvando/Atualizando os dados na PontoSoft
            $this->con->persist($tpTreinamento);
            $this->con->flush();

            $this->commit();
            $this->con->commit();

        } catch (\Exception $e) {
            $this->rollback();
            $this->con->rollback();
            throw $e;
        }

        return $tpTreinamento->getTipoTreinamento();
    }

    /**
     * @param $repository
     * @param $field
     * @return int
     */
    private
    function getLastItemTable($repository, $field)
    {
        $repositoryName = $this->con->getRepository($repository);

        $query = $repositoryName->createQueryBuilder('tb')
            ->select('tb.' . $field)
            ->orderBy('tb.' . $field, 'desc')
            ->setMaxResults(1);

        $result = $query->getQuery()->getResult();


        return (empty($result)) ? 0 : $result[0];
    }

    /**
     * @param TurmaG2 $turma
     * @param $id_sistema
     * @param $id_projetopedagogicosistema
     * @return bool
     * @throws \Exception
     */
    public function salvarDadosTurmaIntegracao(TurmaG2 $turma, $id_sistema, $id_projetopedagogicosistema)
    {

        $this->beginTransaction();
        $this->con->beginTransaction();
        try {

            /*
             * Verificando se a Turma já existe na tabela de turmaintegracao
             * */
            $turmaIntegracao = $this->findOneBy('\G2\Entity\TurmaIntegracao', array(
                'id_turma' => $turma->getId_turma(),
                'id_sistema' => $id_sistema
            ));
            $cadastrarTurma = true;
            /**
             * NÃO ATUALIZAMOS MAIS OS DADOS POR AQUI
             */
            if ($turmaIntegracao != null) {
                /*
                 * Se já existir iremos fazer um find na modelo da pontosoft para atualizar o item
                 * */
                $cadastrarTurma = false;
                $tAcessos = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasAcessos')->find($turmaIntegracao->getst_turmasistema());
                if (!$tAcessos) {
                    $this->delete($turmaIntegracao);
                    $cadastrarTurma = true;
                }
            }
            if ($cadastrarTurma) {

                //Setando e salvando os dados na tabela de projetopedagogicointegracao
                $tAcessos = new TurmasAcessos();

                //Será passado um campo específico ao inves do id da turma
                $tAcessos->setCodTurma($turma->getst_codigo());

                $turmaObj = new TurmaIntegracao();
                $turmaObj->setId_turma($turma);
                $turmaObj->setst_turmasistema($turma->getst_codigo());
                $turmaObj->setId_sistema($this->find('\G2\Entity\Sistema', $id_sistema));


                $this->save($turmaObj);

                $turma->getDt_inicio()->setTime(00, 00, 00);
                $tAcessos->setTipoTreinamento($id_projetopedagogicosistema);
                $tAcessos->setDataInicialTurma($turma->getDt_inicio());
                $tAcessos->setDataFinalTurma(new \DateTime('12/31/2020'));

                //Salvando/Atualizando os dados na PontoSoft
                $this->con->persist($tAcessos);
                $this->con->flush();
            }

            $this->commit();
            $this->con->commit();

        } catch (\Exception $e) {
            $this->rollback();
            $this->con->rollback();
            throw $e;
        }
        return true;
    }

    /**
     * @param array $vwpessoa
     * @param $id_sistema
     * @return bool
     * @throws \Exception
     */
    public function salvarDadosAlunoPontoSoft(array $vwpessoa, $id_sistema)
    {
        try {
            /*
             * Iniciando as transações das conexões
             * */
            $this->beginTransaction();
            $this->con->beginTransaction();

            /*
             * Metodo onde irá salvar os dados do aluno nas seguintes tabelas:
             *
             * PontoSoft: CONTROLES_ACESSOS / Tipos_Controles_Pessoas / Perfis_Acessos_Pessoas
             *
             * G2: tb_usuariointegracao
             *
             * Retorna um array de objetos
             *
             * */
            $aluno = $this->salvarDadosAlunoUnico($vwpessoa, $id_sistema);

            $cPessoas = $aluno['cPessoas'];

            /*
             * Verificando se o usuário existe na vw_matriculaintegracaopontosoft
             *
             * */
            $vwMatriculaIntegracao = $this->findBy('\G2\Entity\VwMatriculaIntegracaoPontoSoft', array(
                'id_usuario' => $vwpessoa['id_usuario'],
                'id_entidade' => $this->sessao->id_entidade
            ));

            if (!empty($vwMatriculaIntegracao)) {
                foreach ($vwMatriculaIntegracao as $vw) {

                    /*
                     * Metodo para salvar a integracao com a matricula e Turmas Pessoas Acessos
                     * */
                    $this->salvarDadosMatriculaIntegracaoETurmasPessoasAcessos($vw, $cPessoas, $id_sistema);
                }
            }

            /*
             * Fechando as transações
             * */
            $this->commit();
            $this->con->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
        return true;
    }

    /**
     * @param array $vwpessoa
     * @param $id_sistema
     * @return array
     * @throws \Exception
     */
    private
    function salvarDadosAlunoUnico(array $vwpessoa, $id_sistema)
    {
        try {
            /*
             * Verificando se a Turma já existe na tabela de turmaintegracao
             */
            $usuarioIntegracao = $this->findOneBy('\G2\Entity\UsuarioIntegracao', array(
                'id_usuario' => $vwpessoa['id_usuario'],
                'id_sistema' => $id_sistema,
                'id_entidade' => $vwpessoa['id_entidade']
            ));

            $novoUser = false;
            $integrado = false;


            if ($usuarioIntegracao != null) {
                $integrado = true;
                /*
                 * Se já existir iremos fazer um find na modelo da pontosoft para atualizar o item
                 * */
                $cPessoas = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\ControlesPessoas')->find($usuarioIntegracao->getSt_codusuario());

                // Procura se tem alguém com esse número de cartão
                $cPessoasCartao = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\ControlesPessoas')
                    ->findOneBy(array('cod_cartao' => $vwpessoa['st_identificacao']));
                // Retorna um erro caso o cartão já esteja sendo usado por alguem com cpf diferente
                if($cPessoasCartao && $cPessoasCartao->getCpf() != $this->limpaCpfCnpj($vwpessoa['st_cpf'])){
                    $erro = array(
                        'error' => 'Já existe esse número de cartão usado pela pessoa ' . $cPessoasCartao->getNomePessoa() . ' Cartao: ' . $vwpessoa['st_identificacao'].' ',
                        'status' => 'Não enviado'
                    );
                    return $erro;


                 }

                if (!$cPessoas) {

                    $novoUser = true;
                    $nextId = $this->getLastIdCPessoa();
                    //Setando e salvando os dados na tabela de projetopedagogicointegracao
                    $cPessoas = new ControlesPessoas();
                    $cPessoas->setCodPessoa($nextId);
                }
            } else {
                $cPessoas = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\ControlesPessoas')
                    ->findOneBy(array('cpf' => $this->limpaCpfCnpj($vwpessoa['st_cpf'])));
                if ($cPessoas == null) {
                    if ($cCartao = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\ControlesPessoas')->findOneBy(array('cod_cartao' => $vwpessoa['st_identificacao']))) {
                        $erro = array(
                            'error' => 'Já existe um aluno com este código de cartão: ' . $cCartao->getNomePessoa() . ' Cartao: ' . $cCartao->getCodCartao(),
                            'status' => 'Não enviado'
                        );
                        return $erro;
                    }
                    $novoUser = true;
                    $nextId = $this->getLastIdCPessoa();
                    //Setando e salvando os dados na tabela de projetopedagogicointegracao
                    $cPessoas = new ControlesPessoas();
                    $cPessoas->setCodPessoa($nextId);
                } else {
                    $nextId = $cPessoas->getCodPessoa();
                }
                try {
                    $usuarioObj = new UsuarioIntegracao();
                    $usuarioObj->setId_usuariocadastro($vwpessoa['id_usuario']);
                    $usuarioObj->setId_usuario($vwpessoa['id_usuario']);
                    $usuarioObj->setSt_codusuario($nextId);
                    $usuarioObj->setBl_encerrado(0);

                    $id_sistema = $this->find('\G2\Entity\Sistema', $id_sistema);
                    $usuarioObj->setId_sistema($id_sistema->getId_sistema());
                    $usuarioObj->setDt_cadastro(new \DateTime());
                    $usuarioObj->setId_entidade($vwpessoa['id_entidade'] ? $vwpessoa['id_entidade'] : $this->sessao->id_entidade);

                    $this->save($usuarioObj);
                } catch (OCI8Exception $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar/atualizar Usuario Integração Ponto Soft. Usuario Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                } catch (DBALException $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar/atualizar Usuario Integração Ponto Soft. Usuario Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                }
            }

            if (!empty($vwpessoa['st_nomecompleto'])) {
                try {
                    $bo = new \Ead1_BO();
                    $cPessoas->setNomePessoa($bo->substituirCaracteresEspeciais(strtoupper($vwpessoa['st_nomecompleto'])));
                    $cPessoas->setCpf($this->limpaCpfCnpj($vwpessoa['st_cpf']));
                    $cPessoas->setAcessoLiberado('N');
                    $cPessoas->setTipoAcesso('S');
                    $cPessoas->setPontoLiberado('N');
                    $cPessoas->setCartaoBloqueado('N');
                    $cPessoas->setSexo($vwpessoa['st_sexo'] == null ? 'F' : $vwpessoa['st_sexo']);
                    $cPessoas->setCodCartao($vwpessoa['st_identificacao']);

//                    Salvando/Atualizando os dados na PontoSoft
                    $this->con->persist($cPessoas);
                    $this->con->flush();
                } catch (OCI8Exception $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar/atualizar Cadastro de pessoa "$cPessoas". Usuario Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                } catch (DBALException $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar/atualizar Cadastro de pessoa "$cPessoas". Usuario Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                }

                try {
                    $pe = $this->findOneBy('\G2\Entity\Pessoa', array(
                        'id_usuario' => $vwpessoa['id_usuario'],
                        'id_entidade' => $vwpessoa['id_entidade']
                    ));


                    if ($pe instanceof \G2\Entity\Pessoa) {
                        if ($pe->getSt_identificacao() != $cPessoas->getCodCartao() && !$integrado) {
                            $pe->setSt_identificacao($cPessoas->getCodCartao());
                        }
                        $pe->setbl_alterado(0);

                        $this->save($pe);
                    }
                } catch (OCI8Exception $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar o Tipo de Controle de Pessoas ou Perfil de Acesso Pessoas no Ponto Soft.  Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                } catch (DBALException $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar o Tipo de Controle de Pessoas ou Perfil de Acesso Pessoas no Ponto Soft.  Entidade: ' . $vwpessoa['id_entidade'] . ' Usuario: ' . $vwpessoa['id_usuario'] . ' Turma: ' . $vwpessoa['id_turma'] . ' ' . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                }

                try {
                    if ($novoUser) {
                        $tiposControlesPessoas = new TiposControlesPessoas();
                        $tiposControlesPessoas->setCodPessoa($cPessoas->getCodPessoa());
                        $tiposControlesPessoas->setTipoPessoa(2);
                        $tiposControlesPessoas->setDataCadastro(new \DateTime());

                        //Salvando/Atualizando os dados na PontoSoft
                        $this->con->persist($tiposControlesPessoas);
                        $this->con->flush();

                        $perfilAcessoPessoas = new PerfisAcessosPessoas();
                        $perfilAcessoPessoas->setCodPessoa($cPessoas->getCodPessoa());
                        $perfilAcessoPessoas->setCodPerfil(1000);
                        $perfilAcessoPessoas->setDataInicio(new \DateTime());
                        $perfilAcessoPessoas->setDataFim(null);

                        //Salvando/Atualizando os dados na PontoSoft
                        $this->con->persist($perfilAcessoPessoas);
                        $this->con->flush();
                    }
                } catch (OCI8Exception $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar o Tipo de Controle de Pessoas ou Perfil de Acesso Pessoas no Ponto Soft.  Cod_Pessoa: ' . $cPessoas->getCodPessoa() . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                } catch (DBALException $e) {
                    $erro = array(
                        'error' => 'Erro ao salvar o Tipo de Controle de Pessoas ou Perfil de Acesso Pessoas no Ponto Soft.  Cod_Pessoa: ' . $cPessoas->getCodPessoa() . $e->getMessage(),
                        'status' => 'Não enviado'
                    );
                    return $erro;
                }
            }
        } catch (OCI8Exception $e) {
            $erro = array(
                'error' => 'Erro geral pontosoft ' . $e->getMessage(),
                'status' => 'Não enviado'
            );
            return $erro;
        } catch (DBALException $e) {
            $erro = array(
                'error' => 'Erro geral pontosoft ' . $e->getMessage(),
                'status' => 'Não enviado'
            );
            return $erro;
        }

        return array(
            'usuarioIntegracao' => $usuarioIntegracao,
            'cPessoas' => $cPessoas,
            'status' => 'Enviado',
            'error' => ''
        );
    }

    private
    function getLastIdCPessoa()
    {
        /*
                           * Se não existir iremos fazer uma pesquisa para pegar o ultimo inserido na tabela
                           * */
        $lastItem = $this->getLastItemTable('\G2\Entity\Integracao\PontoSoft\ControlesPessoas', 'cod_pessoa');
        $nextId = $lastItem != 0 ? $lastItem['cod_pessoa'] + 1 : 1;
        return $nextId;
    }

    /**
     * @param VwMatriculaIntegracaoPontoSoft $vw
     * @param ControlesPessoas $cPessoas
     * @param integer $id_sistema
     * @throws \Exception
     * @return boolean
     */
    private
    function salvarDadosMatriculaIntegracaoETurmasPessoasAcessos(VwMatriculaIntegracaoPontoSoft $vw, ControlesPessoas $cPessoas, $id_sistema)
    {
        try {
            /*
             * Verificando se o usuário existe na tb_matriculaintegracao
             * */
            $matriculaIntegracao = $this->findOneBy('\G2\Entity\MatriculaIntegracao', array(
                'id_matricula' => $vw->getId_matricula(),
                'id_sistema' => $id_sistema
            ));

            if ($matriculaIntegracao == null) {
                /*
                 * Criando um novo objeto para a matriculaIntegracao caso não exista
                 * */
                $mi = new MatriculaIntegracao();
                $mi->setId_matricula($this->find('\G2\Entity\Matricula', $vw->getId_matricula()));
                $mi->setId_matriculasistema($vw->getId_turma());
                $mi->setId_sistema($this->find('\G2\Entity\Sistema', $id_sistema));
                $mi->setBl_encerrado(0);

                $this->save($mi);
            }

            /*
             * Verificando se já existe um usuário cadastrado para a mesma turma  na tablema de Turmas_pessoas_acessos
             *
             * */
            $tipoPessoasAcessos = $tiposPessoasAcessos = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasPessoasAcessos')
                ->findOneBy(array(
                    'cod_turma' => $vw->getst_turmasistema(),
                    'cod_pessoa' => $cPessoas->getCodPessoa(),
                ));

            if ($tipoPessoasAcessos == null) {
                /*
                 * Metodo onde irá salvar os dados das turmas acessos:
                 *
                 * PontoSoft: Turmas acessos pessoas
                 *
                 * Retorna true
                 * */
                $this->salvarTurmasPessoasAcessos($cPessoas, new TurmasPessoasAcessos(), $vw);
            }

            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param ControlesPessoas $cPessoas
     * @param TurmasPessoasAcessos $turmasPessoasAcessos
     * @param VwMatriculaIntegracaoPontoSoft $vw
     * @throws \Exception
     */
    private
    function salvarTurmasPessoasAcessos(ControlesPessoas $cPessoas, TurmasPessoasAcessos $turmasPessoasAcessos, VwMatriculaIntegracaoPontoSoft $vw)
    {
        try {
            $idTipoTreinamento = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasAcessos')->findOneBy(array('cod_turma' => $vw->getst_turmasistema()));

            if ($idTipoTreinamento != null) {
                $turmasPessoasAcessos->setCodPessoa($cPessoas->getCodPessoa());
                $turmasPessoasAcessos->setDataInicial($vw->getDt_iniciomatricula());
                $turmasPessoasAcessos->setDataInicialTurma($idTipoTreinamento->getDataInicialTurma());
                $turmasPessoasAcessos->setTipoTreinamento($idTipoTreinamento->getTipoTreinamento());
                $turmasPessoasAcessos->setCodTurma($idTipoTreinamento->getCodTurma());

                //Salvando/Atualizando os dados na PontoSoft
                $this->con->persist($turmasPessoasAcessos);
                $this->con->flush();
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Nessa atualização transferir o processamento das informações para a negocio
     * Esse método busca as entidades que tem integração com a pontosoft e busca os alunos habilitados
     * parta a sincronização entre os bancos, após salvar no banco da pontosoft salva também no G2 que aquele
     * aluno foi sincronizado
     *
     * @atualizacao neemias santos <neemias.santos@unyleya.com.br>
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function sincronizarDadosAlunosPontosoft()
    {
        //determina a constante pra não ficar escrevendo isso no código inteiro
        $id_sistema = \G2\Constante\Sistema::PONTOSOFT;

        //Buscas as entidades que estão registradas para enviar os alunos ao pontosoft
        $entidadeIntegracao = $this->findBy('\G2\Entity\EntidadeIntegracao', array('id_sistema' => $id_sistema));

        if (isset($entidadeIntegracao) && !empty($entidadeIntegracao)) {
            $log = array();
            foreach ($entidadeIntegracao as $entidade) {
                $id_entidade = $entidade->getId_entidade();

                if (isset($id_entidade) && !empty($id_entidade)) {

                    $entidade = $this->findOneBy('\G2\Entity\Entidade', array('id_entidade' => $id_entidade));

                    if (isset($entidade) && !empty($entidade)) {
                        $log[$id_entidade] = array(
                            'id_entidade' => $id_entidade,
                            'st_nomeentidade' => $entidade->getSt_nomeentidade()
                        );

                        //Procurando todos os alunos que ainda não foram inseridos e vinculados as turmas;
                        $vwMatriculaIntegracao = $this->findBy('\G2\Entity\VwMatriculaIntegracaoPontoSoft', array('id_entidade' => $id_entidade));

                        if (!empty($vwMatriculaIntegracao)) {

                            foreach ($vwMatriculaIntegracao as $key => $vw) {

                                try {
                                    //Instacia a entidade para a conexao ao banco
                                    $this->con = $this->getConnectionIntegracao($id_entidade);
                                    //Configuração necessária para inserir as datas corretamente no oracle.
                                    $this->nlsTimeFormat();
                                    $this->beginTransaction();

                                    /*
                                     * Chamando a função para salvar os dados do usuário
                                    */
                                    $aluno = $this->salvarDadosAlunoUnico($this->toArrayEntity($vw), $id_sistema);

                                    if (isset($aluno['cPessoas'])) {
                                        $cPessoas = $aluno['cPessoas'];

                                        /*
                                         * Metodo para salvar a integracao com a matricula e Turmas Pessoas Acessos
                                         * */
                                        $this->salvarDadosMatriculaIntegracaoETurmasPessoasAcessos($vw, $cPessoas, $id_sistema);
                                    }

                                    /*
                                     * Metodo que carrega os dados para visualização
                                     */
                                    $log[$id_entidade]['alunos'][$key]['st_nomecompleto'] = $vw->getSt_nomecompleto();
                                    $log[$id_entidade]['alunos'][$key]['id_matricula'] = $vw->getId_matricula();
                                    $log[$id_entidade]['alunos'][$key]['st_turmasistema'] = $vw->getSt_turmasistema();
                                    $log[$id_entidade]['alunos'][$key]['status'] = $aluno['status'];
                                    $log[$id_entidade]['alunos'][$key]['error'] = ($aluno['error']) ? $aluno['error'] : '--';

                                    $this->commit();

                                } catch (\PDOException $e) {
                                    $log[$id_entidade]['alunos'][$key]['st_nomecompleto'] = $vw->getSt_nomecompleto();
                                    $log[$id_entidade]['alunos'][$key]['id_matricula'] = $vw->getSt_nomecompleto();
                                    $log[$id_entidade]['alunos'][$key]['st_turmasistema'] = $vw->getSt_turmasistema();
                                    $log[$id_entidade]['alunos'][$key]['status'] = 'Não Enviado';
                                    $log[$id_entidade]['alunos'][$key]['error'] = $aluno;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $log;
    }

    /**
     * Integra a Grade Horária com a PontoSoft
     */
    public
    function salvarGradePontoSoft()
    {

        echo '<h3>Sincronizando Grade Ponto Soft</h3>';

        $query = $this->em->getRepository('\G2\Entity\VwGradePontoSoft')->createQueryBuilder('gr')
            ->select('gr.st_turmasistema')
            ->distinct()
            ->getQuery();
        $turmas = $query->getResult();
        if ($turmas) {
            foreach ($turmas as $turma) {

                echo 'Turma: ' . $turma['st_turmasistema'] . '<br>';

//                    $this->con->beginTransaction();
//                    $this->em->beginTransaction();
                try {

                    $grades = $this->findBy('\G2\Entity\VwGradePontoSoft', array('st_turmasistema' => $turma['st_turmasistema']));
                    if ($grades) {
                        /**
                         * Excluimos toda a grade da PontoSoft
                         */
                        $deletequery = $this->con
                            ->createQueryBuilder()->delete('\G2\Entity\Integracao\PontoSoft\TurmasAcessosSalas', 't')
                            ->where('t.cod_turma = \'' . $turma['st_turmasistema'] . '\'');
                        $numDeleted = $deletequery->getQuery()->execute();
                        if ($numDeleted) {
                            echo $numDeleted . ' Grades ecluídas na PontoSoft<br>';
                        }

                        foreach ($grades as $grade) {
                            if ($grade instanceof \G2\Entity\VwGradePontoSoft) {
                                echo 'Projeto: ' . $grade->getid_projetopedagogicosistema() . ' - Turma: ' . $grade->getst_turmasistema() . ' - Grade: ' . $grade->getid_gradehoraria() . ' - Data: ' . substr($grade->getdt_inicio(), 0, 10) . '  - Horario:  ' . $grade->getid_codhorarioacesso() . ' - Dia da Semana: ' . $grade->getid_diasemana() . '<br>';

                                $nprojeto = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TipoTreinamentosPessoas')->findOneBy(array(
                                    'tipo_treinamento' => $grade->getid_projetopedagogicosistema()
                                ));

                                if (!$nprojeto) {
                                    echo 'O Projeto Pedagógico não foi encontrado no PontoSoft.<br>';
                                    continue;
                                }

                                $nturma = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasAcessos')->findOneBy(array(
                                    'cod_turma' => $grade->getst_turmasistema(),
                                    'data_inicial_turma' => new \DateTime(substr($grade->getdt_inicio(), 0, 10)),
                                    'tipo_treinamento' => $grade->getid_projetopedagogicosistema()
                                ));

                                if (!$nturma) {
                                    $nturma = new \G2\Entity\Integracao\PontoSoft\TurmasAcessos();
                                    $nturma->setTipoTreinamento($grade->getid_projetopedagogicosistema());
                                    $nturma->setCodTurma($grade->getst_turmasistema());
                                    $nturma->setDataInicialTurma(new \DateTime(substr($grade->getdt_inicio(), 0, 10)));
                                    $nturma->setDataFinalTurma(new \DateTime('2050-12-31'));
                                    $this->con->persist($nturma);
                                    $this->con->flush();
                                    echo ' TurmasAcessos: TIPO_TREINAMENTO: ' . $nturma->getTipoTreinamento() . ' - COD_TURMA: ' . $nturma->getCodTurma() . ' - DATA_INICIAL ' . substr($grade->getdt_inicio(), 0, 10) . '   INSERIDA<br>';
                                }

                                $nturma = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasAcessos')->findOneBy(array(
                                    'cod_turma' => $grade->getst_turmasistema(),
                                    'data_inicial_turma' => new \DateTime(substr($grade->getdt_inicio(), 0, 10)),
                                    'tipo_treinamento' => $grade->getid_projetopedagogicosistema()
                                ));

                                if (!$nturma) {
                                    echo 'A TurmaAcessos não tem o Registro da Turma, a rotina de sincronização não conseguiu Inserir.<br>';
                                    continue;
                                }

                                $gradePontoSoft = new \G2\Entity\Integracao\PontoSoft\TurmasAcessosSalas();
                                $gradePontoSoft->settipo_treinamento((int)$grade->getid_projetopedagogicosistema());
                                $gradePontoSoft->setcod_horario_acesso((int)$grade->getid_codhorarioacesso());
                                $gradePontoSoft->setcod_sala((int)$grade->getst_codigocatraca());
                                $gradePontoSoft->setcod_turma($grade->getst_turmasistema());
                                $gradePontoSoft->setdata_inicial_turma(new \DateTime(substr($grade->getdt_inicio(), 0, 10)));
                                $gradePontoSoft->setdia_semana((int)$grade->getid_diasemana());

                                $this->con->persist($gradePontoSoft);
                                $this->con->flush();
                                echo ' TurmasAcessosSalas INSERIDA<br>';

                                $gradeH = $this->em->getRepository('\G2\Entity\GradeHoraria')->find($grade->getid_gradehoraria());
                                if ($gradeH instanceof \G2\Entity\GradeHoraria) {
                                    $gradeH->setbl_sincronizado(1);
                                    $this->em->persist($gradeH);
                                    $this->em->flush();
                                    echo ' GradeHoraria ' . $grade->getid_gradehoraria() . ' MARCADA COMO ATUALIZADA<br><br><br>';
                                }

                            }
                        }
                    }
                    echo '<br><br>';

//                        $this->con->commit();
//                        $this->em->commit();

                } catch (\Exception $e) {
//                        $this->con->rollback();
//                        $this->em->rollback();
                    echo '<p style="color: red">Projeto: ' . $grade->getid_projetopedagogicosistema() . ' - Turma: ' . $grade->getst_turmasistema() . ' - Grade: ' . $grade->getid_gradehoraria() . ' - Data: ' . substr($grade->getdt_inicio(), 0, 10) . '  - Horario:  ' . $grade->getid_codhorarioacesso() . ' - Dia da Semana: ' . $grade->getid_diasemana() . '<br>';
                    echo $e->getMessage() . '<br><br></p>';
                }

            }
        } else {
            echo '<p style="color: red">Nenhuma Grade para Sincronização</p>';
        }

    }

    /**
     * Cancela a Turma na Ponto SOFT
     *
     * @param TurmaG2 $turma
     * @throws \Exception
     */
    public
    function cancelarTurmaIntegracao(\G2\Entity\Turma $turma)
    {
        try {

            $turmaAcessos = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasAcessos')->find(array('cod_turma' => $turma->getst_codigo()));
            if ($turmaAcessos) {
                $turmaAcessos->setDataFinalTurma($turma->getDt_fim());
                $this->con->persist($turmaAcessos);
                $this->con->flush();
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }


    public
    function sincronizarMatriculasTurmasFinalizada($id_sistema)
    {
        try {
            /*
             * Procurando todos os alunos que ainda não foram inseridos e vinculados as turmas;
             * */
            $vwMatriculaEncerramento = $this->findAll('\G2\Entity\VwEncerramentoTurmaMatriculaPontoSoft');

            if (!empty($vwMatriculaEncerramento)) {
                /*
                 * Iniciando as transações
                 *
                 * */
                $this->beginTransaction();
                $this->con->beginTransaction();

                foreach ($vwMatriculaEncerramento as $vw) {
                    try {
                        switch ($vw->getBl_usuario()) {
                            case 0:
                                $this->cancelarTurmasPessoasIntegracao($this->toArrayEntity($vw));
                                break;
                            case 1:
                                $this->cancelarUsuarioIntegracao($this->toArrayEntity($vw), $id_sistema);
                                break;
                            default:
                                break;
                        }
                    } catch (\PDOException $e) {
                        $this->rollback();
                        $this->con->rollback();
                        throw $e;
                    }
                }
                $this->commit();
                $this->con->commit();
            }
        } catch (\Exception $e) {
            throw $e;
        }
        return true;
    }

    /**
     * @param array $vw
     * @throws \Exception
     */
    private
    function cancelarTurmasPessoasIntegracao(array $vw)
    {
        try {

            if (!isset($vw['st_turmasistema']) || !$vw['st_turmasistema'])
                throw new \Zend_Exception("O campo st_turmasistema é obrigatório");

            $turmaPessoasAcessos = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\TurmasPessoasAcessos')
                ->find(
                    array(
                        'cod_pessoa' => $vw['st_codusuario'],
                        'cod_turma' => $vw['st_turmasistema']
                    )
                );

            if ($turmaPessoasAcessos != null) {
                $turmaPessoasAcessos->setDataFinal(new \DateTime());

                //Salvando/Atualizando os dados na PontoSoft
                $this->con->persist($turmaPessoasAcessos);
                $this->con->flush();

                $matriculaIntegracao = $this->find('\G2\Entity\MatriculaIntegracao', array(
                    'id_matriculaintegracao' => $vw['id_matriculaintegracao']
                ));

                if ($matriculaIntegracao != null) {
                    $matriculaIntegracao->setBl_encerrado(1);
                    $this->save($matriculaIntegracao);
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    private
    function cancelarUsuarioIntegracao(array $vw)
    {
        try {
            $pessoaAcesso = $this->con->getRepository('\G2\Entity\Integracao\PontoSoft\PerfisAcessosPessoas')
                ->find(array('cod_pessoa' => $vw['st_codusuario']));

            if ($pessoaAcesso != null && $pessoaAcesso->getDataFim() == null) {
                $pessoaAcesso->setDataFim(new \DateTime());

                //Salvando/Atualizando os dados na PontoSoft
                $this->con->persist($pessoaAcesso);
                $this->con->flush();

                $usuarioIntegracao = $this->find('\G2\Entity\UsuarioIntegracao', array(
                    'id_usuariointegracao' => $vw['id_usuariointegracao']
                ));

                if ($usuarioIntegracao != null) {
                    $usuarioIntegracao->setBl_encerrado(1);
                    $this->save($usuarioIntegracao);
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
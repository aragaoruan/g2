<?php
/**
 * Classe de negócio para VwEcommerce
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
namespace G2\Negocio;


class VwEcommerce extends Negocio
{

    private $repository = '\G2\Entity\VwEcommerce';

    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Parâmetros para Pesquisa. Ao menos uma data é obrigatória
     * @param null $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaResultadoPesquisa($params = null)
    {
        try {

            if (!($params['dt_inicial_comprar'] && $params['dt_termino_comprar']) && !($params['dt_inicial_confirmacao'] && $params['dt_termino_confirmacao'])) {
                throw new \Exception("Voce precisa informar ao menos um Periodo para pesquisa!");
            }


            if ($params['dt_inicial_comprar'] || $params['dt_termino_comprar'] || $params['dt_inicial_confirmacao'] || $params['dt_termino_confirmacao']) {
                $dt_inicial_comprar = ($params['dt_inicial_comprar']) ? $this->converteDataBanco($params['dt_inicial_comprar'] . ' 00:00:00') : '';
                $dt_termino_comprar = ($params['dt_termino_comprar']) ? $this->converteDataBanco($params['dt_termino_comprar'] . ' 23:59:59') : '';
                $dt_inicial_confirmacao = ($params['dt_inicial_confirmacao']) ? $this->converteDataBanco($params['dt_inicial_confirmacao'] . ' 00:00:00') : '';
                $dt_termino_confirmacao = ($params['dt_termino_confirmacao']) ? $this->converteDataBanco($params['dt_termino_confirmacao'] . ' 23:59:59') : '';

                $params['dt_inicial_comprar'] = $dt_inicial_comprar;
                $params['dt_termino_comprar'] = $dt_termino_comprar;
                $params['dt_inicial_confirmacao'] = $dt_inicial_confirmacao;
                $params['dt_termino_confirmacao'] = $dt_termino_confirmacao;

                if ($params['id_entidade'] == '0') {
                    unset($params['id_entidade']);
                }

            }
            //PARAMETROS OBRIGATÓRIOS
            $params['id_entidadepai'] = $this->sessao->id_entidade;

            $resultado = $this->em->getRepository($this->repository)->pesquisaRelatorioEcommerce($params);
            return $resultado;
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception("Erro ao Pesquisar." . $e->getMessage());
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }


} 
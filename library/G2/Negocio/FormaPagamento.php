<?php

/**
 * Classe Negocio para Forma de Pagamento
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-05
 */

namespace G2\Negocio;

class FormaPagamento extends Negocio
{

    private $repositoryName = 'G2\Entity\FormaPagamento';
    private $bo;

    public function __construct()
    {
        $this->bo = new \FormaPagamentoBO();
        parent::__construct();
    }

    /**
     * Retornar formas de pagamento
     * @param array $where
     * @param array $order
     * @return G2\Entity\FormaPagamento
     * @throws \Exception
     */
    public function findByFormaPagamento(array $where = array(), array $order = array())
    {
        try {
            return $this->findBy($this->repositoryName, $where, $order);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Retorna Formas de Pagamento by id_entidade
     * @param integer $idEntidade Opicional parametro, quando não passado, pega o id_entidade da sessão
     * @return \G2\Entity\FormaPagamento
     * @throws \Exception
     */
    public function retornaFormaPagamentoByEntidade($idEntidade = null)
    {
        try {
            //Set id_entidade
            $idEntidade = ($idEntidade ? $idEntidade : $this->sessao->id_entidade);
            return $this->findByFormaPagamento(array(
                'id_entidade' => $idEntidade
            ));
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Retorna Formas de Pagamento por Aplicação
     * @return \Ead1_Mensageiro
     */
    public function retornarFormaPagamentoPorAplicacao()
    {
        $formaPagamentoTO = new \FormaPagamentoTO();
        $formaPagamentoAplicacaoTO = new \FormaPagamentoAplicacaoTO();

        $formaPagamentoTO->setBl_todosprodutos(FALSE);

        $mensageiro = $this->bo->retornarFormaPagamentoPorAplicacao($formaPagamentoTO, $formaPagamentoAplicacaoTO, array(1, 5, 6));

        return $mensageiro->toArrayAll();
    }


    /**
     * Retorna registros de FormaDiaVencimento
     * @param array $where array chave=>valor
     * @param bool $toArray true para retornar array de array | false para retornar array de objetos
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarFormaDiaVencimento(array $where = array(), $toArray = false)
    {
        try {
            foreach ($where as $key => $value) {
                if (!$value) {
                    unset($where[$key]);
                }
            }
            $result = $this->findBy('\G2\Entity\FormaDiaVencimento', $where);
            if ($toArray) {
                foreach ($result as $i => $row) {
                    $result[$i] = array(
                        'id_formadiavencimento' => $row->getId_formadiavencimento(),
                        'id_formapagamento' => $row->getId_formapagamento() ? $row->getId_formapagamento()->getId_formapagamento() : NULL,
                        'nu_diavencimento' => $row->getNu_diavencimento()
                    );
                }
            }
            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados." . $e->getMessage());
        }
    }

    /**
     * Retorna dados de VwFormaMeioDivisao
     * @param array $where chave=>valor
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarTipoDivisaoFinanceira(array $where = array())
    {
        try {
            foreach ($where as $key => $value) {
                if (!$value) {
                    unset($where[$key]);
                }
            }
            return $this->findBy('\G2\Entity\VwFormaMeioDivisao', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("ERro ao tentar consultar dados." . $e->getMessage());
        }
    }

    /**
     * Retorna dados de forma de pagamento relacionada com forma de pagamento aplicacao relação by id_entidade
     * @param $idEntidade
     * @return null|\Zend_Db_Table_Row_Abstract
     * @throws \Zend_Exception
     */
    public function retornaFormaPagamentoJoinFormaPagamentoAplicacaoRelacaoByEntidade($idEntidade)
    {
        try {
            $wsDAO = new \WebServiceDAO();
            return $wsDAO->retornarFormaPagamento($idEntidade);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados." . $e->getMessage());
        }
    }

    
    public function save($data)
    {

        try {
            if (isset($data['id_formapagamento'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id_formapagamento']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\FormaPagamento();
            }


            /*********************
             * Definindo atributos
             *********************/

            if (isset($data['st_formapagamento']))
                $entity->setSt_formapagamento($data['st_formapagamento']);

            // FK id_situacao (tb_situacao)
            if (isset($data['id_situacao']))
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', $data['id_situacao']));

            if (isset($data['st_descricao']))
                $entity->setSt_descricao($data['st_descricao']);

            if (isset($data['nu_entradavalormin']))
                $entity->setNu_entradavalormin($data['nu_entradavalormin']);

            if (isset($data['nu_entradavalormax']))
                $entity->setNu_entradavalormax($data['nu_entradavalormax']);

            if (isset($data['nu_valormin']))
                $entity->setNu_valormin($data['nu_valormin']);

            if (isset($data['nu_valormax']))
                $entity->setNu_valormax($data['nu_valormax']);

            if (isset($data['nu_valorminparcela']))
                $entity->setNu_valorminparcela($data['nu_valorminparcela']);

            $entity->setDt_cadastro(date('Y-m-d H:i:s'));

            $entity->setId_usuariocadastro($this->sessao->id_usuario);

            $entity->setId_entidade($this->sessao->id_entidade);

            if (isset($data['bl_todosprodutos']))
                $entity->setBl_todosprodutos($data['bl_todosprodutos']);

            if (isset($data['nu_multa']))
                $entity->setNu_multa($data['nu_multa']);

            if (isset($data['nu_juros']))
                $entity->setNu_juros($data['nu_juros']);

            if (isset($data['nu_maxparcelas']))
                $entity->setNu_maxparcelas($data['nu_maxparcelas']);

            if (isset($data['nu_jurosmin']))
                $entity->setNu_jurosmin($data['nu_jurosmin']);

            if (isset($data['nu_jurosmax']))
                $entity->setNu_jurosmax($data['nu_jurosmax']);

            $entity->setId_tipoformapagamentoparcela($this->find('\G2\Entity\TipoFormaPagamentoParcela', 1));

            $entity->setId_tipocalculojuros($this->find('\G2\Entity\TipoCalculoJuros', 1));


            if (isset($data['id_formapagamento'])) {
                $retorno = $this->merge($entity);
            } else {
                $retorno = $this->persist($entity);
            }

            $data['id_formapagamento'] = $retorno->getId_formapagamento();

            /**
             * Inserir FormaPagamentoAplicacaoRelacao
             */
            // Buscar FormaPagamentoAplicacaoRelacao e remover todos
            // para poder inserir somente o selecionado
            $ItemsEntity = $this->findBy('\G2\Entity\FormaPagamentoAplicacaoRelacao', array(
                'id_formapagamento' => $data['id_formapagamento']
            ));
            // Apagar o registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                parent::delete($ItemEntity);
            }

            // Adicionar o selecionado
            if (isset($data['id_formapagamentoaplicacao'])) {
                $novo = new \G2\Entity\FormaPagamentoAplicacaoRelacao();
                $novo->setId_formapagamento($data['id_formapagamento']);
                $novo->setId_formapagamentoaplicacao($data['id_formapagamentoaplicacao']);
                parent::save($novo);
            }


            /**
             * FormaMeioPagamento
             */
            // Buscar FormaMeioPagamento e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\FormaMeioPagamento', array(
                'id_formapagamento' => $data['id_formapagamento']
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                parent::delete($ItemEntity);
            }

            // Adicionar os selecionados
            foreach ($data['meios_pagamentos'] as $key => $item) {
                $novo = new \G2\Entity\FormaMeioPagamento();
                $novo->setId_formapagamento($data['id_formapagamento']);
                $novo->setId_meiopagamento($item);
                $novo->setId_tipodivisaofinanceira(1);

                parent::save($novo);
            }


            /**
             * FormaPagamentoProduto
             */
            // Buscar FormaPagamentoProduto e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\FormaPagamentoProduto', array(
                'id_formapagamento' => $data['id_formapagamento']
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                parent::delete($ItemEntity);
            }

            // Adicionar os selecionados
            foreach ($data['produtos'] as $key => $item) {
                $novo = new \G2\Entity\FormaPagamentoProduto();
                $novo->setId_produto($item);
                $novo->setId_formapagamento($data['id_formapagamento']);

                parent::save($novo);
            }


            /**
             * FormaPagamentoParcela
             */
            // Buscar FormaPagamentoParcela e remover todos
            // para poder inserir somente os selecionados
            $this->atualizarParcelas($data['parcelas'], $data['id_formapagamento']);


            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro('O registro foi salvo', \Ead1_IMensageiro::SUCESSO, $retorno->getId_formapagamento());
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro ao o registro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }

    // Metodo para gerenciar as parcelas (usado no metodo save)
    function atualizarParcelas($novasParcelas, $idFormaPagamento)
    {
        $ItemsEntity = $this->findBy('\G2\Entity\FormaPagamentoParcela', array(
            'id_formapagamento' => $idFormaPagamento
        ));
        // Apagar cada registro encontrado
        foreach ($ItemsEntity as $ItemEntity) {
            parent::delete($ItemEntity);
        }

        // Adicionar os selecionados
        foreach ($novasParcelas as $key => $item) {
            $novo = new \G2\Entity\FormaPagamentoParcela();
            $novo->setId_formapagamento($idFormaPagamento);
            $novo->setId_meiopagamento($this->find('\G2\Entity\MeioPagamento', $item['id_meiopagamento']['id_meiopagamento']));
            $novo->setNu_parcelaquantidademin($item['nu_parcelaquantidademin']);
            $novo->setNu_parcelaquantidademax($item['nu_parcelaquantidademax']);
            $novo->setNu_juros($item['nu_juros']);
            $novo->setNu_valormin($item['nu_valormin']);

            parent::save($novo);
        }
    }


}

<?php

namespace G2\Negocio;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;

//use G2\Entity\AreaConhecimento as AreaConhecimentoEntity;

/**
 * Classe de negócio para Area Conhecimento
 * @author Débora Castro  <debora.castro@unyleya.com.br>
 */
class AreaConhecimento extends Negocio
{

    private $repositoryName = 'G2\Entity\AreaConhecimento';
    private $situacaoNegocio;
    private $entidadeNegocio;
    private $areaEntidadeNegocio;

    public function __construct()
    {
        parent::__construct();
        $this->situacaoNegocio = new Situacao();
        $this->entidadeNegocio = new Entidade();
        $this->areaEntidadeNegocio = new AreaEntidade();
    }

    /**
     * Find By Entidade
     * @param integer $id_entidade
     * @return object G2\Entity\AreaConhecimento
     */
    public function findByEntidadeSessao($id_entidade = null, $id_origemareaconhecimento = 1)
    {
        if (!$id_entidade) {
            $id_entidade = $this->sessao->id_entidade;
        }

        $dql = 'SELECT a FROM G2\Entity\AreaConhecimento a '
            . 'JOIN G2\Entity\AreaEntidade AS ae WITH ae.id_areaconhecimento = a.id_areaconhecimento '
            . 'WHERE a.bl_ativo = 1 and a.id_situacao = 6 AND ae.id_entidade = ?1 AND a.id_origemareaconhecimento = ?2 '
            . 'ORDER BY a.st_areaconhecimento';
        $query = $this->em->createQuery($dql);
        $query->setParameter(1, $id_entidade);
        $query->setParameter(2, $id_origemareaconhecimento);

        return $query->getResult();
    }

    /**
     * Retorna areas vinculadas a projetos pedagogicos
     * @param integer $id_entidade
     * @return object G2\Entity\AreaConhecimento
     */
    public function findAreaComProjeto($id_entidade = null)
    {
        if (!$id_entidade) {
            $id_entidade = $this->sessao->id_entidade;
        }

        $dql = "SELECT DISTINCT ac
                FROM G2\Entity\Entidade AS e
                JOIN G2\Entity\ProjetoEntidade AS pe WITH pe.id_entidade = e.id_entidade AND pe.id_entidade = ?1
                JOIN G2\Entity\AreaProjetoPedagogico AS tap WITH tap.id_projetopedagogico = pe.id_projetopedagogico
                JOIN G2\Entity\AreaConhecimento AS ac WITH ac.id_areaconhecimento = tap.id_areaconhecimento AND ac.bl_ativo = 1 AND ac.id_situacao = 6
                ORDER BY ac.st_areaconhecimento";

        $query = $this->em->createQuery($dql);
        $query->setParameter(1, $id_entidade);

        return $query->getArrayResult();
    }

    /**
     *
     * @param integer $id
     * @return object G2\Entity\AreaConhecimento
     */
    public function findAreaConhecimento($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /*
     * @param integer $id
     * @return object G2\Entity\AreaConhecimento
     */

    public function findByOneAreaConhecimento($id)
    {
        return $this->findOneBy($this->repositoryName, $id);
    }

    /**
     *
     * @return G2\Entity\AreaConhecimento
     */
    public function findAllArea()
    {
        return $this->findAll($this->repositoryName);
    }

    /**
     * Update data
     * @param array $data
     * @return G2\Entity\AreaConhecimento
     * @Update 2013-10-29 Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function updateAreaConhecimento($data)
    {
        $situacao = $this->getReference('\G2\Entity\Situacao', $data['id_situacao']);
        $tipoareaconhecimento = $this->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);
        $tm = $this->find($this->repositoryName, $data['id_areaconhecimento']);

        $tm->setSt_areaconhecimento($data['st_areaconhecimento'])
            ->setId_situacao($situacao)
            ->setId_tipoareaconhecimento($tipoareaconhecimento)
//            ->setId_usuariocadastro($this->sessao->id_usuario)
            ->setSt_tituloexibicao($data['st_tituloexibicao'])
            ->setSt_descricao($data['st_descricao']);
//            ->setBl_ativo(true)
//            ->setId_entidade($this->sessao->id_entidade);
//
//        $this->em->merge($tm);
//        $this->em->flush();
        $tm = $this->save($tm);
        $this->areaEntidadeNegocio->deletarAreaEntidade($data['id_areaconhecimento']);
        $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $data['id_areaconhecimento']);

        return $tm;
    }

    /**
     * Salva Área de Conhecimento
     * @param array $data
     * @return \G2\Entity\AreaConhecimento
     * @Update 2013-10-29 Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function salvarAreaConhecimento(array $data)
    {
        try {

            if (empty($data['entidades'])) {
                return array('title' => 'Atenção!',
                    'text' => 'Selecione pelo menos uma organização!');
            }

            $situacao = $this->getReference('\G2\Entity\Situacao', $data['id_situacao']);
            $entidade = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
            $usuario = $this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
            $tipoareaconhecimento = $this->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);


            if (isset($data['id'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\AreaConhecimento();
            }
            //seta os atributos
            $entity->setSt_areaconhecimento($data['st_areaconhecimento'])
                ->setId_situacao($situacao)
                ->setId_tipoareaconhecimento($tipoareaconhecimento)
                ->setId_usuariocadastro($this->sessao->id_usuario)
                ->setDt_cadastro(new \DateTime())
                ->setSt_tituloexibicao($data['st_tituloexibicao'])
                ->setSt_descricao($data['st_descricao'])
                ->setBl_ativo(1)
                ->setId_entidade($this->sessao->id_entidade)
                ->setId_areaconhecimentopai(null);

            $retorno = $this->save($entity);

            //Vincula com a entidade
            if ($retorno->getId()) {
                $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $retorno->getId());
            }

            return array(
                'id' => $retorno->getId(),
                'type' => 'success',
                'title' => 'Salvo com Sucesso',
                'text' => 'O registro foi salvo com sucesso!'
            );
        } catch (Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao salvar Área Conhecimento: ' . $e->getMessage()
            );
        }
    }

    /**
     *
     * @param integer $id
     * @return object
     * @Update 2013-30-10 Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function deletarArea($id)
    {
        $tm = $this->getReference($this->repositoryName, $id);
        $tm->setBl_ativo(false);
        $this->em->merge($tm);
        $this->em->flush();

        return $tm;
    }

    /**
     * Retorna Árvore de Área de Conhecimento
     * @param array $params
     * @return Ead1_Mensageiro
     */
    public function retornaArvoreAreaConhecimento(array $params = array())
    {
        try {
            $to = new \AreaConhecimentoTO();
            $to->montaToDinamico($params);
            $areaBO = new \AreaBO();
            return $areaBO->retornaArvoreAreaConhecimento($to);
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva a imagem da area  de conhecimento no servidor e no banco
     * @param integer $id
     * @param array $file
     * @return string
     * @throws \Zend_Exception
     */
    public function uploadImagem($id, $file)
    {
        try {
            //busca a area de conhecimento
            $area = $this->find($this->repositoryName, $id);

            $hashImg = md5(uniqid() . $id);
            $fileName = $hashImg . '.jpg';//cria um md5 com o nome da imagem

            $uploadPath = realpath(APPLICATION_PATH . "/../public/upload/areaconhecimento");


            //apaga a imagem antiga do diretorio
            if (!is_null($area->getSt_imagemarea()) && file_exists(realpath(APPLICATION_PATH . "/../public") . $area->getSt_imagemarea())) {
                unlink(realpath(APPLICATION_PATH . "/../public") . $area->getSt_imagemarea());
            }

            //verifica se tem um array com a imagem
            if (is_array($file)) {
                //verifica o tipo da imagem
                if (!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $file["type"])) {
                    throw new \Exception("O formato da imagem enviada não é suportado.");
                }

                if (move_uploaded_file($file['tmp_name'], $uploadPath . '/' . $fileName)) {
                    // Sobrescrevo a imagem adicionada para o tamanho correto
                    $imagine = new Imagine();
                    $imagine->open($uploadPath . '/' . $fileName)
                        ->resize(new Box(640, 480))
                        ->save($uploadPath . '/' . $fileName);

                    chmod($uploadPath . '/' . $fileName, 0777);

                    $st_imagemarea = '/upload/areaconhecimento/' . $fileName;

                    //salva o nome da imagem
                    $area->setSt_imagemarea($st_imagemarea);
                    $this->save($area);

                    return $st_imagemarea;

                } else {
                    throw new \Exception("Houve um erro ao tentar salvar imagem no servidor.");
                }
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar imagem." . $e->getMessage());
        }
    }

}

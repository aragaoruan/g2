<?php
/**
 * Classe de negócio para VwConsultaDisciplina.php
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
namespace G2\Negocio;


class VwConsultaDisciplina extends Negocio
{

    private $repository = '\G2\Entity\VwConsultaDisciplina';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Parâmetros para Pesquisa. O projeto pedagógico é obrigatório
     * @param null $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaResultadoPesquisa($params = null)
    {
        try {

            if (!($params['id_projetopedagogico'])) {
                throw new \Exception("Voce precisa selecionar ao menos um Projeto Pedagógico!");
            }

            if($params['dt_inicial_cadastro_conteudo'] || $params['dt_termino_cadastro_conteudo']){
                $dt_inicial_cadastro_conteudo = ($params['dt_termino_cadastro_conteudo']) ? $this->converteDataBanco($params['dt_termino_cadastro_conteudo'] . ' 00:00:00') : '';
                $dt_termino_cadastro_conteudo = ($params['dt_termino_cadastro_conteudo']) ? $this->converteDataBanco($params['dt_termino_cadastro_conteudo'] . ' 23:59:59') : '';

                $params['dt_inicial_cadastro_conteudo'] = $dt_inicial_cadastro_conteudo;
                $params['dt_termino_cadastro_conteudo'] = $dt_termino_cadastro_conteudo;

            }

            //PARAMETROS OBRIGATÓRIOS
//            $params['id_entidadepai'] = $this->sessao->id_entidade;

            $resultado = $this->em->getRepository($this->repository)->pesquisaResultadoConsultaDisciplina($params);

            foreach ($resultado as $key=>$value){
                $resultado[$key]['dt_cadastro'] = $value['dt_cadastro'] ? $value['dt_cadastro']->format('d/m/Y H:i:s') : "-";
            }

            return $resultado;
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception("Erro ao Pesquisar." . $e->getMessage());
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }

    }


} 
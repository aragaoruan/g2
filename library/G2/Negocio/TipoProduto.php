<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Tipo de Produto
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class TipoProduto extends Negocio
{

    private $repositoryName = 'G2\Entity\TipoProduto';

    public function __construct ()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = NULL) {
        $result = parent::findBy($this->repositoryName, array(), array('st_tipoproduto' => 'ASC'));
       if($result){
                $arrReturn = array();
                foreach ($result as $row) {
                    $arrReturn[]    = $this->toArrayEntity($row);
                }
           
           return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
       } else {
           return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
       }
    }
 

}
<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Cartão
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class Cartao extends Negocio
{

    private $repositoryName = 'G2\Entity\VwEntidadeCartao';
    private $entidadeBo;

    public function __construct()
    {
        parent::__construct();
    }

    public function getReference($idEntidade, $repositoryName = NULL)
    {
        return parent::getReference($this->repositoryName, $idEntidade);
    }

    /**
     * Find Entidade
     * @param array $params
     */
    public function findByVwEntidadeCartao(array $params)
    {
        return $this->findBy($this->repositoryName, $params);
    }

    public function deletarCartaoEntidade(array $params) {



//        return $this->delete()
    }

    public function retornaBandeirasCartao(array $where){
        try {

            $query = $this->em->getRepository('G2\Entity\VwBandeiraCartao')->createQueryBuilder('vw')->select('DISTINCT vw');
            foreach($where as $key => $value){
                $query->andWhere((strstr($key, 'st_') ? "vw.{$key} like :{$key}" : "vw.{$key} = :{$key}"))->setParameter($key,(strstr($key, 'st_') ? '%'.$value.'%' : $value));
            }
            return $query->getQuery()->getResult();

        } catch (\Zend_Exception $e) {
            throw $e;
        }
    }
}
<?php

namespace G2\Negocio;

use G2\Entity\EntidadeIntegracao;

/**
 * Classe de negócio para Integração
 * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
 */

class IntegracaoNegocio extends Negocio{

    private $repositorioIntegracao = '\G2\Entity\EntidadeIntegracao';
    private $integracaoBO;

    public function __construct(){

        parent::__construct();
        $this->integracaoBO = new \EntidadeBO();
    }

    public function salvarDadosMoodle(array $array){

        try
        {
            if (array_key_exists('id_entidadeintegracao', $array) && $array['id_entidadeintegracao']){
               $entidade = $this->find('\G2\Entity\EntidadeIntegracao',(int)$array['id_entidadeintegracao']);
            } else {
                $entidade = new \G2\Entity\EntidadeIntegracao();
            }

            $entidade->setId_entidade($this->sessao->id_entidade);
            $entidade->setId_sistema(\SistemaTO::MOODLE);
            $entidade->setId_usuariocadastro($entidade->getId_usuariocadastro() ? $entidade->getId_usuariocadastro() : $this->sessao->id_usuario);
            $entidade->setDt_cadastro($entidade->getDt_cadastro() ? $entidade-> getDt_cadastro() : new \DateTime());
            $entidade->setSt_codchave($array['st_codchave']);
            $entidade->setSt_codsistema($array['st_codsistema']);
            $entidade->setSt_codarea($array['st_codarea']);
            $entidade->setNu_perfilsuporte((int)$array['nu_perfilsuporte']);
            $entidade->setNu_perfilprojeto((int)$array['nu_perfilprojeto']);
            $entidade->setNu_perfildisciplina((int)$array['nu_perfildisciplina']);
            $entidade->setNu_perfilobservador((int)$array['nu_perfilobservador']);
            $entidade->setNu_perfilalunoobs((int)$array['nu_perfilalunoobs']);
            $entidade->setNu_perfilalunoencerrado((int)$array['nu_perfilalunoencerrado']);
            $entidade->setNu_contexto((int)$array['nu_contexto']);
            $entidade->setSt_codchavewsuny(($array['st_codchavewsuny'] ? $array['st_codchavewsuny'] : null));
            $entidade->setSt_titulo($array['st_titulo']);
            $entidade->setid_situacao($this->getReference('\G2\Entity\Situacao', $array['id_situacao']));
            $entidade->setbl_ativo($array['bl_ativo']);

            $return = $this->save($entidade);
            $entityArray = $this->toArrayEntity($return);

            $mensageiro = new \Ead1_Mensageiro($entityArray, \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        }catch (\Zend_Exception $e){
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);

            return $mensageiro ;
        }
    }


    /**
     * Retorna array com todos os Moodles cadastrados numa entidade.
     * @update Marcus Pereira <marcus.pereira@unyleya.com.br> (Out, 2017)
     * @return array
     */
    public function buscaDadosMoodle() {

        $params = array();

        $params['id_sistema'] = \G2\Constante\Sistema::MOODLE;
        $params['id_entidade'] = $this->sessao->id_entidade;

        $moodles = $this->findBy($this->repositorioIntegracao, $params);
        $arrReturnMoodles = array();

        if (!empty($moodles)) {
            foreach ($moodles as $moodle) {
                $moodleArray = $this->toArrayEntity($moodle);
                $moodleArray['id_situacao'] = $moodle->getId_situacao()->getId_situacao();
                array_push($arrReturnMoodles, $moodleArray);
            }
        }
        return $arrReturnMoodles;
    }

    /**
     * Sincroniza Salas e Perfis no moodle
     * @return array
     */
    public function acaoSincronizar($id_entidadeintegracao = null)
    {
        $bo = new \MoodleBO();
        $return = $bo->sincronizarMoodleIntegracao($id_entidadeintegracao);

        if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO) {

            $html = '<table class="table table-bordered backgrid table-hover  table-striped"><thead>';
            $htmlPerfil = '<table class="table table-bordered backgrid  table-hover  table-striped"><thead>';


            $arrayGeral = $return->getMensagem();

            if (is_array($arrayGeral) && isset($arrayGeral['salas'])) {
                if (isset($arrayGeral['salas']['erros'])) {
                    $html .='<tr><th colspan="3">Integrações de SALA com Erros</th></tr>';
                    $html .='<tr><th>Nome</th><th>Sala</th><th>Erro</th></tr>';
                    foreach ($arrayGeral['salas']['erros'] as $row) {

                            $html .='<tr><td>'.$row['nome'].'</td>';
                            $html .='<td>'.$row['sala'].'</td>';
                            $html .='<td>'.$row['erro'].'</td></tr>';
                    }
                }
                if (isset($arrayGeral['salas']['sucessos'])) {
                    $html .='<tr><th colspan="3">Integrações de SALA com Sucesso</th></tr>';
                    $html .='<tr><th>Nome</th><th colspan="2">Sala</th></tr></thead><tbody>';

                    foreach ($arrayGeral['salas']['sucessos'] as $row) {
                        $html .='<tr><td>'.$row['nome'].'</td>';
                        $html .='<td colspan="2">'.$row['sala'].'</td></tr>';
                    }
                }
            }


            if (is_array($arrayGeral) && isset($arrayGeral['perfis'])) {
                if (isset($arrayGeral['perfis']['erros'])) {
                    $htmlPerfil .='<tr><th colspan="2">Integrações de PERFIL com Erros</th></tr>';
                    $htmlPerfil .='<tr><th>Nome</th><th>Erro</th></tr>';
                    foreach ($arrayGeral['perfis']['erros'] as $linha) {
                        $htmlPerfil .='<tr><td>'.$linha['nome'].'</td>';
                        $htmlPerfil .='<td>'.$linha['erro'].'</td></tr>';
                    }
                }
                if (isset($arrayGeral['perfis']['sucessos'])) {
                    $htmlPerfil .='<tr><th colspan="2">Integrações de PERFIL com Sucesso</th></tr></thead><tbody>';
                    foreach ($arrayGeral['perfis']['sucessos'] as $linha) {
                        $htmlPerfil .='<tr><td colspan="2">'.$linha['nome'].'</td></tr>';

                    }
                }
            }

            $html .= '</tbody></table>';
            $htmlPerfil .= '</tbody></table>';
            return $html.'<br/><br/>'.$htmlPerfil;

        } else {
            if ($return->getTipo() == \Ead1_IMensageiro::AVISO) {
                return '<h4>'.$return->getFirstMensagem().'</h4>';
            }else{
                return '<label class="label label-warning">Não foi possível realizar a sincronização, tente novamente!</label> ';
            }
        }
    }


    /**
     * Sincroniza Alunos no moodle
     * @return array
     */
    public function acaoSincronizarAlunos()
    {

        $bo = new \MoodleBO();
        $return = $bo->sincronizacaoAlunosMoodle();
        if ($return->getTipo() == \Ead1_IMensageiro::SUCESSO) {

            $html = '<table class="table table-bordered backgrid table-hover  table-striped"><thead>';


            $arrayGeral = $return->getMensagem();

            if (is_array($arrayGeral)) {
                if (isset($arrayGeral['erros'])) {
                    $html .='<tr><th colspan="3">Integrações de Alunos com Erros</th></tr>';
                    $html .='<tr><th>Nome</th><th>Sala</th><th>Erro</th></tr>';
                    foreach ($arrayGeral['erros'] as $row) {

                        $html .='<tr><td>'.$row['nome'].'</td>';
                        $html .='<td>'.$row['sala'].'</td>';
                        $html .='<td>'.$row['erro'].'</td></tr>';
                    }
                }
                if (isset($arrayGeral['sucessos'])) {
                    $html .='<tr><th colspan="3">Integrações de Alunos com Sucesso</th></tr>';
                    $html .='<tr><th>Nome</th><th colspan="2">Sala</th></tr></thead><tbody>';

                    foreach ($arrayGeral['sucessos'] as $row) {
                        $html .='<tr><td>'.$row['nome'].'</td>';
                        $html .='<td colspan="2">'.$row['sala'].'</td></tr>';
                    }
                }
            }

            $html .= '</tbody></table>';

            return $html;

        } else {
            if ($return->getTipo() == \Ead1_IMensageiro::AVISO) {
                return '<h4>'.$return->getFirstMensagem().'</h4>';
            }else{
                return '<label class="label label-warning">Não foi possível realizar a sincronização de alunos, tente novamente!</label>';
            }
        }
    }

    /**
     * Busca uma entidade integração e retorna um array
     * @param array $params
     * @return mixed
     */
    public function buscaEntidadeIntegracaoTotvs(){

        $params = array();

        $params['id_sistema'] = '3';

        $params['id_entidade'] = $this->sessao->id_entidade;

        $result = $this->findOneBy('G2\Entity\EntidadeIntegracao', $params);
        if($result instanceof \G2\Entity\EntidadeIntegracao){
            return $this->toArrayEntity($result);
        }else{
            return array();
        }


    }

    /**
     * Função responsável por salvar todos os dados do Totvs
     * @param array $dadosFinanceiros
     * @param array $meioPagamento
     */
    public function salvarDadosCompletosTotvs(array $dadosFinanceiros, array $meioPagamento){

        try{

            $this->beginTransaction();

                $dadosFinanceiro = $this->salvarDadosFinanceirosTotvs($dadosFinanceiros);

                $this->salvarMeioPagamentoIntegracao($meioPagamento);

            $this->commit();

            //return true;
            $entityArray = $this->toArrayEntity($dadosFinanceiro);
            $mensageiro = new \Ead1_Mensageiro($entityArray, \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;

        }catch (\Zend_Exception $e){

            $this->rollback();
            return $e->getMessage();
        }
    }

    /**
     * Função responsável por salvar os dados financeiros do Totvs
     * @param array $dadosFinanceiros
     * @return \Ead1_Mensageiro
     */
    public function salvarDadosFinanceirosTotvs(array $dadosFinanceiros){

        try{

            $entidade = new \G2\Entity\EntidadeIntegracao();

            if (!empty($dadosFinanceiros['id_entidadeintegracao'])) {
                $colecao = $this->find('\G2\Entity\EntidadeIntegracao', $dadosFinanceiros['id_entidadeintegracao']);

                if($colecao){
                    $entidade = $colecao;
                }
            }

            $entidade->setId_entidadeintegracao($dadosFinanceiros['id_entidadeintegracao']);
            $entidade->setId_usuariocadastro($entidade->getId_usuariocadastro() ? $entidade->getId_usuariocadastro() : $this->sessao->id_usuario);
            $entidade->setDt_cadastro($entidade->getDt_cadastro() ? $entidade-> getDt_cadastro() : new \DateTime());
            $entidade->setId_sistema(\SistemaTO::FLUXUS);
            $entidade->setSt_codsistema($dadosFinanceiros['st_codsistema']);
            $entidade->setSt_codchave($dadosFinanceiros['st_codchave']);
            $entidade->setId_entidade($entidade->getId_entidade() ? $entidade->getId_entidade() : $this->sessao->id_entidade);


            $return = $this->save($entidade);

            if(!$return){
                throw new \Exception("Erro ao salvar os dados - Dados Financeiros.");
            }

        }catch (\Zend_Exception $e){

            return $e->getMessage();
        }
        return $entidade;
        //return true;
    }

    /**
     * Função responsável por armazenar todos os meios de pagamentos do Totvs
     */
    public function salvarMeioPagamentoIntegracao(array $meioPagamento){

        try{
           // var_dump($meioPagamento); exit;
            $return = array();

            foreach ($meioPagamento as $key => $array){

                $entidade = new \G2\Entity\MeioPagamentoIntegracao();

                if(isset($array['id_meiopagamentointegracao'])&&($array['id_meiopagamentointegracao']!=0)){
                    $colecao = $this->find('\G2\Entity\MeioPagamentoIntegracao', $array['id_meiopagamentointegracao']);

                    if($colecao){
                        $entidade = $colecao;
                    }
                }else{
                    $entidade->setId_meiopagamentointegracao(null);

                }
                $entidade->setId_sistema(\SistemaTO::FLUXUS);
                $entidade->setId_entidade($this->sessao->id_entidade);
                $entidade->setId_meiopagamento($array['id_meiopagamento']);
                $entidade->setId_usuariocadastro($entidade->getId_usuariocadastro() ? $entidade->getId_usuariocadastro() : $this->sessao->id_usuario);
                $entidade->setSt_codsistema($array['st_codsistema']);
                $entidade->setDt_cadastro($entidade->getDt_cadastro() ? $entidade-> getDt_cadastro() : new \DateTime());
                $entidade->setSt_codcontacaixa($array['st_codcontacaixa']);

                if(isset($array['id_cartaobandeira']) && !empty($array['id_cartaobandeira'])){
                    $entidade->setId_cartaobandeira($array['id_cartaobandeira']);
                }

                if($entidade->getId_meiopagamento() == 2){
                 //   \Zend_Debug::dump($entidade);die;
                }
                $result = $this->save($entidade);

                if(!$result){
                    throw new \Exception("Erro ao salvar os dados de meio de pagamento.");
                }

                $return[] = $result;
            }

            return true;

        }catch (\Zend_Exception $e){

            return $e->getMessage();
        }

    }

}

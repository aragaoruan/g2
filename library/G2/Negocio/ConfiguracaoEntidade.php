<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Configuração Entidade
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 */
class ConfiguracaoEntidade extends Negocio
{

    private $repositoryName = '\G2\Entity\ConfiguracaoEntidade';

    public function __construct()
    {
        parent::__construct();
    }

    public function getReference($id, $repositoryName = NULL)
    {
        return parent::getReference($this->repositoryName, $id);
    }


    /**
     * Seta os valores padrões no array dos parametros da configuração da entidade
     * @param array $params
     * @return array
     */
    private function setarValoresDefaultConfiguracao(array $params)
    {
        if (!array_key_exists('id_modelogradenotas', $params) || empty($params['id_modelogradenotas'])) {
            $params['id_modelogradenotas'] = \G2\Constante\ModeloGradeNotas::PADRAO;
        }
        if (!array_key_exists('nu_maxextensao', $params) || empty($params['nu_maxextensao'])) {
            $params['nu_maxextensao'] = 0;
        }
        if (!array_key_exists('nu_encerramentoprofessor', $params) || empty($params['nu_encerramentoprofessor'])) {
            $params['nu_encerramentoprofessor'] = 0;
        }
        if (!array_key_exists('bl_encerrasemacesso', $params) || empty($params['bl_encerrasemacesso'])) {
            $params['bl_encerrasemacesso'] = false;
        }

        if (!array_key_exists('bl_tutorial', $params) || empty($params['bl_tutorial'])) {
            $params['bl_tutorial'] = false;
        }

        if (!array_key_exists('bl_msgagendamentoportal', $params) || empty($params['bl_msgagendamentoportal'])) {
            $params['bl_msgagendamentoportal'] = false;
        }

        if (!array_key_exists('st_codgoogleanalytics', $params) || empty($params['st_codgoogleanalytics'])) {
            $params['st_codgoogleanalytics'] = null;
        }

        if (!array_key_exists('st_googletagmanager', $params) || empty($params['st_googletagmanager'])) {
            $params['st_googletagmanager'] = null;
        }

        if (!array_key_exists('bl_acessoapp', $params) || empty($params['bl_acessoapp'])) {
            $params['bl_acessoapp'] = false;
        }
        return $params;
    }

    /**
     * Salva configurações da entidade
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function processarConfiguracaoEntidade(array $params)
    {
        try {

            $config = new \G2\Entity\ConfiguracaoEntidade();
            if (!empty($params['id_configuracaoentidade'])) {
                $config = $this->find('G2\Entity\ConfiguracaoEntidade', $params['id_configuracaoentidade']);

                if (!$config) {
                    throw new \Exception("Configuração de entidade não encontrada.");
                }
            }

            $params = $this->setarValoresDefaultConfiguracao($params);

            $config->setId_entidade($params['id_entidade'])
                ->setBl_bloqueiadisciplina($params['bl_bloqueiadisciplina'])
                ->setBl_professordisciplina($params['bl_professordisciplina'])
                ->setSt_corentidade($params['st_corentidade'])
                ->setBl_acessoapp($params['bl_acessoapp'])
                ->setId_modelogradenotas($params['id_modelogradenotas'])
                ->setNu_maxextensao($params['nu_maxextensao'])
                ->setNu_encerramentoprofessor($params['nu_encerramentoprofessor'])
                ->setBl_encerrasemacesso($params['bl_encerrasemacesso'])
                ->setBl_tutorial($params['bl_tutorial'])
                ->setBl_msgagendamentoportal($params['bl_msgagendamentoportal'])
                ->setSt_googletagmanager($params['st_googletagmanager'])
                ->setSt_codgoogleanalytics($params['st_codgoogleanalytics'])
                ->setBl_minhasprovas($params['bl_minhasprovas']);

            $result = $this->save($config);

            if ($result) {
                return new \Ead1_Mensageiro($result->toBackbonearray(), \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro("Não foi possível salvar a Configuração", \Ead1_IMensageiro::ERRO);
            }

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param $id_entidade
     * @param bool $return_array
     * @return array|null|object
     * @throws \Zend_Exception
     */
    public function findConfiguracaoEntidade($id_entidade, $return_array = true)
    {
        $result = $this->findOneBy($this->repositoryName, array('id_entidade' => $id_entidade));

        if ($result) {
            if ($return_array) {
                $result = $this->toArrayEntity($result);
            }
        }
        return $result;
    }

    /**
     * @param $arquivo
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarAnexoTabelaPreco($arquivo)
    {
        $arquivo['name'] = 'tabelapreco_' . $this->sessao->id_entidade . '.' . pathinfo($arquivo['name'], PATHINFO_EXTENSION);

        // Cria a pasta da entidade, se nao existir
        if (!is_dir('upload/' . $this->sessao->id_entidade)) {
            mkdir("upload/" . $this->sessao->id_entidade);
        }

        $ng_upload = new \G2\Negocio\Upload();
        $mensageiro = $ng_upload->upload($arquivo, 'upload/' . $this->sessao->id_entidade);

        // Se fazer o upload, alterar no projeto
        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $cf_entidade = $this->findOneBy($this->repositoryName, array('id_entidade' => $this->sessao->id_entidade));
            $cf_entidade->setId_tabelapreco($this->find('\G2\Entity\Upload', $mensageiro->codigo));
            $this->save($cf_entidade);
        }

        return $mensageiro;
    }

    public function salvarManualDoAluno($arquivo)
    {
        if (!isset($arquivo['name'])) {
            return false;
        }

        $mensageiro = new \Ead1_Mensageiro();

        $this->beginTransaction();

        try {


            // Remover whitespaces
            $arquivo['name'] = preg_replace('/\s+/', '_', $arquivo['name']);

            $adapter = new \Zend_File_Transfer_Adapter_Http();
            $adapter->addFilter('Rename', $arquivo['name']);
            $adapter->setDestination('upload' . DIRECTORY_SEPARATOR . 'manual' . DIRECTORY_SEPARATOR);

            if (!$adapter->receive()) {
                $messages = $adapter->getMessages();
                throw new \Exception(implode(" | ", $messages));
            }

            // Fazer o upload
            $upload = new \Zend_File_Transfer();
            $upload->receive();

            // Salvar o upload no banco
            $en_upload = new \G2\Entity\Upload();
            $en_upload->setSt_upload($arquivo['name']);
            $en_upload = $this->save($en_upload);

            if (!$en_upload->getId_upload()) {
                throw new \Exception("Erro ao fazer upload do Manual do Aluno!");
            }

            try {
                // Salvar configuração da entidade
                $en_configuracaoentidade = $this->findOneBy('\G2\Entity\ConfiguracaoEntidade', array('id_entidade' => $this->sessao->id_entidade));
                $en_configuracaoentidade->setId_manualaluno($this->find('\G2\Entity\Upload', $en_upload->getId_upload()));
                $this->save($en_configuracaoentidade);
            } catch (\Exception $e) {
                throw new \Exception("Erro ao editar Configuração da Entidade!");
            }

            $this->commit();

            return $mensageiro->setMensageiro($this->toArrayEntity($en_upload), \Ead1_IMensageiro::SUCESSO, $en_upload->getId_upload());
        } catch (\Exception $e) {
            $this->rollback();

            $mensageiro->setMensageiro(array(
                FILE . ':' . LINE,
                $e->getMessage(),
            ), \Ead1_IMensageiro::ERRO);
            $mensageiro->setText('Erro no Upload do Manual do Aluno: ' . $e->getMessage());
            return $mensageiro;
        }

    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Relatorio de Lancamento Venda Produto (vw_lancamentovendaproduto)
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class VwLancamentoVendaProduto extends Relatorio
{

    private $repositoryName = 'G2\Entity\VwLancamentoVendaProduto';

    /**
     * @see \application\apps\default\controllers\RelatorioLancamentoVendaProdutoController.php::pesquisarAction
     * @param array $params
     * @return array
     */
    public function lancamentoVendaProduto($params)
    {
        $query = $this->em->getRepository($this->repositoryName)
            ->createQueryBuilder('vw');

        /**
         * Configurando parametros PERSONALIZADOS
         */

        //Se tiver o parametro id_entidade
        if (isset($params['id_entidade']) && $params['id_entidade']) {
            $query->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $params['id_entidade']);
        }

        //Se tiver o parametro id_atendente
        if (isset($params['id_atendente']) && $params['id_atendente']) {
            $query->andWhere('vw.id_atendente = :id_atendente')
                ->setParameter('id_atendente', $params['id_atendente']);
        }

        //Se tiver o parametro id_tipoproduto
        if (isset($params['id_tipoproduto']) && $params['id_tipoproduto']) {
            $query
                ->andWhere("vw.id_tipoproduto = :id_tipoproduto")
                ->setParameter('id_tipoproduto', $params['id_tipoproduto']);
        }

        //Se tiver o parametro dt_criacao_min (dt_cadastro)
        if (isset($params['dt_criacao_min']) && $params['dt_criacao_min']) {
            $query
                ->andWhere("vw.dt_cadastro >= :dt_criacao_min")
                ->setParameter('dt_criacao_min', $this->converterData($params['dt_criacao_min'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_criacao_max (dt_cadastro)
        if (isset($params['dt_criacao_max']) && $params['dt_criacao_max']) {
            $query
                ->andWhere("vw.dt_cadastro <= :dt_criacao_max")
                ->setParameter('dt_criacao_max', $this->converterData($params['dt_criacao_max'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_vencimento_min (dt_vencimento)
        if (isset($params['dt_vencimento_min']) && $params['dt_vencimento_min']) {
            $query
                ->andWhere("vw.dt_vencimento >= :dt_vencimento_min")
                ->setParameter('dt_vencimento_min', $this->converterData($params['dt_vencimento_min'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_vencimento_max (dt_vencimento)
        if (isset($params['dt_vencimento_max']) && $params['dt_vencimento_max']) {
            $query
                ->andWhere("vw.dt_vencimento <= :dt_vencimento_max")
                ->setParameter('dt_vencimento_max', $this->converterData($params['dt_vencimento_max'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_baixa_min (dt_quitado)
        if (isset($params['dt_baixa_min']) && $params['dt_baixa_min']) {
            $query
                ->andWhere("vw.dt_baixa >= :dt_baixa_min")
                ->setParameter('dt_baixa_min', $this->converterData($params['dt_baixa_min'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_baixa_max (dt_quitado)
        if (isset($params['dt_baixa_max']) && $params['dt_baixa_max']) {
            $query
                ->andWhere("vw.dt_baixa <= :dt_baixa_max")
                ->setParameter('dt_baixa_max', $this->converterData($params['dt_baixa_max'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_confirmacao_min (dt_confirmacao)
        if (isset($params['dt_confirmacao_min']) && $params['dt_confirmacao_min']) {
            $query
                ->andWhere("vw.dt_confirmacao >= :dt_confirmacao_min")
                ->setParameter('dt_confirmacao_min', $this->converterData($params['dt_confirmacao_min'], 'Y-m-d'));
        }

        //Se tiver o parametro dt_confirmacao_max (dt_confirmacao)
        if (isset($params['dt_confirmacao_max']) && $params['dt_confirmacao_max']) {
            $query
                ->andWhere("vw.dt_confirmacao <= :dt_confirmacao_max")
                ->setParameter('dt_confirmacao_max', $this->converterData($params['dt_confirmacao_max'], 'Y-m-d'));
        }

//        $query->orderBy('vw.st_nomecompleto', 'ASC');

        $ar_return = array();
        $results = $query->getQuery()->getArrayResult();

        if (is_array($results) && $results) {
            foreach ($results as $key => &$result) {

                // Retornar somente os campos necessarios para deixar o retorno MAIS LEVE
                $ar_return[] = array(
                    'st_nomeentidade' => $result['st_nomeentidade'],
                    'st_atendente' => $result["st_atendente"],
                    'st_aluno' => $result["st_aluno"],
                    'st_cpf' => $result["st_cpf"],
                    'st_rg' => $result["st_rg"],
                    'st_email' => $result["st_email"],
                    'st_ddd' => $result["st_ddd"],
                    'st_telefone' => $result["st_telefone"],
                    'st_cep' => $result["st_cep"],
                    'st_endereco' => $result["st_endereco"],
                    'st_bairro' => $result["st_bairro"],
                    'st_complemento' => $result["st_complemento"],
                    'nu_numero' => $result["nu_numero"],
                    'st_cidade' => $result["st_cidade"],
                    'sg_uf' => $result["sg_uf"],
                    'id_venda' => $result["id_venda"],
                    'st_meiopagamento' => $result["st_meiopagamento"],
                    'st_valorliquido' => str_replace('.', ',', $result["nu_valorliquido"]),
                    'st_valorbruto' => str_replace('.', ',', $result["nu_valorbruto"]),
                    'id_aluno' => $result["id_aluno"],
                    'dt_confirmacao' => $result["dt_confirmacao"] ?
                        $this->converterData($result["dt_confirmacao"], 'd/m/Y') : '',
                    'st_produto' => $result["st_produto"],
                    'st_idproduto' => $result["id_produto"],
                    'dt_vencimento' => $result["dt_vencimento"] ?
                        $this->converterData($result["dt_vencimento"], 'd/m/Y') : '',
                    'dt_baixa' => $result["dt_baixa"] ?
                        $this->converterData($result["dt_baixa"], 'd/m/Y') : '',
                    'st_valorlancamento' => str_replace('.', ',', $result["nu_valorlancamento"]),
                    'st_valorlancamentopago' => str_replace('.', ',', $result["nu_valorlancamentopago"]),
                    'dt_cadastro' => $result["dt_cadastro"] ?
                        $this->converterData($result["dt_cadastro"], 'd/m/Y') : '',
                    'st_valorproduto' => str_replace('.', ',', $result["nu_valorproduto"]),
                    'id_lancamento' => $result["id_lancamento"],
                    'st_ordem' => $result["nu_ordem"] . '/' . $result['nu_parcelas']
                );
            }
        }

        return $ar_return;
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negocio para funcionalidade de Mensagens para o Usuário no G2
 * @author Débora Castro <debora.castro@unyleya.com.br>
 * @since 2015-04-22
 */
class MensagemUsuario extends Negocio
{
    private $negocioMensagem;

    public function __construct()
    {
        parent::__construct();
        $this->negocioMensagem = new Mensagem();
    }

    /**
     * Metodo responsavel pela busca de mensagens (internas) do usuário
     * @params id_usuario, id_entidade
     * @throws \Exception $e
     * @return mixed
     */
    public function retonarMensagensUsuario(array $parametros = null)
    {
        try {
            $resultado = array();
            if (!(isset($parametros['id_usuario'])))
                $parametros['id_usuario'] = $this->sessao->id_usuario;

            $parametros['id_tipoenvio'] = \G2\Constante\TipoEnvio::HTML;

            $dados = $this->findBy('\G2\Entity\VwMensagemUsuario', $parametros, (array('id_evolucaodestinatario' => 'ASC', 'id_mensagem' => 'DESC')));
            if ($dados) {
                foreach ($dados as $row) {
                    $resultado[] = $this->toArrayEntity($row);
                }
            }
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $resultado;
    }

    public function atualizarMensagemUsuario($data)
    {
        try {
            $mensagem = $this->find('\G2\Entity\EnvioDestinatario', (int)$data['id_enviodestinatario']);

            $mensagem->setId_evolucao($data['id_evolucao']);
            $result = $this->merge($mensagem);

            if(isset($data['atualizar_mensagem'])) {
                unset($data['id_usuariocadastro'], $data['id_entidade'], $data['id_situacao']);
                $this->negocioMensagem->atualizarMensagem($data);
            }

            if ($result) {
                return true;
            } else {
                return false;
            }

        } catch (\Zend_Exception $e) {
            return false;
        }
    }


}

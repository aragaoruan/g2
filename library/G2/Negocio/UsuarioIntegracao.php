<?php

namespace G2\Negocio;

use G2\Constante\Sistema;

/**
 * Classe de negócio para Usuario Integração
 * @author Neemias Santos <neemias.santos@unyleya.com.br>;
 */
class UsuarioIntegracao extends Negocio
{

    /** @var \G2\Entity\UsuarioIntegracao string */
    private $repositoryName = \G2\Entity\UsuarioIntegracao::class;


    /**
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarIntegracao(array $data)
    {
        try {
            if ($data) {
                if (!empty($data['id_sistema']) && !empty($data['id_usuario']) && !empty($data['id_entidade'])) {

                    $params = array(
                        "id_sistema" => $data["id_sistema"],
                        "id_usuario" => $data["id_usuario"],
                        "id_entidade" => $data["id_entidade"],
                    );

                    if ($data["id_sistema"] === Sistema::MOODLE && !empty($data["id_entidadeintegracao"])) {
                        $params["id_entidadeintegracao"] = $data["id_entidadeintegracao"];
                    }

                    /** @var  \G2\Entity\UsuarioIntegracao $entity */
                    $entity = $this->findOneBy($this->repositoryName, $params);
                }

                if (!$entity) {
                    $entity = new $this->repositoryName; //Get instance entity
                }

                //Set data on entity
                $entity->setId_usuariocadastro($data['id_usuariocadastro']);
                $entity->setId_usuario($data['id_usuario']);
                $entity->setSt_codusuario($data['st_codusuario']);
                $entity->setBl_encerrado($data['bl_encerrado']);
                $entity->setId_sistema($data['id_sistema']);
                $entity->setDt_cadastro($data['dt_cadastro']);
                $entity->setId_entidade($data['id_entidade']);

                //verifica se foi passado o id_entidadeintegracao
                if (isset($data["id_entidadeintegracao"]) && !empty($data["id_entidadeintegracao"])) {
                    $entity->setId_entidadeintegracao($data["id_entidadeintegracao"]);
                }

                if (isset($data['st_senhaintegrada'])) {
                    $entity->setSt_senhaintegrada($data['st_senhaintegrada']);
                }

                if (isset($data['st_loginintegrado'])) {
                    $entity->setSt_loginintegrado($data['st_loginintegrado']);
                }
                return $this->save($entity);
            }
        } catch (\Zend_Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna Usuario Integração pelo id_sistema
     * @param array $params
     * @return null|object
     */
    public function findUsuarioIntegracao(array $params)
    {
        return $this->findOneBy($this->repositoryName, $params);
    }
}

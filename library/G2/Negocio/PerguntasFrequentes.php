<?php

namespace G2\Negocio;

/**
 * Classe para gerenciar ações relacionadas ao modulo de Perguntas Frequentes
 * Class PerguntasFrequentes
 * @package G2\Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class PerguntasFrequentes extends Negocio
{
    /**
     * @var string
     */
    private $repositoryName = 'G2\Entity\EsquemaPergunta';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna esquema por id
     * @param $id
     * @return array
     */
    public function findEsquema($id, $toArray = false)
    {
        $result = $this->find($this->repositoryName, $id);
        if ($toArray && $result) {
            $result = array(
                'id_entidadecadastro' => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : NULL,
                'id_esquemapergunta' => $result->getId_esquemapergunta(),
                'st_esquemapergunta' => $result->getSt_esquemapergunta(),
                'dt_cadastro' => $result->getDt_cadastro(),
                'bl_ativo' => $result->getBl_ativo()
            );
        }
        return $result;
    }

    /**
     * Método para retornar esquemas
     * @param array $params
     * @param bool|false $toArray
     * @return Object
     */
    public function retornarEsquemas(array $params = array(), $toArray = false)
    {
        $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
        $result = $this->findBy($this->repositoryName, $params, array('st_esquemapergunta' => 'asc'));
        if ($toArray & $result) {
            foreach ($result as $i => $row) {
                $result[$i] = array(
                    'id_entidadecadastro' => $row->getId_entidadecadastro() ? $row->getId_entidadecadastro()->getId_entidade() : NULL,
                    'id_esquemapergunta' => $row->getId_esquemapergunta(),
                    'st_esquemapergunta' => $row->getSt_esquemapergunta(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                    'bl_ativo' => $row->getBl_ativo()
                );
            }
        }
        return $result;
    }

    /**
     * Método para retornar as categorias
     * @param array $params
     * @param bool|false $toArray
     * @return Object
     */
    public function retornarCategorias(array $params, $toArray = false)
    {
        $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
        $result = $this->findBy('\G2\Entity\CategoriaPergunta', $params);
        if ($toArray & $result) {
            foreach ($result as $i => $row) {
                $result[$i] = array(
                    "id_categoriapergunta" => $row->getId_categoriapergunta(),
                    "st_categoriapergunta" => $row->getSt_categoriapergunta(),
                    "bl_ativo" => $row->getBl_ativo(),
                    "dt_cadastro" => $row->getDt_cadastro(),
                    "id_esquemapergunta" => $row->getId_esquemapergunta() ? $row->getId_esquemapergunta()->getId_esquemapergunta() : null,
                    "id_entidadecadastro" => $row->getId_entidadecadastro() ? $row->getId_entidadecadastro()->getId_entidade() : null,
                    'hashId' => $row->getId_categoriapergunta(),
                    'isNew' => 0
                );
            }
        }
        return $result;
    }


    public function retornarPerguntas(array $params, $toArray = false)
    {
        $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
        $result = $this->findBy('\G2\Entity\Pergunta', $params);
        if ($toArray & $result) {
            foreach ($result as $i => $row) {
                $categoria = $row->getId_categoriapergunta() ? $row->getId_categoriapergunta()->getId_categoriapergunta() : null;
                $result[$i] = array(
                    "id_pergunta" => $row->getId_pergunta(),
                    "st_pergunta" => $row->getSt_pergunta(),
                    "st_resposta" => $row->getSt_resposta(),
                    "bl_ativo" => $row->getBl_ativo(),
                    "dt_cadastro" => $row->getDt_cadastro(),
                    "id_categoriapergunta" => $categoria,
                    "id_entidadecadastro" => $row->getId_entidadecadastro() ? $row->getId_entidadecadastro()->getId_entidade() : null,
                    "hashId" => $categoria,
                    "hasNew" => 0
                );
            }
        }
        return $result;
    }

    /**
     * Retorna os dados da pesquisa
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarPesquisaPerguntasFrequentes(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {

            $arrColumns = array();
            if (isset($params['grid'])) {
                //Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
                $arrColumns = $this->getColumnsFromPesquisaDinamica($params['grid'], "es");
            }
            unset($params['to'], $params['txtSearch'], $params['grid']);

            $result = $this->em->getRepository($this->repositoryName)
                ->retornarPesquisa($arrColumns, $params, $orderBy, $limit, $offset);

            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao pesquisar dados da pesquisa. " . $e->getMessage());
        }
    }

    /**
     * Recupera o parametro da grid e monta um array com as colunas que vão ser retornadas
     * @param string $prefix
     * @param json string $grid
     * @return array
     */
    private function getColumnsFromPesquisaDinamica($grid, $prefix = '')
    {
        $grids = json_decode($grid);
        $arrGrid = array();
        $prefix = (!empty($prefix) ? $prefix . "." : null);
        if (is_object($grids)) {
            foreach ($grids->grid as $key => $grid) {
                array_push($arrGrid, $prefix . $grid->name);
            }
        }
        return $arrGrid;
    }

    /**
     * Método para salvar os dados do esqeuma de pergunta
     * @param array $data
     * @return array|\G2\Entity\EsquemaPergunta|mixed
     * @throws \Zend_Exception
     */
    public function salvarEsquema(array $data)
    {
        try {
//            \Zend_Debug::dump($data);exit;
            //intancia a entity
            $entity = new \G2\Entity\EsquemaPergunta();

            //verifica se tem o valor na posição que é a chave primaria, isso é uma edição
            if (array_key_exists('id_esquemapergunta', $data) && !empty($data['id_esquemapergunta'])) {
                //procura os dados no banco
                $result = $this->find($this->repositoryName, $data['id_esquemapergunta']);
                //verifica se retornou resultado
                if ($result) {
                    $entity = $result;//atribui o resultado a variavel da entity
                }
                unset($result);
            } else {
                //se entrar no else, significa que é um insert, então seta o valor da data e hora de cadastro
                $entity->setDt_cadastro(new \DateTime());
            }
            //seta o valor que é o nome do esquema
            $entity->setSt_esquemapergunta(isset($data['st_esquemapergunta']) ? $data['st_esquemapergunta'] : $entity->getSt_esquemapergunta());

            //verifica se foi passado uma posição com bl_ativo ou se ela não é vazia
            if (array_key_exists('bl_ativo', $data)) {
                //seta o valor que foi passado
                $entity->setBl_ativo($data['bl_ativo']);
            } else {
                //se entrar verifica se é pra pegar o valor default que já esta salvo ou seta o valor como true
                $entity->setBl_ativo($entity->getBl_ativo() ? $entity->getBl_ativo() : true);
            }
            //verifica se foi passado o id da entidade de cadastro
            if (!isset($data['id_entidadecadastro']) || empty($data['id_entidadecadastro'])) {
                //verifica se NÃO tem registro salvo
                if (!$entity->getId_entidadecadastro()) {
                    //se não tiver seta o valor da sessao
                    $entidade = $this->getReference('\G2\Entity\Entidade', $this->getId_entidade());
                    $entity->setId_entidadeCadastro($entidade);
                }
            } else {
                //se for passado o valor seta a referencia
                $entity->setId_entidadeCadastro($this->getReference('\G2\Entity\Entidade', $data['id_entidadecadastro']));
            }

            $entity = $this->save($entity);
            return $entity;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados do Esquema de Perguntas. " . $e->getMessage());
        }
    }

    /**
     * Método para salvar um array com varias categorias
     * @param array $categorias
     * @return array
     * @throws \Zend_Exception
     */
    public function salvarArrayCategorias(array $categorias)
    {
        try {

            //abre a transação
            $this->beginTransaction();
            $arrCategorias = array();//cria um array para retorno vazio

            //verifica se tem categorias para salvar
            if ($categorias) {
                //percorre o array com as categorias
                foreach ($categorias as $categoria) {
                    //chama o método de salvar a categoria
                    $result = $this->salvarCategoriaPergunta($categoria);
                    //verifica se retornou
                    if ($result) {
                        //atribui no array na respectiva posição os valores salvo
                        $arrCategorias[] = array(
                            "id_categoriapergunta" => $result->getId_categoriapergunta(),
                            "st_categoriapergunta" => $result->getSt_categoriapergunta(),
                            "bl_ativo" => $result->getBl_ativo(),
                            "dt_cadastro" => $result->getDt_cadastro(),
                            "id_esquemapergunta" => $result->getId_esquemapergunta() ? $result->getId_esquemapergunta()->getId_esquemapergunta() : null,
                            "id_entidadecadastro" => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : null,
                            'hashId' => isset($categoria['hashId']) ? $categoria['hashId'] : $result->getId_categoriapergunta(),
                            'isActive' => isset($categoria['isActive']) ? $categoria['isActive'] : 0,
                            'isNew' => 0
                        );
                    }
                }

            }

            //fecha a transação
            $this->commit();
            //retorna o array
            return $arrCategorias;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar array de categorias." . $e->getMessage());
        }
    }

    /**
     * Método para salvar categoria da pergunta
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarCategoriaPergunta(array $data)
    {
        try {
            $entity = new \G2\Entity\CategoriaPergunta();

            //verifica se tem o valor na posição que é a chave primaria, isso é uma edição
            if (array_key_exists('id_categoriapergunta', $data) && !empty($data['id_categoriapergunta'])) {
                //procura os dados no banco
                $result = $this->find('\G2\Entity\CategoriaPergunta', $data['id_categoriapergunta']);
                //verifica se retornou resultado
                if ($result) {
                    $entity = $result;//atribui o resultado a variavel da entity
                }
                unset($result);
            } else {
                //se entrar no else, significa que é um insert, então seta o valor da data e hora de cadastro
                $entity->setDt_cadastro(new \DateTime());
            }

            $entity->setSt_categoriapergunta($data['st_categoriapergunta']);

            //seta o valor para esquema de pergunta
            if (isset($data['id_esquemapergunta']) && !empty($data['id_esquemapergunta'])) {
                $entity->setId_esquemapergunta($this->getReference($this->repositoryName, $data['id_esquemapergunta']));
            }

            //verifica se foi passado uma posição com bl_ativo ou se ela é vazia
            if (!isset($data['bl_ativo']) || empty($data['bl_ativo'])) {
                //se entrar verifica se é pra pegar o valor default que já esta salvo ou seta o valor como true
                $entity->setBl_ativo($entity->getBl_ativo() ? $entity->getBl_ativo() : true);
            } else {
                //seta o valor que foi passado
                $entity->setBl_ativo($data['bl_ativo']);
            }

            //verifica se foi passado o id da entidade de cadastro
            if (!isset($data['id_entidadecadastro']) || empty($data['id_entidadecadastro'])) {
                //verifica se NÃO tem registro salvo
                if (!$entity->getId_entidadecadastro()) {
                    //se não tiver seta o valor da sessao
                    $entidade = $this->getReference('\G2\Entity\Entidade', $this->getId_entidade());
                    $entity->setId_entidadeCadastro($entidade);
                }
            } else {
                //se for passado o valor seta a referencia
                $entity->setId_entidadeCadastro($this->getReference('\G2\Entity\Entidade', $data['id_entidadecadastro']));
            }

            return $this->save($entity);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados da Categoria de Perguntas. " . $e->getMessage());
        }
    }


    public function salvarArrayPerguntas(array $perguntas)
    {
        try {
            //abre a transação
            $this->beginTransaction();
            $arrPerguntas = array();//cria um array para retorno vazio

            //verifica se tem categorias para salvar
            if ($perguntas) {
                //percorre o array com as categorias
                foreach ($perguntas as $pergunta) {
                    $result = $this->salvarPergunta($pergunta);
                    if ($result) {
                        $arrPerguntas[] = array(
                            "id_pergunta" => $result->getId_pergunta(),
                            "st_pergunta" => $result->getSt_pergunta(),
                            "st_resposta" => $result->getSt_resposta(),
                            "bl_ativo" => $result->getBl_ativo(),
                            "dt_cadastro" => $result->getDt_cadastro(),
                            "id_categoriapergunta" => $result->getId_categoriapergunta() ? $result->getId_categoriapergunta()->getId_categoriapergunta() : null,
                            "id_entidadecadastro" => $result->getId_entidadecadastro() ? $result->getId_entidadecadastro()->getId_entidade() : null,
                            "hashId" => $pergunta['hashId'],
                            "hasNew" => 0
                        );
                    }
                }
            }

            //fecha a transação
            $this->commit();
            //retorna o array
            return $arrPerguntas;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar perguntas. " . $e->getMessage());
        }
    }

    /**
     * Método para salvar pergunta
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarPergunta(array $data)
    {
        try {
            $entity = new \G2\Entity\Pergunta();

            //verifica se tem o valor na posição que é a chave primaria, isso é uma edição
            if (array_key_exists('id_pergunta', $data) && !empty($data['id_pergunta'])) {
                //procura os dados no banco
                $result = $this->find('\G2\Entity\Pergunta', $data['id_pergunta']);
                //verifica se retornou resultado
                if ($result) {
                    $entity = $result;//atribui o resultado a variavel da entity
                }
                unset($result);
            } else {
                //se entrar no else, significa que é um insert, então seta o valor da data e hora de cadastro
                $entity->setDt_cadastro(new \DateTime());
            }

            $entity->setSt_pergunta(isset($data['st_pergunta']) ? $data['st_pergunta'] : $entity->getSt_pergunta())
                ->setSt_resposta(isset($data['st_resposta']) ? $data['st_resposta'] : $entity->getSt_resposta());


            //seta o valor para a categoria da pergunta
            if (isset($data['id_categoriapergunta']) && !empty($data['id_categoriapergunta'])) {
                $entity->setId_categoriapergunta($this->getReference('\G2\Entity\CategoriaPergunta', $data['id_categoriapergunta']));
            }

            //verifica se foi passado o id da entidade de cadastro
            if (!isset($data['id_entidadecadastro']) || empty($data['id_entidadecadastro'])) {
                //verifica se NÃO tem registro salvo
                if (!$entity->getId_entidadecadastro()) {
                    //se não tiver seta o valor da sessao
                    $entidade = $this->getReference('\G2\Entity\Entidade', $this->getId_entidade());
                    $entity->setId_entidadeCadastro($entidade);
                }
            } else {
                //se for passado o valor seta a referencia
                $entity->setId_entidadeCadastro($this->getReference('\G2\Entity\Entidade', $data['id_entidadecadastro']));
            }
            //verifica se foi passado uma posição com bl_ativo ou se ela não é vazia
            if (array_key_exists('bl_ativo', $data) && !is_null($data['bl_ativo'])) {
                //seta o valor que foi passado
                $entity->setBl_ativo($data['bl_ativo']);
            } else {
                //se entrar verifica se é pra pegar o valor default que já esta salvo ou seta o valor como true
                $entity->setBl_ativo($entity->getBl_ativo() ? $entity->getBl_ativo() : true);
            }
            return $this->save($entity);

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao salvar pergunta. " . $e->getMessage());
        }
    }

    /**
     * Método para inativar categoria e perguntas vinculadas
     * @param $idCategoria
     * @return bool
     * @throws \Zend_Exception
     */
    public function apagarCategoriaPergunta($idCategoria)
    {
        try {
            $this->beginTransaction();
            $perguntas = $this->retornarPerguntas(array('id_categoriapergunta' => $idCategoria));

            if ($perguntas) {
                foreach ($perguntas as $pergunta) {
                    $pergunta->setBl_ativo(false);
                    if (!$this->save($pergunta)) {
                        throw new \Exception("Erro ao inativar pergunta da categoria.");
                    }
                }
            }

            $categoria = $this->find('\G2\Entity\CategoriaPergunta', $idCategoria);
            $categoria->setBl_ativo(false);
            if (!$this->save($categoria)) {
                throw new \Exception("Erro ao inativar categoria");
            }

            $this->commit();
            return true;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar apagar categoria da pergunta. " . $e->getMessage());
        }
    }

    /**
     * Método para retornar as categorias e perguntas vinculadas a entidade da sessão ou passada por parametro
     * @param array $params
     * @param null $idEntidade
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function retornarPesquisaPerguntas(array $params = array(), $idEntidade = null)
    {
        try {
            //seta o id_entidade, com o parametro ou a da sessão
            $idEntidade = $idEntidade ? $idEntidade : $this->getId_entidade();

            //busca entidade
            $entidade = $this->find('\G2\Entity\Entidade', $idEntidade);

            $mensageiro = new \Ead1_Mensageiro();

            //verifica se encontrou a entidade e o esquema de configuração
            if ($entidade && $entidade->getId_esquemapergunta()) {

                //verifica se o esquema de configuração é valido
                if ($entidade->getId_esquemapergunta()->getBl_ativo()) {

                    $params['id_esquemapergunta'] = $entidade->getId_esquemapergunta()->getId_esquemapergunta();
                    $result = $this->em->getRepository('\G2\Entity\Pergunta')->retornarPesquisaPerguntas($params);

                    //verifica se tem categoria
                    if ($result) {

                        //cria uma array vazio para retornar
                        $arrayReturn = array();

                        //percorre as categorias
                        foreach ($result as $key => $pergunta) {

                            //monta o array do retorno com os dados da categoria e uma posição com as perguntas vazia
                            $id_categoria = $pergunta['id_categoriapergunta'];
                            if (!isset($arrayReturn[$id_categoria])) {
                                $arrayReturn[$id_categoria] = array(
                                    'id_categoriapergunta' => $pergunta['id_categoriapergunta'],
                                    'st_categoriapergunta' => $pergunta['st_categoriapergunta'],
                                    'perguntas' => array()
                                );
                            }

                            $arrayReturn[$id_categoria]['perguntas'][] = array(
                                'id_pergunta' => $pergunta['id_pergunta'],
                                'st_pergunta' => $pergunta['st_pergunta'],
                                'st_resposta' => $pergunta['st_resposta']
                            );

                        }

                        sort($arrayReturn);//reordena o array
                        $mensageiro->setMensageiro($arrayReturn, \Ead1_IMensageiro::SUCESSO);
                    } else {

                        $mensageiro->setMensageiro("Sem registros para exibição. :(", \Ead1_IMensageiro::ERRO);
                    }

                } else {
                    $mensageiro->setMensageiro("Esquema de Perguntas vinculado a Entidade não esta ativo. :(", \Ead1_IMensageiro::ERRO);
                }
            } else {

                $mensageiro->setMensageiro("Entidade não tem Esquema de Perguntas vinculado. :(", \Ead1_IMensageiro::ERRO);
            }

            return $mensageiro;
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
}
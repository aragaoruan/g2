<?php
/**
 * Created by PhpStorm.
 * Date: 21/10/14
 * Time: 15:27
 */

namespace G2\Negocio;

use G2\Entity\AssuntoEntidadeCo;

/**
 * Classe de negócio para AssuntoCo
 * @package G2\Negocio
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AssuntoCo extends Negocio
{


    private $repository = 'G2\Entity\AssuntoCo';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Pesquisa assuntos para listagem de cadastro
     * @param array $params
     * @return bool
     * @throws \Zend_Exception
     */
    public function findByVwPesquisaAssuntoCo($params = array())
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwPesquisaAssuntoCo');
            if (!isset($params['id_entidadecadastro'])) {
                $params['id_entidadecadastro'] = $this->sessao->id_entidade;
            }
            $arrColumns = array('vw.id_assuntoco', 'vw.id_assuntocopai', 'vw.dt_cadastro', 'vw.st_assuntoco'
            , 'vw.st_assuntocopai', 'vw.st_situacao', 'vw.id_tipoocorrencia', 'vw.st_tipoocorrencia'
            , 'vw.nu_order');
            $query = $repo->createQueryBuilder("vw")
                ->select('DISTINCT ' . implode(", ", $arrColumns))
                ->where('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
            if (array_key_exists('id_situacao', $params) && $params['id_situacao']) {
                $query->andWhere('vw.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            //seta os parametros do like
            if ((array_key_exists('st_assuntoco', $params) && $params['st_assuntoco'])
                ||
                (array_key_exists('st_assuntocopai', $params) && $params['st_assuntocopai'])
            ) { //verifica se um dos dois parametros veio setado
                $where = '';
                if ($params['st_assuntoco']) {
                    $where .= "vw.st_assuntoco LIKE '%{$params['st_assuntoco']}%'";
                }
                //se os dois parametros veio concatena o OR
                if ($params['st_assuntoco'] && $params['st_assuntocopai']) {
                    $where .= " OR ";
                }
                if ($params['st_assuntocopai']) {
                    $where .= "vw.st_assuntocopai LIKE '%{$params['st_assuntocopai']}%'";
                }
                if ($where)
                    $query->andWhere($where);
            }
            if (array_key_exists('id_tipoocorrencia', $params) && $params['id_tipoocorrencia']) {
                $query->andWhere('vw.id_tipoocorrencia = :id_tipoocorrencia')
                    ->setParameter('id_tipoocorrencia', $params['id_tipoocorrencia']);
            }
            if ($params['id_entidadecadastro']) {
                $query->andWhere('vw.id_entidadecadastro = :id_entidadecadastro')
                    ->setParameter('id_entidadecadastro', $params['id_entidadecadastro']);
            }
            $query->orderBy("vw.nu_order", "ASC");

            // \Zend_Debug::dump($query->getQuery());die;
            $result = $query->getQuery()->getResult();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Retorna tb_tipoocorrencia
     * @param array $where
     * @return \Ead1_Mensageiro
     */
    public function findTipoOcorrencia(array $where = array())
    {
        $result = $this->findBy('G2\Entity\TipoOcorrencia', $where);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * @param $id_assuntoco
     * @return \Ead1_Mensageiro
     */
    public function findAssuntoCo($id_assuntoco)
    {
        $result = $this->find('G2\Entity\AssuntoCo', (int)$id_assuntoco);
        if ($result->getSt_assuntoco() != null) {
            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * @param $params
     * @return array|string
     */
    public function findByAssuntoEntidadeCo($params)
    {
        try {
            $repositoryName = 'G2\Entity\AssuntoEntidadeCo';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $params
     * @return \Ead1_Mensageiro|string
     */
    public function findTextoSistema($params)
    {
        try {
            if (is_array($params)) {
                $find = array('id_textocategoria' => $params['id_textocategoria']
                , 'id_entidade' => $this->sessao->id_entidade
                , 'id_situacao' => 60);
            }
            $repositoryName = 'G2\Entity\TextoSistema';
            $result = $this->findBy($repositoryName, $find);
            if ($result) {
                $arrReturn = array();
                foreach ($result as $row) {
                    $arrReturn[] = array(
                        'id_textosistema' => $row->getId_textosistema(),
                        'st_textosistema' => ($row->getSt_textosistema())
                    );
                }

                return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                    \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $array
     * @return \Ead1_Mensageiro
     */
    public function salvar(array $array)
    {
        $this->beginTransaction();
        try {
            //------------------PREPARAÇÃO DOS MODELOS-----------
            $arrayAssuntoCo = $array['dados'];
            $arrayIds = array();

            //--------------Array de entidads da arvore-----
            if (array_key_exists('id_entidade', $array)) {
                $arrayIds = $array['id_entidade'];
            }

            //--------------Entity AssuntoCo--------------------
            $assuntoCo = new \G2\Entity\AssuntoCo();


            $string_alteracao = ' cadastrado ';
            $string_tipo_assunto = ' Assunto ';
            if (array_key_exists('id_assuntoco', $arrayAssuntoCo) && $arrayAssuntoCo['id_assuntoco']) {
                /** @var \G2\Entity\AssuntoCo $assuntoCo */
                $assuntoCo = $this->find(\G2\Entity\AssuntoCo::class, $arrayAssuntoCo['id_assuntoco']);
                $string_alteracao = ' alterado ';
            }


            //Define os valores default para um novo registro
            if (!$assuntoCo->getId_assuntoco()) {
                $usuario = $this
                    ->getReference(\G2\Entity\Usuario::class, $arrayAssuntoCo['id_usuariocadastro']);
                $entidade = $this
                    ->getReference(\G2\Entity\Entidade::class, $arrayAssuntoCo['id_entidadecadastro']);

                $assuntoCo->setDt_cadastro(new \DateTime())
                    ->setBl_ativo(true)
                    ->setId_usuariocadastro($usuario)
                    ->setId_entidadecadastro($entidade);
            }

            $situacao = $this->getReference(\G2\Entity\Situacao::class, $arrayAssuntoCo['id_situacao']);
            $tipo = $this->find(\G2\Entity\TipoOcorrencia::class, $arrayAssuntoCo['id_tipoocorrencia']);

            $assuntoCo->setBl_interno((int)$arrayAssuntoCo['bl_interno'])
                ->setSt_assuntoco($arrayAssuntoCo['st_assuntoco'])
                ->setId_situacao($situacao)
                ->setId_tipoocorrencia($tipo);


            $assuntoCo = $this->defineParametrosAssuntoPai($assuntoCo, $arrayAssuntoCo);

            $this->verificaAssuntosFilhos($assuntoCo);


            if (array_key_exists('id_assuntocategoria', $arrayAssuntoCo)
                && $arrayAssuntoCo['id_assuntocategoria']
            ) {
                $assuntoCo->setId_assuntocategoria($arrayAssuntoCo['id_assuntocategoria']);
            }

            if (array_key_exists('id_textosistema', $arrayAssuntoCo) && $arrayAssuntoCo['id_textosistema']) {
                //$assuntoCo->setId_textosistema($arrayAssuntoCo['id_textosistema']);
                $assuntoCo->setId_textosistema($this->find('\G2\Entity\TextoSistema',
                    $arrayAssuntoCo['id_textosistema']));
            }


            $assuntoCo = $this->save($assuntoCo);


            //-------------------------SALVA OS REGISTROS DA ARVORE DE ENTIDADES----------------
            if ($assuntoCo->getId_assuntoco()) {
                $this->atualizarBlInternoFilho($assuntoCo->getId_assuntoco(), $assuntoCo->getBl_interno());
                $this->deletarAssuntoEntidadeCo($assuntoCo->getId_assuntoco());
                $this->salvarAssuntoEntidadeCo($arrayIds, $assuntoCo->getId_assuntoco());
            }

            $this->commit();
            return new \Ead1_Mensageiro('Assunto' . $string_alteracao . 'com sucesso',
                \Ead1_IMensageiro::SUCESSO, $assuntoCo->getId_assuntoco());
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Verifica se o assunto tem assuntos filhos com bl_interno false, se não encontrar dispara exception
     * @param \G2\Entity\AssuntoCo $assuntoCo
     * @throws \Exception
     */
    private function verificaAssuntosFilhos(\G2\Entity\AssuntoCo $assuntoCo)
    {
        //se o bl_interno for false e se tiver assunto pai
        if (!$assuntoCo->getBl_interno() && $assuntoCo->getId_assuntocopai()) {
            $verificar = $this->findBy(\G2\Entity\AssuntoCo::class,
                array(
                    'id_assuntoco' => $assuntoCo->getId_assuntocopai()->getId_assuntoco(),
                    'bl_interno' => false
                )
            );

            if (!$verificar) {
                throw new \Exception('O Subassunto deve ser interno');
            }
        }
    }


    /**
     * Define na entity os parametros para quando o assunto pai for passado
     * @param \G2\Entity\AssuntoCo $assuntoCo
     * @param array $arrayAssuntoCo
     * @return \G2\Entity\AssuntoCo
     */
    private function defineParametrosAssuntoPai(\G2\Entity\AssuntoCo $assuntoCo, array $arrayAssuntoCo)
    {
        //define o assunto pai
        if (array_key_exists('id_assuntocopai', $arrayAssuntoCo) && $arrayAssuntoCo['id_assuntocopai']) {
            $assuntoCo->setId_assuntocopai($this->getReference(\G2\Entity\AssuntoCo::class,
                $arrayAssuntoCo['id_assuntocopai']
            ))
                ->setBl_autodistribuicao((int)$arrayAssuntoCo['bl_autodistribuicao'])
                ->setBl_abertura((int)$arrayAssuntoCo['bl_abertura'])
                ->setBl_cancelamento((int)$arrayAssuntoCo['bl_cancelamento'])
                ->setBl_trancamento((int)$arrayAssuntoCo['bl_trancamento'])
                ->setBl_recuperacao_resgate((int)$arrayAssuntoCo['bl_recuperacao_resgate']);
        } else {
            //assuntos pai não tem essas flags no cadastro
            $assuntoCo->setBl_autodistribuicao(0);
            $assuntoCo->setBl_abertura(1); // assunto pai sempre permite abertura
            $assuntoCo->setBl_cancelamento(0);
            $assuntoCo->setBl_trancamento(0);
            $assuntoCo->setBl_interno($arrayAssuntoCo['bl_interno']);
        }

        return $assuntoCo;
    }

    /**
     * @param $id_assuntoco
     * @param $bl_interno
     * @return bool|\Ead1_Mensageiro
     */
    private function atualizarBlInternoFilho($id_assuntoco, $bl_interno)
    {
        $this->beginTransaction();
        try {
            $coAssuntos = $this->findBy('\G2\Entity\AssuntoCo',
                array('id_assuntocopai' => $id_assuntoco)
            );

            foreach ($coAssuntos as $coAssunto) {
                $coAssunto->setBl_interno((int)$bl_interno);
                $this->save($coAssunto);
            }

            $this->commit();
            return true;
        } catch (Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param array $data
     * @param $id_assuntoco
     * @return array
     */
    public function salvarAssuntoEntidadeCo(array $data, $id_assuntoco)
    {
        try {
            $assunto = $this->find('\G2\Entity\AssuntoCo', (int)$id_assuntoco);
            foreach ($data as $entidade) {
                $entity = new AssuntoEntidadeCo();
                $entity->setId_entidade($this->find('\G2\Entity\Entidade', (int)$entidade));
                $entity->setId_assuntoco($assunto);
                $this->save($entity);
            }

            return array(
                'type' => 'success',
                'title' => 'Sucesso :)',
                'text' => 'Entidades vinculadas com Sucesso!'
            );

        } catch (Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao vincular entidades com o sistema: ' . $e->getMessage()
            );
        }


    }

    /**
     * @param $id_assuntoco
     * @return bool
     */
    public function deletarAssuntoEntidadeCo($id_assuntoco)
    {
        $ids = $this->findBy('G2\Entity\AssuntoEntidadeCo', array('id_assuntoco' => $id_assuntoco));

        foreach ($ids as $id) {
            $this->em->remove($id);
            $this->em->flush();
        }

        return true;

    }

}

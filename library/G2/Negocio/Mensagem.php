<?php

namespace G2\Negocio;

use Doctrine\ORM\Query\ResultSetMapping;
use G2\Constante\Evolucao;
use G2\Constante\Notificacao;
use G2\Constante\Sistema;
use G2\Constante\TipoEnvio;
use G2\Entity\EnvioDestinatario;
use G2\Entity\EnvioMensagem;
use G2\Entity\NotificacaoEntidadePerfil;
use G2\Entity\TipoDestinatario;

/**
 * Classe de Negocio para PedidoIntegracao, NotificacaoUsuario
 * tb_mensagem (principal)
 *
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 2014-08-28
 * @update Débora Castro - 15/05/2015
 */
class Mensagem extends Negocio
{

    private $repositoryName = 'G2\Entity\Mensagem';

    public function __construct()
    {
        parent::__construct();
    }

    public function criarMensagemVencimentoCartao()
    {

    }

    public function atualizarMensagem($data)
    {
        try {
            $mensagem = $this->find('\G2\Entity\Mensagem', (int)$data['id_mensagem']);

            unset($data['id_usuariocadastro']);

            if ($mensagem != null)
                $mensagem = $this->arrayToEntity($mensagem, $data);

            $result = $this->merge($mensagem);

            if ($result) {
                return true;
            } else {
                return false;
            }

        } catch (\Zend_Exception $e) {
            return new \Zend_Exception($e->getMessage());
        }
    }


    public function historicoMensagem($id_matricula)
    {
        try {

            $sessao = new \Zend_Session_Namespace('geral');

            $stmt = $this->em->getConnection()->prepare(
                "SELECT vw.st_mensagem,
                    vw.st_evolucao, vw.st_tipoenvio,
                    CONVERT(VARCHAR, dt_envio, 103) AS dt_envio, *
               FROM vw_enviomensagem vw
         INNER JOIN tb_matricula m ON vw.id_usuario = m.id_usuario
              WHERE m.id_matricula = :matricula
                AND vw.id_entidade = :entidade
           ORDER BY vw.dt_envio DESC");

            $stmt->bindParam(':matricula', $id_matricula, \PDO::PARAM_INT);
            $stmt->bindParam(':entidade', $sessao->id_entidade, \PDO::PARAM_INT);

            $stmt->execute();
            $result = $stmt->fetchAll();

            return $result;
        } catch (\Zend_Exception $exception) {
            return new \Zend_Exception($exception->getMessage());
        }
    }

    /*
     * Método padrão para salvar um registro na tb_mensagem, valida todos os parâmetros que são obrigatórios;
     * @param array
     * @return Mensageiro
     */
    public function salvarMesagem(array $dados)
    {
        try {
            if (!isset($dados['id_situacao']) || !isset($dados['st_mensagem']) || !isset($dados['st_texto']) || !isset($dados['bl_importante'])) {
                return new \Ead1_Mensageiro('Não foram passados todos os parâmetros com critério de obrigatoriedade!', \Ead1_IMensageiro::AVISO);
            }

            if (!isset($dados['id_usuariocadastro']) && empty($this->sessao->id_usuario)) {
                return new \Ead1_Mensageiro('Não foi setado o id_usuariocastro', \Ead1_IMensageiro::AVISO);
            }

            if (!isset($dados['id_entidade']) && empty($this->sessao->id_entidade)) {
                return new \Ead1_Mensageiro('Não foi setado o id_entidade', \Ead1_IMensageiro::AVISO);
            }

            $entityMensagem = new \G2\Entity\Mensagem();

            $entityMensagem->setId_entidade((isset($dados['id_entidade'])) ? ($this->find('\G2\Entity\Entidade', (int)$dados['id_entidade'])) : ($this->find('\G2\Entity\Entidade', (int)$this->sessao->id_entidade)));
            $entityMensagem->setId_usuariocadastro((isset($dados['id_usuariocadastro'])) ? ($this->find('\G2\Entity\Usuario', (int)$dados['id_usuariocadastro'])) : ($this->find('\G2\Entity\Usuario', (int)$this->sessao->id_usuario)));
            $entityMensagem->setId_situacao($this->find('\G2\Entity\Situacao', (int)$dados['id_situacao']));
            $entityMensagem->setSt_mensagem($dados['st_mensagem']);
            $entityMensagem->setDt_cadastro(new \DateTime());
            $entityMensagem->setBl_importante($dados['bl_importante']);
            $entityMensagem->setSt_texto($dados['st_texto']);
            //opcionais
            $entityMensagem->setId_areaconhecimento((isset($dados['id_areaconhecimento'])) ? $this->find('\G2\Entity\AreaConhecimento', (int)$dados['id_areaconhecimento']) : null);
            $entityMensagem->setId_turma((isset($dados['id_turma'])) ? $this->find('\G2\Entity\Turma', (int)$dados['id_turma']) : null);
            $entityMensagem->setId_funcionalidade((isset($dados['id_funcionalidade'])) ? $this->find('\G2\Entity\Funcionalidade', (int)$dados['id_funcionalidade']) : null);
            $entityMensagem->setId_projetopedagogico((isset($dados['id_projetopedagogico'])) ? $this->find('\G2\Entity\ProjetoPedagogico', (int)$dados['id_projetopedagogico']) : null);

            $this->save($entityMensagem);

            if ($entityMensagem->getId_mensagem()) {
                $retornoSucesso = new \Ead1_Mensageiro('Mensagem salva com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $retornoSucesso->setId($entityMensagem->getId_mensagem());
                return $retornoSucesso;
            } else {
                return new \Ead1_Mensageiro('Erro ao salvar mensagem!', \Ead1_IMensageiro::ERRO);
            }

        } catch (\Zend_Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar mensagem: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /*
    * Método padrão para salvar dados na tb_enviomensagem, valida todos os parâmetros que são obrigatórios;
    * @param array
    * @return Mensageiro
    */
    public function salvarEnvioMensagem(array $dados)
    {
        try {
            if (!isset($dados['id_mensagem']) || !isset($dados['id_tipoenvio']) || !isset($dados['id_evolucao'])) {
                return new \Ead1_Mensageiro('Não foram passados todos os parâmetros com critério de obrigatoriedade!', \Ead1_IMensageiro::AVISO);
            }

            $entityEnvioMensagem = new \G2\Entity\EnvioMensagem();
            $entityEnvioMensagem->setId_mensagem($this->find('\G2\Entity\Mensagem', (int)$dados['id_mensagem']));
            $entityEnvioMensagem->setId_tipoenvio($this->find('\G2\Entity\TipoEnvio', (int)$dados['id_tipoenvio']));
            $entityEnvioMensagem->setId_evolucao($this->find('\G2\Entity\Evolucao', (int)$dados['id_evolucao']));
            $entityEnvioMensagem->setDt_enviar(isset($dados['dt_enviar']) ? $dados['dt_enviar'] : new \DateTime());
            $entityEnvioMensagem->setDt_cadastro(new \DateTime());

            $entityEnvioMensagem->setId_emailconfig(isset($dados['id_emailconfig']) ? ($this->find('\G2\Entity\EmailConfig', (int)$dados['id_emailconfig'])) : null);
            $entityEnvioMensagem->setId_sistema(isset($dados['id_sistema']) ? $this->find('\G2\Entity\Sistema', (int)$dados['id_sistema']) : null);

            $this->save($entityEnvioMensagem);

            if ($entityEnvioMensagem->getId_enviomensagem()) {
                $retornoSucesso = new \Ead1_Mensageiro('Mensagem Envio salva com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $retornoSucesso->setId($entityEnvioMensagem->getId_enviomensagem());
                return $retornoSucesso;
            } else {
                return new \Ead1_Mensageiro('Erro ao salvar mensagem!', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /*
   * Método padrão para salvar dados na tb_enviodestinatario, valida todos os parâmetros que são obrigatórios;
   * @param array
   * @return Mensageiro
   */
    public function salvarEnvioDestinatario(array $dados)
    {
        try {

            if (!isset($dados['id_usuario']) || !isset($dados['id_tipodestinatario']) || !isset($dados['id_enviomensagem'])) {
                return new \Ead1_Mensageiro('Não foram passados todos os parâmetros com critério de obrigatoriedade!', \Ead1_IMensageiro::AVISO);
            }

            $entityUsuarioDestinatario = new \G2\Entity\EnvioDestinatario();
            $entityUsuarioDestinatario->setId_usuario($this->find('\G2\Entity\Usuario', (int)$dados['id_usuario']));
            $entityUsuarioDestinatario->setId_tipodestinatario($this->find('\G2\Entity\TipoDestinatario', (int)$dados['id_tipodestinatario']));
            $entityUsuarioDestinatario->setId_enviomensagem($this->find('\G2\Entity\EnvioMensagem', (int)$dados['id_enviomensagem']));
            //opcionais
            $entityUsuarioDestinatario->setId_matricula(isset($dados['id_matricula']) ? $this->find('\G2\Entity\Matricula', (int)$dados['id_matricula']) : null);
            $entityUsuarioDestinatario->setSt_endereco(isset($dados['st_endereco']) ? $dados['st_endereco'] : null);
            $entityUsuarioDestinatario->setSt_nome(isset($dados['st_nome']) ? $dados['st_nome'] : null);
            $entityUsuarioDestinatario->setNu_telefone(isset($dados['nu_telefone']) ? $dados['nu_telefone'] : null);
            $entityUsuarioDestinatario->setId_evolucao(isset($dados['id_evolucao']) ? (int)$dados['id_evolucao'] : null);

            $this->save($entityUsuarioDestinatario);

            if ($entityUsuarioDestinatario->getId_enviodestinatario()) {
                $retornoSucesso = new \Ead1_Mensageiro('Usuário para envio, salvo com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $retornoSucesso->setId($entityUsuarioDestinatario->getId_enviodestinatario());
                return $retornoSucesso;
            } else {
                return new \Ead1_Mensageiro('Erro ao salvar usuário destinatário!', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     *  Gera mensagem específica para funcionalidade Organização->Notificações->Sala limite excedido
     * @param $id_entidade
     * @return \Ead1_Mensageiro
     */
    public function salvarMensagemSalaLimiteAlocacao($id_entidade)
    {
        $htmlTextoConteudo = '';
        $htmlTexto = '';
        $dadosMensagem = array();

        $mensagens = $this->findBy('\G2\Entity\VwSalasLimiteAlocacao', array('id_entidade' => $id_entidade));

        if (is_array($mensagens) && !empty($mensagens)) {

            $dadosMensagem['id_entidade'] = (int)$mensagens[0]->getId_entidade();
            $dadosMensagem['id_situacao'] = \G2\Constante\Situacao::TB_MENSAGEM_ATIVA;
            $dadosMensagem['st_mensagem'] = $mensagens[0]->getSt_notificacao();
            $dadosMensagem['id_usuariocadastro'] = 1;
            $dadosMensagem['bl_importante'] = false;

            foreach ($mensagens as $linha) {
                $htmlTextoConteudo .= '<tr><td>' .
                    $linha->getSt_saladeaula() . '</td>'
                    . '<td>' . $linha->getNu_maxalunos() . '</td>'
                    . '<td>' . $linha->getNu_alunosalocados() . '</td>'
                    . '<td>' . $linha->getSt_nomeprofessor() . '</td>'
                    . '</tr>';
            }

            $htmlTexto = '<label>Essas salas atingiram ' . $mensagens[0]->getNu_percentuallimitealunosala() . '% do limite de alunos e tiveram alocação nas últimas 24 horas<label><br>'
                . '<table class="table table-bordered backgrid"><thead><th>Sala</th><th>Máx. de Alunos</th>' .
                '<th>Nº de alunos alocados</th><th>Professor</th></thead>' .
                '<tbody>' . $htmlTextoConteudo . '</tbody></table>';

            $dadosMensagem['st_texto'] = $htmlTexto;

            return $this->salvarMesagem($dadosMensagem);

        } else {
            return new \Ead1_Mensageiro('Não existem registros de sala de aula com limite excedido!', \Ead1_IMensageiro::AVISO);
        }

    }

    /*
     * Gera mensagem específica para funcionalidade de verificação de contratos confirmados sem matricula vinculada
     * @param id_entidade
     * @return id_mensagem
     */
    public function salvarMensagemContratoSemMatriculaVinculada(array $param)
    {
        $htmlTextoConteudo = '';
        $htmlTexto = '';
        $dadosMensagem = array();

        if (is_array($param) && !empty($param)) {

            $dadosMensagem['id_entidade'] = $param['id_entidade'];
            $dadosMensagem['id_situacao'] = \G2\Constante\Situacao::TB_MENSAGEM_ATIVA;
            $dadosMensagem['st_mensagem'] = 'Vendas Confirmadas - Alunos sem matrícula efetivada';
            $dadosMensagem['id_usuariocadastro'] = 1;
            $dadosMensagem['bl_importante'] = false;


            foreach ($param['result'] as $linha) {
                $htmlTextoConteudo .= '
                    <tr>
                        <td>' . $linha['st_nomecompleto'] . '</td>'
                    . '<td>' . $linha['st_produto'] . '</td>'
                    . '<td>' . $this->converteDataPt($linha['dt_cadastro']) . '</td>'
                    . '<td>' . $linha['st_evolucao'] . '</td>'
                    . '<td>' . $linha['st_meiopagamento'] . '</td>'
                    . '</tr>';
            }

            $htmlTexto = '
            Prezado(a);<br>
            Existem ' . count($param['result']) . ' venda(s) confirmadas sem matrícula ativa.
            <br>
            <br>
             <table style="border:1; border-color:#000000;">
                <thead>
                    <th>Nome do Aluno</th>
                    <th>Produto</th>
                    <th>Data da Compra</th>
                    <th>Evolução</th>
                    <th>Meio de Pagamento</th>
                </thead>
                <tbody>' . $htmlTextoConteudo . '</tbody>
            </table>';

            $dadosMensagem['st_texto'] = $htmlTexto;

            return $this->salvarMesagem($dadosMensagem);

        } else {
            return new \Ead1_Mensageiro('Não existem registros de sala de aula com limite excedido!', \Ead1_IMensageiro::AVISO);
        }

    }

    /*
     * Pesquisa os Perfil por entidade, para gerar uma determinada notificação
     *
     */
    public function pesquisaNotificacaoEntidadePerfil(array $dados)
    {

        $entityPerfil = $this->findBy('\G2\Entity\NotificacaoEntidadePerfil', array('id_notificacaoentidade' => (int)$dados['id_notificacaoentidade']));

        if (is_array($entityPerfil) && !empty($entityPerfil)) {

            foreach ($entityPerfil as $idPerfil) {
                $dados['id_perfil'] = $idPerfil->getId_perfil()->getId_perfil();
//                    \Zend_Debug::dump( $idPerfil->getId_perfil()->getId_perfil());
                $retorno = $this->setarUsuariosDestinatarios($dados);

                if ($retorno->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    return new \Ead1_Mensageiro('Erro ao salvar usuários destinatários para este perfil', \Ead1_IMensageiro::ERRO);
                }
            }
        }

        return new \Ead1_Mensageiro('Usuários destinatários salvos com suceso, para este perfil', \Ead1_IMensageiro::SUCESSO);

    }

    /*
     * Pesquisa os usuario de acordo com o perfil e entidade e salva na tabela de destinatários
     * @param array
     * @return mensageiro
     */
    public function setarUsuariosDestinatarios(array $dados)
    {
        $arrayDados = array();

        $usuariosPerfil = $this->findBy('\G2\Entity\UsuarioPerfilEntidade', array('id_entidade' => $dados['id_entidade'], 'bl_ativo' => 1, 'id_perfil' => $dados['id_perfil']));
        if (is_array($usuariosPerfil) && !empty($usuariosPerfil)) {

            foreach ($usuariosPerfil as $row) {
                $arrayDados['id_usuario'] = $row->getId_usuario();
                $arrayDados['id_tipodestinatario'] = TipoDestinatario::DESTINATARIO;
                $arrayDados['id_enviomensagem'] = $dados['id_enviomensagem'];
                $arrayDados['id_evolucao'] = Evolucao::TB_ENVIODESTINATARIO_NAO_LIDA;
                $salvarUsuarioDestinatario = $this->salvarEnvioDestinatario($arrayDados);

                if ($salvarUsuarioDestinatario->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    return new \Ead1_Mensageiro('Erro ao salvar usuário destinatário', \Ead1_IMensageiro::ERRO);
                }
            }

        }
        return new \Ead1_Mensageiro('Usuarios salvos para este perfil!', \Ead1_IMensageiro::SUCESSO);

    }

    /**
     * Ação determinada pela bl de envio de e-mail, na tb_notificacaoentidade
     * @param array $dados
     * @return \Ead1_Mensageiro
     */
    public function salvarEnvioMensagemComEsemEmail(array $dados)
    {

        $dadosEnvioDestinatario = array();
        $dadosEnvioMensagem = array();

        //este trecho gera apenas uma notificação interna
        $dadosEnvioMensagem['id_mensagem'] = $dados['id_mensagem'];
        $dadosEnvioMensagem['id_sistema'] = Sistema::GESTOR;
        $dadosEnvioMensagem['id_emailconfig'] = 3;
        $dadosEnvioMensagem['id_tipoenvio'] = TipoEnvio::HTML;
        $dadosEnvioMensagem['id_evolucao'] = Evolucao::TB_ENVIOMENSAGEM_ENVIADO;

        $salvarEM = $this->salvarEnvioMensagem($dadosEnvioMensagem);

        if ($salvarEM->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $dadosEnvioDestinatario['id_enviomensagem'] = $salvarEM->getId();
            $dadosEnvioDestinatario['id_notificacaoentidade'] = $dados['id_notificacaoentidade'];
            $dadosEnvioDestinatario['id_entidade'] = $dados['id_entidade'];

            $retornoUsuarioEnviar = $this->pesquisaNotificacaoEntidadePerfil($dadosEnvioDestinatario);

            if ($retornoUsuarioEnviar->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return new \Ead1_Mensageiro('Erro ao salvar a mensagem para os usuários', \Ead1_IMensageiro::ERRO);
            }
        }

        if ($dados['bl_enviaparaemail'] == true) {
            $dadosEnvioMensagem['id_sistema'] = Sistema::GESTOR;

            $emailConfig = $this->findOneBy('\G2\Entity\EmailConfig', array('id_entidade' => $dados['id_entidade']));

            if (!$emailConfig->getId_emailconfig()) {
                return new \Ead1_Mensageiro('Não exite serviço de e-mail configurado para esta entidade', \Ead1_IMensageiro::AVISO);
            }
            $dadosEnvioMensagem['id_emailconfig'] = $emailConfig->getId_emailconfig();
            $dadosEnvioMensagem['id_tipoenvio'] = TipoEnvio::EMAIL;
            $dadosEnvioMensagem['id_evolucao'] = Evolucao::TB_ENVIOMENSAGEM_NAO_ENVIADO;
            $salvarEM2 = $this->salvarEnvioMensagem($dadosEnvioMensagem);

            if ($salvarEM2->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $dadosEnvioDestinatario['id_enviomensagem'] = $salvarEM2->getId();
                $dadosEnvioDestinatario['id_notificacaoentidade'] = $dados['id_notificacaoentidade'];
                $dadosEnvioDestinatario['id_entidade'] = $dados['id_entidade'];

                $retornoUsuarioEnviar = $this->pesquisaNotificacaoEntidadePerfil($dadosEnvioDestinatario);

                if ($retornoUsuarioEnviar->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    return new \Ead1_Mensageiro('Erro ao salvar a mensagem para os usuários', \Ead1_IMensageiro::ERRO);
                }
            }

        }
        return new \Ead1_Mensageiro('Envio gerado com sucesso!', \Ead1_IMensageiro::SUCESSO);

    }

    /**
     * Método para geração de notificação para usuário com número de salas com limite de alocação excedido;
     * Exclusivo para o ROBO
     * Executado via Cron (1 vez ao dia)
     * @return \Ead1_Mensageiro
     */
    public function gerarNotificacaoLimiteSalaUsuario()
    {
        try {

            $dadosEnvioMensagem = array();

            //o ID notificação é fixo, pois representa uma notificação específica da ORGANIZAÇÃO->Notificações
            $selectNotificacaoEntidade = $this->findBy('\G2\Entity\NotificacaoEntidade', array('id_notificacao' => Notificacao::NOTIFICACAO_SALAS_LIMITE_EXCEDIDO));

            if (is_array($selectNotificacaoEntidade) && !empty($selectNotificacaoEntidade)) {

                foreach ($selectNotificacaoEntidade as $row) {
                    //gera as notificações de acordo com a entidade;
                    $notificacaoEntidade = $this->salvarMensagemSalaLimiteAlocacao($row->getId_entidade()->getId_entidade());

                    if ($notificacaoEntidade->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                        $dadosEnvioMensagem['id_mensagem'] = $notificacaoEntidade->getId();
                        $dadosEnvioMensagem['id_entidade'] = $row->getId_entidade()->getId_entidade();
                        $dadosEnvioMensagem['id_notificacaoentidade'] = $row->getId_notificacaoentidade();
                        $dadosEnvioMensagem['bl_enviaparaemail'] = $row->getBl_enviaparaemail();

                        $retornoGeral = $this->salvarEnvioMensagemComEsemEmail($dadosEnvioMensagem);

                        if ($retornoGeral->getTipo() == \Ead1_IMensageiro::ERRO) {
                            return $retornoGeral;
                        }

                    }
                }
            }

            return new \Ead1_Mensageiro('Mensagens enviadas com sucesso', \Ead1_IMensageiro::SUCESSO);


        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Geração de e-mail via texto sistema
     * @param array $dados [id_usuario,id_mensagempadrao,id_matricula,st_mensagem]
     * @return \Ead1_Mensageiro|Mensageiro
     */
    public function gerarEmailMensagemTextoSitema(array $dados)
    {
        try {

            if (!isset($dados['id_mensagempadrao']) || empty($dados['id_mensagempadrao'])) {
                return new \Ead1_Mensageiro('Parâmetro id_mensagempadrao não foi passado!', \Ead1_IMensageiro::ERRO);
            }

            if (!isset($dados['id_entidade']) || empty($dados['id_entidade'])) {
                $dados['id_entidade'] = $this->sessao->id_entidade;
            }
            $negocioEEM = new \G2\Negocio\EmailEntidadeMensagem();
            $texto = $negocioEEM->findOneByEmailEntidadeMensagem((int)$dados['id_mensagempadrao'], $dados['id_entidade']);
            if (!is_null($texto)) {
                $textoSistemaBO = new \TextoSistemaBO();
                $arParametros = array('id_matricula' => $dados['id_matricula'], 'id_entidade' => $dados['id_entidade']);

                $html_mensagem = $textoSistemaBO->gerarTexto(new \TextoSistemaTO(array('id_textosistema' => $texto->getId_textosistema()->getId_textosistema())), $arParametros)->subtractMensageiro()->getFirstMensagem()->getSt_texto();

                $parametrosMensagem['id_situacao'] = \G2\Constante\Situacao::TB_MENSAGEM_ATIVA;
                $parametrosMensagem['id_entidade'] = $dados['id_entidade'];
                $parametrosMensagem['id_usuariocadastro'] = $this->sessao->id_usuario ? $this->sessao->id_usuario : $dados['id_usuariocadastro'];
                $parametrosMensagem['st_texto'] = $html_mensagem;
                $parametrosMensagem['st_mensagem'] = $dados['st_mensagem'];
                $parametrosMensagem['bl_importante'] = false;
                $retornoSalvaMensagem = $this->salvarMesagem($parametrosMensagem);

                if ($retornoSalvaMensagem->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                    $parametrosEM['id_mensagem'] = $retornoSalvaMensagem->getId();
                    $parametrosEM['id_tipoenvio'] = TipoEnvio::EMAIL;
                    $parametrosEM['id_evolucao'] = Evolucao::TB_ENVIOMENSAGEM_NAO_ENVIADO;
                    $parametrosEM['id_sistema'] = Sistema::GESTOR;
                    $parametrosEM['id_emailconfig'] = $texto->getId_emailconfig()->getId_emailconfig();
                    $retornoEM = $this->salvarEnvioMensagem($parametrosEM);
                    if ($retornoEM->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                        $parametrosED['id_usuario'] = $dados['id_usuario'];
                        $parametrosED['id_tipodestinatario'] = TipoDestinatario::DESTINATARIO;
                        $parametrosED['id_enviomensagem'] = $retornoEM->getId();
                        $retornoED = $this->salvarEnvioDestinatario($parametrosED);

                        if ($retornoED->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                            return $retornoED;
                        }
                    } else {
                        return $retornoEM;
                    }
                } else {
                    return $retornoSalvaMensagem;
                }

            } else {
                return new \Ead1_Mensageiro('Não foi encontrado nenhum texto sistema!', \Ead1_IMensageiro::AVISO);
            }

            return new \Ead1_Mensageiro('Email Enviado com sucesso!', \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @throws \Zend_Exception
     * Gera as matriculas sem contrato vinculado
     */
    public function matriculaSemContrato()
    {
        try {
            $dadosMensagem = array();
            $dadosMensagem['id_entidade'] = $this->sessao->id_entidade;
            $dadosMensagem['result'] = $this->em->getRepository('\G2\Entity\VwMatricula')->retornarUsuariosSemMatriculaVinculada();
            echo "Buscou as matriculas sem vinculo<br>";

            if ($dadosMensagem['result']) {
                $salvarMensagem = $this->salvarMensagemContratoSemMatriculaVinculada($dadosMensagem);
                echo "Salvou Mensagem Contrato Sem Matricula Vinculada <br>";

                //este trecho gera apenas uma notificação interna
                $dadosEnvioMensagem['id_mensagem'] = $salvarMensagem->getId();
                $dadosEnvioMensagem['id_sistema'] = Sistema::GESTOR;
                $dadosEnvioMensagem['id_emailconfig'] = 3;
                $dadosEnvioMensagem['id_tipoenvio'] = TipoEnvio::HTML;
                $dadosEnvioMensagem['id_evolucao'] = Evolucao::TB_ENVIOMENSAGEM_ENVIADO;

                $salvarEM = $this->salvarEnvioMensagem($dadosEnvioMensagem);
                echo "Salvou Envio Mensagem <br>";


                //o ID notificação é fixo, pois representa uma notificação específica da ORGANIZAÇÃO->Notificações
                $selectNotificacaoEntidade = $this->findBy('\G2\Entity\NotificacaoEntidade', array('id_notificacao' => Notificacao::CONTRATO_SEM_MATRICULA_VINCULADA));
                echo "Buscou as entidades a serem notificadas <br>";

                if (is_array($selectNotificacaoEntidade) && !empty($selectNotificacaoEntidade)) {

                    foreach ($selectNotificacaoEntidade as $row) {
                        if ($salvarEM->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $dadosEnvioDestinatario['id_enviomensagem'] = $salvarEM->getId();
                            $dadosEnvioDestinatario['id_notificacaoentidade'] = $row->getId_notificacaoentidade();
                            $dadosEnvioDestinatario['id_entidade'] = $row->getId_entidade();

                            $retornoUsuarioEnviar = $this->pesquisaNotificacaoEntidadePerfil($dadosEnvioDestinatario);
                            echo "Pesquisa Notificacao Entidade Perfil <br>";

                            if ($retornoUsuarioEnviar->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                                return new \Ead1_Mensageiro('Erro ao salvar a mensagem para os usuários', \Ead1_IMensageiro::ERRO);
                            }
                        } else {
                            echo "Não Pesquisou Notificacao Entidade Perfil <br>";
                        }

                        if ($row->getBl_enviaparaemail() == true) {
                            $dadosEnvioMensagem['id_sistema'] = Sistema::GESTOR;

                            $emailConfig = $this->findOneBy('\G2\Entity\EmailConfig', array('id_entidade' => $row->getId_entidade()));

                            if (!$emailConfig->getId_emailconfig()) {
                                return new \Ead1_Mensageiro('Não exite serviço de e-mail configurado para esta entidade', \Ead1_IMensageiro::AVISO);
                            }
                            $dadosEnvioMensagem['id_emailconfig'] = $emailConfig->getId_emailconfig();
                            $dadosEnvioMensagem['id_tipoenvio'] = TipoEnvio::EMAIL;
                            $dadosEnvioMensagem['id_evolucao'] = Evolucao::TB_ENVIOMENSAGEM_NAO_ENVIADO;
                            $salvarEM2 = $this->salvarEnvioMensagem($dadosEnvioMensagem);
                            echo "Salvar Envio Mensagem Perfil <br>";

                            if ($salvarEM2->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                                $dadosEnvioDestinatario['id_enviomensagem'] = $salvarEM2->getId();
                                $dadosEnvioDestinatario['id_notificacaoentidade'] = $row->getId_notificacaoentidade();
                                $dadosEnvioDestinatario['id_entidade'] = $row->getId_entidade();

                                $retornoUsuarioEnviar = $this->pesquisaNotificacaoEntidadePerfil($dadosEnvioDestinatario);
                                echo "Pesquisa Notificacao Entidade Perfil <br>";

                                if ($retornoUsuarioEnviar->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                                    return new \Ead1_Mensageiro('Erro ao salvar a mensagem para os usuários', \Ead1_IMensageiro::ERRO);
                                }
                            } else {
                                echo "Não Pesquisou Notificacao Entidade Perfil <br>";
                            }

                        } else {
                            echo "Não Salvaou Envio Mensagem Perfil <br>";
                        }
                    }
                }

                echo "Envio gerado com sucesso <br>";
                return new \Ead1_Mensageiro('Envio gerado com sucesso!', \Ead1_IMensageiro::SUCESSO);
            } else {
                echo "Não existe matriculas sem vinculo no período.<br>";
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


    protected $dadosNotificacaoErro = array(
        'nome' => 'Felipe Toffolo',
        'email' => 'felipe.toffolo@unyleya.com.br'
    );

    /**
     * @param array $envioMensagem
     * @param array $envioMensagem
     * @throws \Exception
     */
    public function notificarErroEnvioMensagem(array $envioMensagem)
    {
        try {
            //busca os dados da vw_enviomensagem
            $result = $this->findOneBy('G2\Entity\VwEnvioMensagem', array(
                'id_enviomensagem' => $envioMensagem['id_enviomensagem']
            ));

            if ($result) {
                $strMensagem = "<p>Houve um erro ao enviar enviar " . $result->getSt_tipoenvio() . ".</p>";
                $strMensagem .= "<p>Id Envio mensagem: " . $result->getId_enviomensagem() . "</p>";
                $strMensagem .= "<p>Nome usuário: " . $result->getSt_nome() . "</p>";
                $strMensagem .= "<p>E-mail usuário: " . $result->getSt_endereco() . "</p>";
                $strMensagem .= "<p>Data tentativa: " . $result->getDt_tentativa() . "</p>";
                $strMensagem .= "<p>Erro: " . $envioMensagem['st_retorno'] . "</p>";


//                $emailconfig = new EmailConfig();
//                $emailconfig->alteraStatusConfiguracao($result->getId_emailconfig(), true);

//                $mail = new \Zend_Mail();
//                $mail->setBodyHtml($strMensagem)
//                    ->setFrom($this->dadosNotificacaoErro['email'], $this->dadosNotificacaoErro['nome'])
//                    ->addTo($this->dadosNotificacaoErro['email'], $this->dadosNotificacaoErro['nome'])
//                    ->setSubject("G2S - Erro ao enviar " . $result->getSt_tipoenvio())
//                    ->send();
            }
        } catch (\Exception $e) {
            throw new \Exception("Erro ao notificar erro no envio da mensagem. " . $e->getMessage());
        }
    }

    /**
     * @param $idEmailConfig
     * @return bool
     * @throws \Exception
     */
    public function verificaCofiguracaoEmail($idEmailConfig)
    {
        try {
            $emailConfig = $this->find('\G2\Entity\EmailConfig', $idEmailConfig);

            $smtp = $emailConfig->getSt_smtp();
            $conta = $emailConfig->getSt_conta();
            $senha = $emailConfig->getSt_senha();
            $portaSmtp = $emailConfig->getNu_portasaida();
            $portaPop = $emailConfig->getNu_portaentrada();

            /*
             * Retirando, não é necessário
             */
//            $verificacaoPopImap = $this->verificaPop(array(
//                'st_pop' => $emailConfig->getSt_pop(),
//                'st_imap' => $emailConfig->getSt_imap()
//            ), $portaPop, $conta, $senha);
            $verificacaoPopImap = true;


            if ($emailConfig->getId_tipoconexaoemailsaida() && $emailConfig->getId_tipoconexaoemailsaida()->getId_tipoconexaoemail() == 1) {
                $tipoconexaoemailsaida = 'ssl';
            } elseif ($emailConfig->getId_tipoconexaoemailsaida() && $emailConfig->getId_tipoconexaoemailsaida()->getId_tipoconexaoemail() == 2) {
                $tipoconexaoemailsaida = 'tls';
            }

            $verificaSmtp = $this->verificaSmtp($smtp, $conta, $senha, $tipoconexaoemailsaida, $portaSmtp);

            if ($verificacaoPopImap && $verificaSmtp) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            throw new  \Exception("Erro ao tentar verificar configuração de e-mail. " . $e->getMessage());
        }
    }

    /**
     * Verifica conexão pop/imap
     * @param array $host
     * @param $portaPop
     * @param $conta
     * @param $senha
     * @return bool
     * @throws \Exception
     */
    private function verificaPop(array $host, $portaPop, $conta, $senha)
    {
        try {
            if ($host['st_pop']) {
                $pop = new \Zend_Mail_Storage_Pop3(array(
                    'host' => $host['st_pop'],
                    'port' => $portaPop,
                    'user' => $conta,
                    'password' => $senha
                ));
                if ($pop) {
                    return true;
                } else {
                    return false;
                }
            } elseif ($host['st_imap']) {
                $mail = new \Zend_Mail_Storage_Imap(array(
                    'host' => $host['st_imap'],
                    'port' => $portaPop,
                    'user' => $conta,
                    'password' => $senha
                ));
                if ($mail) {
                    return true;
                } else {
                    return false;
                }
            }

        } catch (\Exception $e) {
            throw new \Exception("Configurações de POP/IMAP inválidas. (" . $e->getMessage() . ")");
        }
    }

    /**
     * Verifica configuração smtp
     * @param $smtp
     * @param $conta
     * @param $senha
     * @param $tipoconexaoemailsaida
     * @param $portaSmtp
     * @return bool
     * @throws \Exception
     */
    private function verificaSmtp($smtp, $conta, $senha, $tipoconexaoemailsaida, $portaSmtp)
    {
        try {
            $config = array(
                'auth' => 'login',
                'username' => $conta,
                'password' => $senha,
                'ssl' => $tipoconexaoemailsaida,
                'port' => $portaSmtp
            );

            $smtp = new \Zend_Mail_Transport_Smtp($smtp, $config);
            if ($smtp) {
                return true;
            } else {
                return false;
            }

        } catch (\Exception $e) {
            throw new \Exception("Configurações de SMTP inválidas. (" . $e->getMessage() . ")");
        }
    }

}

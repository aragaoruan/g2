<?php
/**
 * Classe Negocio para Holding
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Negocio;

use Doctrine\ORM\ORMException;
use G2\Entity\VwEntidadesMesmaHolding;
use G2\G2Entity;

class Holding extends Negocio
{
    private $holding;
    private $holdingFiliada;

    public function __construct()
    {
        parent::__construct();
        $this->holding = 'G2\Entity\Holding';
        $this->holdingFiliada = 'G2\Entity\HoldingFiliada';
    }

    /**
     * Busca a holding com o Id
     * @param $id
     * @return \Ead1_Mensageiro
     */
    public function findByHoldingId($id)
    {
        try {
            $result = $this->findBy($this->holding, array('id_holding' => $id));
            if ($result) {
                $arrReturn = $this->entitySerialize($result);
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Busca as Holdins para a tela de Holdings G2
     * @param array $params
     * @throws \Zend_Exception
     */
    public function retornarHolding(array $params = array())
    {
        try {
            //pega os parametros e remove algumas chaves que não precisaria
            unset($params['to'], $params['txtSearch'], $params['grid']);

            $params['id_entidadematriz'] = $this->sessao->id_entidade;
            $result = $this->em->getRepository('\G2\Entity\Holding')->retornarHolding($params);

            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna todas as holdings contidas na tb_holdings para exibição na tela
     * Organização > Cadastros > Holdings Filiadas. Além disso, verifica se há
     * relacionamentos de cada uma delas na entidade atual e adiciona campos
     * indicativos, caso positivo.
     * @throws \Zend_Exception
     */
    public function retornarHoldingFiliada()
    {
        try {
            $result = $this->findAll($this->holding);
            $arrResult = $this->entitySerialize($result);
            foreach ($arrResult as &$value) {
                $id_entidade = $this->sessao->id_entidade;
                $relacionamento = $this->findOneBy($this->holdingFiliada, array(
                    'id_holding' => $value['id_holding'],
                    'id_entidade' => $id_entidade
                ));
                $value['id_holdingfiliada'] = $relacionamento ? $relacionamento->getId_holdingfiliada() : '';
                $value['bl_emiterelatorioholding'] = $relacionamento ? $relacionamento->getBl_emiterelatorioholding() : true;
                $value['bl_ativo'] = $relacionamento ? 1 : 0;
            }
            return $arrResult;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Salva Holding
     * @param array $data
     * @throws \Zend_Exception
     */

    public function salvarHolding(array $data)
    {
        try {
            $id_usuariocadastro = \Ead1_Sessao::getSessaoGeral()->id_usuario;

            if (isset($data)) {
                //abre transação
                $this->beginTransaction();
                $situacaoNegocio = new Situacao();
                //verifica se existe alguma holding com o mesmo nome
                $verificaNome = $this->findOneBy('G2\Entity\Holding', array('st_holding' => $data['st_holding']));

                if (!$verificaNome) {
                    /* Verifica se é uma alteração ou cadastro*/
                    if (empty($data['id_holding'])) {
                        $entity = new \G2\Entity\Holding();
                        /* Salva os dados enviados */
                        $entity->setSt_holding($data['st_holding']);
                        $entity->setDt_cadastro(new \DateTime());
                        $entity->setId_situacao($situacaoNegocio->getReference($data['id_situacao']));
                        $entity->setId_usuariocadastro($id_usuariocadastro);
                        $entity->setBl_compartilharcarta($data['bl_compartilharcarta']);
                        $result = $this->save($entity);
                    } else {
                        $edit = $this->findOneBy('G2\Entity\Holding', array('id_holding' => $data['id_holding']));
                        $edit->setSt_holding($data['st_holding']);
                        $edit->setId_situacao($situacaoNegocio->getReference($data['id_situacao']));
                        $edit->setBl_compartilharcarta($data['bl_compartilharcarta']);
                        $result = $this->save($edit);
                    }
                    $this->commit(); //commit
                } else {
                    $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NOME_DUPLICADO, \Ead1_IMensageiro::ERRO);
                    return $mensageiro;
                }
            }

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Salva Holding Filiada
     * @param array $data
     * @throws \Zend_Exception
     */

    public function salvarHoldingFiliada(array $data)
    {
        try {
            $id_usuariocadastro = \Ead1_Sessao::getSessaoGeral()->id_usuario;
            $id_entidade = $this->sessao->id_entidade;

            if (isset($data)) {
                //abre transação
                $this->beginTransaction();
                /* Verifica se é uma alteração ou cadastro*/
                if (empty($data['id_holdingfiliada'])) {
                    $entity = new \G2\Entity\HoldingFiliada();
                    /* Salva os dados enviados */
                    $entity->setId_holding($this->getReference('\G2\Entity\Holding', $data['id_holding']));
                    $entity->setId_entidade($id_entidade);
                    $entity->setDt_cadastro(new \DateTime());
                    $entity->setId_situacao(\G2\Constante\Situacao::TB_HOLDING_ATIVO);
                    $entity->setId_usuariocadastro($id_usuariocadastro);
                    $entity->setBl_emiterelatorioholding(false);
                    $result = $this->save($entity);
                } else {
                    $holdingfiliada = $this->findOneBy('G2\Entity\HoldingFiliada', array('id_holdingfiliada' => $data['id_holdingfiliada']));
                    $bl_emiterelatorio = $holdingfiliada->getBl_emiterelatorioholding() ? 'true' : 'false';
                    if ($bl_emiterelatorio != $data['bl_emiterelatorio']) {
                        $holdingfiliada->setId_holding($this->getReference('\G2\Entity\Holding', $data['id_holding']));
                        $holdingfiliada->setId_entidade($id_entidade);
                        $holdingfiliada->setDt_cadastro(new \DateTime());
                        $holdingfiliada->setId_situacao(\G2\Constante\Situacao::TB_HOLDING_ATIVO);
                        $holdingfiliada->setId_usuariocadastro($id_usuariocadastro);
                        $holdingfiliada->setBl_emiterelatorioholding($data['bl_emiterelatorio']);
                        $result = $this->save($holdingfiliada);
                    } else {
                        $this->desvincularHoldingFiliada($data['id_holdingfiliada']);
                    }
                }
                $this->commit(); //commit
            }

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna as entidades em que o usuário está de acordo com a holding
     */
    public function retornaUsuarioHoldingImportacao()
    {
        return $this->em->getRepository('\G2\Entity\Holding')->retornaHoldingEntidade(array('id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * Método para apagar a relação da holding filiada com a entidade.
     * @param $id
     * @return bool
     * @throws \Zend_Exception
     */
    public function desvincularHoldingFiliada($id)
    {
        try {

            //verifica se foi passado o id
            if (!$id) {
                throw new \Exception("Informe o id_holdingfiliada");
            }

            //busca o registro
            $entity = $this->findOneBy('\G2\Entity\HoldingFiliada', array('id_holdingfiliada' => $id));

            //verifica se encontrou o registro
            if ($entity) {
                //remove o registro
                return $this->delete($entity);
            }

            return false;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar desvincular a Holding Filiada. " . $e->getMessage());
        }
    }

    /**
     * Retorna as entidades disponíveis na mesma holding.
     * @param int|int[] $id_entidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarEntidadesHolding($id_entidade = null)
    {
        return $this->findBy('G2\Entity\VwEntidadesMesmaHolding', array(
            'id_entidade' => $id_entidade ?: $this->sessao->id_entidade
        ), array(
            'st_nomeentidadeparceira' => 'ASC'
        ));
    }

    /** Retorna perfis que o usuário logado possui em outras entidades da
     * mesma holding com o mesmo id_perfil.
     * @param $params ['id_usuario'] - ID do usuário logado.
     * @param $params ['id_perfil'] - ID do perfil do usuário logado.
     * @return array
     * @throws \Zend_Exception
     * */

    public function retornarPerfisUsuarioEntidadesMesmaHolding($params)
    {
        try {

            $entidadesMesmaHolding = $this->retornarEntidadesHolding();

            $arrEntidades = array_map(function ($item) {
                return $item->getId_entidadeparceira();
            }, $entidadesMesmaHolding);

            $perfis = $this->findBy('G2\Entity\VwUsuarioPerfilEntidade', array(
                'id_usuario' => $params['id_usuario'],
                'id_perfil' => $params['id_perfil'],
                'id_entidade' => $arrEntidades,
            ), array('st_nomeentidade' => 'ASC'));

            if (!empty($perfis)) {
                return $this->toArrayEntity($perfis);
            }
            return $perfis;

        } catch (\Exception $e) {
            $msg = "Não foi possível consultar os perfis do usuário nas entidades de mesma holding.";
            throw new \Zend_Exception($msg . $e->getMessage());
        }

    }
}

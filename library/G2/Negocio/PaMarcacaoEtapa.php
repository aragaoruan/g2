<?php

namespace G2\Negocio;

/**
 * Classe de negócio para PaMarcacaoEtapa
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */
class PaMarcacaoEtapa extends Negocio
{
      /**
     * @var string
     */
      private $repositoryName = '\G2\Entity\PaMarcacaoEtapa';

    /**
     * Busca a marcacao de etapa de acordo com os parametros enviados.
     * Caso necessario que o retorno seja em array, envie true no segundo parametros
     *
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|Object
     * @throws \Zend_Exception
     */
      public function retornaPaMarcacaoEtapa($dados, $array = false)
      {
            $result = $this->findOneBy($this->repositoryName, $dados);

            if ( !is_null($result) ) {
                    if ( $array ) {
                        $result = $this->toArrayEntity($result);
                        return $result;
                    }
                    return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Entidade Tag
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-01-17
 */
class Carreira extends Negocio
{

    /**
     * String repository name 
     * @var string 
     */
    private $repositoryName = 'G2\Entity\Carreira';
    private $mensageiro;

    public function __construct ()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Método findBy carreira
     * @param array $params
     * @return G2\Entity\Carreira
     */
    public function findByCarreira (array $params = array())
    {
        return $this->findBy($this->repositoryName, $params);
    }

    /**
     * Salva e/ou Edita carreira
     * @param array $data
     * @return \G2\Entity\Carreira
     * @throws \Zend_Exception
     */
    public function salvarCarreira (array $data)
    {
        try {
            $entity = $this->verificaCarreiraExiste($data);
            if (!$entity) {
                $entity = new \G2\Entity\Carreira();
            }

            //verifica se o st_tag veio preenchido para salvar
            if (!isset($data['st_carreira'])) {
                throw new \Zend_Exception("st_carreira é de preenchimento obrigatório!");
            } else {
                $entity->setSt_carreira($data['st_carreira']);
            }
            $entity->setDt_cadastro(new \DateTime($entity->getDt_cadastro()));
            $return = $this->save($entity); //salva
            return $this->mensageiro->setMensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * 
     * @param array $data
     * @return boolean
     */
    public function verificaCarreiraExiste (array $data)
    {
        if (isset($data['id_carreira'])) {
            $result = $this->find($this->repositoryName, $data['id_carreira']);
        } else {
            $result = $this->em->getRepository($this->repositoryName)->findOneBy($data);
        }
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

}
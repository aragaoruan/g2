<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Tipo de Tramite
 * @author Rafael Leite  <rafael.leite@unyleya.com.br>
 */
class TipoTramite extends Negocio
{
    public function __construct ()
    {
        parent::__construct();
    }

    public function findTipoTramiteByIdCategoria ($id_categoriatramite)
    {
        return $this->findBy('\G2\Entity\TipoTramite',array('id_categoriatramite' => $id_categoriatramite));
    }


}
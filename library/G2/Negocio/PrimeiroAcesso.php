<?php

namespace G2\Negocio;

use G2\Constante\TipoOcorrencia;
use G2\Entity\PABoasVindas;

/**
 * Classe de negócio para Area Conhecimento
 * @author rafael.rocha  <rafael.rocha@unyleya.com.br>
 */
class PrimeiroAcesso extends Negocio
{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Buscar pela Entidade da Sessão
   * @return object G2\Entity\PABoasVindas
   */
  public function findPABoasVindasByEntidadeSessao()
  {
    $params['id_entidade'] = $this->sessao->id_entidade;
    return $this->em->getRepository('G2\Entity\PABoasVindas')->findOneBy($params);
  }

  /**
   * Buscar pela Entidade da Sessão
   * @return object G2\Entity\PADadosContato
   */
  public function findPADadosContatoByEntidadeSessao()
  {
    $params['id_entidade'] = $this->sessao->id_entidade;
    return $this->em->getRepository('G2\Entity\PADadosContato')->findOneBy($params);
  }

  /**
   * Buscar pela Entidade da Sessão
   * @return object G2\Entity\PADadosCadastrais
   * @deprecated  user o metodo findPADadosCadastrais()
   */
  public function findPADadosCadastraisByEntidadeSessao()
  {
    $params['id_entidade'] = $this->sessao->id_entidade;
    return $this->em->getRepository('G2\Entity\PADadosCadastrais')->findOneBy($params);
  }

  /**
   * Buscar pela Entidade da Sessão
   * @return object G2\Entity\PAMensagemInformativa
   */
  public function findPAMensagemInformativaByEntidadeSessao()
  {
    $params['id_entidade'] = $this->sessao->id_entidade;
    return $this->em->getRepository('G2\Entity\PAMensagemInformativa')->findOneBy($params);
  }

  /**
   * Buscar pelos assuntos do Primeiro Acesso
   * @return json
   */
  public function findAssuntosJSON()
  {

    $objs = $this->em->createQuery('SELECT u FROM \G2\Entity\VwAssuntos u WHERE '
        . 'u.id_entidade = :id_entidade '
        . ' AND u.id_tipoocorrencia = :id_tipoocorrencia '
        . ' AND u.id_assuntocopai IS NULL '
        . ' AND u.bl_ativo = 1')
        ->setParameter('id_entidade', $this->sessao->id_entidade)
        ->setParameter('id_tipoocorrencia', TipoOcorrencia::ALUNO)
        ->getResult();

    $arrayRetorno[] = array('id' => '', 'text' => 'Selecione um assunto');

    if ($objs) {
      foreach ($objs as $obj) {
        $arrayRetorno[] = array(
            'id' => $obj->getId_assuntoco(),
            'text' => $obj->getSt_assuntoco()
        );
      }
    }
    return json_encode($arrayRetorno);
  }

  /**
   * Buscar pelos assuntos do Primeiro Acesso
   * @return json
   */
  public function findSubAssuntosJSON($id_pai)
  {

    $objs = $this->em->createQuery('SELECT u FROM \G2\Entity\VwAssuntos u WHERE '
        . 'u.id_entidade = :id_entidade '
        . ' AND u.id_tipoocorrencia = :id_tipoocorrencia '
        . ' AND u.id_assuntocopai = :id_assuntocopai '
        . ' AND u.bl_ativo = 1')
        ->setParameter('id_entidade', $this->sessao->id_entidade)
        ->setParameter('id_tipoocorrencia', 1)
        ->setParameter('id_assuntocopai', $id_pai)
        ->getResult();

    $arrayRetorno[] = array('id' => '', 'text' => 'Selecione um subassunto');

    if ($objs) {
      foreach ($objs as $obj) {
        $arrayRetorno[] = array(
            'id' => $obj->getId_assuntoco(),
            'text' => $obj->getSt_assuntoco()
        );
      }
    }
    return json_encode($arrayRetorno);
  }

  /**
   * Buscar pelos assuntos do Primeiro Acesso
   * @return json
   */
  public function findCategoriaOcorrenciaJSON()
  {

    $objs = $this->em->getRepository('\G2\Entity\CategoriaOcorrencia')->findBy(
        array(
            'id_entidadecadastro' => $this->sessao->id_entidade,
            'bl_ativo' => 1
        )
    );

    $arrayRetorno[] = array('id' => '', 'text' => 'Selecione uma categoria');

    if ($objs) {
      foreach ($objs as $obj) {
        $arrayRetorno[] = array(
            'id' => $obj->getId_categoriaocorrencia(),
            'text' => $obj->getSt_categoriaocorrencia()
        );
      }
    }
    return json_encode($arrayRetorno);
  }

  /**
   * Update data
   * @param array $data
   * @return G2\Entity\AreaConhecimento
   *
   */
  public function updatePABoasVindas($data)
  {

    $textoSistema = $this->em->getRepository('G2\Entity\TextoSistema')->getReference($data['id_textosistema']);
    $situacao = $this->situacaoNegocio->getReference($data['id_situacao']);
    $entidade = $this->entidadeNegocio->getReference($this->sessao->id_entidade);
    $usuario = $this->em->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
    $tipoareaconhecimento = $this->em->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);

    $boasVindas = $this->getReference($data['id']);

    $boasVindas->setSt_areaconhecimento($data['st_areaconhecimento'])
        ->setId_situacao($situacao)
        ->setId_tipoareaconhecimento($tipoareaconhecimento)
        ->setId_usuariocadastro($usuario)
        ->setSt_tituloexibicao($data['st_tituloexibicao'])
        ->setSt_descricao($data['st_descricao'])
        ->setBl_ativo(true)
        ->setId_entidade($entidade);

    $this->em->merge($boasVindas);
    $this->em->flush();

    $this->areaEntidadeNegocio->deletarAreaEntidade($data['id']);
    $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $data['id']);

    return $boasVindas;
  }

  /**
   *
   * @param array $data
   * @return \G2\Entity\AreaConhecimento
   */
  public function salvarAreaConhecimento(array $data)
  {
    try {

      if (empty($data['entidades'])) {
        return array('title' => 'Atenção!',
            'text' => 'Selecione pelo menos uma organização!');
      }
      //   echo $data['id_situacao'];die;
      $situacao = $this->situacaoNegocio->getReference($data['id_situacao']);
      $entidade = $this->entidadeNegocio->getReference($this->sessao->id_entidade);
      $usuario = $this->em->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
      $tipoareaconhecimento = $this->em->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);


      if (isset($data['id'])) {
        //se existir o id, busca o registro
        $entity = $this->find($this->repositoryName, $data['id']);
      } else {
        //se não existir o id cria um novo registro
        $entity = new AreaConhecimentoEntity();
      }
      //seta os atributos
      $entity->setSt_areaconhecimento($data['st_areaconhecimento'])
          ->setId_situacao($situacao)
          ->setId_tipoareaconhecimento($tipoareaconhecimento)
          ->setId_usuariocadastro($usuario)
          ->setDt_cadastro(new \DateTime())
          ->setSt_tituloexibicao($data['st_tituloexibicao'])
          ->setSt_descricao($data['st_descricao'])
          ->setBl_ativo(1)
          ->setId_entidade($entidade)
          ->setId_areaconhecimentopai(null);

      $retorno = $this->save($entity);

      //Vincula com a entidade
      if ($retorno->getId()) {
        $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $retorno->getId());
      }

      return array(
          'id' => $retorno->getId(),
          'type' => 'success',
          'title' => 'Salvo com Sucesso',
          'text' => 'O registro foi salvo com sucesso!'
      );
    } catch (Exception $e) {
      return array(
          'type' => 'error',
          'title' => 'Erro',
          'text' => 'Erro ao salvar Área Conhecimento: ' . $e->getMessage()
      );
    }
  }

  /**
   *
   * @param integer $id
   * @return object
   */
  public function deletarBoasVindas($id)
  {

    $boasVindas = $this->em->getReference('G2\Entity\PABoasVindas', $id);
    $boasVindas->setBl_ativo(false);
    $this->em->merge($boasVindas);
    $this->em->flush();

    return $boasVindas;
  }

  public function salvarPrimeiroAcesso($id_matricula)
  {
    $matriculaNegocio = new Matricula();
    $primeiroAcessoBo = new \PrimeiroAcessoBO();

    $matricula = $matriculaNegocio->getRepository('\G2\Entity\Matricula')->retornaVendaMatricula(array('id_matricula' => $id_matricula));
    $id_venda = $matricula['id_venda'];

    $marcacaoEtapaTO = new \PAMarcacaoEtapaTO();

    $marcacaoEtapaTO->setId_venda($id_venda);
    $marcacaoEtapaTO->setBl_boasvindas(true);
    $marcacaoEtapaTO->setBl_aceitacaocontrato(true);
    $marcacaoEtapaTO->setBl_dadoscadastrais(true);
    $marcacaoEtapaTO->setBl_mensageminformativa(true);
    $marcacaoEtapaTO->setDt_aceita(new \Zend_Date());

    $primeiroAcessoBo->cadastrarMarcacaoEtapa($marcacaoEtapaTO);

    $path = "{$_SERVER['DOCUMENT_ROOT']}" . DIRECTORY_SEPARATOR . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . "minhapasta" . DIRECTORY_SEPARATOR . $id_venda . DIRECTORY_SEPARATOR;
    if (!is_dir($path)) {
      mkdir($path, 0777, true);
    }

    return $primeiroAcessoBo->gerarPDFContrato($id_venda);
  }

  /**
   * Metodo responsavel por retornar apenas os dados da tb_PADadosCadastrais. Se enviar o parametro id_entidade
   * ele vai considerar, caso nao exista vai pesquisar o id_entidade da sessao
   * Caso tenha necessidade de retornar os dados em array basta enviar o param true,
   *
   * @param $dados
   * @param bool $array
   * @return array|\Ead1_Mensageiro|null|object
   */
  public function findPADadosCadastrais($dados, $array = false)
  {
    if (!isset($dados['id_entidade']) && empty($dados['id_entidade'])) {
      $dados['id_entidade'] = $this->sessao->id_entidade;
    }
    $result = $this->em->getRepository('G2\Entity\PADadosCadastrais')->findOneBy($dados);
    if ($result) {
      if ($array) {
        $result = $this->toArrayEntity($result);
        return $result;
      }
      return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
    }
    return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
  }

  /**
   * Retona um array com os dados da ocorrencia relacionada ao primeiro acesso
   * E necessario enviar o id_matricula, id_entidade
   *
   * @param $params
   * @return bool
   */
  public function retornaOcorrenciaPrimeiroAcesso($params)
  {
    $ocorrenciaNegocio = new \G2\Negocio\Ocorrencia();
    $paDadosCadastrais = $this->findPADadosCadastrais(array('id_entidade' => $params['id_entidade']), true);

    $whereOcorrencia = array(
        'id_assuntoco' => $paDadosCadastrais['id_assuntoco']['id_assuntoco'],
        'id_entidade' => $params['id_entidade'],
        'id_matricula' => $params['id_matricula'],
    );

    $returnOcorrencia = $ocorrenciaNegocio->retornaOcorrencia($whereOcorrencia, true);

    $ocorrenciaAberta = array_values(array_filter($returnOcorrencia, function ($arrayValue) {
      return $arrayValue['id_situacao']['id_situacao'] != \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA;
    }));

    //caso tenha alguma ocorrencia retorna os dados para utilizaçao
    if (isset($ocorrenciaAberta[0]) && !empty($ocorrenciaAberta[0])) {
      return $ocorrenciaAberta[0];
    }
    return false;
  }
}

<?php

namespace G2\Negocio;
use G2\Constante\TipoPessoa;
use G2\Entity\AplicadorProvaEntidade;
use G2\Entity\Endereco;
use G2\Entity\PessoaEndereco;
use G2\Entity\VwEntidadeEndereco;


/**
 * Classe de negócio para Aplicadores de prova
 * @author Denise Xavier  <denise.xavier@unyleya.com.br>
 */
class AplicadorProva extends Negocio {

    public function __construct() {
        parent::__construct();
    }


    public function salvar(array $paramns){
        try {
            if (empty($paramns['entidades'])) {
                return array('title' => 'Atenção!',
                    'text' => 'Selecione pelo menos uma organização!');
            }

            if ($paramns['id_tipopessoa'] == 0) {
                return new \Ead1_Mensageiro('Escolha um tipo pessoa!', \Ead1_IMensageiro::AVISO);

            }

            //st_aplicadorprova
            if (empty($paramns['st_aplicadorprova'])) {
                return new \Ead1_Mensageiro('O nome do aplicador é obrigatório e não foi passado!', \Ead1_IMensageiro::AVISO);
            }

            if($paramns['id_tipopessoa'] == TipoPessoa::FISICA && $paramns['id_usuarioaplicador'] == ''){
                return new \Ead1_Mensageiro('Escolha uma pessoa para o aplicador!', \Ead1_IMensageiro::AVISO);
            }

            if($paramns['id_tipopessoa'] == TipoPessoa::FISICA){
                $endereco = $this->validaEnderecoAplicador($paramns['endereco']);
                if(!$endereco){
                    return new \Ead1_Mensageiro('Cadastre um endereço para o aplicador pessoa física!', \Ead1_IMensageiro::AVISO);
                }
            }elseif($paramns['id_tipopessoa'] == TipoPessoa::JURIDICA){
                //verifica se o a entidade passada tem endereço cadastrado
                $enderecoEntidade = $this->validaEnderecoAplicadorJuridico($paramns['id_entidadeaplicador']);
                if(!$enderecoEntidade){
                    return new \Ead1_Mensageiro('Essa entidade não possui um endereço PADRÃO completo. Complete o cadastro da organização antes de proesseguir!', \Ead1_IMensageiro::AVISO);
                }
            }



            //Verifica se existe nomes iguais
            $aux = $this->findBy('\G2\Entity\AplicadorProva', array('st_aplicadorprova' => $paramns['st_aplicadorprova']));
            if(!empty($aux)):
                $entityAuxiliar = $aux[0];
                if($entityAuxiliar instanceof \G2\Entity\AplicadorProva && $entityAuxiliar->getId_aplicadorprova()){
                    if( (isset($paramns['id']) && $paramns['id'] != $entityAuxiliar->getId_aplicadorprova()) || !isset($paramns['id'])){
                        return array('title' => 'Atenção!',
                            'text' => 'O nome do aplicador não pode ser repetido! Já existe um registro com esse nome! Id: '.$entityAuxiliar->getId_aplicadorprova());
                    }
                }
            endif;


            if($paramns['id_tipopessoa'] == TipoPessoa::FISICA){
                $id_usuarioaplicador = $this->getReference('G2\Entity\Usuario', $paramns['id_usuarioaplicador']);
            }else{
                $id_usuarioaplicador = 0;
            }

            if($paramns['id_tipopessoa'] == TipoPessoa::JURIDICA){
                $id_entidadeaplicador = $this->getReference('G2\Entity\Entidade', $paramns['id_entidadeaplicador']);
            }else{
                $id_entidadeaplicador = 0;
            }
            $entidadeCadastro = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
            $usuarioCadastro = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);


            $is_edit = false;
            if (isset($paramns['id']) && $paramns['id'] != '') {
                //se existir o id, busca o registro
                $is_edit = true;
                $entity = $this->find('\G2\Entity\AplicadorProva', $paramns['id']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\AplicadorProva();
            }

//            $entity = new AplicadorProva();
            //seta os atributos
            $entity->setId_usuariocadastro($usuarioCadastro)
                ->setId_entidadecadastro($entidadeCadastro)
                ->setSt_aplicadorprova($paramns['st_aplicadorprova'])
                ->setDt_cadastro(new \DateTime())
                ->setBl_ativo(1);


            if($paramns['id_tipopessoa'] == TipoPessoa::FISICA){
                $entity->setId_usuarioaplicador($id_usuarioaplicador);
                $entity->setId_entidadeaplicador(null);
            }else{
                $entity->setId_entidadeaplicador($id_entidadeaplicador);
                $entity->setId_usuarioaplicador(null);
            }
            $retorno = $this->save($entity);

            if($is_edit){
                $this->deletarAplicadorProvaEntidade($paramns['id']);
            }

            //Vincula com a entidade
            if ($retorno instanceof \G2\Entity\AplicadorProva && $retorno->getId_aplicadorprova()) {
                $this->salvarAplicadorProvaEntidade($paramns['entidades'], $retorno->getId_aplicadorprova());
            }

            if($paramns['id_tipopessoa'] == TipoPessoa::FISICA ){
               $enderecoAplicador =  $this->salvaAplicadorEndereco($endereco, $entity);
            }
                return new \Ead1_Mensageiro('O aplicador prova foi '.(isset($paramns['id']) && $paramns['id'] != '' ? 'atualizado' : 'salvo').' com sucesso!', \Ead1_IMensageiro::SUCESSO
                            , array('id' => $retorno->getId_aplicadorprova(), 'id_endereco' => (isset($enderecoAplicador) && $endereco instanceof \G2\Entity\Endereco ? $enderecoAplicador->getId_endereco() : '') ));
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar Aplicador Prova: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     *
     * @param array $data
     * @param integer $id_aplicadorprova
     */
    public function salvarAplicadorProvaEntidade (array $data, $id_aplicadorprova )
    {
        try{
            $id_aplicadorprova = $this->getReference('\G2\Entity\AplicadorProva', $id_aplicadorprova);
            foreach($data as $entidade){
                $entity = new AplicadorProvaEntidade();
                $id_entidade = $this->getReference('\G2\Entity\Entidade', $entidade);

                //seta os atributos
                $entity->setId_entidade($id_entidade)
                    ->setId_aplicadorprova($id_aplicadorprova);
                $this->save($entity);
            }

            return array(
                'type' => 'success',
                'title' => 'Sucesso :)',
                'text' => 'Entidades vinculadas com Sucesso!'
            );

        }catch (Exception $e){
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao vincular entidades com o aplicador prova: '.$e->getMessage()
            );
        }


    }

    /**
     *
     * @param integer $id
     * @return object G2\Entity\AplicadorProva
     */
    public function findVwAplicadorProva ($id)
    {
        return parent::find('G2\Entity\VwAplicadorProva', $id);
    }

    /**
     * @param integer $id
     * @return true
     */
    public function deletarAplicadorProvaEntidade ($id_aplicadorprova)
    {
        $ids = $this->em->getRepository('G2\Entity\AplicadorProvaEntidade')->findBy(array('id_aplicadorprova' => $id_aplicadorprova));

        foreach($ids as $id){
            $this->em->remove($id);
            $this->em->flush();
        }

        return true;

    }

    /**
     * Valida campos obrigatorios no endereço
     * Retorna FALSE caso algum campo obrigatorio não tenha sido preenchido ou retorna  ENTITY ENDEREÇO preenchida
     * @param $dados
     * @return bool|Endereco|Doctrine
     */
    public function validaEnderecoAplicador($dados){
        if(array_key_exists('id_endereco', $dados) && $dados['id_endereco'] != ''){
            $entity = $this->getReference('\G2\Entity\Endereco', (int)$dados['id_endereco']);
        }else {
            $entity = new \G2\Entity\Endereco();
        }

        //Pais
        if(array_key_exists('id_pais', $dados)  && $dados['id_pais'] != ''){
            $entity->setId_pais($this->getReference('\G2\Entity\Pais', (int)$dados['id_pais']));
        }else{
            return false;
        }

        //CEP
        if(array_key_exists('st_cep', $dados)  && $dados['st_cep'] != ''){
            $entity->setSt_cep($dados['st_cep']);
        }else{
            return false;
        }

        //Tipo Endereço
        $entity->setId_tipoendereco($this->getReference('\G2\Entity\TipoEndereco', 6));

        //Endereco
        if(array_key_exists('st_endereco', $dados)  && $dados['st_endereco'] != ''){
            $entity->setSt_endereco($dados['st_endereco']);
        }else{
            return false;
        }

        //Número -> Não obrigatorio
        if(array_key_exists('nu_numero', $dados)  && $dados['nu_numero'] != ''){
            $entity->setNu_numero($dados['nu_numero']);
        }

        //Bairro
        if(array_key_exists('st_bairro', $dados)  && $dados['st_bairro'] != ''){
            $entity->setSt_bairro($dados['st_bairro']);
        }else{
            return false;
        }


        //Complemento -> não obrigatorio
        if(array_key_exists('st_complemento', $dados)  && $dados['st_complemento'] != ''){
            $entity->setSt_complemento($dados['st_complemento']);
        }


        //UF
        if(array_key_exists('sg_uf', $dados)  && $dados['sg_uf'] != ''){
           // $entity->setSg_uf($this->getReference('\G2\Entity\Uf', trim($dados['sg_uf'])));
            $entity->setSg_uf(trim($dados['sg_uf']));
        }else{
            return false;
        }


        //Municipio
        if(array_key_exists('id_municipio', $dados)  && $dados['id_municipio'] != ''){
            $entity->setId_municipio($this->getReference('\G2\Entity\Municipio', (int)$dados['id_municipio']));
        }else{
            return false;
        }
        $entity->setBl_ativo(1);
        return $entity;


    }

    /**
     * Método que salva e vincula um endereço de um aplicador
     * @param \G2\Entity\Endereco $endereco
     * @param \G2\Entity\AplicadorProva $aplicador
     * @return string
     */
    public function salvaAplicadorEndereco($endereco, $aplicador){
        try{
            $is_edit = false;
            if($endereco->getId_endereco()){
                $is_edit = true;
            }
            //verifica se ja existe um endereço do tipo aplicador para a pessoa escolhida
            $auxilar = $this->em->getRepository('G2\Entity\VwPessoaEndereco')->findOneBy(array('id_tipoendereco' => 6
                                                                                                , 'id_usuario' =>  $aplicador->getId_usuarioaplicador()->getId_usuario()
                                                                                                , 'id_entidade' => $this->sessao->id_entidade));
            if($auxilar instanceof \G2\Entity\VwPessoaEndereco && $auxilar->getId_endereco()){
                $endereco->setId_endereco($auxilar->getId_endereco());
            }

            if($endereco->getId_endereco()!= ''){
                $this->merge($endereco);
                $is_edit = true;
            }else{
                $this->persist($endereco);
            }
            //$this->save($endereco);
            if($endereco instanceof \G2\Entity\Endereco && $endereco->getId_endereco()){
                if(!$is_edit):
                    $pessoaEndereco = new \G2\Entity\PessoaEndereco();
                    $pessoaEndereco->setBl_padrao(0);
                    $pessoaEndereco->setId_endereco($endereco);
                    $pessoaEndereco->setId_entidade($this->getReference('\G2\Entity\Entidade', (int) $this->sessao->id_entidade));
                    $pessoaEndereco->setId_usuario($aplicador->getId_usuarioaplicador()->getId_usuario());
                    $retornoPessoaEndereco = $this->save($pessoaEndereco);
                else:
                    //se já tiver o endereço, preciso trocar o vinculo do usuario pra o novo aplicador
                    $pessoaEndereco = $this->em->getRepository('G2\Entity\PessoaEndereco')->findOneBy(array('id_endereco' => $endereco->getId_endereco()));
                    if($pessoaEndereco instanceof \G2\Entity\PessoaEndereco){
                        $pessoaEndereco->setId_usuario($aplicador->getId_usuarioaplicador()->getId_usuario());
                        $retornoPessoaEndereco = $this->save($pessoaEndereco);
                    }
                endif;
            }

            return $endereco;

        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * @param $dados
     * @return array
     */
    public function findVwPessoaEnderecoAplicador ($dados)
    {
        try {
            $return = $this->findOneBy('G2\Entity\VwPessoaEndereco', $dados);

            if ($return)
                return $this->toArrayEntity($return);
            else{
                $entity = new \G2\Entity\VwPessoaEndereco();
                return $this->toArrayEntity($entity);
            }


        } catch (\Zend_Exception $e) {
            return $e;
        }
    }


    /**
     * @param array $params
     * @return array
     */
    public function findByVwAplicadorProva(array $params)
    {
        try {
            $params['id_entidadeaplicador'] = $this->sessao->id_entidade;

            return $this->findBy('\G2\Entity\VwAplicadorProva',$params);

        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * @param $id_usuarioaplicador
     * @return bool|Endereco
     */
    public function validaEnderecoAplicadorJuridico($id_usuarioaplicador){
        try{
            $vwEntidadeEndereco = $this->findOneBy('\G2\Entity\VwEntidadeEndereco', array('id_entidade' => $id_usuarioaplicador
                                                                                        , 'bl_enderecoentidadepadrao' => 1
                                                                                        , 'bl_ativo' => 1));
            /* verifica se a entidade a ser cadastrada como aplicador de prova tem endereço padrao completo,
            deve conter st_endereco, id_municipio e sg_uf. Esses campos são necessários para escolher o aplicador na hora de criar
            uma aplicação
            */
            if($vwEntidadeEndereco instanceof \G2\Entity\VwEntidadeEndereco && $vwEntidadeEndereco->getId_entidadeendereco()){
                if($vwEntidadeEndereco->getSt_endereco() && $vwEntidadeEndereco->getSg_uf() && $vwEntidadeEndereco->getId_municipio()){
                    return true;
                }else{
                    return false;
                }

            }else{
                return false;
            }
        }catch (\Zend_Exception $e){
            return false;
        }

    }

    /**
     * @param $params
     * @param int $id_entidadepai
     * @return \G2\Entity\AplicadorProvaEntidade[]
     * @throws \Zend_Exception
     */
    public function retornarAplicadorEntidadeRecursiva($params, $id_entidadepai = null)
    {
        $repositoryName = '\G2\Entity\AplicadorProvaEntidade';

        $dql = $this->getRepository($repositoryName)
            ->createQueryBuilder('tb')
            ->select('tb')
            ->join('\G2\Entity\AplicadorProva', 'app', 'WITH', 'tb.id_aplicadorprova = app.id_aplicadorprova')
            ->leftJoin('\G2\Entity\Entidade', 'en', 'WITH', 'en.id_entidade = tb.id_entidade');

        if ($id_entidadepai) {
            $dql->where('en.id_entidade = :id_entidadecadastro OR en.id_entidadecadastro = :id_entidadecadastro')
                ->setParameter('id_entidadecadastro', $id_entidadepai);
        }

        foreach ($params as $attr => $val) {
            $dql->andWhere("tb.{$attr} = :{$attr}")
                ->setParameter($attr, $val);
        }

        $dql->orderBy('app.st_aplicadorprova', 'ASC');
        $dql->addOrderBy('tb.id_entidade', 'ASC');

        return $dql->getQuery()->getResult();
    }

}

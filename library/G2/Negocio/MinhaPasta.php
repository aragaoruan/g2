<?php

namespace G2\Negocio;

/**
 * Classe Negocio para minha pasta
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2016-05-01
 */
class MinhaPasta extends Negocio
{

    /**
     * String repository name
     * @var string
     */
    private $repositoryName = 'G2\Entity\MinhaPasta';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados de MinhaPasta
     * @param array $params
     * @return \G2\Entity\MinhaPasta
     */
    public function retornaMinhaPasta(array $params = array())
    {
        try {
            //retorna os bl_ativo, exclusão lógica
            $result = $this->findBy('\G2\Entity\MinhaPasta', $params, array("id_situacao" => "ASC"));
            $arrReturn = array();
            foreach ($result as $key => $row) {
                $dt_cadastro = $row->getDt_cadastro()->format('d/m/Y H:i:s');
                $dt_atualizacao = ($row->getDt_atualizacao()) ? $row->getDt_atualizacao()->format('d/m/Y H:i:s') : '';
                $st_usuarioatualizacao = ($row->getId_usuarioatualizacao()) ? $row->getId_usuarioatualizacao()->getSt_nomecompleto() : '';

                $arrReturn[$key]['id'] = $row->getId_minhapasta();
                $arrReturn[$key]['st_contrato'] = $row->getSt_contrato();
                $arrReturn[$key]['st_contratoUrl'] = $row->getSt_contratourl();
                $arrReturn[$key]['dt_cadastro'] = $dt_cadastro;
                $arrReturn[$key]['dt_atualizacao'] = ($dt_atualizacao) ? $dt_atualizacao : $dt_cadastro;
                $arrReturn[$key]['st_usuariocadastro'] = ($row->getId_usuariocadastro()) ? $row->getId_usuariocadastro()->getSt_nomecompleto() : '';
                $arrReturn[$key]['st_usuarioatualizacao'] = ($st_usuarioatualizacao) ? $st_usuarioatualizacao : ($row->getId_usuariocadastro()) ? $row->getId_usuariocadastro()->getSt_nomecompleto() : '';
                $arrReturn[$key]['id_venda'] = $row->getId_venda()->getId_venda();
                $arrReturn[$key]['st_situacao'] = $row->getId_situacao()->getSt_situacao();
                $arrReturn[$key]['id_situacao'] = $row->getId_situacao()->getId_situacao();
                $arrReturn[$key]['st_evolucao'] = $row->getId_evolucao()->getSt_evolucao();
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * Retorna dados de MinhaPasta
     * @param array $params
     * @return \G2\Entity\MinhaPasta
     */
    public function retornaVwMinhaPasta(array $params = array())
    {
        try {
            //retorna os bl_ativo, exclusão lógica
            $result = $this->findBy('\G2\Entity\VwMinhaPasta', $params, array("dt_acao" => "DESC"));
            $arrReturn = array();
            foreach ($result as $key => $row) {
                $arrReturn[$key]['id_venda'] = $row->getId_venda();
                $arrReturn[$key]['st_contrato'] = $row->getSt_contrato();
                $arrReturn[$key]['st_contratoUrl'] = str_replace(' ', '_', $row->getSt_contrato());
                $arrReturn[$key]['dt_acao'] = $row->getDt_acao()->format('d/m/Y H:i:s');
                $arrReturn[$key]['st_nomecompleto'] = $row->getSt_nomecompleto();
                $arrReturn[$key]['st_evolucao'] = $row->getSt_evolucao();
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param $id
     * @return \Ead1_Mensageiro|null
     */
    public function removerMinhaPasta($id)
    {
        try {
            $uploadPath = realpath(APPLICATION_PATH . "/../public");
            $contrato = $this->find($this->repositoryName, $id);

            $contrato->setId_usuarioatualizacao($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $contrato->setDt_atualizacao(new \DateTime());
            $contrato->setId_evolucao($this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MINHAPASTA_INATIVO));
            $contrato->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MINHAPASTA_INATIVO));

            $this->save($contrato);
            unlink($uploadPath . '/upload/minhapasta/' . $contrato->getId_venda()->getId_venda() . '/' . $contrato->getSt_contratourl());

            return new \Ead1_Mensageiro('O contrato foi excluído com sucesso!', \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Salva o contrato na tb_minhapasta
     *
     * @param $nome_arquivo
     * @param $id_venda
     * @param null $id_matricula
     * @param null $upload
     * @param null $st_contratourl
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarContratoMinhaPasta($nome_arquivo, $id_venda, $id_matricula = null, $upload = null, $st_contratourl = null)
    {
        try {
            if (!$id_venda) {
                throw new \Exception("O id da venda é obrigatório");
            }

            if (!$id_matricula) {
                /** @var \G2\Entity\VendaProduto $vendaproduto */
                $vendaproduto = $this->findOneBy('\G2\Entity\VendaProduto', array('id_venda' => $id_venda));

                //verifica se encontrou o registro no banco
                if (!($vendaproduto instanceof \G2\Entity\VendaProduto)) {
                    throw new \Exception("Venda produto não encontrado.");
                }

                //verifica se existe realmente a mtricula vinculada a venda.
                if (!$vendaproduto->getId_matricula()) {
                    throw new \Exception("Não existe matrícula vinculada a esta venda.");
                }
                $id_matricula = $vendaproduto->getId_matricula()->getId_matricula();
            }

            $minhaPasta = new \G2\Entity\MinhaPasta();
            $matricula = $this->findOneBy('\G2\Entity\Matricula', array('id_matricula' => $id_matricula));

            if ($matricula) {
                $idUsuario = $matricula->getId_usuario()->getId_usuario()
                    ?: $this->sessao->id_usuario;

                $eUpload = ($upload) ? $this->find('\G2\Entity\Upload', $upload) : null;
                $eUsuario = $this->getReference('\G2\Entity\Usuario', $idUsuario);

                $minhaPasta->setId_usuario($eUsuario);
                $minhaPasta->setId_upload($eUpload);
                $minhaPasta->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $idUsuario));
                $minhaPasta->setSt_contrato($nome_arquivo);

                $minhaPasta->setId_venda($this->getReference('\G2\Entity\Venda', $id_venda));
                $minhaPasta->setId_upload($eUpload);

                $minhaPasta->setId_usuariocadastro($eUsuario);

                $minhaPasta->setId_matricula($this->getReference('\G2\Entity\Matricula', $matricula->getId_matricula()));
                $minhaPasta->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MINHAPASTA_ATIVO));
                $minhaPasta->setId_evolucao($this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MINHAPASTA_ADICIONOU));
                $minhaPasta->setBl_ativo(true);
                $minhaPasta->setDt_cadastro(new \DateTime());
                $minhaPasta->setSt_contratourl($st_contratourl);

                $this->save($minhaPasta);

            } else {
                throw new \Exception("Matricula não encontrada");
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para DocumentosObrigatoriedade (tb_documentosobrigatoriedade)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class DocumentosObrigatoriedade extends Negocio
{

    private $repositoryName = 'G2\Entity\DocumentosObrigatoriedade';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_documentosobrigatoriedade' => $row->getId_documentosobrigatoriedade(),
                    'st_documentosobrigatoriedade' => $row->getSt_documentosobrigatoriedade(),
                    'id_usuariocadastro' => $row->getId_usuariocadastro(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'dt_cadastro' => $row->getDt_cadastro()
                );
            }

            return $arrReturn;
        }
    }

}
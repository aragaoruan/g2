<?php

namespace G2\Negocio;

/**
 * Classe de Negocio para VwcategoriaProduto
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 * @since 2015-04-20
 */

class VwCategoriaProduto extends Negocio
{

    private $repositoryName = 'G2\Entity\Produto';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Metodo responsavel por filtrar e retornar lista de produtos relacionadas a categoria
     * @author Neemisa Santos <neemias.santos@unyleya.com.br>
     * @param type $where
     * @return Array VwCategoriaProduto
     */
    public function retornarProdutoCategoriaParams($where)
    {
        try {
            $result = $this->em->getRepository($this->repositoryName)->retornaProdutoCategoriasEntidadeParams($where);
//            $result = null;
//            foreach ($resultFind as $value) {
//                $result[] = $value->entityToArray();
//            }
            return $result;
        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception("Erro ao tentar salvar venda." . $e->getMessage());
            throw new \Zend_Exception($e->getMessage());
        }

    }

} 
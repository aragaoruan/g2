<?php

namespace G2\Negocio;

/**
 * Classe Negocio para DiaSemana
 * Class DiaSemana
 * @package G2\Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-01
 */
class DiaSemana extends Negocio
{
    private $repositoryName = '\G2\Entity\DiaSemana';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($respositoryName = null)
    {
        try {
            return parent::findAll($respositoryName ? $respositoryName : $this->repositoryName);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }
} 
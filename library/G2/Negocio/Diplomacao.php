<?php

namespace G2\Negocio;

use Doctrine\DBAL\DBALException;

/**
 * Negócio para diplomação
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Diplomacao extends Negocio
{

    public $mensageiro;

    public function __contruct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * @param array $params
     * @return bool|null|\G2\Entity\VwMatriculaDiplomacao[]
     */
    public function pesquisaAlunosDiplomacao($params)
    {
        try {
            $customWhere = array();

            $arrWhere = array(
                'bl_ativo' => 1,
                'id_matriculadiplomacao' => " IS NOT NULL",
                'id_entidadematricula' => $this->sessao->id_entidade,
                'id_evolucao' => "IN ( " . (
                        \G2\Constante\Evolucao::TB_MATRICULA_FORMADO . " ," .
                        \G2\Constante\Evolucao::TB_MATRICULA_DIPLOMADO
                    ) . " ) "
            );

            if (!empty($params['bl_academico'])) {
                $arrWhere['bl_academico'] = (int)$params['bl_academico'];
            }

            if (!empty($params['bl_documentacao'])) {
                $arrWhere['bl_documentacao'] = (int)$params['bl_documentacao'];
            }

            if (!empty($params['id_evolucaodiplomacao'])) {
                $arrWhere['id_evolucaodiplomacao'] = (int)$params['id_evolucaodiplomacao'];
            }

            if (!empty($params['id_projetopedagogico'])) {
                $arrWhere['id_projetopedagogico'] = (int)$params['id_projetopedagogico'];
            }

            if (!empty($params['st_nomecompleto'])) {
                $customWhere[] = ("tb.st_nomecompleto LIKE '%" . trim($params['st_nomecompleto']) . "%'
                                      OR tb.st_cpf LIKE '%" . trim($params['st_nomecompleto']) . "%'
                                      OR tb.st_email LIKE '%" . trim($params['st_nomecompleto']) . "%'");
            }

            //dt_aptodiplomar
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_APTO_DIPLOMAR) {
                if (!empty($params['dt_aptoadiplomar_inicio']) && !empty($params['dt_aptoadiplomar_fim'])) {
                    $arrWhere['dt_aptoadiplomar'] = "BETWEEN '" . $this->converterData($params['dt_aptoadiplomar_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_aptoadiplomar_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_aptoadiplomar_inicio']) && empty($params['dt_aptoadiplomar_fim'])) {
                    $arrWhere['dt_aptoadiplomar'] = " >= '" . $this->converterData($params['dt_aptoadiplomar_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_aptoadiplomar_inicio']) && !empty($params['dt_aptoadiplomar_fim'])) {
                    $arrWhere['dt_aptoadiplomar'] = " <= '" . $this->converterData($params['dt_aptoadiplomar_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_colacao
            if (!empty($params['dt_colacao_inicio']) && !empty($params['dt_colacao_fim'])) {
                $arrWhere['dt_colacao'] = " BETWEEN '" . $this->converterData($params['dt_colacao_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_colacao_fim'], 'Y-m-d') . "' ";
            } elseif (!empty($params['dt_colacao_inicio']) && empty($params['dt_colacao_fim'])) {
                $arrWhere['dt_colacao'] = " >= '" . $this->converterData($params['dt_colacao_inicio'], 'Y-m-d') . "' ";
            } elseif (empty($params['dt_aptoadiplomar_inicio']) && !empty($params['dt_aptoadiplomar_fim'])) {
                $arrWhere['dt_colacao'] = " <= '" . $this->converterData($params['dt_colacao_fim'], 'Y-m-d') . "' ";
            }

            //dt_diplomagerado
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_DIPLOMA_GERADO) {
                if (!empty($params['dt_diplomagerado_inicio']) && !empty($params['dt_diplomagerado_fim'])) {
                    $arrWhere['dt_diplomagerado'] = " BETWEEN '" . $this->converterData($params['dt_diplomagerado_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_diplomagerado_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_diplomagerado_inicio']) && empty($params['dt_diplomagerado_fim'])) {
                    $arrWhere['dt_diplomagerado'] = " >= '" . $this->converterData($params['dt_diplomagerado_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_aptoadiplomar_inicio']) && !empty($params['dt_aptoadiplomar_fim'])) {
                    $arrWhere['dt_diplomagerado'] = " <= '" . $this->converterData($params['dt_diplomagerado_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_entreguealuno
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENTREGUE_ALUNO) {
                if (!empty($params['dt_entreguealuno_inicio']) && !empty($params['dt_entreguealuno_fim'])) {
                    $arrWhere['dt_entreguealuno'] = " BETWEEN '" . $this->converterData($params['dt_entreguealuno_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_entreguealuno_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_entreguealuno_inicio']) && empty($params['dt_entreguealuno_fim'])) {
                    $arrWhere['dt_entreguealuno'] = " >= '" . $this->converterData($params['dt_entreguealuno_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_entreguealuno_inicio']) && !empty($params['dt_entreguealuno_fim'])) {
                    $arrWhere['dt_entreguealuno'] = " <= '" . $this->converterData($params['dt_entreguealuno_fim'], 'Y-m-d') . "' ";
                }
            }
            //dt_enviadoassinatura
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENVIADO_ASSINATURA) {
                if (!empty($params['dt_enviadoassinatura_inicio']) && !empty($params['dt_enviadoassinatura_fim'])) {
                    $arrWhere['dt_enviadoassinatura'] = " BETWEEN '" . $this->converterData($params['dt_enviadoassinatura_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_enviadoassinatura_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_enviadoassinatura_inicio']) && empty($params['dt_enviadoassinatura_fim'])) {
                    $arrWhere['dt_enviadoassinatura'] = " >= '" . $this->converterData($params['dt_enviadoassinatura_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_entreguealuno_inicio']) && !empty($params['dt_entreguealuno_fim'])) {
                    $arrWhere['dt_enviadoassinatura'] = " <= '" . $this->converterData($params['dt_enviadoassinatura_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_enviadoregistro
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENVIADO_REGISTRO) {
                if (!empty($params['dt_enviadoregistro_inicio']) && !empty($params['dt_enviadoregistro_fim'])) {
                    $arrWhere['dt_enviadoregistro'] = " BETWEEN '" . $this->converterData($params['dt_enviadoregistro_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_enviadoregistro_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_enviadoregistro_inicio']) && empty($params['dt_enviadoregistro_fim'])) {
                    $arrWhere['dt_enviadoregistro'] = " >= '" . $this->converterData($params['dt_enviadoregistro_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_enviadoregistro_inicio']) && !empty($params['dt_enviadoregistro_fim'])) {
                    $arrWhere['dt_enviadoregistro'] = " <= '" . $this->converterData($params['dt_enviadoregistro_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_enviadosecretaria
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENVIADO_SECRETARIA) {
                if (!empty($params['dt_enviadosecretaria_inicio']) && !empty($params['dt_enviadosecretaria_fim'])) {
                    $arrWhere['dt_enviadosecretaria'] = " BETWEEN '" . $this->converterData($params['dt_enviadosecretaria_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_enviadosecretaria_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_enviadosecretaria_inicio']) && empty($params['dt_enviadosecretaria_fim'])) {
                    $arrWhere['dt_enviadosecretaria'] = " >= '" . $this->converterData($params['dt_enviadosecretaria_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_enviadosecretaria_inicio']) && !empty($params['dt_enviadosecretaria_fim'])) {
                    $arrWhere['dt_enviadosecretaria'] = " <= '" . $this->converterData($params['dt_enviadosecretaria_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_enviopolo
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENVIADO_POLO_ALUNO) {
                if (!empty($params['dt_enviopolo_inicio']) && !empty($params['dt_enviopolo_fim'])) {
                    $arrWhere['dt_enviopolo'] = " BETWEEN '" . $this->converterData($params['dt_enviopolo_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_enviopolo_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_enviopolo_inicio']) && empty($params['dt_enviopolo_fim'])) {
                    $arrWhere['dt_enviopolo'] = " >= '" . $this->converterData($params['dt_enviopolo_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_enviopolo_inicio']) && !empty($params['dt_enviopolo_fim'])) {
                    $arrWhere['dt_enviopolo'] = " <= '" . $this->converterData($params['dt_enviopolo_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_historicogerado
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_HISTORICO_GERADO) {
                if (!empty($params['dt_historicogerado_inicio']) && !empty($params['dt_historicogerado_fim'])) {
                    $arrWhere['dt_historicogerado'] = " BETWEEN '" . $this->converterData($params['dt_historicogerado_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_historicogerado_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_historicogerado_inicio']) && empty($params['dt_historicogerado_fim'])) {
                    $arrWhere['dt_historicogerado'] = " >= '" . $this->converterData($params['dt_historicogerado_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_historicogerado_inicio']) && !empty($params['dt_historicogerado_fim'])) {
                    $arrWhere['dt_historicogerado'] = " <= '" . $this->converterData($params['dt_historicogerado_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_historicogerado
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_EQUIVALENCIA_GERADO) {
                if (!empty($params['dt_equivalenciagerado_inicio']) && !empty($params['dt_equivalenciagerado_fim'])) {
                    $arrWhere['dt_equivalenciagerado'] = " BETWEEN '" . $this->converterData($params['dt_equivalenciagerado_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_equivalenciagerado_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_equivalenciagerado_inicio']) && empty($params['dt_equivalenciagerado_fim'])) {
                    $arrWhere['dt_equivalenciagerado'] = " >= '" . $this->converterData($params['dt_equivalenciagerado_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_equivalenciagerado_inicio']) && !empty($params['dt_equivalenciagerado_fim'])) {
                    $arrWhere['dt_equivalenciagerado'] = " <= '" . $this->converterData($params['dt_equivalenciagerado_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_retornadoassinatura
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_RETORNADO_ASSINATURA) {
                if (!empty($params['dt_retornadoassinatura_inicio']) && !empty($params['dt_retornadoassinatura_fim'])) {
                    $arrWhere['dt_retornadoassinatura'] = " BETWEEN '" . $this->converterData($params['dt_retornadoassinatura_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_retornadoassinatura_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_retornadoassinatura_inicio']) && empty($params['dt_retornadoassinatura_fim'])) {
                    $arrWhere['dt_retornadoassinatura'] = " >= '" . $this->converterData($params['dt_retornadoassinatura_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_retornadoassinatura_inicio']) && !empty($params['dt_retornadoassinatura_fim'])) {
                    $arrWhere['dt_retornadoassinatura'] = " <= '" . $this->converterData($params['dt_retornadoassinatura_fim'], 'Y-m-d') . "' ";
                }
            }

            //dt_retornoregistro
            if (!empty($params['id_evolucaodiplomacao']) && $params['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_RETORNADO_REGISTRO) {
                if (!empty($params['dt_retornoregistro_inicio']) && !empty($params['dt_retornoregistro_fim'])) {
                    $arrWhere['dt_retornoregistro'] = " BETWEEN '" . $this->converterData($params['dt_retornoregistro_inicio'], 'Y-m-d') . "' AND  '" . $this->converterData($params['dt_retornoregistro_fim'], 'Y-m-d') . "' ";
                } elseif (!empty($params['dt_retornoregistro_inicio']) && empty($params['dt_retornoregistro_fim'])) {
                    $arrWhere['dt_retornoregistro'] = " >= '" . $this->converterData($params['dt_retornoregistro_inicio'], 'Y-m-d') . "' ";
                } elseif (empty($params['dt_retornoregistro_inicio']) && !empty($params['dt_retornoregistro_fim'])) {
                    $arrWhere['dt_retornoregistro'] = " <= '" . $this->converterData($params['dt_retornoregistro_fim'], 'Y-m-d') . "' ";
                }
            }

            return $this->findCustom('\G2\Entity\VwMatriculaDiplomacao', $arrWhere, $customWhere, array(
                'st_nomecompleto' => 'ASC',
                'id_matricula' => 'ASC',
                'id_matriculadiplomacao' => 'ASC'
            ));
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Salva array de matriculas diplomação
     * @param array $array_dados
     * @return \Ead1_Mensageiro
     */
    public function salvarEtapaArray(array $array_dados)
    {
        try {
            $arrayReturn = array();
            foreach ($array_dados['data'] as $vwMatriculaDiplomacao) {
                $msgSave = $this->salvarEtapa($vwMatriculaDiplomacao);
                if ($msgSave->getTipo() === \Ead1_IMensageiro::SUCESSO)
                    $arrayReturn[] = $msgSave->getFirstMensagem();
            }
            return new \Ead1_Mensageiro('Informações salvas com sucesso. ', \Ead1_IMensageiro::SUCESSO, $arrayReturn);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar etapas: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * Salva etapas (datas) da diplomação
     * @param $matriculaDiplomacao
     * @return \Ead1_Mensageiro
     */
    public function salvarEtapa($matriculaDiplomacao)
    {
        /**
         * Marca se uma alteração tem tramite diferente, sem alteração da evolucao da diplomação na matricula
         * Essa situação acontece quando há alteracao nas datas de Enviado Ao Polo do Aluno e Entregue Ao aluno
         * posteriormente a finalização do processo por um perfil especifico que pode fazer essa alteração
         */
        $bl_alteracaocomperfil = (boolean)$matriculaDiplomacao['bl_alteracaocomperfil'];
        $data_envio_aux = false;
        $data_enntregue_aux = false;
        $md_en_aux = false;

        try {
            if (!empty($matriculaDiplomacao['id_matriculadiplomacao'])) {

                //trata as datas para o formato correto
                foreach ($matriculaDiplomacao as $ch => $md) {
                    if ((substr($ch, 0, 2) == 'dt') && !empty($md)) {
                        $matriculaDiplomacao[$ch] = $this->converterData($md, 'datetime');
                    }
                }
                $this->beginTransaction();

                //se $bl_alteracaocomperfil = true Guarda as datas para comparação do tramite
                if ($bl_alteracaocomperfil) {
                    /** @var \G2\Entity\MatriculaDiplomacao $md_en_aux * */
                    $md_en_aux = $this->find('\G2\Entity\MatriculaDiplomacao', (int)$matriculaDiplomacao['id_matriculadiplomacao']);
                    if ($md_en_aux instanceof \G2\Entity\MatriculaDiplomacao) {
                        $data_envio_aux = $this->converterData($md_en_aux->getDt_enviopolo(), 'd/m/Y');
                        $data_enntregue_aux = $this->converterData($md_en_aux->getDt_entreguealuno(), 'd/m/Y');
                    }
                }

                // Verificar se foi alterado os dados do diploma
                if (!empty($matriculaDiplomacao['id_matriculadiplomacao'])) {
                    /** @var \G2\Entity\MatriculaDiplomacao $entity */
                    $entity = $this->find('\G2\Entity\MatriculaDiplomacao', $matriculaDiplomacao['id_matriculadiplomacao']);

                    if ($entity instanceof \G2\Entity\MatriculaDiplomacao) {
                        // 0 = Não cadastrar trâmite
                        // 1 = Cadastrar trâmite como "Cadastro"
                        // 2 = Cadastrar trâmite como "Alteração"
                        $tramiteDiploma = 0;

                        // Se não possuir nehum dado de diploma no banco, e possuir dados enviados então é "Cadastro"
                        if (
                            !$entity->getSt_diplomanumero() && !$entity->getSt_diplomalivro() && !$entity->getSt_diplomafolha() &&
                            (!empty($matriculaDiplomacao['st_diplomanumero']) || !empty($matriculaDiplomacao['st_diplomalivro']) || !empty($matriculaDiplomacao['st_diplomalivro']))
                        ) {
                            $tramiteDiploma = 1;
                        } // Se posuir algum dos dados, e qualquer um deles estiver diferente do que está cadastrado no banco, então é "Alteração"
                        else if (
                            (array_key_exists('st_diplomanumero', $matriculaDiplomacao) && $matriculaDiplomacao['st_diplomanumero'] != $entity->getSt_diplomanumero())
                            || (array_key_exists('st_diplomalivro', $matriculaDiplomacao) && $matriculaDiplomacao['st_diplomalivro'] != $entity->getSt_diplomalivro())
                            || (array_key_exists('st_diplomafolha', $matriculaDiplomacao) && $matriculaDiplomacao['st_diplomafolha'] != $entity->getSt_diplomafolha())
                        ) {
                            $tramiteDiploma = 2;
                        }

                        if ($tramiteDiploma) {
                            $this->salvarTramiteRegistroDiploma($matriculaDiplomacao['id_matricula'], $tramiteDiploma === 1 ? false : true, $matriculaDiplomacao);
                        }
                    }
                }

                /** Salvando informacoes da tabela matricula diplomacao */

                /** @var \G2\Entity\MatriculaDiplomacao $matriculaDiplomacao_en * */
                $matriculaDiplomacao_en = $this->preencherEntity('\G2\Entity\MatriculaDiplomacao', 'id_matriculadiplomacao', $matriculaDiplomacao);
                $return = $this->save($matriculaDiplomacao_en);

                /** @var \G2\Entity\Matricula $matricula */
                $matricula = $this->find('\G2\Entity\Matricula', $matriculaDiplomacao['id_matricula']);

                if (!$bl_alteracaocomperfil) {
                    $matricula->setId_evolucaodiplomacao($this->getReference('\G2\Entity\Evolucao', (int)$matriculaDiplomacao['id_evolucaodiplomacao']));

                    //ao salvar a data de Entregue ao aluno a matricula recebe a evolucao de DIPLOMADO
                    if ($matriculaDiplomacao['id_evolucaodiplomacao'] == \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_ENTREGUE_ALUNO) {
                        $matricula->setId_evolucao($this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_DIPLOMADO));
                        //salva o tramite
                        $ng_mat = new \G2\Negocio\Matricula();
                        $ng_mat->gerarTramite($matricula, array(
                            'id_tipotramite' => \G2\Constante\TipoTramite::MATRICULA_EVOLUCAO,
                            'st_tramite' => "A evolução da matrícula foi alterada para Diplomado."
                        ));
                    }

                    if ($this->save($matricula)) {
                        $this->salvarTramiteDiplomacao($matricula);
                    } else {
                        throw new \Exception('Erro ao atualizar evolucao na matricula');
                    }
                } else {
                    //salva o tramite com a alteração das datas apenas
                    $tramiteNegocio = new \G2\Negocio\Tramite();
                    if ($md_en_aux instanceof \G2\Entity\MatriculaDiplomacao) {
                        //verifica se a alteracao foi na data de Enviado ao Pólo do aluno
                        if ($data_envio_aux && ($data_envio_aux !== $this->converterData($matriculaDiplomacao_en->getDt_enviopolo(), 'd/m/Y'))) {
                            $st_tramite_datas = 'A data [Enviado ao Pólo do Aluno] foi alterada de [ ' . $data_envio_aux . ' ] 
                            para [ ' . $this->converterData($matriculaDiplomacao_en->getDt_enviopolo(), 'd/m/Y') . ' ].';
                            $tramiteNegocio->salvarTramiteMatricula($st_tramite_datas, $matricula->getId_matricula());
                        }

                        //verifica se a alteracao foi na data de Entregue ao aluno
                        if ($data_enntregue_aux && ($data_enntregue_aux !== $this->converterData($matriculaDiplomacao_en->getDt_entreguealuno(), 'd/m/Y'))) {
                            $st_tramite_datas = 'A data [Entregue ao Aluno] foi alterada de [ ' . $data_enntregue_aux . ' ] 
                            para [ ' . $this->converterData($matriculaDiplomacao_en->getDt_entreguealuno(), 'd/m/Y') . ' ].';
                            $tramiteNegocio->salvarTramiteMatricula($st_tramite_datas, $matricula->getId_matricula());
                        }
                    }
                }
                $this->commit();
                return new \Ead1_Mensageiro($return);

            }
        } catch (DBALException $de) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao salvar etapa da diplomação [bancodedados]: ' . $de->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar etapa da diplomação: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * @param $matricula
     * @return \Ead1_Mensageiro
     */
    public function salvarTramiteDiplomacao($matricula)
    {
        try {
            $tramite = new \G2\Negocio\Tramite();
            $evolucao = \G2\Constante\Evolucao::getArrayDiplomacao();
            $stTramite = "A Evolução do Diploma foi alterada para " . $evolucao[$matricula->getId_evolucaodiplomacao()->getId_evolucao()];

            if ($tramite->salvarTramiteMatricula($stTramite, $matricula->getId_matricula()))
                return new \Ead1_Mensageiro('Tramite da diplomação salvo com sucesso');
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar trâmite da diplomação: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function salvarTramiteRegistroDiploma($id_matricula, $bl_alteracao = false, $dados = array())
    {
        try {
            $ng_tramite = new \G2\Negocio\Tramite();
            $ar_tramite = ["O Número do Registro do Diploma foi " . ($bl_alteracao ? 'alterado' : 'cadastrado') . '.'];

            if (isset($dados['st_diplomanumero'])) {
                $ar_tramite[] = 'Número: ' . $dados['st_diplomanumero'];
            }

            if (isset($dados['st_diplomalivro'])) {
                $ar_tramite[] = 'Livro: ' . $dados['st_diplomalivro'];
            }

            if (isset($dados['st_diplomafolha'])) {
                $ar_tramite[] = 'Folha: ' . $dados['st_diplomafolha'];
            }

            if ($ng_tramite->salvarTramiteMatricula(implode('<br/>', $ar_tramite), $id_matricula)) {
                return new \Ead1_Mensageiro('Tramite da diplomação salvo com sucesso');
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar trâmite da diplomação: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param int $id_matricula
     * @param string $documento
     * @param bool $download
     * @return string
     * @throws \Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function gerarDocumento($id_matricula, $documento, $download = false, $origem = null)
    {
        $bo_textos = new \TextosBO();
        $bo_matricula = new \MatriculaBO();
        $bo_textosistema = new \TextoSistemaBO();

        $vwGerarDeclaracaoTO = new \VwGerarDeclaracaoTO();
        $vwGerarDeclaracaoTO->setId_matricula($id_matricula);
        $vwGerarDeclaracaoTO = $bo_textos->retornarVwGerarDeclaracao($vwGerarDeclaracaoTO)->subtractMensageiro()->getFirstMensagem();
        $arrTextoVariaveisTO = $bo_textos->retornarTextoVariaveis(new \TextoVariaveisTO(array('id_textocategoria' => \G2\Constante\TextoCategoria::DECLARACOES)))->subtractMensageiro()->getMensagem();

        $genero = $bo_matricula->retornaGenero($id_matricula);

        // Buscar a view para renderizar como string
        $view = new \Zend_View();
        $view->id_grauacademico = $vwGerarDeclaracaoTO->id_grauacademico;
        $view->addScriptPath(APPLICATION_PATH . '/apps/default/views/scripts/');

        $view->origem = $origem;

        $texto = $view->render("diplomacao/gerar-{$documento}.phtml");

        $textoSubstituido = $bo_textosistema->substituirVariaveisTO($texto, $vwGerarDeclaracaoTO, $arrTextoVariaveisTO, array('id_matricula' => $id_matricula), $genero);

        switch ($documento) {
            case 'diploma':
                $filename = 'DIPLOMA_' . date('Y-m-d-H-i');
                $orient = 'landscape';
                break;
            default:
                $filename = strtoupper($documento) . '_' . date('Y-m-d-H-i');
                $orient = 'portrait';
                break;
        }

        $textoSubstituido = utf8_encode($textoSubstituido);

        $pdf = new \Ead1_Pdf();
        $pdf->carregarHTML($textoSubstituido);
        $pdf->configuraPapel('a4', $orient);
        $download ? $pdf->gerarPdf($filename . '.pdf') : $pdf->showPdf();

        return $textoSubstituido;
    }

    /**
     *
     * @param $id_matricula
     * @return \Ead1_Mensageiro
     */
    public function primeiraEtapaDiplomacao($id_matricula)
    {
        //insere registro na tb_matriculadiplomacao e altera matricula do aluno para APTO A DIPLOMAR
        $this->beginTransaction();
        try {
            if (empty($id_matricula)) {
                throw new \Exception('O id_matricula é obrigatório e não foi passado.');
            }

            $dados = array('id_matricula' => (int)$id_matricula
            , 'id_usuariocadastro' => $this->sessao->id_usuario
            , 'id_entidade' => $this->sessao->id_entidade
            , 'dt_cadastro' => new \DateTime()
            , 'bl_ativo' => 1
            , 'dt_aptodiplomar' => new \DateTime()
            , 'id_usuarioaptodiplomar' => $this->sessao->id_usuario);

            //verifica se já existe registro na tb_matriculadiplomacao para a matrícula passada
            $md_aux = $this->findOneBy('\G2\Entity\MatriculaDiplomacao', array('id_matricula' => (int)$id_matricula));
            if ($md_aux instanceof \G2\Entity\MatriculaDiplomacao && $md_aux->getId_matriculadiplomacao()) {
                $dados['id_matriculadiplomacao'] = $md_aux->getId_matriculadiplomacao();
            }

            /** @var \G2\Entity\MatriculaDiplomacao $matriculaDiplomacao * */
            $matriculaDiplomacao = $this->preencherEntity('\G2\Entity\MatriculaDiplomacao', 'id_matriculadiplomacao', $dados);

            if (!$this->save($matriculaDiplomacao)) {
                throw new \Exception('Erro ao salvar o registro na tb_matriculadiplomacao');
            }

            if ($matriculaDiplomacao instanceof \G2\Entity\MatriculaDiplomacao && $matriculaDiplomacao->getId_matriculadiplomacao()) {

                //Salva um novo tramite informando a mudança de evolução para formado
                $tramite = new \G2\Negocio\Tramite();
                $tipoTramite = \G2\Constante\TipoTramite::MATRICULA_EVOLUCAO;
                $textoTramite = 'A evolução da matrícula foi alterada para Formado.';
                $tramite->salvarTramiteMatricula($textoTramite, $id_matricula, $tipoTramite);
                /** @var \G2\Entity\Matricula $mat */
                $mat = $this->find('\G2\Entity\Matricula', $id_matricula);

                $mat->setId_evolucaodiplomacao($this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_DIPLO_APTO_DIPLOMAR));
                $mat->setId_evolucao($this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_FORMADO));

                if ($this->save($mat)) {
                    $this->salvarTramiteDiplomacao($mat);
                }
            }

            $this->commit();
            return new \Ead1_Mensageiro('Primeira etapa da diplomação salva com sucesso.', \Ead1_IMensageiro::SUCESSO, $matriculaDiplomacao);

        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Erro ao salvar primeira etapa da diplomação: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

}

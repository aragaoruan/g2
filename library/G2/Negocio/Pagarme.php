<?php

namespace G2\Negocio;

use Doctrine\Common\Util\Debug;
use G2\Constante\MeioPagamento;
use G2\Constante\MensagemSistema;
use G2\Entity\CartaoBandeira;
use G2\Entity\Lancamento;
use G2\Entity\Usuario;
use G2\Repository\VwVendaLancamento;

require('pagarme/Pagarme.php');

class Pagarme extends Negocio
{

    /** @var \G2\Entity\Sistema $sistema */
    protected $_sistema;
    /** @var   $_arrInfoLancamentos */
    protected $_arrInfoLancamentos;
    /** @var \Ead1_Mensageiro $mensageiro */
    protected $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * @param $idLancamento
     * @return \Ead1_Mensageiro|\Ead1_Mensageiro
     */
    public function transactionBoleto($idLancamento)
    {
        try {

            $idLancamento = (int)$idLancamento;

            if (is_null($idLancamento) || $idLancamento == 0) {
                throw new \InvalidArgumentException('Informe o id_lancamento para geração do boleto');
            }

            /** @var \G2\Entity\Lancamento $lan */
            $lan = $this->findOneBy('\G2\Entity\Lancamento',
                array(
                    'id_lancamento' => $idLancamento,
                    'bl_ativo' => 1,
                    'bl_quitado' => false
                )
            );

            if (empty($lan)) {
                throw new \InvalidArgumentException('Informe o id_lancamento para geração do boleto');
            }

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_lancamento' => $lan->getId_lancamento(),
                    'id_entidade' => $lan->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::BOLETO
                )
            );

            $this->setApiKey($vwResumo->getId_entidade());

            $transacaoFin = null;

            if ($lan->getSt_idtransacaoexterna()) {
                /** @var \G2\Entity\TransacaoFinanceira $transacaoFin */
                $transacaoFin = $this->findOneBy('\G2\Entity\TransacaoFinanceira',
                    array(
                        'st_idtransacaoexterna' => $lan->getSt_idtransacaoexterna(),
                        'id_venda' => $vwResumo->getId_venda()
                    )
                );

            }

            $today = new \DateTime();
            $today->setTime(0, 0, 0);

            if (!empty($transacaoFin)) {
                if (!empty($transacaoFin->getDt_vencimento())) {

                    $vencimentoTransacao = $transacaoFin->getDt_vencimento();
                    $vencimentoTransacao->setTime(0, 0, 0);
                    $diff = $today->diff($vencimentoTransacao);

                    if (
                        $diff->days >= 0
                        && !$diff->invert
                        && !empty($lan->getSt_urlboleto())
                        && $transacaoFin->getId_statustransacao()->getId_situacao() ==
                        \G2\Constante\Situacao::WAITING_PAYMENT
                    ) {
                        $this->mensageiro->setMensageiro($lan, \Ead1_IMensageiro::SUCESSO);
                        return $this->mensageiro;
                    } else if (
                        $diff->days >= 0
                        && !$diff->invert
                    ) {
                        $transacaoPagarme = $this->getPagarmeTransaction(
                            $transacaoFin->getSt_idtransacaoexterna(),
                            $lan->getId_entidade()
                        );

                        if ($transacaoPagarme->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                            $this->mensageiro = $this->salvarTransacaoFinanceira(
                                $transacaoPagarme->getFirstMensagem(),
                                $vwResumo,
                                null,
                                false
                            );
                            return $this->mensageiro;
                        }

                    }
                }
            }

            /** @var \Ead1_Mensageiro $mensageiroValorAtualizado */
            $mensageiroValorAtualizado = $this->retornarLancamentoValorAtualizado($lan);

            if ($mensageiroValorAtualizado->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception('Erro ao retornar os valores atualizados do lançamento');
            }

            $lan = $mensageiroValorAtualizado->getFirstMensagem();

            if ($vwResumo->getId_usuariolancamento()) {
                /** @var \G2\Entity\VwPessoa $uTo */
                $responsavelLancamento = $this->findOneBy('\G2\Entity\VwPessoa',
                    array(
                        'id_usuario' => $vwResumo->getId_usuariolancamento(),
                        'id_entidade' => $vwResumo->getId_entidade()
                    )
                );

            } elseif ($vwResumo->getId_entidadelancamento()) {
                $responsavelLancamento =
                    $this->find('\G2\Entity\Entidade', $vwResumo->getId_entidadelancamento());
            } else {
                throw new \Exception('Erro ao retornar o responsável do lançamento!');
            }

            $token = sha1(
                uniqid(
                    $lan->getId_lancamento() . $lan->getId_entidade() .
                    $vwResumo->getId_venda() . time(), true
                )
            );

            $vwResumo->setSt_coddocumento($token);
            //valor original do lançamento considerando juros, multa e descontos
            $valorLancamento =
                ($lan->getNu_valor() + $lan->getNu_juros() + $lan->getNu_multa()) - $lan->getNu_desconto();
            // pega o valor do boleto vencido considerando juros e multa se estiver setado ou o valor original
            $valor_boleto =
                (
                isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_lancamento_atualizado']) ?
                    $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_lancamento_atualizado'] :
                    $valorLancamento
                );
            // valor de descontos do boleto se existir
            $valor_desconto =
                (
                isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_desconto']) ?
                    $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_desconto'] :
                    0
                );
            // valor de descontos da campanha de pontualidade se existir
            $valor_pontualidade =
                (
                isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_pontualidade']) ?
                    $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_pontualidade'] :
                    0
                );
            /** @var \DateTime $dt_vencimento */
            $dt_vencimento =
                (
                isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['dt_novo_vencimento']) &&
                $this->_arrInfoLancamentos[$lan->getId_lancamento()]['dt_novo_vencimento'] ?
                    $this->_arrInfoLancamentos[$lan->getId_lancamento()]['dt_novo_vencimento'] :
                    $vwResumo->getDt_vencimento()
                );
            $dt_vencimento->setTime(0, 0, 0);
            $diff3 = $today->diff($dt_vencimento);
            if ($diff3->days == 0) {
                $dt_vencimento->modify('+2 day');
            }

            $transaction = new \PagarMe_Transaction(array(
                //Valor a ser cobrado. Deve ser passado em centavos. Ex: R$ 10.00 = 1000
                'amount' => number_format(
                    (($valor_boleto - $valor_desconto - $valor_pontualidade) * 100),
                    0,
                    '',
                    ''),
                //Aceita dois tipos de pagamentos/valores: credit_card e boleto
                'payment_method' => \G2\Constante\PagarmePaymentMethod::BOLETO,
                //Endpoint do seu sistema que receberá informações a cada atualização da transação.
                // Caso você defina este parâmetro, o processamento da transação se tornará assíncrono.
                'postback_url' => \Ead1_Ambiente::geral()->st_url_gestor2 . '/robo/postback/idboleto/' . $token,
                //Prazo limite para pagamento do boleto
                'boleto_expiration_date' => $dt_vencimento->format('Y-m-d\T03:00:00.000\Z'),
                //Descrição que aparecerá na fatura depois do nome da loja. Máximo de 13 caracteres
                'soft_descriptor' => '',
                //* quando criar uma transação, precisa passar o argumento boleto_instructions,
                // que é uma string de no máximo 255 caracteres
                'boleto_instructions' => 'Sr. Caixa, não aceitar o pagamento após o vencimento;',
                'metadata' => array(
                    'id_lancamento' => $lan->getId_lancamento(),
                    'id_venda' => $vwResumo->getId_venda()
                ),
                'customer' => array(
                    //Nome completo ou razão social do cliente que está realizando a transação
                    'name' => $responsavelLancamento instanceof \G2\Entity\VwPessoa ?
                        $responsavelLancamento->getSt_nomecompleto() :
                        $responsavelLancamento->getSt_razaosocial(),
                    //CPF ou CNPJ do cliente, sem separadores
                    'document_number' => $responsavelLancamento instanceof \G2\Entity\VwPessoa ?
                        $responsavelLancamento->getSt_cpf() :
                        $responsavelLancamento->getSt_cnpj(),
                    //email do cliente
                    //'email' => $uTo->getst_email(),
//                    'address' => array(
//                        //logradouro (rua, avenida, etc) do cliente
//                        'street' => $uTo->getst_endereco(),
//                        //Número da residência/estabelecimento do cliente
//                        'street_number' => $uTo->getnu_numero(),
//                        //complemento do endereço do cliente
//                        'complementary' => $uTo->getst_complemento(),
//                        //bairro de localização do cliente
//                        'neighborhood' => $uTo->getst_bairro(),
//                        //CEP do imóvel do cliente, sem separadores
//                        'zipcode' => str_replace('.', '', str_replace('-', '', $uTo->getst_cep())) ,
//                    ),
                    //sexo do cliente
//                    'sex' => $uTo->getst_sexo(),
//                    'phone' => array(
//                        //DDD do telefone do cliente
//                        'ddd' => $uTo->getnu_ddd(),
//                        //número de telefone do cliente
//                        'number' => $uTo->getnu_telefone()
//                    ),
                    //Data de nascimento do cliente. Ex: 11-02-1985
                    //Formato: MM-DD-AAAA   Data de nascimento do cliente. Ex: 11-02-1985
                    //'born_at' => $uTo->getdt_nascimento()->format('m-d-Y'),
                ),
//                'split_rules' => array( array(
//                    //Identificador do recebedor
//                    'recipient_id' => $entidadeIntegracao->getId_recebedor()->getId_recebedorexterno(),
//                    //Indica se o recebedor vinculado a essa regra de divisão será cobrado pelas taxas da transação
//                    'charge_processing_fee' => true,
//                    //Indica se o recebedor vinculado a essa regra de divisão assumirá o risco da transação, ou seja,
//                    // possíveis estornos (chargeback)
//                    'liable' => true,
//                    //Define a porcentagem a ser recebida pelo recebedor configurado na regra.
//                    'percentage' => 100
//                )),

            ));

            $transaction->charge();

//            echo '<pre>';
//            var_dump($transaction);die;

            $this->mensageiro = $this->salvarTransacaoFinanceira(
                $transaction,
                $vwResumo,
                null,
                false
            );

            $stTramite = 'Solicitação de boleto no valor R$ ' .
                number_format(
                    $transaction->amount / 100,
                    2,
                    ',',
                    '.'
                ) . ' com vencimento em ' . $dt_vencimento->format('d/m/Y');

            if (!empty($lan->getId_acordo())) {
                $stTramite = 'Solicitação de boleto(s) proveniente de Acordo com valor R$ ' .
                    number_format(
                        $transaction->amount / 100,
                        2,
                        ',',
                        '.'
                    );
            }

            $venda = new \G2\Entity\Venda();
            $venda->find($vwResumo->getId_venda());

            $tramiteNegocio = new Tramite();
            $tramiteNegocio->salvarTramite(
                array(
                    'st_tramite' => $stTramite,
                    'id_categoriatramite' => 3,
                    'id_campo' => $vwResumo->getId_venda(),
                    'id_entidade' => $vwResumo->getId_entidade(),
                    'id_usuario' =>
                        ($this->sessao->id_usuario) ?
                            $this->sessao->id_usuario :
                            null
                )
            );

        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * @param $idEntidade
     * @throws \Exception
     */
    public function setApiKey($idEntidade)
    {
        try {

            if (!empty(\PagarMe::getApiKey())) {
                return $this;
            }

            if (empty($idEntidade)) {
                throw new \Exception('ID da entidade é obrigatório');
            }

            /** @var \G2\Entity\EntidadeIntegracao $entidadeConfiguracao */
            $entidadeConfiguracao = $this->findOneBy('\G2\Entity\EntidadeIntegracao',
                array(
                    'id_sistema' => \G2\Constante\Sistema::PAGARME,
                    'id_entidade' => $idEntidade
                )
            );

            if (empty($entidadeConfiguracao)) {
                throw new \Exception('Nenhuma configuração foi encontrada para a entidade informada. 
                    ID da entidade: ' . $idEntidade);
            }

            \PagarMe::setApiKey($entidadeConfiguracao->getSt_codchave());
        } catch (\Exception $message) {
            throw $message;
        }
    }

    /**
     * @param \G2\Entity\Lancamento $lancamento
     * @return \Ead1_Mensageiro
     */
    public function retornarLancamentoValorAtualizado(\G2\Entity\Lancamento $lancamento)
    {
        try {

            if (!$lancamento->getId_lancamento()) {
                throw new \Zend_Exception('O id_lancamento não veio setado!');
            }

            /** @var \G2\Entity\VwVendaLancamento $vwLancamento */
            $vwLancamento = $this->findOneBy('\G2\Entity\VwVendaLancamento',
                array(
                    'id_lancamento' => $lancamento->getId_lancamento()
                )
            );

            if (empty($vwLancamento)) {
                throw new \Zend_Validate_Exception('Nenhum Lançamento Encontrado!');
            }

            $data_vencimentoreal = $vwLancamento->getDt_vencimento();
            $today = new \DateTime();
            $today->setTime(0, 0, 0);
            $data_vencimentoreal->setTime(0, 0, 0);
            /**
             * ->format('w') - w  Numeric representation of the day of the week
             * 0 (for Sunday) through 6 (for Saturday)
             */
            if ($data_vencimentoreal->format('w') == 0) {
                $data_vencimentoreal = $data_vencimentoreal->modify('+1 day');
            } elseif ($data_vencimentoreal->format('w') == 6) {
                $data_vencimentoreal = $data_vencimentoreal->modify('+2 day');
            }

            //a segunda mais proxima é uma data maior ou igual a de hoje?
            $diff = $today->diff($data_vencimentoreal);
            if ($diff->days >= 0 && !$diff->invert) {
                $vwLancamento->setDt_vencimento($data_vencimentoreal);
                $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['dt_novo_vencimento'] =
                    $data_vencimentoreal;
            }

            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['porcentagem_juros'] =
                $vwLancamento->getNu_juros();
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['dias_atraso'] = 0;
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['total_juros'] = 0;
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['valor_multa'] =
                $vwLancamento->getNu_multaatraso();
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['total_multa'] =
                (($vwLancamento->getNu_valor() * $vwLancamento->getNu_multaatraso()) / 100);
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['juros_dia'] =
                ($vwLancamento->getNu_jurosatraso() * $vwLancamento->getNu_valor() / 100);
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['valor_pontualidade'] = 0;
            $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['bl_original'] =
                $vwLancamento->getBl_original();

            if (!$vwLancamento->getBl_quitado()) {
                if ($diff->days > 0 && $diff->invert) {
                    $this->aplicarJurosMultaAtraso($vwLancamento);
                } else if ($vwLancamento->getBl_original() || $vwLancamento->getBl_entrada()) {
                    //seta a posição no array com o desconto
                    $this->_arrInfoLancamentos[$vwLancamento->getId_lancamento()]['valor_pontualidade'] =
                        $this->retornaCampanhaVenda($vwLancamento);
                }
            }

            $this->mensageiro->setMensageiro($lancamento, \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $e) {
            $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Zend_Exception $e) {
            $this->mensageiro->setMensageiro('Erro ao Retornar Lançamento!',
                \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $this->mensageiro;
    }

    /**
     * @param \G2\Entity\VwVendaLancamento $vwVendaLancamento
     */
    private function aplicarJurosMultaAtraso(\G2\Entity\VwVendaLancamento $vwVendaLancamento)
    {

        $vencimento = $vwVendaLancamento->getDt_vencimento();
        $novoVencimento = new \DateTime();
        $novoVencimento->setTime(0, 0, 0);
        $vencimento->setTime(0, 0, 0);
        $diff = $novoVencimento->diff($vencimento);

        //nu_valor, nu_juros, nu_desconto, nu_multa
        $valor_boleto =
            ($vwVendaLancamento->getNu_valor() + $vwVendaLancamento->getNu_juros() + $vwVendaLancamento->getNu_multa())
            - $vwVendaLancamento->getNu_desconto();
        $valor_multa = (($valor_boleto * $vwVendaLancamento->getNu_multaatraso()) / 100);
        //((valor original * valor do juros) / 100)
        $juros_dia = ($vwVendaLancamento->getNu_jurosatraso() * $valor_boleto / 100);
        //valor juros dia * Quantidade de Dias em atraso
        $valor_juros = ($juros_dia * $diff->days);
        // valor multa + valor juros + valor original.
        $valor_lancamento_atualizado = ($valor_boleto + $valor_juros + $valor_multa);

        // Valida o dia corrente
        if ($novoVencimento->format('Ymd') == $vencimento->format('Ymd'))
            $valor_lancamento_atualizado = $vwVendaLancamento->getNu_valor();

        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['juros_dia'] = $juros_dia;
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['porcentagem_juros'] =
            $vwVendaLancamento->getNu_juros();
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['dias_atraso'] = $diff->days;
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['total_juros'] = $valor_juros;
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['valor_multa'] =
            $vwVendaLancamento->getNu_multaatraso();
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['total_multa'] = $valor_multa;
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['dt_novo_vencimento'] = $novoVencimento;
        $this->_arrInfoLancamentos[$vwVendaLancamento->getId_lancamento()]['valor_lancamento_atualizado'] =
            $valor_lancamento_atualizado;

    }

    /**
     * @param \G2\Entity\VwVendaLancamento $vwLancamento
     * @return int|mixed
     */
    protected function retornaCampanhaVenda(\G2\Entity\VwVendaLancamento $vwLancamento)
    {

        $valorDesconto = 0;//seta o valor do desconto como zero
        $vendaNegocio = new \G2\Negocio\Venda();
        //busca a venda
        $result = $vendaNegocio->findVenda($vwLancamento->getId_venda());
        //verifica se tem resultado e se tem campanha vinculada
        if ($result && $result->getId_campanhapontualidade()) {
            $campanha = $result->getId_campanhapontualidade();//atribui os dados da campanha a variavel

            //verifica se o desconto é valor ou porcentagem
//            if ($campanha->getId_tipodesconto()->getId_tipodesconto() == \G2\Constante\TipoDesconto::VALOR) {
//                //$valorDesconto = floatval($campanha->getNu_valordesconto());//subtrai o desconto do valor
//            } else
            if ($campanha->getId_tipodesconto()
                && $campanha->getId_tipodesconto()->getId_tipodesconto() == \G2\Constante\TipoDesconto::PORCENTAGEM
            ) {
                //calcula a porcetagem de desconto no valor
                $valorDesconto = $vwLancamento->getNu_valor() * (floatval($campanha->getNu_valordesconto()) / 100);
            }

        }

        return $valorDesconto;
    }

    /**
     * @param $transaction
     * @param \G2\Entity\VwResumoFinanceiro|null $vwResumo
     * @param \G2\Entity\Venda|null $venda
     * @return \Ead1_Mensageiro
     */
    public function salvarTransacaoFinanceira
    (
        $transaction,
        \G2\Entity\VwResumoFinanceiro $vwResumo = null,
        \G2\Entity\Venda $venda = null,
        $returnArray = true,
        $updateIdTransacaoExterna = true
    )
    {
        try {

            //select * from tb_transacaofinanceira where id_venda = 355457 AND id_transacaoexterna = 1969653
            $financialTransaction = $this->findOneBy('\G2\Entity\TransacaoFinanceira',
                array(
                    'id_venda' => !empty($vwResumo) && $vwResumo->getId_venda() ?
                        $vwResumo->getId_venda() :
                        $venda->getId_venda(),
                    'st_idtransacaoexterna' => $transaction->id
                )
            );

            $salvarTransacaoLancamento = false;
            if (!$financialTransaction) {
                $financialTransaction = new \G2\Entity\TransacaoFinanceira();
                $salvarTransacaoLancamento = true;
            }

            // número de parcela
            $financialTransaction->setNu_parcelas($transaction->installments);
            // data da criação do boleto
            $financialTransaction->setDt_cadastro(date_create($transaction->date_created));

            $financialTransaction->setId_usuariocadastro(
                $this->getReference('\G2\Entity\Usuario',
                    !empty($vwResumo) && $vwResumo->getId_usuario() ?
                        $vwResumo->getId_usuario() :
                        $venda->getId_usuario()->getId_usuario()
                )
            );

            $financialTransaction->setId_venda(
                $this->getReference('\G2\Entity\Venda',
                    !empty($vwResumo) && $vwResumo->getId_venda() ?
                        $vwResumo->getId_venda() :
                        $venda->getId_venda()
                )
            );

            $financialTransaction->setSt_codtransacaogateway($transaction->tid); //TID do boleto

            //métodos para transação com cartão de crédito;
            //no momento que for implementar o retorno do cartão é necessário pegar o id da bandeira através desse dado
            // Esse dado é do cartão de crédito, se não me engano é a bandeira do cartão
            $financialTransaction->setSt_codtransacaooperadora($transaction->card_brand);
            if($vwResumo->getId_meiopagamento() != \G2\Constante\MeioPagamento::BOLETO){
                $financialTransaction->setSt_titularcartao($transaction->current_transaction->card_holder_name); // nome que está no cartão
                $financialTransaction->setSt_ultimosdigitos($transaction->current_transaction->card_last_digits); // últimos digitos do cartão.
            }

            //
            $financialTransaction->setUn_transacaofinanceira('00000000-0000-0000-0000-000000000000');
            $financialTransaction->setSt_nsu($transaction->nsu);
            $financialTransaction->setSt_idtransacaoexterna($transaction->id);

            // mensagem com o retorno do porque a transação foi negada
            $financialTransaction->setSt_mensagem(self::statusCreditCard($transaction->refuse_reason));
            $financialTransaction->setNu_valorcielo($transaction->paid_amount / 100); // valor que foi pago da transação

            /*
             *  processing: transação sendo processada.
                authorized: transação autorizada. Cliente possui saldo na conta e este valor foi reservado para futura
                captura, que deve acontecer em no máximo 5 dias. Caso a transação não seja capturada,
                    a autorização é cancelada automaticamente.
                paid: transação paga (autorizada e capturada).
                refunded: transação estornada.
                waiting_payment: transação aguardando pagamento (status para transações criadas com boleto bancário).
                pending_refund: transação paga com boleto aguardando para ser estornada.
                refused: transação não autorizada.
                chargedback: transação sofreu chargeback.
            */
            $financialTransaction->setId_statustransacao(
                $this->findOneBy('\G2\Entity\Situacao',
                    array(
                        'st_situacao' => $transaction->status
                    )
                )
            );

            $financialTransaction->setSt_codigoautorizacao($transaction->authorization_code);

            $financialTransaction->setNu_valorenviado($transaction->amount / 100);
            $financialTransaction->setNu_valorautorizado($transaction->authorized_amount / 100);
            $financialTransaction->setNu_valorrecusado($transaction->refunded_amount / 100);

            $financialTransaction->setSt_meiopagamento($transaction->payment_method);

            $financialTransaction->setSt_urlboleto($transaction->boleto_url);
            $financialTransaction->setSt_boletocoditobarras($transaction->boleto_barcode);
            $financialTransaction->setSt_urlpostback($transaction->postback_url);
            $financialTransaction->setDt_vencimento(date_create($transaction->boleto_expiration_date));

            $this->save($financialTransaction);

            if ($vwResumo) {

                /** @var \G2\Entity\Lancamento $lancamento */
                $lancamento = $this->find('\G2\Entity\Lancamento', $vwResumo->getId_lancamento());
                if (!$lancamento) {
                    throw  new \Exception(MensagemSistema::LANCAMENTO_NAO_ENCONTRADO);
                }

                if ($salvarTransacaoLancamento) {
                    $transacaoLancamento = new \G2\Entity\TransacaoLancamento();
                    $transacaoLancamento->setId_lancamento($lancamento->getId_lancamento());
                    $transacaoLancamento->setId_transacaofinanceira($financialTransaction->getId_transacaofinanceira());

                    $this->save($transacaoLancamento);
                }

                $lancamento->setSt_Urlboleto($financialTransaction->getSt_urlboleto());
                $lancamento->setSt_autorizacao($financialTransaction->getSt_codigoautorizacao());
                $lancamento->setSt_codigobarras($financialTransaction->getSt_boletocoditobarras());
                $lancamento->setSt_coddocumento($vwResumo->getSt_coddocumento());
                if ($updateIdTransacaoExterna) {
                    $lancamento->setSt_idtransacaoexterna($transaction->id);
                }

                $lancamento = $this->save($lancamento);

                if ($returnArray) {
                    $this->mensageiro->setMensageiro($this->toArrayEntity($lancamento), \Ead1_IMensageiro::SUCESSO);
                } else {
                    $this->mensageiro->setMensageiro($lancamento, \Ead1_IMensageiro::SUCESSO);
                }
            } else {
                $this->mensageiro->setMensageiro('Transação financeira inserida com sucesso',
                    \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $message) {
            $this->mensageiro->setMensageiro($message->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;

    }

    /**
     * Traduz os status de recusa Pagarme
     * @param $reason
     * @return string
     */
    public static function statusCreditCard($reason)
    {

        switch ($reason) {
            case 'acquirer':
                return 'Recusado pelo adquirente (acquirer)';
                break;
            case 'antifraud':
                return 'Recusado pelo anti-fraud (antifraud)';
                break;
            case 'internal_error':
                return 'Erro interno na pagarme (internal_error)';
                break;
            case 'no_acquirer':
                return 'Não existe adquirent (no_acquirer)';
                break;
            case 'acquirer_timeout':
                return 'Timeout do adquirente (acquirer_timeout)';
                break;
            case 'chargedback':
                return 'Transação sofreu chargeback (chargedback)';
                break;
            case 'refused':
                return 'Transação não autorizada (refused)';
                break;
            case 'pending_refund':
                return 'Transação paga com boleto aguardando para ser estornada (pending_refund)';
                break;
            case 'waiting_payment':
                return 'Transação aguardando pagamento (status para transações criadas com boleto bancário) (waiting_payment)';
                break;
            case 'paid':
                return 'Transação autorizada (paid)';
                break;
            default:
                return 'Mensagem não encontrada: ' . $reason;
        }

    }

    /**
     * @param $idEntidade
     * @return \Ead1_Mensageiro
     */
    public function getTransactions($idEntidade)
    {
        try {
            $this->setApiKey($idEntidade);
            $transactions = \PagarMe_Transaction::all();
            $this->mensageiro->setMensageiro($transactions, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * @param $idTransaction
     * @param $idEntidade
     * @return \Ead1_Mensageiro
     */
    public function getTransaction($idTransaction, $idEntidade)
    {
        try {
            $this->setApiKey($idEntidade);
            $transaction = \PagarMe_Transaction::findById($idTransaction);

            /** @var \G2\Entity\Lancamento $lan */
            $lan = $this->findOneBy('\G2\Entity\Lancamento',
                array('id_lancamento' => $transaction->metadata->id_lancamento)
            );
            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_lancamento' => $lan->getId_lancamento(),
                    'id_entidade' => $lan->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::BOLETO
                )
            );

            return $this->salvarTransacaoFinanceira($transaction, $vwResumo);
        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Apenas retorna uma transação da Pagarme
     * @param $idTransaction
     * @param $idEntidade
     * @return \Ead1_Mensageiro
     */
    public function getPagarmeTransaction($idTransaction, $idEntidade)
    {
        try {
            $this->setApiKey($idEntidade);
            $transaction = \PagarMe_Transaction::findById($idTransaction);

            $this->mensageiro->setMensageiro($transaction, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * Verifica os pagamnentos recorrentes
     * @param \G2\Entity\VwVendaLancamento $to
     * @param bool $simulando
     * @return \Ead1_Mensageiro
     */
    public function verificarRecorrente(\G2\Entity\VwVendaLancamento $to, $simulando = false)
    {

        $simulando = false;
        $bl_vencido = true;
        $dt_atual = new \DateTime();
        $dt_vencimento = new \DateTime($to->getDt_vencimento());
        if ($dt_vencimento > $dt_atual) {
            $bl_vencido = false;
        }

        $parcelaRecorrente = null;
        $negocio = new \G2\Negocio\Negocio();

        try {
            $negocio->beginTransaction();

            $venda = new \G2\Negocio\Venda();

            $orderId = $to->getRecorrenteOrderid();

            $mensageiroOrder = $this->getSubscriptionTransactions($to->getRecorrenteOrderid(), $to->getIdEntidade(), $simulando);

            if ($mensageiroOrder->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                $subscription = $mensageiroOrder->getMensagem('subscription');
                $transactions = $mensageiroOrder->getMensagem('transactions');
                $nuParcelasPagas = count($transactions);
                $nuParcelaPagarmeAtual = ($to->getNu_cobranca() - 1);
                if($to->getNu_acordos()){
                    $nuParcelaPagarmeAtual =  $nuParcelaPagarmeAtual - $to->getNu_acordos();
                }

                if (is_array($transactions) && isset($transactions[$nuParcelaPagarmeAtual])) {
                    $parcelaRecorrente = (object)$transactions[$nuParcelaPagarmeAtual];
                } else if ($to->getNu_cobranca() > $nuParcelasPagas
                    && is_object($subscription['current_transaction']) && $subscription['current_transaction']->status != "paid"
                ) {
                    $parcelaRecorrente = $subscription['current_transaction'];
                }

                if ($simulando) {
                    echo '<pre>';
                    echo "Nu_cobranca: {$to->getNu_cobranca()} \n";
                    echo "Parcelas pagas: $nuParcelasPagas \n";
                    echo "Indice do Array da parcela atual: $nuParcelaPagarmeAtual \n";
                    echo "Acordos: {$to->getNu_acordos()} \n";
//                    echo "Subscription "; print_r($subscription); echo "\n";
                    echo "Transações "; var_dump($transactions); echo "\n";
                    echo "Parcela Recorrente "; var_dump($parcelaRecorrente); echo "\n";
//                    echo "Parcela Recorrente (current_transaction)"; print_r($subscription['current_transaction'])."\n";
                    echo '----------------------------------------------------------------------------------<br><br>';
//                    \Zend_Debug::dump($to->getNu_cobranca(), __FILE__ . ' - ' . __line__);
//                    \Zend_Debug::dump($nuParcelaPagarmeAtual, __FILE__ . ' - ' . __line__);
//                    \Zend_Debug::dump($subscription->status, __FILE__ . ' - ' . __line__);
//                    \Zend_Debug::dump($parcelaRecorrente, __FILE__ . ' - ' . __line__);
//                    \Zend_Debug::dump($subscription['current_transaction'], __FILE__ . ' - ' . __line__);
//                    \Zend_Debug::dump($transactions, __FILE__ . ' - ' . __line__);
                }

                if (!$parcelaRecorrente) {
                    throw new \Exception("A parcela {$to->getNuOrdem()}, cobrança {$to->getNu_cobranca()} ainda não está na Pagarme.");
                }

                $plan = $subscription['plan'];

                $pedidoTransacao = $negocio->getRepository('G2\Entity\PedidoIntegracao')->findOneBy(array('id_venda' => $to->getIdVenda()));

                if (empty($pedidoTransacao))
                    $pedidoTransacao = new \G2\Entity\PedidoIntegracao();

                $pedidoTransacao->setIdVenda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));
                $pedidoTransacao->setNuValor($parcelaRecorrente->authorized_amount);

                $situacaoIntegracao = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('st_status' => $parcelaRecorrente->status, 'id_sistema' => \G2\Constante\Sistema::PAGARME));

                $dataInicio = new \DateTime($subscription->date_created);

                $pedidoTransacao->setIdSituacaointegracao($situacaoIntegracao->getIdSituacaointegracao());
                $pedidoTransacao->setDtEnvio($dataInicio); // ?
                $pedidoTransacao->setDtInicio($dataInicio); // ?
                $pedidoTransacao->setDtProximopagamento(new \DateTime($subscription->current_period_end));
                $pedidoTransacao->setIdSistema($negocio->getReference('G2\Entity\Sistema', \G2\Constante\Sistema::PAGARME));
                $pedidoTransacao->setStBandeiracartao($subscription->card_brand);
                $pedidoTransacao->setNuDiavencimento($dataInicio->format('dd'));
                $pedidoTransacao->setNuIntervalo($plan->days);
                $pedidoTransacao->setNuTentativas($pedidoTransacao->getNuTentativas() + 1); // não temos esta informação da Pagarme
                $pedidoTransacao->setNuTentativasfeitas($pedidoTransacao->getNuTentativasfeitas() + 1); // não temos esta informação da Pagarme
                $pedidoTransacao->setStOrderid($plan->id);

                if (!$simulando) $negocio->save($pedidoTransacao);
            }

            $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));
            $lancamento->setNuVerificacao($lancamento->getNuVerificacao() + 1);
            if ($parcelaRecorrente)
                $lancamento->setStRetornoverificacao(json_encode($parcelaRecorrente));

            if (!$simulando) $negocio->save($lancamento);

            $mensagem = 'não possui parcela no recorrente Pagarme.';
            if ($parcelaRecorrente) {


                $transacaoFinanceira = new \G2\Entity\TransacaoFinanceira();

                $mensagem = 'verificado pela ' . $lancamento->getNuVerificacao() . ' vez!';
                if ($parcelaRecorrente->status == "paid" && !$simulando) {

                    if($lancamento instanceof Lancamento){
                        $lancamento->setNuQuitado((float)($parcelaRecorrente->authorized_amount / 100));
                        $lancamento->setDtQuitado(new \DateTime($parcelaRecorrente->date_updated));
                        $lancamento->setNu_assinatura($orderId);
                        $lancamento->setDt_atualizado(new \DateTime());
                        $lancamento->setBlBaixadofluxus(true);
                        $menquitar = $venda->quitarCredito(array($lancamento));
                    }

                    $transacaoFinanceira->setNu_valorautorizado((float)($parcelaRecorrente->authorized_amount / 100));

                    if ($menquitar->getTipo() != \Ead1_IMensageiro::SUCESSO) throw new \Exception($menquitar->getFirstMensagem());

                    if (!$simulando) $this->notificarParcelaRecorrentePaga($lancamento->getId_lancamento(), $to->getId_venda());

                    // Será raro cair neste caso, não tem como saber realmente como funciona
//                    $this->atualizarVencimentoLancamentosFuturos($to->getIdVenda(), $lancamento, $parcelaRecorrente->date_updated);

                    $mensagem = 'quitado com sucesso!';
                } else if ($parcelaRecorrente->status == "paid" && $simulando) {
                    $mensagem = 'quitado com sucesso!';
                }

                $transacaoFinanceira->setStMensagem(
                    'acquirer_response_code:' . $parcelaRecorrente->acquirer_response_code
                    . ',acquirer_name:' . $parcelaRecorrente->acquirer_name
                    . ',acquirer_id:' . $parcelaRecorrente->acquirer_id
                );
                $transacaoFinanceira->setDtCadastro(new \DateTime());

                $situacaoIntegracaoLancamento = $negocio->getRepository('G2\Entity\SituacaoIntegracao')->findOneBy(array('st_status' => $parcelaRecorrente->status, 'id_sistema' => \G2\Constante\Sistema::PAGARME));
                $transacaoFinanceira->setIdSituacaointegracao($situacaoIntegracaoLancamento->getIdSituacaointegracao());

                $usuario = $negocio->getRepository('G2\Entity\Usuario')->findOneBy(array('id_usuario' => 1));

                $transacaoFinanceira->setIdUsuariocadastro($usuario);
                $transacaoFinanceira->setIdVenda($negocio->getReference('G2\Entity\Venda', $to->getIdVenda()));
                $transacaoFinanceira->setststatuspagarme($parcelaRecorrente->status);
                $transacaoFinanceira->setNuVerificacao($pedidoTransacao->getNuTentativas());
                $transacaoFinanceira->setUnTransacaofinanceira('00000000-0000-0000-0000-000000000000');
                $transacaoFinanceira->setSt_idtransacaoexterna($parcelaRecorrente->id);

                if (!$simulando) $negocio->save($transacaoFinanceira);

                if (!$simulando) {
                    $transacaoLancamento = new \G2\Entity\TransacaoLancamento();
                    $transacaoLancamento->setIdLancamento($lancamento->getIdLancamento());
                    $transacaoLancamento->setIdTransacaofinanceira($transacaoFinanceira->getIdTransacaofinanceira());
                    $negocio->save($transacaoLancamento);
                }

            }
            $negocio->commit();
            return new \Ead1_Mensageiro('Pagar.me -> ' . $to->getStNomecompleto() . ' Lançamento ' . $to->getIdLancamento() . ' Vencimento ' . $to->getDtVencimento() . ': ' . $mensagem . ' OrderID: ' . $orderId, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $mensagem = null;
            try {
                $negocio->rollback();

                $mensagem = $e->getMessage();
                $vnuverificacao = 0;
                $lancamento = $negocio->getRepository('G2\Entity\Lancamento')->findOneBy(array('id_lancamento' => $to->getIdLancamento()));
                if ($bl_vencido) {
                    $vnuverificacao = $lancamento->getNuVerificacao() + 1;
                    $lancamento->setNuVerificacao($vnuverificacao);
                }
                $lancamento->setDt_atualizado(new \DateTime());
                if (!$simulando) $negocio->save($lancamento);

            } catch (\Exception $e) {
                $mensagem = $mensagem . ' Exception 2:' . $e->getMessage();
            }

            return new \Ead1_Mensageiro('Pagar.me -> ' . $to->getStNomecompleto() . ' Lançamento ' . $to->getIdLancamento() . ' Verificação ' . $vnuverificacao . ' Vencimento ' . $to->getDtVencimento() . ': ' . $mensagem . ' OrderID: ' . $orderId, \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Retorna as transações que estão dentro de uma Assinatura
     * @param $idSubscription
     * @param $idEntidade
     * @param $simulando
     * @return \Ead1_Mensageiro
     */
    public function getSubscriptionTransactions($idSubscription, $idEntidade, $simulando = false)
    {

        $transactions = null;

        try {

            // Caso for necessário simular um retorno da pagarme, habilitar este mock ( chamar a const )
            // if ($simulando) $idSubscription = 346210;


            $this->setApiKey($idEntidade);
            $subscription = \PagarMe_Subscription::findById($idSubscription);
            $request = new \PagarMe_Request(\PagarMe_Subscription::getUrl() . '/' . $idSubscription . '/transactions', 'GET');
            $request->setParameters(array('count' => 9999, 'status' => 'paid'));

            $transactions = $request->run();
            if ($transactions)
                $transactions = array_reverse($transactions);


            // Caso for necessário simular um retorno da pagarme, habilitar este mock ( chamar a const )
            // if ($simulando) $transactions = json_decode(self::mocktransactions);

            $this->mensageiro->setMensageiro(array('subscription' => $subscription, 'transactions' => $transactions), \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     *
     * Atualiza a data de vencimento dos lançamentos com base no último pago, adicionando 1 mês adianta a cada um
     *
     * @param integer $id_venda ID da venda
     * @param Lancamento $lancamento Último lançamento pago
     * @param string $dataUltimoPagamento Data de pagemtno do último lançamento pago
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function atualizarVencimentoLancamentosFuturos($id_venda, \G2\Entity\Lancamento $lancamento, $dataUltimoPagamento)
    {

        $negocio = new \G2\Negocio\Negocio();
        $negocio->beginTransaction();
        try {

            $date = new \DateTime($dataUltimoPagamento);
            $interval = new \DateInterval('P1M');
            $vwe = new \G2\Entity\VwVendaLancamento();

            $vws = $vwe->findBy(array('id_venda' => $id_venda, 'bl_quitado' => 0), array('nu_ordem' => 'ASC'), null, 1, true);

            if ($vws) {
                foreach ($vws as $vw) {
                    if ($vw instanceof \G2\Entity\VwVendaLancamento) {
                        if ($vw->getIdLancamento() <= $lancamento->getIdLancamento()) continue;

                        $lan = new Lancamento();
                        $refLan = $lan->find($vw->getIdLancamento(), true);
                        if ($refLan) {
                            if ($refLan instanceof \G2\Entity\Lancamento) {
                                $date->add($interval);
                                $refLan->setDt_vencimento($date);
                                $negocio->save($refLan);
                            }
                        }

                    }
                }
            }

            $negocio->commit();
        } catch (\Exception $e) {
            $negocio->rollback();
            throw $e;
        }

    }

    ////transactions/797136/postbacks/po_civclpu6w009ecn732xcnpthh/redeliver
    // Buscar todos os postbacks            /transactions/:transaction_id/postbacks                 GET
    // Buscar um determinado postback       /transactions/:transaction_id/postbacks/:id             GET
    // Reenviar um determinado postback     /transactions/:transaction_id/postbacks/:id/redeliver   POST
    //https://api.pagar.me/1/transactions/:id/payables.

    /**
     * @param $params
     * @return \Ead1_Mensageiro
     */
    public function quitarTransacao($params)
    {

        try {

            if ($params['current_status'] === 'paid' && $params['event'] === 'transaction_status_changed') {
                /** @var \G2\Entity\TransacaoFinanceira $transacao */
                $transacao = $this->findOneBy('\G2\Entity\TransacaoFinanceira',
                    array('st_idtransacaoexterna' => $params['id']));

                if (empty($transacao)) {
                    throw new \Exception('Registro não encontrado na base de dados. ID: ' . $params['id']);
                }

                //$transacao->setSt_codigoautorizacao($params['transaction']['authorization_code']);
                $transacao->setId_statustransacao($this->findOneBy('\G2\Entity\Situacao', array('st_situacao' => $params['current_status'])));

                $this->save($transacao);

                /** @var \G2\Entity\Venda $venda */
                $venda = $this->findOneBy('\G2\Entity\Venda', array('id_venda' => $transacao->getId_venda()));
                if (empty($venda)) {
                    throw new \Exception('Registro não encontrado na base de dados. ID: ' . $params['id'] . ' Hash: ' . $params['idboleto']);
                }

                $this->setApiKey($venda->getId_entidade());
                $transaction = \PagarMe_Transaction::findById($params['id']);

                // daqui pra baixo modificar a logica
                if (empty($transaction)) {
                    throw new \Exception('Transação não encontrada junto a Pagar.me');
                }

                if ($transaction['status'] !== 'paid') {
                    throw new \Exception('Não foi possível quitar o boleto. Status retornado: ' . $transaction['status']);
                }

                $pagamentoPagarMe = $this->retornarPagamentos($params['id'], $venda->getId_entidade());
                if ($pagamentoPagarMe instanceof \PagarMe_Object) {
                    $dt_quitado = new \DateTime($pagamentoPagarMe[0]['date_created']);
                    $dt_quitado->modify('-1 day');
                } else {
                    throw new \Zend_Exception('Erro ao quitar o Boleto: Verifique a data de retorno do pagamento.');
                }

//                $dt_vencimento = date_create($transaction['boleto_expiration_date']);
//                $dt_vencimento->modify('+1 day');
//                $nu_quitado = $transaction['paid_amount'];
//
//                /** @var \Ead1_Mensageiro $mensageiroValorAtualizado */
//                $mensageiroValorAtualizado = $this->retornarLancamentoValorAtualizado($lan);
//
//                if ($mensageiroValorAtualizado->getTipo() != \Ead1_IMensageiro::SUCESSO) {
//                    throw new \Exception('Erro ao retornar os valores atualizados do lançamento');
//                }
//
//                $diff = $dt_quitado->diff($dt_vencimento);
//                if($diff->days >= 0 && !$diff->invert) {
//                    if ( $nu_quitado < $transacao->getNu_valorenviado()) {
//                        throw new \Exception('Não foi possível quitar o boleto. Valor pago (R$ '.number_format($nu_quitado / 100, 2, '.', ',').') menor que o valor nominal (R$ '.number_format($transacao->getNu_valorenviado(), 2, '.', ',').')');
//                    }
//                } else {
//                    $lan = $mensageiroValorAtualizado->getFirstMensagem();
//
//                    //valor original do lançamento considerando juros, multa e descontos
//                    $valorLancamento = ($lan->getNu_valor() + $lan->getNu_juros() + $lan->getNu_multa()) - $lan->getNu_desconto();
//                    // pega o valor do boleto vencido considerando juros e multa se estiver setado ou o valor original
//                    $valorBoleto = (isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_lancamento_atualizado']) ? $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_lancamento_atualizado'] : $valorLancamento );
//                    // valor de descontos do boleto se existir
//                    $valor_desconto = (isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_desconto']) ? $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_desconto'] : 0);
//                    // valor de descontos da campanha de pontualidade se existir
//                    $valor_pontualidade = (isset($this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_pontualidade']) ? $this->_arrInfoLancamentos[$lan->getId_lancamento()]['valor_pontualidade'] : 0);
//
//                    if ($nu_quitado < ($valorBoleto - $valor_desconto - $valor_pontualidade )) {
//                        throw new \Exception('Não foi possível quitar o boleto. Valor pago (R$ '.number_format($nu_quitado / 100, 2, '.', ',').') menor que o valor nominal (R$ '.number_format($valorBoleto, 2, '.', ',').')');
//                    }
//                }

                $laTO = new \LancamentoTO();
                $laTO->setId_lancamento($transaction['metadata']['id_lancamento']);
                $laTO->fetch(true, true, true);

                $laTO->setNu_quitado($transaction['paid_amount'] / 100);
                $laTO->setBl_baixadofluxus(true);
                $laTO->setSt_statuslan(1);
                $laTO->setId_usuarioquitacao(1);
                $laTO->setDt_quitado($dt_quitado);

                $fiBO = new \VendaBO();

                $me = $fiBO->quitarBoleto($laTO);
                if ($me->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Zend_Exception('Erro ao quitar o Boleto: ' . $me->getFirstMensagem());
                }

                $this->mensageiro->setMensageiro('Boleto quitado com sucesso', \Ead1_IMensageiro::SUCESSO);

            } else {
                $this->mensageiro->setMensageiro('Não foi possível quitar a transação. Status retornado: ' . $params['current_status'], \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    /**
     * @param $id_transacao
     * @param $idEntidade
     * @return \PagarMe_Object
     * @throws \Exception
     */
    public function retornarPagamentos($id_transacao, $idEntidade)
    {
        try {
            $this->setApiKey($idEntidade);

            $request = new \PagarMe_Request('/transactions/' . $id_transacao . '/payables', 'GET');
            $response = $request->run();
            $object = new \PagarMe_Object($response);

            return $object;

        } catch (\Exception $error) {
            throw $error;
        }
    }

    /**
     * @return mixed
     */
    public function createAccountBank($params)
    {
        try {
            $this->setApiKey($params['id_entidade']);

            $account = new \PagarMe_Recipient(
                array(
                    "transfer_interval" => "monthly",
                    "transfer_day" => 5,
                    "transfer_enabled" => true,
                    "bank_account" =>
                        array(
                            "bank_code" => $params['bank_code'],
                            "agencia" => $params['agencia'],
                            "agencia_dv" => !empty($params['agencia_dv']) ? $params['agencia_dv'] : null,
                            "conta" => $params['conta'],
                            "conta_dv" => $params['conta_dv'],
                            "type" => $params['type'],
                            "document_number" => str_replace('.', '', str_replace('/', '', str_replace('-', '', $params['document_number']))),
                            "legal_name" => $params['legal_name']
                        )
                )
            );
            $account->create();
            return 'Conta criado com sucesso';
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    public function getAllAccountBank($params)
    {

        try {
            $this->setApiKey($params['id_entidade']);
            //1/bank_accounts
            $request = new \PagarMe_Request('/recipients', 'GET');
            return $response = $request->run();
            //return \PagarMe_Bank_Account::all();
        } catch (\Exception $error) {
            return $error->getMessage();
        }
    }

    public function reenviarpostbacklote($idEntidade, $idTransacao)
    {
        try {

            $this->setApiKey($idEntidade);

            if (!empty($idTransacao)) {
                $transacoes = $this->findBy('\G2\Entity\TransacaoFinanceira', array('st_idtransacaoexterna' => $idTransacao));
            } else {
                $stmt = $this->em->getConnection()
                    ->prepare('SELECT * FROM tb_transacaofinanceira lf
                                            WHERE  lf.id_statustransacao = 189
                                            AND lf.id_transacaoexterna IS NOT NULL'
                    );

                $stmt->execute();
                $transacoes = $stmt->fetchAll();
            }

            /** @var \G2\Entity\TransacaoFinanceira $transacao */
            foreach ($transacoes as $transacao) {
                echo '<pre>';

                $postbacks = $this->getAllPostbacks(is_object($transacao) ? $transacao->getSt_idtransacaoexterna() : $transacao['id_transacaoexterna']);
                foreach ($postbacks as $postback) {
                    echo 'Transação: ';
                    \Doctrine\Common\Util\Debug::dump($postback['model_id']);
                    echo '<br>';
                    $this->reenviarPostback(is_object($transacao) ? $transacao->getSt_idtransacaoexterna() : $transacao['id_transacaoexterna'], $postback['id'], $idEntidade);
                }

            }
            die('postbacks reenviados com sucesso');
            return $this->mensageiro;

        } catch (\Exception $error) {
            throw $error;
        }
    }

    public function getAllPostbacks($transaction_id)
    {
        try {
            $request = new \PagarMe_Request('/transactions/' . $transaction_id . '/postbacks', 'GET');
            return $response = $request->run();
        } catch (\Exception $error) {
            return $error->getMessage();
        };
    }

    /**
     * @param $id_transacao
     * @param $id_post
     * @param $idEntidade
     * @return \PagarMe_Object
     * @throws \Exception
     */
    public function reenviarPostback($id_transacao, $id_post, $idEntidade)
    {
        try {

            $this->setApiKey($idEntidade);

            //var_dump('/transactions/' . $id_transacao . '/postbacks/'.$id_post.'/redeliver');die;
            $request = new \PagarMe_Request('/transactions/' . $id_transacao . '/postbacks/' . $id_post . '/redeliver', 'POST');
            $response = $request->run();
            $object = new \PagarMe_Object($response);

            return $object;

        } catch (\Exception $error) {
            throw $error;
        }
    }

    /**
     * @param array $data - Demais dados para completar o processo
     * @param array $transData - Dados para captura da transacao
     * @return \Ead1_Mensageiro|Ead1_Mensageiro
     */
    public function transactionCreditCard(array $data, array $transData)
    {
        try {
            $idVenda = (integer)$data['id_venda'];

            $idLancamento = (integer)!empty($data['id_lancamento']) ? $data['id_lancamento'] : null;
            $idAcordo = (integer)!empty($data['id_acordo']) ? $data['id_acordo'] : null;

            unset($data['id_lancamento']);
            unset($data['id_acordo']);

            if (is_null($idVenda) || $idVenda == 0) {
                throw new \InvalidArgumentException(MensagemSistema::INFORME_ID_VENDA);
            }
            $idVenda = (int)$data['id_venda'];

            if (is_null($idVenda) || $idVenda == 0)
                throw new \InvalidArgumentException('Informe o id_venda para realizar o pagamento');

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->find('\G2\Entity\Venda', $idVenda);
            if (empty($venda)) {
                throw new \InvalidArgumentException(MensagemSistema::VENDA_NAO_ENCONTRADA);
            }

            $pagBO = new \PagamentoBO();
            $pagBO->verificaPossibilidadePagamento($venda->getId_venda());

            /** @var \G2\Entity\VwLancamento $vwLancamento */
            $vwLancamento = $this->findOneBy('\G2\Entity\VwLancamento', array('id_venda' => $idVenda));

            if ($vwLancamento && $vwLancamento->getId_meiopagamento() == MeioPagamento::CARTAO_RECORRENTE) {
                throw new \Exception(MensagemSistema::ERRO_PRODUTO_CARTAO_RECORRENTE);
            }

            $this->setApiKey($venda->getId_entidade());
            $token = sha1(uniqid($venda->getId_entidade() . $venda->getId_venda() . time(), true));

            /** @var \G2\Entity\VwPessoa $uTo */
            $uTo = $this->findOneBy('\G2\Entity\VwPessoa',
                array(
                    'id_usuario' => $venda->getId_usuario(),
                    'id_entidade' => $venda->getId_entidade()
                )
            );

            if (!$transData) {
                throw new \Exception('Parâmetros para captura não encontrados!');
            }

            $transaction = $this->captureTransactionWithHash($transData, $venda->getId_entidade());
            $cartaoBandeira = $this->findOneBy('\G2\Entity\CartaoBandeira', array('st_cartaobandeira' => $transaction->card_brand));

            if (!$cartaoBandeira) {
                throw new \Exception('Nenhuma bandeira configurada!');
            }

            $pagCartaoBao = new \PagamentoCartaoBO($venda->getId_entidade(), $cartaoBandeira->getId_cartaobandeira(), $data['id_meiopagamento']);

            # caso se trate de uma renegociacao, será realizado o registro dos lancamentos
            # tbm no Fluxus
            if (array_key_exists('id_acordo', $data) && $data['id_acordo']) {
                $negociacao = new Negociacao();
                $negociacao->registraBaixaLancamentoFluxos((object)array(
                    'id_venda' => $idVenda,
                    'id_acordo' => (integer)$data['id_acordo'], 'id_transacaoexterna' => $transaction['tid']));
            }

            if ($transaction->status != \G2\Constante\StatusLancamento::PAID) {

                //retira a venda do status processamento para possibilidade de novo pagamento.
                $venda->setBl_processamento(false);
                $this->save($venda);

                $pagCartaoBao->resetaVendaTransacao($venda->getId_venda());
                throw new \Exception('Pagamento não Autorizado!');
            }

            $data['nu_parcelas'] = $transaction->installments;
            $data['status'] = $transaction->status;
            $pagCartaoBao->_procedimentoPosAutorizarCartao($data, $data['nu_cartao'], false);

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_venda' => $venda->getId_venda(),
                    'id_entidade' => $venda->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::CARTAO_CREDITO,
                    'bl_entrada' => true
                )
            );

            if (!empty($idAcordo) && !empty($idLancamento)) {
                /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
                $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                    array(
                        'id_venda' => $venda->getId_venda(),
                        'id_entidade' => $venda->getId_entidade(),
                        'id_meiopagamento' => MeioPagamento::CARTAO_CREDITO,
                        'id_lancamento' => $idLancamento,
                        'id_acordo' => $idAcordo
                    )
                );
            }


            $this->salvarTransacaoFinanceira($transaction, $vwResumo, $venda);

            $this->mensageiro->setMensageiro(MensagemSistema::PAGAMENTO_AUTORIZADO, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $error) {

            if (!empty($transaction))
                $this->salvarTransacaoFinanceira($transaction, null, $venda);

            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    public function transactionSubscription(array $data)
    {
        try {
            $idVenda = (int)$data['id_venda'];
            if (is_null($idVenda) || $idVenda == 0)
                throw new \InvalidArgumentException('Informe o id_venda para realizar o pagamento');

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->findOneBy('\G2\Entity\Venda', array('id_venda' => $idVenda));
            if (empty($venda))
                throw new \InvalidArgumentException('Venda não informada');

            $pagBO = new \PagamentoBO();
            $pagBO->verificaPossibilidadePagamento($venda->getId_venda());

            /** @var \G2\Entity\EntidadeIntegracao $entidadeIntegracao */
            $entidadeIntegracao = parent::findOneBy('\G2\Entity\EntidadeIntegracao', array('id_entidade' => $venda->getId_entidade(), 'id_sistema' => \G2\Constante\Sistema::PAGARME));

            $this->setApiKey($venda->getId_entidade());
            $token = sha1(uniqid($venda->getId_entidade() . $venda->getId_venda() . time(), true));

            /** @var \G2\Entity\VwPessoa $uTo */
            $uTo = $this->findOneBy('\G2\Entity\VwPessoa',
                array(
                    'id_usuario' => $venda->getId_usuario(),
                    'id_entidade' => $venda->getId_entidade()
                )
            );

            $pv = new \VwProdutoVendaTO();
            $pv->setId_venda($venda->getId_venda());
            $pv->fetch(false, true, true);

            //var_dump($data);die;
            $data['nu_valortotal'] = ($venda->getNu_valorbruto() / $data['nu_parcelas']) * 100;
            $plan = new \PagarMe_Plan(array(
                "amount" => $data['nu_valortotal'],
                "trial_days" => 0,
                "days" => 30,
                "name" => $pv->st_produto,
                "charges" => ($data['nu_parcelas'] - 1),
                "payment_methods" => array(\G2\Constante\PagarmePaymentMethod::CREDIT_CARD)
            ));

            $plan->create();

            $subscription = new \PagarMe_Subscription(array(
                "plan" => $plan,
                "payment_method" => \G2\Constante\PagarmePaymentMethod::CREDIT_CARD,
                "card_hash" => $data['card_hash'],
                'async' => 'false',
                'metadata' => array(
                    'id_venda' => $venda->getId_venda()
                ),
                'customer' => array(
                    //Nome completo ou razão social do cliente que está realizando a transação
                    'name' => $uTo->getst_nomecompleto(),
                    //CPF ou CNPJ do cliente, sem separadores
                    'document_number' => $uTo->getst_cpf(),
                    'email' => $uTo->getst_email()
                ),
            ));

            $subscription->create();

            $pagCartaoBao = new \PagamentoCartaoBO($venda->getId_entidade(), $data['id_cartaobandeira'], $data['id_meiopagamento']);

            if ($subscription->status == \G2\Constante\StatusLancamento::REFUSED) {
                $pagCartaoBao->resetaVendaTransacao($venda->getId_venda());
                throw new \Exception('Pagamento não autorizado!');
            }

            $venda->setRecorreteOrderid($subscription->id);
            $this->save($venda);

            $data['id_modelovenda'] = \G2\Entity\ModeloVenda::ASSINATURA;
            $pagCartaoBao->_procedimentoPosAutorizarCartao($data);

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_venda' => $venda->getId_venda(),
                    'id_entidade' => $venda->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::CARTAO_RECORRENTE,
                    'bl_entrada' => true
                )
            );

            $this->salvarTransacaoFinanceira($subscription, $vwResumo);
        } catch (\Exception $error) {
            if ($subscription)
                $this->salvarTransacaoFinanceira($subscription, null, $venda);

            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    public function captureTransaction($token, $amount, $id_entidade, array $data)
    {
        try {
            $this->setApiKey($id_entidade);
            $transaction = \PagarMe_Transaction::findById($token);
            $transaction->capture(array('amount' => $amount, 'metadata' => $data));
            return $transaction;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function captureTransactionWithHash($dados, $id_entidade)
    {
        PagarMe::setApiKey($id_entidade);
        $transaction = new \PagarMe_Transaction($dados);
        $transaction->charge();
        return $transaction;
    }

    public function createPlanPayment($amount, $id_entidade, $days, $id_venda, $parcelas)
    {
        try {
            $this->setApiKey($id_entidade);
            $plan = new \PagarMe_Plan(array(
                'amount' => $amount,
                'days' => $days,
                'name' => "Plano Recorrente - Venda: " . $id_venda,
                'charges' => $parcelas
            ));
            return $plan->create();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function createSubscription($id_entidade, $id_plano, $hash, $email)
    {
        try {
            $this->setApiKey($id_entidade);

            $subscription = new \PagarMe_Subscription(array(
                "payment_method" => "credit_card",
                "plan_id" => $id_plano,
                "card_hash" => $hash,
                'customer' => array(
                    'email' => $email
                )));

            $subscription->create();
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function mudarCartaoAssinatura($params)
    {

        try {

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->findOneBy('\G2\Entity\Venda', array('id_venda' => $params['id_venda']));
            if (empty($venda))
                throw new \InvalidArgumentException('Venda não informada');

            $this->setApiKey($venda->getId_entidade());

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_venda' => $venda->getId_venda(),
                    'id_entidade' => $venda->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::CARTAO_RECORRENTE
                )
            );

            if (!$vwResumo)
                throw new \Zend_Exception('Nenhum dado financeiro encontrado para essa venda');

            $request = new \PagarMe_Request('/subscriptions/' . $vwResumo->getRecorrente_orderid(), 'PUT');
            $request->setParameters(
                array(
                    'payment_method' => \G2\Constante\PagarmePaymentMethod::CREDIT_CARD,
                    'card_hash' => $params['card_hash'],
                    'card_number' => $params['st_cartao'],
                    'card_holder_name' => $params['st_nomeimpresso'],
                    'card_expiration_date' => str_pad($params['nu_mesvalidade'], 2, 0, STR_PAD_LEFT) . substr($params['nu_anovalidade'], 2, 2),
                )
            );

            $response = $request->run();
            $object = new \PagarMe_Object($response);

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_venda' => $venda->getId_venda(),
                    'id_entidade' => $venda->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::CARTAO_RECORRENTE,
                    'bl_entrada' => true
                )
            );

            if (!$vwResumo)
                throw new \Zend_Exception('Nenhum dado financeiro encontrado para essa venda');

            $this->salvarTransacaoFinanceira($object, $vwResumo);

            $this->mensageiro->setMensageiro('Dados do cartão alterado com sucesso', \Ead1_IMensageiro::SUCESSO, $object);

        } catch (\Exception $error) {
            $this->mensageiro->setMensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO, (object)$error->getErrors()[0]);
        };

        return $this->mensageiro;

    }

    public function processarpostback($params)
    {

        if ($params['current_status'] === 'waiting_payment') {
            return $this->atualizarStatusBoleto($params);
        } else {
            return $this->quitarTransacao($params);
        }

    }

    public function atualizarStatusBoleto($params)
    {

        try {
            /** @var \G2\Entity\TransacaoFinanceira $transacao */
            $transacao = $this->findOneBy('\G2\Entity\TransacaoFinanceira',
                array('st_idtransacaoexterna' => $params['id']));

            if (empty($transacao)) {
                throw new \Exception('Registro não encontrado na base de dados. ID: ' . $params['id']);
            }

            /** @var \G2\Entity\Venda $venda */
            $venda = $this->findOneBy('\G2\Entity\Venda', array('id_venda' => $transacao->getId_venda()));
            if (empty($venda)) {
                throw new \Exception('Registro não encontrado na base de dados. ID: ' .
                    $params['id'] . ' Hash: ' . $params['idboleto']);
            }

            $this->setApiKey($venda->getId_entidade());
            $transaction = \PagarMe_Transaction::findById($params['id']);

            // daqui pra baixo modificar a logica
            if (empty($transaction)) {
                throw new \Exception('Transação não encontrada junto a Pagar.me');
            }

            /** @var \G2\Entity\VwResumoFinanceiro $vwResumo */
            $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                array(
                    'id_lancamento' => $transaction['metadata']['id_lancamento'],
                    'id_entidade' => $venda->getId_entidade(),
                    'id_meiopagamento' => MeioPagamento::BOLETO
                )
            );

            $this->mensageiro = $this->salvarTransacaoFinanceira(
                $transaction,
                $vwResumo,
                $venda,
                false
            );

            $recebimentoNegocio = new Recebimento();
            $result = $recebimentoNegocio->enviarBoleto($vwResumo->getId_lancamento());

        } catch (\Exception $message) {
            $this->mensageiro->setMensageiro($message->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $this->mensageiro;
    }

    protected function request($path, $method, array $parameters = array())
    {
        try {
            $request = new \PagarMe_Request($path, $method);

            if (!empty($parameters)) {
                $request->setParameters($parameters);
            }

            $response = $request->run();
            $object = new \PagarMe_Object($response);

            return new \Ead1_Mensageiro($object, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $error) {
            throw $error;
        }
    }

    public function getSubscription($idSubscription, $idEntidade)
    {
        try {
            $this->setApiKey($idEntidade);

            return $this->request('/subscriptions/' . $idSubscription, 'GET');
        } catch (\Exception $error) {
            throw $error;
        }
    }

    public function settleCharge($idSubscription, $idEntidade, $charges)
    {
        try {

            $this->setApiKey($idEntidade);

            $assinatura = $this->getSubscription($idSubscription, $idEntidade)->getFirstMensagem();

            $settleCharge = $this->request(
                '/subscriptions/' . $assinatura->id . '/settle_charge',
                'POST',
                array('charges' => $charges)
            );

            return new \Ead1_Mensageiro($settleCharge, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $error) {
            return new \Ead1_Mensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function mudarAssinatura()
    {
        try {
            //try

            $assinatura = $this->request(
                '/subscriptions/591330',
                'PUT',
                array('payment_method' => 'boleto')
            );
            echo '<pre>';
            var_dump($assinatura);

        } catch (\Exception $error) {
            return new \Ead1_Mensageiro($error->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Este método serve para reativar uma assinatura cancelada. Na verdade ele cria um novo plano e uma assinatura para este novo plano
     * @param array $data - São os dados vindos da tela de troca de cartão, contem as informações do cartão do cliente
     * @return \Ead1_Mensageiro|Ead1_Mensageiro
     */
    public function trocarCartaoAssinaturaCancelada($data)
    {
        try {
            // busca a chave da pagarme
            $entidadeConfiguracao = $this->findOneBy('\G2\Entity\EntidadeIntegracao',
                array(
                    'id_sistema' => \G2\Constante\Sistema::PAGARME,
                    'id_entidade' => $data['id_entidade']
                )
            );

            // antes de criar um plano, verifica o valor total dele e quantas parcelas ainda faltam
            $parcelasAPagar = $this->getRepository('G2\Entity\VwVendaLancamento')->recuperaPlanoEValorRestante($data['id_venda']);

            if (empty($parcelasAPagar)) {
                throw new \Exception('Não existe plano ativo para este usuário');
            }
            $valorTotal = str_replace('.', '', $parcelasAPagar[0]['valor_total']);
            $quantidade = $parcelasAPagar[0]['quantidade'];

            if (empty($quantidade)) {
                throw new \Exception("Não é posível renovar o plano, pois não existem mais parcelas à pagar.");
            }

            // cria um novo plano
            $dadosPlano = array(
                'amount' => $valorTotal / $quantidade,
                'days' => 30,
                'charges' => $quantidade - 1,
                // o número de cobranças do plano tem que ser feito desconsiderando a primeira cobrança, que é feita no ato da assinatura
                'name' => "Renovação plano cancelado de {$data['st_nomecompleto']} - ID: {$data['id_usuario']}"
            );

            $plan = new \PagarMe_Plan($dadosPlano);
            $resultadoPlano = $plan->create();

            // caso haja sucesso na criação do plano, cria uma nova assinatura para este plano. Do contrário, informa do erro
            if (!is_numeric($resultadoPlano['id'])) {
                throw new \Exception('Não foi possível renovar o plano, tente novamente mais tarde.');
            }

            $subscription = new \PagarMe_Subscription( // cria a nova assinatura
                array(
                    'api_key' => $entidadeConfiguracao->getSt_codchave(),
                    'plan_id' => $resultadoPlano['id'],
                    'payment_method' => 'credit_card',
                    'customer' => array('email' => "{$data['st_email']}"),
                    'card_number' => $data['st_cartao'],
                    'card_holder_name' => $data['st_nomeimpresso'],
                    'card_expiration_month' => str_pad($data['nu_mesvalidade'], 2, 0, STR_PAD_LEFT),
                    'card_expiration_year' => substr($data['nu_anovalidade'], 2, 2),
                    'card_cvv' => $data['st_codigoseguranca'],
                )
            );

            $subscription->create();

            // caso o pagamento tenha sido efetuado, atualiza a parcela subsequente que ainda não foi paga e a data de vencimento das demais.
            if ($subscription->current_transaction->status == 'paid') {
                $data_vencimento = new \DateTime();
                foreach ($parcelasAPagar as $key => $index) {
                    $lancamento = $this->findOneBy('\G2\Entity\Lancamento',
                        array('id_lancamento' => $index['id_lancamento']));

                    if ($key == 0) { // o index 0 representa a parcela que foi paga na renovação do plano cancelado, portanto marca ela como paga
                        $lancamento->setBl_quitado(1);
                        $lancamento->setDt_quitado(new \DateTime());
                        $lancamento->setNu_quitado($subscription->current_transaction->paid_amount / 100);
                        $lancamento->setSt_idtransacaoexterna($subscription->current_transaction->id);

                        $vwResumo = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
                            array(
                                'id_venda' => $data['id_venda'],
                                'id_entidade' => $data['id_entidade'],
                                'id_meiopagamento' => 11, //mudar para constante
                                'id_lancamento' => $index['id_lancamento']
                            )
                        );

                        $this->salvarTransacaoFinanceira($subscription, $vwResumo, null, true, false);
                    }
                    $lancamento->setDt_vencimento($data_vencimento);
                    $lancamento->setNu_cobranca(++$key);
                    $lancamento->setNu_assinatura($subscription->current_transaction->subscription_id);

                    $this->save($lancamento);
                    $data_vencimento->add(date_interval_create_from_date_string("30 days"));
                }

                // atualiza a tb_venda para armazenar o id da assinatura vigente
                $venda = $this->findOneBy('\G2\Entity\Venda', array('id_venda' => $data['id_venda']));
                $venda->setRecorreteOrderid($subscription->id);
                $this->save($venda);
            }
            $this->mensageiro->setMensageiro("A assinatura foi renovada com sucesso", \Ead1_IMensageiro::SUCESSO);
            return $this->mensageiro;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }


    /**
     * Método que envia  uma mensagem para o cliente confirmando o pagamento
     * este método não dispara Exceções em caso de falha para evitar que o processo de verificação pare.
     * @param $id_lancamento
     * @param $id_venda
     * @return bool
     */
    public function notificarParcelaRecorrentePaga($id_lancamento, $id_venda)
    {
        try {

            if (!$id_lancamento) return false;

            $lancamentoTO = new \LancamentoTO();
            $lancamentoTO->setId_lancamento($id_lancamento);
            $lancamentoTO->fetch(true, true, true);

            if (!$lancamentoTO->getId_entidade()) return false;

            $mensagemBO = new \MensagemBO();
            $retorno = $mensagemBO->procedimentoGerarEnvioMensagemPadraoEntidade(\MensagemPadraoTO::AVISO_RECORRENTE_PAGO
                , $lancamentoTO
                , \TipoEnvioTO::EMAIL
                , false
                , 0
                , array('id_venda' => $id_venda));

            if ($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                return true;
            }

            return false;

        } catch (\Exception $e) {

            return false;
        }

    }

    const mocktransactions = '[
    {
        "object": "transaction",
        "status": "paid",
        "refuse_reason": null,
        "status_reason": "acquirer",
        "acquirer_response_code": "0000",
        "acquirer_name": "pagarme",
        "acquirer_id": "58a49047916d40fa539ba926",
        "authorization_code": "737870",
        "soft_descriptor": null,
        "tid": 1836030,
        "nsu": 1836030,
        "date_created": "2017-08-15T16:14:58.903Z",
        "date_updated": "2017-08-15T16:14:59.179Z",
        "amount": 10000,
        "authorized_amount": 10000,
        "paid_amount": 0,
        "refunded_amount": 0,
        "installments": 1,
        "id": 1836030,
        "cost": 0,
        "card_holder_name": "Morpheus Fishburne",
        "card_last_digits": "1111",
        "card_first_digits": "411111",
        "card_brand": "visa",
        "card_pin_mode": null,
        "postback_url": null,
        "payment_method": "credit_card",
        "capture_method": "ecommerce",
        "antifraud_score": null,
        "boleto_url": null,
        "boleto_barcode": null,
        "boleto_expiration_date": null,
        "referer": "api_key",
        "ip": "10.2.13.68",
        "subscription_id": null,
        "phone": null,
        "address": null,
        "customer": {
        "object": "customer",
            "id": 234275,
            "external_id": "#3311",
            "type": "individual",
            "country": "br",
            "document_number": null,
            "document_type": "cpf",
            "name": "Morpheus Fishburne",
            "email": "mopheus@nabucodonozor.com",
            "phone_numbers": [
            "+5511999998888",
            "+5511888889999"
        ],
            "born_at": null,
            "birthday": "1965-01-01",
            "gender": null,
            "date_created": "2017-08-15T16:14:58.815Z",
            "documents": [
                {
                    "object": "document",
                    "id": "doc_cj6dsh3bj0mlj6m6dmm39mqmb",
                    "type": "cpf",
                    "number": "00000000000"
                }
            ]
        },
        "billing": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146613
            },
            "object": "billing",
            "id": 35,
            "name": "Trinity Moss"
        },
        "shipping": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146614
            },
            "object": "shipping",
            "id": 29,
            "name": "Neo Reeves",
            "fee": 3311,
            "delivery_date": "2000-12-21",
            "expedited": true
        },
        "items": [],
        "card": {
        "object": "card",
            "id": "card_cj6dsh3d90mlk6m6d951q4wil",
            "date_created": "2017-08-15T16:14:58.894Z",
            "date_updated": "2017-08-15T16:14:58.894Z",
            "brand": "visa",
            "holder_name": "Morpheus Fishburne",
            "first_digits": "411111",
            "last_digits": "1111",
            "country": "UNITED STATES",
            "fingerprint": "3ace8040fba3f5c3a0690ea7964ea87d97123437",
            "valid": null,
            "expiration_date": "0922"
        },
        "split_rules": [
            {
                "object": "split_rule",
                "id": "sr_cj6dsh3eg0mlm6m6dh6r036bk",
                "liable": true,
                "amount": null,
                "percentage": 50,
                "recipient_id": "re_cj6dsgxiz0mlh6m6d6e20rvuy",
                "charge_remainder": false,
                "charge_processing_fee": true,
                "date_created": "2017-08-15T16:14:58.936Z",
                "date_updated": "2017-08-15T16:14:58.936Z"
            },
            {
                "object": "split_rule",
                "id": "sr_cj6dsh3ef0mll6m6dxk40wxw8",
                "liable": true,
                "amount": null,
                "percentage": 50,
                "recipient_id": "re_cj6dsc0i90o56yy6erybyxs9p",
                "charge_remainder": true,
                "charge_processing_fee": true,
                "date_created": "2017-08-15T16:14:58.936Z",
                "date_updated": "2017-08-15T16:14:58.936Z"
            }
        ],
        "antifraud_metadata": {},
        "reference_key": null,
        "metadata": {}
    },
    {
        "object": "transaction",
        "status": "paid",
        "refuse_reason": null,
        "status_reason": "acquirer",
        "acquirer_response_code": "0000",
        "acquirer_name": "pagarme",
        "acquirer_id": "58a49047916d40fa539ba926",
        "authorization_code": "688274",
        "soft_descriptor": null,
        "tid": 1835912,
        "nsu": 1835912,
        "date_created": "2017-08-15T15:58:31.733Z",
        "date_updated": "2017-08-15T15:58:53.177Z",
        "amount": 10000,
        "authorized_amount": 10000,
        "paid_amount": 0,
        "refunded_amount": 0,
        "installments": 1,
        "id": 1835912,
        "cost": 0,
        "card_holder_name": "Morpheus Fishburne",
        "card_last_digits": "1111",
        "card_first_digits": "411111",
        "card_brand": "visa",
        "card_pin_mode": null,
        "postback_url": null,
        "payment_method": "credit_card",
        "capture_method": "ecommerce",
        "antifraud_score": null,
        "boleto_url": null,
        "boleto_barcode": null,
        "boleto_expiration_date": null,
        "referer": "api_key",
        "ip": "10.2.14.195",
        "subscription_id": null,
        "phone": null,
        "address": null,
        "customer": {
        "object": "customer",
            "id": 234265,
            "external_id": "#3311",
            "type": "individual",
            "country": "br",
            "document_number": null,
            "document_type": "cpf",
            "name": "Morpheus Fishburne",
            "email": "mopheus@nabucodonozor.com",
            "phone_numbers": [
            "+5511999998888",
            "+5511888889999"
        ],
            "born_at": null,
            "birthday": "1965-01-01",
            "gender": null,
            "date_created": "2017-08-15T15:58:31.636Z",
            "documents": [
                {
                    "object": "document",
                    "id": "doc_cj6drvxlw0lqn696dtbbwri6y",
                    "type": "cpf",
                    "number": "00000000000"
                }
            ]
        },
        "billing": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146609
            },
            "object": "billing",
            "id": 34,
            "name": "Trinity Moss"
        },
        "shipping": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146610
            },
            "object": "shipping",
            "id": 28,
            "name": "Neo Reeves",
            "fee": 3311,
            "delivery_date": "2000-12-21",
            "expedited": true
        },
        "items": [
            {
                "object": "item",
                "id": "1",
                "title": "Red pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            },
            {
                "object": "item",
                "id": "a123",
                "title": "Blue pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            }
        ],
        "card": {
        "object": "card",
            "id": "card_cj6drvxnm0lqo696dirob3ibk",
            "date_created": "2017-08-15T15:58:31.714Z",
            "date_updated": "2017-08-15T15:58:32.098Z",
            "brand": "visa",
            "holder_name": "Morpheus Fishburne",
            "first_digits": "411111",
            "last_digits": "1111",
            "country": "UNITED STATES",
            "fingerprint": "3ace8040fba3f5c3a0690ea7964ea87d97123437",
            "valid": true,
            "expiration_date": "0922"
        },
        "split_rules": null,
        "antifraud_metadata": {},
        "reference_key": null,
        "metadata": {}
    },
    {
        "object": "transaction",
        "status": "paid",
        "refuse_reason": null,
        "status_reason": "acquirer",
        "acquirer_response_code": "0000",
        "acquirer_name": "pagarme",
        "acquirer_id": "58a49047916d40fa539ba926",
        "authorization_code": "688274",
        "soft_descriptor": null,
        "tid": 1835912,
        "nsu": 1835912,
        "date_created": "2017-08-15T15:58:31.733Z",
        "date_updated": "2017-08-15T15:58:53.177Z",
        "amount": 10000,
        "authorized_amount": 10000,
        "paid_amount": 0,
        "refunded_amount": 0,
        "installments": 1,
        "id": 1835912,
        "cost": 0,
        "card_holder_name": "Morpheus Fishburne",
        "card_last_digits": "1111",
        "card_first_digits": "411111",
        "card_brand": "visa",
        "card_pin_mode": null,
        "postback_url": null,
        "payment_method": "credit_card",
        "capture_method": "ecommerce",
        "antifraud_score": null,
        "boleto_url": null,
        "boleto_barcode": null,
        "boleto_expiration_date": null,
        "referer": "api_key",
        "ip": "10.2.14.195",
        "subscription_id": null,
        "phone": null,
        "address": null,
        "customer": {
        "object": "customer",
            "id": 234265,
            "external_id": "#3311",
            "type": "individual",
            "country": "br",
            "document_number": null,
            "document_type": "cpf",
            "name": "Morpheus Fishburne",
            "email": "mopheus@nabucodonozor.com",
            "phone_numbers": [
            "+5511999998888",
            "+5511888889999"
        ],
            "born_at": null,
            "birthday": "1965-01-01",
            "gender": null,
            "date_created": "2017-08-15T15:58:31.636Z",
            "documents": [
                {
                    "object": "document",
                    "id": "doc_cj6drvxlw0lqn696dtbbwri6y",
                    "type": "cpf",
                    "number": "00000000000"
                }
            ]
        },
        "billing": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146609
            },
            "object": "billing",
            "id": 34,
            "name": "Trinity Moss"
        },
        "shipping": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146610
            },
            "object": "shipping",
            "id": 28,
            "name": "Neo Reeves",
            "fee": 3311,
            "delivery_date": "2000-12-21",
            "expedited": true
        },
        "items": [
            {
                "object": "item",
                "id": "1",
                "title": "Red pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            },
            {
                "object": "item",
                "id": "a123",
                "title": "Blue pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            }
        ],
        "card": {
        "object": "card",
            "id": "card_cj6drvxnm0lqo696dirob3ibk",
            "date_created": "2017-08-15T15:58:31.714Z",
            "date_updated": "2017-08-15T15:58:32.098Z",
            "brand": "visa",
            "holder_name": "Morpheus Fishburne",
            "first_digits": "411111",
            "last_digits": "1111",
            "country": "UNITED STATES",
            "fingerprint": "3ace8040fba3f5c3a0690ea7964ea87d97123437",
            "valid": true,
            "expiration_date": "0922"
        },
        "split_rules": null,
        "antifraud_metadata": {},
        "reference_key": null,
        "metadata": {}
    },
    {
        "object": "transaction",
        "status": "paid",
        "refuse_reason": null,
        "status_reason": "acquirer",
        "acquirer_response_code": "0000",
        "acquirer_name": "pagarme",
        "acquirer_id": "58a49047916d40fa539ba926",
        "authorization_code": "688274",
        "soft_descriptor": null,
        "tid": 1835912,
        "nsu": 1835912,
        "date_created": "2017-08-15T15:58:31.733Z",
        "date_updated": "2017-08-15T15:58:53.177Z",
        "amount": 10000,
        "authorized_amount": 10000,
        "paid_amount": 0,
        "refunded_amount": 0,
        "installments": 1,
        "id": 1835912,
        "cost": 0,
        "card_holder_name": "Morpheus Fishburne",
        "card_last_digits": "1111",
        "card_first_digits": "411111",
        "card_brand": "visa",
        "card_pin_mode": null,
        "postback_url": null,
        "payment_method": "credit_card",
        "capture_method": "ecommerce",
        "antifraud_score": null,
        "boleto_url": null,
        "boleto_barcode": null,
        "boleto_expiration_date": null,
        "referer": "api_key",
        "ip": "10.2.14.195",
        "subscription_id": null,
        "phone": null,
        "address": null,
        "customer": {
        "object": "customer",
            "id": 234265,
            "external_id": "#3311",
            "type": "individual",
            "country": "br",
            "document_number": null,
            "document_type": "cpf",
            "name": "Morpheus Fishburne",
            "email": "mopheus@nabucodonozor.com",
            "phone_numbers": [
            "+5511999998888",
            "+5511888889999"
        ],
            "born_at": null,
            "birthday": "1965-01-01",
            "gender": null,
            "date_created": "2017-08-15T15:58:31.636Z",
            "documents": [
                {
                    "object": "document",
                    "id": "doc_cj6drvxlw0lqn696dtbbwri6y",
                    "type": "cpf",
                    "number": "00000000000"
                }
            ]
        },
        "billing": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146609
            },
            "object": "billing",
            "id": 34,
            "name": "Trinity Moss"
        },
        "shipping": {
        "address": {
            "object": "address",
                "street": "Rua Matrix",
                "complementary": null,
                "street_number": "9999",
                "neighborhood": "Rio Cotia",
                "city": "Cotia",
                "state": "sp",
                "zipcode": "06714360",
                "country": "br",
                "id": 146610
            },
            "object": "shipping",
            "id": 28,
            "name": "Neo Reeves",
            "fee": 3311,
            "delivery_date": "2000-12-21",
            "expedited": true
        },
        "items": [
            {
                "object": "item",
                "id": "1",
                "title": "Red pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            },
            {
                "object": "item",
                "id": "a123",
                "title": "Blue pill",
                "unit_price": 12000,
                "quantity": 1,
                "category": null,
                "tangible": true,
                "venue": null,
                "date": null
            }
        ],
        "card": {
        "object": "card",
            "id": "card_cj6drvxnm0lqo696dirob3ibk",
            "date_created": "2017-08-15T15:58:31.714Z",
            "date_updated": "2017-08-15T15:58:32.098Z",
            "brand": "visa",
            "holder_name": "Morpheus Fishburne",
            "first_digits": "411111",
            "last_digits": "1111",
            "country": "UNITED STATES",
            "fingerprint": "3ace8040fba3f5c3a0690ea7964ea87d97123437",
            "valid": true,
            "expiration_date": "0922"
        },
        "split_rules": null,
        "antifraud_metadata": {},
        "reference_key": null,
        "metadata": {}
    }]';

}

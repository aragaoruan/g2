<?php

namespace G2\Negocio;

/**
 * Classe de negócio para vincular Projeto Pedagógico com Turma e Entidade
 * @author Ilson Nóbrega (ilson.nobrega@unyleya.com.br)
 */

class ProdutoProjetoPedagogico extends Negocio
{

    private $repositorioProdutoPedagogico = 'G2\Entity\VwTurmaProjetoPedagogico';


    public function __construct()
    {

        parent::__construct();
    }

    /**
     * Função que retorna turmas por entidade e projeto pedagógico
     * @param $id_projetopedagogico
     * @return array|string
     * @throws \Exception $e
     */
    public function retornaTurmaPorProjetoEntidade($id_projetopedagogico)
    {
        try {
            if (isset($id_projetopedagogico) && !empty($id_projetopedagogico)) {
                $dados = $this->em->getRepository("G2\\Entity\\ProjetoPedagogico")
                    ->retornarProjetosPedagogicosEntidadeProduto($this->sessao->id_entidade, $id_projetopedagogico);

                return !empty($dados) ? $dados : [];
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna a Entity de ProdutoProjetoPedagogico
     * @param array $params
     * @return bool | Entity
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     */
    public function retornaProdutoProjetoPedagogico(array $params)
    {
        try {

            $result = $this->findBy('G2\Entity\ProdutoProjetoPedagogico', $params);
            $array = null;
            if ($result) {
                return $result;
            }
            return false;
        } catch (\Doctrine\DBAL\DBALException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
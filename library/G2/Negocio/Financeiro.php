<?php

namespace G2\Negocio;

use G2\Constante\Sistema;

/**
 * Classe Negocio para Financeiro
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-08-14
 */
class Financeiro extends Negocio
{
    /**
     *
     * @var \Doctrine\ORM\EntityManager $em
     */
    protected $em;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id_mat
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarResposavelFinanceiro($id_mat)
    {
        try {

            $repositoryName = $this->em->getRepository('G2\Entity\VwMatriculaLancamento');

            $query = $repositoryName->createQueryBuilder('vw')
                ->select('DISTINCT vw.id_usuariolancamento, vw.st_usuariolancamento')
                ->andWhere('vw.id_matricula = :id_matricula')
                ->setParameter('id_matricula', $id_mat);

            $result = $query->getQuery()->getResult();

            return $result;

        } catch (\Zend_Exception $e) {
            THROW $e;
        }
    }

    /**
     * Retorna ClienteVenda
     * @param array $params
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Exception
     * @throws \Exception
     */
    public function retornarClienteVenda(array $params)
    {
        try {
            if (!isset($params['id_entidade']) || $params['id_entidade'] == false)
                $params['id_entidade'] = $this->sessao->id_entidade;
            $result = $this->findBy(\G2\Entity\VwClienteVenda::class, $params);
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (\Doctrine\DBAL\DBALException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @param $id_venda
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function retornaDadosVendaRecorrente($id_venda)
    {
        $pagamento = new \StdClass;
        $dados = array();

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->em;

        //Buscando os dados da Venda
        $vendaNegocio = new \G2\Negocio\Venda();
        /** @var \G2\Entity\Venda $venda */
        $venda = $vendaNegocio->findVenda($id_venda);
        $dados['venda'] = $venda;
        if (!$venda->getDt_cadastro()) {
            throw new Zend_Exception('Esta venda não foi encontrada em nosso banco de dados!<br><br>');
        }

        //Buscando os dados do usuário da Venda informada
        $vwPessoaNegocio = new \G2\Negocio\Pessoa();
        $mensageiroPessoa = $vwPessoaNegocio->retornaVwPessoaOne(array('id_entidade' => $venda->getId_entidade(), 'id_usuario' => $venda->getId_usuario()));
        if ($mensageiroPessoa->getTipo() != \Ead1_IMensageiro::SUCESSO) {
            throw new Zend_Exception('Não foi possível encontrar o usuário em nosso banco de dados!');
        }
        $dados['pessoa'] = $mensageiroPessoa->getFirstMensagem();

        //buscando o curso que o aluno foi matriculado
        $pagamento->carrinho['produtos'] = $vendaNegocio->retornarVwProdutoVenda(array('id_venda' => $venda->getId_venda()));
        if (empty($pagamento->carrinho['produtos'])) {
            throw new Zend_Exception('Dados dos Produtos não encontrados.');
        }

        /** @var \G2\Entity\VwResumoVendaLancamento $lancamento */
        $lancamento = $this->findOneBy('G2\Entity\VwResumoVendaLancamento', array('id_venda' => $venda->getId_venda()));

        //buscando os dados de integração da entidade com a braspag
        $entidadeIntegracaoNegocio = new \G2\Negocio\EntidadeIntegracao();
        /** @var \G2\Entity\EntidadeIntegracao $integracao */
        $integracao = $entidadeIntegracaoNegocio->retornaEntidadeIntegracaoEntity(
            array(
                'id_entidade' => $venda->getId_entidade(),
                'id_sistema' => $lancamento->getId_Sistemacobranca() ? $lancamento->getId_Sistemacobranca() : Sistema::BRASPAG_RECORRENTE
            )
        );

        if (!$integracao || !$integracao->getSt_codchave()) {
            throw new \Zend_Exception('Não existe interface de integração para o sistema ' . ($lancamento->getId_Sistemacobranca() == Sistema::PAGARME ? 'PAGAR.ME' : 'BRASPAG') . ' nesta entidade!');
        }

        $cartaoNegocio = new \G2\Negocio\Cartao();
        $bandeiras = $cartaoNegocio->retornaBandeirasCartao(array('id_entidade' => $venda->getId_entidade(), 'id_sistema' => \SistemaTO::BRASPAG_RECORRENTE));

        if (!$bandeiras) {
            throw new \Zend_Exception('Não foi possível encontrar uma Bandeira para pagamento para o Cartão de Crédito Recorrente.');
        }

        $pagamento->carrinho['formapagamento'] = new \StdClass;
        $pagamento->carrinho['formapagamento']->bandeirarecorrente = $bandeiras;

        $nu_valortotal = null;
        /** @var \G2\Entity\VwProdutoVenda $produto */
        foreach ($pagamento->carrinho['produtos'] as $produto) {
            $nu_valortotal += $produto->getNu_valorliquido();
            $produto->bl_desconto = false;
            if ($produto->getNu_valorbruto() > $produto->getNu_valorliquido()) {
                $produto->bl_desconto = true;
            }

            $produto->setNu_valorliquido(\Ead1_BO::formataValor((double)$produto->getNu_valorliquido(), 'R$ #,##0.00'));
            $produto->setNu_valorbruto(\Ead1_BO::formataValor((double)$produto->getNu_valorbruto(), 'R$ #,##0.00'));
        }

        $pagamento->carrinho['resumo'] = new \StdClass;
        $pagamento->carrinho['resumo']->nu_valortotal = \Ead1_BO::formataValor((double)$nu_valortotal, 'R$ #,##0.00');
        $pagamento->carrinho['resumo']->nu_subtotal = \Ead1_BO::formataValor((double)$nu_valortotal, 'R$ #,##0.00');
        $pagamento->carrinho['resumo']->nu_valorjuros = \Ead1_BO::formataValor(0, 'R$ #,##0.00');
        $pagamento->carrinho['resumo']->nu_juros = '0%';

        $dados['pagamento'] = $pagamento;
        $dados['entidadeintegracao'] = $integracao;

        $qb = $this->em->createQueryBuilder();
        $qb->select('vw')
            ->from('G2\Entity\VwResumoVendaLancamento', 'vw')
            ->Join(
                'G2\Entity\Lancamento',
                'l',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                'vw.id_lancamento = l.id_lancamento'
            )
            ->where('vw.id_venda = :id_venda')
            ->andWhere('vw.bl_quitado = :bl_quitado')
            ->andWhere('vw.bl_ativo = :bl_ativo')
            ->andWhere('vw.id_meiopagamento = :id_meiopagamento')
            ->andWhere('(l.nu_tentativabraspag < 8 or l.nu_tentativabraspag is null)')
            ->setParameters(array('id_venda' => $venda->getId_venda(), 'bl_quitado' => 0, 'bl_ativo' => 1, 'id_meiopagamento' => \MeioPagamentoTO::RECORRENTE));

        $vendaLancamentos = $qb->getQuery()->getResult();

        if (!$vendaLancamentos) {
            throw new \Zend_Exception('Não foi possível encontrar lançamentos recorrentes pendentes nesta venda.');
        }

        $dados['vendaLancamentos'] = $vendaLancamentos;

        return $dados;
    }

    /**
     * Método para retornar o resumo financeiro do aluno
     * @param int $idUsuario
     * @param int $idEntidade
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarResumoFinanceiroByIdUsuario($idUsuario, $idEntidade, array $params = array())
    {
        try {
            $matriculaNegocio = new Matricula();
            $arrResult = [];

            $id_statuslancamento = false;
            if (isset($params['id_statuslancamento']) && $params['id_statuslancamento']) {
                $id_statuslancamento = (int)$params['id_statuslancamento'];
                unset($params['id_statuslancamento']);
            }

            //busca todas as entidades e cursos que o aluno esta vinculado
            $entidades = $matriculaNegocio->retornarCursosByAluno($idUsuario, $idEntidade);

            if (!$entidades) {
                throw new \Exception("Nenhum curso encontrado para o aluno informado.");
            }

            $arrIdEntidade = [];
            foreach ($entidades as $entidade) {
                array_push($arrIdEntidade, $entidade["id_entidade"]);
            }

            $vendas = $this->retornarClienteVenda([
                "id_usuario" => $idUsuario,
                "id_evolucao" => \G2\Constante\Evolucao::TB_VENDA_CONFIRMADA,
                "id_entidade" => $arrIdEntidade
            ]);

            if ($vendas) {
                foreach ($vendas as $venda) {
                    //busca o resumo financeiro de acordo com a matricula do usuario pelos cursos buscados acima
                    $resumo = $matriculaNegocio->findVwResumoFinanceiro(array_merge(array(
                        'id_venda' => $venda->getId_venda()
                    ), $params), false);

                    if ($resumo->getTipo() == \Ead1_IMensageiro::SUCESSO
                        && count($resumo->getMensagem())
                    ) {
                        //alteracao para retornar os dados no novo padrao do app e possivelmente no porta
                        $parcelas = array();

                        // calcula o número de parcelas do boleto
                        foreach ($resumo->getMensagem() as $key => $parcela) {
                            if (isset($parcelas[$parcela['id_venda']])) {
                                $parcelas[$parcela['id_venda']] += 1;
                            } else {
                                $parcelas[$parcela['id_venda']] = 1;
                            }
                        }

                        //pega os dados do array do curso
                        $curso = $this->getDadosCurso($entidades, $venda->getId_matricula(), $venda->getId_venda());

                        foreach ($resumo->getMensagem() as $key => $parcela) {
                            //aplica as regras de negocio para os status da parcela
                            $statusParcela = $this->getStatusParcela($parcela);
                            $stStatusLancamento = \G2\Constante\StatusLancamento::getStatusLancamento($statusParcela);
                            //cria duas posições no array da parcela para indicar qual o status da mesma
                            $parcela['nu_parcelas'] = $parcelas[$parcela['id_venda']];
                            $parcela["id_statuslancamento"] = $statusParcela;
                            $parcela["st_statuslancamento"] = $stStatusLancamento;
                            $idLancamento = $parcela['id_lancamento'];
                            $parcela["dt_vencimento"] = $this->converterData($parcela["dt_vencimento"]['date']);
                            $parcela["st_urlloja"] = \Ead1_Ambiente::geral()->st_url_loja;


                            if ($id_statuslancamento) {
                                if ($id_statuslancamento == $statusParcela) {
                                    $arrResult[$idLancamento] = array_merge($curso, $parcela);
                                }
                            } elseif (is_array($curso)) {
                                $arrResult[$idLancamento] = array_merge($curso, $parcela);
                            }

                        }


                    }

                }
            }

            sort($arrResult);

            return $arrResult;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar resumo financeiro. " . $e->getMessage());
        }
    }


    /**
     * Percorre o array dos cursos do aluno e retorna so os dados do curso
     * @param $arrDadosCurso
     * @param $idMatricula
     * @param $idVenda
     * @return array
     * @throws \Zend_Exception
     */
    private function getDadosCurso($arrDadosCurso, $idMatricula, $idVenda)
    {

        foreach ($arrDadosCurso as $entidade) {
            if ($entidade["cursos"]) {
                foreach ($entidade["cursos"] as $curso) {
                    if ($curso["id_matricula"] == $idMatricula) {
                        return $curso;
                    }
                }
            }
        }

        /** @var \G2\Entity\VwMatricula $matricula */
        $matricula = $this->findOneBy(\G2\Entity\VwMatricula::class, [
            "id_venda" => $idVenda,
            "id_situacao" => \G2\Constante\Situacao::TB_MATRICULA_ATIVA,
            "id_evolucao" => \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO
        ]);

        if ($matricula) {
            return [
                "id_curso" => $matricula->getId_projetopedagogico(),
                "st_curso" => $matricula->getSt_tituloexibicao(),
                "id_matricula" => $matricula->getId_matricula()
            ];
        }

    }

    /**
     * Função para retornar o status dos lançamentos para distribuir os lançamentos na tela
     * Essas regras foram definidas na ISSUE MOB-121
     *
     * As regras para os meios de pagamento (Boleto Bancário, Cartão Recorrente e Cheque) são:
     *
     * A PAGAR Todos os lançamentos não pagos que não tiveram a data limite de pagamento expirado
     * ATRASADO Todos os lançamentos não pagos que tiveram a data limite expirado
     * PAGO Todos os lançamentos pagos e confirmados.
     *
     * A regra para pagamento com cartão de crédito é:
     * Sempre exibir todas as parcelas como pago mesmo quando o valor ainda não tiver sido repassado pela operadora do
     * cartão de crédito.
     *
     * A regra para pagamento em Dinheiro é:
     * Exibir na aba “Pago” o valor do pagamento realizado
     *
     * @param array $parcela
     * @return int
     * @throws \Exception
     */
    private function getStatusParcela($parcela)
    {
        try {
            if (!isset($parcela['id_meiopagamento']) || empty($parcela['id_meiopagamento']))
                throw new \Exception("Meio de pagamento não definido.");

            $idMeioPagamento = $parcela['id_meiopagamento'];

            //verifica a data de vencimento do lançamento, se é um array com as posições corretas
            if (!is_array($parcela['dt_vencimento']) || !array_key_exists('date', $parcela['dt_vencimento'])) {
                throw new \Exception("Data de vencimento do lançamento não definida.");
            }


            $dtVencimento = $parcela['dt_vencimento']['date'];//atribui o valor da data a varaivel

            $bo = new \Ead1_BO();//cria a instancia da bo para usar uma função

            //calcula a diferença de dias entre a data de hoje e a data de vencimento do lançamento
            $dateDiff = $bo->diferencaDias(date("Y-m-d"), $dtVencimento);


            //Boleto, Cartão Recorrente e Cheque
            if ($idMeioPagamento == \G2\Constante\MeioPagamento::BOLETO
                || $idMeioPagamento == \G2\Constante\MeioPagamento::CARTAO_RECORRENTE
                || $idMeioPagamento == \G2\Constante\MeioPagamento::CHEQUE
            ) {

                /*
                 * Regra para lançamentos A pagar
                 * bl_quitado == false && dt_vencimento >= hoje
                 */
                if (!$parcela['bl_quitado'] && $dateDiff >= 0) {
                    return \G2\Constante\StatusLancamento::A_PAGAR;
                }

                /*
                 * Regra para lançamentos Atrasados
                 * bl_quitado == false && dt_vencimento < 0
                 */
                if (!$parcela['bl_quitado'] && $dateDiff < 0) {
                    return \G2\Constante\StatusLancamento::ATRASADO;
                }

                /*
                 * Regra para lançamentos Pagos
                 * bl_quitado == true
                 */
                if ($parcela['bl_quitado']) {
                    return \G2\Constante\StatusLancamento::PAGO;
                }

            }

            // Cartão de Crédito ou Dinheiro
            if ($idMeioPagamento == \G2\Constante\MeioPagamento::CARTAO_CREDITO
                || $idMeioPagamento == \G2\Constante\MeioPagamento::DINHEIRO
            ) {
                return \G2\Constante\StatusLancamento::PAGO;
            }
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Retorna as vendas vinculadas ao usuário
     * @param array $params
     * @return mixed
     */
    private function retornarVendasAluno(array $params)
    {

        $params['id_usuario'] = isset($params['id_usuario']) && !empty($params['id_usuario']) ? $params['id_usuario'] : $this->sessao->id_usuario;

        $vwCVTO = new \VwClienteVendaTO();
        $vwCVTO->montaToDinamico($params);

        $bo = new \PortalFinanceiroBO();
        $result = $bo->retornaVendasCliente($vwCVTO, true);
        return $result;

    }


}

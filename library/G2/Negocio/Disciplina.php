<?php

namespace G2\Negocio;

use G2\Entity\VwAlunoGradeIntegracao;
use G2\Utils\Helper;

/**
 * Classe de negócio para métodos realcionados a Disciplina
 * @author Débora Castro  <debora.castro@unyleya.com.br>
 */
class Disciplina extends Negocio
{

    private $repositoryName;
    private $disciplinaBo;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = '\G2\Entity\Disciplina';
        $this->disciplinaBo = new \DisciplinaBO();
    }

    /**
     * Find By Entidade
     * @param integer $id_entidade
     * @return object G2\Entity\Disciplina
     */
    public function findByEntidadeSessao(array $params = array(), array $order = array('st_disciplina' => 'asc'))
    {
        $params['id_entidade'] = (isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade);
        return $this->findBy($this->repositoryName, $params, $order);
    }

    public function findByOneAreaConhecimento($id)
    {
        return parent::findOneBy($this->repositoryName, $id);
    }

    /*
     * @param integer $id
     * @return object G2\Entity\AreaConhecimento
     */

    /**
     *
     * @return G2\Entity\AreaConhecimento
     */
    public function findAll($repositoryName = null)
    {
        return parent::findAll($this->repositoryName);
    }

    /**
     * Update data
     * @param array $data
     * @return G2\Entity\AreaConhecimento
     */
    public function updateAreaConhecimento($data)
    {
        $situacao = $this->situacaoNegocio->getReference($data['id_situacao']);
        $entidade = $this->entidadeNegocio->getReference($this->sessao->id_entidade);
        $usuario = $this->em->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
        $tipoareaconhecimento = $this->em->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);

        $tm = $this->getReference($data['id']);

        $tm->setSt_areaconhecimento($data['st_areaconhecimento'])
            ->setId_situacao($situacao)
            ->setId_tipoareaconhecimento($tipoareaconhecimento)
            ->setId_usuariocadastro($usuario)
            ->setSt_tituloexibicao($data['st_tituloexibicao'])
            ->setSt_descricao($data['st_descricao'])
            ->setBl_ativo(true)
            ->setId_entidade($entidade);

        $this->em->merge($tm);
        $this->em->flush();

        $this->areaEntidadeNegocio->deletarAreaEntidade($data['id']);
        $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $data['id']);

        return $tm;
    }

    /**
     *
     * @param integer $idAreaConhecimento
     * @return G2\Entity\AreaConhecimento
     */
    public function getReference($idAreaConhecimento, $repositoryName = null)
    {
        return $this->em->getReference($this->repositoryName, $idAreaConhecimento);
    }

    /**
     *
     * @param array $data
     * @return \G2\Entity\AreaConhecimento
     */
    public function salvarAreaConhecimento(array $data)
    {
        try {

            if (empty($data['entidades'])) {
                return array('title' => 'Atenção!',
                    'text' => 'Selecione pelo menos uma organização!');
            }
            //   echo $data['id_situacao'];die;
            $situacao = $this->situacaoNegocio->getReference($data['id_situacao']);
            $entidade = $this->entidadeNegocio->getReference($this->sessao->id_entidade);
            $usuario = $this->em->getReference('G2\Entity\Usuario', $this->sessao->id_usuario);
            $tipoareaconhecimento = $this->em->getReference('G2\Entity\TipoAreaConhecimento', $data['id_tipoareaconhecimento']);

//        var_dump($tipoareaconhecimento);die;

            if (isset($data['id'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new AreaConhecimentoEntity();
            }
            //seta os atributos
            $entity->setSt_areaconhecimento($data['st_areaconhecimento'])
                ->setId_situacao($situacao)
                ->setId_tipoareaconhecimento($tipoareaconhecimento)
                ->setId_usuariocadastro($usuario)
                ->setDt_cadastro(new \DateTime())
                ->setSt_tituloexibicao($data['st_tituloexibicao'])
                ->setSt_descricao($data['st_descricao'])
                ->setBl_ativo(1)
                ->setId_entidade($entidade)
                ->setId_areaconhecimentopai(null);

            $retorno = $this->save($entity);

            //Vincula com a entidade
            if ($retorno->getId()) {
                $this->areaEntidadeNegocio->salvarAreaEntidade($data['entidades'], $retorno->getId());
            }

            return array(
                'id' => $retorno->getId(),
                'type' => 'success',
                'title' => 'Salvo com Sucesso',
                'text' => 'O registro foi salvo com sucesso!'
            );
        } catch (\Exception $e) {
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao salvar Área Conhecimento: ' . $e->getMessage()
            );
        }
    }

    /**
     *
     * @param integer $id
     * @return object
     */
    public function deletarArea($id)
    {
        $tm = $this->getReference($id);

        $tm->setBl_ativo(false);

        $this->em->merge($tm);
        $this->em->flush();

        return $tm;
    }

    /**
     * Find tipos de Disciplina
     * @param array $params
     * @return '\G2\Entity\TipoDisciplina'
     */
    public function getTipoDisciplina(array $params = array())
    {
        return $this->findBy('\G2\Entity\TipoDisciplina', $params);
    }

    /**
     * Retorna Perfil ativo de Coordenador de Disciplina de acordo com a entidade da sessão
     * @param array $params Array com parametros
     * @return 'G2\Entity\Perfil'
     */
    public function retornaPerfilDisciplinaByEntidadeSessao(array $params = array())
    {
        try {
            $perfilNegocio = new \G2\Negocio\Perfil();
            $params['id_entidade'] = (isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade); //id_entidade Sessão

            $result = $perfilNegocio->retornaPerfil($params);
            if ($result) {
                if (is_array($result)) {
                    return array_shift($result);
                }
                return $result;
            } else {
                return array();
            }
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna dados de vw_usuarioperfilentidadereferencia
     * @param array $params
     * @return mixed/array
     */
    public function retornarDadosCoordenadorDisciplina(array $params)
    {
        try {
            //instancia a BO e a TO
            $bo = new \ProjetoPedagogicoBO();
            $to = new \VwUsuarioPerfilEntidadeTO();
            $to->montaToDinamico($params);
            //seta os parametros
            $to->setId_entidadecadastro(($to->getId_entidadecadastro() ? $to->getId_entidadecadastro() : $this->sessao->id_entidade));
            //atribui o retorno da BO a variavel
            $result = $bo->retornarVwUsuarioPerfilEntidade($to);
            $arrReturn = array();
            //verifica o resultado se sucesso
            if ($result->getType() == 'success') {
                //percorre o array da mensagem e
                foreach ($result->getMensagem() as $row) {
                    $arrReturn[] = $row->toArray();
                }
            }
            return $arrReturn;
        } catch (\Zend_Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Deleção lógica de disciplina
     * @param integer $id
     * @return mixed
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function deletaDisciplina($id)
    {
        try {
            if (!$id) {
                throw new \Exception('Id da Disciplina deve ser passado.');
            }
            $data['bl_ativa'] = false;
            $data['id_disciplina'] = $id;
            return $this->salvaDisciplina($data);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }
    }

    /**
     * Salva Disciplina
     * @param array $data Dados de Disciplina
     * @param array $imagem Imagem da Disciplina
     * @return \G2\Entity\Disciplina
     * @throws \Zend_Exception
     */
    public function salvaDisciplina(array $data, $imagem = null)
    {
        try {
            /**
             * @var $entity \G2\Entity\Disciplina
             */
            $entity = new \G2\Entity\Disciplina();
            if (isset($data['id_disciplina']) && !empty($data['id_disciplina'])) {
                $disciplina = $this->findDisciplina($data['id_disciplina']);
                if ($disciplina) {
                    $entity = $disciplina;
                }
            }

            if (isset($data['sg_uf']) && !empty($data['sg_uf'])) {
                $sgUf = parent::getReference('\G2\Entity\Uf', $data['sg_uf']);
                $entity->setSg_uf($sgUf);
            }
            $data ['bl_ativa'] = true;

            if (isset($data['id_situacao']) && !empty($data['id_situacao'])) {
                $idSituacao = parent::getReference('\G2\Entity\Situacao', $data['id_situacao']);
                $entity->setId_situacao($idSituacao);
            }
            if (isset($data['id_entidade']) && !empty($data['id_entidade'])) {
                $entity->setId_entidade($data['id_entidade']);
            } else {
                if (!$entity->getId_entidade()) {
                    $entity->setId_entidade($this->sessao->id_entidade);
                }
            }

            /**
             * Aplica o valor de id_grupodisciplina como Entity (FK)
             */
            if (isset($data['id_grupodisciplina']) && $data['id_grupodisciplina']) {
                $entity->setId_grupodisciplina($this->find('\G2\Entity\GrupoDisciplina',
                    $data['id_grupodisciplina']));
            }

            //Tags permitidas na string para os campos st_descricao, st_ementacertificado
            $allowTags = \G2\Constante\Utils::$tinYMceHtmlTagsAllowed;

            $entity->setSt_disciplina(
                isset($data['st_disciplina']) ? $data['st_disciplina'] : $entity->getSt_disciplina()
            )
                ->setSt_descricao(
                    isset($data['st_descricao']) ?
                        trim(strip_tags($data['st_descricao'], $allowTags)) : $entity->getSt_descricao()
                )
                ->setNu_identificador(
                    isset($data['nu_identificador']) ? $data['nu_identificador'] : $entity->getNu_identificador()
                )
                ->setNu_cargahoraria(
                    isset($data['nu_cargahoraria']) ? $data['nu_cargahoraria'] : $entity->getNu_cargahoraria()
                )
                ->setNu_creditos(
                    isset($data['nu_creditos']) ? $data['nu_creditos'] : $entity->getNu_creditos()
                )
                ->setNu_codigoparceiro(
                    isset($data['nu_codigoparceiro']) ? $data['nu_codigoparceiro'] : $entity->getNu_codigoparceiro()
                )
                ->setBl_ativa($entity->getBl_ativa() == $data['bl_ativa'] ? $entity->getBl_ativa() : $data['bl_ativa'])
                ->setBl_compartilhargrupo(
                    isset($data['bl_compartilhargrupo']) ?
                        $data['bl_compartilhargrupo'] : $entity->getBl_compartilhargrupo()
                )
                ->setSt_tituloexibicao(
                    isset($data['st_tituloexibicao']) ? $data['st_tituloexibicao'] : $entity->getSt_tituloexibicao()
                )
                ->setId_tipodisciplina(
                    isset($data['id_tipodisciplina']) ? $data['id_tipodisciplina'] : $entity->getId_tipodisciplina()
                )
                ->setId_usuariocadastro(
                    isset($data['id_usuariocadastro']) ? $data['id_usuariocadastro'] : $entity->getId_usuariocadastro()
                )
                ->setSt_identificador(
                    isset($data['st_identificador']) ? $data['st_identificador'] : $entity->getSt_identificador()
                )
                ->setSt_ementacertificado(
                    isset($data['st_ementacertificado']) ?
                        trim(strip_tags($data['st_ementacertificado'], $allowTags)) : $entity->getSt_ementacertificado()
                )
                ->setBl_provamontada(
                    isset($data['bl_provamontada']) ? $data['bl_provamontada'] : $entity->getBl_provamontada()
                )
                ->setNu_repeticao(
                    isset($data['nu_repeticao']) ? $data['nu_repeticao'] : $entity->getNu_repeticao()
                )
                ->setDt_cadastro(new \DateTime($entity->getDt_cadastro()))
                ->setSt_apelido(
                    isset($data['st_apelido']) ? $data['st_apelido'] : $entity->getSt_apelido()
                )
                ->setBl_disciplinasemestre(
                    isset($data['bl_disciplinasemestre']) ?
                        $data['bl_disciplinasemestre'] : $entity->getBl_disciplinasemestre()
                );

            // GII-8237 - Salvando provas integradas com a Fábrica De Provas da Maximize.
            $entity->setSt_codprovarecuperacaointegracao(isset($data['st_codprovarecuperacaointegracao'])
                ? $data['st_codprovarecuperacaointegracao']
                : $entity->getSt_codprovarecuperacaointegracao());

            $entity->setSt_provarecuperacaointegracao(isset($data['st_provarecuperacaointegracao'])
                ? $data['st_provarecuperacaointegracao']
                : $entity->getSt_provarecuperacaointegracao());

            $entity->setSt_codprovaintegracao(isset($data['st_codprovaintegracao'])
                ? $data['st_codprovaintegracao']
                : $entity->getSt_codprovaintegracao());

            $entity->setSt_provaintegracao(isset($data['st_provaintegracao'])
                ? $data['st_provaintegracao']
                : $entity->getSt_provaintegracao());

            $entity->setBl_coeficienterendimento(isset($data['bl_coeficienterendimento'])
                ? $data['bl_coeficienterendimento']
                : $entity->getBl_coeficienterendimento());

            $entity->setBl_cargahorariaintegralizada(isset($data['bl_cargahorariaintegralizada'])
                ? $data['bl_cargahorariaintegralizada']
                : $entity->getBl_cargahorariaintegralizada());

            $entitySalva = $this->save($entity);

            if ($imagem && $entitySalva instanceof \G2\Entity\Disciplina) {
                $imagemSalva = $this->uploadImagemDisciplina($imagem, $entitySalva->getId_disciplina());
                $entitySalva->setst_imagem($imagemSalva['file']);
                $entitySalva = $this->save($entity);
            }

            return $entitySalva;

        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     *
     * @param integer $id
     * @return object G2\Entity\AreaConhecimento
     */
    public function findDisciplina($id)
    {
        return parent::find($this->repositoryName, $id);
    }

    /**
     * Faz upload da imagem da Disciplina
     *
     * @param $imagem
     * @param $id_disciplina
     * @return array
     * @throws \Zend_Exception
     */
    public function uploadImagemDisciplina($imagem, $id_disciplina)
    {
        try {
            if (is_array($imagem)) {
                $path = 'upload/disciplina/';
                $type = explode('/', $imagem['type']);
                $fileName = 'disciplina_' . $id_disciplina;
                $fileNameType = $fileName . '.' . $type['1'];

                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }

                //renomeando o nome do arquivo para que a extensão fique realmente a extensão
                //do arquivo
                $imagem['name'] = $fileName;

                //verifica o tamanho da imagem
                list($width, $height) = getimagesize($imagem['tmp_name']);

                if ($width != '1024' || $height != '1024') {
                    throw new \Exception("A imagem não está no tamanho solicitado. Tamanho da imagem deve ser igual a 1024x1024.");
                }

                if (!preg_match("/^image\/(jpg|jpeg)$/", $imagem["type"])) {
                    throw new \Exception("O formato da imagem não é válido. O formato aceito é JPG.");
                }

                if (file_exists($path . $fileNameType)) {
                    unlink($path . $fileNameType);
                }

                $bo = new \Ead1_BO();
                $arquivoTO = new \ArquivoTO();
                $arquivoTO->setSt_caminhodiretorio('disciplina');
                $arquivoTO->setAr_arquivo($imagem);
                $arquivoTO->setSt_nomearquivo($fileName);
                $arquivoTO->setSt_extensaoarquivo($type[1]);
                $img = $bo->uploadBasico($arquivoTO);

                //monta o array de retorno com os dados da imagem
                $arrReturn = array(
                    "type" => $img->getType(),
                    'title' => $img->getTitle(),
                    'text' => $img->getText(),
                    'file' => $path . $fileNameType
                );
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Salva Area Conhecimento join Disciplina
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaAreaConhecimentoJoinDisciplina(array $data)
    {
        $adTO = new \AreaDisciplinaTO();
        $adTO->montaToDinamico($data);
        return $this->disciplinaBo->cadastrarAreaDisciplina($adTO);
    }

    /**
     * Metodo que Exclui a Area a Disciplina
     * @param \AreaDisciplinaTO $adTO
     * @return \Ead1_Mensageiro
     */
    public function deletarAreaDisciplina(\AreaDisciplinaTO $adTO)
    {
        return $this->disciplinaBo->deletarAreaDisciplina($adTO);
    }

    /**
     * Salva Array com Áreas de Conhecimento e Disciplina
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaArrAreaJoinDisciplina(array $data)
    {
        $adTO = new \AreaDisciplinaTO();
        $adTO->setId_disciplina($data['id_disciplina']);
        $arrayAC = array();
        foreach ($data['id_areaconhecimento'] as $area) {
            $acTO = new \AreaConhecimentoTO();
            $acTO->setId_areaconhecimento($area);
            $arrayAC[] = $acTO;
        }
        return $this->disciplinaBo->salvarArrayAreaDisciplina($arrayAC, $adTO);
    }

    /**
     *
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaDisciplinaSerieNivelEnsino(array $data)
    {
        $arrTO = array();
        if ($data) {
            foreach ($data['id_serie'] as $id_serie) {
                $dsneTO = new \DisciplinaSerieNivelEnsinoTO();
                $dsneTO->setId_serie($id_serie);
                $dsneTO->setId_disciplina($data['id_disciplina']);
                $dsneTO->setId_nivelensino($data['id_nivelensino']);
                $arrTO[] = $dsneTO;
            }
        }
        return $this->disciplinaBo->salvarArrayDisciplinaSerieNivelEnsino($arrTO);
    }

    /**
     * Retorna Areas Join Disciplina by params
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findAreasJoinDisciplina(array $params)
    {
        $areaDisciplinaTO = new \AreaDisciplinaTO();
        $areaDisciplinaTO->setId_disciplina(isset($params['id_disciplina']) ? $params['id_disciplina'] : null);
        $areaDisciplinaTO->setId_areaconhecimento(isset($params['id_areaconhecimento']) ? $params['id_areaconhecimento'] : null);

        return $this->disciplinaBo->retornaAreaDisciplina($areaDisciplinaTO);
    }

    /**
     * Busca DisciplinaSerieNivel by params
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findDisciplinaSerieNivel(array $params)
    {
        $dsneTO = new \DisciplinaSerieNivelEnsinoTO($params);
        return $this->disciplinaBo->retornaDisciplinaSerieNivel($dsneTO);
    }

    /**
     * Deleta vinculo de Disciplina Serie Nivel de Ensino
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function deletaDisciplinaSerieNivelEnsino(array $data)
    {
        $dsneTO = new \DisciplinaSerieNivelEnsinoTO();
        $dsneTO->montaToDinamico($data);
        return $this->disciplinaBo->deletarDisciplinaSerieNivel($dsneTO);
    }

    public function findVwDisciplinasPrr(array $where)
    {
        return $this->findBy('\G2\Entity\VwDisciplinasPrr', $where);
    }


    /**
     *
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaDisciplinaIntegracao(array $data)
    {
        if ($data) {
            $dI = new \DisciplinaIntegracaoTO();
            $dI->setId_disciplina($data['id_disciplina']);
            $dI->setId_usuariocadastro($this->sessao->id_usuario);
            $dI->setBl_ativo(true);
            $dI->setDt_cadastro(new \DateTime());
            $dI->setId_entidade($data['id_entidade']);
            $dI->setSt_codsistema($data['st_codsistema']);
            $dI->setId_sistema($data['id_sistema']);
            $dI->setSt_salareferencia($data['st_salareferencia']);
            $dI->setid_entidadeintegracao($data["id_entidadeintegracao"]);
            return $this->disciplinaBo->salvarDisciplinaIntegracao($dI);
        }
    }

    /**
     * Retorna dados da vw_professordisciplina
     * @param array $params
     * @return mixed
     */
    public function retornarVwProfessorDisciplina(array $params = [])
    {
        $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
        $result = $this->findBy('\G2\Entity\VwProfessorDisciplina', $params, array('st_nomecompleto' => 'asc'));
        if ($result) {
            foreach ($result as $key => $row) {
                $result[$key] = array(
                    'id_usuario' => $row->getId_usuario(),
                    'st_nomecompleto' => $row->getSt_nomecompleto(),
                    'id_tipodisciplina' => $row->getId_tipodisciplina(),
                    'id_entidade' => $row->getId_entidade(),
                    'id_disciplina' => $row->getId_disciplina()
                );
            }
        }
        return $result;
    }

    /**
     * Retorna dados das disciplinas para entidade IMP e as filhas
     * @param array $params
     * @throws \Zend_Exception
     * @return array
     */
    public function retornarDisciplinasRecursivaIMP(array $params = [])
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            $params['bl_ativa'] = isset($params['bl_ativa']) ? $params['bl_ativa'] : 1;
            $result = $this->em->getRepository($this->repositoryName)->retornarDisciplinasRecursivaIMP($params);
            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar disciplinas. " . $e->getMessage());
        }
    }

    /**
     * Retorna pesquisa disciplinas pelo nome da disciplina, ativas e por entidade
     * @param array $params
     * @return VwEntidadeProjetoDisciplina
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisaDisciplinaEntidade(array $params = array())
    {
        try {

            if (!isset($params['st_disciplina'])):
                $params['st_disciplina'] = null;
            endif;


            $repo = $this->em->getRepository('G2\Entity\VwEntidadeProjetoDisciplina');
            $array_search = array('vw.id_disciplina', 'vw.st_disciplina');

            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->where('1=1')
                ->orderBy('vw.st_disciplina');


            if (isset($params['st_disciplina']) && !empty($params['st_disciplina'])) {
                $query->andWhere("vw.st_disciplina like '%" . $params['st_disciplina'] . "%'");
            }

            if (isset($params['bl_ativo']) && !empty($params['bl_ativo'])) {
                $query->andWhere('vw.bl_ativo = :bl_ativo')
                    ->setParameter('bl_ativo', $params['bl_ativo']);
            }

            $query->andWhere("vw.id_entidade = :id_entidade")
                ->setParameter("id_entidade", $this->sessao->id_entidade);
            $result = $query->getQuery()->getResult();
            if (count($result) > 0) {
                return new \Ead1_Mensageiro($result);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado', \Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * Retorna pesquisa projetos pedagogicos pelo id_disciplina, ativos e por entidade
     * @param array $params
     * @return VwSalaDisciplinaProjeto
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisaProjetosDisciplina(array $params = array())
    {
        try {

            if (!isset($params['id_disciplina'])):
                $params['id_disciplina'] = null;
            endif;


            $repo = $this->em->getRepository('G2\Entity\VwModuloDisciplina');
            $array_search = array('vw.id_projetopedagogico', 'vw.st_projetopedagogico', 'vw.id_disciplina');

            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->distinct(true)
                ->where('1=1')
                ->orderBy('vw.st_projetopedagogico');


            if (isset($params['id_disciplina']) && !empty($params['id_disciplina'])) {
                $query->andWhere("vw.id_disciplina in (" . $params['id_disciplina'] . ")");
            }


            $result = $query->getQuery()->getResult();

            if (count($result) > 0) {
                return new \Ead1_Mensageiro($result);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado', \Ead1_IMensageiro::AVISO);
            }
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * Retorna as disciplinas vinculadas ao aluno
     * ESTE MÉTODO É UTILIZADO NO APLICATIVO MÓVEL
     * @param integer $idUser id do usuário
     * @param array $params array de chave valor com os parametros array(chave=>valor,chave2=>valor2,...)
     * @param array $retornarDadosMoodle informa se junto com as disciplinas deve vir também os dados do progresso do aluno no Moodle
     * @return array
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarDisciplinasAluno($idUser, array $params = [], $retornarDadosMoodle = false)
    {
        try {

            //verifica se veio o id do usuario
            if (!$idUser || empty($idUser)) {
                throw new \Exception("Não é possível buscar disciplinas do aluno sem informar o id do aluno.");
            }

            if (isset($params['id_saladeaula']) && !$params['id_saladeaula']) {
                unset($params['id_saladeaula']);
            }

            $params['id_usuario'] = $idUser;
            //busca os dados
            $result = $this->findBy(\G2\Entity\VwAlunoGradeIntegracao::class, $params, array(
                'dt_abertura' => 'asc',
                'id_projetopedagogico' => 'asc',
                'id_modulo' => 'asc',
                'id_disciplina' => 'asc'
            ));

            //cria um array vazio para retorno
            $arrReturn = array();

            //verifica se teve retorno
            if ($result) {
                foreach ($result as $x => $row) {
                    if ($row instanceof VwAlunoGradeIntegracao) {

                        if ($retornarDadosMoodle) {

                            $mdl = new \MoodleCursosWebServices(
                                $row->getId_entidadesala(),
                                $row->getId_entidadeintegracao()
                            );

                        }

                        $resultVwNotas = NULL;
                        //cria uma variavel com a data de hoje
                        $dt_hoje = new \DateTime('now');

                        //cria uma variavel com a data de abertura
                        $dt_abertura = $row->getDt_abertura();

                        //verifica se a data de abertura não é uma instancia de datetime
                        if (!($dt_abertura instanceof \DateTime)) {
                            $dt_abertura = new \DateTime($dt_abertura);
                        }

                        //cria uma varivel com a data de encerramento
                        $dt_encerramento = $row->getDt_encerramentosala();

                        //verifica se a data de encerramento não é uma instancia de datetime
                        if (!($row->getDt_encerramentosala() instanceof \DateTime)) {
                            $dt_encerramento = new \DateTime($row->getDt_encerramentosala());
                        }


                        //verifica se a data de abertura é menor ou igual a data de hoje
                        if ($dt_abertura <= $dt_hoje) {
                            // Busca as informações com a carga horária e nota final da disciplina.
                            $resultVwNotas = $this->findBy(\G2\Entity\VwGradeNota::class, array(
                                'id_matricula' => $params['id_matricula'],
                                'id_saladeaula' => $row->getId_saladeaula(),
                                'id_disciplina' => $row->getId_disciplina()
                            ));
                        }
                        // Define a ordem de apresentacao das disciplinas
                        // Disciplinas em andamento > nao iniciadas > fechadas
                        // Mudou para Em andamento -> Fechadas -> Não iniciadas
                        if ($dt_abertura <= $dt_hoje && $dt_encerramento >= $dt_hoje) {
                            $nu_order = 1;
                        }
                        if ($dt_abertura > $dt_hoje && $dt_encerramento >= $dt_hoje) {
                            $nu_order = 3;
                        }
                        if ($dt_encerramento < $dt_hoje) {
                            $nu_order = 2;
                        }

//                        if ($retornarDadosMoodle) {

                        //adiciona os dados no array
                        $arrReturn[$x] = array(
                            'id_saladeaula' => $row->getId_saladeaula(),
                            'id_disciplina' => $row->getId_disciplina(),
                            'st_disciplina' => $row->getSt_disciplina(),
                            'st_imagemdisciplina' => $row->getst_imagemdisciplina(),
                            'st_descricaodisciplina' => $row->getSt_descricaodisciplina(),
                            'id_status' => $row->getId_status(),
                            'id_categoriasala' => $row->getId_categoriasala(),
                            'dt_abertura' => $dt_abertura ? $dt_abertura->format("Y-m-d") : null,
                            'dt_encerramento' => $dt_encerramento ? $dt_encerramento->format("Y-m-d") : null,
                            'st_status' => $row->getSt_status(),
                            'nu_order' => $nu_order,
                            'st_coordenador' => $row->getSt_coordenador(),
                            'id_professor' => $row->getId_professor(),
                            'st_professor' => $row->getSt_professor(),
                            'st_codsistemacurso' => $row->getSt_codsistemacurso(),
                            'nu_notafinal' => null,
                            'nu_cargahoraria' => null,
                            'ar_moodle' => null,
                            'dt_encerramentosala' => $row->getDt_encerramentosala()
                                ? Helper::converterData($row->getDt_encerramentosala()) : null,
                        );

                        //verifica se vw_notas retornou dados
                        if ($resultVwNotas) {
                            $resultVwNotas = array_shift($resultVwNotas);
                            $arrReturn[$x]['nu_notafinal'] = $resultVwNotas->getNu_notafinal();
                            $arrReturn[$x]['nu_cargahoraria'] = $resultVwNotas->getNu_cargahoraria();
                        }

                        //calcula a diferenca das datas de abertura e encerramento

                        //Numero de dias já passados da sala
                        $nu_diaspassados = (int)$dt_abertura->diff($dt_hoje)->format('%R%a');;
                        // Total de dias da salas
                        $nu_diassala = (int)$dt_abertura->diff($dt_encerramento)->format('%R%a');
                        // Calcula quanto porcento da sala ja passou
                        if ($nu_diassala > 0) {
                            $nu_diaspercentual = (int)(($nu_diaspassados * 100) / $nu_diassala);
                        } else {
                            $nu_diaspercentual = 100;
                        }
                        if ($nu_diaspercentual > 100) $nu_diaspercentual = 100;


                        $diff = (int)$dt_hoje->diff($dt_encerramento)->format('%R%a');

                        //verifica se é para buscar os dados do moodle e se a Disciplina está em andamento
                        //Se ela não estiver em andamento os dados do moodle não são necessarios
                        if ($retornarDadosMoodle && $row->getId_status() == 1) {

                            $ei = $mdl->getEntidadeIntegracaoTO();
                            if ($ei->getId_entidadeintegracao()) {
                                $arrReturn[$x]['ar_moodle'] = $mdl->retornarStatusCurso(
                                    $ei, $row->getSt_codusuario(), $row->getSt_codsistemacurso()
                                );
                            }
                        }

                        $st_statuscomplete = "";
                        /*
                         * define os status das salas
                         * se a sala já iniciou
                         */
                        if ($row->getId_status() == 1) {
                            if ($diff < 0) {
                                // Se a diferenca de dias for menor q 0 está atrasada
                                $st_statuscomplete = "Atrasado";
                            } else {
                                /*
                                * Checa o status comparando quanto já passou da sala com quantas atividades o aluno
                                * já fez
                                */
                                if ($arrReturn[$x]['ar_moodle']) {
                                    if ($arrReturn[$x]['ar_moodle']["nu_percentualconcluido"] < $nu_diaspercentual) {
                                        $st_statuscomplete = "Atrasado";
                                    } elseif (
                                        $arrReturn[$x]['ar_moodle']["nu_percentualconcluido"] > $nu_diaspercentual
                                    ) {
                                        $st_statuscomplete = "Adiantado";
                                    } elseif (
                                        $arrReturn[$x]['ar_moodle']["nu_percentualconcluido"] == $nu_diaspercentual
                                    ) {
                                        $st_statuscomplete = "Adiantado";
                                    }
                                }

                            }

                            // se a sala já fechou, define direto como completa
                        } elseif ($row->getId_status() == 2) {
                            $st_statuscomplete = "Completo";

                            $arrReturn[$x]['ar_moodle'] = [];
                            // Se a sala não iniciou define como status futuro
                        } else {
                            $st_statuscomplete = "Futuro";

                            $arrReturn[$x]['ar_moodle'] = [];

                        }
                        $arrReturn[$x]['st_statuscomplete'] = $st_statuscomplete;
                        $arrReturn[$x]['nu_diaspassados'] = $nu_diaspassados;
                        $arrReturn[$x]['nu_diassala'] = $nu_diassala;
                        $arrReturn[$x]['nu_diaspercentual'] = $nu_diaspercentual;
//                        $x++;
                        //  }
                    }

                }

            }

            //remove as variaveis
            unset($result, $params, $idUser);
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar disciplinas do aluno. ' . $e->getMessage());
        }
    }

    /**
     * Metodo responsavel por retornar registros de forma geral tabela disciplina conteudo
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param array $params
     * @param array $order
     * @param null $limit
     * @param null $offset
     * @return array
     */
    public function findByDisciplinaConteudo(array $params = array(), array $order = array(), $limit = NULL, $offset = NULL)
    {
        return $this->findBy('\G2\Entity\DisciplinaConteudo', $params, $order, $limit, $offset);
    }

    /**
     * Metodo responsavel por receber os dados necessarios para salvar o conteudo
     * de uma disciplina enviado, e invocar metodo que faz upload de formatos do tipo PDF ou ZIP
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * @param $data
     * @param $file
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarConteudoDisciplina(array $data, $file)
    {

        $this->beginTransaction();
        try {
            /* @alterado por Reinaldo Farias
             * Evita que o id do $formatoArquivo retorne Zero, pois no banco só existe a opçao 1 e 2 (Zip e PDF).
             * Demanda GII-7787
             * If e Else refatorado para melhorar o código. Linhas 922 e 929
             * */
            $formatoArquivo = parent::getReference('\G2\Entity\FormatoArquivo', (int)$data['id_formatoarquivo']);

            if (!$formatoArquivo || !in_array($formatoArquivo->getId_formatoarquivo(), [
                        \G2\Constante\FormatoArquivo::PDF,
                        \G2\Constante\FormatoArquivo::ZIP
                    ]
                )
            ) {
                throw new \Zend_Exception('Tipo do arquivo não é suportada.');
            }

            if (array_key_exists('id_disciplinaconteudo', $data) and !empty($data['id_disciplinaconteudo'])) {

                $entity = $this->find('\G2\Entity\DisciplinaConteudo', $data['id_disciplinaconteudo']);
                /*Valida se o array existe, para evitar uma gravação errada no banco e evitar um NOTICE */
                if ((isset($data['bl_ativo'])) and (!is_null($data['bl_ativo']))) {
                    $entity->setBl_ativo($data['bl_ativo']);
                }

            } else {
                $entity = new \G2\Entity\DisciplinaConteudo();
                $entity->setBl_ativo(true);
            }
            $disciplina = parent::getReference('\G2\Entity\Disciplina', (int)$data['id_disciplina']);
            $usuarioCadastro = parent::getReference('\G2\Entity\Usuario', (int)$data['id_usuariocadastro']);

            $entity->setDt_cadastro(new \DateTime());
            $entity->setId_disciplina($disciplina);
            $entity->setSt_descricaoconteudo($data['st_descricaoconteudo']);
            $entity->setId_formatoarquivo($formatoArquivo);
            $entity->setId_usuariocadastro($usuarioCadastro);

            $result = $this->save($entity);

            if (!$result) {
                throw new \Zend_Exception('Infomacoes do conteudo nao foram salvas, verifique!', 500);
            }

            //Caso tenha um arquivo enviado por upload
            if ($file) {
                $this->uploadConteudoDisciplina($result, $file);
            }

            $this->commit();
            return $result;

        } catch (\Zend_Exception $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage(), 500);
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage(), 500);
        }
    }

    /**
     * Metodo invocado pelo metodo salvarConteudoDisciplina, que faz o procedimento de upload e
     * descompactacao e renomeio de arquivos associados como conteudo da disciplina
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     *
     * @param $disciplinaConteudo
     * @param $file
     */
    public function uploadConteudoDisciplina($disciplinaConteudo, $file)
    {

        try {

            //Pegando do arquivo application.ini a definicao para localizacao dos arquivos de conteudo de disciplina
            $front = \Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $dir_upload_disciplina_conteudo = realpath($front->getOption('dir_upload_disciplina_conteudo'));

            if (!$front->getOption('dir_upload_disciplina_conteudo')) {
                throw new \Zend_Exception('Nao encontrada a definicao de diretorio para conteudo das disciplinas no arquivo de configuracao principal: ' . $front->getOption('dir_upload_disciplina_conteudo'), 500);
            }

            if (!is_dir($dir_upload_disciplina_conteudo)) {
                throw new \Zend_Exception('Não é um diretório válido para envio dos arquivos de conteúdo: ' . $dir_upload_disciplina_conteudo, 500);
            }

            $path = $dir_upload_disciplina_conteudo . '/' . $disciplinaConteudo->getId_disciplina()->getId_disciplina();

            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }

            $zf = new \Zend_File_Transfer();
            $zf->setDestination($path);

            //validar formato do arquivo
            if (!in_array($zf->getMimeType(), [
                'application/octet-stream',
                'application/zip',
                'application/pdf'
            ])) {
                throw new \Zend_Exception("Formato do arquivo não suportado.");
            }

            if (in_array($zf->getMimeType(), array('application/octet-stream', 'application/zip'))) {
                $zf->addFilter('Rename', $path . '/' . $disciplinaConteudo->getId_disciplinaconteudo() . '.zip');
            }

            if (in_array($zf->getMimeType(), array('application/pdf'))) {
                $zf->addFilter('Rename', $path . '/' . $disciplinaConteudo->getId_disciplinaconteudo() . '.pdf');
            }

            if ($zf->receive()) {

                if (in_array($zf->getMimeType(), array('application/octet-stream', 'application/zip'))) {
                    $zip = new \ZipArchive();

                    $zip->open($zf->getFileName());
                    $extract = $zip->extractTo($path . '/' . $disciplinaConteudo->getId_disciplinaconteudo());
                    $zip->close();
                }

            } else {
                throw new \Zend_Exception('Erro ao upar arquivo ao servidor', 500);
            }

        } catch (\Zend_Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @description Retornar dados da Vw_DisciplinaSerieNevel
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function retornarVwDisciplinaSerieNivel(array $params)
    {
        /*
         * Utilizei o queryBuilder direto na negocio, pois até o momento
         * não vi a necessidade de criar outro arquivo para ter apenas uma função
         * Caso, surja novas funções ligadas a essa entidade, criaremos os arquivos.
         * */
        try {
            $query = $this->em->createQueryBuilder()
                ->select(
                    'd.id_disciplina',
                    'd.st_disciplina',
                    'd.id_entidade',
                    'd.id_serie',
                    'd.id_nivelensino',
                    'd.nu_repeticao',
                    'd.st_serie',
                    'd.id_serieanterior',
                    'd.nu_identificador',
                    'd.nu_cargahoraria',
                    'd.bl_ativa',
                    'd.id_situacao',
                    'd.st_nivelensino',
                    'd.st_descricao'
                )
                ->from('\G2\Entity\VwDisciplinaSerieNivel', 'd')
                ->where('d.st_disciplina LIKE :st_disciplina')
                ->andWhere('d.id_entidade = :id_entidade')
                ->setParameter('st_disciplina', '%' . $params['st_disciplina'] . '%')
                ->setParameter('id_entidade', $params['id_entidade'])
                ->getQuery();

            return new \Ead1_Mensageiro($query->getResult(), \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao realizar a consulta: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function retornarDisciplinasByMatricula($params)
    {
        try {
            return $this->em->getRepository('\G2\Entity\MatriculaDisciplina')->retornarMatriculaDisciplinaByMatricula($params);

        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao pesquisar disciplinas.' . $e->getMessage());
        }
    }


    /***************************************************************
     *                                                              *
     *  Funções responsaveis por fazer o tratamento e importação    *
     *  Das salas de referencias                                    *
     *                                                              *
     ***************************************************************/


    /**
     * Essa função é responsável por fazer a validação
     * dos dados recebidos via post, caso tudo esteja correto
     * Passa para a função responsável por fazer a importação
     *
     * @param array $dados
     * @return true caso tudo ok, false caso tenha algum problema
     */
    public function validaDados(array $dados)
    {
        if (!$dados) {
            return false;
        } else if ($dados['moodleOrigem'] == $dados['moodleDestino']) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Função responsável por receber o array com os parametros
     * E iterar dentro de um for, para ler todos os parametros
     * e chamar a função responsável por salvar no banco de dados
     *
     * @param $dados
     * @return array
     */
    public function importarSalaTranferenia($dados)
    {
        $result = array();
        unset($dados['salasT'][0]); //Ignorando a primeira coluna do excel

        try {
            foreach ($dados['salasT'] as $value) {
                if ($value[0] != null && $value[0] != '' && $value[1] != null && $value[1] != '') {

                    $sala = $this->salvarNovaSala(
                        (int)$dados['moodleOrigem'],
                        (string)$value[0],
                        (string)$value[1],
                        (int)$dados['moodleDestino']
                    );

                    array_push($result, $sala);
                }
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

        return $result;
    }

    /**
     * Função responsável por salvar no banco de dados
     *
     * @param $moodleO
     * @param $st_codsistemaO
     * @param $st_codsistema
     * @param $id_entidadeintegracao
     * @return Ead1_Mensageiro
     * @throws Exception
     */
    public function salvarNovaSala($moodleO, $st_codsistemaO, $st_codsistema, $id_entidadeintegracao)
    {

        try {
            /** @var \G2\Entity\DisciplinaIntegracao $result */
            $result = $this->findOneBy(\G2\Entity\DisciplinaIntegracao::class, array(
                'st_codsistema' => $st_codsistemaO,
                'id_entidadeintegracao' => $moodleO
            ));

            $disciplinaBo = new \DisciplinaBO();
            $disciplinaTo = new \DisciplinaIntegracaoTO();
            $disciplinaTo->montaToDinamico($result->toBackboneArray());

            $disciplinaTo->setid_disciplinaintegracao(null);
            $disciplinaTo->setst_codsistema($st_codsistema); // Sala de referencia de DESTINO
            $disciplinaTo->dt_cadastro(new \DateTime());
            $disciplinaTo->setid_entidadeintegracao($id_entidadeintegracao);

            $importacao = $disciplinaBo->salvarDisciplinaIntegracao($disciplinaTo);

            unset($disciplinaBo);
            unset($disciplinaTo);

            return $importacao;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}

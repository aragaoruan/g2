<?php

namespace G2\Negocio;

use G2\Entity\VisualizaCampanha;

/**
 * Classe Negocio para Camapanha Comercial
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-24-02
 */
class CampanhaComercial extends Negocio
{

    /**
     * String repository name
     * @var string
     */
    private $repositoryName = '\G2\Entity\CampanhaComercial';
    private $repositorioCampanhaFormaPagamento = '\G2\Entity\CampanhaFormaPagamento';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna as campanhas
     * @param array $params
     * @return Object
     */
    public function retornarCampanhaPontualidadePadrao(array $params = array())
    {
        return $this->findOneBy($this->repositoryName, $params);
    }


    /**
     * Retorna 1 registro
     * @param $params
     * @param $id_entidade
     * @param bool $returnEntity
     * @return mixed
     */
    public function retornaVwCampanhaComercial($params, $id_entidade, $returnEntity = true)
    {

        return $this->em->getRepository('\G2\Entity\VwCampanhaComercial')
            ->retornaVwCampanhaComercial($params, $id_entidade, $returnEntity);

    }

    /**
     * Lista os dados da vw_campanhacomercial
     * @param $params
     * @param $id_entidade
     * @param bool $returnEntities
     * @return mixed
     */
    public function retornarVwCampanhaComercial($params, $id_entidade, $returnEntities = true)
    {

        return $this->em->getRepository('\G2\Entity\VwCampanhaComercial')
            ->listarCampanhas($params, $id_entidade, $returnEntities);

    }

    /**
     * Retorna os dados da vw_campanhaprodutocategoria feita para atender ao site do IMP
     * @param array $params
     * @param $idEntidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVwCampanhaProdutoCategoria(array $params, $idEntidade)
    {
        //'\G2\Entity\VwCampanhaProdutoCategoria',
        return $this->em->getRepository('\G2\Entity\VwCampanhaProdutoCategoria')
            ->retornaVwCampanhaProdutoCategoria($params, $idEntidade);
    }

    /**
     * Retorna quantas campanhas baseado nos parametros informados
     * @param $params
     * @param $id_entidade
     * @return mixed
     */
    public function retornarQuantidadeCampanha($params, $id_entidade)
    {

        return $this->em->getRepository('\G2\Entity\VwCampanhaComercial')
            ->retornarQuantidadeCampanha($params, $id_entidade);

    }

    public function marcarCampanhaViualizada(array $params)
    {

        if (empty($params['id_campanhacomercial'])) {
            throw new \Exception('É necessário informar o id da campanha');
        }

        if (empty($params['id_usuario'])) {
            throw new \Exception('É necessário informar o id do usuário');
        }

        $campanha = new VisualizaCampanha();
        $campanha = $campanha->findOneBy(array('id_campanhacomercial' => $params['id_campanhacomercial'], 'id_usuario' => $params['id_usuario']), true);
        if (!$campanha)
            $campanha = new VisualizaCampanha();

        $campanha->setDt_cadastro(new \DateTime());
        $campanha->setId_campanhacomercial(
            $this->getReference(
                '\G2\Entity\CampanhaComercial',
                $params['id_campanhacomercial']
            )
        );
        $campanha->setId_usuario(
            $this->getReference(
                '\G2\Entity\Usuario',
                $params['id_usuario']
            )
        );

        return $this->save($campanha);
    }

    /**
     * Retorna um único registro baseado no id_campanha da vw_campanhacomercial
     * @param $id
     * @param bool $returnEntities
     * @return array|\Doctrine\ORM\NoResultException|\Exception|null|object
     */
    public function findVwCampanhaComercial($id, $returnEntities = true)
    {
        $campanha = $this->find('\G2\Entity\VwCampanhaComercial', $id);

        if ($returnEntities) {
            return $campanha;
        } else {
            return $this->toArrayEntity($campanha);
        }
    }

    /**
     * Retorna as campanhas
     * @param array $params
     * @param bool|false $toArray
     * @return array|Object
     */
    public function retornarCampanhas(array $params = array(), $toArray = false)
    {
        $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->getId_entidade();

        //Repository para retornar as campanhas
        $result = $this->em->getRepository($this->repositoryName)->retornaCampanhaCustomizada($params, array('st_campanhacomercial' => 'asc'));

        if ($toArray) {
            foreach ($result as $key => $row) {
                $result[$key] = array(
                    'id_campanhacomercial' => $row->getId_campanhacomercial(),
                    'st_campanhacomercial' => $row->getSt_campanhacomercial(),
                    'bl_aplicardesconto' => $row->getBl_aplicardesconto(),
                    'bl_ativo' => $row->getBl_ativo(),
                    'bl_disponibilidade' => $row->getBl_disponibilidade(),
                    'dt_cadastro' => $row->getDt_cadastro(),
                    'dt_fim' => $row->getDt_fim(),
                    'dt_inicio' => $row->getDt_inicio(),
                    'id_entidade' => $row->getId_entidade() ? $row->getId_entidade()->getId_entidade() : null,
                    'id_situacao' => $row->getId_situacao() ? $row->getId_situacao()->getId_situacao() : null,
                    'id_tipodesconto' => $row->getId_tipodesconto() ? $row->getId_tipodesconto()->getId_tipodesconto() : null,
                    'id_usuariocadastro' => $row->getId_usuariocadastro() ? $row->getId_usuariocadastro()->getId_usuario() : null,
                    'id_tipocampanha' => $row->getId_tipocampanha() ? $row->getId_tipocampanha()->getId_tipocampanha() : null,
                    'nu_disponibilidade' => $row->getNu_disponibilidade(),
                    'st_descricao' => $row->getSt_descricao(),
                    'bl_todosprodutos' => $row->getBl_todosprodutos(),
                    'bl_todasformas' => $row->getBl_todasformas(),
                    'id_categoriacampanha' => $row->getId_categoriacampanha() ? $row->getId_categoriacampanha()->getId_categoriacampanha() : null,
                    'nu_valordesconto' => $row->getNu_valordesconto(),
                    'nu_diasprazo' => $row->getNu_diasprazo(),
//                    'id_premio' => $row->getId_premio()
                );
            }

        }

        return $result;
    }

    /**
     * Retorna uma campanha
     * @param $id
     * @return array
     */
    public function findCampanha($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /**
     * Retorna Categoria Campanha por parametro
     * @param array $where
     * @return G2\Entity\CategoriaCampanha
     */
    public function retornarCategoriaCampanha(array $where = array())
    {
        return $this->findBy('G2\Entity\CategoriaCampanha', $where);
    }

    /**
     * Retorna tipo de campanha por parametro
     * @param array $where
     * @return G2\Entity\TipoCampanha
     */
    public function retornarTipoCampanha(array $where = array())
    {
        return $this->findBy('G2\Entity\TipoCampanha', $where);
    }

    /**
     * Retorna tipo de desconto po parametro
     * @param array $where
     * @return G2\Entity\TipoDesconto
     */
    public function retornarTipoDesconto(array $where = array())
    {
        return $this->findBy('G2\Entity\TipoDesconto', $where);
    }

    public function salvarCampanha(array $data)
    {
        $this->beginTransaction();
        try {
            //Instancia as Negocios
            $entidadeNegocio = new Entidade();
            $situacaoNegocio = new Situacao();

            //instancia a entity
            $entity = new \G2\Entity\CampanhaComercial();

            //verifica se o id da campanha foi passado
            if (isset($data['id_campanhacomercial']) && !empty($data['id_campanhacomercial'])) {
                $entity = $this->find($this->repositoryName, $data['id_campanhacomercial']); //busca a campanha pelo id
            }
            //instancia os atributos
            if (!empty($data['id_categoriacampanha'])) {
                $categoriaCampanha = $this->getReference('G2\Entity\CategoriaCampanha', $data['id_categoriacampanha']);
                $entity->setId_categoriacampanha($categoriaCampanha); //Set instancia de categoria na entity
            }
            if (isset($data['id_entidade']) && !empty($data['id_entidade'])) {
                $entidade = $entidadeNegocio->getReference($data['id_entidade']);
            } else {
                if ($entity->getId_entidade()) {
                    $entidade = $entity->getId_entidade();
                } else {
                    $entidade = $entidadeNegocio->getReference($this->sessao->id_entidade);
                }
            }
            if (!empty($data['id_situacao'])) {
                $situacao = $situacaoNegocio->getReference($data['id_situacao']);
                $entity->setId_situacao($situacao);
            }

            if (!empty($data['id_tipocampanha'])) {
                if ($data['id_tipocampanha'] == 'NaN') {
                    return array(
                        'type' => 'warning',
                        'title' => 'Atenção!',
                        'text' => 'Selecione a finalidade!'
                    );
                }
                $tipoCampanha = $this->getReference('\G2\Entity\TipoCampanha', $data['id_tipocampanha']);
                $entity->setId_tipocampanha($tipoCampanha);
            }

            if (!empty($data['id_tipodesconto']) && $data['id_tipodesconto'] != 'null') {
                $tipoDesconto = $this->getReference('\G2\Entity\TipoDesconto', $data['id_tipodesconto']);
                $entity->setId_tipodesconto($tipoDesconto);
            }

            if (isset($data['id_usuariocadastro']) && !empty($data['id_usuariocadastro'])) {
                $usuario = $this->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']);
            } else {
                if ($entity->getId_usuariocadastro()) {
                    $usuario = $entity->getId_usuariocadastro();
                } else {
                    $usuario = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);
                }
            }

            if (!empty($data['id_finalidadecampanha'])) {
                $finalidadeCampanha = $this->getReference('\G2\Entity\FinalidadeCampanha', $data['id_finalidadecampanha']);
                $entity->setId_finalidadecampanha($finalidadeCampanha);
            }

            if (empty($data['id_premio'])) {
                $id_premio = $this->getReference('\G2\Entity\Premio', 1);
            } else {
                $id_premio = $data['id_premio'];
            }

            if (!empty($data['nu_disponibilidade']) && $data['nu_disponibilidade'] != 'null') {
                $nu_disponibilidade = $data['nu_disponibilidade'];
            }

            if (!empty($data['nu_valordesconto']) && $data['nu_valordesconto'] != 'null') {
                $nu_valordesconto = $data['nu_valordesconto'];
            }

            if (!empty($data['nu_diasprazo']) && $data['nu_diasprazo'] != 'null') {
                $nu_diasprazo = $data['nu_diasprazo'];
            }

            if (!empty($data['bl_mobile']) && $data['bl_mobile'] != 'null') {
                $bl_mobile = $data['bl_mobile'];
            }

            if (!empty($data['bl_portaldoaluno']) && $data['bl_portaldoaluno'] != 'null') {
                $bl_portaldoaluno = $data['bl_portaldoaluno'];
            }

            if (!empty($data['st_link']) && $data['st_link'] != 'null') {
                $st_link = $data['st_link'];
                if (!preg_match('/http/', $st_link)) {
                    $st_link = "http://" . $st_link;
                }
            } else {
                $st_link = "";
            }

            $entity
                ->setBl_aplicardesconto(isset($data['bl_aplicardesconto']) ? $data['bl_aplicardesconto'] : $entity->getBl_aplicardesconto())
                ->setBl_ativo(isset($data['bl_ativo']) ? $data['bl_ativo'] : $entity->getBl_ativo())
                ->setBl_disponibilidade(isset($data['bl_disponibilidade']) ? $data['bl_disponibilidade'] : $entity->getBl_disponibilidade())
                ->setBl_todasformas(isset($data['bl_todasformas']) ? $data['bl_todasformas'] : $entity->getBl_todasformas())
                ->setBl_todosprodutos(isset($data['bl_todosprodutos']) ? $data['bl_todosprodutos'] : $entity->getBl_todosprodutos())
                ->setDt_cadastro($entity->getDt_cadastro() ? $entity->getDt_cadastro() : new \DateTime())
                ->setDt_fim(isset($data['dt_fim']) && !empty($data['dt_fim']) ? $this->converteDataBanco($data['dt_fim']) : NULL)
                ->setDt_inicio(isset($data['dt_inicio']) ? $this->converteDataBanco($data['dt_inicio']) : $entity->getDt_inicio())
                ->setNu_disponibilidade(isset($nu_disponibilidade) ? $nu_disponibilidade : $entity->getNu_disponibilidade())
                ->setSt_campanhacomercial(isset($data['st_campanhacomercial']) ? $data['st_campanhacomercial'] : $entity->getSt_campanhacomercial())
                ->setSt_descricao(isset($data['st_descricao']) ? $data['st_descricao'] : $entity->getSt_descricao())
                ->setId_usuariocadastro($usuario)
                ->setId_entidade($entidade)
                ->setSt_link(isset($st_link) ? $st_link : $entity->getSt_link())
                ->setId_premio(!empty($id_premio) ? $id_premio : null)
                ->setNu_valordesconto(isset($nu_valordesconto) ? $nu_valordesconto : $entity->getNu_valordesconto())
                ->setNu_diasprazo(isset($nu_diasprazo) ? $nu_diasprazo : $entity->getNu_diasprazo())
                ->setBl_mobile(isset($bl_mobile) ? $bl_mobile : NULL)
                ->setBl_portaldoaluno(isset($bl_portaldoaluno) ? $bl_portaldoaluno : NULL);

            if (!file_exists(realpath(APPLICATION_PATH . "/../public/upload/campanha"))) {
                mkdir((APPLICATION_PATH . "/../public/upload/campanha"), 0777);
            }

            //checa se o tamanho da foto é permitido
            if (!empty($data['imagem'])) {
                try {
                    $this->checaTamanhoImagem($data['imagem']['st_imagem']);
                } catch (\Exception $e) {
                    return array(
                        'type' => 'warning',
                        'title' => 'Atenção!',
                        'text' => $e->getMessage()
                    );
                }
            }

            //faz o upload da imagem e seta o valor na entidade
            try {
                if (!empty($data['imagem']['st_imagem'])) {
                    if ($entity->getSt_imagem()) {
                        try {
                            $this->destruirArquivo($entity->getSt_imagem());
                        } catch (\Exception $e) {
                            return array(
                                'type' => 'warning',
                                'title' => 'Atenção!',
                                'text' => $e->getMessage()
                            );
                        }
                    }
                    try {
                        $entity->setSt_imagem('upload/campanha/' . $this->uploadImagem($data['imagem']['st_imagem'], 'campanha_'));
                    } catch (\Exception $e) {
                        return array(
                            'type' => 'warning',
                            'title' => 'Atenção!',
                            'text' => $e->getMessage()
                        );
                    }
                } else {
                    $entity->setSt_imagem($entity->getSt_imagem());
                }
            } catch (\Exception $e) {
                return array(
                    'type' => 'warning',
                    'title' => 'Atenção!',
                    'text' => $e->getMessage()
                );
            }

            if (!$this->validaDataCampanha($entity->getDt_inicio(), $entity->getDt_fim())) {
                return array(
                    'type' => 'warning',
                    'title' => 'Atenção!',
                    'text' => 'A Data de Término Não Pode ser Menor que a Data de Inicio!'
                );
            } else {
                $retorno = $this->save($entity);
                $this->commit();
                return $retorno;
            }
        } catch (\Exception $ex) {
            $this->rollback();
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Checa o tamanho de uma imagem
     * @param string $arquivo
     * @return boolean
     * @throws \Exception
     */
    private function checaTamanhoImagem($arquivo)
    {
        list($width, $height) = getimagesize($arquivo['tmp_name']);
        if ($width > 1024 || $height > 768 || filesize($arquivo['tmp_name']) >= 5000000) {
            throw new \Exception("A imagem é muito grande, reduza o seu tamanho e tente novamente.");
        } else {
            return true;
        }
    }

    /**
     * Destroi uma imagem
     * @param string $arquivo
     * @return boolean
     * @throws \Exception
     */
    private function destruirArquivo($arquivo)
    {
        if (file_exists(realpath(APPLICATION_PATH . "/../public") . "/" . $arquivo)) {
            $destruir = unlink(realpath(APPLICATION_PATH . "/../public") . "/" . $arquivo);

            if (!$destruir) {
                throw new \Exception('Não foi possível excluir o arquivo já cadastrado.');
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * Faz upload da imagem
     * @param string $prefixo
     * @param array $arquivo
     * @return string
     * @throws \Exception
     */
    private function uploadImagem($arquivo, $prefixo = null)
    {
        $hash = uniqid();
        $extensao = null;
        if (is_array($arquivo) && !empty($arquivo)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            if (isset($arquivo['tmp_name'])) {
                $mime = finfo_file($finfo, $arquivo['tmp_name']);
            } else {
                throw new \Exception('O arquivo não possui uma extensão válida');
            }
            finfo_close($finfo);

            foreach (\G2\Constante\Utils::$tipoImagem as $key => $value) {
                if ($key == $mime) {
                    $extensao = $value;
                }
            }

            if ($extensao == null) {
                throw new \Exception('Extensão não encontrada.');
            }

            if (is_numeric(strpos($mime, 'image'))) {
                $nome = $prefixo . "$hash.$extensao";
                $upload = move_uploaded_file($arquivo['tmp_name'], realpath(APPLICATION_PATH . "/../public/upload/campanha") . "/" . $nome);
                if ($upload) {
                    return $nome;
                } else {
                    throw new \Exception('Não foi possível gravar a imagem no servidor.');
                }
            }
        } else {
            throw new \Exception('Envie um arquivo válido para upload.');
        }
    }

    /**
     * Busca todas as campanhas da entidade da sessão com o bl_aplicardesconto = true,
     * percorre os registros e altera todos os bl_aplicardesconto para false
     * @param null $id_campanha
     * @return bool
     * @throws \Zend_Exception
     */
    private function alteraStatusCampanhasPontualidade($id_campanha = null)
    {
        try {
            $this->beginTransaction();
            //busca todos os registros
            $campanhas = $this->findBy($this->repositoryName, array(
                'bl_aplicardesconto' => true,
                'id_entidade' => $this->getId_entidade(),
                'bl_ativo' => true
            ));
            //verifica se encontrou
            if ($campanhas) {
                //percorre todos eles
                foreach ($campanhas as $campanha) {
                    //verifica se foi passado o parametro id_campanha e se esse id é diferente ao do laço atual
                    if (($id_campanha != $campanha->getId_campanhacomercial())) {
                        //muda o valor
                        $campanha->setBl_aplicardesconto(false);
                        //salva
                        if (!$this->save($campanha)) {
                            throw new \Exception("Erro ao alterar status da campanha " . $campanha->getId_campanhacomercial());
                        }
                    }
                }
            }
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro a tentar alterar status campanhas pontualidade. " . $e->getMessage());
        }
    }

    /**
     * Deleta logicamente uma campanha
     * @param interger $id
     * @return mixed
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function deletarCampanha($id)
    {
        try {
            if (!$id) {
                throw new \Exception('Id da Campanha deve ser passado.');
            }
            $data['bl_ativo'] = false;
            $data['id_campanhacomercial'] = $id;
            return $this->salvarCampanha($data);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
            return false;
        }
    }

    /**
     * Compara a data inicio e data fim da campanha e valida se a data fim é maior que a data inicio
     * @param \DateTime $dtInicio Data de Inicio da Campanha
     * @param \DateTime $dtFim Data fim da Campanha
     * @return boolean
     */
    private function validaDataCampanha($dtInicio, $dtFim)
    {
        if (!empty($dtInicio) && !empty($dtFim)) {
            return $this->comparaData($dtInicio->format('Y-m-d H:i:s'), $dtFim->format('Y-m-d H:i:s'));
        } else {
            return true;
        }
    }

    /**
     * Salva Campanha Desconto
     * @param array $data
     * @return \G2\Entity\CampanhaDesconto
     * @throws \Zend_Exception
     */
    public function salvarCampanhaDesconto(array $data)
    {
        try {
            $entity = new \G2\Entity\CampanhaDesconto();
            if (isset($data['id_campanhadesconto']) && !empty($data['id_campanhadesconto'])) {
                $entity = $this->find('\G2\Entity\CampanhaDesconto', $data['id_campanhadesconto']);
            }

            if ($data['id_campanhacomercial']) {
                $campanhaComercial = $this->getReference($this->repositoryName, $data['id_campanhacomercial']);
                $entity->setId_campanhacomercial($campanhaComercial);
            }
            if ($data['id_meiopagamento']) {
                $meioPgto = $this->getReference('\G2\Entity\MeioPagamento', $data['id_meiopagamento']);
                $entity->setId_meiopagamento($meioPgto);
            }
            if (isset($data['nu_descontomaximo'])) {
                $entity->setNu_descontomaximo($data['nu_descontomaximo']);
            }
            if (isset($data['nu_descontominimo'])) {
                $entity->setNu_descontominimo($data['nu_descontominimo']);
            }
            if (isset($data['nu_valormax'])) {
                $entity->setNu_valormax($data['nu_valormax']);
            }
            if (isset($data['nu_valormin'])) {
                $entity->setNu_valormin($data['nu_valormin']);
            }
            return $this->save($entity);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Salva Formas de Pagamento da Campanha através de um array
     * @param array $dataArray
     * @return mixed
     */
    public function salvarArrayCampanhaFormaPagamento(array $arrayData)
    {
        try {
            $bo = new \FormaPagamentoBO();
            $arrFormaPagamento = array();
            if ($arrayData) {
                foreach ($arrayData as $formaPgto) {
                    $to = new \CampanhaFormaPagamentoTO();
                    $to->montaToDinamico($formaPgto);
                    $arrFormaPagamento[] = $to;
                }
            }
            return $bo->salvarArrayCampanhaFormaPagamento($arrFormaPagamento);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Salva Produtos vinculados a campanha através de um array
     * @param array $arrayData
     * @return mixed
     */
    public function salvarArrayCampanhaProdutos(array $arrayData)
    {
        try {
            $bo = new \ProdutoBO();
            $arrProduto = array();
            if ($arrayData) {
                foreach ($arrayData as $produto) {
                    $to = new \CampanhaProdutoTO();
                    $to->montaToDinamico($produto);
                    $arrProduto[] = $to;
                }
            }
//            print_r($arrProduto);exit;
            return $bo->cadastrarArrayCampanhaProduto($arrProduto);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Salva array de categorias vinculadas a campanha
     * @param array $arrayData
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function salvarArrayCampanhaCategoria(array $arrayData)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro(); //instancia mensageiro
            $this->beginTransaction(); //abre transação
            $arrCategoria = array();
            if ($arrayData) {
                //deleta todas as categorias vinculadas a campanha
                $delete = $this->deleteCampanhaCategoriaByCampanha($arrayData[0]['id_campanhacomercial']); //recupera sempre o id_campanhacomercial da chave 0
                if ($delete->getType() != 'success') { //se retornar erro ao apagar cria um exception
                    throw new \Exception("Erro ao desvincular Campanha Categorias - " . $delete->getText());
                }
                //Percorre o array
                foreach ($arrayData as $categoria) {
                    $entity = new \G2\Entity\CampanhaCategorias(); //instancia a entity
                    //Recupera as instancias
                    $campanha = $this->getReference($this->repositoryName, $categoria['id_campanhacomercial']);
                    $categoria = $this->getReference('\G2\Entity\Categoria', $categoria['id_categoria']);
                    //Seta os atributos
                    $entity->setId_campanhacomercial($campanha)
                        ->setId_categoria($categoria);
                    //Salva
                    $result = $this->save($entity);
                    if ($result) {
                        $arrCategoria[] = $this->toArrayEntity($result); //Atribui o que foi salvo no array
                    } else {
                        throw new \Exception($result);
                    }
                }
            }
            $this->commit(); //commit
            return $mensageiro->setMensageiro($arrCategoria, \Ead1_IMensageiro::SUCESSO); //retorna o mensageiro
        } catch (\Exception $ex) {
            $this->rollback();
            return $mensageiro->setMensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Deleta todas as categorias vinculadas a campanha
     * @param array $idCampanha
     * @return \Ead1_Mensageiro
     */
    public function deleteCampanhaCategoriaByCampanha($idCampanha)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();
            $result = true;
            $results = $this->findBy('\G2\Entity\CampanhaCategorias', array('id_campanhacomercial' => $idCampanha)); //Recupera todas as categorias por campanha
            //Se tiver alguma categoria
            if ($results) {
                //percorre o array de objetos
                foreach ($results as $row) {
                    //recupera a campanha categoria
                    $entity = $this->find('\G2\Entity\CampanhaCategorias', $row->getId_campanhacategoris());
                    if (!$this->delete($entity)) { //apaga, se der erro ele sata a variavel para false
                        $result = false;
                    }
                }
            }
            //verifica a variavel de resultado
            if ($result) {
                return $mensageiro->setMensageiro('Campanha Categorias desvinculadas', \Ead1_IMensageiro::SUCESSO);
            } else {
                return $mensageiro->setMensageiro('Erro ao desvincular Campanha Categorias', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $ex) {
            return $mensageiro->setMensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Deleta categorias vinculadas a campanha
     * @param array $idCategoria
     * @return \Ead1_Mensageiro
     */
    public function deleteCampanhaCategoria($idCategoria)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();
            $result = true;
            //recupera a campanha categoria
            $entity = $this->find('\G2\Entity\CampanhaCategorias', $idCategoria);
            if (!$this->delete($entity)) { //apaga, se der erro ele sata a variavel para false
                $result = false;
            }
            //verifica a variavel de resultado
            if ($result) {
                return $mensageiro->setMensageiro('Campanha Categorias desvinculadas', \Ead1_IMensageiro::SUCESSO);
            } else {
                return $mensageiro->setMensageiro('Erro ao desvincular Campanha Categorias', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $ex) {
            return $mensageiro->setMensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna Vw Campanha Forma de pagamento
     * @param array $params
     * @param array $order
     * @return G2\Entity\VwCampanhaFormaPagamento
     */
    public function retornarVwCampanhaFormaPagamento(array $where = array(), array $order = array())
    {
        $where['id_entidade'] = isset($where['id_entidade']) ? $where['id_entidade'] : $this->sessao->id_entidade;
        return $this->findBy('G2\Entity\VwCampanhaFormaPagamento', $where, $order);
    }

    /**
     * Retorna Vw campanha produto
     * @param array $where
     * @param array $order
     * @return G2\Entity\VwCampanhaProduto
     */
    public function retornarVwCampanhaProduto(array $where = array(), array $order = array())
    {
        return $this->findBy('G2\Entity\VwCampanhaProduto', $where, $order);
    }

    /**
     * Retorna Categorias da campanha
     * @param array $where
     * @param array $order
     * @return G2\Entity\CampanhaCategorias
     */
    public function retornaCampanhaCategorias(array $where = array(), array $order = array())
    {
        return $this->findBy('G2\Entity\CampanhaCategorias', $where, $order);
    }

    /**
     * Retorna dados para compor listagem da tabela de cupons da tab de Cupons no formulário
     * @param array $where
     * @return array
     */
    public function retornarDadosListaTabCupom(array $where = array())
    {
        $negocio = new Cupom();
        $result = $negocio->retornarCupons($where);
        $arrReturn = array();
        if ($result) {
            foreach ($result as $cupom) {
                $dt_inicio = new \DateTime($cupom->getDt_inicio());
                $dt_fim = new \DateTime($cupom->getDt_fim());
                $arrReturn[] = array(
                    'id' => $cupom->getId_cupom(),
                    'id_cupom' => $cupom->getId_cupom(),
                    'st_codigocupom' => $cupom->getSt_codigocupom(),
                    'st_prefixo' => $cupom->getSt_prefixo(),
                    'st_complemento' => $cupom->getSt_complemento(),
                    'nu_desconto' => $cupom->getNu_desconto(),
                    'id_usuariocadastro' => $cupom->getId_usuariocadastro() ? $cupom->getId_usuariocadastro()->getId_usuario() : NULL,
                    'id_tipodesconto' => $cupom->getId_tipodesconto() ? $cupom->getId_tipodesconto()->getId_tipodesconto() : NULL,
                    'st_tipodesconto' => $cupom->getId_tipodesconto() ? $cupom->getId_tipodesconto()->getSt_tipodesconto() : NULL,
                    'dt_inicio' => $dt_inicio->format('Y-m-d H:i:s'),
                    'dt_fim' => $dt_fim->format('Y-m-d H:i:s'),
                    'st_campanhacomercial' => $cupom->getId_campanhacomercial() ? $cupom->getId_campanhacomercial()->getSt_campanhacomercial() : NULL,
                    'id_campanhacomercial' => $cupom->getId_campanhacomercial() ? $cupom->getId_campanhacomercial()->getId_campanhacomercial() : NULL,
                    'bl_ativacao' => $cupom->getBl_ativacao(),
                    'bl_unico' => $cupom->getBl_unico(),
                    'dt_cadastro' => $cupom->getDt_cadastro(),
                    'bl_ativo' => $cupom->getBl_ativo()
                );
            }
        }
        return $arrReturn;
    }

    /**
     * Deleta vinculo de Produto com Campanha comercial
     * @param array $params
     * @return bool
     * @throws \Zend_Exception
     */
    public function deleteCampanhaProduto(array $params)
    {
        try {
            $result = $this->findBy("\G2\Entity\CampanhaProduto", $params);
            if ($result) {
                $this->beginTransaction();
                foreach ($result as $campanhaproduto) {
                    $camp = $this->delete($campanhaproduto);
                    if (!$camp) {
                        throw new \Exception("Erro ao deletar vinculo de Produto com Campanha");
                    }
                }
                $this->commit();
                return true;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar apagar vinculo de campanha com produto." . $e->getMessage());
        }
    }

    public function deletaCampanhaFormaPagamento($idCampanha, $idFormaPagamento)
    {

        try {
            $array = array(
                'id_campanhacomercial' => $idCampanha,
                'id_formapagamento' => $idFormaPagamento
            );

            $result = $this->findOneBy($this->repositorioCampanhaFormaPagamento, $array);
            $return = $this->delete($result);

        } catch (\Exception $e) {

            $return = false;
        }

        return $return;
    }

    public function inserirCampanhaFormaPagamento($idCampanha, $idMeioPagamento)
    {

        try {

            $entity = new \G2\Entity\CampanhaFormaPagamento();
            $entity->setId_campanhacomercial($idCampanha);
            $entity->setId_formapagamento($idMeioPagamento);

            $return = $this->save($entity);

        } catch (\Exception $e) {

            $return = false;
        }

        return $return;
    }

    /**
     * Método para retornar dados da consulta do webservice de campanha premio.
     * @param array $produtosId array com os id's dos produtos array(produto_id,...,[produto_id])
     * @param integer $idEntidade ID da entidade
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarConsultaWsCampanhaPremio(array $produtosId, $idEntidade)
    {
        try {
            //busca as campanhas premios vinculadas aos produtos
            $result = $this->em->getRepository('\G2\Entity\VwCampanhaPremioProduto')
                ->retornaDadosVwCampanhaPremioProduto($produtosId, $idEntidade);

            $arrReturn = array();
            if ($result) {
                $arrCampanhas = array();
                foreach ($result as $row) {
                    //separa o id da campanha e do produto para usar nos indices dos arrays
                    $idCampanha = $row['id_campanhacomercial'];
                    $idProduto = $row['id_produto'];

                    //verifica se não existe uma posição no array com o id da campanha, se não existir cria
                    if (!isset($arrCampanhas[$idCampanha])) {
                        $arrCampanhas[$idCampanha] = array(
                            'id_campanha' => $idCampanha,
                            'st_campanha' => $row['st_campanhacomercial'],
                            'id_premio' => $row['id_premio'],
                            'bl_compraacima' => boolval($row['bl_compraacima']),
                            'nu_comprasacima' => floatval($row['nu_comprasacima']),
                            'bl_compraunidade' => boolval($row['bl_compraunidade']),
                            'nu_compraunidade' => intval($row['nu_compraunidade']),
                            'st_prefixocupompremio' => $row['st_prefixocupompremio'],
                            'nu_descontopremio' => floatval($row['nu_descontopremio']),
                            'id_tipodescontopremio' => $row['id_tipodescontopremio'],
                            'id_tipopremio' => $row['id_tipopremio'],
                            'bl_promocaocumulativa' => boolval($row['bl_promocaocumulativa']),
                            'id_cupomcampanha' => $row['id_cupomcampanha']

                        );
                    }
                    //cria no array na posição do id da campanha outras posições com os dados do produto
                    $arrCampanhas[$idCampanha]['produtos'][$idProduto] = array(
                        'id_produto' => $row['id_produto'],
                        'nu_valorproduto' => floatval($row['nu_valorproduto'])
                    );
                }

                //valida quais campanhas respeitam os criterios
                $arrProdutosValidos = $this->validarItensCampanhaPremio($arrCampanhas);


                //verifica se tem produtos validos para retornar na campanha
                if ($arrProdutosValidos) {
                    //percorre o primeiro resultado novamente
                    foreach ($result as $key => $row) {
                        //verifica se o id do produto no laço esta no array de produtos validos
                        if (in_array($row['id_produto'], $arrProdutosValidos)) {

                            //atribui o valor do produto na variavel
                            $id_produto = $row['id_produto'];

                            //verifica se não existe a chave com o id do produto
                            if (!isset($arrReturn[$id_produto])) {
                                $arrReturn[$id_produto] = array(
                                    'id_produto' => $row['id_produto'],
                                    'campanhapremio' => array()
                                );
                            }

                            //cria uma nova posição para armazenar as campanhas
                            $arrReturn[$id_produto]['campanhapremio'][$row['id_campanhacomercial']] = array(
                                'id_campanha' => $row['id_campanhacomercial'],
                                'st_tipopremio' => \G2\Constante\TipoPremio::getTipo($row['id_tipopremio']),
                                'st_tipodesconto' => $row['st_tipodescontopremio'],
                                'nu_valordesconto' => floatval($row['nu_descontopremio']),
                                'produtos' => array()
                            );

                            //busca os produtos que estão vinculados ao premio
                            $produtosPremio = $this->retornarProdutosVinculadosPremio($row['id_campanhacomercial']);
                            if ($produtosPremio) {
                                //percorre os registros para atribuir ao array somente o nome do produto e do tipo
                                foreach ($produtosPremio as $prod) {
                                    $arrReturn[$id_produto]['campanhapremio'][$row['id_campanhacomercial']]['produtos'][] = array(
                                        'st_produto' => $prod->getSt_produto(),
                                        'st_tipoproduto' => $prod->getSt_tipoproduto()
                                    );
                                }


                            }
                        }
                    }
                }

            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados. " . $e->getMessage());
        }

    }


    /**
     * Valida os produtos nas campanhas premio
     * @param array $campanhas
     * @return array
     * @throws \Exception
     */
    private function validarItensCampanhaPremio(array $campanhas)
    {
        try {
            foreach ($campanhas as $campanha) {
                $idCampanha = $campanha['id_campanha'];
                //verifica se a campanha valida o valor dos produtos
                if ($campanha['bl_compraacima']) {
                    $valorCompra = $campanha['nu_comprasacima'];
                    $valorProdutos = 0;

                    //percorre os produtos para calcular o valor deles
                    foreach ($campanha['produtos'] as $produto) {
                        $valorProdutos += $produto['nu_valorproduto'];
                    }

                    //verifica se o valor dos produtos é maior que o valor aceito pela campanha
                    if ($valorProdutos > $valorCompra) {
                        unset($campanhas[$idCampanha]);//se o valor foi maior remove a campanha
                    }

                } elseif ($campanha['bl_compraunidade']) {//ou a quantidade de produtos

                    //valida se a quantidade de produtos da campanha é menor que a aceita pela companha
                    if (count($campanha['produtos']) > $campanha['nu_compraunidade']) {
                        unset($campanhas[$idCampanha]);//se a quantidade for maior remove a campanha
                    }

                }
            }
            $arrayProdutos = array();
            foreach ($campanhas as $campanha) {
                foreach ($campanha['produtos'] as $produto) {
                    if (!in_array($produto['id_produto'], $arrayProdutos))
                        array_push($arrayProdutos, $produto['id_produto']);
                }


            }

            return $arrayProdutos;

        } catch (\Exception $e) {
            throw new \Exception("Erro ao validar dados dos itens das campanhas. " . $e->getMessage());
        }
    }

    /**
     * Método para retornar produtos que estão vinculados ao premio
     * @param integer $idCampanha id da campanha comercial
     * @param bool|false $toArray parametro para retornar um array de array ao inves de array de objetos
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarProdutosVinculadosPremio($idCampanha, $toArray = false)
    {
        try {
            //busca os dados ordenando os valores pelo nome do produto
            $result = $this->findBy(
                '\G2\Entity\VwProdutosPremio',
                array('id_campanhacomercial' => $idCampanha),
                array('st_produto' => 'ASC')
            );

            //verifica se tem resultado e se o parametro para retornar um array é true
            if ($result && $toArray) {
                //percorre os registros e transforma tudo em array
                foreach ($result as $key => $row) {
                    $result[$key] = $this->toArrayEntity($row);
                }
            }

            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar produtos vinculados ao prêmio. " . $e->getMessage());
        }
    }

    /**
     * Método para retornar tipo de desconto (Porcentagem e Valor) e Valor da campanha de pontualidade vinculada a venda
     * @param integer $idVenda
     * @return \Ead1_Mensageiro
     */
    public function retornarCampanhaPontualidadeByVenda($idVenda)
    {
        try {
            //instancia o mensageiro e a negocio da venda
            $mensageiro = new \Ead1_Mensageiro();
            $vendaNegocio = new \G2\Negocio\Venda();
            //busca a venda
            $result = $vendaNegocio->findVenda($idVenda);
            //verifica se tem resultado e se tem campanha vinculada
            if ($result && $result->getId_campanhapontualidade()) {
                $campanha = $result->getId_campanhapontualidade();//atribui os dados da campanha a variavel

                //verifica se a campanha esta ativa
                if (!$campanha->getBl_ativo()) {
                    throw new \Exception("Campanha não esta ativa.");
                }

                //verifica se a campanha começou
                if (!$this->comparaData($campanha->getDt_inicio(), date("Y-m-d"))) {
                    throw new \Exception("Campanha ainda não iniciou.");
                }

                //verifica se a campanha terminou
                if (!$this->comparaData(date("Y-m-d"), $campanha->getDt_fim())) {
                    throw new \Exception("Campanha já finalizou.");
                }

                //atribui o tipo do desconto e o valor
                $arrReturn = array(
                    'tipo_desconto' => array(
                        'id' => $campanha->getId_tipodesconto()->getId_tipodesconto(),
                        'nome' => $campanha->getId_tipodesconto()->getSt_tipodesconto()
                    ),
                    'valor_desconto' => $campanha->getNu_valordesconto()
                );
                //seta o mensageiro
                $mensageiro->setMensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);

            }
        } catch (\Exception $e) {
            $mensageiro->setMensageiro("Erro ao buscar dados da campanha de pontualidade. " . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;//retorna o mensageiro
    }

    public function retornaCampanhasCupom($params)
    {
        try {
            return $result = $this->em->getRepository('\G2\Entity\CampanhaComercial')->retornaCampanhaComercialCupom($params);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar pesquisar campanhas do tipo cupom." . $e->getMessage());
        }
    }

    public function salvaPremio($params)
    {
        try {
            $this->beginTransaction();

            $objetoPremio = $this->find('\G2\Entity\Premio', (int)$params['id_premio']);
            if (!$objetoPremio) {
                $objetoPremio = new \G2\Entity\Premio();
                $objetoPremio->setId_campanhacomercial($this->getReference('\G2\Entity\CampanhaComercial', (int)$params['id_campanhacomercial']));
            }

            $objetoPremio->setDt_cadastro(new \DateTime());
            $objetoPremio->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objetoPremio->setBl_ativo(true);
            $objetoPremio->setBl_compraacima($params['bl_comprasacima']);

            if ($params['nu_comprasunidades']) {
                $objetoPremio->setNu_compraunidade($params['nu_comprasunidades']);
            }
            if ($params['nu_comprasacima']) {
                $objetoPremio->setNu_comprasacima($params['nu_comprasacima']);
            }

            $objetoPremio->setBl_compraunidade($params['bl_comprasunidades']);

            $objetoPremio->setBl_promocaocumulativa($params['bl_promocaoCumulativa']);
            $objetoPremio->setId_tipopremio($params['id_tipopremio']);


            if ($params['id_tipopremio'] == 2) {
                $objetoPremio->setSt_prefixocupompremio($params['premio']['prefixo_cupom']);
                $objetoPremio->setId_tipodescontopremio($params['premio']['tipo_desconto']);
                $objetoPremio->setNu_descontopremio($params['premio']['valor_desconto']);
                $objetoPremio->setId_cupomcampanha($this->getReference('\G2\Entity\CampanhaComercial', $params['premio']['select_campanha_cupom']));
            }

            $resultado = $this->save($objetoPremio);

            if ($resultado) {
                $objetoCampanha = $this->find('\G2\Entity\CampanhaComercial', (int)$params['id_campanhacomercial']);
                $objetoCampanha->setId_premio($resultado);
                $this->save($objetoCampanha);
            }

            if ($params['id_tipopremio'] == 1) {
                if ($resultado) {
                    $objetoPremioProdutoToBl_false = $this->findBy('\G2\Entity\PremioProduto', array('id_premio' => $resultado->getId_premio()));
                    if ($objetoPremioProdutoToBl_false) {
                        foreach ($objetoPremioProdutoToBl_false as $value) {
                            $value->setBl_ativo(false);
                            $this->save($value);
                        }
                    }
                    foreach ($params['premio'] as $value) {
                        $objetoPremioProduto = $this->findOneBy('\G2\Entity\PremioProduto', array('id_premio' => $resultado->getId_premio(), 'id_produto' => (int)$value));
                        if (empty($objetoPremioProduto)) {
                            $objetoPremioProduto = new \G2\Entity\PremioProduto();
                            $objetoPremioProduto->setId_premio($this->getReference('\G2\Entity\Premio', $resultado->getId_premio()));
                            $objetoPremioProduto->setId_produto($this->getReference('\G2\Entity\Produto', $value));
                            $objetoPremioProduto->setId_campanhacomercial($this->getReference('\G2\Entity\CampanhaComercial', (int)$params['id_campanhacomercial']));
                            $objetoPremioProduto->setDt_cadastro(new \DateTime());
                            $objetoPremioProduto->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                        }
                        $objetoPremioProduto->setBl_ativo(true);
                        $this->save($objetoPremioProduto);
                    }
                }
            }

            $this->commit();

            return $resultado;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar cadastrar premio." . $e->getMessage(), 500);
        }
    }

    public function retornaPremio($params)
    {
        $premio = $this->find('\G2\Entity\Premio', $params);
        $premio = $this->toArrayEntity($premio);
        return $premio;
    }

    public function retornaProdutoPremioSelecionado($params)
    {
        return $this->em->getRepository('\G2\Entity\PremioProduto')->retornaPremioProduto(array('id_campanhacomercial' => $params['id_campanhacomercial'], 'id_premio' => $params['id_premio']));
    }

    public function retornaProdutoLike($params)
    {
        return $this->em->getRepository('\G2\Entity\VwProduto')->retornaProdutoLike($params);
    }

    public function getCampanhaTransferencia()
    {

        $result = $this->findBy('\G2\Entity\CampanhaComercial', array(
            'id_tipocampanha' => \G2\Constante\TipoCampanha::PRODUTOS,
            'id_finalidadecampanha' => \G2\Constante\FinalidadeCampanha::TRANSFERÊNCIA_CURSO,
            'id_categoriacampanha' => \G2\Constante\CategoriaCampanha::PROMOCAO));

        foreach ($result as $campanha) {
            $response[] = array(
                'id_campanhacomercial' => $campanha->getId_campanhacomercial(),
                'st_campanhacomercial' => $campanha->getSt_campanhacomercial(),
                'id_tipodesconto' => $campanha->getId_tipodesconto()->getId_tipodesconto(),
                'nu_valordesconto' => $campanha->getNu_valordesconto()
            );
        }
        return $response;
    }
}

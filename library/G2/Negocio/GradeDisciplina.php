<?php

namespace G2\Negocio;

/**
 * Classe negocio para grade disciplina
 * Class GradeDisciplina
 * @package G2\Negocio
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @since 2016-02-16
 */
class GradeDisciplina extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $id_matricula
     * @return mixed
     * @throws \Zend_Exception
     */
    public function verificaMensagemRenovacao($id_matricula = null)
    {
        try {
            $id_matricula = $id_matricula ?: $this->sessao->id_matricula;

            /** @var \G2\Repository\Matricula $rep */
            $rep = $this->getRepository('\G2\Entity\VwMatricula');
            return $rep->retornaDataLimiteRenovacao($id_matricula);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao verificar mensagem de renovação ' . $e->getMessage());
        }
    }


    public function retornaSalasMarcadas($params)
    {
        try {
            return $this->findBy('\G2\Entity\PreMatriculaDisciplina', array('id_venda' => $params['id_venda']));
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar salas marcadas. ' . $e->getMessage());
        }
    }

    public function saveGrade($params, $bl_confirmarvenda = false)
    {
        try {
            $this->beginTransaction();

            $id_venda = $params['modelo']['id_venda'];

            // Primeiramente remover as disciplinas antigas (em caso de edicao da grade)
            $disc_ja_adicionadas = $this->findBy('\G2\Entity\PreMatriculaDisciplina', array(
                'id_venda' => $id_venda
            ));

            if ($disc_ja_adicionadas) {
                foreach ($disc_ja_adicionadas as $disc_added) {
                    $delete = true;

                    foreach ($params['disciplinasSelecionadas'] as $key => $disciplina) {
                        // Caso o registro possua o mesmo id_disciplina e id_saladeaula, entao ele ja ESTA ADICIONADO,
                        // nao precisando nem REMOVER nem ADICIONAR
                        if ($disc_added->getId_disciplina()->getId_disciplina() == $disciplina['id_disciplina']
                            && $disc_added->getId_saladeaula()->getId_saladeaula() == $disciplina['id_saladeaula']
                        ) {
                            $delete = false;
                            unset($params['disciplinasSelecionadas'][$key]);
                            break;
                        }
                    }

                    if ($delete) {
                        $tramite = new \G2\Entity\Tramite();

                        $tramite->setDt_cadastro(new \DateTime())
                            ->setId_entidade($this->getReference('\G2\Entity\Entidade',
                                $this->sessao->id_entidade))
                            ->setBl_visivel(true)
                            ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite',
                                \G2\Constante\TipoTramite::ALTERANDO_DISCIPLINA_GRADE))
                            ->setId_usuario($this->getReference('\G2\Entity\Usuario',
                                $params['modelo']['id_usuario']))
                            ->setSt_tramite('Sala de aula ' . $disc_added->getId_saladeaula()->getSt_saladeaula()
                                . ' removida pelo usuário ' . $params['modelo']['st_nomecompleto']
                                . ' na seleção de disciplinas.');

                        $objTramite = $this->save($tramite);

                        $objTramiteVenda = new \G2\Entity\TramiteVenda();
                        $objTramiteVenda->setId_tramite($objTramite->getId_tramite());
                        $objTramiteVenda->setId_venda($id_venda);
                        $this->save($objTramiteVenda);

                        $this->delete($disc_added);
                    }
                }
            }

            // Salvando os dados das disciplinas selecionadas na tb_prematriculadisciplina
            if (array_key_exists('disciplinasSelecionadas', $params) && $params['disciplinasSelecionadas']) {
                foreach ($params['disciplinasSelecionadas'] as $value) {
                    $objeto = new \G2\Entity\PreMatriculaDisciplina();
                    $objeto->setId_disciplina($this->getReference('\G2\Entity\Disciplina',
                        $value['id_disciplina']));
                    $objeto->setId_saladeaula($this->getReference('\G2\Entity\SalaDeAula',
                        $value['id_saladeaula']));
                    $objeto->setId_venda($this->getReference('\G2\Entity\Venda', $id_venda));
                    $objeto->setDt_cadastro(new \DateTime());
                    $objeto->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario',
                        $params['modelo']['id_usuario']));
                    $this->save($objeto);

                    $tramite = new \G2\Entity\Tramite();

                    $tramite->setDt_cadastro(new \DateTime())
                        ->setId_entidade($this->getReference('\G2\Entity\Entidade',
                            $this->sessao->id_entidade))
                        ->setBl_visivel(true)
                        ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite',
                            \G2\Constante\TipoTramite::ALTERANDO_DISCIPLINA_GRADE))
                        ->setId_usuario($this->getReference('\G2\Entity\Usuario',
                            $params['modelo']['id_usuario']))
                        ->setSt_tramite('Sala de aula ' . $value['st_saladeaula'] . ' adicionada pelo usuário '
                            . $params['modelo']['st_nomecompleto'] . ' na seleção de disciplinas.');

                    $objTramite = $this->save($tramite);


                    $objTramiteVenda = new \G2\Entity\TramiteVenda();
                    $objTramiteVenda->setId_tramite($objTramite->getId_tramite());
                    $objTramiteVenda->setId_venda($id_venda);
                    $this->save($objTramiteVenda);

                }
            }

            //atualizando os lançamentos
            $valorLancamento = $params['modelo']['nu_valornegociacao'] / 6;

            /** @var \G2\Entity\LancamentoVenda[] $objetoLancamentos */
            $objetoLancamentos = $this->findBy('\G2\Entity\LancamentoVenda', array(
                'id_venda' => $id_venda
            ));

            if ($objetoLancamentos) {
                foreach ($objetoLancamentos as $i => $obj) {
                    /** @var \G2\Entity\Lancamento $objLanc */
                    $objLanc = $this->find('\G2\Entity\Lancamento', $obj->getIdLancamento());

                    // @history GII-9383
                    // Quando o valor for ZERO, gerar apenas uma parcela com 1 centavo e já PAGA
                    if ((int)$params['modelo']['nu_valornegociacao'] <= 0) {
                        // Manter só o primeiro lançamento ativo
                        if ($i) {
                            $objLanc->setBl_ativo(false);
                            $objLanc->setNu_valor(0);
                        } else {
                            $objLanc->setBl_quitado(true);
                            $objLanc->setDt_quitado(new \DateTime());
                            $objLanc->setNu_valor(0.01);
                        }
                    } else {
                        $objLanc->setNu_valor($valorLancamento);
                    }

                    $objLanc->setId_meiopagamento($this->getReference('\G2\Entity\MeioPagamento', $params['modelo']['id_meiopagamento']));
                    $this->save($objLanc);
                }
            }

            //atualizando a venda
            $objVenda = $this->find('\G2\Entity\Venda', $id_venda);
            if ($objVenda) {
                $objVenda->setNu_descontovalor($params['modelo']['nu_desconto']);
                $objVenda->setNu_valorbruto($params['modelo']['nu_valorcontrato']);
                $objVenda->setNu_valorliquido($params['modelo']['nu_valornegociacao']);
                $objVenda->setNu_creditos($params['modelo']['nu_creditos']);
                $this->save($objVenda);
            }

            //atualizando vendaproduto
            $objVendaProduto = $this->findOneBy('\G2\Entity\VendaProduto', array(
                'id_venda' => $id_venda
            ));

            if ($objVendaProduto) {
                $objVendaProduto->setNu_valorliquido($params['modelo']['nu_valornegociacao']);
                $objVendaProduto->setNu_valorbruto($params['modelo']['nu_valorcontrato']);
                $objVendaProduto->setNu_desconto($params['modelo']['nu_desconto']);
                $this->save($objVendaProduto);
            }

            $this->commit();

            /**
             * @history GII-8758
             */
            if ($bl_confirmarvenda) {
                /** @var \G2\Repository\Matricula $rp_temp */
                $rp_temp = $this->getRepository('\G2\Entity\VwMatricula');
                $matriculas = $rp_temp->retornaMatriculasRenovacaoFinalizar($id_venda);

                if ($matriculas) {
                    $ng_matricula = new \G2\Negocio\Matricula();
                    $ng_matricula->finalizaMatriculaRenovacao(array_shift($matriculas));
                }
            }

            return array('title' => 'Sucesso', 'text' => 'Grade salva com sucesso.', 'type' => 'success');
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao salvar a grade. ' . $e->getMessage());
        }
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Documentos (tb_documentos)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Documentos extends Negocio
{

    private $repositoryName = 'G2\Entity\Documentos';

    public function __construct()
    {
        parent::__construct();
    }

    public function model($row)
    {
        $final = array(
            'id_documentos' => $row->getId_documentos(),
            'bl_digitaliza' => $row->getBl_digitaliza(),
            'st_documentos' => $row->getSt_Documentos(),
            'id_tipodocumento' => array(
                'id_tipodocumento' => $row->getId_tipodocumento()->getId_tipodocumento()
            ),
            'nu_quantidade' => $row->getNu_quantidade(),
            'id_situacao' => array(
                'id_situacao' => $row->getId_situacao()->getId_situacao()
            ),
            'id_entidade' => array(
                'id_entidade' => $row->getId_entidade()->getId_entidade()
            ),
            'id_usuariocadastro' => $row->getId_usuariocadastro(),
            'id_documentosobrigatoriedade' => array(
                'id_documentosobrigatoriedade' => $row->getId_documentosobrigatoriedade()->getId_documentosobrigatoriedade()
            ),
            'id_documentoscategoria' => array(
                'id_documentoscategoria' => $row->getId_documentoscategoria()->getId_documentoscategoria()
            ),
            'projetos_pedagogicos' => array(),
            'documentos_utilizacao' => array(),
            'nu_ordenacao' => $row->getNu_ordenacao(),
        );

        // Busca ProjetoPedagogico associados (tb_projetopedagogico)
        $results = $this->findBy('G2\Entity\DocumentosProjetoPedagogico', array(
            'id_documentos' => $final['id_documentos']
        ));
        foreach ($results as $result) {
            $arr = $this->toArrayEntity($result);
            if (isset($arr['id_projetopedagogico']))
                $final['projetos_pedagogicos'][] = $arr['id_projetopedagogico'];
        }

        // Busca DocumentosUtilizacao associados (tb_documentosutilizacaorelacao)
        $results = $this->findBy('G2\Entity\DocumentosUtilizacaoRelacao', array(
            'id_documentos' => $final['id_documentos']
        ));
        foreach ($results as $result) {
            $arr = $this->toArrayEntity($result);
            if (isset($arr['id_documentosutilizacao']))
                $final['documentos_utilizacao'][] = $arr['id_documentosutilizacao'];
        }

        return $final;
    }

    public function findByEntidade($where = array())
    {
        if (!isset($where['id_entidade'])) {
            $where['id_entidade'] = $this->sessao->id_entidade;
        }

        // Remove o st documentos porque
        // se usa mais abaixo fazendo LIKE
        $st_documentos = null;
        if (isset($where['st_documentos'])) {
            $st_documentos = $where['st_documentos'];
            unset($where['st_documentos']);
        }

        // Criando a string do WHERE
        $conds = array();

        if (isset($where['bl_entidadepai'])) {
            $entidade = $this->find('G2\Entity\Entidade', $where['id_entidade']);

            if ($entidade->getId_entidadecadastro()) {
                $idEntidade    = $where['id_entidade'];
                $idEntidadePai = $entidade->getId_entidadecadastro()->getId_entidade();

                $conds[] = "(d.id_entidade IN ({$idEntidadePai}, {$idEntidade}))";

                unset($where['id_entidade']);
            }
            unset($where['bl_entidadepai']);
        }

        foreach ($where as $key => $cond) {
            $conds[] = "d.{$key} = '{$cond}'";
        }
        $conds = implode(" AND ", $conds);

        // Montando a query
        $query = "SELECT d FROM {$this->repositoryName} d WHERE " . $conds;
        // Adicionando o LIKE do st_documentos, caso ele exista
        if ($st_documentos) {
            $query .= " AND d.st_documentos LIKE '%{$st_documentos}%'";
        }

        $query .= " ORDER BY d.id_entidade ASC, d.nu_ordenacao ASC";

        // Executa a query criada
        $query = $this->em->createQuery($query);
        $result = $query->execute();

        // $result = $this->findBy($this->repositoryName, $where);

        $arrReturn = array();

        foreach ($result as $row) {
            $arrReturn[] = $this->model($row);
        }

        return $arrReturn;
    }

    public function findAll($repositoryName = null)
    {
        $result = $this->findBy($this->repositoryName, array('id_entidade' => $this->sessao->id_entidade));

        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->model($row);
            }

            return $arrReturn;
        }
    }

    public function save($data)
    {

        try {
            if (isset($data['id_documentos'])) {
                //se existir o id, busca o registro
                $entity = $this->find($this->repositoryName, $data['id_documentos']);
            } else {
                //se não existir o id cria um novo registro
                $entity = new \G2\Entity\Documentos();
            }

            /*********************
             * Definindo atributos
             *********************/

            if (isset($data['bl_digitaliza'])) {
                $entity->setBl_digitaliza($data['bl_digitaliza']);
            }

            if (isset($data['st_documentos'])) {
                $entity->setSt_documentos($data['st_documentos']);
            }

            // FK id_tipodocumento (tb_tipodocumento)
            if (isset($data['id_tipodocumento'])) {
                $entity->setId_tipodocumento($this->find('\G2\Entity\TipoDocumento', $data['id_tipodocumento']));
            }

            if (isset($data['bl_obrigatorio'])) {
                $entity->setBl_obrigatorio($data['bl_obrigatorio']);
            } else {
                $entity->setBl_obrigatorio(0);
            }

            if (isset($data['nu_quantidade'])) {
                $entity->setNu_quantidade($data['nu_quantidade']);
            }

            // FK id_situacao (tb_situacao)
            if (isset($data['id_situacao'])) {
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', $data['id_situacao']));
            }

            if (isset($data['id_entidade']['id_entidade'])) {

                $entity->setId_entidade($this->find('\G2\Entity\Entidade', $data['id_entidade']));
            } else {
                $entity->setId_entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));
            }

            if (isset($data['id_usuariocadastro'])) {
                $entity->setId_usuariocadastro($data['id_usuariocadastro']);
            } else {
                $entity->setId_usuariocadastro($this->sessao->id_usuario);
            }

            if (isset($data['bl_ativo'])) {
                $entity->setBl_ativo($data['bl_ativo']);
            } else {
                $entity->setBl_ativo(1);
            }

            // FK id_documentosobrigatoriedade (tb_documentosobrigatoriedade)
            if (isset($data['id_documentosobrigatoriedade'])) {
                $entity->setId_documentosobrigatoriedade($this->find('\G2\Entity\DocumentosObrigatoriedade', $data['id_documentosobrigatoriedade']));
            }

            // FK id_documentoscategoria (tb_documentoscategoria)
            if (isset($data['id_documentoscategoria'])) {
                $entity->setId_documentoscategoria($this->find('\G2\Entity\DocumentosCategoria', $data['id_documentoscategoria']));
            }

            if (isset($data['nu_ordenacao'])) {
                $entity->setNu_ordenacao($data['nu_ordenacao']);
            }

            if (isset($data['id_documentos'])) {
                $retorno = $this->merge($entity);
            } else {
                $retorno = $this->persist($entity);
            }

            $data['id_documentos'] = $retorno->getId_documentos();

            /**
             * DocumentosProjetoPedagogico
             */
            // Buscar DocumentosProjetoPedagogico e remover todos
            // para poder inserir somente os selecionados
            $ProjetosPedagogicos = $this->findBy('\G2\Entity\DocumentosProjetoPedagogico', array(
                'id_documentos' => $data['id_documentos']
            ));

            // Apagar cada registro encontrado
            foreach ($ProjetosPedagogicos as $ProjetoEntity) {
                parent::delete($ProjetoEntity);
            }

            // Adicionar os selecionados
            foreach ($data['projetos_pedagogicos'] as $key => $item) {
                $novo = new \G2\Entity\DocumentosProjetoPedagogico();
                $novo->setId_documentos($data['id_documentos']);
                $novo->setId_projetopedagogico($item);

                parent::save($novo);
            }

            /**
             * DocumentosUtilizacaoRelacao
             */
            // Buscar DocumentosUtilizacaoRelacao e remover todos
            // para poder inserir somente os selecionados
            $DocumentosUtilizacao = $this->findBy('\G2\Entity\DocumentosUtilizacaoRelacao', array(
                'id_documentos' => $data['id_documentos']
            ));

            // Apagar cada registro encontrado
            foreach ($DocumentosUtilizacao as $UtilizacaoEntity) {
                parent::delete($UtilizacaoEntity);
            }

            // Adicionar os selecionados
            foreach ($data['documentos_utilizacao'] as $key => $item) {
                $novo = new \G2\Entity\DocumentosUtilizacaoRelacao();
                $novo->setId_documentos($data['id_documentos']);
                $novo->setId_documentosutilizacao($item);

                parent::save($novo);
            }

            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro('O registro foi salvo', \Ead1_IMensageiro::SUCESSO, $retorno->getId_documentos());
        } catch (Exception $e) {
            return new \Ead1_Mensageiro('Erro ao o registro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }
}

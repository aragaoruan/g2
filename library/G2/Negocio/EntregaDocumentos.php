<?php

namespace G2\Negocio;
use Doctrine\Common\Util\Debug;

/**
 * Classe de negócio para Entrega de documentos
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 * @size 15/10/2014
 */
class EntregaDocumentos extends Negocio
{

    private $repositoryName = 'G2\Entity\EntregaDocumentos';

    public function __construct()
    {
        parent::__construct();
    }

    /**
    * Find VwEntregaDocumentos
    * @param array $where
    * @param array $order
    * @return mixed
    */
    public function buscarVwEntregaDocumentos(array $where, array $order = null) {

        if (isset($where['bl_entidadepai'])) {
            $entidade = $this->find('G2\Entity\Entidade', $where['id_entidade']);

            if ($entidade->getId_entidadecadastro()) {
                $idEntidade    = $where['id_entidade'];
                $idEntidadePai = $entidade->getId_entidadecadastro()->getId_entidade();
                $where['id_entidade'] = array($idEntidade, $idEntidadePai);
            }
            unset($where['bl_entidadepai']);
        }

        $result = $this->em->getRepository('G2\Entity\VwEntregaDocumento')
                           ->findVwEntregaDocumentos($where, $order);
        return $result;
    }

    /**
     * Find VwEntregaDocumentos
     * @param array $where
     * @param array $order
     * @return mixed
     */
    public function findVwEntregaDocumentos(array $where, array $order = null){
        $result = $this->findBy('G2\Entity\VwEntregaDocumento', $where, $order);
        return $result;
    }

    /**
     * Find TbEntregaDocumentos
     * @param array $where
     * @return mixed
     */
    public function findEntregaDocumentos(array $where){
        $result = $this->findBy('G2\Entity\EntregaDocumento', $where);
        return $result;
    }

    /**
     * Salva entrega de documentos
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function salvarEntregaDocumentos($data)
    {
        try {
            if (isset($data['id_entregadocumentos']) && ( (int)($data['id_entregadocumentos']))) {
                $entity = $this->find($this->repositoryName, $data['id_entregadocumentos']);
            } else {
                $entity = new \G2\Entity\EntregaDocumentos();
            }

            if (isset($data['id_documentos'])){
                $entity->setId_documentos($this->find('\G2\Entity\Documentos', $data['id_documentos']));
            }else{
                throw new \Zend_Exception('O id_documentos é obrigatório e não foi passado');
            }


            if (isset($data['id_usuarioaluno'])){
                $entity->setId_usuario($data['id_usuarioaluno']);
            }else{
                throw new \Zend_Exception('O id_usuarioaluno é obrigatório e não foi passado');
            }

            if (isset($data['id_entidade'])){
                $entity->setId_entidade($data['id_entidade']);
            }else{
                $entity->setId_entidade($this->sessao->id_entidade);
            }

            if (isset($data['id_situacao'])){
                $entity->setId_situacao($this->find('\G2\Entity\Situacao', $data['id_situacao']));
            }

            if (isset($data['id_contratoresponsavel']) && ( (int)($data['id_contratoresponsavel'] != 0 ) ) )
            {
                $entity->setId_contratoresponsavel($this->find('\G2\Entity\ContratoResponsavel', $data['id_contratoresponsavel']));
            }
            $entity->setId_usuariocadastro($this->sessao->id_usuario);
            $entity->setDt_cadastro(new \DateTime());


            $retorno = $this->save($entity);
            $data['id_entregadocumentos'] = $retorno->getId_entregadocumentos();


            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro($retorno, \Ead1_IMensageiro::SUCESSO, $retorno->getId_entregadocumentos());
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar entrega de documentos: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }


    /**
     * Salva Revisão - ABA
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function salvarRevisao($data)
    {
        try {
            if (!isset($data['id_matricula']) || !$data['id_matricula']) {
                throw new \Zend_Exception('O id_matricula é obrigatório e não foi passado');
            }

            /**
             * @var \G2\Entity\Matricula $entity
             */
            $entity = $this->find('G2\Entity\Matricula', $data['id_matricula']);

            if ($data['bl_documentacao'] === '') {
                $data['bl_documentacao'] = null;
            }

            $entity->setBl_documentacao($data['bl_documentacao']);
            $retorno = $this->save($entity);

            //salva trâmite
            $this->salvaTramiteDocumentacao($retorno);

            //verifica apto agendamento após alteração da documentação do aluno
            $negocioAvaliacao = new \G2\Negocio\Avaliacao();
            $negocioAvaliacao->verificaProvaFinal($entity);

            return new \Ead1_Mensageiro($retorno, \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar entrega de documentos: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }


    public function salvaTramiteDocumentacao($entity){
        try{
            $bo = new \TramiteBO();
            $matriculaTO = new \MatriculaTO();
            $matriculaTO->setId_matricula($entity->getId_matricula());
            $matriculaTO->fetch(true, true, true);

            if ($entity->getBl_documentacao()) {
                $st_tramite = 'OK';
            } else if ($entity->getBl_documentacao() === '' || $entity->getBl_documentacao() === null){
                $st_tramite = 'Não Analisado';
            } else {
                $st_tramite = 'Pendente';
            }


            $tramite = new \TramiteTO();
            $tramite->setId_tipotramite(\G2\Constante\TipoTramite::DOCUMENTACAO);
            $tramite->setId_entidade($this->sessao->id_entidade);
            $tramite->setBl_visivel(1);
            $tramite->setId_usuario($this->sessao->id_usuario);
            $tramite->setSt_tramite('Alterando marcação de documentação completa da matrícula para ' . $st_tramite);
            $tramite->setDt_cadastro(new \DateTime());


            $retornoTramite = $bo->cadastrarMatriculaTramite($matriculaTO, $tramite);
            return new \Ead1_Mensageiro('Tramites cadastrados com sucesso: ' , \Ead1_IMensageiro::SUCESSO);


        }catch (\Exception $e ){
            return new \Ead1_Mensageiro('Erro ao salvar entrega de documentos: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }


    public function salvarMarcacaoAcademico($data)
    {
        try {
            if (!isset($data['id_matricula']) || !$data['id_matricula']) {
                throw new \Zend_Exception('O id_matricula é obrigatório e não foi passado');
            }

            /**
             * @var \G2\Entity\Matricula $entity
             */
            $entity = $this->find('G2\Entity\Matricula', $data['id_matricula']);

            if ($data['bl_academico'] === '') {
                $data['bl_academico'] = null;
            }

            $entity->setBl_academico($data['bl_academico']);
            $retorno = $this->save($entity);

            //salva trâmite
            $this->salvaTramiteMarcacaoAcademica($retorno);


            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro($retorno, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao salvar entrega de documentos: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }

    public function salvaTramiteMarcacaoAcademica($entity){
        try{
            $bo = new \TramiteBO();
            $matriculaTO = new \MatriculaTO();
            $matriculaTO->setId_matricula($entity->getId_matricula());
            $matriculaTO->fetch(true, true, true);

            if ($entity->getBl_academico()) {
                $st_tramite = 'OK';
            } else if ($entity->getBl_academico() === '' || $entity->getBl_academico() === null){
                $st_tramite = 'Não Analisado';
            } else {
                $st_tramite = 'Pendente';
            }

            $tramite = new \TramiteTO();
            $tramite->setId_tipotramite(\G2\Constante\TipoTramite::DOCUMENTACAO);
            $tramite->setId_entidade($this->sessao->id_entidade);
            $tramite->setBl_visivel(1);
            $tramite->setId_usuario($this->sessao->id_usuario);
            $tramite->setSt_tramite('Alterando marcação acadêmica para certificação da matrícula para ' . $st_tramite);
            $tramite->setDt_cadastro(new \DateTime());

            $retornoTramite = $bo->cadastrarMatriculaTramite($matriculaTO, $tramite);
            return new \Ead1_Mensageiro('Tramites cadastrados com sucesso: ' , \Ead1_IMensageiro::SUCESSO);


        }catch (\Exception $e ){
            return new \Ead1_Mensageiro('Erro ao salvar entrega de documentos: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }
    }

    /**
     * @param int $id_matricula
     * @param string $st_observacao
     * @return \Ead1_Mensageiro
     */
    public function salvarObservacao($id_matricula, $st_observacao)
    {
        $this->beginTransaction();

        try {
            $en_matricula = $this->find('\G2\Entity\Matricula', $id_matricula);

            if ($en_matricula) {
                $en_tramite = new \G2\Entity\Tramite();
                $en_tramite->setId_usuario($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $en_tramite->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::DOCUMENTACAO));
                $en_tramite->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
                $en_tramite->setSt_tramite('Observação da Entrega Documentos: ' . $st_observacao);
                $en_tramite->setBl_visivel(1);
                $en_tramite->setDt_cadastro(new \DateTime());

                $this->save($en_tramite);

                $tramiteMatricula = new \G2\Entity\TramiteMatricula();
                $tramiteMatricula->setId_matricula($en_matricula);
                $tramiteMatricula->setId_tramite($en_tramite);
                $this->save($tramiteMatricula);

                $this->commit();

                return new \Ead1_Mensageiro("Trâmite com a observação salvo.", \Ead1_IMensageiro::SUCESSO);
            } else {
                throw new \Exception('Não há matrícula com id' . $id_matricula . 'cadastrada');
            }
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

}

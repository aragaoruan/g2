<?php

namespace G2\Negocio;

/**
 * Classe de negocio para VWAvisoBoleto
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwAvisoBoleto extends Negocio {

    /**
     *
     * @var G2\Entity\MensagemCobranca 
     */
    private $repositoryName = "G2\Entity\VwAvisoBoleto";

    public function findAll($repositoryName = '') {
        return parent::findAll($repositoryName ? $repositoryName : $this->repositoryName);
    }

}

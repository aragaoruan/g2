<?php

namespace G2\Negocio;


/**
 * Class CarteiraCliente
 * @package G2\Negocio
 */
class CarteiraCliente extends Negocio{

    protected $totalcarteiras = 0;
    protected $carteirasCB = 0;
    protected $carteirasComum = 0;
    protected $resultado_comum = 0;
    protected $resultado_CB;

    /**
     * __construct
     */
    public function __construct(){
        parent::__construct();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function executaRelatorio($params = array()){
        /**
         * Array para armazenamento dos dados do relatório, pois haverá várias consultas para retorno dos mesmos.
         * @var array $dadosRelatorio
         */
        $dadosRelatorio = array();

        /** verifica se a data de inicio foi informada */
        if(!empty($params["dt_inicio"])){
            /**
             * Criando a data de inicio com o parâmetro informado pelo usuário
             * @var \DateTime $dt_inicio
             */
            $dt_inicio = \DateTime::createFromFormat('d/m/Y', $params["dt_inicio"]);
        }
        /** verifica se a data final foi informada */
        if(!empty($params["dt_fim"])){
            /**
             * Criando a data final com o parâmetro informado pelo usuário
             * @var \DateTime $dt_fim
             */
            $dt_fim = \DateTime::createFromFormat('d/m/Y', $params["dt_fim"]);
        }
        /** se a data de inicio e fim não forem informadas pega a data atual mais os 12 meses anteriores para os parametros de
         * data de inicio e fim
         * dt_inicio = dt_inicio - 12 months
         * dt_fim = new DateTime() (data atual)
         **/
        if(empty($params["dt_inicio"]) && empty($params["dt_fim"])){
            /**
             * criando a data de inicio com base na data do sistema
             * @var \DateTime $dt_inicio
             */
            $dt_inicio = new \DateTime();
            /**
             * criando a data final com base na data do sistema
             * @var \DateTime() $dt_fim
             */
            $dt_fim = new \DateTime();
            /**
             * setando a data inicial para para 12 meses atrás
             * Ex.
             *      dt_final    2016-02-18
             *      dt_inicio   2015-02-18
             */
            $dt_inicio->setDate($dt_fim->format('Y'),$dt_fim->format('m')-11, 1);

            /** caso o parâmetro de inicio seja informado e o final não, acrescenta-se 12 meses na data final com base na data informada pelo usuário  */
        }elseif(!empty($params["dt_inicio"]) && empty($params["dt_fim"])){
            /**
             * Criando a data final  a partir da data do sistema
             * @var \DateTime() $dt_fim
             */
            $dt_fim = new \DateTime();
            /** setando a data final com base na data informada pelo usuário(data inicial) e adicionando 12 meses da mesma */
            $dt_fim->setDate($dt_inicio->format('Y'),$dt_inicio->format('m')+11, 1);

            /** se a data de inicio não for informada e a data final sim, diminui 12 meses na data informada pelo usuário */
        }elseif(empty($params["dt_inicio"]) && !empty($params["dt_fim"])){
            /**
             * Criando a data inicial  a partir da data do sistema
             * @var \DateTime $dt_inicio
             */
            $dt_inicio = new \DateTime();
            /** setando a data inicial com base na data informada pelo usuário( data final) e retirando 12 meses da mesma */
            $dt_inicio->setDate($dt_fim->format('Y'),$dt_fim->format('m')-11, 1);
        }

        /** pegando a diferença entre a data inicial e a data final */
        $diff = $dt_inicio->diff($dt_fim);
        /** inicializando a variável que serve de parâmetro para a busca dos dados para o relatório */
        $diffMess = 0 ;
        /** caso a diferença a diferença das datas se der em anos (12 meses) faremos a conversão do mesmo para meses */
        if($diff->y > 0){
            /** conversão da diferença em anos para meses */
            $diffMess = $diff->y * 12;
        }

        /** somando a difenreça calculada dos meses com o calculo feito anteriormente  */
        $diffMess += $diff->m;

        $dt_inicioM = new \DateTime();
        $dt_inicioM->setDate($dt_inicio->format('Y'),$dt_inicio->format('m'), 1);

        /** @var \G2\Repository\Venda $repository */
        $repository = $this->em->getRepository('G2\Entity\Venda');

        /** chamada para o método para a busca dos dados do relatório */
        $dados = $repository->executaRelatorioPreparaSe(array('dt_inicio' => $dt_inicioM->format('Y-m-d'), 'id_entidade' => $this->sessao->id_entidade), false);

        $this->tratarDadosRelatorio($dados);

        /** laço de repetição para a buscas dos dados para o relatório */
        for($i = 0; $i <= $diffMess; $i++){
            /**
             * Criação das datas para a busca do relatório durante o periodo mensal
             * @var \DateTime $dt_inicioM
             */

            $dt_inicioM->setDate($dt_inicio->format('Y'),$dt_inicio->format('m')+$i, 1);

            /**
             * Criação das datas para a busca do relatório durante o periodo mensal
             * @var \DateTime $dt_fimM
             */
            $dt_fimM = new \DateTime();
            $dt_fimM->setDate($dt_inicio->format('Y'),$dt_inicio->format('m')+$i+1, 1);

            /** em caso do mês ser o último setar o valor do dia baseado na data final do mês informado pelo o usuário somado de mais, por causa do horário. */
            if ($i == $diffMess){
                /** setando o dia para a data informada */
                $dt_fimM->setDate($dt_inicio->format('Y'), $dt_inicio->format('m')+$i, $dt_fim->format('d')+1);
            }

            /** @var \G2\Repository\Venda $repository */
            $repository = $this->em->getRepository('G2\Entity\Venda');

            /** chamada para o método para a busca dos dados do relatório */
            $dados = $repository->executaRelatorioPreparaSe(array('dt_inicio' => $dt_inicioM->format('Y-m-d'), 'dt_fim' => $dt_fimM->format('Y-m-d'), 'id_entidade' => $this->sessao->id_entidade));
            /** tratamento dos dados para o relatório conforme a especificação  http://confluence.unyleya.com.br/pages/viewpage.action?pageId=8356379 */
            $dadosRelatorio[$dt_inicioM->format('m').$dt_inicioM->format('Y')] = $this->tratarDadosRelatorio($dados);

        }

        return array('dados' => $dadosRelatorio, 'params' => array('dt_inicio' => $dt_inicio->format('d/m/Y'), 'dt_fim' => $dt_fim->format('d/m/Y')));
    }

    /**
     * @param $dados array
     * @return mixed
     * @Especificação http://confluence.unyleya.com.br/pages/viewpage.action?pageId=8356379
     */
    public function tratarDadosRelatorio($dados){

        foreach($dados as &$dado){
            /** calculo do total de confirmações, (cofirmação comum + confirmação CB ) */
            $dado['total_confirmacao'] = $dado['confirmacao_comum'] + $dado['confirmacao_cb'];
            /** calculo dos cancelamentos, (cancelamento comum + cancelamento CB) */
            $dado['total_cancelamento'] = $dado['cancelamento_comum'] + $dado['cancelamento_cb'];
            /** calculo do valortotal para novas confirmações */
            $dado['total_somatorioconfirmacao'] = number_format(($dado['valor_confirmacao_comum'] + $dado['valor_confirmacao_cb']), 2, ',', '.');
            /** aplicando mascara */
            $dado['somatoriaconf_cb'] = number_format($dado['valor_confirmacao_cb'] , 2, ',', '.');
            /** aplicando mascara */
            $dado['somatoriaconf_comum'] = number_format($dado['valor_confirmacao_comum'] , 2, ',', '.');

            $this->resultado_comum = $dado['resultadoconf_comum'] =  $dado['valor_confirmacao_comum'] - ($dado['cancelamento_comum'] * 29.9) + $this->resultado_comum;
            $this->resultado_CB = $dado['resultadoconf_cb'] = $dado['valor_confirmacao_cb'] - ($dado['cancelamento_cb'] * 19.9) + $this->resultado_CB;

            $dado['total_resultadoconfirmacao'] = number_format(($dado['resultadoconf_comum'] + $dado['resultadoconf_cb']), 2, ',', '.');
            $dado['resultadoconf_comum'] = number_format($dado['resultadoconf_comum'] , 2, ',', '.');
            $dado['resultadoconf_cb'] = number_format($dado['resultadoconf_cb'] , 2, ',', '.');

            $this->totalcarteiras = $dado['total_carteira'] = ($dado['total_confirmacao'] - $dado['total_cancelamento']) + $this->totalcarteiras;
            $this->carteirasComum = $dado['carteirasComum'] = ($dado['confirmacao_comum'] - $dado['cancelamento_comum']) + $this->carteirasComum;
            $this->carteirasCB = $dado['carteirasCB'] = ($dado['confirmacao_cb'] - $dado['cancelamento_cb']) + $this->carteirasCB;


        }

        return $dados[0];

    }
}
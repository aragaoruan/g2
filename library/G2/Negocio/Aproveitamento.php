<?php

namespace G2\Negocio;

use G2\Constante\Evolucao;
use G2\Constante\Situacao as Situa;
use G2\Constante\TipoTramite;
use G2\Entity\VwOcorrenciasPessoa;

/**
 * Classe de negócio para Aproveitamento
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class Aproveitamento extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna as disciplinas referentes aquela matrícula e seus modulos
     * @param $params
     * @return mixed
     */
    public function retornaModulosDisciplinasAproveitamento($params)
    {
        $response = array();

        $results = $this->getRepository('\G2\Entity\Aproveitamento')->findModulosDisciplinasAproveitamento($params);

        // Buscar as disciplinas originais utilizadas
        if ($results) {
            foreach ($results as $result) {
                $result['disciplinas_origem'] = array();

                /** @var \G2\Entity\DiscMatriculaDiscOrigem[] $disciplinas_origem */
                $disciplinas_origem = $this->findBy('\G2\Entity\DiscMatriculaDiscOrigem', array(
                    'id_matriculadisciplina' => $result['id_matriculadisciplina']
                ));

                if ($disciplinas_origem) {
                    foreach ($disciplinas_origem as $disciplina) {
                        $result['disciplinas_origem'][] = array(
                            'id_discmatriculadiscorigem' => $disciplina->getId_discmatriculadiscorigem(),
                            'id_disciplinaorigem' => $disciplina->getId_disciplinaorigem()->getId_disciplinaorigem(),
                            'st_disciplina' => $disciplina->getId_disciplinaorigem()->getSt_disciplina()
                        );
                    }
                }

                $response[] = $result;
            }
        }

        return $response;
    }

    /**
     * Salva o aproveitamento das disciplinas e suas ocorrencias relacionadas
     * @param $param
     * @return array
     * @throws \Zend_Exception
     */
    public function saveAproveitamento($param)
    {
        try {
            $this->beginTransaction();
            //Verifica se existe um aproveitamento, se não cria um e vincula a matrícula
            if (array_key_exists('id_aproveitamento', $param) && $param['id_aproveitamento']) {
                $resAproveitamento = $this->getReference('\G2\Entity\Aproveitamento', $param['id_aproveitamento']);
            } else {
                $objAproveitamento = new \G2\Entity\Aproveitamento();
                $objAproveitamento->setDt_cadastro(new \DateTime());
                $objAproveitamento->setBl_ativo(true);
                $objAproveitamento->setId_matricula($this->getReference('\G2\Entity\Matricula', $param['id_matricula']));
                $objAproveitamento->setId_usuario($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $resAproveitamento = $this->save($objAproveitamento);

                $objMatricula = $this->find('\G2\Entity\Matricula', $param['id_matricula']);
                $objMatricula->setId_aproveitamento($resAproveitamento);
                $this->save($objMatricula);
            }

            //se existir disciplinas alteradas, persiste os dados do aproveitamento.
            if (!empty($param['disciplinas'])) {
                $alocacao = new Alocacao();
                $tramite = new Tramite();

                //VARIAVEIS PARA ARMAZENAR OS TRAMITES
                $tramiteRemocao = [];
                $tramiteAlteracao = [];

                foreach ($param['disciplinas'] as $value) {
                    $where = array(
                        'id_matriculadisciplina' => $value['id_matriculadisciplina']
                    );

                    if (!empty($value['disciplinas_origem'])) {
                        // Sincronizar as disciplinas de origem salvas
                        $disciplinas_manter = array_filter(array_map(function ($item) {
                            return $item['id_discmatriculadiscorigem'];
                        }, $value['disciplinas_origem']));

                        if ($disciplinas_manter) {
                            $where['id_discmatriculadiscorigem'] = 'NOT IN (' . implode(',', $disciplinas_manter) . ')';
                        }
                    }

                    // Buscar as disciplinas para remover
                    $disciplinas_delete = $this->findCustom('\G2\Entity\DiscMatriculaDiscOrigem', $where);

                    // Remover as disciplinas encontradas
                    if ($disciplinas_delete) {
                        foreach ($disciplinas_delete as $disciplina) {
                            $this->delete($disciplina);
                        }
                    }

                    //VERIFICA SE O ELEMENTO ESTA SENDO EDITADO/INSERIDO OU SE ESTA SENDO REMOVIDO
                    if ($value['checked']) {
                        $objMatriculaDisciplina = $this->find('\G2\Entity\MatriculaDisciplina', $value['id_matriculadisciplina']);

                        // Remover alocação
                        $objAlocacao = $this->findOneBy('\G2\Entity\Alocacao', array('id_matriculadisciplina' => $value['id_matriculadisciplina']));
                        if ($objAlocacao) {
                            $alocacao->desalocarAluno(array('id_alocacao' => $objAlocacao->getId_alocacao()));
                        }

                        if (!$objMatriculaDisciplina->getId_aproveitamento()) {
                            $tramite->salvarTramiteMatricula(
                                $this->getReference('\G2\Entity\Disciplina', $objMatriculaDisciplina->getId_disciplina())->getSt_disciplina(),
                                $param['id_matricula'],
                                \G2\Constante\TipoTramite::EVOLUCAO_APROVEITAMENTO);
                        }

                        $objMatriculaDisciplina->setId_aproveitamento($resAproveitamento);

                        //VARIAVEL PARA ARMAZENAR AS ALTERACOES EXECUTADAS NAS PROXIMAS VERIFICACOES
                        $alteracoes = $this->verificaAlteracoes($value);

                        if ($alteracoes) {
                            $tramiteAlteracao[$value['st_disciplina']] = $alteracoes;
                        }

                        //$objMatriculaDisciplina->setSt_disciplinaoriginal($value['st_disciplinaoriginal']);
                        //$objMatriculaDisciplina->setNu_cargahorariaaproveitamento((integer)$value['nu_cargahorariaaproveitamento']);
                        //$objMatriculaDisciplina->setSt_instituicaoaproveitamento($value['st_instituicaoaproveitamento']);
                        //$objMatriculaDisciplina->setSg_uf($this->getReference('\G2\Entity\Uf', $value['sg_uf']));

                        // Salvar cada disciplina de aproveitamento
                        if (!empty($value['disciplinas_origem'])) {
                            foreach ($value['disciplinas_origem'] as $disc_origem) {
                                $en_discmatdiscorigem = $this->preencherEntity('\G2\Entity\DiscMatriculaDiscOrigem', 'id_discmatriculadiscorigem', $disc_origem, array(
                                    'dt_cadastro' => date('Y-m-d H:i:s'),
                                    'id_entidadecadastro' => $this->sessao->id_entidade,
                                    'id_matriculadisciplina' => $value['id_matriculadisciplina'],
                                    'id_usuariocadastro' => $this->sessao->id_usuario
                                ));

                                $this->save($en_discmatdiscorigem);
                            }
                        }

                        $objMatriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO);
                        $objMatriculaDisciplina->setId_situacao(\G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA);
                    } else {
                        $tramiteRemocao[] = '- ' . $value['st_disciplina'];

                        $objMatriculaDisciplina = $this->find('\G2\Entity\MatriculaDisciplina', $value['id_matriculadisciplina']);
                        $objMatriculaDisciplina->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO);
                        $objMatriculaDisciplina->setId_situacao(\G2\Constante\Situacao::TB_MATRICULADISCIPLINA_ATIVA);
                        $objMatriculaDisciplina->setId_aproveitamento(null);

                        //$objMatriculaDisciplina->setSt_disciplinaoriginal(null);
                        //$objMatriculaDisciplina->setNu_cargahorariaaproveitamento(null);
                        //$objMatriculaDisciplina->setSt_instituicaoaproveitamento(null);
                        //$objMatriculaDisciplina->setSg_uf(null);

                        // Remover cada disciplina de aproveitamento
                        if (!empty($value['disciplinas_origem'])) {
                            foreach ($value['disciplinas_origem'] as $disc_origem) {
                                if (!empty($disc_origem['id_discmatriculadiscorigem'])) {
                                    $en_discmatdiscorigem = $this->find('\G2\Entity\DiscMatriculaDiscOrigem', $disc_origem['id_discmatriculadiscorigem']);
                                    $this->delete($en_discmatdiscorigem);
                                }
                            }
                        }
                    }

                    $this->save($objMatriculaDisciplina);
                }

                //GERA OS TRAMITES DE EDICAO E DELECAO.

                if ($tramiteAlteracao) {
                    foreach ($tramiteAlteracao as $key => $ta) {
                        $texto = 'A disciplina ' . $key . ' teve a(s) seguinte(s) altereção(ões): <br>' . implode('<br>', $ta);
                        $tramite->salvarTramiteMatricula(
                            $texto,
                            $param['id_matricula'], \G2\Constante\TipoTramite::EVOLUCAO_APROVEITAMENTO);
                    }
                }

                if ($tramiteRemocao) {
                    $texto = 'As disciplinas abaixo tiveram a insenção removida: <br>' . implode('<br>', $tramiteRemocao);
                    $tramite->salvarTramiteMatricula(
                        $texto,
                        $param['id_matricula'], \G2\Constante\TipoTramite::EVOLUCAO_APROVEITAMENTO);
                }

            };

            //se existir ocorrencias
            if (array_key_exists('ocorrencias', $param) && $param['ocorrencias']) {

                //Seta o bl_ativo como zero em todas as ocorrencias de aproveitamento relacionadas
                $setZero = $this->findBy('\G2\Entity\OcorrenciaAproveitamento', array('id_aproveitamento' => $resAproveitamento->getId_aproveitamento()));
                foreach ($setZero as $value) {
                    $value->setBl_ativo(0);
                    $this->save($value);
                }

                //Seta o bl_ativo true quando ja existe a ocorrencia deste aproveitamento já relacionada, se não cria o relacionamento
                foreach ($param['ocorrencias'] as $value) {
                    $objOcorrencia = $this->findOneBy('\G2\Entity\OcorrenciaAproveitamento',
                        array('id_ocorrencia' => $value['id_ocorrencia'], 'id_aproveitamento' => $resAproveitamento->getId_aproveitamento()));
                    if (!$objOcorrencia) {
                        $objOcorrencia = new \G2\Entity\OcorrenciaAproveitamento();
                        $objOcorrencia->setBl_ativo(true);
                        $objOcorrencia->setId_ocorrencia($this->getReference('\G2\Entity\Ocorrencia', $value['id_ocorrencia']));
                        $objOcorrencia->setId_aproveitamento($resAproveitamento);
                        $objOcorrencia->setDt_cadastro(new \DateTime());
                        $objOcorrencia->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                        $this->save($objOcorrencia);
                    } else {
                        $objOcorrencia->setBl_ativo(true);
                        $this->save($objOcorrencia);
                    }
                }
            } else {
                //Se não existe nenhuma ocorrência passada, seta todas as que são relacionadas como bl_ativo false.
                $setZero = $this->findBy('\G2\Entity\OcorrenciaAproveitamento', array('id_aproveitamento' => $resAproveitamento->getId_aproveitamento()));
                foreach ($setZero as $value) {
                    $value->setBl_ativo(0);
                    $this->save($value);
                }
            }
            $this->commit();
            return array('type' => 'success', 'mensagem' => 'Dados salvos com sucesso!', 'title' => 'Sucesso');
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar dados de aproveitamento. " . $e->getMessage());
        }

    }

    //VERIFICA AS ALTERACOES DOS LANCAMENTOS DE APROVEITAMENTO
    public function verificaAlteracoes($value)
    {
        $alteracoes = ['Disciplinas de origem:'];

        if (empty($value['disciplinas_origem'])) {
            return '';
        }

        foreach ($value['disciplinas_origem'] as $disciplina) {
            $alteracoes[] = '- ' . $disciplina['st_disciplina'];
        }

        return $alteracoes;
    }

    /**
     * Busca a venda, verifica se tem o id_ocorrenciaaproveitamento, verifica se a ocorrência está fechada, se sim,
     * reabre a ocorrência devolvendo para distribuição.
     * @return array
     * @throws Exception
     * @author helder.silva <helder.silva@unyleya.com.br>
     */
    public function reabrirOcorrenciaParaAproveitamento($venda)
    {
        try {
            $objVenda = $this->find('\G2\Entity\Venda', $venda->getId_venda());

            if ($objVenda->getId_ocorrenciaaproveitamento()) {
                $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $objVenda->getId_ocorrenciaaproveitamento());

                if (!$ocorrencia->getId_matricula()) {
                    $matricula = $this->findOneBy('\G2\Entity\VendaProduto', array('id_venda' => $objVenda->getId_venda()));
                    $ocorrencia->setId_matricula($matricula->getId_matricula());
                    $this->save($ocorrencia);
                }

                if ($ocorrencia && $ocorrencia->getId_situacao()->getId_situacao() == \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA) {
                    $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
                    $tramite = 'Essa ocorrência foi reaberta para dar continuidade ao processo de aproveitamento de disciplina. Caso o aproveitamento esteja aprovado pelo coordenador,será necessário lançar o aproveitamento das disciplinas através da funcionalidade "Lançar Aproveitamento".';

                    return $negocioOcorrencia->devolverOcorrencia(array(
                        'id_ocorrencia' => $ocorrencia->getId_ocorrencia(),
                        'id_evolucao' => $ocorrencia->getId_evolucao()->getId_evolucao(),
                        'id_situacao' => $ocorrencia->getId_situacao()->getId_situacao()
                    ), $tramite);
                }
            }

            return false;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao reabrir ocorrência. " . $e->getMessage());
        }

    }

}

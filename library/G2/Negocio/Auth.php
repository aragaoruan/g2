<?php

namespace G2\Negocio;

/**
 * Classe para autenticações
 * @author Felipe Pastor
 */
class Auth extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Método que monta o select de login de usuarios no portal
     * @param $to
     * @return \G2\Entity\VwValidaLoginPortal
     */
    public function retornaSelectLoginUsuarioPortal($to)
    {
        $arrayWhere = array(
            'st_email' => $to->getSt_email(),
            'st_senhaentidade' => $to->getSt_senhaentidade()
        );

        if ($to->getBl_acessoapp()) {
            $arrayWhere['bl_acessoapp'] = $to->getBl_acessoapp();
        }

        $retorno = $this->findOneBy('\G2\Entity\VwValidaLoginPortal', $arrayWhere);

        if (empty($retorno)) {
            $arrayWhere = array(
                'st_login' => $to->getSt_login(),
                'st_senhaentidade' => $to->getSt_senhaentidade()
            );

            $retorno = $this->findOneBy('\G2\Entity\VwValidaLoginPortal', $arrayWhere);
        }

        if (empty($retorno)) {
            $arrayWhere = array(
                'st_login' => $to->getSt_login(),
                'st_senha' => $to->getSt_senha()
            );

            $retorno = $this->findOneBy('\G2\Entity\VwValidaLoginPortal', $arrayWhere);
        }

        if (empty($retorno)) {
            $arrayWhere = array('st_email' => $to->getSt_email(), 'st_senha' => $to->getSt_senha());

            $retorno = $this->findOneBy('\G2\Entity\VwValidaLoginPortal', $arrayWhere);
        }

        return $retorno;
    }

    /**
     * Efetua o login do usuário criando o token de acesso
     * @param $auth
     * @param $id_entidade
     * @param null $id_usuariooriginal - Usuário que está logando como aluno, normalmente o ID do Professor
     * @return array|bool
     * @throws \Exception
     */
    public function login($auth, $id_entidade, $usuarioOriginal = null){
        try {

            if ($auth && $auth instanceof \G2\Entity\VwValidaLoginPortal) {

                $user = New \G2\Utils\User();
                $user->setlogin($auth->getSt_login())
                    ->setuser_id($auth->getId_usuario())
                    ->setuser_email($auth->getSt_email())
                    ->setuser_cpf($auth->getSt_cpf())
                    ->setuser_avatar($auth->getSt_urlavatar() != '' ? \Ead1_Ambiente::geral()->st_url_gestor2 . $auth->getSt_urlavatar() : '')
                    ->setuser_name($auth->getSt_nomecompleto())
                    ->setentidade_id($id_entidade)
                    ->setid_usuariooriginal(null)
                    ->setst_usuariooriginal(null)
                    ->setbl_acessarcomo(false);

                if($usuarioOriginal && $usuarioOriginal instanceof \G2\Entity\Usuario && $usuarioOriginal->getSt_nomecompleto()){
                    $user->setid_usuariooriginal($usuarioOriginal->getId_usuario())
                        ->setst_usuariooriginal($usuarioOriginal->getSt_nomecompleto())
                        ->setbl_acessarcomo(true);
                }

                $authToken = new \G2\Entity\AuthToken();
                $this->token = \JWT::encode($user->toArray(), $authToken->getSecretToken());

            } else {
                throw new \Exception('Atenção! Sua credencial não está correta. Verifique se seu usuário e senha estão corretos.');
            }

            return [
                'retorno' => true,
                'messagem' => 'Sucesso! Usuário logado com sucesso.',
                'token' => $this->token,
                'message' => 'Sucesso! Usuário logado com sucesso.',
                'type' => 'success'
            ];


        } catch(\Exception $e){
            throw $e;
            return false;
        }

    }
}

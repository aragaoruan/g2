<?php
/**
 * Created by PhpStorm.
 * User: Neemias
 * Date: 25-03-2015
 * Time: 15:15
 */

namespace G2\Negocio;


use Doctrine\ORM\ORMException;

class VwFormaMeioPagamento extends Negocio
{
    private $repositoryName;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = 'G2\Entity\VwFormaMeioPagamento';
    }

    /**
     * Retornar formas meio de pagamento
     *
     * @param array $where
     * @param array $order
     * @param bool $retornArray
     * @return array|Object
     * @throws \Zend_Exception
     */
    public function findByFormaMeioPagamento(array $where = array(), array $order = array(), $retornArray = false)
    {
        try {
            $return = $this->findBy($this->repositoryName, $where, $order);

            if ($retornArray) {
                $arrReturn = array();
                foreach ($return as $row) {
                    $arrReturn[] = $this->toArrayEntity($row);
                }
                return $arrReturn;
            }

            return $return;
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }
}
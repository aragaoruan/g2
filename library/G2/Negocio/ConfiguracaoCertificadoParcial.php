<?php
/**
 * Created by PhpStorm.
 * User: Alex Alexandre <alex.alexandre@unyleya.com.br>
 * Date: 03/04/2018
 * Time: 15:54
 */

namespace G2\Negocio;

use G2\Constante\CategoriaTextoSistema;
use G2\Constante\TextoCategoria;
use G2\Constante\TextoSistemaFixo;
use G2\Utils\Helper;

class ConfiguracaoCertificadoParcial extends Negocio
{
    private $repositoryCertificadoParcial;
    private $repositoryCertificadoDisciplina;
    private $repositoryTextoSistema;


    public function __construct()
    {
        parent::__construct();
        $this->repositoryCertificadoParcial = '\G2\Entity\CertificadoParcial';
        $this->repositoryCertificadoDisciplina = '\G2\Entity\CertificadoParcialDisciplina';
        $this->repositoryTextoSistema = 'G2\Entity\TextoSistema';
    }

    /**
     * @param $param
     * @return array
     * @throws \Zend_Exception
     */
    public function certificadoParcialIndex($param)
    {
        try {
            $where = array(
                "id_certificadoparcial" => $param == null ? '' : $param
            );

            $certificadoParcial = $this->find($this->repositoryCertificadoParcial, $where);
            $disciplinas = $this->certificadoParcialDisciplinas($where);
            $data = array(
                'id_certificadoparcial' => $param == null ? '' : $certificadoParcial->getId_certificadoparcial(),
                'st_certificadoparcial' => $param == null ? '' : $certificadoParcial->getSt_certificadoparcial(),
                'dt_iniciovigencia' => $param == null ? ''
                    : Helper::converterData(
                        $certificadoParcial->getDt_iniciovigencia(), 'd/m/Y'
                    ),
                'dt_fimvigencia' => $param == null ? ''
                    : Helper::converterData(
                        $certificadoParcial->getDt_fimvigencia(), 'd/m/Y'
                    ),
                'dt_cadastro' => $param == null ? '' : Helper::converterData($certificadoParcial->getDt_cadastro()),
                'id_textosistema' => $param == null ? '' : $certificadoParcial->getId_textosistema(),
                'id_usuariocadastro' => $param == null ? '' : $certificadoParcial->getId_usuariocadastro(),
                'bl_gerado' => $param == null ? '' : (boolean)$certificadoParcial->getBl_gerado(),
                'disciplinas' => $param == null ? '' : $disciplinas
            );

            return $data;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $where
     * @return array
     * @throws \Zend_Exception
     */
    public function certificadoParcialDisciplinas($where)
    {
        try {
            $disciplinas = array();
            $certificadoParcialDisciplinas = $this->findBy($this->repositoryCertificadoDisciplina, $where);

            foreach ($certificadoParcialDisciplinas as $certificadoParcialDisciplina) {
                $disciplina = array(
                    'id_disciplina' => $certificadoParcialDisciplina->getId_disciplina()->getId_disciplina(),
                    'st_disciplina' => $certificadoParcialDisciplina->getId_disciplina()->getSt_disciplina()
                );
                array_push($disciplinas, $disciplina);
            }
            return $disciplinas;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function salvarCertificadoParcial($params)
    {

        $mensageiro = new \Ead1_Mensageiro();
        try {
            $this->beginTransaction();

            $certificadoParcial = new \G2\Entity\CertificadoParcial();
            $disciplinaEntity = new \G2\Entity\Disciplina();
            $menssagem = "adicionado";
            if (array_key_exists("id_certificadoparcial", $params)) {
                if (!empty($params['id_certificadoparcial'])) {
                    $certificadoParcial = $this->find($certificadoParcial, $params['id_certificadoparcial']);
                    $certificadoParcialDisciplinaExcluir = new \G2\Entity\CertificadoParcialDisciplina();
                    $where = array("id_certificadoparcial" => $params['id_certificadoparcial']);
                    $certificadoParcialDisciplinaExcluir = $this->findBy($certificadoParcialDisciplinaExcluir, $where);
                    if ($certificadoParcialDisciplinaExcluir) {
                        foreach ($certificadoParcialDisciplinaExcluir as $deletar) {
                            $this->delete($deletar);
                        }
                    }
                }
                $menssagem = "atualizado";
            }

            $certificadoParcial
                ->setSt_certificadoparcial($params['st_certificadoparcial'])
                ->setDt_iniciovigencia(Helper::converterData($params['dt_iniciovigencia']))
                ->setDt_fimvigencia(
                    $params['dt_fimvigencia'] == null ?
                        null : Helper::converterData($params['dt_fimvigencia'])
                )
                ->setId_textosistema($params['id_textoSistema'])
                ->setDt_cadastro(Helper::converterData(new \DateTime()))
                ->setId_usuariocadastro($this->sessao->id_usuario)
                ->setBl_gerado(false)
                ->setId_EntidadeCadastro($this->sessao->id_entidade);

            $id_certificacaoParcial = $this->save($certificadoParcial);

            foreach ($params['disciplinas'] as $disciplina) {
                $certificadoParcialDisciplina = new \G2\Entity\CertificadoParcialDisciplina();
                $certificadoParcialDisciplina
                    ->setId_certificadoparcial($this->getReference($certificadoParcial,
                        $id_certificacaoParcial->getId_certificadoparcial()))
                    ->setId_disciplina($this->getReference($disciplinaEntity, $disciplina['id_disciplina']));
                $this->save($certificadoParcialDisciplina);
            }

            $this->commit();
            return $mensageiro->setMensageiro("Certificado parcial " . $menssagem, \Ead1_IMensageiro::SUCESSO);

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornaCertificadoParcial(array $params)
    {
        try {

            $customWhere = array(
                "st_certificadoparcial" => " LIKE '%" . trim($params['txtSearch']) . "%'",
                "id_entidadecadastro" => $this->sessao->id_entidade
            );

            $repositoryCertificadoParciais = $this->findCustom($this->repositoryCertificadoParcial, $customWhere);

            if ($repositoryCertificadoParciais) {

                foreach ($repositoryCertificadoParciais as $indice => $repositoryCertificadoParcial) {

                    $repositoryCertificadoParciais[$indice]->setDt_iniciovigencia(
                        Helper::converterData($repositoryCertificadoParcial->getDt_iniciovigencia(), 'd/m/Y')
                    );
                    $repositoryCertificadoParciais[$indice]->setDt_fimvigencia(
                        Helper::converterData($repositoryCertificadoParcial->getDt_fimvigencia(), 'd/m/Y')
                    );
                }

                return $this->toArrayEntity($repositoryCertificadoParciais);
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Find By Entidade
     * @param integer $idTextoExibicao
     * @return object G2\Entity\TextoSistema
     */
    public function retornaTextoSistema()
    {

        return $this->em->createQueryBuilder()
            ->select('ts.id_textosistema, ts.st_textosistema, en.id_entidade')
            ->from('\G2\Entity\EntidadeRelacao', 'en')
            ->join('\G2\Entity\TextoSistema', 'ts',
                'WITH', 'ts.id_entidade = en.id_entidade  OR ts.id_entidade = en.id_entidadepai'
            )
            ->where('en.id_entidade = :id_entidade')
            ->andWhere('ts.id_textocategoria = :id_textocategoria')
            ->setParameters(
                array(
                    'id_entidade' => $this->sessao->id_entidade,
                    'id_textocategoria' => CategoriaTextoSistema::CERTIFICADO
                )
            )
            ->getQuery()->getResult();

    }

    public function retornaDisciplina()
    {
        return $this->em->createQueryBuilder()
            ->select('  di.id_disciplina, di.st_disciplina, en.id_entidade')
            ->from('\G2\Entity\EntidadeRelacao', 'en')
            ->join(
                '\G2\Entity\Disciplina', 'di',
                'WITH', 'di.id_entidade = en.id_entidade
                             OR
                             di.id_entidade = en.id_entidadepai AND en.id_entidadepai <> 2'
            )
            ->where('en.id_entidade = :id_entidade')
            ->andWhere('di.bl_ativa = 1')
            ->andWhere('di.id_tipodisciplina = 1')
            ->setParameters(
                array(
                    'id_entidade' => $this->sessao->id_entidade
                )
            )
            ->getQuery()->getResult();
    }

}

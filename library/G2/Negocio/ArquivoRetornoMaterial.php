<?php
/**
 * Created by DeniseXavier.
 * Date: 27/03/15
 * Time: 08:27
 */

namespace G2\Negocio;
use G2\Constante\Situacao;
use G2\Entity\ArquivoRetornoPacote;
use G2\Entity\EntregaMaterial;
use G2\Entity\ItemDeMaterial;
use G2\Entity\Pacote;
use G2\Entity\Upload;

/**
 * Classe de negócio para Arquivo Retorno de Material
 * @package G2\Negocio
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class ArquivoRetornoMaterial extends  Negocio {


    private $repository = 'G2\Entity\AssuntoCo';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Pesquisa arquivos de retorno
     * @param array $params
     * @return bool
     * @throws \Zend_Exception
     */
    public function findByArquivoRetornoMaterial($params = array())
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\ArquivoRetornoMaterial');
            $query = $repo->createQueryBuilder("arm")
                ->where('arm.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true)
                ->andWhere('arm.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);
            if (array_key_exists('id_situacao', $params) && $params['id_situacao']) {
                $query->andWhere('arm.id_situacao = :id_situacao')
                    ->setParameter('id_situacao', $params['id_situacao']);
            }
            if ( (array_key_exists('dt_cadastroinicio', $params) && $params['dt_cadastroinicio'])
                &&
                (array_key_exists('dt_cadastrofim', $params) && $params['dt_cadastrofim'] )) {
                $dataEntrada = new \Zend_Date( new \Zend_Date($params['dt_cadastroinicio']), \Zend_Date::ISO_8601 );
                $dataTermino = new \Zend_Date( new \Zend_Date($params['dt_cadastrofim']." 23:59:59"), \Zend_Date::ISO_8601 );

                $where = "arm.dt_cadastro BETWEEN '{$dataEntrada->toString( 'yyyy-MM-dd HH:mm:ss' )}' AND '{$dataTermino->toString( 'yyyy-MM-dd HH:mm:ss' )}'";
                    $query->andWhere($where);
            }else{
                if(array_key_exists('dt_cadastroinicio', $params) && $params['dt_cadastroinicio']){
                    $dataEntrada = new \Zend_Date( new \Zend_Date($params['dt_cadastroinicio']." 00:00:00"), \Zend_Date::ISO_8601 );
                    $query->andWhere("arm.dt_cadastro > '{$dataEntrada->toString( 'yyyy-MM-dd HH:mm:ss' )}'");
                }elseif(array_key_exists('dt_cadastrofim', $params) && $params['dt_cadastrofim'] ){
                    $dataTermino = new \Zend_Date( new \Zend_Date($params['dt_cadastrofim']." 23:59:59"), \Zend_Date::ISO_8601 );
                    $query->andWhere("arm.dt_cadastro < '{$dataTermino->toString( 'yyyy-MM-dd HH:mm:ss' )}'");
                }
            }

            if(array_key_exists('st_arquivoretornomaterial', $params) && ($params['st_arquivoretornomaterial'] != '')){
                $query->andWhere("arm.st_arquivoretornomaterial like '%{$params['st_arquivoretornomaterial']}%'");
            }

            //\Zend_Debug::dump($query->getQuery());die;
            $result = $query->getQuery()->getResult();
            if ($result) {
                $arrReturn = array();
                foreach ($result as $key => $row) {
                    $arrReturn[$key]['id_arquivoretornomaterial'] = $row->getId_arquivoretornomaterial();
                    $arrReturn[$key]['id_entidade'] = $row->getId_entidade()->getId_entidade();
                    $arrReturn[$key]['st_arquivoretornomaterial'] = $row->getSt_arquivoretornomaterial();
                    $arrReturn[$key]['dt_cadastro'] = $row->getDt_cadastro() instanceof \DateTime ? $row->getDt_cadastro()->format("d/m/Y H:m:s") : $row->getDt_cadastro()->toString("dd/MM/Y HH:mm:ss");
                    $arrReturn[$key]['id_situacao'] = $row->getId_situacao()->getId_situacao();
                    $arrReturn[$key]['st_situacao'] = $row->getId_situacao()->getSt_situacao();
                    $arrReturn[$key]['bl_ativo'] = $row->getBl_ativo();
                    $arrReturn[$key]['id_upload'] = $row->getId_upload()->getId_upload();
                    $arrReturn[$key]['st_upload'] = $row->getId_upload()->getSt_upload();
                    $arrReturn[$key]['st_link'] = $_SERVER['HTTP_HOST'].DIRECTORY_SEPARATOR.'upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR.$row->getId_upload()->getSt_upload();
                    $arrReturn[$key]['nu_itens'] = count($this->findBy('\G2\Entity\ArquivoRetornoPacote', array('id_arquivoretornomaterial' => $row->getId_arquivoretornomaterial())));
                }
                return $arrReturn;
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Exception($e->getMessage());
            return false;
        }
    }



    public function findByArquivoRetornoPacote($params = array())
    {
        try {
            if (!isset($params['id_arquivoretornomaterial'])) {
                throw new \Exception('É obrigatório o id do arquivo retorno de material para continuar a pesquisa!');
            }
            $result = $this->findBy('G2\Entity\ArquivoRetornoPacote', array('id_arquivoretornomaterial' => $params['id_arquivoretornomaterial']));

            if ($result) {
                $arrReturn = array();
                foreach ($result as $key => $row) {
                    $arrReturn[$key]['id_pacote'] = ($row->getId_pacote() instanceof Pacote ? $row->getId_pacote()->getId_pacote() : null );
                    $arrReturn[$key]['id_itemdematerial'] = ($row->getId_itemdematerial() instanceof ItemDeMaterial ? $row->getId_itemdematerial()->getId_itemdematerial() : null );
                    $arrReturn[$key]['st_codrastreamento'] = $row->getSt_codrastreamento();
                    $arrReturn[$key]['st_resultado'] = $row->getSt_resultado();
                }
                return $arrReturn;
            } else {
                return false;
            }
        } catch (\Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            THROW new \Zend_Exception($e->getMessage());
            return false;
        }
    }

    /**
     * Download do arquivo de retorno e letura do mesmo
     * @param $arquivo
     * @return \Ead1_Mensageiro
     */
    public function uploadArquivo($arquivo){
        $this->em->beginTransaction();
        try {

            $bo = new \EntidadeBO();
            $fileName = $arquivo['name'];
            $nomeLimpo = $bo->substituirCaracteresEspeciais($arquivo['name']);
            $arquivo['name'] = str_replace(' ', '_', rtrim($nomeLimpo));

               $adapter = new \Zend_File_Transfer_Adapter_Http();
               $adapter->addFilter('Rename', $arquivo['name']);
               if (!is_dir('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR)) {
                   if (mkdir('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR, 0777, true) === false) {
                      throw new \Exception ('Não foi possível criar o diretório informado.');
                   }
               }
               chmod ('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR, 0777);
               $adapter->setDestination('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR);
               if (!$adapter->receive()) {
                   $messages = $adapter->getMessages();
                   echo implode("\n", $messages);
               }
                $upload = new \Zend_File_Transfer();
                $upload->receive();

            $upload = new Upload();
            $upload->setSt_upload($arquivo['name']);
            $this->save($upload);


            if (!$upload->getId_upload()) {
                throw new \Zend_Exception("Erro ao fazer upload do arquivo de retorno do material! ");
            } else {

                $arquivoRetorno = new \G2\Entity\ArquivoRetornoMaterial();
                $arquivoRetorno->setId_entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));
                $arquivoRetorno->setId_situacao($this->find('\G2\Entity\Situacao', 161));
                $arquivoRetorno->setBl_ativo(1);
                $arquivoRetorno->setDt_cadastro(new \DateTime());
                $arquivoRetorno->setId_upload($upload);
                $arquivoRetorno->setId_usuariocadastro( $this->find('\G2\Entity\Usuario',$this->sessao->id_usuario));
                $arquivoRetorno->setSt_arquivoretornomaterial($fileName);
                $this->save($arquivoRetorno);

                if($arquivoRetorno->getId_arquivoretornomaterial()){
                    $retorno =   $this->readFileReturn($arquivoRetorno, $upload);
                }else{
                    $retorno =  new \Ead1_Mensageiro('Erro ao salvar o arquivo retorno. Tente fazer o upload novamente');
                }
                if($retorno->getTipo() == \Ead1_IMensageiro::SUCESSO){
                    $this->em->commit();
                }
                return $retorno;
            }


        } catch (\Exception $e) {
            $this->em->rollBack();
            return new \Ead1_Mensageiro('Erro ao salvar o arquivo retorno: '.$e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Lê o arquivo de retorno e atualiza situações dos itens e pacotes
     * @param $arquivoRetorno
     * @param Upload $upload
     * @return \Ead1_Mensageiro
     */
    public function readFileReturn($arquivoRetorno, Upload $upload){
        try{
            $excelReader = \PHPExcel_IOFactory::createReaderForFile('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR. $upload->getSt_upload() );
            $excelObj = $excelReader->load('upload' . DIRECTORY_SEPARATOR . 'arquivo-retorno-material' . DIRECTORY_SEPARATOR. $upload->getSt_upload() );
            $array_dados = $excelObj->getActiveSheet()->toArray(null, true,false,true);

            if(count($array_dados)){
                if(\PHPExcel_Cell::columnIndexFromString( $excelObj->getActiveSheet()->getHighestColumn() ) !==  5){
                    throw new \Exception('Formato do arquivo inválido. Verifique o modelo e importe novamente.');
                }
                for($x=2; $x <= count($array_dados); $x++){
                    $dataPostagem = \PHPExcel_Style_NumberFormat::toFormattedString($array_dados[$x]['D'], 'YYYY-MM-DD hh:mm:ss');
                    $situacao = $array_dados[$x]['E'] == 1 ? 164 : 163 ;
                    $arquivoPacote = new ArquivoRetornoPacote();
                    $arquivoPacote->setDt_postagem(new \DateTime($dataPostagem));
                    $arquivoPacote->setId_arquivoretornomaterial($arquivoRetorno);
                    $arquivoPacote->setDt_devolucao(new \DateTime());
                    $arquivoPacote->setSt_codrastreamento($array_dados[$x]['C']);
                    $arquivoPacote->setId_itemdematerial($this->find('\G2\Entity\ItemDeMaterial', $array_dados[$x]['B']));
                    $arquivoPacote->setId_pacote($this->find('\G2\Entity\Pacote', $array_dados[$x]['A']));
                    $arquivoPacote->setId_situacaopostagem($this->find('\G2\Entity\Situacao', $situacao));

                    $this->save($arquivoPacote);
                    if($arquivoPacote->getId_arquivoretornopacote()){
                        if($situacao == 164 ){ //apenas se a situacao foi enviada, atualiza o arquivo
                            //atualiza a situacao do pacote para enviado
                            $pacote = $arquivoPacote->getId_pacote();
                            $pacote->setId_situacao(Situacao::TB_PACOTE_ENVIADO);
                            $this->persist($pacote);

                            //atualiza entrega de material
                            $entregaMaterial = $this->findOneBy('\G2\Entity\EntregaMaterial', array('id_pacote' => $pacote->getId_pacote() , 'id_itemdematerial' => (int) $array_dados[$x]['B'] ));
                            if($entregaMaterial instanceof EntregaMaterial && $entregaMaterial->getId_entregamaterial()) {
                                $entregaMaterial->setId_situacao(Situacao::TB_ENTREGAMATERIAL_ENTREGUE);
                                $entregaMaterial->setDt_entrega(new \DateTime($dataPostagem));
                                $this->persist($entregaMaterial);
                            }
                            $arquivoPacote->setSt_resultado('Arquivo atualizado com sucesso');
                        }else{
                            $arquivoPacote->setSt_resultado('Item não foi atualizado pois retornou como não enviado.');
                        }
                        $this->merge($arquivoPacote);
                    }

                }
                return new \Ead1_Mensageiro('Arquivo de retorno salvo com sucesso.', \Ead1_IMensageiro::SUCESSO);
            }else{
                return new \Ead1_Mensageiro('Arquivo retorno vazio. Valide o arquivo e tente novamente.', \Ead1_IMensageiro::AVISO);
            }

        }catch (\Exception $e){
            //\Zend_Debug::dump($e->getMessage());die;
            return new \Ead1_Mensageiro('Erro ao ler o arquivo de retorno: '.$e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


}
<?php
namespace G2\Negocio;

/**
 * Classe de negócio para métodos realcionados a VwDisciplina
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

class VwModuloDisciplina extends Negocio
{

    private $repositoryName;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = '\G2\Entity\VwModuloDisciplina';
    }

    public function findByVwDisciplina(array $params = array(), array $order = array('st_disciplina' => 'asc'))
    {
        return $this->findBy($this->repositoryName, $params, $order);
    }

}
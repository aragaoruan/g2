<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 06/08/2015
 * Time: 16:04
 */

namespace G2\Negocio;


use Doctrine\Common\Util\Debug;
use DoctrineTest\InstantiatorTestAsset\ExceptionAsset;
use G2\Entity\EsquemaConfiguracaoItem;

class EsquemaConfiguracao extends Negocio
{

    public function __construct()
    {
        parent::__construct();
        $this->esquemaConfiguracao = 'G2\Entity\EsquemaConfiguracao';
    }

    /**
     * Busca dos esquemas de configuração
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornarEsquema(array $params = [], $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            //pega os parametros e remove algumas chaves que não precisaria
            unset($params['to'], $params['txtSearch'], $params['grid']);

            $result = $this->em->getRepository($this->esquemaConfiguracao)
                ->retornaEsquemaConfiguracao($params, $orderBy, $limit, $offset);

            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Salva um esquema de configuração
     *
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarEsquemaConfiguracao(array $params)
    {

        $this->beginTransaction();
        try {

            if (isset($params['id_esquemaconfiguracao']) && !empty($params['id_esquemaconfiguracao'])) {
                /** @var \G2\Entity\EsquemaConfiguracao $esquemaConfiguracao */
                $esquemaConfiguracao = $this->em->getRepository($this->esquemaConfiguracao)->findOneBy(array('id_esquemaconfiguracao' => $params['id_esquemaconfiguracao']));
            } else {
                /** @var \G2\Entity\EsquemaConfiguracao $esquemaConfiguracao */
                $esquemaConfiguracao = new \G2\Entity\EsquemaConfiguracao();
                $esquemaConfiguracao->setid_usuariocadastro($this->getReference('G2\Entity\Usuario', \Ead1_Sessao::getSessaoGeral()->id_usuario));
            }

            $esquemaConfiguracao->setid_usuarioatualizacao($this->getReference('G2\Entity\Usuario', \Ead1_Sessao::getSessaoGeral()->id_usuario));
            $esquemaConfiguracao->setst_esquemaconfiguracao($params['st_esquemaconfiguracao']);

            $result = $this->save($esquemaConfiguracao);
            $this->commit(); //commit

            foreach ($params as $key => $value) {
                $paramsItem = array();
                switch ($key) {
                    case 'regra_funcionamento';
                    case 'regra_pedagogico';
                    case 'regra_portal_aluno';
                    case 'regra_comercial';
                    case 'regra_financeiro';
                        foreach ($value as $dados) {
                            $paramsItem['esquemaConfiguracao'] = $esquemaConfiguracao;
                            $paramsItem['itemConfiguracao'] = $this->em->getRepository('G2\Entity\ItemConfiguracao')->findOneBy(array('id_itemconfiguracao' => $dados['id_itemconfiguracao']));
                            $paramsItem['st_valor'] = $dados['st_default'];
                            $retorno = $this->salvarEsquemaConfiguracaoItem($paramsItem);
                        }
                        break;
                }
            }

            $dataResult = $this->findByEsquemaId($esquemaConfiguracao->getid_esquemaconfiguracao());

            $mensageiro = new \Ead1_Mensageiro(array('message' => \Ead1_IMensageiro::MENSAGEM_SUCESSO, 'data' => $dataResult), \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarEsquemaConfiguracaoItem(array $params)
    {
        $this->beginTransaction();
        try {

            /** @var \G2\Entity\EsquemaConfiguracaoItem $esquemaConfiguracao */
            $esquemaConfiguracaoItem = $this->em->getRepository('G2\Entity\EsquemaConfiguracaoItem')
                ->findOneBy(array(
                    'id_esquemaconfiguracao' => $params['esquemaConfiguracao'],
                    'id_itemconfiguracao' => $params['itemConfiguracao']
                ));


            if (!$esquemaConfiguracaoItem)
                $esquemaConfiguracaoItem = new EsquemaConfiguracaoItem();

            $esquemaConfiguracaoItem->setid_esquemaconfiguracao($params['esquemaConfiguracao']);
            $esquemaConfiguracaoItem->setid_itemconfiguracao($params['itemConfiguracao']);
            $esquemaConfiguracaoItem->setst_valor($params['st_valor']);

            $result = $this->save($esquemaConfiguracaoItem);
            $this->commit(); //commit


            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Doctrine\ORM\ORMException $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Busca um esquema de configuração por um id
     *
     * @param $id
     * @return \Ead1_Mensageiro
     */
    public function findByEsquemaId($id)
    {

        try {
            $results = $this->em->getRepository('\G2\Entity\EsquemaConfiguracaoItem')
                ->retornaEsquemaConfiguracaoItem(array('id_esquemaconfiguracao' => $id));
            if ($results) {
                $mensageiro = new \Ead1_Mensageiro($results, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @param $id
     * @return \Ead1_Mensageiro
     */
    public function removerEsquemaConfiguracao($id)
    {
        try {
            /** @var \G2\Entity\EsquemaConfiguracao $result */
            $result = $this->em->getRepository('G2\Entity\EsquemaConfiguracao')->findOneBy(array('id_esquemaconfiguracao' => $id));

            if ($result) {
                $esquemaItem = $this->em->getRepository('G2\Entity\EsquemaConfiguracaoItem')->findBy(array('id_esquemaconfiguracao' => $result->getid_esquemaconfiguracao()));

                foreach ($esquemaItem as $item) {
                    $this->delete($item);
                }

                $this->delete($result);
            }

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::TYPE_SUCESSO);

        } catch (\Exception $e) {
            //Zend_Debug::dump($mensageiro,__CLASS__.'('.__LINE__.')');exit;
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @return \Ead1_Mensageiro
     */
    public function findAllEsquema()
    {
        try {
            $result = $this->em->getRepository($this->esquemaConfiguracao)->findBy(array());
            if ($result) {
                $arrReturn = array();
                foreach ($result as $row) {
                    $arrReturn[] = $this->toArrayEntity($row);
                }

                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @return \Ead1_Mensageiro
     */
    public function retornaValidadeCredito()
    {
        try {
            $configuracaoentidade = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array("id_itemconfiguracao" => \G2\Constante\ItemConfiguracao::NU_DIAS_CARTA_CREDITO));
            $nu_validadecartacredito = $configuracaoentidade->getSt_valor();
            return $nu_validadecartacredito;
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
            return $mensageiro;
        }
    }

    /**
     * @param $params
     * @return \Ead1_Mensageiro
     */
    public function retornarEsquemaValores($params)
    {
        try {
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->getId_entidade();
            $results = $this->em->getRepository('G2\Entity\EsquemaConfiguracaoItem')->retornaEsquemaConfiguracaoEntidade(
                array(
                    'id_entidade' => $params['id_entidade'],
                    'id_itemconfiguracao' => $params['id_itemconfiguracao']
                ));

            if ($results) {
                $mensageiro = new \Ead1_Mensageiro($results, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;

    }

    /**
     * Retorna as configurações da entidade para politica de cadastro de emails duplicados
     * @return bool
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoParaEmailDuplicado($idEntidade = null)
    {
        try {
            $idEntidade = $idEntidade ? $idEntidade : $this->getId_entidade();
            //busca os dados da configuração de acordo com os itens
            $resultPermicao = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::IMPEDIR_EMAILS_DUPLICADOS
            ));
            //inicia a variavel como true
            $validarEmail = true;

            //verifica se retornou alguma configuração do esquema
            if ($resultPermicao->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $configuracao = $resultPermicao->getFirstMensagem();//atribui a configuração a variavel
                //pega o valor da configuração
                $validarEmail = boolval($configuracao['st_valor']);
            } else {
                //se não encontrou nenhuma configuração vindo do esquema então busca a configuração da forma antiga
                $ceTO = new \ConfiguracaoEntidadeTO();
                $ceTO->setId_entidade($idEntidade);//seta a entidade da sessao para buscar

                $conf = new \ConfiguracaoEntidadeBO();//instancia a bo da configuração da entidade
                $mensageiro = $conf->retornarConfiguracaoEntidade($ceTO);//busca a configuração
                //verifica se não retornou nada, ou se deu erro, e gera um erro
                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Zend_Exception($mensageiro->getFirstMensagem());
                }


                $ceTO = $mensageiro->getFirstMensagem();//atribui o primeiro resultado do mensageiro a variavel
                if ($ceTO->getBl_emailunico()) {//verifica se a configuração para email unico é true
                    //if true, significa que é preciso validar no banco se o e-mail passa já existe
                    $validarEmail = true;

                }
            }

            return $validarEmail;
        } catch (\Exception $e) {
            throw $e;
        }


    }

    /**
     * Retorna as configurações para politica de carga horaria do encontro segundo a entidade da sessão ou passada
     * por parametro
     * @param int $idEntidade opcional ID da entidade
     * @return int Carga horaria
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornaConfiguracaoCargaHorariaEncontro($idEntidade = null)
    {
        try {
            $idEntidade = $idEntidade ? $idEntidade : $this->getId_entidade();


            //busca os dados da configuração de acordo com os itens
            $resultPermicao = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::HORAS_ENCONTRO
            ));

            $nuCargaHoraria = 0;
            //verifica se retornou alguma configuração do esquema
            if ($resultPermicao->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $encontro = $resultPermicao->getFirstMensagem();
                $nuCargaHoraria = (int)$encontro['st_valor'];
            }

            return $nuCargaHoraria;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna a configuração para politica de exibir tutorial
     * @return bool
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoTutorial()
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultPermicao = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::EXIBIR_TUTORIAL_PORTAL
            ));

            $exibirTutorial = false;
            //verifica se retornou alguma configuração do esquema
            if ($resultPermicao->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $tutorial = $resultPermicao->getFirstMensagem();
                $exibirTutorial = boolval($tutorial['st_valor']);
            }

            return $exibirTutorial;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna a configuração para politica de exibir tutorial para primeiro acesso
     * @return bool
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoPrimeiroAcesso($id_entidade = null)
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultPermicao = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::FERRAMENTA_PRIMEIRO_ACESSO,
                'id_entidade' => $id_entidade
            ));

            $exibirTutorial = false;
            //verifica se retornou alguma configuração do esquema
            if ($resultPermicao->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $tutorial = $resultPermicao->getFirstMensagem();
                $exibirTutorial = boolval($tutorial['st_valor']);
            }

            return $exibirTutorial;

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna as configurações para politica de acesso simultaneo do portal
     * @param null $idEntidade
     * @return bool
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoLoginSimultaneo($idEntidade = null)
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultLogin = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::PERMITIR_LOGIN_SIMULTANEO,
                'id_entidade' => $idEntidade
            ));

            $loginSimultaneo = true;
            if ($resultLogin->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $result = $resultLogin->getFirstMensagem();
                $loginSimultaneo = boolval($result['st_valor']);
            }
            return $loginSimultaneo;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna configuração para campo da google para cartão de credito
     * @return null | string
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoCampoGoogleCredito()
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultConfig = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::CONFIGURARACAO_VENDA_CARTAO_CREDITO,
            ));

            $valor = null;
            if ($resultConfig->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $result = $resultConfig->getFirstMensagem();
                $valor = $result['st_valor'];
            } else {
                $configuracaoEntidade = new ConfiguracaoEntidade();
                $resultconfiguracaoEntidade = $configuracaoEntidade->findConfiguracaoEntidade($this->getId_entidade());
                if ($resultconfiguracaoEntidade)
                    $valor = $resultconfiguracaoEntidade['st_campotextogooglecredito'];
            }
            return $valor;
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Retorna configurações do google para pagamentos em boleto
     * @return null
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarConfiguracaoCampoGoogleBoleto()
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultConfig = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::CONFIGURARACAO_VENDA_BOLETO,
            ));
            $valor = null;
            if ($resultConfig->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $result = $resultConfig->getFirstMensagem();
                $valor = $result['st_valor'];
            } else {
                $configuracaoEntidade = new ConfiguracaoEntidade();
                $resultconfiguracaoEntidade = $configuracaoEntidade->findConfiguracaoEntidade($this->getId_entidade());
                if ($resultconfiguracaoEntidade)
                    $valor = $resultconfiguracaoEntidade['st_campotextogoogleboleto'];
            }
            return $valor;
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Busca as configurações do modelo de grade nota
     * @return null
     * @throws \Exception
     */
    public function retornarConfiguracaoModeloGradeNotas()
    {
        try {
            //busca os dados da configuração de acordo com os itens
            $resultConfig = $this->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::MODELO_GRADE_HORARIA,
            ));
            $valor = null;
            if ($resultConfig->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $result = $resultConfig->getFirstMensagem();
                $valor = $result['st_valor'];
            } else {
                $configuracaoEntidade = new ConfiguracaoEntidade();
                $resultconfiguracaoEntidade = $configuracaoEntidade->findConfiguracaoEntidade($this->getId_entidade());
                if ($resultconfiguracaoEntidade)
                    $valor = $resultconfiguracaoEntidade['id_modelogradenotas'];
            }
            return $valor;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $id_itemconfiguracao
     * @param $id_entidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaItemPorEntidade($id_itemconfiguracao, $id_entidade)
    {
        try {
            return $this->toArrayEntity($this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
                'id_itemconfiguracao' => $id_itemconfiguracao,
                'id_entidade' => $id_entidade,
            )));
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar Item Configuração. ' . $e->getMessage());
        }
    }

    public function isProvaPorDisciplina($id_entidade = null)
    {
        $id_entidade = $id_entidade ? $id_entidade : $this->sessao->id_entidade;

        $en_confg = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', array(
            'id_entidade' => $id_entidade,
            'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::AVALIACAO_POR_DISCIPLINA,
            'st_valor' => true
        ));

        return ($en_confg instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao);
    }

    /**
     * @param $idITemConfiguracao
     * @return \Doctrine\ORM\NoResultException|\Exception|null|object
     */
    public function retornarItemConfiguracaoPorId($idITemConfiguracao)
    {
        return $this->find('\G2\Entity\ItemConfiguracao', $idITemConfiguracao);
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Contas e relacionados
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-07-30
 */
class EntidadeIntegracao extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados dos tipos de conta
     * @return array $return
     */
    public function retornaEntidadeIntegracao($id_entidade, $id_sistema)
    {
        try {
            $return = $this->findOneBy('G2\Entity\EntidadeIntegracao', array(
                'id_entidade' => $id_entidade,
                'id_sistema' => $id_sistema
            ));

            if ($return)
                return $this->toArrayEntity($return);

        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    public function retornaEntidadeIntegracaoEntity(array $where)
    {
        try {
            return $this->findOneBy('\G2\Entity\EntidadeIntegracao', $where);
        } catch (\Zend_Exception $message) {
            throw $message;
        } catch (\Exception $message) {
            throw $message;
        }
    }

    /**
     * Retorna dados para integra��o com Pearson
     * @param integer $id_entidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarIntegracaoPearson($id_entidade = null, $id_usuario = null)
    {
        try {
            //verifica se foi passado o id_entidade se n�o foi, pega a entidade da sess�o
            $id_entidade = !empty($id_entidade) ? $id_entidade : $this->getId_entidade();
            //pega o id do sistema da classe de constantes
            $id_sistema = \G2\Constante\Sistema::PEARSON;

            //busca os dados da integração
            $result = $this->retornaEntidadeIntegracao($id_entidade, $id_sistema);

            //pega os dados da entidade
            $entidadeNegocio = new \G2\Negocio\Entidade();
            $entidade = $entidadeNegocio->findEntidade($id_entidade);

            $arrReturn = array();//cria uma array vazio para retorno
            if (!empty($result) && !empty($entidade)) {
                //busca os dados do usu�rio
                $pessoaNegocio = new Pessoa();
                $pessoa = $pessoaNegocio->retornaUsuarioById($id_usuario ? $id_usuario : $this->sessao->id_usuario);

                //monta o array para retorno
                $arrReturn = array(
                    'st_caminho' => str_replace('[nome_da_instituicao]', urlencode(str_replace(' ', '_', 'unyead')), $result['st_caminho']),
                    'st_token' => $result['st_codchave'],
                    'id_usuario' => $pessoa->getId_usuario(),
                    'st_usuario' => $pessoa->getSt_login()
                );
                unset($result);
            }

            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao buscar integraçãoo com sistema Pearson");
        }

    }

}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Entidade PeriodoLetivo
 * @author Felipe Pastor
 * @since 2013-01-16
 */
class PeriodoLetivo extends Negocio
{

    /**
     * String repository name
     * @var string
     */
    private $repositoryName = 'G2\Entity\PeriodoLetivo';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAllPeriodos()
    {
        return $this->findBy($this->repositoryName, ['id_entidade' => $this->sessao->id_entidade], ['id_periodoletivo' => 'DESC']);
    }

    /**
     * @param array $dados
     * @return \Ead1_Mensageiro|null
     */
    public function salvarDadosPeriodoLetivo(array $dados)
    {
        try {
            $this->beginTransaction();

            $id_tipooferta = $this->getReference('\G2\Entity\TipoOferta', $dados['id_tipooferta']);
            $dt_inicioinscricao = $this->converteDataBanco($dados['dt_inicioinscricao']);
            $dt_fiminscricao = $this->converteDataBanco($dados['dt_fiminscricao']);
            $dt_encerramento = $this->converteDataBanco($dados['dt_encerramento']);
            $dt_abertura = $this->converteDataBanco($dados['dt_abertura']);

            if (($dt_inicioinscricao <= $dt_fiminscricao) && ($dt_abertura <= $dt_encerramento)) {
                if (isset($dados['id_periodoletivo'])) {
                    $entidade = $this->find($this->repositoryName, $dados['id_periodoletivo']);

                    $entidade->setId_entidade($this->sessao->id_entidade);
                    $entidade->setId_usuariocadastro($this->sessao->id_usuario);
                    $entidade->setSt_periodoletivo($dados['st_periodoletivo']);
                    $entidade->setId_tipooferta($id_tipooferta);
                    $entidade->setDt_inicioinscricao($dt_inicioinscricao);
                    $entidade->setDt_fiminscricao($dt_fiminscricao);
                    $entidade->setDt_encerramento($dt_encerramento);
                    $entidade->setDt_abertura($dt_abertura);

                } else {
                    $dados['id_entidade'] = $this->sessao->id_entidade;
                    $dados['id_usuariocadastro'] = $this->sessao->id_usuario;
                    $dados['id_tipooferta'] = $id_tipooferta;
                    $dados['dt_inicioinscricao'] = $dt_inicioinscricao;
                    $dados['dt_fiminscricao'] = $dt_fiminscricao;
                    $dados['dt_encerramento'] = $dt_encerramento;
                    $dados['dt_abertura'] = $dt_abertura;

                    $entidade = $this->arrayToEntity(new \G2\Entity\PeriodoLetivo(), $dados);
                }

                $entidadeSave = $this->save($entidade);

                $this->commit();

                if ($entidadeSave) {
                    return new \Ead1_Mensageiro($entidadeSave, \Ead1_IMensageiro::SUCESSO);
                }
            } else {
                return new \Ead1_Mensageiro('Erro ao salvar os dados, as datas de início devem ser menor que as datas finais.', \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return false;
    }

    /**
     * @param $id
     * @return \Ead1_Mensageiro|null
     */
    public function removerPeriodoLetivo($id)
    {
        try {

            $delete = $this->delete($this->find($this->repositoryName, $id));

            if ($delete)
                return new \Ead1_Mensageiro('Removido com sucesso', \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Afiliados
 *
 * @author Rafael Leite <rafael.leite@unyleya.com.br>
 */
class Afiliado extends Negocio
{


    /**
     * Metodo responsavel por gravar os afiliados na tabela
     * @param array $dadosAfiliados
     * @return \Ead1_Mensageiro
     */
    public function salvaAfiliado($afiliado)
    {

        $mensageiro = new \Ead1_Mensageiro();

        try {
            // Pesquisar para verificar se o registro tb_classeafiliacao existe
            $entityClasseAfiliacao = $this->find('\G2\Entity\ClasseAfiliacao', $afiliado['id_classeafiliacao']);

            if ($entityClasseAfiliacao instanceof \G2\Entity\ClasseAfiliacao) {
                // Atualiza o tb_classeafiliacao existente
                $entityClasseAfiliacao->setSt_classeafiliacao($afiliado['st_classeafiliacao']);
                $entityClasseAfiliacao = $this->merge($entityClasseAfiliacao);
            } else {
                // Salva novo tb_classeafiliacao
                $entityClasseAfiliacao = new \G2\Entity\ClasseAfiliacao();
                $entityClasseAfiliacao->setId_classeafiliacao($afiliado['id_classeafiliacao']);
                $entityClasseAfiliacao->setSt_classeafiliacao($afiliado['st_classeafiliacao']);
                $entityClasseAfiliacao = $this->persist($entityClasseAfiliacao);
            }

            // Pesquisar para verificar se o registro tb_afiliado existe
            $entityAfiliado = $this->find('\G2\Entity\Afiliado', $afiliado['id_afiliado']);

            //se não existir afiliado...
            if ($entityAfiliado instanceof \G2\Entity\Afiliado) {
                // Atualiza o registro tb_afiliado existente
                $entityAfiliado->setSt_afiliado($afiliado['st_nomeafiliado']);
                $entityAfiliado = $this->merge($entityAfiliado);
            } else {
                // Salva novo registro tb_afiliado
                $entityAfiliado = new \G2\Entity\Afiliado();
                $entityAfiliado->setId_afiliado($afiliado['id_afiliado']);
                $entityAfiliado->setSt_afiliado($afiliado['st_nomeafiliado']);
                $entityAfiliado = $this->persist($entityAfiliado);
            }

            // Salvar relacionamento Afiliado, Classe Afiliação e Entidade
            if ($entityAfiliado && $entityAfiliado->getId_afiliado()) {
                $entityRelacionamento = $this->findOneBy('\G2\Entity\AfiliadoClasseEntidade', array(
                    'id_afiliado'        => $entityAfiliado->getId_afiliado(),
                    'id_classeafiliacao' => $entityClasseAfiliacao->getId_classeafiliacao(),
                    'id_entidade'        => $afiliado['id_entidade']
                ));

                if (!($entityRelacionamento instanceof \G2\Entity\AfiliadoClasseEntidade)) {
                    $entityRelacionamento = new \G2\Entity\AfiliadoClasseEntidade();
                    $entityRelacionamento->setId_afiliado($entityAfiliado);
                    $entityRelacionamento->setId_classeafiliacao($entityClasseAfiliacao);
                    $entityRelacionamento->setId_entidade($this->getReference('\G2\Entity\Entidade', $afiliado['id_entidade']));
                    $entityRelacionamento = $this->persist($entityRelacionamento);
                }
            }

            $mensageiro->setTipo(\Ead1_IMensageiro::SUCESSO);
            $mensageiro->setText('Registro salvo com sucesso');
        } catch (\Exception $e) {
            $mensageiro->setTipo(\Ead1_IMensageiro::ERRO);
            $mensageiro->setText('Erro ao salvar o registro');
        }

        return $mensageiro->setMensagem($afiliado);
    }

}
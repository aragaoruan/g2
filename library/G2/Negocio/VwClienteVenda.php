<?php

namespace G2\Negocio;

/**
 * Classe de negócio para VwClientaVenda
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwClienteVenda extends Negocio
{

    private $repositoryName = 'G2\Entity\VwClienteVenda';

    public function _construct()
    {
        set_time_limit(0);
        parent::__construct();
    }

    public function findById($id)
    {
        $result = parent::find($this->repositoryName, $id);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * @param array $param
     * @return \Ead1_Mensageiro
     */
    public function findBy($repositoryName, array $where, array $order = NULL, $limit = NULL, $offset = NULL)
    {
        $result = parent::findBy($this->repositoryName, $where);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function findAll($repositoryName = null)
    {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function pesquisar(array $filtro = null)
    {
        $dql = 'SELECT vw FROM G2\Entity\VwClienteVenda vw WHERE vw.id_entidade = :id_entidade ';

        if ($filtro) {
            foreach ($filtro as $key => $value) {
                if ($key == 'st_nomecompleto') {
                    $dql .= ' AND vw.' . $key . ' LIKE \'%' . str_ireplace(' ', '_', $value) . '%\''; //prepara os parametros que serão atribuidos abaixo
                } else {
                    $dql .= ' AND vw.' . $key . ' = ' . $value; //prepara os parametros que serão atribuidos abaixo
                }
            }
        }

        $query = $this->em->createQuery($dql);
        $query->setParameter('id_entidade', \Ead1_Sessao::getSessaoGeral()->id_entidade);

        //\Zend_Debug::dump($query->getSQL(), __CLASS__ . ' on ' . __FILE__ . '('.__LINE__.')');exit;
        return $query->getResult();
    }

}

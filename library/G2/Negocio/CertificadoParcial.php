<?php
/**
 * Created by PhpStorm.
 * User: aragaoruan
 * Date: 12/04/18
 * Time: 16:46
 */

namespace G2\Negocio;


use G2\Entity\HistoricoCertificadoParcial;
use G2\Entity\Usuario;
use G2\G2Entity;
use G2\Utils\Helper;

class CertificadoParcial extends Negocio
{
    /**
     * @var \G2\Entity\CertificadoParcial
     */
    private $repositoryCertificadoParcial;

    /**
     * @var \G2\Entity\VwAlunosaptoscertificadoparcial
     */
    private $repositoryVwAlunosAptosCertificadoParcial;

    /**
     * @var \G2\Entity\CertificadoParcialDisciplina
     */
    private $repoistoryCertificadoParcialDisc;

    /**
     * @var \G2\Entity\VwGradeDisciplinaCertificadoP
     */
    private $repositoryVwGradeDisciplinaCertificadoP;

    /**
     * @var \G2\Entity\HistoricoCertificadoParcial;
     */
    private $repositoryHistoricoCertificadoParcial;

    private $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryCertificadoParcial = '\G2\Entity\CertificadoParcial';
        $this->repositoryVwAlunosAptosCertificadoParcial = '\G2\Entity\VwAlunosaptoscertificadoparcial';
        $this->repoistoryCertificadoParcialDisc = '\G2\Entity\CertificadoParcialDisciplina';
        $this->repositoryVwGradeDisciplinaCertificadoP = '\G2\Entity\VwGradeDisciplinaCertificadoP';
        $this->repositoryHistoricoCertificadoParcial = '\G2\Entity\HistoricoCertificadoParcial';

        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Metodo responsável por buscar todos os certificados parciais
     * Todos os tratamentos de dados estão sendo feitos no front-end.
     *
     * @return array|\Ead1_Mensageiro
     */
    public function retornaCertificadoParcial()
    {
        try {

            return $this->getRepository('\G2\Entity\CertificadoParcial')
                ->retornaCertificadoParcial($this->sessao->id_entidade);

        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo responsável por buscar os polos
     * Todos os tratamentos de dados estão sendo feitos no front-end.
     *
     * @return array|\Ead1_Mensageiro
     */
    public function retornaPolos()
    {
        try {
            $polos = $this->findBy('\G2\Entity\VwEntidadeRelacao',
                array('id_entidadepai' => $this->getId_entidade()));

            if ($polos) {
                return $this->toArrayEntity($polos);
            }

            return $polos;
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo responsável por retornar os alunos aptos a emitir um certificado parcial
     *
     * @param $params
     * @return array
     */
    public function retornaAlunosAptosCertificadoParcial($params)
    {
        $repo = $this->em->getRepository($this->repositoryVwAlunosAptosCertificadoParcial);
        $query = $repo->createQueryBuilder("vw");

        if ($params["id_entidade"]) {
            $query->where("vw.id_entidade = :id_entidade")
            ->setParameter("id_entidade", $params["id_entidade"]);
        } else {
            $polos = $this->retornaPolos();
            if ($polos) {
                foreach ($polos as $value) {
                    $entidade[] = $value["id_entidade"];
                }
                $query->where("vw.id_entidade IN (" . implode(',', $entidade) . ")");

                $query->andWhere("vw.id_entidadecadastro = :id_entidadecadastro")
                    ->setParameter("id_entidadecadastro", $this->sessao->id_entidade);

            } else {
                $query->where("vw.id_entidade IN (" . $this->sessao->id_entidade . ")");
            }

        }

        if ($params["id_certificadoparcial"]) {
            $query->andWhere("vw.id_certificadoparcial = :id_certificadoparcial")
                ->setParameter("id_certificadoparcial", $params["id_certificadoparcial"]);
        }

        if ($params["st_nomecompleto"]) {
            $query->andWhere("vw.st_nomecompleto LIKE '%".trim($params["st_nomecompleto"])."%' ");
        }

        if ($params["st_cpf"]) {
            $query->andWhere("vw.st_cpf LIKE '%".trim($params["st_cpf"])."%' ");
        }

        if ($params["status"] == "1") {
             $query->andWhere("vw.dt_impressao IS NOT NULL");
        } else {
             $query->andWhere("vw.dt_impressao IS NULL");
             $query->andWhere("vw.id_entidadecadastro = :id_entidadecadastro")
                ->setParameter("id_entidadecadastro", $this->sessao->id_entidade);
        }

        $query->orderBy("vw.st_nomecompleto");
        return $query->getQuery()->getArrayResult();
    }

    public function salvarCertificadoParcial($params, $texto)
    {
        try {
            $registro = $this->gerarRegistroCertificadoParcial($params["id_certificadoparcial"],
                $params["id_matricula"], $params["id_entidade"]);
            $this->beginTransaction();

            if ($registro instanceof \Ead1_Mensageiro) {
                return $this->mensageiro->setMensageiro("Por favor preencha o campo Sigla para Certificado",
                    \Ead1_IMensageiro::ERRO);
            }

            $historico = new HistoricoCertificadoParcial();
            $historico->setId_certificadoparcial($params["id_certificadoparcial"]);
            $historico->setId_matricula($params["id_matricula"]);
            $historico->setId_entidade($params["id_entidade"]);
            $historico->setId_usuariocadastro($this->sessao->id_usuario);
            $historico->setSt_conteudo($texto);
            $historico->setSt_registro($registro);
            $historico->setDt_cadastro(new \DateTime());

            $this->save($historico);

            $certificado = $this->findOneBy($this->repositoryCertificadoParcial, array(
                'id_certificadoparcial' => $params["id_certificadoparcial"]));
            $certificado->setBl_gerado(true);

            $this->save($certificado);
            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Função responsável por gerar o registro do certificado parcial
     *
     * @param $idCertificado
     * @param $idMatricula
     * @param $idEntidade
     * @return string
     * @throws \Exception
     *
     */
    public function gerarRegistroCertificadoParcial($idCertificado, $idMatricula, $idEntidade)
    {
        try {
            $entidade = $this->findOneBy(\G2\Entity\Entidade::class, array('id_entidade' => $idEntidade));
            $date = \Zend_Date::now();
            $registro = $idCertificado.'.'.$idMatricula. '/'
                . $entidade->getSt_siglaentidade(). '-' . $date->get(\Zend_Date::YEAR_SHORT);

            if (!$entidade->getSt_siglaentidade()) {
                return $this->mensageiro->setMensageiro("Por favor preencha o campo Sigla para Certificado",
                    \Ead1_IMensageiro::ERRO);
            }

            return $registro;

        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo responsavel por retornar um conjunto de diciplinas de um certificado parcial
     * De um determinado aluno. Essa função será utilizada pelo gerador de certificados, para substituir uma
     * variavel de texto.
     *
     * @param $params
     * @return array
     * @throws \Zend_Exception
     *
     */
    public function retornaDisciplinasCertificadoParcial($idMatricula, $params)
    {
        $arrDisciplinas = $this->findBy($this->repositoryVwGradeDisciplinaCertificadoP, array(
            'id_certificadoparcial' => $params['id_certificadoparcial'],
            'id_matricula' => $idMatricula
        ));

        if ($arrDisciplinas) {
            return $this->toArrayEntity($arrDisciplinas);
        }

        return $arrDisciplinas;
    }

    public function gerarPeriodoRealizacaoCertificadoParcial($idMatricula, $params)
    {
        $disciplinas = $this->retornaDisciplinasCertificadoParcial($idMatricula, $params);
        $semestre = array();

        foreach ($disciplinas as $value) {
            $semestre[] = $value["st_semestre"];
        }
        arsort($semestre);

        $primeiro = end($semestre);
        $ultimo = $semestre[0];

        if ($primeiro != $ultimo) {
            return $primeiro . ' a ' . $ultimo;
        }

        return $primeiro;
    }

    /**
     * Função responsável por buscar o texto de sistema, para ser exibido no certificado parcial
     *
     * @param $params
     * @return \TextoSistemaTO
     * @throws \Zend_Exception
     */
    public function visualizarCertificado($params)
    {
        $certificadoParcial = $this->findOneBy($this->repositoryCertificadoParcial,
            array('id_certificadoparcial' => $params['id_certificadoparcial'])
        );

        $textoSistemaTO = new \TextoSistemaTO();
        $textoSistemaTO->setId_textosistema($certificadoParcial->getId_textosistema());
        $textoSistemaTO->fetch(true, true, true);

        return $textoSistemaTO;
    }

    /**
     * Função responsável por retornar o historico de impressões de certificado parcial do aluno
     *
     * @param $params
     * @return array|\Ead1_Mensageiro
     */
    public function retornaHistorico($params)
    {
        try {
            $arrRetorno = array();
            $historico = $this->findBy($this->repositoryHistoricoCertificadoParcial, array(
                'id_certificadoparcial' => $params['id_certificadoparcial'],
                'id_matricula' => $params['id_matricula']
            ));

            if ($historico) {

                foreach ($historico as $value) {
                    $certificado = $this->findOneBy($this->repositoryCertificadoParcial, array(
                        'id_certificadoparcial' => $value->getId_certificadoparcial()));
                    $usuario = $this->findOneBy(Usuario::class, array(
                        'id_usuario' => $value->getId_usuariocadastro()
                    ));
                    $polo = $this->findOneBy(\G2\Entity\Entidade::class, array(
                        'id_entidade' => $value->getId_entidade()
                    ));

                    $arrRetorno[] = array(
                        "st_certificadoparcial" => $certificado->getSt_certificadoparcial(),
                        "dt_cadastro" => Helper::converterData($value->getDt_cadastro(), 'd/m/Y'),
                        "st_usuariocadastro" => $usuario->getSt_nomecompleto(),
                        "st_polo" => $polo->getSt_nomeentidade()
                    );

                }

            }

            return $arrRetorno;
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    public function verificarSigla($idEntidade)
    {
        try {
            $entidade = $this->find('\G2\Entity\Entidade', $idEntidade);
            if (!$entidade->getSt_siglaentidade()) {
                return $this->mensageiro->setMensageiro("Por favor preencha o campo Sigla para Certificado", \Ead1_IMensageiro::AVISO);
            }
            return $entidade;
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }
}

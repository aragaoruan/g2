<?php

namespace G2\Negocio;

use G2\Constante\Evolucao;
use G2\Constante\TipoAvaliacao;
use G2\G2Entity;
use G2\Transformer\Cancelamento\MatriculaCancelamentoTransformer;
use G2\Constante\Sistema;
use G2\Entity\AlocacaoIntegracao;
use G2\Entity\VwGradeNota;
use G2\Utils\Helper;

/**
 * Classe de negócio para Contrato
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @update Denise Xavier <denise.xavier@unyleya.com.br>
 */
class Matricula extends Negocio
{
    const LABEL_MODULO_COMPLEMENTAR = 'Módulo(s) Complementar(es)';


    public $mensageiro;
    private $repository = 'G2\Entity\Matricula';

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Retorna a grade de notas no app
     * @param $gradenota -- Objeto com os dados da vw_gradenota para serem parâmetros exemplo: gradenota: {"id_matricula":"518031","id_disciplina":4105}
     * @param array $order
     * @param $limit
     * @param $offset
     * @param bool $returnMoodleData
     * @return array
     * @throws \Exception
     */
    public function retornaGradeNotas($gradenota, array $order, $limit, $offset, $returnMoodleData = false)
    {
        try {

            $vwGrade = new \G2\Entity\VwGradeNota();
            $uss = $vwGrade->findByCustom($gradenota, $order, $limit, $offset, false);
            if ($uss) {
                foreach ($uss as $key => $line) {
                    $uss[$key] = array_map(function ($value) {
                        if ($value instanceof \DateTime) {
                            $value = $value->format('Y-m-d H:i:s');
                        }
                        return $value;
                    }, $line);

                    if (!empty($line['id_alocacao']) && $returnMoodleData) {

                        $ali = new AlocacaoIntegracao();
                        $ali->findOneBy(array('id_alocacao' => $line['id_alocacao']), false, array('dt_cadastro'=>'DESC'));
                        if ($ali->getSt_codalocacao() && $ali->getId_entidadeintegracao()) {
                            $uss[$key] = array_merge($uss[$key], $this->_returnaNotasAVA($ali->getSt_codalocacao(), $ali->getId_entidadeintegracao()));
                        }

                    }

                }
                return $uss;
            }
            return array();

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * Retorna as notas do aluno no moodle
     * @param $st_codalocacao
     * @param $id_entidadeintegracao
     * @return array
     * @throws \Exception
     */
    private function _returnaNotasAVA($st_codalocacao, $id_entidadeintegracao, $indice = 'ar_notasmoodle')
    {
        $id_perfilmoodle = null;
        $id_usuariomoodle = null;
        $id_cursomoodle = null;
        $ar_notasmoodle = null;

        if (!$id_entidadeintegracao || !$st_codalocacao) {
            throw new \Exception("Integração com a Alocação inexistente");
        }

        @list($id_perfilmoodle, $id_usuariomoodle, $id_cursomoodle) = explode('#', $st_codalocacao);
        if ($id_usuariomoodle && $id_cursomoodle) {
            $moodle = new \MoodleUsuariosWebServices(null, $id_entidadeintegracao);
            $notasmoodle = $moodle->retornarNotaDetalhada($id_usuariomoodle, $id_cursomoodle);
            if (isset($notasmoodle->items)) {
                $ar_notasmoodle = $notasmoodle->items;
            }
        }
        return array($indice => $ar_notasmoodle);
    }


    /**
     * Retorna a grade de notas agrupada
     * @param $params
     * @param bool $retornarNotasMoodle
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVwGradeNotaAgrupada($params, $retornarNotasMoodle = false)
    {
        try {
            $vws = $this->findByVwGradeNota($params);
            $return = array();
            $arr = array();

            if (is_array($vws)) {
                // Preparar o array inicial somente de salas com categoria "NORMAL" ("PRR" serão adicionados mais abaixo)
                foreach ($vws as $key => $vw) {
                    if ($vw instanceof VwGradeNota) {
                        if ($vw->getId_categoriasala() == \G2\Constante\CategoriaSala::NORMAL || !$vw->getId_categoriasala()) {
                            $return[$key]['url'] = self::verificarSeExisteRelatorioLog(
                                $vw->getId_saladeaula(), $vw->getId_matricula()
                            );
                            $return[$key]['dt_inicio'] = self::dateRender($vw, 'getDt_inicio');
                            $return[$key]['dt_abertura'] = self::dateRender($vw, 'getDt_abertura');
                            $return[$key]['dt_encerramento'] = self::dateRender($vw, 'getDt_encerramento');
                            $return[$key]['dt_encerramentoextensao'] = self::dateRender($vw, 'getDt_encerramentoextensao');
                            $return[$key]['dt_cadastroaproveitamento'] = self::dateRender($vw, 'getDt_cadastroaproveitamento');
                            $return[$key]['dt_cadastroaproveitamento'] = self::dateRender($vw, 'getDt_cadastroaproveitamento');
                            $return[$key]['dt_defesatcc'] = $vw->getDt_defesatcc();
                            $return[$key]['id_saladeaula'] = $vw->getId_saladeaula();
                            $return[$key]['st_saladeaula'] = $vw->getSt_saladeaula();
                            $return[$key]['id_disciplina'] = $vw->getId_disciplina();
                            $return[$key]['id_alocacao'] = $vw->getId_alocacao();
                            $return[$key]['id_projetopedagogico'] = $vw->getId_projetopedagogico();
                            $return[$key]['st_projetopedagogico'] = $vw->getSt_projetopedagogico();
                            $return[$key]['st_notatcc'] = is_null($vw->getSt_notatcc()) ? '-' : (int)$vw->getSt_notatcc();
                            $return[$key]['nu_notafaltante'] = is_null($vw->getNu_notafaltante()) ? '0' : (int)$vw->getNu_notafaltante();
                            $return[$key]['nu_notamaxima'] = is_null($vw->getNu_notamaxima()) ? '0' : (int)$vw->getNu_notamaxima();
                            $return[$key]['nu_percentualaprovacao'] = is_null($vw->getNu_percentualaprovacao()) ? '0' : (int)$vw->getNu_percentualaprovacao();
                            $return[$key]['st_notaead'] = is_null($vw->getSt_notaead()) ? '-' : (int)$vw->getSt_notaead();
                            $return[$key]['st_notafinal'] = is_null($vw->getSt_notafinal()) ? '-' : (int)$vw->getSt_notafinal();
                            $return[$key]['st_disciplina'] = $vw->getSt_disciplina();
                            $return[$key]['id_tipodisciplina'] = $vw->getId_tipodisciplina();
                            $return[$key]['st_tituloexibicaodisciplina'] = $vw->getSt_tituloexibicaodisciplina();
                            $return[$key]['id_matricula'] = $vw->getId_matricula();
                            $return[$key]['nu_notafinal'] = is_null($vw->getNu_notafinal()) || $vw->getNu_notafinal() == 0 ? '-' : (int)$vw->getNu_notafinal();
                            $return[$key]['id_evolucao'] = $vw->getId_evolucao();
                            $return[$key]['st_evolucao'] = $vw->getSt_evolucao();
                            $return[$key]['id_situacao'] = $vw->getId_situacao();
                            $return[$key]['st_situacao'] = $vw->getSt_situacao();
                            $return[$key]['nu_cargahoraria'] = (int)$vw->getNu_cargahoraria() ? (int)$vw->getNu_cargahoraria() : ' - ';
                            $return[$key]['nu_notatotal'] = $vw->getNu_notatotal();
                            $return[$key]['id_encerramentosala'] = $vw->getId_encerramentosala();
                            $return[$key]['st_status'] = $vw->getSt_status();
                            $return[$key]['bl_status'] = $vw->getBl_status();
                            $return[$key]['id_matriculadisciplina'] = $vw->getId_matriculadisciplina();
                            $return[$key]['id_avaliacaoconjuntoreferencia'] = $vw->getId_avaliacaoconjuntoreferencia();
                            $return[$key]['id_tiponotatcc'] = $vw->getId_tiponotatcc();
                            $return[$key]['id_tiponotafinal'] = $vw->getId_tiponotafinal();
                            $return[$key]['id_tiponotaead'] = $vw->getId_tiponotaead();
                            $return[$key]['id_tiponotarecuperacao'] = $vw->getId_tiponotarecuperacao();
                            $return[$key]['bl_complementar'] = $vw->getBl_complementar();
                            $return[$key]['st_notaatividade'] = is_null($vw->getSt_notaatividade()) ? '-' : (int)$vw->getSt_notaatividade();
                            $return[$key]['st_notarecuperacao'] = is_null($vw->getSt_notarecuperacao()) ? '-' : (int)$vw->getSt_notarecuperacao();
                            $return[$key]['st_nomeusuarioaproveitamento'] = $vw->getSt_nomeusuarioaproveitamento();
                            $return[$key]['id_modulo'] = $vw->getId_modulo();
                            $return[$key]['st_modulo'] = $vw->getSt_modulo();
                            $return[$key]['id_categoriasala'] = $vw->getId_categoriasala();

                            //avaliacoes graduacao
                            $return[$key]['st_statusdisciplina'] = is_null($vw->getSt_statusdisciplina()) ? '-' : $vw->getSt_statusdisciplina();
                            $return[$key]['st_avaliacaoatividade'] = $vw->getSt_avaliacaoatividade();
                            $return[$key]['st_avaliacaoead'] = $vw->getSt_avaliacaoead();
                            $return[$key]['st_avaliacaofinal'] = $vw->getSt_avaliacaofinal();
                            $return[$key]['st_avaliacaorecuperacao'] = $vw->getSt_avaliacaorecuperacao();
                            $return[$key]['st_nomeusuarioaproveitamento'] = $vw->getSt_nomeusuarioaproveitamento();
                            $return[$key]['id_aproveitamento'] = is_null($vw->getId_aproveitamento()) ? NULL : (int)$vw->getId_aproveitamento();
                            $return[$key]['st_disciplinaoriginal'] = is_null($vw->getSt_disciplinaoriginal()) ? '-' : $vw->getSt_disciplinaoriginal();

                            //dados da PRR
                            $return[$key]['st_status_prr'] = "";
                            $return[$key]['st_notaead_prr'] = "";
                            $return[$key]['st_notatcc_prr'] = "";
                            $return[$key]['st_saladeaula_prr'] = "";
                            $return[$key]['nu_notafinal_prr'] = "";
                            $return[$key]['nu_cargahoraria_prr'] = "";
                            $return[$key]['id_disciplina_prr'] = "";
                            $return[$key]['st_disciplina_prr'] = "";
                            $return[$key]['id_matriculadisciplina_prr'] = "";
                            $return[$key]['dt_inicio_prr'] = "";
                            $return[$key]['dt_abertura_prr'] = "";
                            $return[$key]['dt_encerramento_prr'] = "";
                            $return[$key]['id_tiponotatcc_prr'] = "";
                            $return[$key]['id_tiponotafinal_prr'] = "";
                            $return[$key]['id_tiponotaead_prr'] = "";
                            $return[$key]['id_tiponotarecuperacao_prr'] = "";
                            $return[$key]['st_status_prr'] = "";
                            $return[$key]['ar_notasmoodle'] = array();
                            $return[$key]['ar_notasmoodle_prr'] = array();
                            $return[$key]['url_prr'] = null;
                            /**
                             * Dados da alocação com o Moodle
                             */
                            if ($retornarNotasMoodle) {
                                $ali = new AlocacaoIntegracao();
                                $ali->findOneBy(array('id_alocacao' => $vw->getId_alocacao()), false, array('dt_cadastro'=>'DESC'));
                                if ($ali->getSt_codalocacao() && $ali->getId_entidadeintegracao()) {
                                    $notasMoodle = $this->_returnaNotasAVA($ali->getSt_codalocacao(), $ali->getId_entidadeintegracao()->getId_entidadeintegracao());
                                    $return[$key] = array_merge($return[$key], $notasMoodle);
                                }
                            }

                            //pega o módulo
                            $idModulo = $vw->getId_modulo();
                            $stModulo = $vw->getSt_modulo();

                            //se existir um modulo diferent...
                            if (!array_key_exists($idModulo, $arr)) {
                                $arr[$idModulo]['id_modulo'] = $idModulo;
                                $arr[$idModulo]['st_modulo'] = $stModulo;
                                $arr[$idModulo]['disciplinas'] = array();
                            }
                        }


                    }

                }

                foreach ($vws as $key2 => $vw2) {
                    if ($vw2 instanceof VwGradeNota) {

                        // Procurar salas com categoria "PRR" para completar o array de salas "NORMAL"
                        if ($vw2->getId_categoriasala() == \G2\Constante\CategoriaSala::PRR) {

                            $flagDisciplinasPrrDiferentes = 0;

                            //varre o array procurando a disciplina igual a do PRR para adicionar os dados da PRR.
                            foreach ($return as $keyret => $ret) {

                                //varre o array procurando a disciplina igual a do PRR para adicionar os dados da PRR.
                                if ((isset($ret['id_disciplina']) && !empty($ret['id_disciplina']))
                                    && $vw2->getId_disciplina() == $ret['id_disciplina']
                                ) {

                                    $return[$keyret]['st_notaead_prr'] = is_null($vw2->getSt_notaead()) ? '' : (int)$vw2->getSt_notaead();
                                    $return[$keyret]['id_saladeaula_prr'] = is_null($vw2->getId_saladeaula()) ? ''
                                        : (int)$vw2->getId_saladeaula();

                                    /* POR-8
                                    Caso a disciplina PRR seja do tipo TCC, retorna um hífen (-) se não houver nota.
                                    Se não for do tipo TCC, retorna uma string vazia.
                                    Anteriormente, não retornava nada caso a disciplina existisse e não houvesse nota,
                                    causando problemas na manipulação e exibição na nova nota na grade de notas. */

                                    if ($vw2->getId_tipodisciplina() == 2) {
                                        $return[$keyret]['st_notatcc_prr'] = is_null($vw2->getSt_notatcc()) ? '-' : (int)$vw2->getSt_notatcc();
                                    } else {
                                        $return[$keyret]['st_notatcc_prr'] = '';
                                    }
                                    // Se o PRR tiver nota satisfatoria, setar o status da disciplina como satisfatório
                                    if ($vw2->getBl_status() == 1 && $return[$keyret]['bl_status'] == 0) {
                                        $return[$keyret]['st_status'] = $vw2->getSt_status();
                                    }
                                    $return[$keyret]['st_status_prr'] = $vw2->getSt_status();
                                    $return[$keyret]['st_saladeaula_prr'] = $vw2->getSt_saladeaula();
                                    $return[$keyret]['nu_notafinal_prr'] = $vw2->getNu_notafinal();
                                    $return[$keyret]['nu_cargahoraria_prr'] = (int)$vw2->getNu_cargahoraria() ? (int)$vw2->getNu_cargahoraria() : ' - ';
                                    $return[$keyret]['id_disciplina_prr'] = $vw2->getId_disciplina();
                                    $return[$keyret]['st_disciplina_prr'] = $vw2->getSt_disciplina();
                                    $return[$keyret]['id_matriculadisciplina_prr'] = $vw2->getId_matriculadisciplina();
                                    $return[$keyret]['id_tiponotatcc_prr'] = $vw2->getId_tiponotatcc();
                                    $return[$keyret]['id_tiponotafinal_prr'] = $vw2->getId_tiponotafinal();
                                    $return[$keyret]['id_tiponotaead_prr'] = $vw2->getId_tiponotaead();
                                    $return[$keyret]['id_tiponotarecuperacao_prr'] = $vw2->getId_tiponotarecuperacao();
                                    $return[$keyret]['dt_inicio_prr'] = self::dateRender($vw2, 'getDt_inicio');
                                    $return[$keyret]['dt_abertura_prr'] = self::dateRender($vw2, 'getDt_abertura');
                                    $return[$keyret]['dt_encerramento_prr'] = self::dateRender($vw2, 'getDt_encerramento');
                                    $return[$keyret]['url_prr'] = self::verificarSeExisteRelatorioLog(
                                        $vw2->getId_saladeaula(), $vw2->getId_matricula());

                                    $flagDisciplinasPrrDiferentes = 1;


                                    /**
                                     * Dados da alocação com o Moodle
                                     */
                                    if ($retornarNotasMoodle) {
                                        $ali = new AlocacaoIntegracao();
                                        $ali->findOneBy(array('id_alocacao' => $vw2->getId_alocacao()), false, array('dt_cadastro' => 'DESC'));
                                        if ($ali->getSt_codalocacao() && $ali->getId_entidadeintegracao()) {
                                            $notasMoodle = $this->_returnaNotasAVA($ali->getSt_codalocacao(), $ali->getId_entidadeintegracao()->getId_entidadeintegracao(), 'ar_notasmoodle_prr');
                                            $return[$keyret] = array_merge($return[$keyret], $notasMoodle);
                                        }
                                    }


                                }

                            }

                            if ($flagDisciplinasPrrDiferentes == 0) {
                                //Se não tiver disciplinas iguais coloca a disciplina de PRR no array $retorno
                                $return[$key2]['url'] = self::verificarSeExisteRelatorioLog(
                                    $vw->getId_saladeaula(), $vw->getId_matricula()
                                );
                                $return[$key2]['id_saladeaula'] = $vw2->getId_saladeaula();
                                $return[$key2]['st_saladeaula'] = "-";
                                $return[$key2]['id_disciplina'] = $vw2->getId_disciplina();
                                $return[$key2]['dt_inicio'] = '-';
                                $return[$key2]['dt_abertura'] = '-';
                                $return[$key2]['dt_encerramento'] = '-';
                                $return[$key2]['id_projetopedagogico'] = $vw2->getId_projetopedagogico();
                                $return[$key2]['st_projetopedagogico'] = $vw2->getSt_projetopedagogico();
                                $return[$key2]['st_notatcc'] = is_null($vw2->getSt_notatcc()) ? '-' : (int)$vw2->getSt_notatcc();
                                $return[$key2]['nu_notafaltante'] = is_null($vw2->getNu_notafaltante()) ? '0' : (int)$vw2->getNu_notafaltante();
                                $return[$key2]['nu_notamaxima'] = is_null($vw2->getNu_notamaxima()) ? '0' : (int)$vw2->getNu_notamaxima();
                                $return[$key2]['nu_percentualaprovacao'] = is_null($vw2->getNu_percentualaprovacao()) ? '0' : (int)$vw2->getNu_percentualaprovacao();
                                $return[$key2]['st_notaead'] = '-';
                                $return[$key2]['st_notafinal'] = is_null($vw2->getSt_notafinal()) ? '-' : (int)$vw2->getSt_notafinal();
                                $return[$key2]['st_disciplina'] = $vw2->getSt_disciplina();
                                $return[$key2]['id_tipodisciplina'] = $vw2->getId_tipodisciplina();
                                $return[$key2]['st_tituloexibicaodisciplina'] = $vw2->getSt_tituloexibicaodisciplina();
                                $return[$key2]['id_matricula'] = $vw2->getId_matricula();
                                $return[$key2]['nu_notafinal'] = is_null($vw2->getNu_notafinal()) ? '-' : (int)$vw2->getNu_notafinal();
                                $return[$key2]['id_evolucao'] = $vw2->getId_evolucao();
                                $return[$key2]['st_evolucao'] = $vw2->getSt_evolucao();
                                $return[$key2]['id_situacao'] = $vw2->getId_situacao();
                                $return[$key2]['st_situacao'] = $vw2->getSt_situacao();
                                $return[$key2]['nu_cargahoraria'] = (int)$vw2->getNu_cargahoraria() ? (int)$vw2->getNu_cargahoraria() : ' - ';
                                $return[$key2]['nu_notatotal'] = $vw2->getNu_notatotal();
                                $return[$key2]['id_encerramentosala'] = $vw2->getId_encerramentosala();
                                $return[$key2]['st_status'] = $vw2->getSt_status();
                                $return[$key2]['bl_status'] = $vw2->getBl_status();
                                $return[$key2]['id_matriculadisciplina'] = $vw2->getId_matriculadisciplina();
                                $return[$key2]['id_avaliacaoconjuntoreferencia'] = $vw2->getId_avaliacaoconjuntoreferencia();
                                $return[$key2]['id_tiponotatcc'] = null;
                                $return[$key2]['id_tiponotafinal'] = null;
                                $return[$key2]['id_tiponotaead'] = null;
                                $return[$key2]['id_tiponotarecuperacao'] = null;
                                $return[$key2]['bl_complementar'] = $vw2->getBl_complementar();
                                $return[$key2]['dt_encerramentoextensao'] = '-';
                                $return[$key2]['st_notaatividade'] = is_null($vw2->getSt_notaatividade()) ? '-' : (int)$vw2->getSt_notaatividade();
                                $return[$key2]['st_notarecuperacao'] = is_null($vw2->getSt_notarecuperacao()) ? '-' : (int)$vw2->getSt_notarecuperacao();
                                $return[$key2]['dt_cadastroaproveitamento'] = self::dateRender($vw2, 'getDt_cadastroaproveitamento');
                                $return[$key2]['st_nomeusuarioaproveitamento'] = $vw2->getSt_nomeusuarioaproveitamento();
                                $return[$key2]['id_modulo'] = $vw2->getId_modulo();
                                $return[$key2]['st_modulo'] = $vw2->getSt_modulo();
                                $return[$key2]['id_categoriasala'] = $vw2->getId_categoriasala();

                                //avaliacoes graduacao
                                $return[$key2]['st_statusdisciplina'] = is_null($vw2->getSt_statusdisciplina()) ? '-' : $vw2->getSt_statusdisciplina();
                                $return[$key2]['st_avaliacaoatividade'] = $vw2->getSt_avaliacaoatividade();
                                $return[$key2]['st_avaliacaoead'] = $vw2->getSt_avaliacaoead();
                                $return[$key2]['st_avaliacaofinal'] = $vw2->getSt_avaliacaofinal();
                                $return[$key2]['st_avaliacaorecuperacao'] = $vw2->getSt_avaliacaorecuperacao();

                                $return[$key2]['st_nomeusuarioaproveitamento'] = $vw2->getSt_nomeusuarioaproveitamento();
                                $return[$key2]['dt_cadastroaproveitamento'] = $vw2->getDt_cadastroaproveitamento() instanceof \DateTime ? $vw2->getDt_cadastroaproveitamento()->format("d/m/Y") : $vw2->getDt_cadastroaproveitamento();
                                $return[$key2]['id_aproveitamento'] = is_null($vw2->getId_aproveitamento()) ? NULL : (int)$vw2->getId_aproveitamento();
                                $return[$key2]['st_disciplinaoriginal'] = is_null($vw2->getSt_disciplinaoriginal()) ? '-' : $vw2->getSt_disciplinaoriginal();

                                //dados da PRR
                                $return[$key2]['st_notaead_prr'] = is_null($vw2->getSt_notaead()) ? '' : (int)$vw2->getSt_notaead();
                                $return[$key2]['st_notatcc_prr'] = is_null($vw2->getSt_notatcc()) ? '' : (int)$vw2->getSt_notatcc();
                                $return[$key2]['st_saladeaula_prr'] = $vw2->getSt_saladeaula();
                                $return[$key2]['nu_notafinal_prr'] = $vw2->getNu_notafinal();
                                $return[$key2]['nu_cargahoraria_prr'] = (int)$vw2->getNu_cargahoraria() ? (int)$vw2->getNu_cargahoraria() : ' - ';
                                $return[$key2]['id_disciplina_prr'] = $vw2->getId_disciplina();
                                $return[$key2]['st_disciplina_prr'] = $vw2->getSt_disciplina();
                                $return[$key2]['id_matriculadisciplina_prr'] = $vw2->getId_matriculadisciplina();
                                $return[$key2]['dt_inicio_prr'] = self::dateRender($vw2, 'getDt_inicio');
                                $return[$key2]['dt_abertura_prr'] = self::dateRender($vw2, 'getDt_abertura');
                                $return[$key2]['dt_encerramento_prr'] = self::dateRender($vw2, 'getDt_encerramento');
                                $return[$key2]['id_tiponotatcc_prr'] = $vw2->getId_tiponotatcc();
                                $return[$key2]['id_tiponotafinal_prr'] = $vw2->getId_tiponotafinal();
                                $return[$key2]['id_tiponotaead_prr'] = $vw2->getId_tiponotaead();
                                $return[$key2]['id_tiponotarecuperacao_prr'] = $vw2->getId_tiponotarecuperacao();
                                $return[$key2]['st_status_prr'] = $vw2->getSt_status();
                                $return[$key2]['url_prr'] = self::verificarSeExisteRelatorioLog(
                                    $vw2->getId_saladeaula(), $vw2->getId_matricula()
                                );
                            }
                        }
                    }
                }

                ksort($return);

                // gera a lista de disciplinas separadas por módulos,
                // caso ele tenha só um módulo como na pos-graduação
                // ele vai deixar todas disciplinas num módulo só.
                foreach ($arr as $keymod => $mod) {
                    foreach ($return as $rtn) {
                        if (@$mod['id_modulo'] == @$rtn['id_modulo']) {
                            $arr[$keymod]['disciplinas'][] = $rtn;
                            # atribui titulo ao módulo complementar
                            if (empty($mod['id_modulo']) && empty($mod['st_modulo'])) {
                                $arr[$keymod]['st_modulo'] = self::LABEL_MODULO_COMPLEMENTAR;
                            }
                        }

                        # atribui titulo ao módulo complementar
                        if (empty($mod['id_modulo']) && empty($mod['st_modulo'])) {
                            $arr[$keymod]['st_modulo'] = self::LABEL_MODULO_COMPLEMENTAR;
                        }
                    }
                }
            }

            return $arr;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Não foi possível retornar a grade de notas agrupada por módulos. " . $e->getMessage());
        }
    }

    public function retornaVwGradeNotaPorStatusDisciplina($params = null)
    {
        $result = $this->toArrayEntity($this->findByVwGradeNota($params, array('dt_abertura' => 'DESC')));
        return $result;
    }

    private function verificarSeExisteRelatorioLog($idSaladeaula, $idMatricula)
    {
        $usuario = $this->findOneBy('\G2\Entity\VwMatricula', array('id_matricula' => $idMatricula));
        $parametrosUrl = $this->em->getRepository('\G2\Entity\Matricula')
            ->retornaParametroMoodle($idSaladeaula, $usuario->getId_usuario());
        $url = null;
        if ($parametrosUrl) {
            return true;
        }
        return false;
    }

    /**
     * Método que busca os valores da VwGradeNota com base no id_matricula e outros parâmetros conforme a necessidade.
     * @param array $params - Parâmetros de busca.
     * @param array $orderBy - Parâmetros de ordenação no formato array('campo_a_ser_ordenado' => 'ASC|DESC').
     * @return array|string
     */
    public function findByVwGradeNota($params = array(), $orderBy = null)
    {
        try {
            if (!array_key_exists('id_matricula', $params) || empty($params['id_matricula'])) {
                throw new \Exception('O id_matricula é obrigatório e não foi passado');
            }
            $repo = $this->em->getRepository('\G2\Entity\VwGradeNota');

            $query = $repo->createQueryBuilder('m');

            $query->where('m.id_matricula = :id_matricula')
                ->setParameter('id_matricula', $params['id_matricula']);

            if (isset($params['id_categoriasala'])) {
                $query->andWhere('m.id_categoriasala = '
                    . $params['id_categoriasala']
                    . ($params['id_categoriasala'] == 1 ? ' OR m.id_categoriasala is null' : ''));
            }

            if (!empty($params['id_alocacao'])) {
                $query->andWhere('m.id_alocacao = :id_alocacao')->setParameter('id_alocacao', $params['id_alocacao']);
            }

            if (!empty($params['id_disciplina'])) {
                $query->andWhere('m.id_disciplina = :id_disciplina')->setParameter('id_disciplina', $params['id_disciplina']);
            }

            /* Caso não tenha sido passado o array $orderBy com as especificações de ordenação, utiliza a ordenação
            previamente implementada neste método. */

            if (is_null($orderBy)) {
                $query->orderBy('m.st_modulo', 'ASC')
                    ->addOrderBy('m.dt_abertura', 'ASC')
                    ->addOrderBy('m.id_tipodisciplina', 'ASC');
            }

            if (is_array($orderBy) && !empty($orderBy)) {
                foreach ($orderBy as $sort => $order) {
                    $query->addOrderBy('m.'. $sort, $order);
                }
            }

            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Consultra as Matrículas que podem receber o Arquivo de contrato
     * @param array $params
     * @return Object
     * @throws \Zend_Exception
     */
    public function consultaVwMatriculaContrato(array $params)
    {
        try {

            if (!isset($params['id_usuario']))
                throw new \Exception("Usuário não informado!");

            $params['id_entidadematricula'] = $this->sessao->id_entidade;
            $matriculas = $this->findBy('\G2\Entity\VwMatricula', $params);
            $array = array();
            if ($matriculas) {
                $x = 0;
                foreach ($matriculas as $matricula) {
                    if ($matricula->getid_venda()) {
                        $array[$x] = $matricula->tobackbonearray();
                        $array[$x]['st_labelselect'] = $matricula->getid_matricula() . '-' . $matricula->getst_projetopedagogico();
                        $x++;
                    }
                }
            }

            return $array;

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_matricula de acordo com os parametros passado
     * @param array $params
     * @return \G2\Entity\VwMatricula
     * @throws \Zend_Exception
     */
    public function consultaVwMatriculaJson(array $params)
    {
        try {
            if ($params['dt_inicio'] || $params['dt_termino']) {
                $params['dt_inicio'] = $this->converteDataBanco($params['dt_inicio'] . ' 00:00:00');
                $params['dt_termino'] = $this->converteDataBanco($params['dt_termino'] . ' 23:59:59');
            }
            $result = $this->em->getRepository('\G2\Entity\VwMatricula')->retornaAlunosProjetoPedagogico($params);
            $return = array();
            foreach ($result as $key => $value) {
                $return[$key] = $this->toArrayEntity($value);
                $return[$key]['dt_terminomatricula'] = $value->getDt_terminomatricula() ? date('d/m/Y', strtotime($value->getDt_terminomatricula()->format('d-m-Y H:i:s'))) : null;
                $return[$key]['dt_cadastro'] = date('d/m/Y', strtotime($value->getDt_cadastro()));
                $return[$key]['bl_prorroga'] = true;
            }

            return $return;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    /**
     * Retorna se matricula esta ativa ou inativa
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaMatriculaValida(array $params)
    {
        try {
            //Retorna matricula
            $matricula = $this->em->getRepository('\G2\Entity\VwMatricula')->retornaMatriculaValida($params);

            $arrReturn = array();
            if ($matricula) {
                if ($matricula['id_situacao'] == 50) { //matricula ativa
                    $arrReturn = array(
                        'situacao' => $matricula['st_situacao'],
                        'evolucao' => $matricula['st_evolucao']
                    );
                } else {
                    $arrReturn = array(
                        'situacao' => $matricula['st_situacao'],
                        'evolucao' => $matricula['st_evolucao']
                    );
                }
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    /**
     * Recupera da dos da Matricula, Contratato e altera Evolução da venda para cancelada
     * @param array $matricula
     * @throws \Zend_Exception
     */
    public function recuperaDadosMatriculaAssinanteCancelaVenda(array $arrMatricula)
    {
        try {
            //verifica se id_matricula existe no array
            if (!$arrMatricula['id_matricula']) {
                throw new \Exception("Não foi possível recuperar dados de matricula.");
            }
            //consulta dados da matricula
            $resultMatricula = $this->consultaVwMatricula(array('id_matricula' => $arrMatricula['id_matricula']));

            //Verifica se achou uma matricula
            if ($resultMatricula) {
                //instancia a negocio de Contrato
                $contratoNegocio = new Contrato();

                $this->beginTransaction(); //inicia a transação
                //percorre o resultado das matriculas
                foreach ($resultMatricula as $matricula) {
                    //verifica se a matricula tem um registro de venda produto
                    if ($matricula->getId_vendaproduto()) {
                        //procura o produto relacionado a venda produto e retorna se ele é do tipo Assinatura
                        $vendaProduto = $this->retornaProdutoAssociadoByVendaProduto($matricula->getId_vendaproduto());
                        //se retorna o produto
                        if ($vendaProduto) {
                            //procura o contrato
                            $resultContrato = $contratoNegocio->findContrato($matricula->getId_contrato());
                            //verifica se achou o contrato
                            if ($resultContrato) {
                                //verifica se existe venda relacionada
                                if ($resultContrato->getId_venda()) {
                                    //passa o objeto de venda para setar os parametros e salvar
                                    $resultSave = $this->cancelarVenda($resultContrato->getId_venda());
                                    //verifica se ocorreu o erro e gera um exception
                                    if (!$resultSave) {
                                        throw new \Exception("Erro ao cancelar Venda.");
                                    }
                                }
                            }
                        }
                    }
                }
                $this->commit(); //commit
                return $resultMatricula; //retorna
            }
        } catch (\Exception $ex) {
            $this->rollback(); //rollback
            throw new \Zend_Exception("Erro ao recuperar informações da Matricula/Contrato e Cancelar Venda. " . $ex->getMessage());
        }
    }

    /**
     * Retorna dados da vw_matricula de acordo com os parametros passado
     * @param array $params
     * @return \G2\Entity\VwMatricula
     * @throws \Zend_Exception
     */
    public function consultaVwMatricula(array $params)
    {
        try {
            return $this->findBy('\G2\Entity\VwMatricula', $params);

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados do produto caso ele seja um produto do modelo "Assinatura"
     * @param integer $idVendaProduto
     * @return \G2\Entity\Produto
     * @throws \Zend_Exception
     */
    private function retornaProdutoAssociadoByVendaProduto($idVendaProduto)
    {
        try {
            $vendaNegocio = new Venda(); //instancia a venda
            $vendaProduto = $vendaNegocio->findVendaProduto($idVendaProduto); //procura o produto venda
            if ($vendaProduto) {
                //verifica se tem produto relacionado
                if ($vendaProduto->getId_produto()) {
                    //verifica se o modelo da venda é do tipo Assinatura, id = 2
                    if ($vendaProduto->getId_produto()->getId_modelovenda()->getId_modelovenda() == 2) {
                        return $vendaProduto->getId_produto();
                    }
                }
            }
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao retornar dados de Produto. " . $ex->getMessage());
        }
    }

    /**
     * Seta o id_evolucao para cancelado, e salva a venda
     * @param \G2\Entity\Venda $venda
     * @return \G2\Entity\Venda
     * @throws \Zend_Exception
     */
    private function cancelarVenda($venda)
    {
        try {
            if ($venda->getIdVenda()) {
                $vendaNegocio = new Venda();
                $data['id'] = $venda->getIdVenda();
                $data['id_evolucao'] = Evolucao::TB_VENDA_CANCELADA; //Id evolução 8 = Cancelada, Tabela Evolução
                return $vendaNegocio->salvarVenda($data);
            }
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao cancelar venda. " . $ex->getMessage());
        }
    }

    /**
     * Retorna Dados de Contrato Matricula
     * @param array $where
     * @return \G2\Entity\ContratoMatricula
     * @throws \Zend_Exception
     */
    public function retornaContratoMatricula(array $where)
    {
        try {
            return $this->findBy('\G2\Entity\ContratoMatricula', $where);
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao consultar dados de Contrato Matricula. " . $ex->getMessage());
        }
    }

    /**
     * Retor
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaStatusMatriculaAssinante(array $params)
    {
        try {
            //            if ($params['id_usuario']) {
            //                $idUsuario = $params['id_usuario'];
            //            }
            if ($params['id_matricula']) {
                $idMatricula = $params['id_matricula'];
            }
            $matricula = $this->em->getRepository('\G2\Entity\VwMatricula')
                ->retornaMatriculaAssinatura($idMatricula);
            if ($matricula) {
                return array(
                    'id_matricula' => $matricula['id_matricula'],
                    'id_situacao' => $matricula['id_situacao'],
                    'st_situacao' => $matricula['st_situacao'],
                    'id_evolucao' => $matricula['id_evolucao'],
                    'st_evolucao' => $matricula['st_evolucao'],
                    'dt_terminomatricula' => $matricula['dt_terminomatricula']
                );
            }

        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao retornar dados da Matricula. " . $ex->getMessage());
        }

    }

    public function corrigirMatriculaDisciplina($id_projetopedagogico)
    {
        try {
            /** @var \G2\Repository\Matricula $rep */
            $rep = $this->em->getRepository('\G2\Entity\VwMatricula');
            return $rep->corrigirMatriculaDisciplina($id_projetopedagogico);
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao retornar dados da Matricula. " . $ex->getMessage());
        }

    }

    public function alterarEvolucao($dados)
    {
        $to = new \MatriculaTO();
        $bo = new \SecretariaBO();
        $to->setId_matricula($dados['id_matricula']);

        $arrdatas[] = $dados['dt_alteracao']['dt_abertura'];
        $arrdatas[] = $dados['dt_alteracao']['dt_encerramento'];
        $toOcorrencia = new \OcorrenciaTO();
        $mensageiro = $bo->alterarMatricula($to, $dados['st_acao'], $dados['st_motivo'], $toOcorrencia, $arrdatas);

        return $mensageiro;
    }

    public function retornaVwUsuarioMatriculaProjeto($where)
    {
        return $this->findBy('\G2\Entity\VwUsuarioMatriculaProjeto', $where);
    }


    /**
     * Retorna entity TbAlocacao
     * @param array $params
     * @param array $order
     * @return \G2\Entity\Alocacao[]
     * @throws \Zend_Exception
     */
    public function findByAlocacao(array $params, array $order = array())
    {
        try {
            return $this->findBy('G2\Entity\Alocacao', $params, $order);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados da alocação. " . $e->getMessage());
        }
    }

    /**
     * @class \G2\Negocio\Matricula
     * @param \ContratoTO $contratoTO
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function ativarMatricula(\ContratoTO $contratoTO)
    {
        $matriculaBO = new \MatriculaBO();
        return $matriculaBO->ativarMatricula($contratoTO);
    }

    /**
     * Retorna pesquisa dos alunos para o pelo nome
     * @param array $params
     * @return bool
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisarAlunosMatricula($params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwMatricula');
            $array_search = array(
                'vw.id_matricula',
                'vw.st_nomecompleto',
                'vw.st_projetopedagogico',
                'vw.id_projetopedagogico',
                'vw.dt_inicio',
                'vw.st_turma',
                'vw.st_situacao',
                'vw.st_evolucao',
                'vw.st_entidadematriz'
            );

            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->where('1=1')
                ->orderBy('vw.st_nomecompleto');

            $where = array();
            if (isset($params['st_dadopesquisa']) && !empty($params['st_dadopesquisa'])) {
                $where[] = "vw.st_nomecompleto LIKE '%" . $params['st_dadopesquisa'] . "%'";
                $where[] = "vw.st_email LIKE '%" . $params['st_dadopesquisa'] . "%'";
                $query->andWhere(implode(" OR ", $where));
            }

            if ($params['id_esquemaconfiguracao'] == \G2\Constante\EsquemaConfiguracao::Graduacao_AVM) {
                $query->andWhere("vw.id_esquemaconfiguracao = 1");
            } else {
                $query->andWhere("vw.id_entidadeatendimento = :id_entidadeatendimento")
                    ->setParameter("id_entidadeatendimento", $params['id_entidadeatendimento']);
            }

            $result = $query->getQuery()->getResult();
            return $result;

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * Retorna pesquisa dos alunos para o pelo nome ou e-mail, de acordo com o perfil relacionado ao usuário logado
     * Utilizado na ferramenta de pesquisar alunos do portal
     * @param array $params
     * @return bool
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function pesquisarAlunosMatriculaPerfil($params)
    {
        try {
            $repo = $this->em->getRepository('G2\Entity\VwAlunosVinculoPerfil');
            $array_search = array('vw.id_matricula', 'vw.st_nomecompleto', 'vw.st_projetopedagogico', 'vw.st_turma',
                'vw.id_projetopedagogico', 'vw.st_entidadematriz', 'vw.dt_inicio', 'vw.st_situacao', 'vw.st_evolucao');

            $query = $repo->createQueryBuilder("vw")
                ->select($array_search)
                ->where('1=1')
                ->orderBy('vw.st_nomecompleto');

            $where = array();
            if (isset($params['st_dadopesquisa']) && !empty($params['st_dadopesquisa'])) {
                $where[] = "vw.st_nomecompleto LIKE '%" . $params['st_dadopesquisa'] . "%'";
                $where[] = "vw.st_email LIKE '%" . $params['st_dadopesquisa'] . "%'";
                $query->andWhere(implode(" OR ", $where));
            }

            if ($params['id_esquemaconfiguracao'] == \G2\Constante\EsquemaConfiguracao::Graduacao_AVM) {
                $query->andWhere("vw.id_esquemaconfiguracao = 1");
            } else {
                $query->andWhere("vw.id_entidade = :id_entidade")
                    ->setParameter("id_entidade", $params['id_entidade']);
            }

            $query->andWhere("vw.id_usuarioperfil = :id_usuarioperfil")
                ->setParameter("id_usuarioperfil", $params['id_usuarioperfil']);

            $query->andWhere("vw.id_perfilpedagogico = :id_perfilpedagogico")
                ->setParameter("id_perfilpedagogico", $params['id_perfilpedagogico']);

            $result = $query->getQuery()->getResult();
            return $result;
        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * Salva nova data de termino da matricula e do contrato
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function alterarDataTermino(array $data)
    {
        try {
            //instancia a classes para auxiliar
            $matriculaDAO = new \MatriculaDAO();
            $contratoNegocio = new Contrato();
            $mensageiro = new \Ead1_Mensageiro();
            $matriculaDAO->beginTransaction();//abre transação
            //verifica se veio o id_matricula se não veio gera um exception
            if (!$data['id_matricula']) {
                throw new \Exception("Id da matrícula não informado.");
            }
            $id_matricula = $data['id_matricula'];//armazena o id_matricula numa variavel

            //força a evolução da matricula pra cursando
            $data['id_evolucao'] = \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
            $data['id_situacao'] = \G2\Constante\Situacao::TB_MATRICULA_ATIVA;

            //salva matricula
            $resultMatricula = $this->salvarMatricula($data);
            //verifica se houve erro ao salvar matricula
            if ($resultMatricula->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception("Erro ao salvar matricula. " . $resultMatricula->getText());
            }
            //busca o contrato da matricula
            $contrato = $contratoNegocio->findContratoAtivoByMatricula($id_matricula);
            //verifica se não encontrou
            if (!$contrato) {
                throw new \Exception("Contrato da matricula não encontrado.");
            }
            //transforma o objeto doctrine em um array
            $contratoArr = $this->toArrayEntity($contrato);
            $contratoArr['dt_ativacao'] = $contrato->getDt_ativacao();
            $contratoArr['dt_termino'] = $this->converteDataBanco($data['dt_termino']);//altera a data de termino do contrato
            $contratoArr['id_evolucao'] = \G2\Constante\Evolucao::TB_CONTRATO_CONFIRMADO;//força a evolução
            $contratoArr['id_situacao'] = \G2\Constante\Situacao::TB_CONTRATO_ATIVO;//força a situação
            $contratoArr['id_venda'] = $contrato->getId_venda() ? $contrato->getId_venda()->getIdVenda() : NULL;
            $contratoArr['id_contratoregra'] = $contrato->getId_contratoregra() ? $contrato->getId_contratoregra()->getId_contratoregra() : NULL;

            //salva o contrato
            $resultContrato = $contratoNegocio->salvarContrato($contratoArr);

            //verifica se houve erro ao salvar o contrato
            if ($resultContrato->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception("Erro ao salvar contrato. " . $resultContrato->getText());
            }


            $matriculaDAO->commit();//commit
            //seta o mensageiro
            $mensageiro->setMensageiro("Data de término da matricula atualizada com sucesso!", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $matriculaDAO->rollBack();//rollback
            //seta o mensageiro com a mensagem de erro
            $mensageiro->setMensageiro("Erro ao tentar alterar data de término da matricula."
                . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;//retorna o mensageiro
    }

    /**
     * Salva os dados da matricula
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function salvarMatricula(array $data)
    {
        try {
            $matriculaTO = new \MatriculaTO();
            $matriculaTO->montaToDinamico($data);
            $bo = new \MatriculaBO();

            if ($matriculaTO->getId_matricula()) {
                $result = $bo->editarMatricula($matriculaTO);
            } else {
                $result = $bo->cadastrarMatricula($matriculaTO);
            }

            if ($result->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                return $result;
            } else {
                throw new \Exception($result->getText());
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados da matricula. " . $e->getMessage());
        }
    }

    /**
     * Metodo que retorna dados da vw_gradenota usando findBy basico padrao doctrine
     * @param array $where
     * @param array $order
     * @param array $limit
     * @return array
     */
    public function findVwGradeNota(array $where, array $order = null, $limit = null, $offset = null)
    {
        $result = $this->findBy('\G2\Entity\VwGradeNota', $where, $order, $limit, $offset);
        return $result;
    }

    /**
     * @param array $where
     * @param array|null $order
     * @return mixed
     */
    public function findVwGradeNotaWithInsentos(array $where, array $order = null)
    {
        $result = $this->em->getRepository('\G2\Entity\VwGradeNota')->returnGradeWithInsentos($where, $order);
        return $result;
    }

    /**
     * @return array|string
     */
    public function comboProjetoMatriculaInstitucional()
    {
        try {
            $repo = $this->em->getRepository('\G2\Entity\VwProdutoProjetoTipoValor');
            $query = $repo->createQueryBuilder("vw")
                ->select('vw.id_produto', 'vw.id_projetopedagogico', 'vw.st_projetopedagogico')
                ->distinct('vw.id_projetopedagogico')
                ->where('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);

            $result = $query->getQuery()->getResult();


            return $result;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Verifica se os dados foram passados e monta os parametros da consulta
     * @param $idAluno
     * @param $idEntidade
     * @param array $params
     * @return array
     * @throws \Exception
     */
    private function setParamsRetornarCursoByAluno($idAluno, $idEntidade, array $params)
    {
        //verifica o id do aluno
        if (!$idAluno) {
            throw new \Exception("Não é possível consultar cursos sem informar o id do aluno.");
        }

        //verifica o id da entidade
        if (!$idEntidade) {
            throw new \Exception("Não é possível consultar cursos sem informar o id da entidade.");
        }

        //mescla o array de parametros default com o array que sera passado via parametro
        return array_merge($params, array(
            'bl_ativo' => true,
            'id_evolucao' => array(
                \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO,
                \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE
            ),
            'id_situacao' => \G2\Constante\Situacao::TB_MATRICULA_ATIVA,
            'bl_acessoapp' => true,
            'id_usuario' => (int)$idAluno,
            'id_entidade' => (int)$idEntidade
        ));
    }

    /**
     * Verifica se é para mostrar o primeiro acesso de acordo com a matricula
     * @param $idMatricula
     * @return bool
     */
    private function hasPrimeiroAcesso($idMatricula)
    {
        $venda = $this->getRepository(\G2\Entity\Matricula::class)->retornaVendaMatricula([
            'id_matricula' => $idMatricula
        ]);

        $pesquisaContratoMatricula = (new ContratoMatricula())
            ->retornaContratoMatricula(['id_matricula' => $idMatricula], true);

        if ($pesquisaContratoMatricula instanceof \Ead1_Mensageiro) {
            $pesquisaContratoMatricula = $pesquisaContratoMatricula->getFirstMensagem();
        }

        $marcacaoEtapa = (new PaMarcacaoEtapa())->retornaPaMarcacaoEtapa(['id_venda' => $venda['id_venda']]);

        $configuracaoTutorial = (new \G2\Negocio\EsquemaConfiguracao())
            ->retornarConfiguracaoPrimeiroAcesso($venda['id_entidadematricula']);

        $bl_primeiroacesso = false;
        if ((empty($pesquisaContratoMatricula['id_contrato'])
                || $marcacaoEtapa->getTipo() == \Ead1_IMensageiro::AVISO)
            && !$venda['bl_institucional']
            && $configuracaoTutorial
        ) {
            $bl_primeiroacesso = true;
        }

        return $bl_primeiroacesso;
    }

    /**
     * Retorna os cursos vinculados ao aluno
     * ESTE MÉTODO É UTILIZADO PARA O MOBILE
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @param $idAluno
     * @param $idEntidade
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornarCursosByAluno($idAluno, $idEntidade, array $params = [])
    {
        try {

            $params = $this->setParamsRetornarCursoByAluno($idAluno, $idEntidade, $params);
            //busca os dados no banco
            $result = $this->em->getRepository('G2\Entity\VwMatricula')->retornarCursosAlunos($params);

            //monta o array vazio para o retorno
            $arrReturn = [];
            //verifica se tem resultado
            if ($result) {
                //percorre a variavel do resultado
                $url = \Ead1_Ambiente::geral()->st_url_gestor2;
                foreach ($result as $row) {
                    $img = $url . ($row['st_urlimgapp'] ? $row['st_urlimgapp'] : $row['st_urlimglogo']);

                    $bl_primeiroacesso = $this->hasPrimeiroAcesso($row['id_matricula']);

                    //verifica se não existe a posição no array
                    if (!isset($arrReturn[$row['id_entidadematricula']])) {
                        //monta o array
                        $arrReturn[$row['id_entidadematricula']] = [
                            'id_entidade' => $row['id_entidadematricula'],
                            'st_entidade' => $row['st_entidadematricula'],
                            'st_logo' => $img,
                            'st_urlsite' => trim(str_replace(['http://', 'https://'], null, $row['st_urlsite'])),
                            'cursos' => [],
                            'bl_primeiroacesso' => $bl_primeiroacesso
                        ];
                    }

                    //monta as outras posições no array com os cursos
                    $arrReturn[$row['id_entidadematricula']]['cursos'][] = [
                        'id_curso' => $row['id_projetopedagogico'],
                        'st_curso' => $row['st_tituloexibicao'],
                        'id_matricula' => $row['id_matricula'],
                        'dt_inicioturma' => $row['dt_inicioturma'] ? $row['dt_inicioturma'] : NULL,
                        'st_imagemarea' => $row['st_imagemarea'] ? $url . $row['st_imagemarea'] : NULL,
                        'id_entidade' => $row['id_entidadematricula'],
                        'st_descricao' => $row['st_descricao'],
                        'st_imagem' => $row['st_imagem'],
                        'bl_primeiroacesso' => $bl_primeiroacesso,
                        'st_chaveacesso' => $row['st_chaveacesso'],
                        'st_conexao' => $row['st_conexao'],
                        'bl_atendimentovirtual' => $row['bl_atendimentovirtual']

                    ];

                }
                sort($arrReturn);
            }

            //retorna o array
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao retornar cursos vinculados ao aluno. " . $e->getMessage());
        }
    }

    /**
     * Salva nova data de termino da matricula e do contrato
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function alterarDataTerminoCombo(array $data)
    {
        try {
            //instancia a classes para auxiliar
            $matriculaDAO = new \MatriculaDAO();
            $mensageiro = new \Ead1_Mensageiro();

            $matriculaDAO->beginTransaction();//abre transação

            //faz o foreache com todas as pessoas recebendo alterando a data termino recebida
            foreach ($data['collection'] as $value) {
                if ($value['bl_prorroga'] === 'true') {
                    //Adiciona os dias na matricula
                    $d = $data['nu_dias'];
                    $dt_termino = $value['dt_terminomatricula'];
                    $dt_final = $this->somarData($dt_termino, $d);
                    unset($value['dt_termino']);
                    $value['dt_termino'] = $this->converteDataBancoZendDate($dt_final);
                    //verifica se veio o id_matricula se não veio gera um exception
                    if (!$value['id_matricula']) {
                        throw new \Exception("Id da matrícula não informado.");
                    }
                    //força a evolução da matricula pra cursando
                    $value['id_evolucao'] = \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
                    $value['id_situacao'] = \G2\Constante\Situacao::TB_MATRICULA_ATIVA;

                    //salva matricula
                    $resultMatricula = $this->salvarMatricula($value);

                    //Salvar Alocação

                    $objetoMatriculaDisciplina = $this->findBy('\G2\Entity\MatriculaDisciplina', array('id_matricula' => $value['id_matricula']));
                    if ($objetoMatriculaDisciplina) {
                        foreach ($objetoMatriculaDisciplina as $omd) {
                            $objetoAlocacao = $this->findOneBy('\G2\Entity\Alocacao', array('id_matriculadisciplina' => $omd->getId_matriculadisciplina(), 'bl_ativo' => true));
                            if ($objetoAlocacao) {
                                $objetoAlocacao->setNu_diasextensao($data['nu_dias']);
                                $this->save($objetoAlocacao);
                            }
                        }
                    }


                    //verifica se houve erro ao salvar matricula
                    if ($resultMatricula->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                        throw new \Exception("Erro ao salvar matricula. " . $resultMatricula->getText());
                    }

                }
            }
            $matriculaDAO->commit();//commit

            //seta o mensageiro
            $mensageiro->setMensageiro("Data de término prorrogada com sucesso!", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {

            $matriculaDAO->rollBack();//rollback
            //seta o mensageiro com a mensagem de erro
            $mensageiro->setMensageiro("Erro ao tentar alterar data de término da matricula." . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;//retorna o mensageiro
    }

    /**
     * Insere os dias de extensão para a funcionalidade de prorrogação de sala de aula
     *
     * @funcionalidade: Prorrogação de Sala de Aula
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function alteraDataTerminoSala(array $data)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            $this->beginTransaction();
            foreach ($data['collection'] as $value) {
                /*
                * Estende os dias incluindo a quantidade de dias na tb_alocação
                */
                if ($value['bl_prorroga'] === 'true') {
                    $alocacao = $this->find('\G2\Entity\Alocacao', $value['id_alocacao']);
                    $alocacao->setNu_diasextensao($data['nu_dias']);
                    $this->save($alocacao);
                }
            }
            $this->commit();

            //seta o mensageiro
            $mensageiro->setMensageiro("Data de estendida com sucesso!", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            $mensageiro->setMensageiro("Erro ao tentar estender a data." . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        try {
            //instancia a classes para auxiliar
            $matriculaDAO = new \MatriculaDAO();
            $mensageiro = new \Ead1_Mensageiro();

            $matriculaDAO->beginTransaction();//abre transação

            //faz o foreache com todas as pessoas recebendo alterando a data termino recebida
            foreach ($data['collection'] as $value) {
                if ($value['bl_prorroga'] === 'true') {
                    //Adiciona os dias na matricula
                    $d = $data['nu_dias'];
                    $dt_termino = $value['dt_terminomatricula'];
                    $dt_final = $this->somarData($dt_termino, $d);
                    unset($value['dt_termino']);
                    $value['dt_termino'] = $this->converteDataBancoZendDate($dt_final);

                    //verifica se veio o id_matricula se não veio gera um exception
                    if (!$value['id_matricula']) {
                        throw new \Exception("Id da matrícula não informado.");
                    }

                    //força a evolução da matricula pra cursando
                    $value['id_evolucao'] = \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO;
                    $value['id_situacao'] = \G2\Constante\Situacao::TB_MATRICULA_ATIVA;
                    $dados = array(
                        'id_matricula' => $value['id_matricula'],
                        'dt_termino' => $value['dt_termino'],
                        'id_situacao' => $value['id_situacao'],
                        'id_evolucao' => $value['id_evolucao'],
                    );

                    //salva matricula
                    $resultMatricula = $this->salvarMatricula($dados);

                    //verifica se houve erro ao salvar matricula
                    if ($resultMatricula->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                        throw new \Exception("Erro ao salvar matricula. " . $resultMatricula->getText());
                    }

                }
            }

            $matriculaDAO->commit();//commit

            $mensageiro->setMensageiro("Data de matricula prorrogada com sucesso!", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {

            $matriculaDAO->rollBack();//rollback
            //seta o mensageiro com a mensagem de erro
            $mensageiro->setMensageiro("Erro ao tentar alterar data de término da matricula." . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Retorna os dados do resumo financeiro
     * @param array $param parametros para consulta
     * @param bool $convertDate Parametro com default true para converter as datas para o formato
     * pt_br (Este parametro foi adicionado para retornar o resumo financeiro no app e novo portal)
     * @return \Ead1_Mensageiro
     */
    public function findVwResumoFinanceiro(array $param, $convertDate = true)
    {
        try {
            $bo = new \MatriculaBO();
            if (!empty($param['id_matricula']) || isset($param['id_venda'])) {
                if (isset($param['id_venda'])) {
                    $id_venda = $param['id_venda'];
                } else {
                    $mensageiro = $bo->retornarIdVendaMatricula($param['id_matricula']);
                    $id_venda = null;

                    if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                        $retorno = $mensageiro->getFirstMensagem();
                        $id_venda = $retorno['id_venda'];
                    } else {
                        return new \Ead1_Mensageiro('Nenhuma venda localizada para esta matrícula.', \Ead1_IMensageiro::AVISO);
                    }
                }

                $where = ' vw.id_venda = ' . $id_venda;
                if (isset($param['id_usuariolancamento']) && $param['id_usuariolancamento']) {
                    $where .= ' AND vw.id_usuario = ' . $param['id_usuariolancamento'];
                }

                if (isset($param['id_lancamento']) && $param['id_lancamento']) {
                    $where .= ' AND vw.id_lancamento = ' . $param['id_lancamento'];
                }

                if (isset($param['id_statuslancamento']) && $param['id_statuslancamento']) {
                    $where .= ' AND vw.id_statuslancamento = ' . $param['id_statuslancamento'];
                }

                if (isset($param['bl_original']) && (int)$param['bl_original']) {
                    //Se for bl_original, vai trazer somente as originais, independentes do bl_ativo
                    $where .= ' AND vw.bl_original = 1';
                } else {
                    //Se não for bl_original, vai trazer somente as bl_ativo = true e independente do bl_original
                    $where .= ' AND vw.bl_ativo = 1';
                }

                $sql = 'SELECT vw FROM G2\Entity\VwResumoFinanceiro vw WHERE ' . $where . ' ORDER BY vw.nu_parcela ASC';
                $query = $this->em->createQuery($sql);
                $vws = $query->getResult();

                $vendaMatricula = null;
                if (\Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula) {
                    $sql = 'SELECT vp FROM G2\Entity\VendaProduto vp WHERE vp.id_matricula = ' . \Ead1_Sessao::getSessaoProjetoPedagogico()->id_matricula;
                    $query = $this->em->createQuery($sql);
                    $vendaMatricula = $query->getResult();
                }

                /* $nu_ordemparcela é inicializada com 2,
                * já que a parcela de entrada é considerada
                * como "parcela 1". */

                $nu_ordemparcela = 2;
                foreach ($vws as $key => $vw) {
                    $vws[$key] = $this->toArrayEntity($vw);
                    if ($vendaMatricula)
                        $vws[$key]['id_vendamatricula'] = $vendaMatricula[0]->getId_venda()->getIdVenda();

                    //Verificando se e um lancamento oriundo de transferencia
                    if (!empty($vws[$key]['id_vendaaditivo'])) {
                        $vws[$key]['st_meiopagamento'] = $vws[$key]['st_meiopagamento'] . ' - Transferência de Curso';
                    }

                    if (!empty($vws[$key]['id_acordo'])) {
                        $vws[$key]['st_meiopagamento'] = $vws[$key]['st_meiopagamento'] . ' - Acordo';
                    }

                    $vws[$key]['nu_ordemparcela'] = $vws[$key]['bl_entrada'] ? 'Entrada' : $nu_ordemparcela++;

                    if ($vws[$key]['id_meiopagamento'] == \MeioPagamentoTO::RECORRENTE) {
                        $vws[$key]['st_codtransacaooperadora'] = $vws[$key]['recorrente_orderid'] ? $vws[$key]['recorrente_orderid'] : $vws[$key]['id_venda'] . 'G2U';
                    } else {
                        $vws[$key]['st_codtransacaooperadora'] = $vws[$key]['st_codtransacaooperadora'] ? $vws[$key]['st_codtransacaooperadora'] : ' - ';
                    }

                    //verifica se a data deve ser convertida para o formato pt_br
                    if ($convertDate) {
                        $dt_vencimento = !$vws[$key]['dt_vencimento'] ? ' - ' : new \DateTime(is_array($vws[$key]['dt_vencimento']) ? $vws[$key]['dt_vencimento']['date'] : $vws[$key]['dt_vencimento']);
                        $vws[$key]['dt_vencimento'] = $dt_vencimento == ' - ' ? $dt_vencimento : substr($dt_vencimento->format('d/m/Y'), 0, 10);

                        $dt_quitado = !$vws[$key]['dt_quitado'] ? ' - ' : new \DateTime(is_array($vws[$key]['dt_quitado']) ? $vws[$key]['dt_quitado']['date'] : $vws[$key]['dt_quitado']);
                        $vws[$key]['dt_quitado'] = $dt_quitado == ' - ' ? $dt_quitado : substr($dt_quitado->format('d/m/Y'), 0, 10);

                        $dt_atualizado = !$vws[$key]['dt_atualizado'] ? ' - ' : new \DateTime(is_array($vws[$key]['dt_atualizado']) ? $vws[$key]['dt_atualizado']['date'] : $vws[$key]['dt_atualizado']);
                        $vws[$key]['dt_atualizado'] = $dt_atualizado == ' - ' ? $dt_atualizado : $dt_atualizado->format('d/m/Y H:i:s');

                        //formata a data da venda
                        $dt_venda = !$vw->getDt_venda() ? ' - ' : new \DateTime($vw->getDt_venda());
                        $vws[$key]['dt_venda'] = $dt_venda == ' - ' ? $dt_venda : $dt_venda->format('d/m/Y H:i:s');

                    }

                    $vws[$key]['nu_cheque'] = $vws[$key]['id_meiopagamento'] == \MeioPagamentoTO::CHEQUE ? $vws[$key]['st_numcheque'] : ' - ';
                    $vws[$key]['st_link'] = '/default/textos/recibo/?type=html&arrParams[id_lancamento]=' . $vws[$key]['id_lancamento'] . '&arrParams[id_venda]=' . $vws[$key]['id_venda'] . '&id_textosistema=' . $vws[$key]['id_textosistemarecibo'];
                }
                return new \Ead1_Mensageiro($vws, \Ead1_IMensageiro::TYPE_SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Parâmetro id_matricula não informado.', \Ead1_IMensageiro::TYPE_ERRO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro("Erro ao pesquisar Resumo Financeiro." . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os dados da Vw_matriculaalocacao
     *
     * @funcionalidades Prorrogação de Sala de aula
     * @param array $param
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaVwMatriculaAlocacao(array $params)
    {
        try {
            if ($params['dt_inicio'] || $params['dt_termino']) {
                $params['dt_inicio'] = $this->converteDataBanco($params['dt_inicio'] . ' 00:00:00');
                $params['dt_termino'] = $this->converteDataBanco($params['dt_termino'] . ' 23:59:59');
            }

            /*
            * Nessa Vw_matriculaalocacao não é necessário passar todos os parametros enviados, somente
            * é necessário o id_saladeaula;
            */

            $result = $this->em->getRepository('\G2\Entity\VwMatriculaAlocacao')->retornaAlunosProjetoPedagogico($params);
            $return = array();
            foreach ($result as $key => $value) {
                $return[$key] = $this->toArrayEntity($value);
                $return[$key]['dt_encerramento'] = $value->getDt_encerramento() ? date('d/m/Y', strtotime($value->getDt_encerramento()->format('d-m-Y H:i:s'))) : '--';
                $return[$key]['dt_ultimoacesso'] = ($value->getDt_ultimoacesso()) ? date('d/m/Y', strtotime($value->getDt_ultimoacesso()->format('d-m-Y H:i:s'))) : '--';
                $return[$key]['bl_prorroga'] = '';
            }

            return $return;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    public function retornaTurmasDisponiveisCompartilhadas($array_params)
    {
        try {
            $sql = "SELECT  tp.id_turmaprojeto ,
            tp.id_turma ,
            tp.id_projetopedagogico ,
            tm.id_entidadecadastro,
            tm.st_turma
            FROM tb_turmaprojeto tp
            OUTER APPLY ( SELECT    COUNT(id_matricula) AS nu_alunos
            FROM      dbo.tb_matricula
            WHERE     id_turma = tp.id_turma
            ) AS mt
            JOIN tb_turma AS tm ON tm.id_turma = tp.id_turma
            AND tm.bl_ativo = 1
            AND mt.nu_alunos < tm.nu_maxalunos
            JOIN tb_turmaentidade AS te ON te.id_turma = tm.id_turma

            WHERE   tp.bl_ativo = 1
            AND id_projetopedagogico = " . $array_params['id_projetopedagogico'] . "
            AND te.id_entidade  = " . $this->sessao->id_entidade . "
            ORDER BY tm.st_turma ASC";
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->execute();

            return $stmt->fetchAll();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método para alocar automaticamente
     *  1 - busca o projeto pedagógico na matrícula pelo id_matricula
     *  2 - Verifica se é pra auto-alocar
     *  3 - Busca Salas de Aulas disponíveis para a matrícula
     *  4 - verifica se o id_matriculadisciplina já não está alocado
     *  5 - verifica se o número máximo de alunos é maior do que o número de alunos alocados
     *  6 - Verifica se a disciplina em questão tem disciplina pré-requisito
     *  7 - Verifica se a discipolina pré-requisito está concluída
     *  8 - Chama o processo de alocação
     * @param \MatriculaTO $matriculaTO
     * @param bool $bl_naoalocar
     * @param int $id_venda
     * @return \Ead1_Mensageiro
     */
    public function alocarAutomatico(\MatriculaTO $matriculaTO, $bl_naoalocar = false, $id_venda = null)
    {
        try {
            $venda = false;

            if (!$matriculaTO->getId_matricula()) {
                throw new \Zend_Exception("Matrícula não identificada!");
            }

            $id_matricula = $matriculaTO->getId_matricula();

            $projetoPedagogicoTO = new \ProjetoPedagogicoTO();
            $projetoPedagogicoTO->setId_projetopedagogico($matriculaTO->getId_projetopedagogico());
            $projetoPedagogicoTO->fetch(true, true, true);
            if (!$projetoPedagogicoTO->getBl_autoalocaraluno()) {
                return new \Ead1_Mensageiro("Auto Alocar desabilitado para o curso.", \Ead1_IMensageiro::AVISO);
            }

            $parametros = array(
                'id_matricula' => $id_matricula
            );

            $salas = null;

            $bl_matriculagraduacao = $this->isMatriculaGraduacao($id_matricula);
            $bl_graduacaocomvenda = $bl_matriculagraduacao && $matriculaTO->getId_vendaproduto();

            // Verificar se a matricula/venda é da graduacao e possui venda
            if ($bl_graduacaocomvenda) {
                /**
                 * @history GII-8088
                 * Se não for enviado o id_venda por parâmetro, buscar o id_venda da última venda confirmada
                 */
                if (!$id_venda) {
                    /** @var \G2\Repository\Venda $rep */
                    $rep = $this->em->getRepository('\G2\Entity\Venda');
                    $result = $rep->retornarUltimaVendaConfirmada($id_matricula);

                    if ($result) {
                        /** @var \G2\Entity\Venda $venda */
                        $venda = $this->find('\G2\Entity\Venda', $result['id_venda']);
                        $id_venda = $venda->getId_venda();
                    }
                }

                if ($id_venda) {
                    $parametros['id_venda'] = $id_venda;
                }

                $salas = $this->findSalasAutoAlocarGraduacao($matriculaTO, $parametros);
            } else if ($matriculaTO->getId_turma()) {
                $salas = $this->findBy('\G2\Entity\VwSalaDisciplinaTurma', $parametros);
            } else {
                $salas = $this->findBy('\G2\Entity\VwSalaDisciplinaProjeto', $parametros);
            }

            if (empty($salas)) {
                return new \Ead1_Mensageiro('Nenhuma Sala de Aula Disponivel para Alocação', \Ead1_IMensageiro::AVISO);
            }

            $alocados = array();
            $dados = array();

            foreach ($salas as $sala) {
                $id_alocacao = $sala->getId_alocacao();
                if (!empty($id_alocacao) && $id_alocacao != 0) {
                    $alocados[] = $sala->getId_matriculadisciplina();
                } elseif (!in_array($sala->getId_matriculadisciplina(), $alocados)) {
                    if ((int)$sala->getNu_maxalunos() > (int)$sala->getNu_alocados()) {

                        $alocar = true;

                        if ($alocar) {
                            $alocacaoTO = new \AlocacaoTO();
                            $alocacaoTO->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO);
                            $alocacaoTO->setId_situacao(\G2\Constante\Situacao::TB_ALOCACAO_ATIVA);
                            $alocacaoTO->setId_saladeaula($sala->getId_saladeaula());
                            $alocacaoTO->setId_categoriasala(\SalaDeAulaTO::SALA_NORMAL);
                            $alocacaoTO->setId_usuariocadastro($matriculaTO->getId_usuario());
                            $alocacaoTO->setId_matriculadisciplina($sala->getId_matriculadisciplina());

                            $bo_saladeaula = new \SalaDeAulaBO();
                            //Se for uma matricula da graduação, passa o objeto venda para verifica o nu_semestre
                            $mensageiroAlocacao = $bo_saladeaula->salvarAlocacaoAluno($alocacaoTO, $bl_naoalocar, ($bl_matriculagraduacao ? $venda : false));

                            if ($mensageiroAlocacao->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                                $alocados[] = $sala->getId_matriculadisciplina();
                                $dados['sucesso'][] = $mensageiroAlocacao->getText();

                                // Marcar a sala da Pre Matricula como ALOCADA
                                if ($bl_graduacaocomvenda) {
                                    /**
                                     * @var \G2\Entity\PreMatriculaDisciplina $prematricula
                                     */
                                    $prematricula = $this->findOneBy('\G2\Entity\PreMatriculaDisciplina', array(
                                        'id_venda' => $sala->getId_venda(),
                                        'id_disciplina' => $sala->getId_disciplina(),
                                        'id_saladeaula' => $sala->getId_saladeaula(),
                                    ));

                                    if ($prematricula instanceof \G2\Entity\PreMatriculaDisciplina) {
                                        $prematricula->setBl_alocado(true);
                                        $this->save($prematricula);
                                    }
                                }
                            } else {
                                $dados['erro'][] = $mensageiroAlocacao->getText();
                            }
                        }
                    } else {
                        //aqui fica o tratamento do número máximo de alunos
                        $dados['erro'][] = '- O número máximo de alunos: ' . $sala->getNu_maxalunos() . ',  na sala de aula ' . $sala->getSt_saladeaula() . '  foi atingido.';
                    }
                }
            }

            /**
             * Calcular o número do semestre
             * @history GII-8771
             */
            if ($bl_matriculagraduacao) {
                // Após alocar nas novas salas, recalcular o semestre atual
                $nu_semestre = $this->getSemestreAtual($id_matricula);

                // Salvar o novo semestre na matricula
                $this->alterarSemestreMatricula($id_matricula, $nu_semestre);
            }

            if (is_array($dados) && array_key_exists('erro', $dados) && count($dados['erro'])) {
                $mensagem = '';
                foreach ($dados['erro'] as $erro) {
                    $mensagem = $mensagem . $erro . '<br/>';
                }
                return new \Ead1_Mensageiro("Alocação automatica bem sucedida, porém algumas não foram realizadas: " . $mensagem, \Ead1_IMensageiro::AVISO);
            } else {
                return new \Ead1_Mensageiro("Alocação automatica bem sucedida", \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Zend_Validate_Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna true se a matricula (id_entidadematricula) for de graduação
     * LINHA DE NEGOCIO = 2
     * @param $id_matricula
     * @return mixed
     */
    public function isMatriculaGraduacao($id_matricula)
    {
        return $this->em->getRepository('\G2\Entity\VwMatricula')->isMatriculaGraduacao($id_matricula);
    }

    /**
     * @param \MatriculaTO $matriculaTO
     * @param array $params
     * @return \G2\Entity\VwSalaDisciplinaVenda[]
     * @throws \Exception
     */
    public function findSalasAutoAlocarGraduacao($matriculaTO, $params = array())
    {
        /** @var \G2\Entity\VendaProduto $en_vendaproduto */
        $en_vendaproduto = $this->findOneBy('\G2\Entity\VendaProduto', array('id_vendaproduto' => $matriculaTO->getId_vendaproduto()));

        if (!$en_vendaproduto || !$en_vendaproduto->getId_produto()) {
            throw new \Zend_Validate_Exception('Nenhum produto encontrado');
        }

        // Buscar as salas pre matriculadas da Venda que NÃO possuem alocação
        /** @var \G2\Entity\VwSalaDisciplinaVenda[] $salas */
        $salas = $this->findBy('\G2\Entity\VwSalaDisciplinaVenda', array_merge(array(
            'id_alocacao' => null,
            'bl_alocado' => 0
        ), $params));

        return $salas;
    }

    /**
     * @param \MatriculaTO $matriculaTO
     * @return array
     */
    public function findSalasPreMatricula(\MatriculaTO $matriculaTO)
    {
        $salas = array();

        $en_vendaproduto = $this->findOneBy('\G2\Entity\VendaProduto', array('id_vendaproduto' => $matriculaTO->getId_vendaproduto()));

        // DESCOBRIR ID DO PROJETO PEDAGOGICO
        $ng_produto = new \G2\Negocio\Produto();
        $mensageiro = $ng_produto->findProdutoProjetoPedagogicoByArray(array('id_produto' => $en_vendaproduto->getId_produto()->getId_produto()));

        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $id_projetopedagogico = $mensageiro->getFirstMensagem()['id_projetopedagogico'];

            // Caso a matricula tenha turma, a Data referente pra buscar as salas padrao, deve ser a dt_inicio da Turma
            $dt_abertura = date('Y-m-d');
            if ($matriculaTO->getId_turma()) {
                $turma = $this->find('\G2\Entity\Turma', (int)$matriculaTO->getId_turma());
                if ($turma instanceof \G2\Entity\Turma) {
                    $dt_abertura = $this->converterData($turma->getDt_inicio(), 'Y-m-d H:i:s');
                }
            }

            $ng_vendagraduacao = new \G2\Negocio\VendaGraduacao();
            $ng_vendagraduacao->idEntidade = $matriculaTO->getId_entidadeatendimento();
            $salas = $ng_vendagraduacao->findSalasPadraoPorData($dt_abertura, $matriculaTO->getId_usuario(), $id_projetopedagogico, $ng_vendagraduacao->retornaCreditosDefault()['st_valor']);
        }

        return $salas;
    }


    /**
     * Metodo para finalizar a matricula de renovação
     * author helder silva <helder.silva@unyleya.com.br>
     */
    public function finalizaMatriculaRenovacao($matricula)
    {
        try {

            $ng_lancamentos = new \G2\Negocio\Venda();

            $arrLancamentos = $ng_lancamentos->retornaVwVendaLancamento(array(
                'id_venda' => $matricula['id_venda'],
                'bl_ativo' => true
            ));

            /**
             * Se os lançamentos forem do tipo cartão de crédito e não foram quitados, muda os lançamentos para boleto.
             * E Seta os lançamentos como originais
             */
            $en_meiopagamento_boleto = $this->getReference(
                '\G2\Entity\MeioPagamento',
                \G2\Constante\MeioPagamento::BOLETO
            );

            foreach ($arrLancamentos as $value) {
                $lancamento = $this->find('\G2\Entity\Lancamento', $value->getIdLancamento());

                $id_meiopagamento = $lancamento->getIdMeiopagamento() instanceof \G2\Entity\MeioPagamento
                    ? $lancamento->getIdMeiopagamento()->getId_meiopagamento()
                    : null;

                if (\G2\Constante\MeioPagamento::CARTAO_CREDITO == $id_meiopagamento && false == $lancamento->getBlQuitado()) {
                    $lancamento->setIdMeiopagamento($en_meiopagamento_boleto);
                }

                $lancamento->setBlOriginal(true);

                $this->save($lancamento);
            }

            $contratoTo = new \ContratoTO();
            $contratoTo->setId_venda($matricula['id_venda']);
            $contratoTo->fetch(false, true, true);

            $matriculaBo = new \MatriculaBO();
            $matriculaBo->procedimentoAtivarMatricula($contratoTo, $matricula);

            return new \Ead1_Mensageiro("Ativação de matricula de renovação efetuada com sucesso!", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * * Retorna dados da vw_matricula de acordo com os parametros passado, se passar o parametro $all como true vai retorna
     * todos os registros se não somente um array simples
     *
     * @param array $params
     * @param bool $all
     * @return Object
     * @throws \Zend_Exception
     */
    public function retornaVwMatricula(array $params, $all = false)
    {
        try {
            if ($all) {
                return $this->findBy('\G2\Entity\VwMatricula', $params);
            } else {
                return $this->findOneBy('\G2\Entity\VwMatricula', $params);
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao consultar dados de matricula. " . $e->getMessage());
        }
    }

    public function retornarGradeAlunoPortal($params)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            if (!array_key_exists('id_matricula', $params) || !$params['id_matricula']) {
                THROW new \Zend_Validate_Exception("Matrícula não encontrada!");
            }

            $order = array('id_modulo' => 'ASC', 'dt_abertura' => 'ASC', 'id_tipodisciplina' => 'ASC');
            $vws = $this->findVwGradeNotaWithInsentos($params, $order);

            $return = array();
            $arr = array();
            $arrModulos = array();
            if (is_array($vws)) {
                $arr['bl_temnotafinal'] = false;
                $arr['bl_mostratcc'] = false;
                foreach ($vws as $key => $vw) {
                    $return[$key]['id_saladeaula'] = $vw->getId_saladeaula();
                    $return[$key]['st_saladeaula'] = $vw->getSt_saladeaula();
                    $return[$key]['id_disciplina'] = $vw->getId_disciplina();
                    $return[$key]['dt_abertura'] = $vw->getDt_abertura() instanceof DateTime ? $vw->getDt_abertura()->format("d/m/Y") : $vw->getDt_abertura();
                    $return[$key]['dt_encerramento'] = $vw->getDt_encerramento() instanceof DateTime ? $vw->getDt_encerramento()->format("d/m/Y") : $vw->getDt_encerramento();
                    $return[$key]['id_projetopedagogico'] = $vw->getId_projetopedagogico();
                    $return[$key]['st_projetopedagogico'] = $vw->getSt_projetopedagogico();
                    $return[$key]['st_notatcc'] = is_null($vw->getSt_notatcc()) ? '-' : (int)$vw->getSt_notatcc();
                    $return[$key]['st_notaead'] = is_null($vw->getSt_notaead()) ? '-' : (int)$vw->getSt_notaead();
                    $return[$key]['st_notafinal'] = is_null($vw->getSt_notafinal()) ? '-' : (int)$vw->getSt_notafinal();
                    $return[$key]['st_disciplina'] = $vw->getSt_disciplina();
                    $return[$key]['id_tipodisciplina'] = $vw->getId_tipodisciplina();
                    $return[$key]['st_tituloexibicaodisciplina'] = $vw->getSt_tituloexibicaodisciplina();
                    $return[$key]['id_matricula'] = $vw->getId_matricula();
                    $return[$key]['nu_notafinal'] = is_null($vw->getNu_notafinal()) ? '-' : (int)$vw->getNu_notafinal();
                    $return[$key]['id_evolucao'] = $vw->getId_evolucao();
                    $return[$key]['st_evolucao'] = $vw->getSt_evolucao();
                    $return[$key]['id_situacao'] = $vw->getId_situacao();
                    $return[$key]['st_situacao'] = $vw->getSt_situacao();
                    $return[$key]['nu_cargahoraria'] = (int)$vw->getNu_cargahoraria() ? (int)$vw->getNu_cargahoraria() : ' - ';
                    $return[$key]['nu_notatotal'] = $vw->getNu_notatotal();
                    $return[$key]['st_status'] = $vw->getSt_status();
                    $return[$key]['bl_status'] = $vw->getBl_status();
                    $return[$key]['id_matriculadisciplina'] = $vw->getId_matriculadisciplina();
                    $return[$key]['id_avaliacaoconjuntoreferencia'] = $vw->getId_avaliacaoconjuntoreferencia();
                    $return[$key]['id_tiponotatcc'] = $vw->getId_tiponotatcc();
                    $return[$key]['id_tiponotafinal'] = $vw->getId_tiponotafinal();
                    $return[$key]['id_tiponotaead'] = $vw->getId_tiponotaead();
                    $return[$key]['id_tiponotarecuperacao'] = $vw->getId_tiponotarecuperacao();
                    $return[$key]['bl_complementar'] = $vw->getBl_complementar();
                    $return[$key]['dt_encerramentoextensao'] = $vw->getDt_encerramentoextensao() instanceof \DateTime ? $vw->getDt_encerramentoextensao()->format("d/m/Y") : $vw->getDt_encerramentoextensao();
                    $return[$key]['st_notaatividade'] = is_null($vw->getSt_notaatividade()) ? '-' : (int)$vw->getSt_notaatividade();
                    $return[$key]['st_notarecuperacao'] = is_null($vw->getSt_notarecuperacao()) ? '-' : (int)$vw->getSt_notarecuperacao();
                    $return[$key]['dt_cadastroaproveitamento'] = $vw->getDt_cadastroaproveitamento() instanceof \DateTime ? $vw->getDt_cadastroaproveitamento()->format("d/m/Y") : $vw->getDt_cadastroaproveitamento();
                    $return[$key]['st_nomeusuarioaproveitamento'] = $vw->getSt_nomeusuarioaproveitamento();
                    $return[$key]['bl_mostratcc'] = $vw->getBl_mostratcc();
                    $return[$key]['id_modulo'] = $vw->getId_modulo();
                    $return[$key]['st_modulo'] = $vw->getSt_modulo();

                    //avaliacoes graduacao
                    $return[$key]['st_statusdisciplina'] = is_null($vw->getSt_statusdisciplina()) ? '-' : $vw->getSt_statusdisciplina();
                    $return[$key]['st_avaliacaoatividade'] = $vw->getSt_avaliacaoatividade();
                    $return[$key]['st_avaliacaoead'] = $vw->getSt_avaliacaoead();
                    $return[$key]['st_avaliacaofinal'] = $vw->getSt_avaliacaofinal();
                    $return[$key]['st_avaliacaorecuperacao'] = $vw->getSt_avaliacaorecuperacao();

                    $return[$key]['st_nomeusuarioaproveitamento'] = $vw->getSt_nomeusuarioaproveitamento();
                    $return[$key]['dt_cadastroaproveitamento'] = $vw->getDt_cadastroaproveitamento() instanceof \DateTime ? $vw->getDt_cadastroaproveitamento()->format("d/m/Y") : $vw->getDt_cadastroaproveitamento();
                    $return[$key]['id_aproveitamento'] = is_null($vw->getId_aproveitamento()) ? NULL : (int)$vw->getId_aproveitamento();
                    $return[$key]['st_disciplinaoriginal'] = is_null($vw->getSt_disciplinaoriginal()) ? '-' : $vw->getSt_disciplinaoriginal();


                    if (!array_key_exists($vw->getId_modulo(), $arr)) {
                        //@todo Trata caso a disciplina nao seja ligada a nenhum modulo, mostra na tela assim. é preciso melhorar
                        if (is_null($vw->getId_modulo())) {
                            $vw->setId_modulo(999999);
                            $vw->setSt_modulo("Módulo sem nome");
                        }
                        $arr[$vw->getId_modulo()]['id_modulo'] = $vw->getId_modulo();
                        $arr[$vw->getId_modulo()]['st_modulo'] = $vw->getSt_modulo();
                        $arr[$vw->getId_modulo()]['dados'] = array();
                        array_push($arr[$vw->getId_modulo()]['dados'], $return[$key]);

                        //modulo da matricula
                        $arrModulos[] = $vw->getId_modulo();
                    } else {
                        array_push($arr[$vw->getId_modulo()]['dados'], $return[$key]);
                    }
                    /** A nota de TCC do aluno só é mostrada se ele tem nota final lançada e a data de defesa lançada também
                     * as váriaveis abaixo fazem essa validação */
                    if (!is_null($vw->getSt_notatcc()))
                        $arr['bl_temnotafinal'] = true;

                    if (!is_null($vw->getSt_notatcc()) && $vw->getId_tipodisciplina() == 2){
                        $arr['bl_mostratcc'] = $vw->getBl_mostratcc();

                    }

                }
            }
            //adiciona a variavel $arr os módulosda matricula
            $arr['mod'] = $arrModulos;
            /** Retorna o número de de avaliações que o aluno tem. Essa variavel serve para organizar o theader da grid de notas */
            $arr['tiposAvaliacao'] = $this->retornaTiposAvaliacao($params['id_matricula']);
            $mensageiro->setMensageiro($arr, \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Validate_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Zend_Exception $e) {
            $mensageiro->setMensageiro("Erro ao retornar grade!", \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $mensageiro;
    }

    /**
     * Retorna tipos de avaliação
     * @param $id_matricula
     * @return array
     */
    public function retornaTiposAvaliacao($id_matricula)
    {
        try {
            $arrAvaliacoes = $this->getRepository('\G2\Entity\Matricula')->retornaTiposAvaliacao($id_matricula);
            $arrAvaliacoes = array_map(function ($value) {
                return array(
                    $value['id_tipoavaliacao'] => $value['st_labeltipo']
                );
            }, $arrAvaliacoes);
            $_arr = array();
            foreach ($arrAvaliacoes as $ar) {
                $key = array_keys($ar);
                $key = array_shift($key);
                $_arr[$key] = array('st_labeltipo' => $ar[$key]);
            }
            return $_arr;
        } catch (\Exception $e) {
            return array();
        }
    }


    public function retornarGradePrrAlunoPortal($params)
    {
        $mensageiro = new \Ead1_Mensageiro();

        try {
            if (!array_key_exists('id_matricula', $params) || !$params['id_matricula']) {
                THROW new \Zend_Validate_Exception("Matrícula não encontrada!");
            }
            $order = array('id_modulo' => 'ASC', 'dt_abertura' => 'ASC', 'id_tipodisciplina' => 'ASC');
            $vws = $this->findVwGradeNotaWithInsentos($params, $order);

            $return = array();

            if (is_array($vws)) {
                foreach ($vws as $key => $vw) {
                    $return[$key]['id_disciplina'] = $vw->getId_disciplina();
                    $return[$key]['st_notaead'] = is_null($vw->getSt_notaead()) ? '-' : (int)$vw->getSt_notaead();
                    $return[$key]['st_notaatividade'] = is_null($vw->getSt_notaatividade()) ? '-' : (int)$vw->getSt_notaatividade();
                    $return[$key]['st_notarecuperacao'] = is_null($vw->getSt_notarecuperacao()) ? '-' : (int)$vw->getSt_notarecuperacao();
                    $return[$key]['nu_notafinal'] = is_null($vw->getNu_notafinal()) ? '-' : (int)$vw->getNu_notafinal();
                    $return[$key]['st_disciplina'] = $vw->getSt_disciplina();
                    $return[$key]['st_tituloexibicaodisciplina'] = $vw->getSt_tituloexibicaodisciplina();
                    $return[$key]['st_status'] = $vw->getSt_status();
                    $return[$key]['bl_mostratcc'] = $vw->getBl_mostratcc();
                    $return[$key]['st_notatcc'] = is_null($vw->getSt_notatcc()) ? '-' : (int)$vw->getSt_notatcc();
                    $return[$key]['st_notafinal'] = is_null($vw->getSt_notafinal()) ? '-' : (int)$vw->getSt_notafinal();
                    $return[$key]['id_tipodisciplina'] = $vw->getId_tipodisciplina();
                }
            }

            $mensageiro->setMensageiro($return, \Ead1_IMensageiro::SUCESSO);

        } catch (\Zend_Validate_Exception $e) {
            $mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::AVISO);
        } catch (\Zend_Exception $e) {
            $mensageiro->setMensageiro("Erro ao retornar grade!", \Ead1_IMensageiro::ERRO, $e->getMessage());
        }
        return $mensageiro;
    }

    /**
     *  Metodo responsavel por retornar apenas os dados da matricula de uma pessoa, da tb_matricula.
     * Caso tenha necessidade de retornar os dados em array basta enviar o param true,
     * além disso ele vai retonar os objetos convertidos para facilitar
     *
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|null|object
     * @throws \Zend_Exception
     */
    public function retornaMatricula($dados, $array = false)
    {
        $result = $this->findOneBy('\G2\Entity\Matricula', $dados);
        if ($result) {
            if ($array) {
                $result = $this->toArrayEntity($result);
                return $result;
            } else {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * @param \G2\Entity\Matricula $en_matricula
     * @param \G2\Entity\Tramite|array $en_tramite
     * @return bool
     */
    public function gerarTramite($en_matricula, $en_tramite)
    {
        if (!($en_tramite instanceof \G2\Entity\Tramite)) {
            // Valores padrão
            $data = array_merge(array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_usuario' => $this->sessao->id_usuario,
                'dt_cadastro' => new \DateTime(),
                'bl_visivel' => true
            ), $en_tramite);

            $en_tramite = new \G2\Entity\Tramite();
            $this->entitySerialize->arrayToEntity($data, $en_tramite);
        }

        $this->save($en_tramite);

        // Salvar o relacionamento de Trâmite e Matrícula
        $en_tramitematricula = new \G2\Entity\TramiteMatricula();
        $en_tramitematricula
            ->setId_matricula($en_matricula)
            ->setId_tramite($en_tramite);
        $this->save($en_tramitematricula);

        return $en_tramite->getId_tramite() && $en_tramitematricula->getId_tramitematricula();
    }

    /**
     * Esta função permite conferir se o usuário já está matriculado na disciplina.
     * @access public
     * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
     */
    public function checaSeUsuarioEstaMatriculado($id_usuario, $id_projetopedagogico, $id_turma)
    {
        $return = $this->findOneBy('\G2\Entity\Matricula', array(
            'id_usuario' => $id_usuario,
            'id_projetopedagogico' => $id_projetopedagogico,
            'id_turma' => $id_turma
        ));

        return $return;
    }


    /**
     * Destranca todas as disciplinas de uma matricula
     * @see \G2\Negocio\VendaGraduacao::renovacaoAutomaticaMatricula - line 982
     * @param $id_matricula
     * @return \Ead1_Mensageiro
     */
    public function destrancarDisciplinas($id_matricula)
    {
        try {
            if (!$id_matricula)
                throw new \Exception('O id_matricula é obrigatório e não foi passado');

            /** @var \G2\Entity\MatriculaDisciplina[] $matriculasDisciplinas * */
            $matriculasDisciplinas = $this->findBy('\G2\Entity\MatriculaDisciplina',
                array('id_matricula' => (int)$id_matricula,
                    'id_evolucao' => \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_TRANCADA
                ));

            if ($matriculasDisciplinas) {
                foreach ($matriculasDisciplinas as $en_md) {
                    $en_md->setId_evolucao(\G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO);
                    $this->save($en_md);
                }
            }
            return new \Ead1_Mensageiro('Disciplinas destrancadas com sucesso.', \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao destrancar disciplinas: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param int $id_matricula
     * @return null|int
     */
    public function retornarCoeficienteRendimento($id_matricula)
    {
        // O cálculo é feito somente para GRADUAÇÃO
        if ($this->isMatriculaGraduacao($id_matricula)) {
            /** @var \G2\Entity\VwGradeNota[] $notas */
            $notas = $this->findBy('\G2\Entity\VwGradeNota', array(
                'id_matricula' => $id_matricula,
                'id_situacao' => \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_ATIVA,
                'id_evolucao' => array(
                    \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO,
                    \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO,
                ),
                'bl_coeficienterendimento' => true
            ));

            if ($notas) {
                $somatorio = 0;

                foreach ($notas as $nota) {
                    $somatorio += $nota->getNu_notafinal();
                }

                return floatval(number_format($somatorio / count($notas), 2, '.', ''));
            }
        }

        return null;
    }

    /**
     * @author Helder Silva <helder.silva@unyleya.com.br>
     * @param $max_execute
     * @return array|\Ead1_Mensageiro
     *
     * Torna o aluno concluinte caso ele esteja cursando em turma encerrada
     *
     */
    public function tornarMatriculaConcluinteTurmaEncerrada($max_execute)
    {
        try {
            //Busca no repository os registros
            $objmatricula = $this->em->getRepository('\G2\Entity\Matricula')->retornarAlunosTurmasEncerradas($max_execute);

            if ($objmatricula) {
                $retorno = array();

                foreach ($objmatricula as $m) {

                    $matricula = $this->find('\G2\Entity\Matricula', $m['id_matricula']);
                    $matricula->setId_evolucao(
                        $this->em->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CONCLUINTE));
                    $resultado = $this->save($matricula);
                    if ($resultado) {

                        //Salva o tramite de alteracao da matricula
                        $tramite = new Tramite();
                        $mensagem = 'Matricula alterada para concluinte automaticamente devido ao encerramento da sala de aula!';
                        $tramite->salvarTramiteMatricula($mensagem, $resultado->getId_matricula(),
                            \TramiteMatriculaTO::EVOLUCAO, $resultado->getId_usuario(), $m['id_entidadematricula']);

                        array_push($retorno, array(
                            'matricula' => $resultado->getId_matricula(),
                            'nome' => $resultado->getId_usuario()->getSt_nomecompleto(),
                            'entidade' => $m['st_nomeentidade'],
                            'turma' => $resultado->getId_turma()->getSt_turma(),
                            'status' => 'OK'));
                    } else {
                        array_push($retorno, array(
                            'matricula' => $m['id_matricula'],
                            'nome' => $m['st_nomecompleto'],
                            'entidade' => $m['st_nomeentidade'],
                            'turma' => $m['st_turma'],
                            'status' => 'FALHA'));
                    }
                }
                return $retorno;
            } else {
                return new \Ead1_Mensageiro('Nenhuma matricula encontrada.', \Ead1_IMensageiro::SUCESSO);
            }
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro ao consultar alunos cursando em turmas encerradas do IMP. Erro:'
                . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param int $id_matricula
     * @param bool $projeto (caso true, retorna o termo para a titulação do projeto ao invés do aluno)
     * @return string
     * @throws \Zend_Exception
     */
    public function retornarTitulacao($id_matricula, $projeto = false)
    {
        /** @var \G2\Entity\Matricula $en_matricula */
        $en_matricula = $this->find('\G2\Entity\Matricula', $id_matricula);

        if (!$en_matricula instanceof \G2\Entity\Matricula) {
            throw new \Zend_Exception('Matrícula não encontrada.');
        }

        $en_projetopedagogico = $en_matricula->getId_projetopedagogico();
        $en_grauacademico = $en_projetopedagogico->getId_grauacademico();

        if ($en_grauacademico) {
            return $projeto ? $en_grauacademico->getSt_grauacademico() : $en_grauacademico->getSt_titulacao();
        }
    }

    /**
     * Este método é responsável por alterar matrículas para a evolução Decurso de Prazo.
     * @access public
     * @author Arthur Luiz Lara Quites <arthur.quites@unyleya.com.br>
     */
    public function alterarMatriculaDecursoDePrazo($dados)
    {
        $erro = [];
        $sucesso = [];
        if (empty($dados) || !($dados[0] instanceof \G2\Entity\VwAlterarMatriculaDecursoDePrazo)) {
            return new \Ead1_Mensageiro("Não foi possível realizar a alteração das matrículas para Decurso de Prazo, pois não existem registros para tal.", \Ead1_IMensageiro::ERRO);
        }
        // instanciação de objetos
        $data = new \DateTime();

        // loop que itera entre todas as matrículas a serem alteradas
        foreach ($dados as $key => $value) {
            try {
                // início da da instrução DML update
                $this->beginTransaction();

                // recupera os dados da matrícula e executa a alteração
                $mat = $this->find('\G2\Entity\Matricula', $value->getId_matricula());
                $mat->setId_evolucao($this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_DECURSO_DE_PRAZO));

                // checa por erros na gravação
                if ($mat)
                    $this->save($mat);
                else {
                    throw new \Zend_Exception($value->getId_matricula());
                }
                // executa a inserção de um novo trâmite
                $tramite = new \G2\Entity\Tramite();
                $tramite
                    ->setId_tipotramite($this->getReference('\G2\Entity\TipoTramite', \G2\Constante\TipoTramite::MATRICULA_EVOLUCAO))
                    ->setId_entidade($this->getReference('\G2\Entity\Entidade', $value->getId_entidadematricula()))
                    ->setId_usuario($this->getReference('\G2\Entity\Usuario', $value->getId_usuario()))
                    ->setSt_tramite('Alterando Evolução para: Decurso de Prazo')
                    ->setBl_visivel(1)
                    ->setDt_cadastro($data);

                // checa por erros na gravação
                if ($mat)
                    $this->save($tramite);
                else {
                    throw new \Zend_Exception($value->getId_matricula());
                }

                // executa a vinculação de um trâmite à matrícula
                $tramiteMatricula = new \G2\Entity\TramiteMatricula();
                $tramiteMatricula
                    ->setId_tramite($this->getReference('\G2\Entity\Tramite', $tramite->getId_tramite()))
                    ->setId_matricula($this->getReference('\G2\Entity\Matricula', $mat->getId_matricula()));

                // checa por erros na gravação
                if ($mat)
                    $this->save($tramiteMatricula);
                else {
                    throw new \Zend_Exception($value->getId_matricula());
                }
                $sucesso[] = $value->getId_matricula();
                $this->commit();
            } catch (\Zend_Exception $e) {
                $this->rollback();
                $erro[] = $e->getMessage();
            }
        }
        return array("sucesso" => $sucesso, "erro" => $erro);
    }

    /**
     * Método utilizado em Default\MatriculaController
     * Retorna dados específicos da tela secretaria matricula.
     * O ID da sessao é desprezado na consulta dos dados.
     * @param $params
     * @return array
     * @throws \Zend_Exception
     */
    public function consultarDadosSecretariaMatricula($params)
    {
        $params['bl_ativo'] = true;

        $retorno = array();

        if (array_key_exists('id_usuario', $params) && $params['id_usuario']) {
            $params['id_entidadematricula'] = $this->getId_entidade();
            $result = $this->consultaVwMatricula($params);
            foreach ($result as $r) {

                $cargaHoraria = $this->findOneBy('\G2\Entity\VwMatriculaCargaHoraria', array("id_matricula" => $r->getId_matricula()));

                $arMatricula = $this->toArrayEntity($r);
                $arMatricula['nu_cargahorariaintegralizada'] = $cargaHoraria ? $cargaHoraria->getNu_cargahorariaintegralizada() : 0;

                array_push($retorno, $arMatricula);
            }
        } elseif (array_key_exists('id_matricula', $params) && $params['id_matricula']) {
            $result = $this->findOneBy('\G2\Entity\VwMatricula', $params);
            if ($result) {
                $tratamento = $this->toArrayEntity($result);
                // Ajustar formato das datas
                $tratamento['dt_inicio'] = Helper::converterData($tratamento['dt_inicio'], 'd/m/Y H:i');
                $tratamento['dt_termino'] = Helper::converterData($tratamento['dt_termino'], 'd/m/Y H:i');
                $tratamento['dt_cadastro'] = Helper::converterData($tratamento['dt_cadastro'], 'd/m/Y H:i');
                $tratamento['dt_concluinte'] = Helper::converterData($tratamento['dt_concluinte'], 'd/m/Y');
                $tratamento['dt_terminomatricula'] = Helper::converterData($tratamento['dt_terminomatricula']['date'], 'd/m/Y H:i');
                $tratamento['dt_colacao'] = Helper::converterData($tratamento['dt_colacao'], 'd/m/Y');

                // Calcular o Coeficiente de Rendimento Acumulado
                $tratamento['nu_coeficienterendimento'] = $this->retornarCoeficienteRendimento($params['id_matricula']);

                // Verificar se alguma venda foi transferida de entidade
                $tratamento['bl_transferidoentidade'] = $this->transferidoEntidade($params['id_matricula']);

                if ($tratamento['st_urlportalmatricula']) {
                    if (!empty($tratamento['bl_novoportal']) && !empty($tratamento['st_urlnovoportal'])) {
                        $tratamento['st_urlacesso'] = $tratamento['st_urlnovoportal']
                            . '/login/direto/0/' . $this->sessao->id_usuario
                            . '/' . $tratamento['id_entidadematricula']
                            . '/' . $tratamento['id_usuario']
                            . '/' . $tratamento['st_urlacesso'];
                    } else {
                        $tratamento['st_urlacesso'] = \Ead1_Ambiente::geral()->st_url_portal
                            . '/portal/login/autenticacao-direta?'
                            . 'l=nao&m=' . $this->sessao->id_usuario
                            . '&e=' . $tratamento['id_entidadematricula']
                            . '&p=' . $tratamento['id_usuario']
                            . '&u=' . $tratamento['st_urlacesso'];
                    }
                } else {
                    $tratamento['st_urlacesso'] = null;
                }

                $cargaHoraria = $this->findOneBy('\G2\Entity\VwMatriculaCargaHoraria', array("id_matricula" => $params['id_matricula']));
                $tratamento['nu_cargahorariaintegralizada'] = $cargaHoraria ? $cargaHoraria->getNu_cargahorariaintegralizada() : 0;

                array_push($retorno, $tratamento);
            }
        } else {
            throw new \Zend_Exception('É obrigatório pelo menos o id do usuário ou a matrícula para consulta.');
        }

        return $retorno;


    }

    /**
     * retorna o semestre baseado na contagem de disciplinas aprovadas
     * seguindo demanda GII-8771
     *
     * http://confluence.unyleya.com.br/pages/viewpage.action?pageId=16256155
     *
     * @param int $id_matricula
     * @return int
     * @throws \Zend_Exception
     */
    public function getSemestreAtual($id_matricula)
    {
        $nu_semestreatual = 1;
        $nu_aprovadas = 0;

        if ($id_matricula) {
            $ng_disciplina = new \G2\Negocio\Disciplina();

            // Buscar disciplinas para verificar o semestre
            $disciplinas = $ng_disciplina->retornarDisciplinasByMatricula(array('id_matricula' => $id_matricula));

            if ($disciplinas) {
                foreach ($disciplinas as $disciplina) {
                    if ($disciplina['bl_disciplinasemestre'] == true
                        && ($disciplina['id_evolucao'] == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO
                            || $disciplina['id_evolucao'] == \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO)
                    ) {
                        $nu_aprovadas++;
                    }
                }
            }
        }

        if ($nu_aprovadas) {
            $resultado = (int)floor(($nu_aprovadas / 5));
            $resto = ($nu_aprovadas % 5);

            if ($resto < 3) {
                $nu_semestreatual = $resultado;
            } else {
                $nu_semestreatual = $resultado + 1;
            }
        }

        return $nu_semestreatual;
    }

    /**
     * altera o semestre da matricula para o informado
     *
     * @param int $id_matricula
     * @param int $nu_semestre
     * @return \Ead1_Mensageiro|mixed
     */
    public function alterarSemestreMatricula($id_matricula, $nu_semestre)
    {
        try {
            $matricula = $this->find('\G2\Entity\Matricula', $id_matricula);
            $matricula->setNu_semestreatual($nu_semestre);
            $resultado = $this->save($matricula);
            return $resultado;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro(
                'Erro ao salvar semestre em matrícula. Erro:' . $e->getMessage(), \Ead1_IMensageiro::ERRO
            );
        }
    }

    /**
     * Insere um novo histórico de grade horária para uma matrícula
     *
     * @param array $params
     * @param string $html
     *
     * @throws \Zend_Exception|\Exception
     *
     * @return \Ead1_Mensageiro|boolean
     */
    public function salvarHistoricoMatriculaGradeNota(array $params, $html)
    {
        try {
            $this->beginTransaction();
            $tramiteNegocio = new \G2\Negocio\Tramite();

            $matricula = $this->find('\G2\Entity\Matricula', $params['id_matricula']);
            $projetoPedagogico = $matricula->getId_projetopedagogico();
            $tramiteMatricula = $tramiteNegocio->getTramiteMatricula($params['id_matricula'], false, array('id_tramite' => 'DESC'), 1);

            $historico = new \G2\Entity\HistoricoMatriculaGradeNota();
            $historico->setId_matricula($matricula)
                ->setId_projetopedagogico($projetoPedagogico)
                ->setId_tramitematricula($tramiteMatricula)
                ->setSt_htmlgrade($html)
                ->setDt_cadastro(new \DateTime());

            if (!$this->save($historico)) {
                throw new \Zend_Exception($params['id_matricula']);
            }

            $this->commit();

            return true;
        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro(
                'Erro ao salvar histórico. Erro:' . $e->getMessage(), \Ead1_IMensageiro::ERRO
            );
        }
    }

    /** Função para autenticar e gerar link para o redirecionar para o relatorio de log do aluno
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Doctrine\ORM\ORMException
     * @throws \Zend_Exception
     * @throws \Zend_Session_Exception
     */
    public function autenticarLogMoodle($params)
    {
        try {

            if (!array_key_exists('id_matricula', $params)) {
                throw new \Zend_Exception('Matrícula não encontrada.');
            }

            if (!array_key_exists('id_saladeaula', $params)) {
                throw new \Zend_Exception('Sala de aula não encontrada.');
            }

            $where = array(
                'id_matricula' => $params['id_matricula']
            );

            $aluno = $this->findOneBy('\G2\Entity\VwMatricula', $where);

            if (!$aluno) {
                throw new \Zend_Exception('Aluno não encontrado');
            }

            $idEntidade = $this->sessao->id_entidade;
            $idEntidadeMoodle = false;

            // Verificar se existe moodle integrado na entidade logado, se não estiver ira buscar o moddle na entidade pai
            do {

                $entidadeIntegracao = $this->findOneBy(
                    '\G2\Entity\EntidadeIntegracao', array('id_entidade' => $idEntidade)
                );

                if (!$entidadeIntegracao) {
                    throw new \Zend_Exception('Entidade não posui integração com o moodle.');
                }
                //verifica se exite entidade moodle na entidade

                if (!$entidadeIntegracao->getSt_codsistema()) {
                    $entidadeRelacao = $this->findOneBy(
                        '\G2\Entity\EntidadeRelacao', array('id_entidade' => $idEntidade)
                    );

                    if (!$entidadeRelacao) {
                        throw new \Zend_Exception('Entidade não posui integração com o moodle.');
                    }

                    $idEntidade = $entidadeRelacao->getId_entidadepai();
                } else {
                    $idEntidadeMoodle = $entidadeIntegracao->getId_entidade();
                }

            } while ($idEntidadeMoodle == false);

            $parametrosUsuarioLogado = $this->em->getRepository('\G2\Entity\Matricula')
                ->retornaParametroMoodle($params['id_saladeaula'], $this->sessao->id_usuario, $idEntidadeMoodle);

            if (!$parametrosUsuarioLogado) {
                throw new \Zend_Exception('Usuário atualmente logado no Portal não existe no Moodle.');
            }

            $parametrosAluno = $this->em->getRepository('\G2\Entity\Matricula')
                ->retornaParametroMoodle($params['id_saladeaula'], $aluno->getId_usuario());

            if (!$parametrosAluno) {
                throw new \Zend_Exception('Usuário atualmente aluno no Moodle.');
            }

            $moodleUsuario = new \MoodleUsuariosWebServices(
                $parametrosUsuarioLogado['id_entidade'], $parametrosUsuarioLogado['id_entidadeintegracao']
            );
            $existeUserMoodle = $moodleUsuario->retornarUsuarioByUserName(
                $parametrosUsuarioLogado['st_loginintegrado']
            );

            if (!$existeUserMoodle) {
                throw new \Zend_Exception('Usuário atualmente usuario logado não existe no Moodle.');
            }

            $USUARIO_MOODLE = $parametrosUsuarioLogado['st_loginintegrado'];
            $SENHA_MOODLE = $parametrosUsuarioLogado['st_senhaintegrada'];
            $CURSO_MOODLE = $parametrosUsuarioLogado['st_codsistemacurso'];
            $COD_SISTEMA_ENT = $parametrosUsuarioLogado['st_codsistema'];
            $ID_USER = $parametrosAluno['st_codusuario'];

            $sessaoMoodle = new \Zend_Session_Namespace('moodle');
            $sessaoMoodle->usuario = $USUARIO_MOODLE;
            $sessaoMoodle->senha = $SENHA_MOODLE;
            $sessaoMoodle->curso = $CURSO_MOODLE;
            $URL = substr($COD_SISTEMA_ENT, 0, -26) .
                "login/indexg2.php?u=" . $USUARIO_MOODLE . "&p=" . $SENHA_MOODLE .
                "&rel=log&id_sala=" . $CURSO_MOODLE . "&id_user=" . $ID_USER;

            return $this->mensageiro->setMensageiro($URL, \Ead1_IMensageiro::SUCESSO);

        } catch (\Zend_Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**Função para buscar TCC das matriculas que tem MC com tcc vinculado ou não
     * @param $params
     * @return array|\Ead1_Mensageiro
     */
    public function buscarTcc($params)
    {
        try {

            if (!array_key_exists('id_matricula', $params)) {
                throw new \Zend_Exception('Obrigatorio matricula');
            }

            $idMatriculaPrincipal = $params['id_matricula'];
            $where = array(
                'id_matriculavinculada' => $params['id_matricula']
            );

            $matriculas = $this->findBy('\G2\Entity\Matricula', $where);


            $arrayMatriculas = array();
            if ($matriculas) {
                foreach ($matriculas as $matricula) {
                    array_push($arrayMatriculas, $matricula->getId_matricula());
                }
                $params['id_matricula'] = $arrayMatriculas;
            }

            $whereAvaliacaoAluno = array(
                'id_tipoavaliacao' => TipoAvaliacao::TCC,
                'id_matricula' => $params['id_matricula']
            );

            $orderAvaliacaoAluno = array(
                'dt_cadastroavaliacao' => 'DESC'
            );

            //findOneBy PORQUE É PRA SEMRPRE TER UM TCC COMO VINCULADA
            $avaliacaoAluno = $this->findOneBy('\G2\Entity\VwAvaliacaoAluno', $whereAvaliacaoAluno, $orderAvaliacaoAluno);


            if ($avaliacaoAluno) {
                //retorna o TCC quando é uma vinculada
                return $this->toArrayEntity($avaliacaoAluno);
            }
            $whereAvaliacaoAlunoPrincipal = array(
                'id_tipoavaliacao' => TipoAvaliacao::TCC,
                'id_matricula' => $idMatriculaPrincipal
            );
            $avaliacaoAlunoPrincipal = $this->findOneBy(
                '\G2\Entity\VwAvaliacaoAluno', $whereAvaliacaoAlunoPrincipal, $orderAvaliacaoAluno);

            if ($avaliacaoAlunoPrincipal) {
                return $this->toArrayEntity($avaliacaoAlunoPrincipal);
            } else {
                return array();
            }
        } catch (\Zend_Exception $exception) {
            return new \Ead1_Mensageiro($exception->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }


    /**
     * @param $idMatricula
     * @return array
     * @throws \Exception
     */
    public function retornarMaterias($idMatriculas)
    {

        foreach ($idMatriculas as $idMatricula) {


            $arrMatriculas[] = $idMatricula;
            $arrayResult = [];

            //busca as matriculas vinculada a matricula passado por parametro
            $matriculas = $this->findBy(\G2\Entity\VwMatriculaRecursiva::class, ["id_matricula" => $idMatricula]);

            //se encontrar matriculas vinculada, adiciona no array de matriculas
            if ($matriculas) {
                foreach ($matriculas as $matricula) {

                    if ($matricula->getNu_matriculaorigem()) {
                        $arrMatriculas[] = $matricula->getNu_matriculaorigem();
                    }

                }
            }

            if (!$arrMatriculas) {
                throw  new \Exception("Nenhuma matricula informada");
            }

            //percorre o array de matricula consultando os cursos e suas disciplinas
            foreach ($arrMatriculas as $matricula) {
                $matriculacancelamentos = (new \G2\Entity\VwMatriculaCancelamento())
                    ->findBy(["id_matricula" => $matricula], ["dt_abertura" => "ASC"]);

                $arr = [];
                foreach ($matriculacancelamentos as $item) {
                    if ($item->getId_disciplina()) {
                        $arr[] = $item;
                    }
                }

                $transformer = (new MatriculaCancelamentoTransformer($arr))
                    ->merge("disciplinas")
                    ->on([
                        "id_matricula",
                        "id_projeto",
                        "st_projeto",
                        "id_evolucao",
                        "st_evolucao",
                        "st_aceite",
                        "nu_cargahorariaprojeto",
                        "nu_valortotalcurso",
                        "nu_chdisponibilizada",
                        "id_venda",
                        "dt_cancelamento",
                        "dt_matricula",
                        "dt_referencia"
                    ])
                    ->getTransformer();
                if ($transformer) {
                    array_push($arrayResult, $transformer[0]);
                }
            }
        }
        return $arrayResult;
    }

    public function atualizarMatriculaDisciplinaCancelamento($disciplinas){

        if(!$disciplinas){
            throw  new \Exception("Favor Informar Disciplinas");
        }
        foreach ($disciplinas as $matricula){
            $_idMatricula = $matricula['id_matricula'];
            foreach ($matricula['diciplinas'] as $disciplina){
                $_idDisciplina = $disciplina['id_disciplina'];

                /** @var \G2\Entity\MatriculaDisciplina[] $_matriculasDisciplinas * */
                $_matriculasDisciplinas = $this->findOneBy('\G2\Entity\MatriculaDisciplina',
                    array('id_matricula' => $_idMatricula,
                        'id_disciplina' => $_idDisciplina
                    ));

                if (!$_matriculasDisciplinas) {
                    throw  new \Exception("Nenhuma matricula informada");
                }
                $_matriculasDisciplinas->setBl_cancelamento($disciplina['bl_cancelamento']);

                $this->save($_matriculasDisciplinas);
            }
        }
        return array("sucesso" => "Matricula");
    }

    public function retornaMatriculaCancelamento($idCancelamento)
    {

        $arr = array();
        $customWhere = array(
            "tb.id_cancelamento =". $idCancelamento ." AND ( tb.id_evolucao = 40 OR tb.id_evolucao = 6 )"
        );

        $results = $this->findCustom('\G2\Entity\Matricula',$arr,$customWhere);

        if ($results) {
                return $this->toArrayEntity($results);

        } else {
            return new \Ead1_Mensageiro(array(), \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Verificar se a matrícula possui alguma venda que foi transferida de entidade
     * @param int $id_matricula
     * @return bool
     */
    public function transferidoEntidade($id_matricula)
    {
        $query = $this->em->createQueryBuilder()
            ->select('ve')
            ->from('\G2\Entity\VendaProduto', 'vp')
            ->join('\G2\Entity\Venda',
                've',
                \Doctrine\ORM\Query\Expr\Join::WITH,
                've.id_venda = vp.id_venda'
            )
            ->where('vp.id_matricula = :id_matricula')
            ->andWhere('vp.bl_ativo = :bl_ativo')
            ->andWhere('ve.bl_ativo = :bl_ativo')
            ->andWhere('ve.id_situacao != :id_situacao')
            ->andWhere('ve.id_evolucao != :id_evolucao')
            ->setParameters(array(
                'id_matricula' => $id_matricula,
                'bl_ativo' => 1,
                'id_situacao' => \G2\Constante\Situacao::TB_VENDA_INATIVO,
                'id_evolucao' => \G2\Constante\Evolucao::TB_VENDA_CANCELADA
            ));

        /** @var \G2\Entity\Venda[] $response */
        $response = $query->getQuery()->getResult();

        if ($response) {
            foreach ($response as $venda) {
                if ($venda->getBl_transferidoentidade()) {
                    return true;
                }
            }
        }

        return false;
    }

}

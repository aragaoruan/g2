<?php

namespace G2\Negocio;

/**
 * Classe de Negocio para PessoaEndereco
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2014-10-03
 */
class PessoaEndereco extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function gravaEndereco($body, $id_usuario)
    {

        try {
            $this->beginTransaction();
            $result = $this->find('\G2\Entity\Endereco', $body['id_enderecoentrega']);
            if (!$result) {
                $result = new \G2\Entity\Endereco();
            }
            $result->setSt_cep($body['st_cep']);
            $result->setSt_endereco($body['st_endereco']);
            $result->setNu_numero($body['nu_numero']);
            $result->setSt_complemento($body['st_complemento']);
            $result->setSt_bairro($body['st_bairro']);
            $result->setSt_cidade($body['st_cidade']);
            $result->setId_municipio($this->find('\G2\Entity\Municipio', $body['id_municipio']));
            $result->setSg_uf($body['sg_uf']);
            $result->setId_pais($this->find('\G2\Entity\Pais', $body['id_pais']));
            $result->setId_tipoendereco($this->find('\G2\Entity\TipoEndereco', 1));
            $result->setBl_ativo(true);
            $resultado = $this->save($result);

            $objetoVenda = $this->find('\G2\Entity\Venda', $body['id_vendaendereco']);
            $objetoVenda->setIdEnderecoentrega($result);
            $this->save($objetoVenda);


            $this->commit();
            return $resultado;
        } catch (Exception $exc) {
            $this->rollback();
            echo $exc->getTraceAsString();
        }
    }

    public function validaEndereco($id_venda){
        $objetoVenda = $this->find('\G2\Entity\Venda', $id_venda);
        if ($objetoVenda->getIdEnderecoentrega()) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * /**
     * Metodo responsavel por retornar os dados de uma pessoa, da vw_pessoaendereco
     * Caso tenha necessidade de retornar os dados em array basta enviar o param true,
     *
     * @update Neemias Santos
     * @param $dados
     * @param bool $array
     * @return array|\Ead1_Mensageiro|null|object
     * @throws \Zend_Exception
     */
    public function retornaVwPessoaEnderecoOne($dados, $array = false)
    {
        $result = $this->findOneBy('\G2\Entity\VwPessoaEndereco', $dados);
        if ($result) {
            if($array){
                $result = $this->toArrayEntity($result);
                return $result;
            }else{
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }
        } else {
            return new \Ead1_Mensageiro('Endereço não encontrado', \Ead1_IMensageiro::ERRO);
        }
    }

}
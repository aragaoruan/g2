<?php
/**
 * Created by PhpStorm.
 * User: helder.silva
 * Date: 12/12/2014
 * Time:
 **/

namespace G2\Negocio;

/**
 * Classe de negócio para VwSaladeAulaIntegracao
 * @author Helder Silva <rafael.rocha@unyleya.com.br>
 */
class VwSaladeAulaIntegracao extends Negocio
{

    private $repositoryName = 'G2\Entity\VwSaladeAulaIntegracao';

    public function __construct()
    {
        parent::__construct();
    }

    public function retornaSalaByEntityDisc($params = null)
    {
        return $this->findBy('G2\Entity\VwSaladeAulaIntegracao', array('id_entidade' => $this->sessao->id_entidade, 'id_disciplina' => $params['id_disciplina']));
    }

    /**
     * @param $body
     * @return \Ead1_Mensageiro
     * Método para integrar a sala existente, verifica se a sala já está integrada ou não.
     */
    public function integrarSalaExistente($body)
    {
        //verifica se a sala já está integrada
        if (!$this->findOneBy('\G2\Entity\SalaDeAulaIntegracao', array('id_saladeaula' => $body['id_saladeaula']))) {

            //se não estiver integrada faz uma cópia identica da sala a ser copiada e faz um insert na tabela tb_saladeaulaintegracao
            //com os dados copiados.

            $result = $this->findOneBy('\G2\Entity\SalaDeAulaIntegracao', array('id_saladeaula' => $body['id_saladeaulaexistente']));
            $objeto = new \G2\Entity\SalaDeAulaIntegracao();
            $objeto->setId_saladeaula($body['id_saladeaula']);
            $objeto->setId_usuariocadastro($result->getId_usuariocadastro());
            $objeto->setId_sistema($result->getId_sistema());
            $objeto->setSt_codsistemacurso($result->getSt_codsistemacurso());
            $objeto->setSt_codsistemasala($result->getSt_codsistemasala());
            $objeto->setSt_codsistemareferencia($result->getSt_codsistemareferencia());
            $objeto->setDt_cadastro($result->getDt_cadastro());
            $objeto->setBl_conteudo($result->getBl_conteudo());
            $objeto->setBl_visivel($result->getBl_visivel());
            $objeto->setSt_retornows($result->getSt_retornows());
            return new \Ead1_Mensageiro($this->save($objeto, \Ead1_IMensageiro::SUCESSO));
        } else {
            return new \Ead1_Mensageiro("Sala de aula já integrada!", \Ead1_IMensageiro::AVISO);
        }
    }

    /**
     * Verifica se a sala já está integrada.
     * @param $params
     * @return array
     */
    public function verificaSalaIntegracao($params)
    {
        return $this->findBy('\G2\Entity\SalaDeAulaIntegracao', array('id_saladeaula' => $params['id']));
    }

    /**
     * @param array $where
     * @param null $order
     * @param null $limit
     * @param int $offset
     * @return array
     */
    public function getDisciplinasProfessor($where = array(), $order = null, $limit = null, $offset = 0)
    {
        $response = array();

        /**
         * @var \G2\Repository\SalaDeAula $rp_saladeaula
         */
        $rp_saladeaula = $this->getRepository('\G2\Entity\VwSalaDeAulaProfessorIntegracao');
        $results = $rp_saladeaula->retornarDisciplinasProfessorPortal($where, $order, $limit, $offset);

        if (is_array($results) && $results) {
            foreach ($results as $result) {
                $response[] = array(
                    'id_disciplina' => $result['id_disciplina'],
                    'st_disciplina' => $result['st_disciplina']
                );
            }
        } else {
            $response[] = array(
                'id_disciplina' => '',
                'st_disciplina' => 'Nenhuma disciplina Encontrada'
            );
        }

        return $response;
    }

    public function getSalasProfessor($where = array(), $order = null)
    {
        $results = $this->findCustom('G2\Entity\VwSalaDeAulaProfessorIntegracao', $where, null, $order);

        $arrSalas = array();

        $entrar = \Ead1_Portal_Funcionalidade::verificaAcesso(479); //479	Entrar
        $gestao = \Ead1_Portal_Funcionalidade::verificaAcesso(480); //480	Gestão

        foreach ($results as &$result) {
            $result->lb_sala = '';

            if ($entrar) {
                $result->lb_sala .= '<label class="salaSelected" id="' . $result->getId_saladeaula() . '-' . $result->getId_disciplina() . '">Entrar</label>';
            }
            if ($gestao) {
                $result->lb_sala .= '&nbsp;&nbsp; | &nbsp;&nbsp;<label class="salaGestao" id="' . $result->getId_saladeaula() . '-' . $result->getId_disciplina() . '">Gestão</label>';
            }

            $arrSalas[$result->getId_disciplina()]['st_disciplina'] = $result->getSt_disciplina();
            $arrSalas[$result->getId_disciplina()]['disciplinas'][] = (object)$this->toArrayEntity($result);
        }

        return $arrSalas;
    }

    public function getVwSalaDeAulaProfessorIntegracao($where = array(), $order = null)
    {
        $results = $this->findBy('\G2\Entity\VwSalaDeAulaProfessorIntegracao', $where, $order);
        return $results;
    }
}

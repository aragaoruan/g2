<?php

namespace G2\Negocio;

/**
 */
class VwAvaliacaoConjuntoDisciplina extends Negocio {

    private $repositoryName = 'G2\Entity\VwAvaliacaoConjuntoDisciplina';

    public function __construct() {
        parent::__construct();
    }

    public function findByVw(array $where = NULL, array $order = NULL) {
//        \Zend_Debug::dump($where);die;
        $result = parent::findBy($this->repositoryName,$where,$order);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

}

<?php
/**
 * Classe de negócio para Horário Aula (vw_horarioaula)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */

namespace G2\Negocio;

class VwHorarioAula extends Negocio
{

    private $repositoryName = 'G2\Entity\VwHorarioAula';

    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($repositoryName = null)
    {
        $result = $this->findBy($this->repositoryName, array('id_entidade' => $this->sessao->id_entidade));
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = array(
                    'id_horarioaula' => $row->getId_horarioaula(),
                    'id_codhorarioacesso' => $row->getid_codhorarioacesso(),
                    'st_codhorarioacesso' => $row->getst_codhorarioacesso(),
                    'st_horarioaula' => $row->getSt_horarioaula(),
                    'hr_inicio' => date("H:i", strtotime($row->getHr_inicio())),
                    'hr_fim' => date("H:i", strtotime($row->getHr_fim())),
                    'id_turno' => array(
                        'id_turno' => $row->getId_turno(),
                        'st_turno' => $row->getSt_turno()
                    ),
                    'id_entidade' => array(
                        'id_entidade' => $row->getId_entidade(),
                        'st_nomeentidade' => $this->getReference('\G2\Entity\Entidade',$row->getId_entidade())->getSt_nomeentidade()
                    ),
                    'id_tipoaula'=>array(
                        'id_tipoaula'=>$row->getId_tipoaula(),
                        'st_tipoaula'=>$row->getSt_tipoaula()
                    ),
                    'diasemana' => array(
                        'bl_segunda' => $row->getBl_segunda(),
                        'bl_terca' => $row->getBl_terca(),
                        'bl_quarta' => $row->getBl_quarta(),
                        'bl_quinta' => $row->getBl_quinta(),
                        'bl_sexta' => $row->getBl_sexta(),
                        'bl_sabado' => $row->getBl_sabado(),
                        'bl_domingo' => $row->getBl_domingo(),
                    )
                );


            }

            return $arrReturn;
        }
    }

    public function delete($entity)
    {
        try {
            $entity->setBl_ativo(0);
            $this->save($entity);
            $this->em->flush();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function findHorario()
    {
        try {
            $repositoryHorarioAula = 'G2\Entity\VwHorarioAula';
            return $this->findby($repositoryHorarioAula, array('id_entidade' => $this->sessao->id_entidade));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


}
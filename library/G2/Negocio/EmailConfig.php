<?php

namespace G2\Negocio;

use Doctrine\Tests\DBAL\Types\DateTest;

/**
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class EmailConfig extends Negocio
{

    private $repositoryName = 'G2\Entity\EmailConfig';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     * @param integer $id
     * @return \G2\Entity\EmailConfig
     */
    public function getReference($id, $repositoryName = NULL)
    {
        return $this->em->getReference($this->repositoryName, $id);
    }

    public function findByEntidade()
    {
        return $this->findBy($this->repositoryName, array('id_entidade' => $this->sessao->id_entidade, 'bl_ativo' => true));
    }

    public function salva($array)
    {
        try {
            $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
            $objeto = new \G2\Entity\EmailConfig();
            if ($array['id_emailconfig']) {
                $objeto = $this->find("\G2\Entity\EmailConfig", $array['id_emailconfig']);
            }

            $array['id_usuariocadastro'] = $this->sessao->id_usuario;
            $array['id_entidade'] = $this->sessao->id_entidade;
            $newSerialize = $serialize->arrayToEntity($array, $objeto);

            if (!$array['id_emailconfig']) {
                $newSerialize->setDt_cadastro(new \DateTime());
            }
            return $this->save($newSerialize);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método para alterar o status de erro da configuração de e-mail
     * @param $id
     * @param $status
     * @return mixed
     * @throws \Zend_Exception
     */
    public function alteraStatusConfiguracao($id, $status)
    {
        try {
            $config = $this->find($this->repositoryName, $id);
            $config->setBl_erro($status);
            return $this->save($config);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao alterar status da configuração de e-mail. " . $e->getMessage());
        }
    }

}
<?php

namespace G2\Negocio;

use Doctrine\ORM\Query;
use G2\Constante\Evolucao;
use G2\Constante\Situacao;
use G2\Constante\TipoTramite;
use G2\Entity\OcorrenciaResponsavel;
use G2\Entity\Usuario;
use G2\Entity\VwOcorrenciasPessoa;
use G2\Utils\Helper;

/**
 * Classe de negócio para Tipo de Ocorrencia
 * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
 * @update Debora Castro; Denise Xavier
 * @update Alex Alexandre <alex.alexandre@unyleya.com.br>
 */
class Ocorrencia extends Negocio
{

    private $ocorrenciaRepository = 'G2\Entity\Ocorrencia';
    private $nucleoRepository = 'G2\Entity\NucleoCo';

    protected $entitySerialize;

    public function __construct()
    {
        parent::__construct();
        $this->entitySerialize = new \Ead1\Doctrine\EntitySerializer($this->em);
    }

    /**
     * Find By Funçao Co
     * Retorna dados para o combo de funcao na pesquisa de Ocorrência
     * @return \G2\Entity\VwNucleoPessoaCo[]
     */
    public function findFuncaoCo(array $params = array())
    {
        $repositoryName = $this->em->getRepository('G2\Entity\VwNucleoPessoaCo');

        $query = $repositoryName->createQueryBuilder('vw')
            ->select('DISTINCT vw.st_nucleoco, vw.id_nucleoco, vw.id_usuario, vw.id_funcao, vw.st_nomecompleto, vw.st_funcao, vw.id_textonotificacao')
            ->andWhere('vw.id_usuario = :id_usuario')
            ->setParameter('id_usuario', $this->sessao->id_usuario);

        if (empty($params['id_entidade'])) {
            $query->andWhere('vw.id_entidade = :id_entidade')
                ->setParameter('id_entidade', $this->sessao->id_entidade);
        }

        foreach ($params as $key => $value) {
            $query->andWhere('vw.' . $key . ' = :' . $key)
                ->setParameter($key, $value);
        }

        $result = $query->getQuery()->getResult();

        return $result;
    }

    /**
     * Find By AtendenteCo
     * @param integer $id_nucleoco
     * @param integer $id_funcao
     * @return object G2\Entity\VwNucleoPessoa
     * Retorna dados para o combo de ATENDENTE na pesquisa de Ocorrência
     * Ultima alteracao 22/04/14 por Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>,
     * retirados campos id_funcao e st_funcao e adicionado Ref: 01.
     */
    public function findByAtendenteCo($params)
    {
        try {

            $repositoryName = 'G2\Entity\VwNucleoPessoaCo';

            $query = null;
            $query .= ' AND vw.id_funcao IN(8, 10)'; //Ref: 01

            if (empty($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            foreach ($params as $key => $value) {
                if (!empty($value)) {
                    $query .= " AND vw.{$key} = '{$value}'";
                }
            }

            $result = $this->em->createQuery(
                "SELECT DISTINCT vw.st_nucleoco
                        , vw.id_nucleoco
                        , vw.id_usuario
                        , lower(vw.st_nomecompleto) AS st_nomecompleto
                        , vw.id_textonotificacao
                            FROM " . $repositoryName . " AS vw
                                  WHERE 1=1" . $query . ' ORDER BY st_nomecompleto ASC');

            return $result->getResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Find NucleoAssuntoCo
     * @param integer $id_tipoocorrencia
     * @return object G2\Entity\VwNucleoAssuntoCo
     * Retorna os núcleos para o tipo de ocorrência especificado por $id_tipoocorrencia
     */
    public function findNucleoAssuntoCo($params)
    {
        try {
            if (!array_key_exists('id_tipoocorrencia', $params)) {
                throw new \Zend_Exception('O parâmetro id_tipoocorrencia é obrigatório');
            }

            $repositoryName = $this->em->getRepository('G2\Entity\NucleoCo');

            $params['id_entidade'] = $this->sessao->id_entidade;

            $result = $repositoryName->findBy($params);

            return $result;
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Retorna dados para o combo de ASSUNTO na pesquisa de Ocorrência
     * @param array $params
     * @return array|string
     */
    public function findAssuntoCo($params)
    {
        try {
            if (!array_key_exists('id_nucleoco', $params)) {
                throw new \Zend_Exception('Falta o parâmetro id_nucleoco');
            }
            $repositoryName = $this->em->getRepository('G2\Entity\VwNucleoAssuntoCo');

            if (empty($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $params['bl_ativo'] = true;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_ASSUNTOCO_ATIVO;

            $query = $repositoryName->createQueryBuilder('vw')
                ->select('DISTINCT vw.id_assuntoco, vw.st_assuntoco, vw.id_tipoocorrencia, vw.id_nucleoco')
                ->andWhere('vw.st_assuntopai is null')
                ->orderBy('vw.st_assuntoco');

            foreach ($params as $key => $value) {
                if ($value) {
                    $query->andWhere('vw.' . $key . '= :' . $key)
                        ->setParameter($key, $value);
                }
            }

            return $query->getQuery()->getResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Retorna dados para o combo de SUBASSUNTO na pesquisa de Ocorrência
     * @param array $params
     * @return \G2\Entity\VwAssuntoCo[]|string
     */
    public function findSubAssuntoCo($params)
    {
        try {
            if (!array_key_exists('id_assuntocopai', $params)) {
                throw new \Zend_Exception('Falta o parâmetro id_assuntocopai');
            }

            if (empty($params['id_entidade'])) {
                $params['id_entidade'] = $this->sessao->id_entidade;
            }

            $params['bl_ativo'] = 1;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_ASSUNTOCO_ATIVO;

            return $this->findBy('\G2\Entity\VwAssuntos', $params, array('st_assuntoco' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo responsavel por saltar tipos de ocorrencia
     * @param array $array
     * @return \G2\Entity\CategoriaOcorrencia
     */
    public function salvarTipoOcorrencia(array $array)
    {

        $entityTipoOcorrencia = $this->find('\G2\Entity\TipoOcorrencia', $array['id_tipoocorrencia']);
        $entitySituacao = $this->find('\G2\Entity\Situacao', $array['id_situacao']);
        $entityUsuario = $this->find('\G2\Entity\Usuario', $this->sessao->id_usuario);
        $entityEntidade = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

        if (!array_key_exists('id', $array)) {
            $entity = new \G2\Entity\CategoriaOcorrencia();
            $entity->setId_tipoocorrencia($entityTipoOcorrencia)
                ->setId_situacao($entitySituacao)
                ->setId_usuariocadastro($entityUsuario)
                ->setSt_categoriaocorrencia($array['st_categoriaocorrencia'])
                ->setId_entidadecadastro($entityEntidade)
                ->setDt_cadastro(new \DateTime())
                ->setBl_ativo(true);
        } else {
            $entity = $this->find('\G2\Entity\CategoriaOcorrencia', $array['id']);
            $entity->setId_tipoocorrencia($entityTipoOcorrencia)
                ->setId_situacao($entitySituacao)
                ->setId_usuariocadastro($entityUsuario)
                ->setSt_categoriaocorrencia($array['st_categoriaocorrencia'])
                ->setId_entidadecadastro($entityEntidade)
                ->setDt_cadastro(new \DateTime())
                ->setBl_ativo(true);
        }

        return $this->save($entity);
    }

    /**
     * Metodo responsavel por apagar logicamente um tipo de ocorrencia
     * @param array $array
     * @return \G2\Entity\CategoriaOcorrencia
     */
    public function deletarTipoOcorrencia(array $array)
    {
        try {

            $entity = $this->find('\G2\Entity\CategoriaOcorrencia', $array['id']);

            $entity->setBl_ativo(false);
            $result = $this->save($entity);

            return $result;
        } catch (Zend_Exception $e) {
            return $e;
        }
    }

    public function findMinhasOcorrencias(array $where = array())
    {

        $repo = $this->em->getRepository('G2\Entity\VwMinhasOcorrencias');

        $result = $repo->findBy(array(
            'id_usuario' => $this->sessao->id_usuario,
            'id_entidade' => $this->sessao->id_entidade
        ), // $where
            array('id_ocorrencia' => 'desc'), // $orderBy
            null, // limit
            null // offset
        );


        return $result;
    }

    /**
     * Retorna um array com as colunas especificas para retornar a busca das ocorrencias
     * @return array
     */
    private function getColumnsForSelect()
    {
        return array(
            'vw.st_nomeinteressado',
            'vw.id_ocorrencia',
            'vw.id_ocorrencia as id',
            'vw.st_titulo',
            'vw.st_atendente',
            'vw.id_atendente',
            'vw.st_evolucao',
            'vw.id_evolucao',
            'vw.dt_cadastroocorrencia',
            'vw.dt_ultimotramite',
            'vw.nu_horastraso',
            'vw.nu_diasaberto',
            'vw.st_assuntoco',
            'vw.id_assuntoco',
            'vw.id_assuntocopai',
            'vw.st_assuntocopai',
            'vw.st_cor',
            'vw.st_emailinteressado',
            'vw.id_matricula',
            'vw.st_telefoneinteressado',
            'vw.st_ocorrencia',
            'vw.id_situacao',
            'vw.id_usuariointeressado',
            'vw.id_categoriaocorrencia',
            'vw.id_saladeaula',
            'vw.id_entidade',
            'vw.id_entidadematricula',
            'vw.st_entidadematricula',
            'vw.id_tipoocorrencia',
            'vw.st_situacao',
            'vw.st_ultimotramite',
            'vw.st_cadastroocorrencia',
            'vw.st_projetopedagogico',
            'vw.st_cpf',
            'vw.bl_resgate',
            'vw.id_nucleoco',
            'vw.id_funcao',
            'vw.st_urlacesso',
            'vw.st_urlportal',
            'vw.st_saladeaula',
            'vw.st_tituloexibicao',
            'vw.id_ocorrenciaoriginal',
            'vw.st_coordenadortitular'
        );
    }

    /**
     * @description Ser $limit for passado, retornara um objeto com a pagina, o total e os items, senao, retornara
     * somente o array de items
     * @param array $where
     * @param int $limit
     * @param int $offset
     * @param bool $optimizarSelect
     * @return array|object
     * @throws \Exception
     */
    public function findByOcorrenciaPessoa($where = array(), $limit = 0, $offset = 0, $optimizarSelect = false)
    {
        if (!is_array($where)) {
            $where = array();
        }

        /*
         * Verificação se esses parametros estão vindo na requisicao
         * retornando falso caso nao exista.
         *
         * @author Kayo Silva
         * Adicionei a segunda condição (!isset($where['id_ocorrencia']) || empty($where['id_ocorrencia']))
         * pois quando eu pesquisava a ocorrencia por id ele não buscava porque sempre pedia que fosse passado
         * os outros dois parametros.
         */
        if ((empty($where['id_funcao']) || empty($where['id_nucleoco'])) && (!isset($where['id_ocorrencia']) || empty($where['id_ocorrencia']))) {
            return false;
        }

        // View para buscar diretamente pelo ID da Ocorrência
        $view = '\G2\Entity\VwOcorrenciasPessoa';

        if (!empty($where['id_funcao']) && !empty($where['id_nucleoco'])) {
            $view = '\G2\Entity\VwOcorrenciasPessoaFuncao';

            // Verificar se o usuário está atribuído à TODOS os assuntos do núcleo em sua Função
            /** @var \G2\Entity\NucleoPessoaCo $nucleopessoa */
            $nucleopessoa = $this->findOneBy('\G2\Entity\NucleoPessoaCo', array(
                'id_usuario' => $this->sessao->id_usuario,
                'id_nucleoco' => $where['id_nucleoco'],
                'id_funcao' => $where['id_funcao'],
                'bl_todos' => true
            ));

            if ($nucleopessoa && $nucleopessoa->getBl_todos()) {
                $view = '\G2\Entity\VwOcorrenciasPessoaGeral';
            }
        }

        $repo = $this->em->getRepository($view);
        $query = $repo->createQueryBuilder('vw');

        if ($optimizarSelect) {
            $query->select($this->getColumnsForSelect());
        } else {
            $query->select('DISTINCT vw');
        }

        if (empty($where['id_entidade']) && empty($where['ignorar_entidade'])) {
            $where['id_entidade'] = $this->sessao->id_entidade;
        }

        if (empty($where['id_usuario'])) {
            $where['id_usuario'] = $this->sessao->id_usuario;
        }

        unset($where['ignorar_entidade']);

        if (array_key_exists('st_nomeinteressado', $where) and !empty($where['st_nomeinteressado'])) {
            $query->andWhere('vw.st_nomeinteressado LIKE :st_nomeinteressado  ')
                ->setParameter('st_nomeinteressado', "%{$where['st_nomeinteressado']}%");
        }
        if (array_key_exists('st_emailinteressado', $where) and !empty($where['st_emailinteressado'])) {
            $query->andWhere('vw.st_emailinteressado LIKE :st_emailinteressado')
                ->setParameter('st_emailinteressado', "%{$where['st_emailinteressado']}%");
        }

        if (array_key_exists('dt_cadastroocorrencia_inicio', $where) and array_key_exists('dt_cadastroocorrencia_fim', $where)) {
            if (($where['dt_cadastroocorrencia_inicio']) != '' and $where['dt_cadastroocorrencia_fim'] != '') {

                $where['dt_cadastroocorrencia_inicio'] = new \Zend_Date($where['dt_cadastroocorrencia_inicio']);
                $where['dt_cadastroocorrencia_fim'] = new \Zend_Date($where['dt_cadastroocorrencia_fim']);

                $query->andWhere('vw.dt_cadastroocorrencia BETWEEN :b1 AND :b2')
                    ->setParameter('b1', $where['dt_cadastroocorrencia_inicio']->toString('Y-M-d 00:00:00'))
                    ->setParameter('b2', $where['dt_cadastroocorrencia_fim']->toString('Y-M-d 23:59:59'));

                //Adicionando pesquisa de horario - apenas quando tem data
                if (array_key_exists('hr_inicio', $where) && $where['hr_inicio'] != '') {
                    $query->setParameter('b1', $where['dt_cadastroocorrencia_inicio']->toString('Y-M-d ' . $where['hr_inicio'] . ':00'));
                }
                if (array_key_exists('hr_fim', $where) && $where['hr_fim'] != '') {
                    $query->setParameter('b2', $where['dt_cadastroocorrencia_fim']->toString('Y-M-d ' . $where['hr_fim'] . ':00'));
                }
            }
        }

        // Adicionar filtro para verificar ocorrências não distribuídas (id_atendente)
        if (!empty($where['id_atendente']) && $where['id_atendente'] === 'nd') {
            $query->andWhere('vw.id_atendente IS NULL');
            unset($where['id_atendente']);
        }

        //Retirando atributos descartaveis utilizados somente para otimizacao e
        //nao necessarios na montagem da query dinamica
        unset($where['dt_cadastroocorrencia_inicio']);
        unset($where['dt_cadastroocorrencia_fim']);
        unset($where['st_nomeinteressado']);
        unset($where['st_emailinteressado']);
        unset($where['hr_inicio']);
        unset($where['hr_fim']);

        // Adicionando dinamicamente condicoes a query
        foreach ($where as $key => $value) {
            if ($value) {
                $query->andWhere('vw.' . $key . ' = :' . $key)
                    ->setParameter($key, $value);
            }
        }

        $page = 1;

        // Se possuir o parametro LIMIT, sera paginado
        if ($limit) {
            // Calcula o total a partir da mesma query, SEM ORDER BY e LIMITS
            try {
                $query_total = clone $query;
                $query_total->select('COUNT(DISTINCT vw.id_ocorrencia)');
                $query_total = $query_total->getQuery()->getSingleScalarResult();
            } catch (\Exception $e) {
                throw new \Exception("Erro ao tentar contabilizar total de registros.");
            }

            // PAGINACAO
            $query->setFirstResult($offset);
            $query->setMaxResults($limit);

            $page = round($offset / $limit) + 1;
        }

        // Aplica ORDER BY e LIMIT somente apos ter feito a consulta de contar o total
        $query->orderBy('vw.nu_ordem', 'ASC')->addOrderBy('vw.id_ocorrencia', 'ASC');

        if ($optimizarSelect) {
            $rows = $query->getQuery()->getArrayResult();
        } else {
            $rows = $query->getQuery()->getResult();
        }

        if ($limit) {
            return (object)array(
                'page' => $page,
                'offset' => (int)$offset,
                'limit' => (int)$limit,
                'total' => (int)$query_total,
                'rows' => $rows
            );
        } else {
            return $rows;
        }
    }


    /**
     * Metodo responsavel por salvar ocorrencia
     * @param array $data
     * @param bool $acompanhadores
     * @return array|\Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarOcorrencia(array $data, $acompanhadores = true)
    {
        try {
            $this->beginTransaction();
            if (array_key_exists('id_ocorrencia', $data) && $data['id_ocorrencia'] !== "" && $data['id_ocorrencia'] > 0) {
                $mensageiro = $this->editarOcorrencia($data);

                //verifica se o retorno do salvar ocorrencia não foi sucesso então retorna o mensageiro
                if ($mensageiro->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw  new \Exception($mensageiro->getText());
                }

                //pega a ocorrencia do mensageiro e atribui a variavel da ocorrencia
                $ocorrencia = $mensageiro->getFirstMensagem();


            } else {
                $cAtencaoNegocio = new CAtencao();
                $mensageiro = $cAtencaoNegocio->salvarOcorrencia($data['ocorrencia'], isset($data['arquivo']) ? $data['arquivo'] : null);
                //verifica se o retorno do salvar ocorrencia não foi sucesso então retorna o mensageiro
                if ($mensageiro->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw  new \Exception($mensageiro->getText());
                }

                //pega a ocorrencia do mensageiro e atribui a variavel da ocorrencia
                $ocorrencia = $mensageiro->getFirstMensagem();

                $atendente = false;
                if (isset($data['ocorrencia']['id_atendente']))
                    $atendente = $data['ocorrencia']['id_atendente'];


                if (array_key_exists('graduacao', $data) && $data['graduacao']) {
                    $cAtencaoNegocio->vinculaResponsavelDefinido($ocorrencia, $atendente);
                } else {
                    $cAtencaoNegocio->vinculaResposansavelOcorrencia($ocorrencia);
                }

            }

            // Gerenciar Acompanhadores
            if ($acompanhadores && isset($data['ocorrencia']['acompanhadores']))
                $this->salvaAcompanhadores($ocorrencia->getId_ocorrencia(), $data['ocorrencia']['acompanhadores']);

            $this->commit();

            return $mensageiro;

        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }


    /**
     * Método para editar a ocorrência
     * @param array $data
     * @return array
     * @throws \Zend_Exception
     */
    public function editarOcorrencia(array $data)
    {
        try {

            //busca os dados da ocorrencia
            $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $data['id_ocorrencia']);
            unset($data['id_ocorrencia']);

            //inicia uma variavel com array vazio para armazenar as mensagens do tramite
            $msgNovoTramite = [];
            //verifica se a evolução passada é diferente da evolucao atual
            if (isset($data['id_evolucao']) && $data['id_evolucao'] != $ocorrencia->getId_evolucao()->getId_evolucao()) {
                if (!isset($data['st_evolucao']) || empty($data['st_evolucao'])) {
                    $evolucao = $this->find('\G2\Entity\Evolucao', $data['id_evolucao']);
                    $data['st_evolucao'] = $evolucao->getSt_evolucao();
                }
                $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO, $ocorrencia->getId_evolucao()->getSt_evolucao(), $data['st_evolucao'], $ocorrencia->getId_usuariocadastro()->getSt_nomecompleto());
            }

            //verifica se a situacao passada é diferente da evolucao atual
            if (isset($data['id_situacao']) && $data['id_situacao'] != $ocorrencia->getId_situacao()->getId_situacao()) {
                if (!isset($data['st_situacao']) || empty($data['st_situacao'])) {
                    $situacao = $this->find('\G2\Entity\Situacao', $data['id_situacao']);
                    $data['st_situacao'] = $situacao->getSt_situacao();
                }

                $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO, $ocorrencia->getId_situacao()->getSt_situacao(), $data['st_situacao'], $ocorrencia->getId_usuariocadastro()->getSt_nomecompleto());
            }

            //seta os valores na entity

            $serialize = new \Ead1\Doctrine\EntitySerializer($this->getem());
            $ocorrencia = $serialize->arrayToEntity($data, $ocorrencia);

            //salva a entity
            if (!$this->save($ocorrencia)) {

                throw new \Exception('Erro ao editar a ocorrencia.');
            }

            if ($msgNovoTramite) {
                $date = new \Zend_Date(NULL, NULL, 'pt-BR');
                $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;

                $tramite = new \G2\Entity\Tramite();
                $tramite->setSt_tramite($stTramite)
                    ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO))
                    ->setId_entidade($ocorrencia->getId_entidade())
                    ->setBl_visivel(true)
                    ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

                $cAtencaoNegocio = new CAtencao();
                $cAtencaoNegocio->salvaTramite($ocorrencia, $tramite);
            }

            return new \Ead1_Mensageiro($ocorrencia, \Ead1_IMensageiro::SUCESSO);


        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar editar a ocorrência. " . $e->getMessage());
        }
    }


    /**
     * Método para criar o job no servidor para salvar a ocorrencia no JIRA
     * @param int $idOcorrencia
     */
    public function criarJobOcorrenciaJira($idOcorrencia)
    {
        try {
            if (!$idOcorrencia) {
                return;
            }

            $ocorrencia = $this->find('G2\Entity\VwOcorrencia', $idOcorrencia);;

            if ($ocorrencia) {

                /*
                 * se não existir o id da entidade para a execução, pois sem ele não é possível encontrar as configurações
                 * de integração com o JIRA
                 */

                if (!$ocorrencia->getId_entidade()) {
                    return;
                }

                $entidadeIntegracaoNegocio = new EntidadeIntegracao();
                $integracao = $entidadeIntegracaoNegocio->retornaEntidadeIntegracaoEntity(array(
                    'id_entidade' => $ocorrencia->getId_entidade(),
                    'id_sistema' => \G2\Constante\Sistema::JIRA
                ));

                //verifica se a entidade tem integração com o sistema do jira
                if (!$integracao) {
                    return;
                }

                //monta os parametros da issue para enviar ao job
                $issueData = $this->montaParametrosOcorrenciaIssue($ocorrencia, $integracao->getSt_codarea());
                //cria o job
                $schedule_time = date('Y-m-d H:i:s', time() + 10);
                $jq = $this->createHttpJob(
                    \Ead1_Ambiente::geral()->st_url_gestor2 . '/robo/zend-job-criar-ocorrencia-jira',
                    array(
                        'issue' => $issueData,
                        'jiraUrl' => $integracao->getSt_caminho(),
                        'jiraUser' => $integracao->getSt_codsistema(),
                        'jiraPwd' => $integracao->getSt_codchave(),
                        'issueReference' => $ocorrencia->getSt_codissue()
                    ), array(
                        'name' => 'Enviar Ocorrecia para o Jira.',
                        'schedule_time' => $schedule_time,
                        'queue_name' => 'JiraXG2'
                    )
                );
            }

            return;
        } catch (\Exception $e) {

        }
    }

    /**
     * Método para criar o job no zend server para enviar o tramite para o jira
     * @param $idOcorrencia
     * @param $interacao
     */
    public function criarJobInteracaoOcorrenciaJira($idOcorrencia, \TramiteTO $interacao)
    {
        try {

            //busca os dados da ocorrencia
            $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $idOcorrencia);
            if ($ocorrencia && $ocorrencia->getSt_codissue()) {

                $entidadeIntegracaoNegocio = new EntidadeIntegracao();
                $integracao = $entidadeIntegracaoNegocio->retornaEntidadeIntegracaoEntity(array(
                    'id_entidade' => $ocorrencia->getId_entidade()->getId_entidade(),
                    'id_sistema' => \G2\Constante\Sistema::JIRA
                ));

                //verifica se a entidade tem integração com o sistema do jira
                if (!$integracao) {
                    return;
                }


                //busca os dados do usuário
                $user = $this->findOneBy('\G2\Entity\VwPessoa', array(
                    'id_usuario' => $interacao->getId_usuario(),
                    'id_entidade' => $ocorrencia->getId_entidade()->getId_entidade()
                ));

                //busca o link do anexo
                $anexo = "";
                if ($interacao->getId_upload())
                    $anexo = $this->retornaStringLinksAnexos(array('id_upload' => $interacao->getId_upload()));

                //monta a string com o nome do autor e email
                $author = "Autor: " . $user->getst_nomecompleto() . " - " . $user->getSt_email();
                //monta o texto
                $texto = $interacao->getSt_tramite();


                $commentData = array(
                    'body' => $texto . "\n" . $anexo . "\n" . $author
                );

                //cria o job
                $schedule_time = date('Y-m-d H:i:s', time() + 10);
                $jq = $this->createHttpJob(
                    '/robo/zend-job-criar-interacao-ocorrencia-jira',
                    array(
                        'issueComment' => $commentData,
                        'jiraUrl' => $integracao->getSt_caminho(),
                        'jiraUser' => $integracao->getSt_codsistema(),
                        'jiraPwd' => $integracao->getSt_codchave(),
                        'issueReference' => $ocorrencia->getSt_codissue()
                    ), array(
                        'name' => 'Enviar Interação da Ocorrecia para o Jira.',
                        'schedule_time' => $schedule_time,
                        'queue_name' => 'JiraXG2'
                    )
                );


            }

            return;

        } catch (\Exception $e) {

        }
    }


    /**
     * Método para montar o array com os parametros para gerar a issue no jira
     * @param \G2\Entity\VwOcorrencia $ocorrencia
     * @return array
     */
    private function montaParametrosOcorrenciaIssue(\G2\Entity\VwOcorrencia $ocorrencia, $projetoKey = 'TEC')
    {

        //verifica se o solicitante da issue diferente do responsável
        if ($ocorrencia->getId_usuariointeressado() != $ocorrencia->getId_usuarioresponsavel() && !empty($ocorrencia->getId_usuarioresponsavel())) {
            $usuario = $this->findOneBy('\G2\Entity\VwPessoa', array(
                'id_usuario' => $ocorrencia->getId_usuarioresponsavel(),
                'id_entidade' => $ocorrencia->getId_entidade()
            ));
            $responsavel = array(
                'name' => $usuario->getSt_email(),
            );
        } else {
            $responsavel = array(
                'name' => $ocorrencia->getSt_email(),
            );
        }

        //busca os anexos da issue
        $anexos = "";
        if ($ocorrencia->getId_ocorrencia())
            $anexos = $this->retornaStringLinksAnexos(array('id_ocorrencia' => $ocorrencia->getId_ocorrencia()));


        //pega o  texto da ocorrencia e limpa a string removendo as tags.
        $texto = $ocorrencia->getSt_ocorrencia();

        $texto = strip_tags($texto);
        $texto = trim($texto . $anexos);

        //monta o array com os dados da issue
        $issueData = array(
            'fields' => array(
                'project' => array(
                    'key' => $projetoKey
                ),
                'summary' => $ocorrencia->getSt_titulo(),
                "issuetype" => array(
                    "id" => \G2\Constante\IssueTypes::SOLICITACAO
                ),
                'reporter' => array(
                    'name' => $ocorrencia->getSt_email(),
                ),
                'customfield_10400' => $ocorrencia->getId_ocorrencia(),
                'customfield_11011' => array($ocorrencia->getSt_email()),
                'assignee' => $responsavel,
                "description" => $texto,
            )
        );


        //verifica qual projeto é para criar a issue e se for o DDSSS ele omite o reporter
        if ($projetoKey == 'DDSSS') {
            unset($issueData['fields']['reporter']);
        }


        return $issueData;
    }

    /**
     * Método para buscar os anexos vinculados a issue e montar uma string para anexar ao corpo da mensagem
     * que será enviado para o jira.
     * @param array $params array(chave=>valor)
     * @return string
     */
    private function retornaStringLinksAnexos(array $params)
    {
        if ($params) {
            $result = $this->em->getRepository('\G2\Entity\VwOcorrenciaInteracao')
                ->retornarAnexosOcorrencia($params);

            $strLink = "";
            if ($result) {
                $strLink = "ANEXOS\n";
                $url = \Ead1_Ambiente::geral()->st_url_gestor2;
                foreach ($result as $item) {
                    $strLink .= "- " . $url . "/upload/tramite/" . $item['st_upload'] . "\n";
                }
            }

            return $strLink;
        }
    }


    /**
     * Metodo que procura perfis de usuario da entidade, que nao sejam aluons
     */
    public function getPerfilNaoAlunos($st_nomecompleto = false)
    {
        $repository = '\G2\Entity\VwUsuarioPerfilEntidade';
        // Criando a string do WHERE
        $query = "SELECT p FROM {$repository} p WHERE
        p.id_perfilpedagogico IS NULL AND p.id_entidade = {$this->sessao->id_entidade}";

        if ($st_nomecompleto)
            $query .= " AND p.st_nomecompleto LIKE '{$st_nomecompleto}%'";

        // Executa a query criada
        $query = $this->em->createQuery($query);
        $result = $query->execute();

        return $result;
    }

    /**
     * Metodo que gerencia acompnhadores
     * @param $idOcorrencia
     * @param array $arrayIdAcompanhadores
     * @return Object
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvaAcompanhadores($idOcorrencia, $arrayIdAcompanhadores = array())
    {
        try {
            $this->beginTransaction();

            $arrayIdAcompanhadores = (array)$arrayIdAcompanhadores;

            // Apagar todos os acompanhadores
            // Buscar OcorrenciaAcompanhante e remover todos
            // para poder inserir somente os selecionados
            $acompanhadores = $this->findBy('\G2\Entity\OcorrenciaAcompanhante', array(
                'id_ocorrencia' => $idOcorrencia
            ));
            // Apagar cada registro encontrado
            foreach ($acompanhadores as $acompanhadorEntity) {
                parent::delete($acompanhadorEntity);
            }

            // Adicionar os selecionados
            foreach ($arrayIdAcompanhadores as $key => $item) {
                $novo = new \G2\Entity\OcorrenciaAcompanhante();
                $novo->setId_ocorrencia($idOcorrencia);
                $novo->setId_usuario($item);
                $novo->setBl_ativo(true);
                if (!parent::save($novo)) {
                    throw new \Exception("Erro ao vincular Acompanhante para Ocorrencia.");
                }
            }
            $this->commit();
            return $this->findBy('\G2\Entity\OcorrenciaAcompanhante', array(
                'id_ocorrencia' => $idOcorrencia
            ));
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de encerrar ocorrencia
     * @updated reinaldo.pereira 03/07/2017 GII-7817 - Tratando campos da variável $data
     * @param array $data
     * @param \G2\Utils\User|null $user
     * @return VwOcorrenciasPessoa|string
     * @throws \Exception
     */
    public function encerrarOcorrencia(array $data, \G2\Utils\User $user = null)
    {
        try {
            $cAtencaoBO = new \CAtencaoBO();
            $ocorrenciaTO = new \OcorrenciaTO();

            if (empty($data['id_ocorrencia'])) {
                throw new \Exception("Id da ocorrência não reconhecido.");
            }
            if (empty($data['id_situacao'])) {
                throw new \Exception("Tipo de situação não reconhecida.");
            }
            if (empty($data['id_evolucao'])) {
                throw new \Exception("Tipo de evolução não reconhecida.");
            }
            $ocorrenciaTO->setId_ocorrencia($data['id_ocorrencia']);
            $ocorrenciaTO->setId_situacao($data['id_situacao']);
            $ocorrenciaTO->setId_evolucao($data['id_evolucao']);

            $result = $cAtencaoBO->encerrarOcorrencia($ocorrenciaTO, null, $user);
            if ($result->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($result->getText());
            }
            return $this->retornaVwOcorrenciasPessoa($ocorrenciaTO->getId_ocorrencia());
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de devolucao de ocorrencia
     * @param array $data
     * @return type
     * @throws \G2\Negocio\VwOcorrenciasPessoa
     */
    public function devolverOcorrencia(array $data, $st_tramite = null)
    {
        try {
            $cAtencaoBO = new \CAtencaoBO();
            $ocorrenciaTO = new \OcorrenciaTO();
            $ocorrenciaTO->setId_ocorrencia($data['id_ocorrencia']);
            $ocorrenciaTO->setId_situacao($data['id_situacao']);
            $ocorrenciaTO->setId_evolucao($data['id_evolucao']);
            $cAtencaoBO->devolverOcorrencia($ocorrenciaTO, $st_tramite);

            return $this->retornaVwOcorrenciasPessoa($ocorrenciaTO->getId_ocorrencia());
        } catch (Exception $ex) {
            throw $ex;
        }
    }


    /**
     * Metodo responsavel por chamar BO e executar acao de reabrir ocorrencia
     * @param array $data
     * @return VwOcorrenciasPessoa
     * @throws Zend_Exception
     */
    public function reabrirOcorrencia(array $data)
    {

        try {
            $cAtencaoBO = new \CAtencaoBO();
            $vwOcorrenciasPessoaTO = new \VwOcorrenciasPessoaTO();
            $vwOcorrenciasPessoaTO->setId_ocorrencia($data['id_ocorrencia']);

            //Verifica se os parametros de evolucao e situacao foram setados, se não tiverem sido completa o array $data
            if (!array_key_exists('id_situacao', $data) || !array_key_exists('id_evolucao', $data)) {
                $vwOcorrenciasPessoaTO->fetch(false, true, true);
                $data['id_situacao'] = $vwOcorrenciasPessoaTO->getId_situacao();
                $data['id_evolucao'] = $vwOcorrenciasPessoaTO->getId_evolucao();
            }
            $vwOcorrenciasPessoaTO->setId_situacao($data['id_situacao']);
            $vwOcorrenciasPessoaTO->setId_evolucao($data['id_evolucao']);
            $cAtencaoBO->reabrirOcorrencia($vwOcorrenciasPessoaTO);

            return $this->retornaVwOcorrenciasPessoa($vwOcorrenciasPessoaTO->getId_ocorrencia());
        } catch (Zend_Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de solicitar encerramento
     * @param array $data
     * @return VwOcorrenciasPessoa
     * @throws \Exception
     */
    public function solicitarEncerramento(array $data, \G2\Utils\User $user = null)
    {
        try {
            $cAtencaoBO = new \CAtencaoBO();
            $ocorrenciaTO = new \OcorrenciaTO();
            $ocorrenciaTO->setId_ocorrencia($data['id_ocorrencia']);
            $vwOcorrenciasPessoa = $this->retornaVwOcorrenciasPessoa($data['id_ocorrencia']);

            //Reutilizando metodo de encerramento antigo da BO
            $resultEncerramento = $cAtencaoBO->solicitaEncerramentoOcorrencia($ocorrenciaTO);


            if ($resultEncerramento->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception("Erro ao solicitar encerramento da ocorrencia. " . $resultEncerramento->getText());
            }


            if ($resultEncerramento->getCodigo() instanceof \OcorrenciaTO) {
                $ocorrenciaTO = $resultEncerramento->getCodigo();
            }


            /**
             * LEIA REGRA: se a evolucao ou a situacao da ocorrenia for alterada
             * sera gerado um tramite.
             */
            $date = new \Zend_Date(NULL, NULL, 'pt-BR');
            $msgNovoTramite = NULL;
            $arrayEvolucao = \G2\Constante\Evolucao::getArrayOcorrencia();
            $arraySituacao = \G2\Constante\Situacao::getArrayOcorrencia();

            //Guardando valores necessarios antes do update, para gerar o texto do tramite
            $stSituacaoNova = $arraySituacao[\G2\Constante\Situacao::TB_OCORRENCIA_AGUARDANDO_ENCERRAMENTO];
            $stEvolucaoNova = $arrayEvolucao[\G2\Constante\Evolucao::AGUARDANDO_INTERESSADO];

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_EVOLUCAO_TEXTO,
                $vwOcorrenciasPessoa->getSt_evolucao(), $stEvolucaoNova,
                $this->sessao->nomeUsuario ? $this->sessao->nomeUsuario : $user->getuser_name());

            $msgNovoTramite[] = sprintf(\G2\Constante\MensagemSistema::OCORRENCIA_ALTERACAO_SITUACAO_TEXTO,
                $vwOcorrenciasPessoa->getSt_situacao(), $stSituacaoNova,
                $this->sessao->nomeUsuario ? $this->sessao->nomeUsuario : $user->getuser_name());

            if ($msgNovoTramite) {
                $stTramite = 'A ' . implode(', ', $msgNovoTramite) . ' em ' . $date;
                $this->gerarNovaInteracao(array('ocorrencia' => $ocorrenciaTO->toArray(), 'st_tramite' => $stTramite), NULL, FALSE, TRUE);
            }
            //Fim Regra

            return $this->retornaVwOcorrenciasPessoa($ocorrenciaTO->getId_ocorrencia());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de encaminhar ocorrencia
     * @param array $data
     * @return \G2\Negocio\VwOcorrenciasPessoa
     * @throws \Exception
     */
    public function encaminharOcorrencia(array $data)
    {
        try {
            $negocio = new CAtencao();
            $vwOcorrenciaPessoaTo = new VwOcorrenciasPessoa();
            $vwOcorrenciaPessoaTo->setId_ocorrencia($data['id_ocorrencia']);
            $vwOcorrenciaPessoaTo->setId_atendente($data['id_atendente']);
            $vwOcorrenciaPessoaTo->setSt_assuntocopai($data['st_assuntocopai']);
            $vwOcorrenciaPessoaTo->setId_assuntoco($data['id_assuntoco']);
            $vwOcorrenciaPessoaTo->setSt_assuntoco($data['st_assuntoco']);
            $vwOcorrenciaPessoaTo->setSt_atendente($data['st_atendente']);
            $vwOcorrenciaPessoaTo->setId_evolucao($data['id_evolucao']);
            $vwOcorrenciaPessoaTo->setId_situacao($data['id_situacao']);

            $mensageiro = $negocio->encaminharOcorrencia($vwOcorrenciaPessoaTo);

            if ($mensageiro->getTipo() !== \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($mensageiro->getText());
            }


            return $this->retornaVwOcorrenciasPessoa($vwOcorrenciaPessoaTo->getId_ocorrencia());
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de gerar copia de ocorrencia
     * @param array $data
     * @return type
     * @throws \G2\Negocio\VwOcorrenciasPessoa
     */
    public function gerarCopiaOcorrencia(array $data)
    {
        $cAtencaoBO = new \CAtencaoBO();
        $vwOcorrenciaPessoaTo = new \VwOcorrenciasPessoaTO($data);
        $cAtencaoBO->gerarCopiaOcorrencia($vwOcorrenciaPessoaTo);

        return $this->retornaVwOcorrenciasPessoa($vwOcorrenciaPessoaTo->getId_ocorrencia());
    }

    /**
     * Metodo responsavel por retornar dados de ocorrencia
     * @param type $id
     * @return \G2\Entity\VwOcorrenciasPessoa
     */
    public function retornaVwOcorrenciasPessoa($id)
    {
        return $this->find('G2\Entity\VwOcorrenciasPessoa', $id);
    }


    /**
     * Metodo responsavel por retornar dados de ocorrencia
     * @param type $id
     * @return \G2\Entity\VwOcorrencia
     */
    public function retornaVwOcorrencia($id)
    {
        return $this->find('G2\Entity\VwOcorrencia', $id);
    }

    /**
     * Metodo responsavel por retornar dados de ocorrencia
     * @param $param
     * @param $return_array
     * @return array
     * @entity G2\Entity\Ocorrencia
     */
    public function retornaOcorrencia($param, $return_array)
    {
        $result = $this->findBy('G2\Entity\Ocorrencia', $param);

        if ($result) {
            if ($return_array) {
                $resultado = array();
                foreach ($result as $value) {
                    $resultado[] = $this->toArrayEntity($value);
                }
                return $resultado;
            }
        }
        return $result;
    }

    /**
     * Lista de interacoes de uma ocorrencia ordenados pela data mais atual
     * @param array $params
     * @param bool $returnEntity
     * @return \G2\Entity\VwOcorrenciaInteracao[]|array|string
     */
    public function retornaVwOcorrenciaInteracao(array $params, $returnEntity = true)
    {
        try {
            $repo = $this->em->getRepository('\G2\Entity\VwOcorrenciaInteracao');

            $query = $repo->createQueryBuilder('vw')
                ->select('DISTINCT vw')
                ->orderBy('vw.dt_cadastro', 'DESC');

            foreach ($params as $key => $value) {
                if ($value) {
                    $query->andWhere('vw.' . $key . ' = :' . $key)
                        ->setParameter($key, $value);
                }
            }

            if ($returnEntity) {
                return $query->getQuery()->getResult();
            } else {
                return $query->getQuery()->setHint(Query::HINT_INCLUDE_META_COLUMNS, true)->getArrayResult();
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo responsavel por chamar BO e executar acao de gerar nova interacao
     * @param array $data
     * @param null $arquivo
     * @param null $bl_interessado
     * @param null $bl_notificacao
     * @param null $sistema
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function gerarNovaInteracao(array $data, $arquivo = null, $bl_interessado = null, $bl_notificacao = null, $sistema = null)
    {

        $cAtencaoBO = new \CAtencaoBO();

        //Se a data['ocorrencia'] não for um array não continua o processo
        if (!is_array($data['ocorrencia'])) {
            return (new \Ead1_Mensageiro())
                ->setMensageiro('Erro ao tentar realizar nova interacao: Dados da ocorrência incorretos.',
                    \Ead1_IMensageiro::ERRO);
        }

        //Preenchendo a TO de ocorrencia
        $dataOcorrencia = $data['ocorrencia'];
        $ocorrenciaTO = new \OcorrenciaTO();
        $ocorrenciaTO->setId_ocorrencia($dataOcorrencia['id_ocorrencia']);
        $ocorrenciaTO->setId_evolucao($dataOcorrencia['id_evolucao']);

        if (array_key_exists('id_matricula', $dataOcorrencia) && $dataOcorrencia['id_matricula'])
            $ocorrenciaTO->setId_matricula($dataOcorrencia['id_matricula']);

        $ocorrenciaTO->setId_usuariointeressado($dataOcorrencia['id_usuariointeressado']);

        if (array_key_exists('id_saladeaula', $dataOcorrencia) && $dataOcorrencia['id_saladeaula'])
            $ocorrenciaTO->setId_saladeaula($dataOcorrencia['id_saladeaula']);

        if (isset($dataOcorrencia['id_entidade']))
            $ocorrenciaTO->setId_entidade($dataOcorrencia['id_entidade']);

        //Preenchendo a TO de tramite
        $tramiteTO = new \TramiteTO();
        $tramiteTO->setSt_tramite($data['st_tramite']);
        $tramiteTO->setId_entidade($this->sessao->id_entidade ? $this->sessao->id_entidade : $ocorrenciaTO->getId_entidade());
        $tramiteTO->setId_usuario($this->sessao->id_usuario ? $this->sessao->id_usuario : $ocorrenciaTO->getId_usuariointeressado());
        $tramiteTO->setDt_cadastro(new \DateTime());
        $tramiteTO->setBl_visivel($bl_notificacao);


        $result = $cAtencaoBO->novaInteracao($ocorrenciaTO, $tramiteTO, $arquivo, $bl_notificacao, $bl_interessado, $sistema);


        //verifica se o mensageiro retornou sucesso
        if ($result->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
            //chama o método que cria o job da interacao da ocorrencia no jira
            $this->criarJobInteracaoOcorrenciaJira($ocorrenciaTO->getId_ocorrencia(), $tramiteTO);

            //busca o tramite salvo
            $resultTramite = $this->retornaVwOcorrenciaInteracao(array(
                'id_tramite' => $result->getFirstMensagem()
            ));
            //verifica se encontrou o tramite e seta ele no mensageiro
            if ($resultTramite) {
                $result->setMensageiro($this->toArrayEntity($resultTramite));
            }
        }
        return $result;

    }

    /**
     * Metodo responsavel por adicionar atendente a uma ocorrencia.
     * @param array $data
     * @return \G2\Entity\VwOcorrenciasPessoa
     */
    public function addResponsavelOcorrencia(array $data)
    {

        try {
            $this->beginTransaction();

            if (!$this->sessao->id_usuario) {
                throw new \Exception("Sessão inválida");
            }

            if (empty($data['id_ocorrencia'])) {
                throw new \Exception("Id inválido");
            }

            $referenceUsuario = $this->find('G2\Entity\Usuario', $this->sessao->id_usuario);
            $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $data['id_ocorrencia']);
            $referenceSituacao = $this->find('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_OCORRENCIA_EM_ANDAMENTO);
            $referenceEvolucao = $this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::SENDO_ATENDIDO);

            $entity = new \G2\Entity\OcorrenciaResponsavel();
            $entity->setBl_ativo(true)
                ->setDt_cadastro(new \DateTime())
                ->setId_ocorrencia($data['id_ocorrencia'])
                ->setId_usuario($referenceUsuario);
            $this->save($entity);

            $ocorrencia->setId_situacao($referenceSituacao);
            $ocorrencia->setId_evolucao($referenceEvolucao);

            $this->save($ocorrencia);

            $this->commit();

            return $this->retornaVwOcorrenciasPessoa($entity->getId_ocorrencia());
        } catch (Zend_Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro('Já existe um usuário atendendo essa ocorrência', \Ead1_IMensageiro::AVISO);
//            Zend_Debug::dump($e);
        }
    }

    /**
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     * Find By AtendenteCoEncaminhar
     * @param integer $params
     * @return object G2\Entity\VwNucleoPessoa
     * Retorna dados para o combo de ATENDENTE para combo de encaminhar
     */
    public function findAtendenteCoEncaminhar($params)
    {
        try {
            $repositoryName = 'G2\Entity\VwNucleoPessoaCo';
            $query = '';

            $params['id_entidade'] = !empty($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;

            //Set tipos de funcao permitidos para retornar atendentes para encaminhamento
            $query .= ' AND vw.id_funcao IN(8, 10)';
            $query .= ' AND vw.id_assuntoco IN ( ' . $params['id_assuntoco'] . ', ' . $params['id_assuntocopai'] . ')';
            $query .= ' AND vw.id_entidade = ' . $params['id_entidade'];

            $result = $this->em->createQuery(
                "SELECT DISTINCT vw.st_nucleoco
                        , vw.id_nucleoco
                        , vw.id_usuario
                        , vw.id_funcao
                        , vw.st_funcao
                        , lower(vw.st_nomecompleto) AS st_nomecompleto
                        , vw.id_textonotificacao
                            FROM " . $repositoryName . " AS vw
                                  WHERE 1=1" . $query . ' ORDER BY st_nomecompleto ASC');

            return $result->getResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Metodo que e chamado no inicio da aplicacao para buscar possiveis
     * ocorrencias pendentes.
     * @param array $where
     * @return Object
     */
    public function findVwOcorrenciaAguardando($where)
    {
        return $this->findBy('\G2\Entity\VwOcorrenciaAguardando', $where);
    }

    /**
     * Metodo responsavel por chamar metodos antigos das BO's e checkar no banco
     * de dados a confirmacao de ciencia do atendentes nas ocorrencias pendentes.
     * @param $data
     * @return \Ead1_Mensageiro|\Exception|\Zend_Exception
     * @throws \Exception
     */
    public function confirmarCienciaOcorrenciaPendente($data)
    {

        try {

            if (!is_array($data)) {
                throw new \Zend_Exception('A função confirmarCienciaOcorrenciaPendente espera um array');
            }
            foreach ($data as $value) {

                $where = array(
                    'id_ocorrencia' => $value['id_ocorrencia']
                );
                $ocorrencia = $this->findOneBy('G2\Entity\Ocorrencia', $where);

                if ($ocorrencia) {

                    //Atualizando ocorrencia com bl_confirmarciencia = true
                    $ocorrencia->setBl_confirmarciencia(true);
                    $this->save($ocorrencia);

                    //Gerando texto do tramite
                    $stTramite = 'O atendente ' . $this->sessao->nomeUsuario .
                        ' tomou ciencia da ocorrência nº ' . $value['id_ocorrencia'];

                    //Chamando metodo que cria tramite para ocorrencia
                    $this->gerarNovaInteracao(array('ocorrencia' => $value, 'st_tramite' => $stTramite));
                } else {
                    throw new \Zend_Exception('Ocorrencia não encontrado');
                }

            }
            return new \Ead1_Mensageiro(' Confirmar ciencia executado com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Exception $ex) {
            return new \Ead1_Mensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo responsavel por retornar lista de ocorrencias filtrando por
     * parametros e utilizando metodo doctrine findBy
     * @param array $where
     * @return array
     */
    public function findByVwOcorrenciasPessoa(array $where)
    {
        return $this->findBy('\G2\Entity\VwOcorrenciasPessoa', $where, array('nu_ordem' => 'ASC', 'dt_cadastroocorrencia' => 'ASC'));
    }


    /**
     * Metodo responsavel por apagar logicamente os responsaveis de uma ocorrencia
     * @param array $array
     * @return
     */
    public function deletarOcorrenciaResponsavel(array $array)
    {
        try {
            $result = NULL;
            $entitys = $this->findBy('\G2\Entity\OcorrenciaResponsavel', $array);
            foreach ($entitys as $entity) {
                $entity->setBl_ativo(false);
                $result = $this->save($entity);
            }
            return $result;
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    /**
     * Método que verifica quantas ocorrências estão em andamento no assunto antes de desvincula-lo do núcleo
     * @param $params
     * @return string
     */
    public function verificaOcorrenciaAssunto($params)
    {
        try {

            $repositoryName = 'G2\Entity\VwOcorrenciasPessoa';

            $query = null;

            $result = $this->em->createQuery(
                "SELECT count(oc.id_ocorrencia) AS nu_ocorrencias
                            FROM " . $repositoryName . " AS oc
                                  WHERE oc.id_assuntoco = " . $params->getId_assuntoco()
                . " AND oc.id_nucleoco = " . $params->getId_nucleoco()
                . " AND oc.id_situacao <> 99");
            return $result->getResult();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Find By VwAssuntos
     * @return object G2\Entity\VwAssuntos
     * Retorna dados para o combo de ASSUNTO para abrir ocorrência por aluno
     */
    public function findAssuntoAluno($params)
    {
        try {
            $repositoryName = $this->em->getRepository('G2\Entity\VwAssuntos');

            $params['id_entidade'] = $this->sessao->id_entidade;
            $params['bl_abertura'] = true;
            $params['id_situacao'] = \G2\Constante\Situacao::TB_ASSUNTOCO_ATIVO;

            $query = $repositoryName->createQueryBuilder('vw')
                ->select('DISTINCT vw.id_assuntoco, vw.st_assuntoco, vw.id_tipoocorrencia')
                ->andWhere('vw.id_assuntocopai is null')
                ->orderBy('vw.st_assuntoco');

            foreach ($params as $key => $value) {
                if ($value) {
                    $query->andWhere('vw.' . $key . '= :' . $key)
                        ->setParameter($key, $value);
                }
            }

            return $query->getQuery()->getResult();

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param array $whereLike
     * @param array $where
     * @param array $order
     * @param null $limit
     * @param int $offset
     * @return array
     */
    public function findBySearchVwGestaoDeOcorrencias($whereLike = array(), $where = array(), $order = array(), $limit = null, $offset = 0)
    {
        if ($where || $whereLike) {
            $repository = $this->em->getRepository('\G2\Entity\VwGestaoDeOcorrencias');
            $query = $repository->createQueryBuilder("vw")
                ->select('vw')
                ->where('1 = 1');

            $where = array_filter($where);
            $whereLike = array_filter($whereLike);

            if (isset($where['dt_inicio']) && isset($where['dt_fim'])) {
                $query->andWhere("vw.dt_cadastro BETWEEN '{$where['dt_inicio']}' AND '{$where['dt_fim']}'");
                unset($where['dt_inicio'], $where['dt_fim']);
            }

            foreach ($where as $attr => $value) {
                $query->andWhere("vw.{$attr} = '{$value}'");
            }

            if ($whereLike) {
                foreach ($whereLike as $attr => $value) {
                    $query->andWhere("vw.{$attr} LIKE '%{$value}%'");
                }
            }

            if ($order) {
                $andOrder = false;
                foreach ($order as $attr => $sort) {
                    if ($andOrder)
                        $query->addOrderBy("vw.{$attr}", $sort);
                    else {
                        $query->orderBy("vw.{$attr}", $sort);
                        $andOrder = true;
                    }
                }
            }

            if ($limit) {
                $query->setFirstResult($offset);
                $query->setMaxResults($limit);
            }

            return $query->getQuery()->getResult();
        } else {
            return $this->findBy('\G2\Entity\VwGestaoDeOcorrencias', $where, $order, $limit, $offset);
        }
    }


    /**
     * @param $dados
     * @return \Ead1_Mensageiro
     */
    public function gerarDuplicidadeOcorrencia($dados)
    {
        $this->em->beginTransaction();
        try {
            if ($validacao = $this->validaCamposObrigatorios($dados, array('id_ocorrencia', 'st_ocorrencia', 'st_naotratada', 'st_tratada'))) {
                throw new \Exception($validacao);
            }
            //verifica id_assuntoduplicidade da entidade
            $configuracaoEntidade = $this->findOneBy('G2\Entity\ConfiguracaoEntidade', array('id_entidade' => $this->sessao->id_entidade));
            if (($configuracaoEntidade instanceof \G2\Entity\ConfiguracaoEntidade) && ($configuracaoEntidade->getId_assuntoduplicidade() instanceof \G2\Entity\AssuntoCo)
                && ($configuracaoEntidade->getId_assuntoduplicidade()->getId_assuntoco())
            ) {
                $ocorrencia = $this->em->find('G2\Entity\Ocorrencia', $dados['id_ocorrencia']);
                if (!$ocorrencia instanceof \G2\Entity\Ocorrencia) {
                    throw new \Exception('Ocorrência não encontrada!');
                }
                $copyOcorrencia = new \G2\Entity\Ocorrencia();
                $copyOcorrencia->setId_usuariointeressado($ocorrencia->getId_usuariointeressado());
                $copyOcorrencia->setSt_titulo($ocorrencia->getSt_titulo());
                $copyOcorrencia->setSt_ocorrencia($ocorrencia->getSt_ocorrencia());
                $copyOcorrencia->setId_assuntoco($configuracaoEntidade->getId_assuntoduplicidade());
                $copyOcorrencia->setId_categoriaocorrencia($ocorrencia->getId_categoriaocorrencia());
                $copyOcorrencia->setId_saladeaula($ocorrencia->getId_saladeaula());
                $copyOcorrencia->setId_matricula($ocorrencia->getId_matricula());
                $copyOcorrencia->setId_evolucao($this->find('G2\Entity\Evolucao', Evolucao::AGUARDANDO_ATENDIMENTO));
                $copyOcorrencia->setId_situacao($this->find('G2\Entity\Situacao', Situacao::TB_OCORRENCIA_PENDENTE));
                $copyOcorrencia->setId_entidade($ocorrencia->getId_entidade());
                $copyOcorrencia->setId_usuariocadastro($this->find('G2\Entity\Usuario', $this->sessao->id_usuario));
                $copyOcorrencia->setdt_atendimento(new \DateTime());
                $copyOcorrencia->setDt_cadastro(new \DateTime());
                $copyOcorrencia->setdt_cadastroocorrencia(new \DateTime());
                $copyOcorrencia->setId_ocorrenciaoriginal($ocorrencia);

                //\Zend_Debug::dump($this->toArrayEntity($copyOcorrencia));
                $this->save($copyOcorrencia);

                if ($copyOcorrencia instanceof \G2\Entity\Ocorrencia && $copyOcorrencia->getId_ocorrencia()) {
                    //Gerando texto do tramite de duplicidade
                    $stTramite = 'A ocorrência Nº ' . $copyOcorrencia->getId_ocorrencia() . ' foi criada a partir de uma cópia da ocorrência Nº '
                        . $ocorrencia->getId_ocorrencia() . '. Sendo que na ocorrência ' . $ocorrencia->getId_ocorrencia() . ' será tratado [' .
                        $dados['st_tratada'] . '] e não será tratado [' . $dados['st_naotratada'] . ']';

                    $tramite = new \G2\Entity\Tramite();
                    $tramite->setSt_tramite($stTramite);
                    $tramite->setId_tipotramite($this->find('G2\Entity\TipoTramite', TipoTramite::OCORRENCIA_AUTOMATICO));
                    $tramite->setId_entidade($this->find('G2\Entity\Entidade', $this->sessao->id_entidade));
                    $tramite->setBl_visivel(false);
                    $tramite->setId_usuario($this->find('G2\Entity\Usuario', $this->sessao->id_usuario));
                    $tramite->setDt_cadastro(new \DateTime());

                    //Chamando metodo que cria tramite para ocorrencia original
                    $cAtencaoNegocio = new \G2\Negocio\CAtencao();
                    $cAtencaoNegocio->salvaTramite($ocorrencia, $tramite);

                    //Chamando metodo que cria tramite para ocorrencia duplicada
                    $cAtencaoNegocio->salvaTramite($copyOcorrencia, $tramite);

                } else {
                    throw new \Exception('Ocorreu um erro ao salvar a ocorrência de duplicidade.');
                }
            } else {
                throw new \Exception('A entidade não possui id_assuntoduplicidade cadastrado. Verifique com o setor responsável.');
            }

            $this->em->commit();
            return new \Ead1_Mensageiro('Ocorrências duplicadas com sucesso!', \Ead1_IMensageiro::SUCESSO, $copyOcorrencia->getId_ocorrencia());

        } catch (\Exception $e) {
            $this->em->rollback();
            //\Zend_Debug::dump($e->getMessage());die;
            return new \Ead1_Mensageiro('Erro ao duplicar ocorrências: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Metodo responsavel por retornar lista de ocorrencias (vw_ocorrencia) filtrando por parametros
     * @param array $where
     * @return array|boolean
     */
    public function findByVwOcorrencia(array $where)
    {
        try {
            return $this->findBy('\G2\Entity\VwOcorrencia', $where, array('id_ocorrencia' => 'ASC'));
        } catch (\Zend_Exception $e) {
            return false;
        }
    }

    /**
     * Busca as ocorrências relacionadas
     * Busca tanto as ocorrencias que tem id_ocorrenciaoriginal como as que foram criadas a partir de outra
     * Ex.: SELECT * FROM vw_ocorrencia where id_ocorrencia = 149957 -- é o id_ocorrenciaorinal da 152911
     * OR id_ocorrenciaoriginal = 152911 -- se ocorrencia 152911 tem ocorrencias que foram criadas a partir dela
     * @param $id_ocorrencia
     * @return array|bool
     */
    public function findOcorrenciasRelacionadas($id_ocorrencia)
    {
        try {
            $id_ocorrenciaoriginal = null;
            if (!(int)$id_ocorrencia) {
                throw new \Zend_Exception('O id_ocorrencia é obrigatorio e não foi passado.');
            }
            $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $id_ocorrencia);
            $return = $this->findBy('\G2\Entity\VwOcorrencia', array('id_ocorrenciaoriginal' => $id_ocorrencia), array('id_ocorrencia' => 'ASC'));
            if ($ocorrencia instanceof \G2\Entity\Ocorrencia && $ocorrencia->getId_ocorrenciaoriginal() instanceof \G2\Entity\Ocorrencia) {
                $return[] = $this->find('\G2\Entity\VwOcorrencia', $ocorrencia->getId_ocorrenciaoriginal()->getId_ocorrencia());
            }
            return $return;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @param $params
     * @return array
     */
    public function getServicos($params)
    {
        $arrRetorno = array();
        $valorTotalProdutos = 0;
        $objetoOcorrencia = $this->find('\G2\Entity\Ocorrencia', $params);
        if ($objetoOcorrencia->getId_venda()) {
            $objetoVendaProduto = $this->findBy('\G2\Entity\VendaProduto', array('id_venda' => $objetoOcorrencia->getId_venda()));
            $arrRetorno['id_ocorrencia'] = $objetoOcorrencia->getId_ocorrencia();
            $arrRetorno['st_codigorastreio'] = $objetoOcorrencia->getSt_codigorastreio();
            foreach ($objetoVendaProduto as $key => $value) {
                $arrRetorno['st_produto'][$key]['id_produto'] = $value->getId_produto()->getId_produto();
                $arrRetorno['st_produto'][$key]['st_produto'] = $value->getId_produto()->getSt_produto();
                $arrRetorno['st_produto'][$key]['nu_valorproduto'] = number_format($value->getNu_valorliquido(), 2, ',', '');

                $objetoProdutoTextoSistema = $this->findOneBy('\G2\Entity\ProdutoTextoSistema',
                    array('id_produto' => $value->getId_produto()->getId_produto()));

                $arrRetorno['st_produto'][$key]['bl_gratuito'] = $value->getId_produto()->getNu_gratuito();
                $arrRetorno['st_produto'][$key]['id_matricula'] = $value->getId_matricula()->getId_matricula();
                $arrRetorno['st_produto'][$key]['id_textosistema'] = $objetoProdutoTextoSistema->getId_textosistema();
                $arrRetorno['st_produto'][$key]['id_formadisponibilizacao'] = $objetoProdutoTextoSistema->getId_formadisponibilizacao();
                $arrRetorno['st_produto'][$key]['id_ocorrencia'] = $objetoOcorrencia->getId_ocorrencia();

                $objetoEntregaDeclaracao = $this->findOneBy('\G2\Entity\EntregaDeclaracao',
                    array('id_vendaproduto' => $value->getId_vendaproduto()));
                $arrRetorno['st_produto'][$key]['id_situacao'] = $objetoEntregaDeclaracao->getId_situacao()->getId_situacao();
                $arrRetorno['st_produto'][$key]['st_situacao'] = $objetoEntregaDeclaracao->getId_situacao()->getSt_situacao();

                if (!$value->getId_produto()->getNu_gratuito())
                    $valorTotalProdutos += $value->getNu_valorliquido();

//                $objetoDeclaracaoEntrega = $this->findBy('\G2\Entity\EntregaDeclaracao');
            }
            $arrRetorno['totalProdutos'] = number_format($valorTotalProdutos, 2, ',', '');


        }
        return $arrRetorno;
    }

    /**
     * @param $params
     * @return mixed|string
     */
    public function salvarCodigoRastreio($params)
    {
        try {
            $objetoOcorrencia = $this->find('\G2\Entity\Ocorrencia', $params['id_ocorrencia']);
            $objetoOcorrencia->setSt_codigorastreio($params['st_codigorastreio']);
            return $this->save($objetoOcorrencia);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $params
     * @return bool|string
     */
    public function salvarEvolucaoServico($params)
    {
        try {
            $negocio = new \G2\Negocio\Negocio();
            $objetoOcorrencia = $negocio->find('\G2\Entity\Ocorrencia', $params['id_ocorrencia']);
            if ($objetoOcorrencia->getId_venda()) {
                $objetoVendaProduto = $negocio->findOneBy('\G2\Entity\VendaProduto', array('id_venda' => $objetoOcorrencia->getId_venda(), 'id_produto' => $params['id_produto']));
                if ($objetoVendaProduto) {
                    $objetoEntregaDeclaracao = $negocio->findOneBy('\G2\Entity\EntregaDeclaracao', array('id_vendaproduto' => $objetoVendaProduto->getId_vendaproduto()));
                    if ($objetoEntregaDeclaracao) {
                        $objetoEntregaDeclaracao->setId_situacao($params['id_situacao']);
                        $negocio->save($objetoEntregaDeclaracao);
                        return true;
                    }
                    return false;
                }
                return false;
            }
            return false;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $params
     * @return array|string
     */
    public function retornaOcorrenciasRelacionadasAproveitamento($params)
    {
        try {
            $arr = array();
            if (array_key_exists('id_aproveitamento', $params) && $params['id_aproveitamento']) {
                $resultado = $this->findBy('\G2\Entity\OcorrenciaAproveitamento', $params);
                if ($resultado) {
                    foreach ($resultado as $value) {
                        array_push($arr, $this->find('\G2\Entity\Ocorrencia', $value->getId_ocorrencia()->getId_ocorrencia()));
                    }
                }

            };
            return $arr;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $id
     * @return array
     */
    public function findOcorrencia($id)
    {
        $entity = $this->find('\G2\Entity\Ocorrencia', $id);

        if ($entity) {
            $arrRetorno = $this->toArrayEntity($entity);
            $arrRetorno['id_assuntoco'] = $arrRetorno['id_assuntoco']['id_assuntoco'];
            $arrRetorno['id_categoriaocorrencia'] = $arrRetorno['id_categoriaocorrencia']['id_categoriaocorrencia'];
            $arrRetorno['id_entidade'] = $arrRetorno['id_entidade']['id_entidade'];
            $arrRetorno['id_evolucao'] = $arrRetorno['id_evolucao']['id_evolucao'];
            $arrRetorno['id_matricula'] = $arrRetorno['id_matricula']['id_matricula'];
            $arrRetorno['id_situacao'] = $arrRetorno['id_situacao']['id_situacao'];
            $arrRetorno['id_usuariocadastro'] = $arrRetorno['id_usuariocadastro']['id_usuario'];
            $arrRetorno['id_usuariointeressado'] = $arrRetorno['id_usuariointeressado']['id_usuario'];
            $arrRetorno['id_ocorrenciaoriginal'] = $arrRetorno['id_ocorrenciaoriginal']['id_ocorrencia'];

            $arrRetorno['id_entidadematricula'] = null;
            $arrRetorno['st_entidadematricula'] = null;

            if ($entity->getId_matricula()) {
                $entidade = $this->find('\G2\Entity\Entidade', $entity->getId_matricula()->getId_entidadematricula());
                $arrRetorno['id_entidadematricula'] = $entidade->getId_entidade();
                $arrRetorno['st_entidadematricula'] = $entidade->getSt_nomeentidade();
            }
        }
        $atendente = $this->findBy('\G2\Entity\OcorrenciaResponsavel', array('id_ocorrencia' => $id, 'bl_ativo' => true));
        if ($atendente)
            $arrRetorno['id_atendente_solicitacao'] = $atendente[0]->getId_usuario()->getId_usuario();

        return $arrRetorno;
    }

    /**
     * pesquisa ocorrencias de uma dada matricula
     *
     * @param integer[] $like ['id_entidade', 'id_matricula']
     * @param string[] $order
     * @return string[]
     * */
    public function findByOcorrenciaMatricula(array $like, array $order)
    {
        return $this->findBy('\G2\Entity\VwOcorrencia', $like, $order);
    }

    /**
     * Método para retornar as ocorrencias dos alunos para o aplicativo
     * @param $idAluno
     * @param $idEntidade
     * @return mixed
     * @throws \Exception
     */
    public function getOcorrenciasAlunos($idAluno, $idEntidade, $idOcorrencia = null, $order = 'dt_ultimaacao', $direction = 'DESC')
    {
        try {

            //busca o vinculo com compartilhamento de dados
            $entidadesCompartilhadas = $this->findBy('\G2\Entity\CompartilhaDados', array(
                'id_entidadeorigem' => $idEntidade,
                'id_tipodados' => \G2\Constante\TipoDados::LOGIN
            ));

            //cria um array vazio
            $arrCompartilhamentoDados = [];

            //verifica se há vinculo com compartilhamento de dados
            if ($entidadesCompartilhadas) {
                //percorre os registro retornando no array somente o id_entidadedestino
                $arrCompartilhamentoDados = array_map(function ($compartilhamento) {
                    return $compartilhamento->getId_entidadedestino();
                }, $entidadesCompartilhadas);

            }
            //cria uma posição no array com o id_entidade passado por parametro
            $arrCompartilhamentoDados[] = $idEntidade;

            //busca as ocorrencias passando os parametros
            $ocorrencias = $this->em->getRepository('\G2\Entity\VwOcorrencia')
                ->retornarOcorrenciasAluno($idAluno, array_unique($arrCompartilhamentoDados), $idOcorrencia, $order, $direction);

            //retorn as ocorrencias
            return $ocorrencias;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna as categorias da Ocorrencia
     * @param array $params
     * @return Object
     * @throws \Zend_Exception
     */
    public function getCategoriaOcorrencia(array $params = [])
    {
        return $this->findBy('\G2\Entity\CategoriaOcorrencia', $params, array('st_categoriaocorrencia' => 'asc'));
    }

    /**
     * Busca a primeira categoria ocorrencia de acordo com os parametros
     * @param array $params
     * @return null|object
     * @throws \Exception
     */
    public function getFirstCategoriaOcorrencia(array $params = [])
    {
        try {
            return $this->findOneBy('\G2\Entity\CategoriaOcorrencia', $params);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @description Corrigir as Ocorrências que possuem o id_entidade diferente do id_entidade do núcleo
     * @param array $params
     * @return array
     */
    public function corrigirEntidadeOcorrencias($params = array())
    {
        $success = array();
        $error = array();

        /**
         * @var \G2\Entity\VwOcorrenciasEntidadeIncorreta[] $results
         */
        $results = $this->findBy('\G2\Entity\VwOcorrenciasEntidadeIncorreta', $params);

        if (is_array($results) && $results) {
            foreach ($results as $result) {
                try {
                    /**
                     * @var \G2\Entity\Ocorrencia $en_ocorrencia
                     */
                    $en_ocorrencia = $this->find('\G2\Entity\Ocorrencia', $result->getId_ocorrencia());
                    $en_ocorrencia->setId_entidade($this->getReference('\G2\Entity\Entidade', $result->getId_entidadenucleoco()));
                    $this->save($en_ocorrencia);
                    $success[] = $result;
                } catch (\Exception $e) {
                    $error[] = $result;
                }
            }
        }

        return array(
            'results' => $results,
            'error' => $error,
            'success' => $success
        );
    }

    /**
     * Método para retornar os assuntos das ocorrencias
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function getAssuntosOcorrencia(array $params = [])
    {
        $result = $this->findBy('\G2\Entity\VwAssuntos', $params, array('st_assuntoco' => 'asc'));

        $arrReturn = [];
        if ($result) {
            foreach ($result as $item) {
                $arrReturn[] = $this->toArrayEntity($item);
            }
        }
        return $arrReturn;
    }

    /**
     * Método para alterar a situação da ocorrẽncia
     * @param array $params
     * @return boolean
     * @throws \Zend_Exception
     */
    public function alterarSituacaoOcorrecia($params)
    {
        try {
            $enOcorrencia = $this->find('\G2\Entity\Ocorrencia', $params['id_ocorrencia']);
            if ($enOcorrencia instanceof \G2\Entity\Ocorrencia) {
                $enOcorrencia->setId_situacao($this->getReference('\G2\Entity\Situacao', (int)$params['id_situacao']));
                if ($this->save($enOcorrencia)) {
                    return true;
                }
                return false;
            }
            return false;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Método para alterar a evolução da ocorencia Pai para Pendencia Solucionada
     * @param $params
     * @return array
     * @throws \Zend_Exception
     */
    public function updateOcorrenciaVinculada($param)
    {
        try {
            $contavinculadaFilho = $this->toArrayEntity(
                $this->findOneBy(
                    '\G2\Entity\Ocorrencia',
                    array(
                        'id_ocorrencia' => $param
                    )
                )
            );

            if (array_key_exists('id_ocorrenciaoriginal', $contavinculadaFilho)
                && $contavinculadaFilho['id_ocorrenciaoriginal'] != null
                && $contavinculadaFilho['id_ocorrenciaoriginal'] != ''
            ) {

                $contavinculadaFilho['id_situacao'] = 99;
                $contavinculadaFilho['id_evolucao'] = Evolucao::PENDENCIA_SOLUCIONADA;
                $this->salvarOcorrencia($contavinculadaFilho);

                $msg = "Ocorrência vinculada id " . $contavinculadaFilho['id_ocorrencia'] . " foi encerrada";
                $this->tramiteOcorrenciaVinculada(
                    $contavinculadaFilho['id_ocorrenciaoriginal']['id_ocorrencia'],
                    $msg
                );

                $where = array(
                    'id_ocorrenciaoriginal' => $contavinculadaFilho['id_ocorrenciaoriginal']['id_ocorrencia'],
                    'id_evolucao' => 'NOT IN (' . Evolucao::PENDENCIA_SOLUCIONADA . ')'

                );

                $contarOcorrenciaVinculada = count(
                    $this->toArrayEntity(
                        $this->findCustom(
                            '\G2\Entity\Ocorrencia',
                            $where
                        )
                    )
                );

                // Atualizar ocorrencia pai para PENDENCIA_SOLUCIONADA
                if ($contarOcorrenciaVinculada == 0) {
                    $entity = $this->findOcorrencia($contavinculadaFilho['id_ocorrenciaoriginal']);
                    $entity['id_evolucao'] = \G2\Constante\Evolucao::PENDENCIA_SOLUCIONADA;
                    $this->salvarOcorrencia($entity);
                }


                return $contavinculadaFilho;
            } else {
                return false;
            }


        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Metodo para adcionar tramite em ocoorencia
     * @param $ocorrencia
     * @param $msg
     * @throws \Exception
     */
    public function tramiteOcorrenciaVinculada($idcorrencia, $msg)
    {
        try {

            /**
             * Criando um Novo Tramite
             */
            $tramiteEntity = new \G2\Entity\Tramite();
            $idTipoTramite = $this->em->find(
                'G2\Entity\TipoTramite',
                \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO
            );
            $idUsuario = $this->em->find('G2\Entity\Usuario', $this->sessao->id_usuario);
            $idEntidade = $this->em->find('G2\Entity\Entidade', $this->sessao->id_entidade);

            $tramiteEntity->setId_tipotramite($idTipoTramite);
            $tramiteEntity->setId_usuario($idUsuario);
            $tramiteEntity->setId_entidade($idEntidade);
            $tramiteEntity->setSt_tramite($msg);
            $tramiteEntity->setDt_cadastro(new \DateTime());
            $tramiteEntity->setBl_visivel(true);

            $negocioTramite = new \G2\Negocio\Tramite();

            $negocioTramite->save($tramiteEntity);

            /**
             * Salva os tramites.
             */
            $cAtencao = new \G2\Negocio\CAtencao();
            $negocioOcorrencia = new \G2\Negocio\Ocorrencia();
            $where = array(
                'id_ocorrencia' => $idcorrencia
            );
            $ocorrencia = $negocioOcorrencia->findOneBy('\G2\Entity\Ocorrencia', $where);

            $cAtencao->salvaTramite($ocorrencia, $tramiteEntity);


        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Função responsável por contar quantas ocorrencias
     * cada atendente tem
     *
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     * @param $idAtendente
     * @return mixed
     */
    public function getNumOcorrenciaAtendente($idAtendente)
    {
        $ocR = "G2\Entity\OcorrenciaResponsavel";

        $qtd = $this->em->getRepository($ocR)->createQueryBuilder("ocR")
            ->select("COUNT(ocR.id_usuario) AS qtd")
            ->join($this->ocorrenciaRepository, "oc", "WITH",
                "ocR.id_ocorrencia = oc.id_ocorrencia")
            ->where("oc.id_evolucao NOT IN (36, 38)")
            ->andWhere("oc.id_entidade = :id_entidade")
            ->andWhere("ocR.id_usuario = :id_atendente")
            ->setParameter("id_entidade", $this->getId_entidade())
            ->setParameter("id_atendente", $idAtendente)
            ->getQuery()->getResult();

        return $qtd[0]["qtd"];
    }

    /**
     * Função responsável por fazer o tratamento dos paramêtros
     * e montar um array de acordo com o esperando pela função
     * $this->salvarOcorrencia($data), para então salvar as ocorrências
     *
     * @param $aluno
     * @param $salaDeAula
     * @param $atendente
     * @param $assuntoCo
     * @throws \Exception
     * @return \G2\Entity\Ocorrencia
     */
    public function tratarDadosNovaOcorrencia($aluno, $salaDeAula, $atendente, $assuntoCo)
    {

        $this->validaParamCriarOcEmLote($aluno, $salaDeAula, $atendente, $assuntoCo);

        /**
         * Recupera o id_assuntoco de acordo com o parametro $assuntoCO
         */
        try {

            $whereNucleo = array(
                "id_entidade" => $this->sessao->id_entidade,
                "bl_ativo" => 1,
                "id_tipoocorrencia" => 1
            );
            $nucleoResult = $this->findBy($this->nucleoRepository, $whereNucleo);
            $nucleoResult = $this->toArrayEntity($nucleoResult);

            $assuntoCo == "resgate" ? $idAssuntoCO = $nucleoResult[0]["id_assuntocoresgate"]
                : $idAssuntoCO = $nucleoResult[0]["$assuntoCo"];


            $idMatricula = $this->findOneBy(
                "\G2\Entity\VwMatricula",
                array("id_usuario" => $aluno));
            $idMatricula = $this->toArrayEntity($idMatricula);

        } catch (\Exception $e) {
            throw new \Exception("Erro, não foi possível encontrar o assunto.");
        }

        try {

            $data = ([
                "ocorrencia" =>
                    [
                        "id_evolucao" => \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO,
                        "id_situacao" => \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE,
                        "id_categoriaocorrencia" => 1,
                        "id_usuariocadastrado" => $this->sessao->id_usuario,
                        "id_atendente" => $atendente,
                        "id_assuntoco" => $idAssuntoCO,
                        "id_usuariointeressado" => $aluno,
                        "id_saladeaula" => $salaDeAula,
                        "id_matricula" => $idMatricula["id_matricula"],
                        "st_titulo" => "Ocorrencia Gerada via Recuperar Aluno",
                        "st_ocorrencia" => "Ocorrencia Gerada via Sistema > Recuperar Aluno",
                        "id_entidade" => $this->getId_entidade(),
                        "dt_cadastro" => new \DateTime()
                    ],
                "graduacao" => true
            ]);

            return $ocorrencia = $this->salvarOcorrencia($data);


        } catch (\Exception $e) {
            throw new \Exception("Erro, não foi possível criar as ocorrencias em lote." . $e->getMessage());
        }

    }

    /**
     * Função responsável por verificar se todos os parametros estão de acordo
     * com o exigido. Para então liberar para criar uma ocorrência.
     *
     * @return bool
     */
    private function validaParamCriarOcEmLote($aluno, $salaDeAula, $atendente, $assuntoCo)
    {
        if (!$aluno || !$salaDeAula || !$atendente || !$assuntoCo) {
            throw new Exception("Erro! Existem campos vazios que são obrigatórios.");
        }
        return true;
    }

    /**
     * Função responsável por chamar o repository ocorrencia
     * para buscar todos os atendentes de um assunto.
     *
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     * @return array
     */
    public function getAtendente($tipoServico)
    {
        $tipoServico == "resgate" ? $colunaTipoServico = "id_assuntocoresgate"
            : $colunaTipoServico = "id_assuntorecuperacao";


        $atendentes = $this->em->getRepository($this->ocorrenciaRepository)
            ->retornaAtendentesOcorrencia($this->getId_entidade(), $colunaTipoServico);

        $retornoAtendentes = array();

        if (is_array($atendentes) && $atendentes) {

            foreach ($atendentes as $result) {
                $arRetorno[] = array(
                    "id_usuario" => $result["id_usuario"],
                    "st_nomecompleto" => $result["st_nomecompleto"]
                        . " - "
                        . $this->getNumOcorrenciaAtendente($result["id_usuario"])
                );

            }

            return $arRetorno;
        }

        return $retornoAtendentes;
    }


    public function atualizarOcorrenciaVisivel($params)
    {
        try {

            /** @var '\G2\Entity\Tramite $entity */
            $entity = $this->find('\G2\Entity\Tramite', $params['id_tramite']);
            $entity->setBl_visivel($params['bl_visivel']);
            return $this->save($entity);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Função que encerra todas as ocorrencias de uma matriculas canceladas
     * @param null $id_matricula
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function roboEncerrarOcorrencia($id_matricula = null)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();
            $parameterOuput = $id_matricula;
            $this->beginTransaction();

            if (is_null($id_matricula)) {
                $id_matricula = $this->em->getRepository('\G2\Entity\Ocorrencia')
                    ->retornarMatriculasCanceladas();
                if (empty($id_matricula)) {
                    return $mensageiro->setMensageiro(
                        "Não existem ocorrêcias para serem canceladas", \Ead1_IMensageiro::AVISO);
                } else {
                    foreach ($id_matricula as $_id) {
                        $parameterOuput[] = $_id['id_matricula'];
                    }
                }
            }

            $where = array(
                'id_matricula' => "IN (" . implode(",", $parameterOuput) . ")",
                'id_situacao' => "<> " . \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA
            );

            $entityOcorrencia = $this->findCustom('\G2\Entity\Ocorrencia', $where);

            foreach ($entityOcorrencia as $ocorrencia) {

                $entityMatricula = $this->findOneBy('\G2\Entity\Matricula',
                    array('id_matricula' => $ocorrencia->getId_matricula()->getId_matricula()));

                $entityTramite = new \G2\Entity\Tramite();
                $entityTramite->setBl_visivel(true)
                    ->setId_entidade($this->getReference('G2\Entity\Entidade',
                        $entityMatricula->getId_entidadematricula()))
                    ->setId_tipotramite($this->getReference(
                        'G2\Entity\TipoTramite', TipoTramite::OCORRENCIA_AUTOMATICO))
                    ->setId_usuario($this->getReference('G2\Entity\Usuario',
                        $entityMatricula->getId_usuario()->getId_usuario()))
                    ->setSt_tramite('Ocorrência encerrada devido ao cancelamento da matrícula.')
                    ->setDt_cadastro(new \DateTime());

                $tramite = $this->save($entityTramite);

                $ocorrencia->setId_situacao($this->getReference(
                    'G2\Entity\Situacao', \G2\Constante\Situacao::TB_OCORRENCIA_ENCERRADA));
                $ocorrencia->setId_evolucao($this->getReference(
                    'G2\Entity\Evolucao', \G2\Constante\Evolucao::ENCERRAMENTO_REALIZADO_INTERESSADO));
                $this->save($ocorrencia);

                $entityTramiteOcorrencia = new \G2\Entity\TramiteOcorrencia();
                $entityTramiteOcorrencia->setId_tramite($tramite->getId_tramite());
                $entityTramiteOcorrencia->setId_ocorrencia($ocorrencia->getId_ocorrencia());
                $entityTramiteOcorrencia->setBl_interessado(false);

                $this->save($entityTramiteOcorrencia);

            }

            $this->commit();
            return $mensageiro->setMensageiro("Ocorrências encerradas com sucesso", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            return $mensageiro->setMensageiro(
                "Erro ao encerrar ocorrencias" . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Função que retorna ocorrencia com a evolução Aguardando interesado
     * @param $id_matricula
     * @return mixed
     * @throws \Exception
     */
    public function ocorrenciaAguardandoInteresado($id_matricula)
    {

        try {
            return $this->em->getRepository($this->ocorrenciaRepository)
                ->ocorrenciaAguardandoInteresado($id_matricula);
        } catch (\Exception $e) {
            throw new \Exception("Erro ao tentar consultar ocorrencia." . $e->getMessage());
        }
    }

    /**
     * Função que adiciona no tramite da ocorrencia que o aluno visualisou o toou ciencia da ocorrencia
     * @param $ocorrencias
     * @param $nomeBotao
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function aguardandoRespostaTramite($ocorrencias, $nomeBotao, $idUsuarioOriginal = 0)
    {

        try {
            //Se não estiver sendo acessado por usuário do G2
            if ($idUsuarioOriginal == 0) {
                $this->beginTransaction();
                foreach ($ocorrencias as $ocorrencia) {
                    $stTramite = 'O aluno ' . $nomeBotao . ' ocorrência nº ' .
                        $ocorrencia['id_ocorrencia'] . ' em ' . Helper::converterData(new \DateTime(), 'd/m/Y');

                    $entityOcorrencia = $this->findOneBy('\G2\Entity\Ocorrencia',
                        array("id_ocorrencia" => $ocorrencia['id_ocorrencia']));
                    $tramite = new \G2\Entity\Tramite();
                    $tramite->setSt_tramite($stTramite)
                        ->setId_tipotramite($this->getReference('G2\Entity\TipoTramite',
                            \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO))
                        ->setId_entidade($entityOcorrencia->getId_entidade())
                        ->setBl_visivel(true)
                        ->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

                    $cAtencaoNegocio = new CAtencao();
                    $menssagem = $cAtencaoNegocio->salvaTramite($entityOcorrencia, $tramite);
                }

                $this->commit();
                return $menssagem;
            }
            return new \Ead1_Mensageiro('Acessando com usuário do G2.', \Ead1_IMensageiro::SUCESSO, null);
        } catch (\Exception $e) {
            $this->rollBack();
            throw new \Exception("Erro ou criar tramite." . $e->getMessage());
        }

    }

    /**
     * Função responsável por encaminhar ocorrências em lote para outro Atendente
     * GII-9400
     *
     * @param $params
     * @param $idOcorrencia
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function encaminharOcorrenciasEmLote($params, $idOcorrencia)
    {
        if (!$idOcorrencia) {
            throw new \Zend_Exception('O parâmetro id_ocorrencia é obrigatório');
        }

        try {
            $this->beginTransaction();
            $tramiteMsg = $this->gerarMensagemTramiteEncaminharOcorrencia($idOcorrencia, $params);

            $ocorrenciaEntity = $this->findOneBy($this->ocorrenciaRepository, array('id_ocorrencia' => $idOcorrencia));
            $ocorrenciaEntity->setId_assuntoco($this->getReference(\G2\Entity\AssuntoCo::class, $params['id_assuntoco']));
            $this->save($ocorrenciaEntity);

            //Necessario para poder mudar o responsável pela ocorrencia
            $this->deletarOcorrenciaResponsavel(array('id_ocorrencia' =>$idOcorrencia, 'bl_ativo' => true));

            $ocorrenciaResponsavel = new OcorrenciaResponsavel();
            $ocorrenciaResponsavel->setDt_cadastro(new \DateTime());
            $ocorrenciaResponsavel->setBl_ativo(true);
            $ocorrenciaResponsavel->setId_ocorrencia($idOcorrencia);
            $ocorrenciaResponsavel->setId_usuario($this->getReference('G2\Entity\Usuario', $params['id_usuario']));
            $this->save($ocorrenciaResponsavel);

            $tramiteEntity = new \G2\Entity\Tramite();
            $tramiteEntity->setSt_tramite($tramiteMsg);
            $tramiteEntity->setId_tipotramite($this->getReference('G2\Entity\TipoTramite', \G2\Constante\TipoTramite::OCORRENCIA_AUTOMATICO));
            $tramiteEntity->setId_entidade($this->getReference(\G2\Entity\Entidade::class, $this->getId_entidade()));
            $tramiteEntity->setBl_visivel(false);
            $tramiteEntity->setId_usuario($this->getReference('G2\Entity\Usuario', $this->sessao->id_usuario));

            $cAtencao = new CAtencao();
            $return = $cAtencao->salvaTramite($ocorrenciaEntity, $tramiteEntity);

            $this->commit();

            return $return;

        } catch (\Zend_Exception $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Função responsável por gerar mensagem para salvar no tramite, no menu Central de Atencao > processos > Encaminhar Ocorrencias
     * GII-9400
     *
     * @param $idOcorrencia
     * @param $params
     * @return string
     * @throws \Zend_Date_Exception
     * @throws \Zend_Exception
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     */
    public function gerarMensagemTramiteEncaminharOcorrencia($idOcorrencia, $params)
    {
        $ocorrenciaAtual = $this->em->getRepository($this->ocorrenciaRepository)->retornaDadosOcorrenciaAtual($idOcorrencia);
        $ocorrenciaNova = $this->em->getRepository($this->ocorrenciaRepository)->retornaDadosOcorrenciaNova($params['id_assuntoco']);
        $novoAtendente = $this->findOneBy(Usuario::class, array('id_usuario' => $params['id_usuario']));
        $atendenteAtual = $ocorrenciaAtual["st_nomecompleto"] != NULL ? $ocorrenciaAtual["st_nomecompleto"] : 'Não distribuída';

        $msg = 'Ocorrência N. ' . $idOcorrencia . ' encaminhada do assunto [' . $ocorrenciaAtual["assunto"] . '] '
            . ' e subassunto [' . $ocorrenciaAtual["subassunto"] . '] para o assunto [' . $ocorrenciaNova["assunto"] . ']'
            . ' e subassunto [' . $ocorrenciaNova["subassunto"] . '], do atendente [' . $atendenteAtual . ']'
            . ' para o atendente [' . $novoAtendente->getSt_nomecompleto() . '] em ' . new \Zend_Date();

        return $msg;
    }
}

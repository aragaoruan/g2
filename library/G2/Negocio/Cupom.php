<?php

namespace G2\Negocio;

/**
 * Classe de negocio para Cupom
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-03-14
 */
class Cupom extends Negocio
{

    private $repositoryName = '\G2\Entity\Cupom';
    private $validaDados;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna Cupom por id
     * @param integer $id
     * @return \G2\Entity\Cupom
     */
    public function findCupom($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /**
     * Procedimento para salvar cupons, utilizado para salvar vários cupons de uma só vez.
     * @param array $data
     * @return \G2\Entity\Cupom|null
     * @throws \Zend_Exception
     */
    public function procedimentoSalvarCupom(array $data)
    {
        try {
            $result = NULL;
            $this->beginTransaction(); //abre transação
            //verifica se em $data existe uma chave chamada nu_quantidadecupons
            if (array_key_exists('nu_quantidadecupons', $data) && $data['nu_quantidadecupons']) {
                //atribui a quantidade de cupons na variavel
                $lacosCupons = $data['nu_quantidadecupons'];

                unset($data['nu_quantidadecupons']); //remove a chave nu_quantidadecupons do array
                //faz o for para a quantidade de cupons que devem ser salvas
                for ($i = 0; $i < $lacosCupons; $i++) {
                    $result = $this->salvarCupom($data); //salva o cupom
                    //se não salvou cria uma exception
                    if (!($result instanceof \G2\Entity\Cupom)) {
                        $this->rollback(); //rollback
                        return $result;
                    }
                }
            } else { //senão tiver a chave tenta salvar o cupom
                $result = $this->salvarCupom($data);
            }
            //commit
            $this->commit();
            return $result; //retorna

        } catch (\Zend_Exception $e) {
            $this->rollback(); //rollback
            //cria o exception
            throw new \Zend_Exception("Erro ao tentar executar procedimento para salvar cupons. " . $e->getMessage());
        }
    }

    /**
     * Salva dados de cupom
     * @param array $data
     * @return \G2\Entity\Cupom
     * @throws \Zend_Exception
     */
    public function salvarCupom(array $data)
    {
//        \Zend_Debug::dump($data);exit;
        try {
            $entity = new \G2\Entity\Cupom();
            if (isset($data['id_cupom']) && !empty($data['id_cupom'])) {
                $entity = $this->findCupom($data['id_cupom']);
            }

            if (isset($data['bl_ativo']) && !empty($data['bl_ativo'])) {
                $entity->setBl_ativo($data['bl_ativo']);
            } else {
                $entity->setBl_ativo($entity->getBl_ativo() ? $entity->getBl_ativo() : true);
            }

            if (isset($data['st_prefixo'])) {
                $entity->setSt_prefixo($data['st_prefixo']);
            }

            if (isset($data['id_campanhacomercial'])) {
                $campanha = $this->getReference('\G2\Entity\CampanhaComercial', $data['id_campanhacomercial']);
                $entity->setId_campanhacomercial($campanha);
            }

            if (isset($data['id_tipodesconto'])) {
                $tipoDesconto = $this->getReference('\G2\Entity\TipoDesconto', $data['id_tipodesconto']);
                $entity->setId_tipodesconto($tipoDesconto);
            }

            //Seta o complemento do codigo do cupom
            if (isset($data['st_complemento']) && !empty($data['st_complemento'])) {
                $entity->setSt_complemento($data['st_complemento'])
                    ->setSt_codigocupom($entity->getSt_prefixo() . $entity->getSt_complemento());
            } else {
                if (!$entity->getSt_complemento()) {
                    $codigoAleatorio = $this->gerarCodigoCupomAleatorio($entity->getSt_prefixo());
                    $entity->setSt_complemento($codigoAleatorio)
                        ->setSt_codigocupom($entity->getSt_prefixo() . $codigoAleatorio);
                }
            }

            //seta o usuario de cadastro
            if (isset($data['id_usuariocadastro']) && !empty($data['id_usuariocadastro'])) {
                $usuario = $this->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']);
                $entity->setId_usuariocadastro($usuario);
            } else {
                if (!$entity->getId_usuariocadastro()) {
                    $usuario = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);
                    $entity->setId_usuariocadastro($usuario);
                }
            }
            $entity->setBl_ativacao(isset($data['bl_ativacao']) ? $data['bl_ativacao'] : $entity->getBl_ativacao())
                ->setBl_unico(isset($data['bl_unico']) ? $data['bl_unico'] : $entity->getBl_unico())
                ->setDt_cadastro($entity->getDt_cadastro() ? $entity->getDt_cadastro() : new \DateTime())
                ->setDt_fim(isset($data['dt_fim']) ? $this->converteDataBanco($data['dt_fim']) : $entity->getDt_fim())
                ->setDt_inicio(isset($data['dt_inicio']) ? $this->converteDataBanco($data['dt_inicio']) : $entity->getDt_inicio())
                ->setNu_desconto(isset($data['nu_desconto']) ? $data['nu_desconto'] : $entity->getNu_desconto());

            if (!$this->validaDadosCupom($entity)) {
                return $this->validaDados;
            }

            return $this->save($entity);
        } catch (\Exception $ex) {
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Valida dados como data e valor de desconto quando porcentagem
     * @param \G2\Entity\Cupom $entity
     * @return boolean
     */
    private function validaDadosCupom($entity)
    {
        $valid = true;
        //valida a data
        if (!$this->comparaData($entity->getDt_inicio()->format('Y-m-d H:i:s'), $entity->getDt_fim()->format('Y-m-d H:i:s'))) {
            $valid = false;
            $this->validaDados = array(
                'status' => false,
                'type' => 'warning',
                'title' => 'Atenção!',
                'text' => 'A Data de Término Não Pode ser Menor que a Data de Inicio!'
            );
        }

        //valida porcentagem
        if ($entity->getId_tipodesconto()->getId_tipodesconto() == 2) {
            if ($entity->getNu_desconto() > 100) {
                $valid = false;
                $this->validaDados = array(
                    'status' => false,
                    'type' => 'warning',
                    'title' => 'Atenção!',
                    'text' => 'A porcentagem de desconto não pode exceder a 100%!'
                );
            }
        }

        //valida se cupom já existe
        if (!$entity->getId_cupom() && $this->retornarCupons(array('st_codigocupom' => $entity->getSt_codigocupom()))) {
            $valid = false;
            $this->validaDados = array(
                'status' => false,
                'type' => 'warning',
                'title' => 'Atenção!',
                'text' => 'O código informado para o cupom já existe.'
            );
        }

        return $valid;
    }

    /**
     * Gera string com 5 caracteres para hash do codigo do cupom
     * @param string $prefixo Prefixo do codigo do cupom
     * @return string Hash concatenada para codigo do cupom
     */
    private function gerarCodigoCupomAleatorio($prefixo)
    {
        $bo = new \Ead1_BO();
        $hashCodigoCupom = $bo->geradorHash(5);
//        $hashCodigoCupom = substr(base64_encode(rand(0, time()) . uniqid() . $prefixo), 0, 5);
        return $hashCodigoCupom;
    }

    /**
     * Retorna Cupons
     * @param array $where
     * @param array $order
     * @throws \Zend_Exception
     * @return \G2\Entity\Cupom
     */
    public function retornarCupons(array $where = array(), array $order = array())
    {
        try {
            return $this->findBy($this->repositoryName, $where, $order);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao retornar cupom. " . $e->getMessage());
        }
    }

    /**
     * Retorna Dados de Cupom para Ws
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function findCuponsToWs(array $params = array())
    {
        try {
            $mensageiro = new \Ead1_Mensageiro(); //intancia o mensageiro
            $verificaCupom = $this->verificaCupomUtilizadoByCodigo($params['st_codigocupom']);
            if ($verificaCupom) {
                //chama o metodo do repository que busca os cupons vinculados a um produto
                $result = $this->em->getRepository("G2\Entity\VwCupomCampanhaProduto")
                    ->retornarCupomCampanhaProdutoWs($params);

                if ($result) {
                    return $mensageiro->setMensageiro($result, \Ead1_IMensageiro::SUCESSO);
                } else {
                    return $mensageiro->setMensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
                }
            } else {
                return $mensageiro->setMensageiro("Esse cupom já foi utilizado em outra venda.", \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $ex) {
//            return $mensageiro->setMensageiro($ex->getMessage(), \Ead1_IMensageiro::ERRO);
            throw new \Zend_Exception($ex->getMessage());
        }
    }

    /**
     * Verifica se o Cupom já foi vinculado a uma venda
     * e se tenha sido verifica se ele pode ter mais de uma venda vinculado a ele
     * @param string $stCodigoCupom Código do Cupom
     * @return boolean|\Zend_Exception
     * @throws \Exception
     */
    private function verificaCupomUtilizadoByCodigo($stCodigoCupom)
    {
        try {
            $cupomResult = $this->findOneBy($this->repositoryName, array('st_codigocupom' => $stCodigoCupom)); //pesquisa o cupom
            if ($cupomResult) { //verifica se achou o cupom
                $vendaNegocio = new Venda(); //instancia a negocio de venda
                //pesquisa uma venda com o cupom encontrado
                $vendaResult = $vendaNegocio->findVendas(array('id_cupom' => $cupomResult->getId_cupom()));
                if ($vendaResult) { //achou alguma venda com o cupom
                    if ($cupomResult->getBl_unico()) { //verifica se o cupom pode ter mais de uma venda vinculada
                        return FALSE; //se poder ter mais de uma venda retorna true
                    } else {
                        return TRUE; //se o cupom não pode ter mais de uma venda retorna false
                    }
                } else {
                    return TRUE; //se não encontrou venda vinculada ao cupom retorna true
                }
            } else {
                throw new \Exception('Este Cupom não foi encontrado, talvez ele já tenha sido Utilizado. Favor verificar o mesmo e tentar novamente.'); //não encontrou o cupom
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("CUPOM: " . $e->getMessage());
        }
    }

}

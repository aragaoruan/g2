<?php
/**
 * Created by PhpStorm.
 * User: kayo.silva
 * Date: 17/08/2017
 * Time: 16:32
 */

namespace G2\Negocio;


class TipoSelecao extends Negocio
{
    private $repositoryName = '\G2\Entity\TipoSelecao';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna a pesquisa dos tipos de seleção
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @param bool $toArray
     * @return array|null
     * @throws \Exception
     */
    public function retornarPesquisa(array $params = [], $orderBy = null, $limit = 0, $offset = 0, $toArray = true)
    {
        try {
            $where['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;

            $arrCriterios = $this->defineCriteriosDaBusca($params);

            $result = $this->findCustom($this->repositoryName, $where, $arrCriterios, $orderBy, $limit, $offset);

            if ($result && $toArray) {
                /** @var \G2\Entity\TipoSelecao $item */
                foreach ($result as $key => $item) {
                    $result[$key] = $this->toArrayEntity($item);
                    $result[$key]['id_usuariocadastro'] = null;
                    //verifica se o usuario cadastro foi encontrado
                    if ($item->getid_usuariocadastro())
                        $result[$key]['id_usuariocadastro'] = $item->getid_usuariocadastro()->getId_usuario();
                }
            }

            return $result;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Define o criterio da busca que serão strings
     * @param array $params
     * @return array
     */
    private function defineCriteriosDaBusca(array $params = [])
    {
        $arrCriterios = [];
        //verifica se foi escrito algo no campo de busca generica
        if (!empty($params['txtSearch'])) {
            //verifica qual dos checks estão marcados
            if (!empty($params['st_tiposelecao'])) {
                $arrCriterios[] = "tb.st_tiposelecao LIKE '%" . trim($params['txtSearch']) . "%'";
            }

            if (!empty($params['st_descricaotiposelecao'])) {
                $arrCriterios[] = "tb.st_descricao LIKE '%" . trim($params['txtSearch']) . "%'";
            }
            $arrCriterios = array(
                implode(" OR ", $arrCriterios)
            );
        }
        return $arrCriterios;
    }

    /**
     * Salva os dados do Tipo de Seleção
     * @param array $dados
     * @return mixed
     * @throws \Exception
     */
    public function salvarDados(array $dados)
    {
        try {
            /** @var \G2\Entity\TipoSelecao $tipoSelecaoEntity */
            $tipoSelecaoEntity = new $this->repositoryName;

            if (isset($dados['id_tiposelecao']) && !empty($dados['id_tiposelecao'])) {
                $tipoSelecaoEntity = $this->find($this->repositoryName, $dados['id_tiposelecao']);
                if (!$tipoSelecaoEntity) {
                    throw new \Exception("Forma de ingresso não encontrada.");
                }
            }

            //pega o id do usuario na sessão ou que foi passado via parametro
            $idUsuarioCadastro = $this->defineUsuarioCadastro($dados);

            //verifica se o id da tabela não foi passado ou é vazio, define os valores padrões para o insert
            if (!isset($dados['id_tiposelecao']) || empty($dados['id_tiposelecao'])) {

                //verifica se o id do usuario não foi definido
                if (!$idUsuarioCadastro) {
                    throw new \Exception("Usuário de cadastro não definido, recarregue a página e tente novamente");
                }

                //seta os valores padrões para o insert
                $tipoSelecaoEntity->setdt_cadastro(new \DateTime())
                    ->setid_usuariocadastro($this->getReference("\G2\Entity\Usuario", $idUsuarioCadastro))
                    ->setBl_ativo(true);

            }

            $tipoSelecaoEntity->setst_tiposelecao($dados['st_tiposelecao'])
                ->setst_descricao($dados['st_descricao']);


            if ($this->verificaDadosObrigatorios($tipoSelecaoEntity)) {
                return $this->save($tipoSelecaoEntity);
            }

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Define o id do usuario pegando da sessão ou do que foi passado via parametro
     * @param array $dados
     * @return mixed
     */
    private function defineUsuarioCadastro(array $dados)
    {
        //pega o id do usuario na sessão
        $idUsuarioCadastro = $this->sessao->id_usuario;

        //verifica se o id do usuario foi passado via parametro
        if (isset($dados['id_usuariocadastro'])
            && !empty($dados['id_usuariocadastro'])
        ) {
            //substitui o valor da variavel
            $idUsuarioCadastro = $dados['id_usuariocadastro'];
        }
        return $idUsuarioCadastro;
    }

    /**
     * Verifica se a descrição ou o titulo não foi definido
     * @param $entity
     * @return bool
     * @throws \Exception
     */
    private function verificaDadosObrigatorios($entity)
    {
        if (!trim($entity->getst_tiposelecao())) {
            throw new \Exception("Informe o título do Tipo de Seleção");
        }

        if (!trim($entity->getst_descricao())) {
            throw new \Exception("Informe a descrição do Tipo de Seleção.");
        }
        return true;
    }

    /**
     * Faz a deleção lógica do tipo de seleção
     * @param $idTipoSelecao
     * @return mixed
     * @throws \Exception
     */
    public function apagarTipoSelecao($idTipoSelecao)
    {
        try {
            if (!$idTipoSelecao) {
                throw new \Exception("Informe o ID do registro para deletar.");
            }

            if (!$this->verificarAptoEditar($idTipoSelecao)) {
                throw new \Exception("Esta Forma de Ingresso está vinculada a uma Venda Produto ou Pessoa, impossível deletar.");
            }

            /** @var \G2\Entity\TipoSelecao $tipoSelecao */
            $tipoSelecao = $this->find($this->repositoryName, $idTipoSelecao);
            $tipoSelecao->setBl_ativo(false);
            $result = $this->save($tipoSelecao);

        } catch (\Exception $e) {
            throw $e;
        }
        return $result;
    }

    /**
     * Verifica se o tipo de seleção esta vinculado a algum registro de venda prduto ou dados complementares
     * @param $idTipoSelecao
     * @return bool
     * @throws \Exception
     */
    public function verificarAptoEditar($idTipoSelecao)
    {
        try {
            $vendaProduto = $this->findOneBy('\G2\Entity\VendaProduto', array(
                'id_tiposelecao' => $idTipoSelecao
            ));

            $dadosComplementares = $this->findOneBy('\G2\Entity\PessoaDadosComplementares', array(
                "id_formaingresso" => $idTipoSelecao
            ));

            if (!$vendaProduto && !$dadosComplementares) {
                return true;
            }

        } catch (\Exception $e) {
            throw $e;
        }

        return false;
    }

}

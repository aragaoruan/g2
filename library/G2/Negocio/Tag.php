<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Entidade Tag
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-01-16
 */
class Tag extends Negocio
{

    /**
     * String repository name 
     * @var string 
     */
    private $repositoryName = 'G2\Entity\Tag';
    private $mensageiro;

    public function __construct ()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Find Tag by params call method findBy
     * @param array $params params for find
     * @return G2\Entity\Tag
     */
    public function findTagBy (array $params = array())
    {
        return parent::findBy($this->repositoryName, $params);
    }

    /**
     * Find especific Tag by id
     * @param integer $id
     * @return G2\Entity\Tag
     */
    public function findTag ($id)
    {
        return parent::find($this->repositoryName, $id);
    }

    /**
     * Verifica se tag existe no banco e salva ou atualiza
     * @param array $data
     * @return \G2\Entity\Tag
     * @throws \Zend_Exception
     */
    public function salvarTag (array $data)
    {
        try {
            $entity = $this->verificaTagExiste($data);
            if (!$entity) {
                $entity = new \G2\Entity\Tag();
            }
            //verifica se o st_tag veio preenchido para salvar
            if (!isset($data['st_tag'])) {
                throw new \Zend_Exception("st_tag é de preenchimento obrigatório!");
            }


            if (isset($data['id_usuariocadastro'])) {
                $user = $this->getReference('\G2\Entity\Usuario', $data['id_usuariocadastro']);
                $entity->setId_usuariocadastro($user);
            }
            $entity->setSt_tag($data['st_tag'])
                    ->setDt_cadastro(new \DateTime($entity->getDt_cadastro()));
            $return = $this->save($entity); //salva
            return $this->mensageiro->setMensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Pesquisa na entity e retorna boolean se registro não existir, se existir retorna a entidade
     * @param array $data
     * @return mixed array of G2\Entity\Tag or boolean if false
     */
    private function verificaTagExiste (array $data)
    {
        if (isset($data['id_tag'])) {
            $result = $this->findTag($data['id_tag']);
        } else {
            $result = $this->em->getRepository($this->repositoryName)->findOneBy($data);
        }
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

}

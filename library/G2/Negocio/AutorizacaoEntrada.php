<?php

namespace G2\Negocio;

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;

/**
 * Classe de negocio para Autorizacao
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2015-06-05
 */
class AutorizacaoEntrada extends Negocio
{

    private $repositoryName = '\G2\Entity\AutorizacaoEntrada';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna numero de emissoes da declaração pelo id do usuario
     * @param integer $id_usuario
     * @return type
     * @throws \Zend_Exception
     */
    public function retornaVwAutorizacaoEntradaByUsuario($id_usuario)
    {
        try {
            $where['id_entidade'] = $this->sessao->id_entidade;
            $where['id_usuario'] = $id_usuario;
            return $this->findOneBy('\G2\Entity\VwEmissoesAutorizacaoEntrada', $where);
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao consultar dados. " . $ex->getMessage());
        }
    }

    /**
     * Salva as autorizações de entrada
     * @param array $data
     * @return \G2\Entity\AutorizacaoEntrada
     * @throws \Zend_Exception
     * @throws \Exception
     */
    public function salvarAutorizacaoEntrada(array $data)
    {
        try {
            $this->beginTransaction();
            //pega a instancia da entity
            $entity = new \G2\Entity\AutorizacaoEntrada();
            //verifica se foi passado o id da autorizacao
            if (array_key_exists('id_autorizacaoentrada', $data) && !empty($data['id_autorizacaoentrada'])) {
                //procura a autorizacao
                $result = $this->find($this->repositoryName, $$data['id_autorizacaoentrada']);
                //verifica se encontrou
                if ($result) {
                    $entity = $result; //reescreve o valor da variavel entity
                }
            }

            if (array_key_exists('bl_ativo', $data) && !empty($data['bl_ativo'])) {
                $bl_ativo = $data['bl_ativo'];
            } else {
                $bl_ativo = true;
            }

            //recupera os dados da entidade da sessao
            $entidadeCadastro = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);
            $usuarioCadastro = $this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario);

            if (array_key_exists('id_textosistema', $data) && !empty($data['id_textosistema'])) {
                $textoSistema = $this->getReference('\G2\Entity\TextoSistema', $data['id_textosistema']);
            } else {
                $textoSistema = null;
            }

            if (array_key_exists('id_usuario', $data) && !empty($data['id_usuario'])) {
                $usuario = $this->getReference('\G2\Entity\Usuario', $data['id_usuario']);
            } else {
                throw new \Exception("Não é possível salvar autorização de entrada sem informar o usuário.");
            }

            if (array_key_exists('dt_cadastro', $data) && empty($data['dt_cadastro'])) {
                $entity->setDt_cadastro(new \DateTime());
            }

            $entity->setBl_ativo($bl_ativo)
                ->setId_entidade($entity->getId_entidade() ? $entity->getId_entidade() : $entidadeCadastro)
                ->setId_usuariocadastro($entity->getId_usuariocadastro() ? $entity->getId_usuariocadastro() : $usuarioCadastro)
                ->setId_textosistema($textoSistema)
                ->setId_usuario($usuario);

            $entity = $this->save($entity);
            $this->commit();
        } catch (ORMException $orm) {
            $this->rollback();
            throw new \Zend_Exception('$orm->getMessage()');
        } catch (DBALException $dbal) {
            $this->rollback();
            throw new \Zend_Exception('$dbal->getMessage()');
        }

        return $entity;
    }

}

<?php

namespace G2\Negocio;

use \G2\Constante\Evolucao as Constante_Evolucao;

use G2\Entity\Motivo;
use G2\Utils\Helper;

/**
 * Classe de negócio para Cancelamento de Alunos
 *
 * @author Helder Silva<helder.silva@unyleya.com.br>;
 */
class Cancelamento extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retora nos dados de matricula do usuário
     *
     * @param $params
     * @return array|string
     */
    public function retornaMatriculasUsuario($params)
    {
        try {

            $params['id_usuario'] = $params['id'];
            unset($params['id']);
            $params['id_entidadematricula'] = $this->sessao->id_entidade;
            $params['bl_ativo'] = true;
            $repositoryName = 'G2\Entity\VwMatricula';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Salva o cancelamento inicial dos dados gerais
     * @param $params
     * @return mixed|null
     * @throws \Exception
     */
    public function salvarCancelamento($params)
    {
        $this->beginTransaction();
        try {
            $cancelamento = null;
            $matriculacancelamento = null;
            $motivocancelamento = null;
            $usuarioSessao = $this->getReference(\G2\Entity\Usuario::class, $this->sessao->id_usuario);


            /**
             * Gravando na tabela Cancelamento
             */
            $objetoCancelamento = new \G2\Entity\Cancelamento();

            $objetoMatricula = $this->find(\G2\Entity\Matricula::class, $params['matricula']);

            //procura o cancelamento
            if ($objetoMatricula->getId_cancelamento()) {
                $objetoCancelamento = $this
                    ->find(\G2\Entity\Cancelamento::class, $objetoMatricula->getId_cancelamento());

                if (!$objetoCancelamento) {
                    new \Exception("Cancelamento não encontrado.");
                }
            }

            //verifica se o bl_cancelamentofinalizado não foi setado
            if (is_null($objetoCancelamento->getId_cancelamento())) {
                $objetoCancelamento->setBl_cancelamentofinalizado(false)
                    ->setDt_cadastro(new \DateTime())
                    ->setId_usuariocadastro($usuarioSessao);
            }

            $objetoCancelamento->setDt_solicitacao($this->converteDataBanco($params['dt_solicitacao']))
                ->setId_evolucao($this->getReference(\G2\Entity\Evolucao::class, $params['evolucao']))
                ->setNu_diascancelamento($params['nu_diasAcesso'])
                ->setSt_observacao($params['observacao']);

            //seta o id_ocorrencia no cancelamento
            if (!empty($params["id_ocorrencia"])) {
                $objetoCancelamento
                    ->setId_ocorrencia($this->getReference(\G2\Entity\Ocorrencia::class, $params["id_ocorrencia"]));
            }

            /** @var \G2\Entity\Cancelamento $cancelamento */
            $cancelamento = $this->save($objetoCancelamento);
            /*
             * Gravando na tabela Matricula
             */
            if (!$objetoMatricula->getId_cancelamento()) {
                $objetoMatricula->setId_Cancelamento($cancelamento);
                $this->save($objetoMatricula);
            }


            //altera a evolução do cancelamento e da matricula
            $this->alterarEvolucaoCancelamento(
                $cancelamento->getId_cancelamento(),
                $params["evolucao"],
                $objetoMatricula->getId_matricula()
            );

            /*
             * Gravando tabela matricula motivo
             */
            if (array_key_exists('motivos', $params)) {
                $motivo = $params['motivos'];
                $getMotivos = $this->findBy(\G2\Entity\MotivoCancelamento::class, array(
                    'id_cancelamento' => $cancelamento->getId_cancelamento(),
                    'bl_ativo' => true
                ));

                if ($getMotivos) {
                    foreach ($getMotivos as $gM) {
                        $gM->setBl_ativo(false);
                        $this->save($gM);
                    }
                }

                $motivosConcat = null;
                foreach ($motivo as $array) {
                    $objetoMotivo = $this->findOneBy(\G2\Entity\MotivoCancelamento::class, array(
                        'id_motivo' => $array['id_motivo'],
                        'id_cancelamento' => $cancelamento->getId_cancelamento()
                    ));

                    if (!$objetoMotivo) {
                        $objetoMotivo = new \G2\Entity\MotivoCancelamento();
                    }
                    $objetoMotivo->setId_cancelamento($cancelamento)
                        ->setDt_cadastro(new \DateTime())
                        ->setId_usuariocadastro($usuarioSessao)
                        ->setId_motivo($this->getReference(\G2\Entity\Motivo::class, $array))
                        ->setBl_ativo(true);
                    $motivocancelamento = $this->save($objetoMotivo);

                    $motivosConcat = $motivosConcat . '<p>' . $array['st_motivo'];
                    // Gravando tabela LogCancelamento
                }
                $this->salvarLogcancelamento($objetoMatricula, $cancelamento, $motivosConcat);
            } else {
                $getMotivos = $this->findBy(\G2\Entity\MotivoCancelamento::class, array(
                    'id_cancelamento' => $cancelamento->getId_cancelamento(),
                    'bl_ativo' => true
                ));
                if ($getMotivos) {
                    foreach ($getMotivos as $gM) {
                        $gM->setBl_ativo(false);
                        $this->save($gM);
                    }
                }
            }


            $this->commit();


            return $cancelamento;
        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }


    /**
     * @param \G2\Entity\Matricula $matricula
     * @param \G2\Entity\Cancelamento $cancelamento
     * @param $motivo
     * @return mixed
     * @throws \Exception
     */
    public function salvarLogCancelamento(
        \G2\Entity\Matricula $matricula,
        \G2\Entity\Cancelamento $cancelamento,
        $motivo,
        $resarcimento = false
    )
    {
        try {
            if ($motivo) {
                $objetoLog = new \G2\Entity\LogCancelamento();
                $objetoLog->setId_matricula($matricula)
                    ->setId_cancelamento($cancelamento)
                    ->setId_usuariocadastro($this->getReference(\G2\Entity\Usuario::class, $this->sessao->id_usuario))
                    ->setDt_cadastro(new \DateTime())
                    ->setId_evolucao($matricula->getId_evolucao())
                    ->setId_evolucaocancelamento($cancelamento->getId_evolucao())
                    ->setId_situacao($matricula->getId_situacao())
                    ->setSt_motivo($motivo)
                    ->setNu_valorproporcionalcursado($cancelamento->getNu_totalutilizado())
                    ->setNu_valordevolucao($cancelamento->getNu_valordevolucao())
                    ->setbl_ressarcimento($resarcimento);

                return $this->save($objetoLog);
            }
        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * @param $param
     * @return array|bool
     * @throws \Exception
     */

    public function salvarJustificativaCalculoCancelamentoPos($param)
    {

        try {

            $st_disciplina = $param['st_disciplina'];
            $id_matricula = $param['id_matricula'];
            $id_disciplina = $param['id_disciplina'];
            $motivo = $param['txt-calculo-justificativa'];
            $bl_cancelamento = $param['bl_cancelamento'];

            $msg = "<b>Disciplina: </b>".$st_disciplina." <b> Justificativa: </b>".$motivo;

            $where = array(
                "id_matricula" => $id_matricula,
                "id_disciplina" => $id_disciplina
            );

            $objetoMatricula = $this->find(\G2\Entity\Matricula::class, $id_matricula);

            if (!$objetoMatricula) {
                return false;
            } else {
                if ($objetoMatricula->getId_cancelamento()) {
                    $objetoCancelamento = $this
                        ->find(\G2\Entity\Cancelamento::class, $objetoMatricula->getId_cancelamento());

                    if (!$objetoCancelamento) {
                        return false;
                    }

                } else {
                    return false;
                }
            }
            $justificativaCalculo = $this->toArrayEntity(
                $this->salvarLogcancelamento($objetoMatricula, $objetoCancelamento, $msg)
            );

            $objetoMatriculaDisciplina = $this->findOneBy(\G2\Entity\MatriculaDisciplina::class, $where);
            if (!$objetoMatriculaDisciplina) {
                return false;
            } else {
                $objetoMatriculaDisciplina->setBl_cancelamento($bl_cancelamento);
                $this->save($objetoMatriculaDisciplina);
            }

            return $justificativaCalculo;

        } catch (\Exception $e) {
            throw $e;
        }

    }

    /**
     * retorna os dados ce cancelamento
     *
     * @param $params
     * @return array
     */
    public function retornaDadosCancelamento($params)
    {
        $matricula_cancelamento = $this->find('\G2\Entity\Matricula', $params);
        //se houver uma matricula com cancelamento
        if ($matricula_cancelamento->getId_cancelamento() != NULL) {

            //busca todos os dados necessários no banco
            $cancelamento = $this->find('\G2\Entity\Cancelamento', $matricula_cancelamento->getId_cancelamento());
            $motivos = $this->findBy('\G2\Entity\MotivoCancelamento', array('id_cancelamento' => $cancelamento->getId_cancelamento(), 'bl_ativo' => true));
            if ($matricula_cancelamento->getId_vendaproduto() != null) {
                $dadosFinanceiros = $this->find('\G2\Entity\VendaProduto', $matricula_cancelamento->getId_vendaproduto());

                $dadosLancamentos = $this->findBy('\G2\Entity\VwLancamento', array('id_venda' => $dadosFinanceiros->getId_venda(), 'bl_quitado' => \G2\Constante\Lancamento::PAGAMENTO_QUITADO));
                $produto = $this->findBy('\G2\Entity\ProdutoProjetoPedagogico', array('id_produto' => $dadosFinanceiros->getId_produto()));
            }

            //busca as porcetagens de cobrança das multas.
            $multas = $this->getValoresMultasEntidadeSessao();
//            $multas = $this->findOneBy('\G2\Entity\EntidadeFinanceiro', array('id_entidade' => $this->sessao->id_entidade));

            if (!$cancelamento->getNu_valormaterial()) {
                $materialTotal = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaValorMateriais(array('id_matricula' => $params, 'id_entidadeatendimento' => $this->sessao->id_entidade));
                $materialUtilizado = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaValorMateriais(array('id_matricula' => $params, 'id_entidadeatendimento' => $this->sessao->id_entidade, 'id_situacao' => \G2\Constante\Situacao::TB_ENTREGAMATERIAL_ENTREGUE));
            } else {
                $materialTotal['nu_valortotal'] = $cancelamento->getNu_valormaterial();
                $materialUtilizado['nu_valortotal'] = $cancelamento->getNu_valorutilizadomaterial();
            }

            if (!$cancelamento->getNu_cargahorariacursada()) {
                $cargaUtilizada = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaCargaHorariaOfertada(array('id_matricula' => $params, 'id_entidadeatendimento' => $this->sessao->id_entidade));
            } else {
                $cargaUtilizada['nu_somacargaofertada'] = $cancelamento->getNu_cargahorariacursada();
            }

            if (isset($dadosLancamentos)) {
                $valorPago = 0;
                $valorPagoCarta = 0;
                foreach ($dadosLancamentos as $lancamento) {
                    $valorPago += $lancamento->getNu_valor();
                    if ($lancamento->getId_meiopagamento() == \G2\Constante\MeioPagamento::CARTA_CREDITO) {
                        $valorPagoCarta += $lancamento->getNu_valor();
                    }
                }

                if (isset($dadosFinanceiros) && $dadosFinanceiros->getId_venda()->getNuValorCartaCredito() !== null) {
                    $valorPagoCarta += $dadosFinanceiros->getId_venda()->getNuValorCartaCredito();
                }
            }

            //cria um array com os motivos
            $objetosArray = array();
            $arrayObjetosMotivos = array();
            foreach ($motivos as $array) {
                array_push($objetosArray, $this->toArrayEntity($array));

                //add para retornar o objeto puro
                array_push($arrayObjetosMotivos, $array);

            }

            //Busca a Forma de pagamento
            $paramsFormaPagamento = array(
                'id_entidade' => $matricula_cancelamento->getId_entidadematricula(),
                'id_situacao' => \G2\Constante\Situacao::TB_FORMAPAGAMENTO_ATIVO,
                'id_formapagamentoaplicacao' => \G2\Constante\FormaPagamentoAplicacao::VENDA_INTERNA
            );

            $formaPagamento = $this->findBy('\G2\Entity\VwFormaMeioPagamento', $paramsFormaPagamento, array('st_meiopagamento' => 'ASC'));

            if (isset($dadosLancamentos) && !empty($dadosLancamentos)) {
                $formaPagamentoArray = array();
                foreach ($dadosLancamentos as $dadosLancamento) {
                    foreach ($formaPagamento as $key => $item) {
                        $formaPagamentoArray[$key] = $this->toArrayEntity($item);
                        if ($dadosLancamento->getId_meiopagamento() == $item->getId_meiopagamento()) {
                            $formaPagamentoArray[$key]['valorPago'] = $dadosLancamento->getNu_valor();
                        }
                    }
                }
            }

            //preenche os dados de resultado para retornar
            $resultado = [
                'id' => $matricula_cancelamento->getId_cancelamento()->getId_cancelamento(),
                'id_matricula' => $params,
                'id_evolucao' => $cancelamento->getId_evolucao()->getId_evolucao(),
                'st_evolucao' => $cancelamento->getId_evolucao()->getSt_evolucao(),
                'dt_solicitacao' => ($cancelamento->getDt_solicitacao()) ? $cancelamento->getDt_solicitacao()->format('d/m/Y') : '--',
                'dt_finalizacao' => ($cancelamento->getDt_finalizacao()) ? $cancelamento->getDt_finalizacao() : '--',
                'nu_diasAcesso' => $cancelamento->getNu_diascancelamento(),
                'st_observacao' => $cancelamento->getSt_observacao(),
                'st_observacaocalculo' => $cancelamento->getSt_observacaocalculo(),
                'nu_valorhoraaula' => $cancelamento->getNu_valorhoraaula(),
                'motivos' => $objetosArray,
                'motivosObjeto' => $arrayObjetosMotivos,
                'valorBruto' => (isset($dadosFinanceiros)) ? $dadosFinanceiros->getNu_valorbruto() : null,
                'valorNegociado' => (isset($dadosFinanceiros)) ? $dadosFinanceiros->getNu_valorliquido() : null,
                'valorDesconto' => (isset($dadosFinanceiros)) ? $dadosFinanceiros->getNu_desconto() : null,
                'cargaHoraria' => (isset($produto)) ? $produto[0]->getnu_cargahoraria() : null,
                'valorMaterial' => $materialTotal['nu_valortotal'],
                'valorUtilizadoMaterial' => $materialUtilizado['nu_valortotal'],
                'valorCargaHorariaCursada' => $cargaUtilizada['nu_somacargaofertada'],
                'valorTotal' => $cargaUtilizada['nu_somacargaofertada'] * $cancelamento->getNu_valorhoraaula(),
                'multaPorcentagemDevolucao' => $multas['nu_multasemcarta'],//$multas->getNu_multasemcarta(),
                'multaPorcentagemCarta' => $multas['nu_multacomcarta'],//$multas->getNu_multacomcarta(),
                'multaPorcentagemDevolucaoCancelamento' => $cancelamento->getNu_multaporcentagemdevolucao(),
                'multaPorcentagemCartaCancelamento' => $cancelamento->getNu_multaporcentagemcarta(),
                'valormultaCarta' => $cancelamento->getNu_valormultacarta(),
                'valormultadevolucao' => $cancelamento->getNu_valormultadevolucao(),
                'bl_cartaGerada' => $cancelamento->getBl_cartagerada(),
                'bl_cancelamentofinalizado' => $cancelamento->getBl_cancelamentofinalizado(),
                'valorPago' => (isset($valorPago)) ? $valorPago : null,
                'valorPagoCarta' => (isset($valorPagoCarta)) ? $valorPagoCarta : null,
                'dadosFinanceiros' => (isset($dadosFinanceiros)) ? $dadosFinanceiros : null,
                'dadosMatriculaCancelamento' => (isset($matricula_cancelamento)) ? $matricula_cancelamento : null,
                'dadosLancamento' => (isset($dadosLancamentos)) ? $dadosLancamentos : null,
                'formaPagamentoArray' => (isset($formaPagamentoArray)) ? $formaPagamentoArray : null,
                'dadosCancelamento' => (isset($cancelamento)) ? $cancelamento : null
            ];
            return $resultado;
        } else {
            //caso não haja nenhum cancelamento para tal matricula, retorna um array com os dados vazios.
            return array(
                'id' => '',
                'id_evolucao' => 0,
                'dt_solicitacao' => (new \DateTime())->format('d/m/Y'),
                'nu_diasAcesso' => 0,
                'motivos' => null,
                'st_evolucao' => 'Nenhuma evolução definida.'
            );
        }
    }

    /**
     * Retorna as porcetagens de multa com e sem carta de credito cobradas por cancelado
     *
     * @return array
     * @throws \Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    private function getValoresMultasEntidadeSessao()
    {
        try {

            $esquemaNegocio = new EsquemaConfiguracao();

            //busca os dados da configuração para multa de cancelamento com carta
            $resultComCarta = $esquemaNegocio->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::MULTA_CANCELAMENTO_COM_CARTA_CREDITO
            ));


            //busca os dados da configuração para multa de cancelamento sem carta
            $resultSemCarta = $esquemaNegocio->retornarEsquemaValores(array(
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::MULTA_CANCELAMENTO_SEM_CARTA_CREDITO
            ));

            //monta array com chaves, valores vazios
            $return = array(
                'nu_multacomcarta' => null,
                'nu_multasemcarta' => null,
            );

            //verifica se trouxe dados para cancelamento com carta
            if ($resultComCarta->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $arrResult = $resultComCarta->getFirstMensagem();
                $return['nu_multacomcarta'] = floatval($arrResult['st_valor']);
            }

            //verifica se trouxe dados para cancelamento sem carta
            if ($resultSemCarta->getType() == \Ead1_IMensageiro::TYPE_SUCESSO) {
                $arrResultSemCarta = $resultSemCarta->getFirstMensagem();
                $return['nu_multasemcarta'] = floatval($arrResultSemCarta['st_valor']);
            }

            return $return;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Salva todos os calculos
     * @param array $params
     * @return \G2\Entity\Cancelamento
     * @throws \Exception
     */
    public function salvarCalculos(array $params)
    {
        $this->beginTransaction();
        try {
            /** @var \G2\Entity\Cancelamento $objeto */
            $objeto = $this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']);
//            $objeto->setNu_valorbruto($params['nu_valorBruto']);
            $objeto->setNu_valornegociado(!empty($params['nu_valorNegociado']) ? $params['nu_valorNegociado'] : null)
                ->setNu_valordiferenca(!empty($params['nu_valorDiferenca']) ? $params['nu_valorDiferenca'] : null)
                ->setNu_cargahoraria(!empty($params['cargaHoraria']) ? $params['cargaHoraria'] : null)
                ->setNu_valorhoraaula(!empty($params['valorHoraAula']) ? $params['valorHoraAula'] : null)
                ->setNu_valorutilizadomaterial(!empty($params['valorUtilizadoMaterial'])
                    ? $params['valorUtilizadoMaterial'] : null)
                ->setNu_valormaterial(!empty($params['valorMaterial']) ? $params['valorMaterial'] : null)
                ->setNu_cargahorariacursada(!empty($params['cargaHorariaCursada'])
                    ? $params['cargaHorariaCursada'] : null)
                ->setNu_valortotal(!empty($params['valorTotal']) ? $params['valorTotal'] : null)
                ->setNu_valormultadevolucao(!empty($params['valorMultaDevolucao'])
                    ? $params['valorMultaDevolucao'] : null)
                ->setNu_valormultacarta(!empty($params['valorMultaCarta']) ? $params['valorMultaCarta'] : null)
                ->setNu_multaporcentagemcarta(!empty($params['valorMultaPorcentagemCarta'])
                    ? $params['valorMultaPorcentagemCarta'] : null)
                ->setNu_multaporcentagemdevolucao(!empty($params['valorMultaPorcentagemDevolucao'])
                    ? $params['valorMultaPorcentagemDevolucao'] : null)
                ->setNu_totalutilizado(!empty($params['totalUtilizado']) ? $params['totalUtilizado'] : null)
                ->setNu_valordevolucao(!empty($params['valorDevolucao']) ? $params['valorDevolucao'] : null)
                ->setNu_valorcarta(!empty($params['valorCarta']) ? $params['valorCarta'] : null)
                ->setSt_observacaocalculo(!empty($params['st_observacao']) ? $params['st_observacao'] : null)
                ->setId_evolucao($this->getReference(
                    '\G2\Entity\Evolucao', Constante_Evolucao::TB_MOTIVOCANCELANENTO_PROCESSO_CANCELAMENTO
                ));

            if (array_key_exists('dt_cancelamento', $params)) {
                $objeto->setDt_solicitacao(Helper::converterData($params['dt_cancelamento'], 'Y-m-d'));
            }

            if (!empty($params['selectCarta']) && $params['selectCarta'] == 1) {
                $objeto->setBl_cartagerada(true);
            } else {
                $objeto->setBl_cartagerada(false);
            }

            $objeto = $this->save($objeto);

            $motivosConcat = null;
            if (array_key_exists('motivos', $params)) {
                foreach ($params['motivos'] as $array) {
                    $motivosConcat = $motivosConcat . '<p>' . $array['st_motivo'];
                }
            }

            /** @var \G2\Entity\Matricula $objetoMatricula */
            $objetoMatricula = $this->findOneBy('\G2\Entity\Matricula', array(
                'id_cancelamento' => $params['id_cancelamento']
            ));

            if (!$objetoMatricula) {
                throw new \Exception("Nenhuma matricula vinculada ao processo de cancelamento");
            }

            $objetoLog = new \G2\Entity\LogCancelamento();
            $objetoLog->setDt_cadastro(new \DateTime());
            $objetoLog->setId_matricula($objetoMatricula);
            $objetoLog->setId_cancelamento($objeto);
            $objetoLog->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objetoLog->setId_evolucao($objetoMatricula->getId_evolucao());
            $objetoLog->setId_evolucaocancelamento($objeto->getId_evolucao());
            $objetoLog->setId_situacao($objetoMatricula->getId_situacao());
            $objetoLog->setNu_valorproporcionalcursado($objeto->getNu_totalutilizado());
            $objetoLog->setNu_valordevolucao($objeto->getNu_valordevolucao());
            $objetoLog->setSt_motivo($motivosConcat);
            $objetoLog->setBl_cartacredito($objeto->getBl_cartagerada());

            if ($objeto->getBl_cartagerada()) {
                $objetoLog->setNu_valordevolucao($objeto->getNu_valorcarta());
                $objetoLog->setNu_percentualmulta($objeto->getNu_multaporcentagemcarta());
            } else {
                $objetoLog->setNu_percentualmulta($objeto->getNu_multaporcentagemdevolucao());
                $objetoLog->setNu_valordevolucao($objeto->getNu_valordevolucao());
            }

            $objetoLog->setNu_valormaterialutilizado($objeto->getNu_valorutilizadomaterial());
            $objetoLog->setNu_cargahorariautilizada($objeto->getNu_cargahorariacursada());


            $log = $this->save($objetoLog);


            if (array_key_exists('regiaoRessarcimento', $params)) {

                $motivosConcat = null;
                $regiaoRessarcimento = get_object_vars($params['regiaoRessarcimento']);

                if (array_key_exists('st_tid', $regiaoRessarcimento) &&
                    $regiaoRessarcimento['st_tid'] != null) {
                    $motivosConcat .= '<p> <b>Tid: </b>' . $regiaoRessarcimento['st_tid'] . '<br />';
                    if (array_key_exists('st_observacao', $regiaoRessarcimento)) {
                        $motivosConcat .= '<b>Observação: </b>' . $regiaoRessarcimento['st_observacao'] . '</p>';
                    }
                    $this->salvarLogCancelamento($objetoMatricula, $objeto, $motivosConcat, true);
                } else {
                    $banco = $this->findOneBy('G2\Entity\Banco',
                        array("id_banco" => $regiaoRessarcimento['id_banco']));
                    $motivosConcat .= '<p> <b>Banco: </b>' . $banco->getSt_nomebanco() . '<br />';
                    $motivosConcat .= '<b>Agencia: </b>' . $regiaoRessarcimento['nu_agencia'] . '<br />';
                    $motivosConcat .= '<b>Conta: </b>' . $regiaoRessarcimento['nu_conta'] . '<br />';
                    if (array_key_exists('nu_operacao', $regiaoRessarcimento)) {
                        $motivosConcat .= '<b>Operação: </b>' . $regiaoRessarcimento['nu_operacao'] . '<br />';
                    }
                    if (array_key_exists('st_observacao', $regiaoRessarcimento)) {
                        $motivosConcat .= '<b>Observação: </b>' . $regiaoRessarcimento['st_observacao'] . '</p>';
                    }
                    $this->salvarLogCancelamento($objetoMatricula, $objeto, $motivosConcat, true);
                }

            }


            $this->commit();

            return $objeto;


        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }

    }

    /**
     * Finaliza o cancelamento gerando ou não a carta de crédito
     *
     * @param $params
     * @return type|string
     */
    public function finalizarCarta($params)
    {
        //caso seja para gerar carta de crédito.
        $motivosConcat = "";

        if ($params['gerarCarta']) {

            try {
                $this->beginTransaction();

                $objeto = new \G2\Entity\CartaCredito();
                $objeto->setId_Cancelamento($params['id_cancelamento']);
                $objeto->setNu_valororiginal($params['nu_valorcarta']);
                $objeto->setDt_cadastro(new \DateTime());
                $objeto->setId_usuario($this->find('\G2\Entity\Usuario', $params['id_usuarioCadastro']));
                $objeto->setId_situacao($this->find('\G2\Entity\Situacao', $params['id_situacao']));
                $objeto->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $objeto->setBl_ativo(true);
                $objeto->setId_Entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));
                $objeto->setId_Cancelamento($this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']));
                $entidade = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

                $esquemaConfiguracao = $this->em->getRepository('\G2\Entity\EsquemaConfiguracaoItem')
                    ->retornaEsquemaConfiguracaoEntidade(array('id_entidade' => $this->sessao->id_entidade
                    , 'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::NU_DIAS_CARTA_CREDITO,
                        'id_esquemaconfiguracao' => $entidade->getId_esquemaconfiguracao()->getId_esquemaconfiguracao()));

                $cancelamento = $this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']);
                $dt_solicitacao = $cancelamento->getDt_solicitacao();
                $objeto->setDt_validade($dt_solicitacao->add(new \DateInterval('P' . (int)$esquemaConfiguracao[0]['st_valor'] . 'D')));
                $resultado = $this->save($objeto);

                $objeto2 = $this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']);
                $objeto2->setBl_cartagerada(true);
                $objeto2->setBl_cancelamentofinalizado(true);
                $objeto2->setDt_finalizacao(new \DateTime());
                $objeto2->setId_evolucao($this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_FINALIZADO));
                $cancelamento = $this->save($objeto2);


                if (array_key_exists('motivos', $params)) {
                    $motivosConcat = null;
                    foreach ($params['motivos'] as $array) {
                        $motivosConcat = $motivosConcat . '<p>' . $array['st_motivo'];
                    }
                }

                $objetoMatricula = $this->findOneBy('\G2\Entity\Matricula', array('id_cancelamento' => $params['id_cancelamento']));
                $objetoLog = new \G2\Entity\LogCancelamento();
                $objetoLog->setDt_cadastro(new \DateTime());
                $objetoLog->setId_matricula($objetoMatricula);
                $objetoLog->setId_cancelamento($objeto2);
                $objetoLog->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $objetoLog->setId_evolucao($this->find('\G2\Entity\Evolucao', $objetoMatricula->getId_evolucao()));
                $objetoLog->setId_evolucaocancelamento($this->find('\G2\Entity\Evolucao', $cancelamento->getId_evolucao()));
                $objetoLog->setId_situacao($this->find('\G2\Entity\Situacao', $objetoMatricula->getId_situacao()));
                $objetoLog->setNu_valordevolucao($cancelamento->getNu_valordevolucao());
                $objetoLog->setNu_valorproporcionalcursado($cancelamento->getNu_totalutilizado());
                $objetoLog->setSt_motivo($motivosConcat);
                $objetoLog->setBl_cartacredito($cancelamento->getBl_cartagerada());
                $objetoLog->setNu_valordevolucao($cancelamento->getNu_valorcarta());
                $objetoLog->setNu_percentualmulta($cancelamento->getNu_multaporcentagemcarta());
                $objetoLog->setNu_valormaterialutilizado($cancelamento->getNu_valorutilizadomaterial());
                $objetoLog->setNu_cargahorariautilizada($cancelamento->getNu_cargahorariacursada());
                $objetoLog->setNu_percentualmulta($cancelamento->getNu_multaporcentagemcarta());
                $log = $this->save($objetoLog);

                $this->commit();

                $resultado = $this->toArrayEntity($resultado);
                $resultado['id_matricula'] = $objetoMatricula->getId_matricula();

                return $resultado;

            } catch (\Exception $e) {
                $this->rollback();
                return $e->getMessage();
            }
        } else {
            try {
                //caso não seja para gerar carta de crédito, apenas atualiza o status para finalizado.
                $this->beginTransaction();
                $objeto2 = $this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']);
                $objeto2->setBl_cartagerada(false);
                $objeto2->setBl_cancelamentofinalizado(true);
                $objeto2->setDt_finalizacao(new \DateTime());
                $objeto2->setId_evolucao($this->find('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_FINALIZADO));
                $resultado = $this->save($objeto2);


                if (array_key_exists('motivos', $params)) {
                    $motivosConcat = null;
                    foreach ($params['motivos'] as $array) {
                        $motivosConcat = $motivosConcat . '<p>' . $array['st_motivo'];
                    }
                }

                $objetoMatricula = $this->findOneBy('\G2\Entity\Matricula', array('id_cancelamento' => $params['id_cancelamento']));

                $objetoLog = new \G2\Entity\LogCancelamento();
                $objetoLog->setDt_cadastro(new \DateTime());
                $objetoLog->setId_matricula($objetoMatricula);
                $objetoLog->setId_cancelamento($this->find('\G2\Entity\Cancelamento', $params['id_cancelamento']));
                $objetoLog->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
                $objetoLog->setId_evolucao($this->find('\G2\Entity\Evolucao', $objetoMatricula->getId_evolucao()));
                $objetoLog->setId_evolucaocancelamento($this->find('\G2\Entity\Evolucao', $resultado->getId_evolucao()));
                $objetoLog->setId_situacao($this->find('\G2\Entity\Situacao', $objetoMatricula->getId_situacao()));
                $objetoLog->setNu_valordevolucao($resultado->getNu_valordevolucao());
                $objetoLog->setNu_valorproporcionalcursado($resultado->getNu_totalutilizado());
                $objetoLog->setSt_motivo($motivosConcat);
                $objetoLog->setBl_cartacredito($resultado->getBl_cartagerada());
                $objetoLog->setNu_valormaterialutilizado($resultado->getNu_valorutilizadomaterial());
                $objetoLog->setNu_cargahorariautilizada($resultado->getNu_cargahorariacursada());
                $objetoLog->setNu_percentualmulta($resultado->getNu_multaporcentagemdevolucao());
                $log = $this->save($objetoLog);

                $resultado = $this->toArrayEntity($resultado);
                $resultado['id_matricula'] = $objetoMatricula->getId_matricula();

                $this->commit();
                return $resultado;
            } catch (\Exception $e) {
                $this->rollback();
                return $e->getMessage();
            }
        }
    }

    /**
     * Retorna o log da matricula
     */
    public function retornaLog($params)
    {
        $resultado = $this->findBy('\G2\Entity\LogCancelamento', array('id_matricula' => $params['id_matricula']), array('id_logcancelamento' => 'desc'));
        $array = array();
        foreach ($resultado as $key => $result) {
            $array[$key]['id_matricula'] = $result->getId_matricula()->getId_matricula();
            $array[$key]['id_situacao'] = $result->getId_situacao()->getId_situacao();
            $array[$key]['st_situacao'] = $result->getId_situacao()->getSt_situacao();
            $array[$key]['id_evolucao'] = $result->getId_evolucao()->getId_evolucao();
            $array[$key]['st_evolucao'] = $result->getId_evolucao()->getSt_evolucao();
            $array[$key]['id_evolucaocancelamento'] = $result->getId_evolucaocancelamento()->getId_evolucao();
            $array[$key]['st_evolucaocancelamento'] = $result->getId_evolucaocancelamento()->getSt_evolucao();
            $array[$key]['dt_registro'] = $result->getDt_cadastro()->format('d/m/Y');
            $array[$key]['nu_valorproporcionalcursado'] = $result->getNu_valorproporcionalcursado();
            $array[$key]['nu_valordevolucao'] = number_format($result->getNu_valordevolucao(), 2, ',', '.');
            $array[$key]['st_motivocancelamento'] = $result->getSt_motivo() ? $result->getSt_motivo() : 'Nenhum motivo cadastrado!';
            $array[$key]['id_perfil'] = $result->getId_usuariocadastro()->getId_usuario();
            $array[$key]['st_perfil'] = $result->getId_usuariocadastro()->getSt_nomecompleto();
            if ($result->getNu_cargahorariautilizada()) {
                $array[$key]['nu_cargahorariacursada'] = $result->getNu_cargahorariautilizada() . ' horas';
            } else {
                $array[$key]['nu_cargahorariacursada'] = "Nenhuma hora cursada.";
            }
            $array[$key]['nu_materialutilizado'] = number_format($result->getNu_valormaterialutilizado(), 2, ',', '.');
            $array[$key]['nu_percentualmulta'] = number_format($result->getNu_percentualmulta(), 2, ',', '') . '%';
            if ($result->getBl_cartacredito()) {
                $array[$key]['bl_cartacredito'] = 'Sim';
            } else {
                $array[$key]['bl_cartacredito'] = 'Não';
            }
        }
        return $array;
    }

    /**
     * Retorna o log da matricula pelo filtro
     */
    public function retornaFiltroLog($params)
    {
        $parametros = array();
        if ($params['id_matricula'] != '') {
            $parametros['id_matricula'] = $params['id_matricula'];
        }
        if ($params['dt_inicio'] != '') {
            $parametros['dt_inicio'] = $this->converteDataBanco($params['dt_inicio'] . ' 00:00:00');
        }
        if ($params['dt_fim'] != '') {
            $parametros['dt_fim'] = $this->converteDataBanco($params['dt_fim'] . ' 23:59:59');
        }


        $resultado = $this->em->getRepository('\G2\Entity\LogCancelamento')->getLogCancelamento($parametros);
        $array = array();
        foreach ($resultado as $key => $result) {
            $array[$key]['id_matricula'] = $result->getId_matricula()->getId_matricula();
            $array[$key]['id_situacao'] = $result->getId_situacao()->getId_situacao();
            $array[$key]['st_situacao'] = $result->getId_situacao()->getSt_situacao();
            $array[$key]['id_evolucao'] = $result->getId_evolucao()->getId_evolucao();
            $array[$key]['st_evolucao'] = $result->getId_evolucao()->getSt_evolucao();
            $array[$key]['id_evolucaocancelamento'] = $result->getId_evolucaocancelamento()->getId_evolucao();
            $array[$key]['st_evolucaocancelamento'] = $result->getId_evolucaocancelamento()->getSt_evolucao();
            $array[$key]['dt_registro'] = $result->getDt_cadastro()->format('d/m/Y');
            $array[$key]['nu_valorproporcionalcursado'] = $result->getNu_valorproporcionalcursado();
            $array[$key]['nu_valordevolucao'] = number_format($result->getNu_valordevolucao(), 2, ',', '.');
            $array[$key]['st_motivocancelamento'] = $result->getSt_motivo() ? $result->getSt_motivo() : 'Nenhum motivo cadastrado!';
            $array[$key]['id_perfil'] = $result->getId_usuariocadastro()->getId_usuario();
            $array[$key]['st_perfil'] = $result->getId_usuariocadastro()->getSt_nomecompleto();
            if ($result->getNu_cargahorariautilizada()) {
                $array[$key]['nu_cargahorariacursada'] = $result->getNu_cargahorariautilizada() . ' horas';
            } else {
                $array[$key]['nu_cargahorariacursada'] = "Nenhuma hora cursada.";
            }
            $array[$key]['nu_materialutilizado'] = number_format($result->getNu_valormaterialutilizado(), 2, ',', '.');
            $array[$key]['nu_percentualmulta'] = number_format($result->getNu_percentualmulta(), 2, ',', '') . '%';
            if ($result->getBl_cartacredito()) {
                $array[$key]['bl_cartacredito'] = 'Sim';
            } else {
                $array[$key]['bl_cartacredito'] = 'Não';
            }
        }
        return $array;
    }

    /**
     * Retorna o texto de sistema para geração da carta de crédito
     */
    public function retornaTextoCartaCredito($params)
    {
        return $this->findBy('\G2\Entity\VwSistemaEntidadeMensagem', array('id_mensagempadrao' => $params['id_mensagempadrao'], 'id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * @param $params
     * @return mixed
     */
    public function atualizaMateriais($params)
    {
        $materialTotal = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaValorMateriais(array('id_matricula' => $params['id_matricula'], 'id_entidadeatendimento' => $this->sessao->id_entidade));
        $materialUtilizado = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaValorMateriais(array('id_matricula' => $params['id_matricula'], 'id_entidadeatendimento' => $this->sessao->id_entidade, 'id_situacao' => \G2\Constante\Situacao::TB_ENTREGAMATERIAL_ENTREGUE));
        $retorno['nu_valortotal'] = $materialTotal['nu_valortotal'];
        $retorno['nu_valortotalutilizado'] = $materialUtilizado['nu_valortotal'];
        return $retorno;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function atualizaCargaUtilizada($params)
    {
        $cargaUtilizada = $this->em->getRepository('\G2\Entity\Cancelamento')->retornaCargaHorariaOfertada(array('id_matricula' => $params['id_matricula']));
        $retorno['nu_cargahorariaofertada'] = $cargaUtilizada['nu_somacargaofertada'];
        return $retorno;
    }

    /**
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function retornaMatriculasCanceladasVencidas()
    {
        $retorno = $this->findAll('\G2\Entity\VwMatriculasCanceladasVencidas');
        if ($retorno) {
            echo('------------------- Matriculas canceladas -------------------<br><br>');
            foreach ($retorno as $value) {
                $objetoMatricula = $this->find('\G2\Entity\Matricula', $value->getId_matricula());
                $objetoMatricula->setId_evolucao($this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO));
                $objetoMatricula->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_MATRICULA_INATIVA));
                $resultado = $this->save($objetoMatricula);
                echo('Matricula: ' . $resultado->getId_matricula() . ' Nome: ' . $resultado->getId_usuario()->getSt_nomecompleto() . '<br>');
            }
        } else {
            echo('------------------- Nenhuma matricula para cancelar -------------------');
        }
    }

    /**
     * Método separado para salvar as alterações de evolução do cancelamento e alterar também a matricula
     * caso se faça necessário
     * @param $idCancelamento
     * @param $idEvolucao
     * @param $idMatricula
     * @param null $textoLog
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    public function alterarEvolucaoCancelamento($idCancelamento, $idEvolucao, $idMatricula, $textoLog = null)
    {
        $mensageiro = new \Ead1_Mensageiro();
        $matriculaNegocio = new \G2\Negocio\Matricula();

        try {

            /** @var \G2\Entity\Cancelamento $cancelamento */
            $cancelamento = $this->find(\G2\Entity\Cancelamento::class, $idCancelamento);
            if (!$cancelamento) {
                throw new \Exception("Cancelamento não encontrado.");
            }

            $cancelamento->setId_evolucao($this->getReference(\G2\Entity\Evolucao::class, $idEvolucao));
            if (!$this->save($cancelamento)) {
                throw new \Exception("Erro ao alterar evolução do cancelamento.");
            }

            $mensageiro->setMensageiro("Evolução do cancelamento alterada com sucesso.");
            $matricula = $matriculaNegocio->retornaMatricula(["id_matricula" => $idMatricula]);
            if ($matricula->getType() === \Ead1_IMensageiro::TYPE_SUCESSO) {
                /** @var \G2\Entity\Matricula $matricula */
                $matricula = $matricula->getFirstMensagem();
            }

            //salva o log do cancelamento
            if ($textoLog) {
                $this->salvarLogCancelamento($matricula, $cancelamento, $textoLog);
            }


            /*
               * Altera a evolução da matricula evolução para bloqueado caso a evolução do cancelamento
               * seja 'Processo de Cancelamento'
               */
            if (
                $cancelamento->getId_evolucao()
                && $idEvolucao === \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_PROCESSO_CANCELAMENTO
            ) {

                $evolucaoMatricula = $matriculaNegocio->alterarEvolucao([
                    'id_matricula' => $idMatricula,
                    'st_acao' => 'bloquear',
                    'st_motivo' => 'Processo de cancelamento iniciado',
                    'dt_alteracao' => [
                        'dt_abertura' => '',
                        'dt_encerramento' => ''
                    ]
                ]);

                if ($evolucaoMatricula->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw new \Exception($evolucaoMatricula->getText());
                }
                $mensageiro->addMensagem($evolucaoMatricula->getText());
            }

            if ($cancelamento->getId_evolucao()
                && $idEvolucao == \G2\Constante\Evolucao::TB_MOTIVOCANCELANENTO_DESISTIU
                && $matricula->getId_evolucao()->getId_evolucao() === \G2\Constante\Evolucao::TB_MATRICULA_BLOQUEADO) {
                $evolucaoMatricula = $matriculaNegocio->alterarEvolucao([
                    'id_matricula' => $idMatricula,
                    'st_acao' => 'liberar',
                    'st_motivo' => 'Aluno desistiu do Processo de Cancelamento',
                    'dt_alteracao' => [
                        'dt_abertura' => '',
                        'dt_encerramento' => ''
                    ]
                ]);
                if ($evolucaoMatricula->getType() !== \Ead1_IMensageiro::TYPE_SUCESSO) {
                    throw new \Exception($evolucaoMatricula->getText());
                }
                $mensageiro->addMensagem($evolucaoMatricula->getText());
            }


        } catch (\Exception $e) {
            throw new \Exception("Erro ao tentar alterar evolução do cancelamento. " . $e->getMessage());
        }
        return $mensageiro;
    }

    public function retornarCancelamento($param)
    {
        return $this->findOneBy('\G2\Entity\Cancelamento', $param);
    }

    public function retornarMotivoCancelamento($param)
    {
        $result = array();
        $getMotivos = $this->findBy('\G2\Entity\MotivoCancelamento', $param);
        if ($getMotivos) {
            foreach ($getMotivos as $gM) {
                $motivos = array(
                    'st_motivo' => $gM->getId_motivo()->getSt_motivo()
                );
                array_push($result, $motivos);
            }
        }
        return $result;
    }

    public function retornaResacimentoCancelamento($param)
    {
        $where = array(
            "id_cancelamento" => $param,
            "bl_ressarcimento" => true
        );
        $order = array('id_logcancelamento' => 'DESC');
        return $this->findOneBy('\G2\Entity\LogCancelamento', $where, $order);
    }
}




<?php

namespace G2\Negocio;

/**
 * Classe de Negocio para Prevenda
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-11-06
 */
class PrevendaProduto extends Negocio
{

    public $repositoryName = 'G2\Entity\PrevendaProduto';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Get PrevendaProduto join Prevenda by $params
     * @param array $params Query params
     * @return mixed/array Array of object Doctrine
     */
    public function getPrevendaProdutoJoinPrevenda (array $params = array())
    {
        $repo = $this->em->getRepository($this->repositoryName);
        $result = $repo->getPrevendaProdutoJoinPrevenda($params);
        if ($result) {
            return $result;
        }
    }

    /**
     * Get PrevendaProduto join Prevenda by $params
     * @param array $params Query params
     * @return mixed/array Array of object Doctrine
     */
    public function getPreVendas(array $params = array())
    {
        $repo = $this->em->getRepository($this->repositoryName);
        $result = $repo->getPreVendas($params);
        if ($result) {
            return $result;
        }
    }

    /**
     * Get PrevendaProduto join Prevenda by $params
     * @param array $params Query params
     * @return mixed/array Array of object Doctrine
     */
    public function getInteracoesPreVendas($idPrevenda)
    {
        $repo = $this->em->getRepository($this->repositoryName);
        $result = $repo->retornarInteracoesPrevenda($idPrevenda);
        if ($result) {
            return $result;
        }
    }

}

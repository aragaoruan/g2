<?php

namespace G2\Negocio;

use G2\Utils\Helper;

/**
 * Classe de negócio para Relatorio (tb_relatorio)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Relatorio extends Negocio
{

    private $repositoryName = 'G2\Entity\Relatorio';

    public function delete($entity)
    {
        /**
         * CampoRelatorio
         */
        // Buscar e remover todas as dependencias
        $ItemsEntity = $this->findBy('\G2\Entity\CampoRelatorio', array(
            'id_relatorio' => $entity->getId_relatorio()
        ));
        // Apagar cada registro encontrado
        if ($ItemsEntity) {
            foreach ($ItemsEntity as $ItemEntity) {
                // Remover PropriedadeCampoRelatorio primeiarmente
                $Props = $this->findBy('\G2\Entity\PropriedadeCampoRelatorio', array(
                    'id_camporelatorio' => $ItemEntity->getId_camporelatorio()
                ));
                if ($Props) {
                    foreach ($Props as $prop) {
                        parent::delete($prop);
                    }
                }

                parent::delete($ItemEntity);
            }
        }

        parent::delete($entity);
    }

    public function save($data)
    {
        try {
            if (isset($data['id_relatorio'])) {
                // Se existir o ID, buscar registro
                $entity = $this->find($this->repositoryName, $data['id_relatorio']);
            } else {
                // se não existir o ID, criar novo registro
                $entity = new \G2\Entity\Relatorio();
            }


            /*********************
             * Definindo atributos
             *********************/

            if (isset($data['id_tipoorigemrelatorio']['id_tipoorigemrelatorio']))
                $entity->setId_tipoorigemrelatorio($this->find('\G2\Entity\TipoOrigemRelatorio', $data['id_tipoorigemrelatorio']['id_tipoorigemrelatorio']));

            if (isset($data['id_funcionalidade']['id_funcionalidade']))
                $entity->setId_funcionalidade($this->find('\G2\Entity\Funcionalidade', $data['id_funcionalidade']['id_funcionalidade']));

            if (isset($data['id_usuario']['id_usuario']))
                $entity->setId_usuario($this->find('\G2\Entity\Usuario', $data['id_usuario']['id_usuario']));
            else
                $entity->setId_usuario($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));

            if (isset($data['id_entidade']['id_entidade']))
                $entity->setId_entidade($this->find('\G2\Entity\Entidade', $data['id_entidade']['id_entidade']));
            else
                $entity->setId_entidade($this->find('\G2\Entity\Entidade', $this->sessao->id_entidade));

            if (isset($data['st_relatorio']))
                $entity->setSt_relatorio($data['st_relatorio']);

            if (isset($data['st_origem']))
                $entity->setSt_origem($data['st_origem']);

            if (isset($data['bl_geral']))
                $entity->setBl_geral($data['bl_geral']);

            if (isset($data['dt_cadastro']))
                $entity->setDt_cadastro($data['dt_cadastro']);
            else
                $entity->setDt_cadastro(date('Y-m-d H:i:s'));

            if (isset($data['st_descricao']))
                $entity->setSt_descricao($data['st_descricao']);


            if (isset($data['id_relatorio'])) {
                $retorno = $this->merge($entity);
            } else {
                $retorno = $this->persist($entity);
            }

            $data['id_relatorio'] = $retorno->getId_relatorio();


            /**
             * CampoRelatorio
             */
            // Buscar e remover todos
            // para poder inserir somente os selecionados
            $ItemsEntity = $this->findBy('\G2\Entity\CampoRelatorio', array(
                'id_relatorio' => $data['id_relatorio']
            ));
            // Apagar cada registro encontrado
            foreach ($ItemsEntity as $ItemEntity) {
                // Remover PropriedadeCampoRelatorio primeiarmente
                $Props = $this->findBy('\G2\Entity\PropriedadeCampoRelatorio', array(
                    'id_camporelatorio' => $ItemEntity->getId_camporelatorio()
                ));
                foreach ($Props as $prop) {
                    parent::delete($prop);
                }

                parent::delete($ItemEntity);
            }


            // Adicionar os Campos selecionados
            if (isset($data['campos']) && $data['campos']) {
                foreach ($data['campos'] as $key => $item) {
                    $novo = new \G2\Entity\CampoRelatorio();
                    $novo->setBl_exibido($item['bl_exibido']);
                    $novo->setBl_filtro($item['bl_filtro']);
                    $novo->setBl_obrigatorio($item['bl_obrigatorio']);
                    $novo->setId_relatorio($data['id_relatorio']);
                    $novo->setNu_ordem($item['nu_ordem']);
                    $novo->setSt_camporelatorio($item['st_camporelatorio']);
                    $novo->setSt_titulocampo($item['st_titulocampo']);

                    $campoSalvo = parent::save($novo);
                    $idPropriedade = $campoSalvo->getId_camporelatorio();

                    // Adicionar cada PropriedadeCampoRelatorio do campo
                    if (isset($item['propriedade_campo_relatorio']) && $item['propriedade_campo_relatorio']) {
                        foreach ($item['propriedade_campo_relatorio'] as $id => $value) {
                            // Nao salva o item se tiver vazio
                            if (!$value || trim($value) == '')
                                continue;

                            $propriedade = new \G2\Entity\PropriedadeCampoRelatorio();
                            $propriedade->setId_camporelatorio($idPropriedade);
                            $propriedade->setId_tipopropriedadecamporel($id);
                            $propriedade->setSt_valor($value);

                            parent::save($propriedade);
                        }
                    }


                }
            }


            $mensageiro = new \Ead1_Mensageiro();
            return $mensageiro->setMensageiro('O registro foi salvo', \Ead1_IMensageiro::SUCESSO, $retorno->getId_relatorio());
        } catch (\ORMException $e) {
            \Zend_Debug::dump($e);
            exit;
            return new \Ead1_Mensageiro('Erro ao salvar o registro: ' . $e->getMessage(), \Ead1_IMensageiro::ERRO, $e);
        }

    }

    /**
     * Funcao util para qualquer relatorio
     * @param string $val
     * @param string $mask
     * @return string
     */
    public function mask($str, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($str[$k]))
                    $maskared .= $str[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    /**
     * Exporta diretamente para o nevegador um ARRAY de dados (Exemplo: XLS OU HTML)
     * @param string $type
     * @param array $dados
     * @param array $cabecalho
     * @param string $titulo
     */
    public function export($dados = array(), $cabecalho = array(), $type = 'xls', $titulo = null)
    {
        $type = trim(strtolower($type));
        $titulo = trim($titulo) ? trim($titulo) : 'Relatorio_' . date('Y-m-d');

        /**
         * TYPE XLS
         */
        if ($type == 'xls') {
            // Montar cabecalho do relatorio
            $arXlsHeaderTO = array();
            if ($cabecalho) {
                foreach ($cabecalho as $attr => $label) {
                    $xlsHeaderTO = new \XLSHeaderTO();
                    $xlsHeaderTO->setSt_header(utf8_decode($label));
                    $xlsHeaderTO->setSt_par($attr);
                    $arXlsHeaderTO[] = $xlsHeaderTO;
                }
            }

            $arTO = \Ead1_TO_Dinamico::encapsularTo($dados, new \ParametroRelatorioTO(), false, false, false);

            $x = new \XLSConfigurationTO();
            $x->setFilename($titulo);

            $xls = new \Ead1_GeradorXLS();
            $xls->geraXLS($arXlsHeaderTO, $arTO, $x, false);
        }

        /**
         * TYPE HTML
         */
        if ($type == 'html') {
            $entidade = $this->find('\G2\Entity\Entidade', $this->sessao->id_entidade);

            foreach ($cabecalho as $name => &$label) {
                $label = array(
                    'name' => $name,
                    'label' => $label
                );
            }

            $view = new \Zend_View();
            $view->addScriptPath(APPLICATION_PATH . "/apps/default/views/scripts/");

            $view->nomeEntidade = $entidade->getSt_nomeentidade();
            $view->imgEntidade = $entidade->getSt_urlimglogo();
            $view->nomeRelatorio = $titulo;
            $view->cabecalhoDadosRelatorio = $cabecalho;
            $view->nomeUsuario = $this->sessao->nomeUsuario;
            $view->qtdRegistros = count($dados);
            $view->dataGeracao = date("d/m/Y à\s H:i:s");
            $view->dadosRelatorio = $dados;

            echo $view->render('relatorio/impressao.phtml');
        }

        exit;
    }

    /**
     * @see \application\apps\default\controllers\RelatorioAlunosAgendadosController.php::pesquisarAction
     * @param array $params
     * @param int $id_linhadenegocio
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function alunosAgendados($params, $id_linhadenegocio = 0)
    {
        if ($id_linhadenegocio === \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
            $repository = '\G2\Entity\VwAvaliacaoAgendamentoPos';
        } else {
            $repository = '\G2\Entity\VwAvaliacaoAgendamento';
        }

        $id_entidade = !empty($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;

        $query = $this->em->getRepository($repository)
            ->createQueryBuilder('vw')
            // Filtrar entidade
            ->where('vw.id_entidade = :id_entidade')
            ->setParameter('id_entidade', $id_entidade)
            // Somente situacoes AGENDADO e REAGENDADO
            ->andWhere('vw.id_situacao IN (:id_situacao)')
            ->setParameter('id_situacao', array(
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_AGENDADO,
                \G2\Constante\Situacao::TB_AVALIACAOAGENDAMENTO_REAGENDADO
            ));

        // Na Pós Graduação, mostra o último agendamento INATIVO - depois da presença lançada
        if ($id_linhadenegocio === \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
            $query->andWhere('vw.bl_ativo = :bl_ativo OR vw.nu_presenca IS NOT NULL')
                ->setParameter('bl_ativo', true);
        } else {
            // Somente agendamentos ativos
            $query->andWhere('vw.bl_ativo = :bl_ativo')
                ->setParameter('bl_ativo', true);
        }

        /**
         * Configurando parametros PERSONALIZADOS
         */

        if (isset($params['dt_agendamento_min']) && $params['dt_agendamento_min']) {
            $query
                ->andWhere("vw.dt_agendamento >= :dt_agendamento_min")
                ->setParameter('dt_agendamento_min', $this->converterData($params['dt_agendamento_min'], 'Y-m-d'));
        }

        if (isset($params['dt_agendamento_max']) && $params['dt_agendamento_max']) {
            $query
                ->andWhere("vw.dt_agendamento <= :dt_agendamento_max")
                ->setParameter('dt_agendamento_max', $this->converterData($params['dt_agendamento_max'], 'Y-m-d'));
        }

        if (isset($params['st_nomecompleto']) && trim($params['st_nomecompleto'])) {
            $query
                ->andWhere("vw.st_nomecompleto LIKE :st_nomecompleto")
                ->setParameter('st_nomecompleto', '%' . trim($params['st_nomecompleto']) . '%');
        }

        if (isset($params['st_cpf']) && trim($params['st_cpf'])) {
            // Remover caracteres nao numericos
            $st_cpf = preg_replace("/\D/", '', $params['st_cpf']);

            $query
                ->andWhere("vw.st_cpf = :st_cpf")
                ->setParameter('st_cpf', $st_cpf);
        }

        if (isset($params['id_aplicadorprova']) && $params['id_aplicadorprova']) {
            $query
                ->andWhere("vw.id_aplicadorprova = :id_aplicadorprova")
                ->setParameter('id_aplicadorprova', $params['id_aplicadorprova']);
        }

        if (!empty($params['nu_anoaplicacao']) && !empty($params['nu_mesaplicacao'])) {
            $params['nu_mesaplicacao'] = str_pad($params['nu_mesaplicacao'], 2, '0', STR_PAD_LEFT);

            $dt_min = "{$params['nu_anoaplicacao']}-{$params['nu_mesaplicacao']}-01";
            $dt_max = date("Y-m-t", strtotime($dt_min));

            $query->andWhere("vw.dt_aplicacao >= :dt_min")->setParameter('dt_min', $dt_min);
            $query->andWhere("vw.dt_aplicacao <= :dt_max")->setParameter('dt_max', $dt_max);
        }

        if (!empty($params['id_avaliacaoaplicacao'])) {
            $query
                ->andWhere("vw.id_avaliacaoaplicacao = :id_avaliacaoaplicacao")
                ->setParameter('id_avaliacaoaplicacao', $params['id_avaliacaoaplicacao']);
        }

        if (!empty($params['id_projetopedagogico'])) {
            $query
                ->andWhere("vw.id_projetopedagogico = :id_projetopedagogico")
                ->setParameter('id_projetopedagogico', $params['id_projetopedagogico']);
        }

        if (!empty($params['bl_possuiprova'])) {
            $bl_possuiprova = $params['bl_possuiprova'] == 's' ? 1 : 0;
            $query
                ->andWhere("vw.bl_possuiprova = :bl_possuiprova")
                ->setParameter('bl_possuiprova', $bl_possuiprova);
        }

        if (isset($params['bl_temprovaintegrada']) && $params['bl_temprovaintegrada'] !== '') {
            $query
                ->andWhere("vw.bl_temprovaintegrada = :bl_temprovaintegrada")
                ->setParameter('bl_temprovaintegrada', $params['bl_temprovaintegrada']);
        }

        if (isset($params['st_responsavel']) && $params['st_responsavel']) {
            switch ($params['st_responsavel']) {
                case 'sistema':
                    $query
                        ->andWhere("vw.bl_automatico = :bl_automatico")
                        ->setParameter('bl_automatico', true);
                    break;
                case 'aluno':
                    $query
                        ->andWhere("vw.id_usuario = vw.id_usuariocadastro")
                        ->andWhere("vw.bl_automatico = :bl_automatico")
                        ->setParameter('bl_automatico', false);
                    break;
                case 'atendente':
                    $query
                        ->andWhere("vw.id_usuario != vw.id_usuariocadastro")
                        ->andWhere("vw.bl_automatico = :bl_automatico")
                        ->setParameter('bl_automatico', false);
                    break;
                default:
                    break;

            }
        }

        $query->orderBy('vw.st_nomecompleto', 'ASC');

        $ar_return = array();
        $results = $query->getQuery()->getResult();

        if (is_array($results) && $results) {

            foreach ($results as $key => &$result) {
                $result = $this->toArrayEntity($result);

                if ($result['bl_provaglobal']) {
                    $result['st_disciplinas'] = 'Prova Global';
                } else {
                    // Buscar disciplinas
                    if ($this->sessao->bl_provapordisciplina) {
                        $ng_avaliacao = new \G2\Negocio\Avaliacao();
                        $disciplinas = $ng_avaliacao->findVwAvaliacaoAluno(array(
                            'id_matricula' => $result['id_matricula'],
                            'id_avaliacao' => $result['id_avaliacao'],
                            'id_disciplina' => $result['id_disciplina']
                        ), array(
                            // ORDER BY
                            'st_tituloexibicaodisciplina' => 'ASC'
                        ));
                        $method = 'getSt_tituloexibicaodisciplina';
                    } else {
                        $ng_gerenciaprova = new \G2\Negocio\GerenciaProva();
                        $disciplinas = $ng_gerenciaprova->findByVwDisciplinasAgendamento(array(
                            'id_matricula' => $result['id_matricula'],
                            'id_avaliacao' => $result['id_avaliacao'],
                            'id_avaliacaoagendamento' => $result['id_avaliacaoagendamento'],
                            'bl_agendado' => true
                        ), array(
                            // ORDER BY
                            'st_disciplina' => 'ASC'
                        ));
                        $method = 'getSt_disciplina';
                    }

                    $result['st_disciplinas'] = array();
                    if (is_array($disciplinas) && $disciplinas) {
                        foreach ($disciplinas as $disciplina) {
                            $result['st_disciplinas'][$disciplina->getId_disciplina()] = $disciplina->$method();
                        }
                        $result['st_disciplinas'] = implode('<br/>', $result['st_disciplinas']);
                    }
                }

                // Ajustar valores para retornar EXATAMENTE como aparecerá no relatório

                // Verificar o responsavel pelo agendamento
                if ($result['bl_automatico']) {
                    // Sistema G2 (Agendamento Automático)
                    $result['st_responsavel'] = 'Sistema G2';
                } else if ($result['id_usuario'] == $result['id_usuariocadastro']) {
                    // Aluno
                    $result['st_responsavel'] = $result['st_nomecompleto'];
                } else {
                    // Atendente
                    $user = $this->find('\G2\Entity\Usuario', $result['id_usuariocadastro']);
                    $result['st_responsavel'] = $user->getSt_nomecompleto();
                }

                $result['bl_temprovaintegrada'] == 1
                    ? $result['bl_temprovaintegrada'] = 'Sim'
                    : $result['bl_temprovaintegrada'] = 'Não';

                // Verificar se a mascara do telefone vai ser de 9 digitos ou 8 para OS DOIS TELEFONES

                // Telefone principal
                if ($result['nu_telefone']) {
                    if (strlen($result['nu_telefone']) == 8) {
                        $result['nu_telefone'] = $this->mask($result['nu_ddd'] . $result['nu_telefone'], '(##) ####-####');
                    } else {
                        $result['nu_telefone'] = $this->mask($result['nu_ddd'] . $result['nu_telefone'], '(##) #####-####');
                    }
                }

                // Telefone alternativo
                if ($result['nu_telefonealternativo'] && $result['nu_telefonealternativo'] != $result['nu_telefone']) {
                    if (strlen($result['nu_telefonealternativo']) == 8) {
                        $result['nu_telefonealternativo'] = $this->mask($result['nu_dddalternativo'] . $result['nu_telefonealternativo'], '(##) ####-####');
                    } else {
                        $result['nu_telefonealternativo'] = $this->mask($result['nu_dddalternativo'] . $result['nu_telefonealternativo'], '(##) #####-####');
                    }
                }

                if ($id_linhadenegocio !== \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
                    // Verificar presença do aluno no agendamento
                    if ($result['nu_presenca'] === 0) {
                        $result['st_presenca'] = 'Ausente';
                    } else if ($result['nu_presenca'] === 1) {
                        $result['st_presenca'] = 'Presente';
                    } else {
                        $result['st_presenca'] = 'Não lançada';
                    }
                }

                // Retornar somente os campos necessários para deixar o retorno MAIS LEVE
                $ar_return[$key] = array(
                    'id_avaliacaoagendamento' => $result['id_avaliacaoagendamento'],
                    'st_nomecompleto' => $result['st_nomecompleto'],
                    'st_cpf' => \G2\Utils\Helper::mask($result['st_cpf'], '###.###.###-##'),
                    'st_email' => $result['st_email'],
                    'st_telefone' => $result['nu_telefone'] ? $result['nu_telefone'] : '-',
                    'st_telefonealternativo' => $result['nu_telefonealternativo'] ? $result['nu_telefonealternativo'] : '-',
                    'st_projetopedagogico' => $result['st_projetopedagogico'],
                    'st_possuiprova' => $result['st_possuiprova'],
                    'st_aplicadorprova' => $result['st_aplicadorprova'],
                    'st_horarioaula' => $result['st_horarioaula'],
                    'dt_aplicacao' => \G2\Utils\Helper::converterData($result['dt_aplicacao']['date'], 'd/m/Y'),
                    'dt_agendamento' => \G2\Utils\Helper::converterData($result['dt_agendamento']['date'], 'd/m/Y'),
                    'st_responsavel' => $result['st_responsavel'],
                    'st_avaliacao' => $result['st_avaliacao'],
                    'st_disciplinas' => $result['st_disciplinas'],
                    'st_presenca' => $result['st_presenca'],
                    'bl_temprovaintegrada' => $result['bl_temprovaintegrada']
                );

                if ($id_linhadenegocio === \G2\Constante\LinhaDeNegocio::POS_GRADUACAO) {
                    $ar_return[$key]['st_documentacao'] = $result['st_documentacao'];
                }
            }
        }

        return $ar_return;
    }

    public function relatorioSinteticoCoordenadorProjetosPedagogicos($params){
        if(empty($params)){
            throw new \Exception('Informe os parametros para a busca');
        }

        $id_usuario = array_key_exists('id_usuario', $params) ? $params['id_usuario'] : false;
        $id_projetopedagogico = array_key_exists('id_projetopedagogico', $params) && !empty($params['id_projetopedagogico']) ? 'and id_projetopedagogico = '.$params['id_projetopedagogico'] : false;
        $data_inicio = array_key_exists('data_inicio', $params) ? date_create(str_replace('/', '-', $params['data_inicio'])) : false;
        $data_termino = array_key_exists('data_termino', $params) ? date_create(str_replace('/', '-', $params['data_termino'])) : false;

        if(empty($id_usuario)){
            throw new Exception('Informe o id do usuario');
        }else if(is_numeric($id_usuario)){
            $id_usuario = "id_usuario = $id_usuario";
        }else{
            $id_usuario = "id_usuario IS NOT NULL";
        }

        if(!empty($data_inicio)){
            $data_inicio = date_format($data_inicio,"Y-m-d");
            if(!empty($data_termino)){
                $data_termino = date_format($data_termino,"Y-m-d");
                $data_inicio = "and dt_inicio BETWEEN CONVERT(datetime, '$data_inicio') and CONVERT(datetime, '$data_termino')";
            }else{
                $data_inicio = "and dt_inicio >= CONVERT(datetime, '$data_inicio')";
            }
        }

        $id_entidade = "AND (id_entidade in (select id_entidadepai from tb_entidaderelacao where id_entidade = {$this->sessao->id_entidade}) or id_entidade = {$this->sessao->id_entidade})";

        // o distinct foi adicionado, porque estava recuperando projetos pedagógicos da entidade filha e pai quando a relação na tb_entidaderelacao estava errada
        $sql = "SELECT distinct
                    id_entidade,
                    st_nomecompleto,
                    id_projetopedagogico,
                    st_projetopedagogico,
                    SUM(case when id_evolucao = 40 then 1 else 0 end) as bloqueado,
                    SUM(case when id_evolucao = 27 then 1 else 0 end) as cancelado,
                    SUM(case when id_evolucao = 16 then 1 else 0 end) as certificado,
                    SUM(case when id_evolucao = 15 then 1 else 0 end) as concluinte,
                    SUM(case when id_evolucao = 6 then 1 else 0 end) as cursando,
                    SUM(case when id_evolucao = 69 then 1 else 0 end) as decurso_de_prazo,
                    SUM(case when id_evolucao = 21 then 1 else 0 end) as trancado,
                    SUM(case when id_evolucao = 20 then 1 else 0 end) as transferido from rel.vw_relatoriosinteticocoordenador
                    WHERE $id_usuario $data_inicio $id_projetopedagogico $id_entidade
                    group by id_projetopedagogico, id_entidade, st_nomecompleto, id_projetopedagogico, st_nomeentidade, st_projetopedagogico";

        try{
            $stmt = $this->getem()->getConnection()->prepare($sql);
            $stmt->execute();
            $resultado = $stmt->fetchAll();
        }catch (\Exception $e){
            throw new \Exception($e->getMessage());
        }
        return $resultado;
    }

    public function relatorioLogAluno($params = array())
    {


        $query = $this->em->createQueryBuilder('')
            ->select('a.id_matricula,j.st_nomeentidade,f.st_cpf,f.st_nomecompleto,i.st_email,a.id_funcionalidade,
                     g.id_projetopedagogico,g.st_projetopedagogico,d.id_saladeaula,d.st_saladeaula,c.st_funcionalidade,
                            e.st_nomeperfil,a.dt_cadastro')
            ->from('\G2\Entity\LogAcesso', 'a')
            ->join('\G2\Entity\Matricula', 'b', \Doctrine\ORM\Query\Expr\Join::WITH,
                'a.id_matricula = b.id_matricula')
            ->join('\G2\Entity\ProjetoPedagogico', 'g', \Doctrine\ORM\Query\Expr\Join::WITH,
                'b.id_projetopedagogico = g.id_projetopedagogico')
            ->join('\G2\Entity\Funcionalidade', 'c', \Doctrine\ORM\Query\Expr\Join::WITH,
                'c.id_funcionalidade = a.id_funcionalidade')
            ->join('\G2\Entity\Entidade', 'j', \Doctrine\ORM\Query\Expr\Join::WITH,
                'j.id_entidade = a.id_entidade')
            ->leftJoin('\G2\Entity\SalaDeAula', 'd', \Doctrine\ORM\Query\Expr\Join::WITH,
                'd.id_saladeaula = a.id_saladeaula')
            ->leftJoin('\G2\Entity\Perfil', 'e', \Doctrine\ORM\Query\Expr\Join::WITH,
                'e.id_perfil = a.id_perfil')
            ->join('\G2\Entity\Usuario', 'f', \Doctrine\ORM\Query\Expr\Join::WITH,
                'f.id_usuario = a.id_usuario')
            ->leftJoin('\G2\Entity\ContatosEmailPessoaPerfil', 'h', \Doctrine\ORM\Query\Expr\Join::WITH,
                'h.id_usuario = a.id_usuario AND h.bl_ativo=1 AND h.bl_padrao = 1 AND h.id_entidade = a.id_entidade')
            ->leftJoin('\G2\Entity\ContatosEmail', 'i', \Doctrine\ORM\Query\Expr\Join::WITH,
                'i.id_email = h.id_email')
            ->where('a.id_usuario = :id_usuario')
            ->andWhere('a.id_matricula = :id_matricula')
            ->andWhere('a.id_entidade ='.$this->sessao->id_entidade);

        if ($params['id_saladeaula']) {
            $query->andWhere('a.id_saladeaula = :id_saladeaula');
            $query->setParameter('id_saladeaula', $params['id_saladeaula']);
        }
        if ($params['dt_inicial'] && $params['dt_final']) {
            $query->andWhere("a.dt_cadastro >= :dt_inicial AND a.dt_cadastro <= :dt_final");
            $query->setParameter('dt_inicial',
                Helper::converterData($params['dt_inicial'] . ' 00:00:00', "Y-m-d H:i:s"));
            $query->setParameter('dt_final',
                Helper::converterData($params['dt_final'] . ' 23:59:59', "Y-m-d H:i:s"));

        }
        if ($params['dt_inicial'] && !$params['dt_final']) {
            $query->andWhere('a.dt_cadastro >= :dt_inicial');
            $query->setParameter('dt_inicial',
                Helper::converterData($params['dt_inicial'] . ' 00:00:00', "Y-m-d H:i:s"));
        }
        if ($params['dt_final'] && !$params['dt_inicial']) {
            $query->andWhere('a.dt_cadastro <= :dt_final');
            $query->setParameter('dt_final',
                Helper::converterData($params['dt_final'] . ' 23:59:59', "Y-m-d H:i:s"));
        }
        $query->orderBy('a.dt_cadastro', 'DESC');

        $query->setParameter('id_usuario', $params['id_usuario']);
        $query->setParameter('id_matricula', $params['id_matricula']);

        $result = $query->getQuery()->getResult();

        return $result;


    }


}

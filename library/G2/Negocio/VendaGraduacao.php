<?php

namespace G2\Negocio;

use G2\Utils\Helper;

/**
 * Classe negocio Venda
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @update Denise Xavier <denise.xavier@unyleya.com.br>
 */
class VendaGraduacao extends Negocio
{

    private $mensageiro;
    public $idEntidade;

    /**
     * @var \G2\Negocio\Matricula
     */
    public $negocioMatricula;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
        $this->idEntidade = null;
        $this->negocioMatricula = new \G2\Negocio\Matricula();
    }

    /**
     * @return array
     * @throws \Zend_Exception
     * Retorna os creditos default da entidade
     * todo: futuramente nas proximas implementações, deve-se modificar o método para buscar a quantidade de disciplinas que o aluno vai fazer.
     */
    public function retornaCreditosDefault($params = array())
    {
        try {
            $id_entidade = $this->idEntidade ? (int)$this->idEntidade : $this->sessao->id_entidade;

            $params = array_merge(array(
                'id_entidade' => $id_entidade,
                'id_itemconfiguracao' => \G2\Constante\ItemConfiguracao::CREDITO_PADRAO_ENTIDADE
            ), $params);

            $response = $this->findOneBy('\G2\Entity\VwEntidadeEsquemaConfiguracao', $params);

            if (!($response instanceof \G2\Entity\VwEntidadeEsquemaConfiguracao)) {
                throw new \Zend_Exception('A quantidade padrão de Créditos não está configurada para a entidade ' . $id_entidade);
            }

            return $this->toArrayEntity($response);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar creditos. ' . $e->getMessage());
        }
    }

    /**
     * @param int $id_produto
     * @param string $dt_vigencia
     * @return array
     * @throws \Zend_Exception
     * Retornar as informações da tb_produtovalor.
     */
    public function retornaInformacoesCredito($id_produto, $dt_vigencia = null)
    {
        try {
            $ng_produto = new \G2\Negocio\Produto();
            $produtoValor = $ng_produto->retornarValorPorData(array(
                'id_produto' => $id_produto,
                'id_situacao' => \G2\Constante\Situacao::TB_PRODUTO_ATIVO
            ), $dt_vigencia);

            // Verificar se encontrou os dados com o valor do produto
            if ($produtoValor instanceof \G2\Entity\VwProdutoValorTransferencia) {
                return $this->toArrayEntity($produtoValor);
            }

            return null;
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar informações de crédito. ' . $e->getMessage());
        }
    }

    /**
     * @param $parametros
     * @return mixed
     * Método para formatar os parâmetros de modo que o método procedimentoSalvarVenda() possa ser executado
     */
    public function trataParametros($parametros)
    {
        $objeto = array();

        if (array_key_exists('id_vendaproduto', $parametros['venda'])) {
            $objeto['produtos'][0]['id_vendaproduto'] = $parametros['venda']['id_vendaproduto'];
        }

        if (array_key_exists('id_matricula', $parametros['venda'])) {
            $objeto['produtos'][0]['id_matricula'] = $parametros['venda']['id_matricula'];
        }

        $objeto['produtos'][0]['id_produto'] = $parametros['venda']['id_produto'];
        $objeto['produtos'][0]['dt_ingresso'] = !empty($parametros['venda']['dt_ingresso']) ? $parametros['venda']['dt_ingresso'] : null;
        $objeto['produtos'][0]['nu_valorbruto'] = $parametros['venda']['nu_valorbruto'];
        $objeto['produtos'][0]['nu_valorliquido'] = $parametros['venda']['nu_valorliquido'];
        $objeto['produtos'][0]['nu_descontovalor'] = $parametros['venda']['nu_descontovalor'];
        $objeto['produtos'][0]['nu_desconto'] = !empty($parametros['venda']['nu_desconto']) ? $parametros['venda']['nu_desconto'] : null;
        $objeto['produtos'][0]['id_campanhacomercial'] = $parametros['venda']['id_campanhacomercial'];
        $objeto['produtos'][0]['id_turma'] = $parametros['venda']['id_turma'];
        $objeto['produtos'][0]['id_tiposelecao'] = $parametros['venda']['id_tiposelecao'];
        $objeto['forma_pagamento']['id_formapagamento'] = 265;
        $objeto['forma_pagamento']['id_atendente'] = $parametros['venda']['id_atendente'];

        $objeto['forma_pagamento']['id_campanhapontualidade'] = '';

        if (!empty($parametros['venda']['id_campanhapontualidade'])) {
            $objeto['forma_pagamento']['id_campanhapontualidade'] = $parametros['venda']['id_campanhapontualidade'];
        }

        $objeto['forma_pagamento']['id_evolucao'] = $parametros['venda']['id_evolucao'];
        $objeto['forma_pagamento']['id_situacao'] = $parametros['venda']['id_situacao'];
        $objeto['forma_pagamento']['id_contratoregra'] = $parametros['venda']['id_contratoregra'];

        $objeto['venda'] = $parametros['venda'];

        $objeto['venda']['nu_valorliquido'] = str_replace('.', ',', $objeto['venda']['nu_valorliquido']);
        $objeto['venda']['nu_valorbruto'] = str_replace('.', ',', $objeto['venda']['nu_valorbruto']);

        //retirando a campanha comercial porque não é necessário armazenar nada na venda
        unset($objeto['venda']['id_campanhacomercial']);

        $objeto['pessoa'] = $parametros['pessoa'];

        if (array_key_exists('lancamentos', $parametros)) {
            $objeto['lancamentos'] = $parametros['lancamentos'];
//            $bo = new \VendaBO();
//            foreach ($objeto['lancamentos'] as $key => $lancamento) {
//                $objeto['lancamentos'][$key]['nu_valor'] = str_replace(',', '.', $bo->converteMoeda($lancamento['nu_valor']));
//            }
        }

        if (array_key_exists('disciplinas', $parametros)) {
            $objeto['disciplinas'] = $parametros['disciplinas'];
        }

        return $objeto;
    }

    /**
     * @param $params
     * @return array
     * @throws \Zend_Exception
     * Retorna as informações de valores do produto formatado no padrão para mostrar na tela.
     */
    public function retornaInformacoesValores($params)
    {
        try {
            $retorno = array();

            $id_produto = !empty($params['id_produto']) ? $params['id_produto'] : null;
            $dt_vigencia = null;

            if (!empty($params['id_matricula'])) {
                $en_matricula = $this->find('\G2\Entity\Matricula', $params['id_matricula']);
                $dt_vigencia = ($en_matricula instanceof \G2\Entity\Matricula) ? $en_matricula->getDt_cadastro() : null;
            }

            // Buscar informações de valor do produto
            $informacoesValor = $this->retornaInformacoesCredito($id_produto, $dt_vigencia);

            // Se não tiver informações do valor do produto, então o processo é interrompido
            if (!$informacoesValor) {
                return array(
                    'error' => $dt_vigencia
                        ? "Definir valor do crédito de acordo com a data da matrícula do aluno ("
                        . Helper::converterData($dt_vigencia, 'd/m/Y') . ")"
                        : "É necessário cadastrar o valor do produto para concluir a venda"
                );
            }

            $valorCredito = floatval(number_format($informacoesValor['nu_valormensal'], 2, '.', ''));
            $nuCreditos = (int)$this->retornaCreditosDefault()['st_valor'];

            $retorno['disciplinas'] = array();
            $retorno['nu_totalofertaspadrao'] = null;

            // Descobrir as disciplinas PADRAO e/ou SELECIONADAS
            // Se possuir o id_venda, entao buscar na tb_prematriculadisciplina as disciplinas adicionadas
            if (!empty($params['id_venda'])) {
                /** @var \G2\Entity\Venda $venda */
                $venda = $this->find('\G2\Entity\Venda', $params['id_venda']);
                $retorno['disciplinas'] = $this->findSalasVenda($params['id_venda']);
                $retorno['dt_ultimaoferta'] = Helper::converterData($venda->getDt_ultimaoferta(), 'Y-m-d');
            } else {
                // DESCOBRIR ID DO PROJETO PEDAGOGICO
                $ng_produto = new \G2\Negocio\Produto();
                $mensageiro = $ng_produto->findProdutoProjetoPedagogicoByArray(array(
                    'id_produto' => $informacoesValor['id_produto']
                ));

                if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                    $dt_oferta = date('Y-m-d');

                    if (isset($params['bl_renovacao']) && $params['bl_renovacao']) {
                        if (isset($params['id_matricula']) && $params['id_matricula']) {
                            $today = date('Y-m-d');

                            /** @var \G2\Repository\SalaDeAula $rep */
                            $rep = $this->getRepository('\G2\Entity\SalaDeAula');
                            $dt_oferta = $rep->retornarDataEncerramentoUltimaSala($params['id_matricula']);

                            // A data precisa ser maior que a data atual
                            if ($today > $dt_oferta) {
                                $dt_oferta = $today;
                            }
                        }
                    } // Caso tenha o parametro de turma, a Data referente pra buscar as salas padrao, deve ser a dt_inicio da Turma
                    else if (isset($params['id_turma']) && $params['id_turma']) {
                        $turma = $this->find('\G2\Entity\Turma', $params['id_turma']);

                        if ($turma instanceof \G2\Entity\Turma) {
                            $dt_oferta = Helper::converterData($turma->getDt_inicio(), 'Y-m-d H:i:s');
                        }
                    }

                    $id_projetopedagogico = $mensageiro->getFirstMensagem()['id_projetopedagogico'];
                    $retorno['disciplinas'] = $this->findSalasPadraoPorData(
                        $dt_oferta,
                        $params['id_usuario'],
                        $id_projetopedagogico,
                        $nuCreditos,
                        !empty($params['id_matricula']) ? $params['id_matricula'] : null
                    );

                    // Das 5 ofertas, buscar a data de término da maior
                    /** @var \G2\Repository\PeriodoLetivo $rep */
                    $rep = $this->getRepository('\G2\Entity\PeriodoLetivo');
                    $ofertas_padrao = $rep->findProximasOfertasPorData(
                        $dt_oferta,
                        $this->idEntidade,
                        \G2\Constante\TipoOferta::PADRAO
                    );

                    if ($ofertas_padrao) {
                        $retorno['nu_totalofertaspadrao'] = count($ofertas_padrao);

                        // Buscar a maior data de encerramento das ofertas padrão encontradas
                        $dt_ultimaoferta = end($ofertas_padrao)->getDt_encerramento();
                        foreach ($ofertas_padrao as $oferta) {
                            if ($oferta->getDt_encerramento() > $dt_ultimaoferta) {
                                $dt_ultimaoferta = $oferta->getDt_encerramento();
                            }
                        }

                        $retorno['dt_ultimaoferta'] = $dt_ultimaoferta;
                    }
                }
            }

            $idEntidade = $this->idEntidade ?: $this->sessao->id_entidade;

            // APLICA O NU_CREDITOS, O VALOR TOTAL DA SOMA DOS NU_CREDITOS DAS DISCIPLINAS ENCONTRADAS
            $nuCreditos = 0;
            foreach ($retorno['disciplinas'] as $disciplina) {
                $nuCreditos += $disciplina['nu_creditos'];
            }

            $ng_esquema = new \G2\Negocio\EsquemaConfiguracao();

            $retorno['id_produto'] = $informacoesValor['id_produto'];
            $retorno['nu_creditos'] = $nuCreditos;
            $retorno['nu_mindisciplinas'] = (int)$ng_esquema->retornaItemPorEntidade(20, ($idEntidade))['st_valor'];
            $retorno['nu_maxdisciplinas'] = (int)$ng_esquema->retornaItemPorEntidade(21, ($idEntidade))['st_valor'];
            $retorno['nu_valorcredito'] = $valorCredito;
            $retorno['nu_valorcreditomostrar'] = str_replace('.', ',', money_format('%.2n', $valorCredito));

            // Busca dados do Contrato e Mensalidade
            $retorno = array_merge($retorno, $this->calcularCampanhas(array(
                'id_venda' => isset($params['id_venda']) ? $params['id_venda'] : null,
                'id_campanhacomercial' => isset($params['id_campanhacomercial']) ? intval($params['id_campanhacomercial']) : null,
                'id_campanhapontualidade' => isset($params['id_campanhapontualidade']) ? intval($params['id_campanhapontualidade']) : null,
                'nu_valorcontrato' => $nuCreditos * $valorCredito,
                'bl_renovacao' => (array_key_exists('bl_renovacao', $params) && isset($params['bl_renovacao'])) ? $params['bl_renovacao'] : null,
                'nu_desconto' => (array_key_exists('nu_desconto', $params) && isset($params['nu_desconto'])) ? $params['nu_desconto'] : null,
            )));

            return $retorno;
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retorna informações de valor. ' . $e->getMessage());
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws \Zend_Exception
     */
    public function calcularCampanhas($data)
    {
        if (!array_key_exists('nu_valorcontrato', $data)) {
            throw new \Zend_Exception('O parâmetro nu_valorcontrato é obrigatório.');
        }

        $data['nu_valorcontrato'] = floatval($data['nu_valorcontrato']);
        $data['nu_valorentrada'] = $data['nu_valorcontrato'] / 6;

        // Se possuir o ID_VENDA, verificar se possui VALOR PAGO DA ENTRADA
        $nu_valorquitado = 0;
        if (isset($data['id_venda']) && $data['id_venda']) {
            $entrada = $this->findOneBy('\G2\Entity\VwVendaLancamento', array(
                'id_venda' => $data['id_venda'],
                'bl_entrada' => true
            ));

            // Se encontrar, entao definir o valor pago
            if ($entrada instanceof \G2\Entity\VwVendaLancamento && ($entrada->getBlQuitado() || ($entrada->getIdMeiopagamento() == \G2\Constante\MeioPagamento::CARTAO_CREDITO && $entrada->getDtPrevquitado()))) {
                $nu_valorquitado = $entrada->getNuValor();
            }
        }

        // Seta os valores padrao
        $data = array(
            'id_campanhacomercial' => isset($data['id_campanhacomercial']) ? intval($data['id_campanhacomercial']) : null,
            'id_campanhapontualidade' => isset($data['id_campanhapontualidade']) ? intval($data['id_campanhapontualidade']) : null,
            'nu_valorcontrato' => $data['nu_valorcontrato'],
            'nu_valornegociacao' => $data['nu_valorcontrato'],
            // DESCONTOS
            'nu_porcentagemdesconto' => 0,
            'nu_desconto' => 0,
            'nu_porcentagemdescontopontualidade' => array_key_exists('nu_desconto', $data) && $data['nu_desconto'] ? $data['nu_desconto'] : 0,
            'nu_descontopontualidade' => 0,
            'nu_valorentrada' => $data['nu_valorentrada'],
            'nu_valordescontoentrada' => 0,
            'nu_valormensalidade' => $data['nu_valorentrada'],
            'nu_valormensalidadepontualidade' => $data['nu_valorentrada'],
            'nu_valorquitado' => $nu_valorquitado,
            'bl_renovacao' => array_key_exists('bl_renovacao', $data) && $data['bl_renovacao'] ? $data['bl_renovacao'] : false
        );
        //VERIFICA SE É RENOVAÇÃO, SE SIM, VAI PEGAR O VALOR DO DESCONTO DA PRIMEIRA VENDA
        //A função max vai retornar 0 caso o numero seja negativo.


        // Calcular desconto se tiver Campanha Comercial
        if (array_key_exists('id_campanhacomercial', $data) && $data['id_campanhacomercial']) {
            $campanha = $this->retornaCampanhaFormatada($data['id_campanhacomercial']);
            $data['nu_desconto'] = 0;
            if (!empty($campanha) && $campanha['id_tipodesconto']['id_tipodesconto'] == \G2\Constante\TipoDesconto::PORCENTAGEM) {
                $data['nu_porcentagemdesconto'] = !empty($campanha['nu_valordesconto']) ? floatval($campanha['nu_valordesconto']) : 0;
                $data['nu_desconto'] = ($data['nu_porcentagemdesconto'] / 100 * $data['nu_valorcontrato']);
            }

            $data['nu_valornegociacao'] -= max($data['nu_desconto'], 0);

            // Redefinir as mensalidades de acordo como novo valor do contrato
            $data['nu_valorentrada'] = max(($data['nu_valornegociacao'] / 6), 0);
            $data['nu_valormensalidade'] = max($data['nu_valorentrada'], 0);
            $data['nu_valormensalidadepontualidade'] = max($data['nu_valorentrada'], 0);
        }

        // Calcular desconto se tiver Campanha de Pontualidade
        if (array_key_exists('id_campanhapontualidade', $data) && $data['id_campanhapontualidade']) {
            $campanha = $this->retornaCampanhaFormatada($data['id_campanhapontualidade']);
            $data['nu_descontopontualidade'] = 0;
            if (!empty($campanha) && $campanha['id_tipodesconto']['id_tipodesconto'] == \G2\Constante\TipoDesconto::PORCENTAGEM) {
                $data['nu_porcentagemdescontopontualidade'] = !empty($campanha['nu_valordesconto']) ? floatval($campanha['nu_valordesconto']) : 0;
                $data['nu_descontopontualidade'] = $data['nu_porcentagemdescontopontualidade'] / 100 * floatval($data['nu_valormensalidade']);
            }
            $data['nu_valormensalidadepontualidade'] -= $data['nu_descontopontualidade'];
        }

        // Realizar o desconto da ENTRADA
        if (array_key_exists('nu_valorquitado', $data) && $data['nu_valorquitado']) {
            $data['nu_valordescontoentrada'] = max(0, ($data['nu_valornegociacao'] / 6 - $data['nu_valorquitado']));
            $data['nu_valorentrada'] = $data['nu_valorquitado'];
        }

        // Valores para mostrar como STRING
        $data['nu_valorcontratomostrar'] = number_format($data['nu_valorcontrato'], 2, ',', '.');
        $data['nu_valornegociacaomostrar'] = number_format($data['nu_valornegociacao'], 2, ',', '.');
        $data['nu_descontomostrar'] = number_format($data['nu_desconto'], 2, ',', '.');
        $data['nu_valorentradamostrar'] = number_format($data['nu_valorentrada'], 2, ',', '.');
        $data['nu_valordescontoentradamostrar'] = number_format($data['nu_valordescontoentrada'], 2, ',', '.');
        $data['nu_valormensalidademostrar'] = number_format($data['nu_valormensalidade'], 2, ',', '.');
        $data['nu_valormensalidadepontualidademostrar'] = number_format($data['nu_valormensalidadepontualidade'], 2, ',', '.');
        $data['nu_valorquitadomostrar'] = number_format($data['nu_valorquitado'], 2, ',', '.');

        return $data;
    }

    /**
     * @param $id_campanha
     * @return array
     * Retorna a campanha formatada.
     */
    public function retornaCampanhaFormatada($id_campanha)
    {
        try {
            /*$campanha = new \G2\Negocio\CampanhaComercial();
            return $this->toArrayEntity($campanha->findCampanha($id_campanha));*/
            return $this->toArrayEntity($this->findOneBy('\G2\Entity\CampanhaComercial', array('id_campanhacomercial' => $id_campanha)));

        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar campanha formatada. ' . $e->getMessage());
        }
    }

    /**
     * @param \G2\Entity\Venda $objVenda
     * @return array $venda
     * @throws \Exception
     * Retorna a venda formatada.
     */
    public function retornaVenda($objVenda)
    {
        try {
            $venda = [];

            /**
             * @var \G2\Entity\VendaProduto $produto
             */
            $produto = $this->findOneBy('\G2\Entity\VendaProduto', array('id_venda' => $objVenda->getId_venda()));

            if ($objVenda) {
                $venda = array(
                    'id_venda' => $objVenda->getId_venda(),
                    'dt_ingresso' => $produto->getDt_ingresso(),
                    'dt_cadastro' => $objVenda->getDt_cadastro(),
                    'nu_descontoporcentagem' => $objVenda->getNu_descontoporcentagem(),
                    'nu_descontovalor' => floatval($objVenda->getNu_descontovalor()),
                    'nu_juros' => $objVenda->getNu_juros(),
                    'bl_ativo' => $objVenda->getBl_ativo(),
                    'id_usuariocadastro' => $objVenda->getId_usuariocadastro(),
                    'nu_valorliquido' => $objVenda->getNu_valorliquido(),
                    'nu_valorbruto' => $objVenda->getNu_valorbruto(),
                    'id_entidade' => $objVenda->getId_entidade(),
                    'nu_parcelas' => $objVenda->getNu_parcelas(),
                    'bl_contrato' => $objVenda->getBl_contrato(),
                    'nu_diamensalidade' => $objVenda->getNu_diamensalidade(),
                    'id_origemvenda' => $objVenda->getId_origemvenda(),
                    'id_vendedor' => $objVenda->getId_vendedor() ? $objVenda->getId_vendedor()->getId_vendedor() : null,
                    'id_nucleotelemarketing' => $objVenda->getId_nucleotelemarketing() ? $objVenda->getId_nucleotelemarketing()->getId_nucleotelemarketing() : null,
                    'id_afiliado' => $objVenda->getId_afiliado() ? $objVenda->getId_afiliado()->getId_afiliado() : null,
                    'st_afiliado' => $objVenda->getId_afiliado() ? $objVenda->getId_afiliado()->getSt_afiliado() : null,
                    'st_siteorigem' => $objVenda->getSt_siteorigem(),
                    'dt_agendamento' => $objVenda->getDt_agendamento(),
                    'dt_confirmacao' => $objVenda->getDt_confirmacao(),
                    'st_observacao' => $objVenda->getSt_observacao(),
                    'nu_valoratualizado' => $objVenda->getNu_valoratualizado(),
                    'recorrete_orderid' => $objVenda->getRecorreteOrderid(),
                    'id_formapagamento' => $objVenda->getId_formapagamento() ? $objVenda->getId_formapagamento()->getId_formapagamento() : null,
                    'id_evolucao' => $objVenda->getId_evolucao() ? $objVenda->getId_evolucao()->getId_evolucao() : null,
                    'id_situacao' => $objVenda->getId_situacao() ? $objVenda->getId_situacao()->getId_situacao() : null,
                    'id_usuario' => $objVenda->getId_usuario() ? $objVenda->getId_usuario()->getId_usuario() : null,
                    'id_tipocampanha' => $objVenda->getId_tipocampanha() ? $objVenda->getId_tipocampanha()->getId_tipocampanha() : null,
                    'id_prevenda' => $objVenda->getId_prevenda(),
                    'id_protocolo' => $objVenda->getId_protocolo() ? $objVenda->getId_protocolo()->getIdProtocolo() : null,
                    'id_enderecoentrega' => $objVenda->getId_enderecoentrega() ? $objVenda->getId_enderecoentrega()->getId_endereco() : null,
                    'id_ocorrencia' => $objVenda->getId_ocorrencia(),
                    'id_cupom' => $objVenda->getId_cupom() ? $objVenda->getId_cupom()->getId_cupom() : null,
                    'st_cupom' => $objVenda->getId_cupom() ? $objVenda->getId_cupom()->getSt_codigocupom() : null,
                    'id_atendente' => $objVenda->getId_atendente() ? $objVenda->getId_atendente()->getId_usuario() : null,
                    'st_atendente' => $objVenda->getId_atendente() ? $objVenda->getId_atendente()->getSt_nomecompleto() : null,
                    'nu_creditos' => $objVenda->getNu_creditos(),
                    'id_campanhacomercial' => $objVenda->getId_campanhacomercial()
                        ? $objVenda->getId_campanhacomercial()->getId_campanhacomercial()
                        : null,
                    'st_campanhacomercial' => $objVenda->getId_campanhacomercial()
                        ? $objVenda->getId_campanhacomercial()->getSt_campanhacomercial()
                        : null,
                    'id_campanhapontualidade' => $objVenda->getId_campanhapontualidade()
                        ? $objVenda->getId_campanhapontualidade()->getId_campanhacomercial()
                        : null,
                    'st_campanhapontualidade' => $objVenda->getId_campanhapontualidade()
                        ? $objVenda->getId_campanhapontualidade()->getSt_campanhacomercial()
                        : null,
                    'nu_viascontrato' => $objVenda->getNu_viascontrato(),
                    'dt_ultimaoferta' => Helper::converterData($objVenda->getDt_ultimaoferta(), 'Y-m-d'),
                    'id_ocorrenciaaproveitamento' => $objVenda->getId_ocorrenciaaproveitamento() ? $objVenda->getId_ocorrenciaaproveitamento()->getId_ocorrencia() : null
                );

                if ($produto instanceof \G2\Entity\VendaProduto) {
                    $venda['id_produto'] = $produto->getId_produto() ? $produto->getId_produto()->getId_produto() : null;
                    $venda['id_vendaproduto'] = $produto->getId_vendaproduto() ? $produto->getId_vendaproduto() : null;
                    $venda['id_turma'] = $produto->getId_turma() ? $produto->getId_turma()->getId_turma() : null;
                    $venda['id_tiposelecao'] = $produto->getId_tiposelecao() ? $produto->getId_tiposelecao()->getId_tiposelecao() : null;
                    $venda['id_matricula'] = $produto->getId_matricula() ? $produto->getId_matricula()->getId_matricula() : null;

                    // Campanha Comercial da tb_vendaproduto é usado na funcionalidade "Negociação - Graduação" ao invés da tb_venda
                    $venda['id_campanhacomercial'] = $produto->getId_campanhacomercial()
                        ? $produto->getId_campanhacomercial()->getId_campanhacomercial()
                        : null;
                    $venda['st_campanhacomercial'] = $produto->getId_campanhacomercial()
                        ? $produto->getId_campanhacomercial()->getSt_campanhacomercial()
                        : null;
                }

                $contrato = $this->findOneBy('\G2\Entity\Contrato', array('id_venda' => $objVenda->getIdVenda()));

                if ($contrato instanceof \G2\Entity\Contrato) {
                    $venda['id_contratoregra'] = $contrato->getId_contratoregra()
                        ? $contrato->getId_contratoregra()->getId_contratoregra()
                        : null;

                    $venda['st_contratoregra'] = $contrato->getId_contratoregra()
                        ? $contrato->getId_contratoregra()->getSt_contratoregra()
                        : null;
                }

                // Assunto (solicitacao de isencao) da venda
                $ng_cfgentidade = new \G2\Negocio\ConfiguracaoEntidade();
                $cfg = $ng_cfgentidade->findConfiguracaoEntidade($ng_cfgentidade->sessao->id_entidade, false);
                $venda['id_assuntoisencao'] = $cfg && $cfg->getId_assuntoisencao() ? $cfg->getId_assuntoisencao()->getId_assuntoco() : null;
                $venda['st_assuntoisencao'] = $cfg && $cfg->getId_assuntoisencao() ? $cfg->getId_assuntoisencao()->getSt_assuntoco() : null;

                $venda['nu_mesesintervalovencimento'] = 1;

                $venc_entrada = null;
                $venc_parcela = null;

                $lancamentos = $this->findBy('\G2\Entity\VwVendaLancamento', array('id_venda' => $objVenda->getIdVenda()));
                if (is_array($lancamentos) && !empty($lancamentos)) {
                    $venda['bl_quitadaparcela'] = 0;

                    foreach ($lancamentos as $lancamento) {
                        if ($lancamento->getBlEntrada()) {
                            $venda['id_meiopagamentoentrada'] = $lancamento->getIdMeiopagamento();
                            $venda['dt_vencimentoentrada'] = \G2\Utils\Helper::converterData($lancamento->getDtVencimento(),
                                'd/m/Y');
                            $venda['bl_quitadaentrada'] = ($lancamento->getBlQuitado() ? 1 : 0);
                            $venda['dt_prevquitado'] = ($lancamento->getDtPrevquitado() ? $lancamento->getDtPrevquitado() : null);

                            $venc_entrada = $lancamento->getDtVencimento();
                        } else if ($lancamento->getNuOrdem() == 2) {
                            $venda['id_meiopagamentoparcelas'] = $lancamento->getIdMeiopagamento();
                            $venda['nu_vencimentoparcelas'] = $lancamento->getDtVencimento()->format('d');
                            $venda['id_responsavelfinanceirograd'] = $lancamento->getIdUsuariolancamento();

                            if (!$venc_parcela) {
                                $venc_parcela = $lancamento->getDtVencimento();
                            }
                        }

                        if ($lancamento->getNuOrdem() >= 2 && $lancamento->getBlQuitado()) {
                            $venda['bl_quitadaparcela'] = 1;
                        }
                    }

                    // Descobrir a quantidade de meses de intervalo da Entrada para a Segunda Parcela
                    if ($venc_entrada && $venc_parcela) {
                        $diff = $venc_parcela->diff($venc_entrada);
                        $venda['nu_mesesintervalovencimento'] = (($diff->format('%y') * 12) + $diff->format('%m'));
                    }
                } else {
                    $venda['id_meiopagamentoentrada'] = '';
                    $venda['id_meiopagamentoentrada'] = '';
                    $venda['dt_vencimentoentrada'] = '';
                    $venda['id_meiopagamentoparcelas'] = '';
                    $venda['nu_vencimentoparcelas'] = '';
                    $venda['id_responsavelfinanceirograd'] = '';
                    $venda['bl_quitadaentrada'] = 0;
                    $venda['bl_quitadaparcela'] = 0;
                }
            }
            return $venda;
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retorna informações da venda. ' . $e->getMessage());
        }
    }

    public function calculaValoresContrato($id_disciplinas, $params = array())
    {
        try {
            // Buscar informações de valor do produto
            $id_produto = !empty($params['id_produto']) ? $params['id_produto'] : null;
            $informacoesValor = $this->retornaInformacoesCredito($id_produto);

            // SETAR OS VALORES PADRAO
            $ng_esquema = new \G2\Negocio\EsquemaConfiguracao();
            $return = array(
                //'nu_creditos'       => (int)$ng_esquema->retornaItemPorEntidade(19, $this->sessao->id_entidade)['st_valor'],
                'nu_creditos' => 0,
                'nu_mindisciplinas' => (int)$ng_esquema->retornaItemPorEntidade(20, $this->sessao->id_entidade)['st_valor'],
                'nu_maxdisciplinas' => (int)$ng_esquema->retornaItemPorEntidade(21, $this->sessao->id_entidade)['st_valor'],
                'id_disciplina' => array(),
                'nu_desconto' => 0
            );

            // BUSCAR AS DISCIPLINAS E PEGAR A QUANTIDADE DE CREDITOS QUE ELA CUSTA
            $nu_discSelecionadas = 0;
            if ($id_disciplinas && is_array($id_disciplinas)) {
                foreach ($id_disciplinas as $id_disciplina) {
                    $en_disciplina = $this->find('\G2\Entity\Disciplina', $id_disciplina);
                    if ($en_disciplina) {
                        $nu_discSelecionadas++;
                        $nu_creditos = $en_disciplina->getNu_creditos();

                        $return['nu_creditos'] += $nu_creditos;
                        $return['id_disciplina'][] = $en_disciplina->getId_disciplina();
                    }
                }
            }

            $return['nu_disciplinasselecionadas'] = $nu_discSelecionadas;
            $return['nu_valorcredito'] = floatval(number_format($informacoesValor['nu_valormensal'], 2, '.', ''));
            $return['nu_valorcreditomostrar'] = str_replace('.', ',', money_format('%.2n', $return['nu_valorcredito']));

            // Busca dados do Contrato e Mensalidade
            $return = array_merge($return, $this->calcularCampanhas(array(
                'id_venda' => isset($params['id_venda']) ? $params['id_venda'] : null,
                'id_campanhacomercial' => isset($params['id_campanhacomercial']) ? intval($params['id_campanhacomercial']) : null,
                'id_campanhapontualidade' => isset($params['id_campanhapontualidade']) ? intval($params['id_campanhapontualidade']) : null,
                'nu_valorcontrato' => $return['nu_creditos'] * $return['nu_valorcredito']
            )));

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return new \Ead1_Mensageiro($return);
    }

    /**
     * @description Retorna todas as salas disponiveis para montar a Grade a partir de uma Turma
     * @param $id_produto
     * @param $id_usuario
     * @param $params
     * @param $portal
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function findSalasDisponiveisOfertaTurma($id_produto, $id_usuario, $params = array(), $portal = false)
    {
        if (!isset($params['id_projetopedagogico'])) {
            // Descobrir qual e o Projeto Pedagogico do Produto
            $ng_produto = new \G2\Negocio\Produto();
            $mensageiro = $ng_produto->findProdutoProjetoPedagogicoByArray(array('id_produto' => $id_produto));

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception('Nenhum projeto pedagógico encontrado para este produto');
            }
            $projeto = $mensageiro->getFirstMensagem();

            $params['id_projetopedagogico'] = $projeto['id_projetopedagogico'];
        }

        if (!isset($params['id_entidade'])) {
            $params['id_entidade'] = $this->sessao->id_entidade;
        }

        $id_matricula = isset($params['id_matricula']) ? $params['id_matricula'] : null;
        $id_tipooferta = isset($params['id_tipooferta']) ? $params['id_tipooferta'] : null;
        unset($params['id_matricula'], $params['id_tipooferta']);

        // Se não tiver o parâmetro id_matricula, então buscar usando o usuário e projeto pedagógico (se possuir)
        if (!$id_matricula) {
            /** @var \G2\Repository\Matricula $rep */
            $rep = $this->getRepository('\G2\Entity\Matricula');
            $id_matricula = $rep->retornarMatriculaAtivaProjeto($id_usuario, $params['id_projetopedagogico']);
        }

        $grupos = array();
        $salas = array();

        /**
         * Buscar 5 proximas Ofertas de Sala para montar o array de Grupos
         */
        $ofertas = array();
        $dt_base = null;

        if ($portal) {
            $today = date('Y-m-d');

            /** @var \G2\Repository\SalaDeAula $rep */
            $rep = $this->getRepository('\G2\Entity\SalaDeAula');
            $dt_base = $rep->retornarDataEncerramentoUltimaSala($id_matricula);

            // A data precisa ser maior que a data atual
            if ($today > $dt_base) {
                $dt_base = $today;
            }
        } else if (isset($params['id_turma']) && $params['id_turma']) {
            // Busca a turma correspondete para poder usar sua data no filtro de Ofertas
            $turma = $this->find('\G2\Entity\Turma', $params['id_turma']);
            if ($turma) {
                $dt_base = $turma->getDt_inicio();
            }
        }

        if ($dt_base) {
            /** @var \G2\Repository\PeriodoLetivo $rep */
            $rep = $this->getRepository('\G2\Entity\PeriodoLetivo');
            $ofertas = $rep->findProximasOfertasPorData($dt_base, null, $id_tipooferta);
        }

        if (!$ofertas) {
            throw new \Exception('Nenhuma oferta de sala disponível.');
        }

        /**
         * Montar o array de Grupos
         */
        // Indexes usado para que ao converter para JSON, o array continue na mesma ordem
        $indexes = array();
        foreach ($ofertas as $key => $oferta) {
            $grupos[] = $this->converterData($oferta->getDt_abertura(), 'd/m/Y') . ' - ' . $this->converterData($oferta->getDt_encerramento(), 'd/m/Y') . '<span class="hidden"> (' . $oferta->getId_periodoletivo() . ')</span>';
            $indexes[] = $oferta->getId_periodoletivo();
        }

        // Adicionar o parametro de periodo letivo, pra trazer somente as salas dos 5 primeiros periodos letivos buscados acima
        $params['id_periodoletivo'] = array_map(function ($item) {
            return $item->getId_periodoletivo();
        }, $ofertas);

        // Buscas todas as salas
        if ($portal) {
            $results = $this->findBy('\G2\Entity\VwSalaOfertaRenovacao', array_filter($params, function ($key) {
                return $key != 'id_turma';
            }, ARRAY_FILTER_USE_KEY));
        } else {
            $results = $this->findBy('\G2\Entity\VwSalaOfertaTurma', $params);
        }

        if ($results) {
            // Buscar os dados para a verificação da obrigatoriedade da Disciplina
            $nu_percentualaprovado = $this->getPercentualAprovado($id_matricula, $params['id_projetopedagogico']);

            // Buscar o semestre atual e somar 1 para definir o próximo semestre
            $nu_semestre = $this->negocioMatricula->getSemestreAtual($id_matricula) + 1;

            foreach ($results as $sala) {
                $oferta = current(array_filter($ofertas, function ($item) use ($sala) {
                    return $item->getId_periodoletivo() == $sala->getId_periodoletivo();
                }));

                // Buscar informações do componente curricular obrigatório
                $modulodisciplina = $this->findOneBy('\G2\Entity\ModuloDisciplina', array(
                    'id_disciplina' => $sala->getId_disciplina(),
                    'id_modulo' => $sala->getId_modulo(),
                    'bl_ativo' => true
                ));

                // Verificar obrigatoriedade da Disciplina
                $arrayRetorno = $this->verificarDisciplinaObrigatoriaOuDisponivel($modulodisciplina->getNu_obrigatorioalocacao(), $nu_semestre, $nu_percentualaprovado, $modulodisciplina->getNu_disponivelapartirdo());

                $bl_obrigatorio = $arrayRetorno['bl_obrigatorio'];
                $bl_disponivel = $arrayRetorno['bl_disponivel'];

                if (!$bl_disponivel) {
                    continue;
                }

                // Buscar o status do aluno na disciplina
                /** @var \G2\Repository\SalaDeAula $rep */
                $rep = $this->getRepository('\G2\Entity\SalaDeAula');
                $status = $rep->getStatusAlunoSala($id_usuario, $sala->getId_disciplina(), $sala->getId_saladeaula(), $params['id_projetopedagogico']);

                $salas[$sala->getId_saladeaula()] = array(
                    'id_saladeaula' => $sala->getId_saladeaula(),
                    'st_saladeaula' => $sala->getSt_saladeaula(),
                    'id_disciplina' => $sala->getId_disciplina(),
                    'st_disciplina' => $sala->getSt_disciplina(),
                    'nu_creditos' => $sala->getNu_creditos(),
                    'id_evolucao' => $status['id_evolucao'],
                    'st_status' => $status['st_status'],
                    'id_periodoletivo' => $oferta->getId_periodoletivo(),
                    'id_tipooferta' => $oferta->getId_tipooferta(),
                    'dt_abertura' => Helper::converterData($sala->getDt_abertura(), 'Y-m-d'),
                    'dt_encerramento' => Helper::converterData($sala->getDt_encerramento(), 'Y-m-d'),
                    'nu_obrigatorioalocacao' => $modulodisciplina->getNu_obrigatorioalocacao(),
                    'nu_disponivelapartirdo' => $modulodisciplina->getNu_disponivelapartirdo(),
                    'bl_obrigatorio' => $bl_obrigatorio,
                    'id_group' => array_search($sala->getId_periodoletivo(), $indexes)
                );
            }
        }

        return array(
            'grupos' => $grupos,
            'salas' => array_values($salas),
            'ofertas' => $this->toArrayEntity($ofertas)
        );
    }

    /**
     * @param int $id_venda
     * @return array
     * @throws \Zend_Exception
     */
    public function findSalasVenda($id_venda)
    {
        $results = array();

        if ($id_venda) {
            $venda_disc = $this->findBy('\G2\Entity\PreMatriculaDisciplina', array(
                'id_venda' => $id_venda
            ));

            if ($venda_disc) {
                $disciplinas = $this->findBy('\G2\Entity\VwSalaOferta', array(
                    'id_saladeaula' => array_map(function ($current) {
                        return $current->getId_saladeaula();
                    }, $venda_disc)
                ));

                if ($disciplinas) {
                    foreach ($disciplinas as $disciplina) {
                        $results[$disciplina->getId_saladeaula()] = $this->toArrayEntity($disciplina);
                    }
                }
            }
        }

        return array_values($results);
    }

    /**
     * @param $dt_abertura
     * @param $id_usuario
     * @param $id_projetopedagogico
     * @param $limit
     * @return array
     * @throws \Exception
     */
    public function findSalasPadraoPorData($dt_abertura, $id_usuario, $id_projetopedagogico, $limit, $id_matricula = null)
    {
        /** @var \G2\Repository\PeriodoLetivo $rep */
        $rep = $this->getRepository('\G2\Entity\PeriodoLetivo');
        $ofertas_padrao = $rep->findProximasOfertasPorData($dt_abertura, $this->idEntidade,
            \G2\Constante\TipoOferta::PADRAO);
        $ofertas_estendidas = $rep->findProximasOfertasPorData($dt_abertura, $this->idEntidade,
            \G2\Constante\TipoOferta::ESTENDIDA);

        if (!$ofertas_padrao) {
            return array();
        }

        // Se não tiver o parâmetro id_matricula, então buscar usando o usuário e projeto pedagógico (se possuir)
        if (!$id_matricula) {
            /** @var \G2\Repository\Matricula $rep */
            $rep = $this->getRepository('\G2\Entity\Matricula');
            $id_matricula = $rep->retornarMatriculaAtivaProjeto($id_usuario, $id_projetopedagogico);
        }

        $order = array(
            // Valores com bl_ofertaexcepcional = 1 devem vir por ultimo
            'bl_ofertaexcepcional' => 'ASC',
            'st_modulo' => 'ASC',
            'nu_ordem' => 'ASC',
            'id_saladeaula' => 'ASC'
        );

        // Inicializa variaveis
        $salas_padrao = array();
        $salas_extendidas = array();
        $salas_selecionadas = array();

        // Buscar as Salas com Oferta de id_tipooferta = 1 (Padrão)
        if ($ofertas_padrao) {
            $salas_padrao = $this->findBy('\G2\Entity\VwSalaOferta', array(
                'id_entidade' => $this->idEntidade != null ? $this->idEntidade : $this->sessao->id_entidade,
                'id_projetopedagogico' => $id_projetopedagogico,
                'id_periodoletivo' => array_map(function ($oferta) {
                    return $oferta->getId_periodoletivo();
                }, $ofertas_padrao)
            ), $order);
        }

        // Buscar as Salas com Oferta de id_tipooferta = 2 (Estendida)
        if ($ofertas_estendidas) {
            $salas_extendidas = $this->findBy('\G2\Entity\VwSalaOferta', array(
                'id_entidade' => $this->idEntidade != null ? $this->idEntidade : $this->sessao->id_entidade,
                'id_projetopedagogico' => $id_projetopedagogico,
                'id_periodoletivo' => array_map(function ($oferta) {
                    return $oferta->getId_periodoletivo();
                }, $ofertas_estendidas)
            ), $order);
        }

        // Se tiver matrícula, verificar o trancamento e dar prioridade para as disciplinas Trancadas e Insatisfatórias, colocando-as no topo do array
        if ($id_matricula) {
            $salas_trancadas = array();
            foreach ($salas_padrao as $key => $salaTrancamento) {
                $trancamento = $this->findOneBy('\G2\Entity\MatriculaDisciplina', array(
                    'id_matricula' => $id_matricula,
                    'id_disciplina' => $salaTrancamento->getId_disciplina(),
                    'id_evolucao' => array(
                        \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO,
                        \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_TRANCADA
                    )
                ));

                if ($trancamento instanceof \G2\Entity\MatriculaDisciplina) {
                    $salas_trancadas[] = $salaTrancamento;
                    unset($salas_padrao[$key]);
                }
            }

            // Caso tenha alguma disciplina Trancada ou Insatisfatória, add ela no começo da array para dar prioridade ao selecionar as salas
            if (!empty($salas_trancadas)) {
                $salas_padrao = array_merge($salas_trancadas, $salas_padrao);
            }
        }

        /** @var \G2\Entity\VwSalaOferta[] $salas */
        $salas = array_merge($salas_padrao, $salas_extendidas);

        if ($salas) {
            // Buscar os dados para a verificação da obrigatoriedade da Disciplina
            $nu_percentualaprovado = $this->getPercentualAprovado($id_matricula, $id_projetopedagogico);

            // Buscar o semestre atual e somar 1 para definir o próximo semestre
            $nu_semestre = $this->negocioMatricula->getSemestreAtual($id_matricula) + 1;

            $max_ofertas = array();
            foreach ($salas as $sala) {
                if ($id_matricula) {
                    // Se possuir APROVEITAMENTO DE DISCIPLINA, Ignorar
                    $aproveitamento = $this->findOneBy('\G2\Entity\MatriculaDisciplina', array(
                        'id_matricula' => $id_matricula,
                        'id_disciplina' => $sala->getId_disciplina(),
                        'id_situacao' => \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_CONCEDIDA
                    ));

                    if ($aproveitamento instanceof \G2\Entity\MatriculaDisciplina) {
                        continue;
                    }
                }

                $id_periodoletivo = $sala->getId_periodoletivo();
                $id_tipooferta = $sala->getId_tipooferta();

                // Verificar obrigatoriedade da Disciplina
                $arrayRetorno = $this->verificarDisciplinaObrigatoriaOuDisponivel($sala->getNu_obrigatorioalocacao(), $nu_semestre, $nu_percentualaprovado, $sala->getNu_disponivelapartirdo());

                $bl_obrigatorio = $arrayRetorno['bl_obrigatorio'];
                $bl_disponivel = $arrayRetorno['bl_disponivel'];

                // Se ja tiver adicionado 2 disciplinas "Não Obrigatorias" da mesma oferta, então não pode adicionar mais
                if (!isset($max_ofertas[$id_periodoletivo])) {
                    $max_ofertas[$id_periodoletivo] = array();
                }
                if (count($max_ofertas[$id_periodoletivo]) >= 2) {
                    continue;
                }

                // Se o id_tipooferta da Oferta for ESTENDIDA, deverá adicionar somente se for Obrigatória
                if ($id_tipooferta == \G2\Constante\TipoOferta::ESTENDIDA && !$bl_obrigatorio) {
                    continue;
                }

                if (!$bl_disponivel) {
                    continue;
                }

                // Verificar limite APENAS para Salas NÃO são Obrigatórias
                if (!$bl_obrigatorio && count(array_filter($salas_selecionadas, function ($item) {
                        return !$item['bl_obrigatorio'];
                    })) >= $limit
                ) {
                    continue;
                }

                /** @var \G2\Repository\SalaDeAula $rep */
                $rep = $this->getRepository('\G2\Entity\SalaDeAula');
                $status = $rep->getStatusAlunoSala(
                    $id_usuario,
                    $sala->getId_disciplina(),
                    $sala->getId_saladeaula(),
                    $id_projetopedagogico
                );

                // Esse index é usado para que nao adicione uma sala da MESMA DISCIPLINA no array
                $index = $sala->getId_disciplina();
                if (!isset($salas_selecionadas[$index])
                    && $status['id_evolucao'] != \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO
                    && $status['id_evolucao'] != \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO
                ) {
                    $salas_selecionadas[$index] = $this->toArrayEntity($sala);
                    $salas_selecionadas[$index]['bl_obrigatorio'] = $bl_obrigatorio;

                    // Adicionar ao contador de Maximo disciplinas apenas as que não são obrigatorias
                    if (!$bl_obrigatorio) {
                        $max_ofertas[$id_periodoletivo][] = $sala->getId_saladeaula();
                    }
                }
            }
        }
        return array_values($salas_selecionadas);
    }

    /**
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function saveOcorrencia($params)
    {
        try {
            $this->beginTransaction();

            $objetoOcorrencia = new \G2\Entity\Ocorrencia();
            if ($params['id_ocorrencia']) {
                $objetoOcorrencia = $this->find('\G2\Entity\Ocorrencia', $params['id_ocorrencia']);
            }
            $objetoOcorrencia->setId_categoriaocorrencia($this->getReference('\G2\Entity\CategoriaOcorrencia', 137));
            $objetoOcorrencia->setId_evolucao($this->getReference('\G2\Entity\Evolucao', \G2\Constante\Evolucao::AGUARDANDO_ATENDIMENTO));
            $objetoOcorrencia->setId_situacao($this->getReference('\G2\Entity\Situacao', \G2\Constante\Situacao::TB_OCORRENCIA_PENDENTE));
            $objetoOcorrencia->setId_usuariointeressado($this->getReference('\G2\Entity\Usuario', $params['id_usuario']));
            $objetoOcorrencia->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objetoOcorrencia->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
            $objetoOcorrencia->setSt_titulo('Aproveitamento de Disciplina');
            $objetoOcorrencia->setSt_ocorrencia($params['st_descricao']);
            $objetoOcorrencia->setDt_cadastro(new \DateTime());
            $objetoOcorrencia->setDt_atendimento(new \DateTime());
            $objetoOcorrencia->setId_assuntoco($this->getReference('\G2\Entity\AssuntoCo', $params['id_subassunto']));
            $objetoOcorrencia->setId_venda($params['id_venda']);

            $ocorrencia = $this->save($objetoOcorrencia);

            $objetoVenda = $this->find('\G2\Entity\Venda', $params['id_venda']);
            $objetoVenda->setId_ocorrenciaaproveitamento($ocorrencia);
            $venda = $this->save($objetoVenda);

            $this->commit();

            $arrOcorrencia = $this->toArrayEntity($ocorrencia);
            $arrOcorrencia['id_assuncoco'] = $ocorrencia->getId_assuntoco()->getId_assuntoco();
            $arrOcorrencia['id_evolucao'] = $ocorrencia->getId_assuntoco()->getId_assuntoco();
            $arrOcorrencia['id_situacao'] = $ocorrencia->getId_assuntoco()->getId_assuntoco();
            $arrOcorrencia['id_usuariocadastro'] = $ocorrencia->getId_assuntoco()->getId_assuntoco();
            $arrOcorrencia['id_usuariointeressado'] = $ocorrencia->getId_assuntoco()->getId_assuntoco();


            return new \Ead1_Mensageiro(array('venda' => $venda, 'ocorrencia' => $ocorrencia), \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {
            $this->rollback();
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

    }

    /**
     * Renova automaticamente alunos na graduação
     * Cria uma nova venda, e envia um e-mail mostrando a data de limite para alteração da grade e forma de pagamento
     * @see RoboController.php::renovaMatriculaGraduacaoAction
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function renovacaoAutomaticaMatricula($params = array())
    {
        try {
            $renovacoesSucesso = 0;
            $erro = '';

            // Buscar os alunos que estão aptos a renovação da matricula
            $alunos = $this->findVwAlunosRenovacao($params);

            if (is_array($alunos) && !empty($alunos)) {
                $ng_esquema = new \G2\Negocio\EsquemaConfiguracao();

                $nuAlunos = count($alunos);

                foreach ($alunos as $index => $aluno) {
                    $venda = $aluno->getId_venda();

                    if (!($venda instanceof \G2\Entity\Venda)) {
                        $erro .= 'Venda não encontrada.<br/>';
                        continue;
                    }

                    $this->idEntidade = (int)$aluno->getId_entidadematricula();
                    $id_venda = $venda->getId_venda();

                    // Buscar a quantidade mínima de disciplinas que está configurado na entidade
                    $minDisc = (int)$ng_esquema->retornaItemPorEntidade(20,
                        ($this->idEntidade !== null ? $this->idEntidade : $this->sessao->id_entidade))['st_valor'];

                    // Remover a descrição do último erro e salvar a data de dentativa da renovação
                    $venda->setSt_errorenovacao(null);
                    $venda->setDt_tentativarenovacao(date('Y-m-d'));
                    $this->save($venda);

                    $vendaProduto = $this->find('\G2\Entity\VendaProduto', $aluno->getId_vendaproduto());

                    //define o id da campanha comercial
                    $idCampanhaComercial = null;
                    if ($vendaProduto->getId_campanhacomercial() instanceof \G2\Entity\CampanhaComercial) {
                        $idCampanhaComercial = (int)$vendaProduto->getId_campanhacomercial()->getId_campanhacomercial();
                    }

                    $valores = $this->retornaInformacoesValores(array(
                        'id_venda' => '',
                        'id_projetopedagogico' => $aluno->getId_projetopedagogico(),
                        'id_usuario' => $aluno->getId_usuario(),
                        'id_produto' => $aluno->getId_produto(),
                        'id_campanhacomercial' => $idCampanhaComercial,
                        'bl_renovacao' => true,
                        'nu_desconto' => $vendaProduto->getNu_desconto(),
                        'id_matricula' => $aluno->getId_matricula()
                    ));

                    // Verificar se os dados foram buscados corretamente
                    if (!$valores) {
                        $venda->setSt_errorenovacao('Não foi possível buscar os dados da venda');
                    } // Se retornar erro, registrar o erro
                    else if (!empty($valores['error'])) {
                        $venda->setSt_errorenovacao($valores['error']);
                    } // Se o número de ofertas PADRÃO for menor que 5, não deve permitir a renovação
                    else if (empty($valores['nu_totalofertaspadrao']) || $valores['nu_totalofertaspadrao'] < 5) {
                        $venda->setBl_semsalarenovacao(true);
                        $venda->setSt_errorenovacao('Não foram encontradas ofertas padrões disponíveis (min 5)');
                    } // Se o número de salas for menor que o configurado na entidade, não deve permitir a renovação
                    else if (empty($valores['disciplinas']) || count($valores['disciplinas']) < $minDisc) {
                        $venda->setBl_semsalarenovacao(true);
                        $venda->setSt_errorenovacao('Não foram encontradas salas disponíveis (min: ' . $minDisc . ')');
                    }

                    // Não continuar caso possua erro
                    if ($venda->getSt_errorenovacao()) {
                        $erro .= 'Venda: ' . $id_venda . ' - ' . $venda->getSt_errorenovacao() . '.<br/>';
                        $this->save($venda);
                        continue;
                    }

                    // A data limite da renovação será a data atual + 7 dias
                    $dt_limiterenovacao = new \DateTime();
                    $dt_limiterenovacao->add(new \DateInterval('P7D'));

                    $novaVenda = array(
                        'id_produto' => $aluno->getId_produto(),
                        'nu_creditos' => $valores['nu_creditos'],
                        'nu_descontovalor' => $valores['nu_desconto'],
                        'nu_valorbruto' => $valores['nu_valorcontrato'],
                        'nu_valorliquido' => $valores['nu_valornegociacao'],
                        'id_campanhapontualidade' => (
                        $venda->getId_campanhapontualidade() instanceof \G2\Entity\CampanhaComercial ?
                            $venda->getId_campanhapontualidade()->getId_campanhacomercial() : null
                        ),
                        'id_campanhacomercial' => $idCampanhaComercial,
                        'id_turma' => $aluno->getId_turma(),
                        'id_tiposelecao' => $aluno->getId_tiposelecao(),
                        'id_atendente' => $venda->getId_atendente() instanceof \G2\Entity\Usuario ?
                            $venda->getId_atendente()->getId_usuario() : null,
                        'id_contratoregra' => $aluno->getId_contratoregra(),
                        'id_evolucao' => '',
                        'id_situacao' => '',
                        'id_venda' => '',
                        'id_entidade' => $aluno->getId_entidadematricula(),
                        'dt_cadastro' => new \DateTime(),
                        'bl_ativo' => true,
                        'id_usuariocadastro' => $aluno->getId_usuario(),
                        'id_usuario' => $aluno->getId_usuario(),
                        'bl_contrato' => $venda->getBl_contrato(),
                        'id_matricula' => $aluno->getId_matricula(),
                        'bl_renovacao' => true,
                        'dt_ultimaoferta' => empty($valores['dt_ultimaoferta']) ? null : $valores['dt_ultimaoferta'],
                        'dt_limiterenovacao' => $dt_limiterenovacao
                    );

                    //Organiza array de lancamentos da nova venda
                    $vw_vendalancamento = $this->findOneBy('G2\Entity\VwVendaLancamento', array(
                        'id_venda' => $id_venda,
                        'bl_entrada' => true
                    ));

                    if (!($vw_vendalancamento instanceof \G2\Entity\VwVendaLancamento)) {
                        $venda->setSt_errorenovacao('Não foi encontrado a parcela do tipo Entrada');
                        $erro .= 'Venda: ' . $id_venda . ' - ' . $venda->getSt_errorenovacao() . '.<br/>';
                        $this->save($venda);
                        continue;
                    }

                    $lancamentos = false;
                    if ($valores['nu_valorentrada']) {
                        $lancamentos = $this->geraLancamentosGraduacao(
                            $valores,
                            $venda,
                            \G2\Constante\MeioPagamentoGraduacao::BOLETO,
                            $vw_vendalancamento,
                            $dt_limiterenovacao
                        );
                    }

                    //pessoa
                    $pessoa = array('id_usuario' => $aluno->getId_usuario());

                    $arraySalvar = array(
                        'pessoa' => $pessoa,
                        'venda' => $novaVenda,
                        'disciplinas' => $valores['disciplinas']
                    );

                    if ($lancamentos) {
                        $arraySalvar['lancamentos'] = $lancamentos;
                    }

                    $vendaNegocio = new \G2\Negocio\Venda();

                    $this->beginTransaction();

                    $parametros = $this->trataParametros($arraySalvar);
                    $parametros['renovacaoautomatica'] = true;
                    $vendaSave = $vendaNegocio->procedimentoSalvarVenda($parametros);

                    if ($vendaSave->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                        $this->rollback();
                        $venda->setSt_errorenovacao($vendaSave->getFirstMensagem());
                        $erro .= 'Venda: ' . $id_venda . ' - ' . $venda->getSt_errorenovacao() . ' <br/>';
                        $this->save($venda);
                    } else {
                        // Salvar a dt_ultimaoferta da ÚLTIMA venda no tb_matrícula
                        /** @var \G2\Entity\Matricula $en_matricula */
                        $en_matricula = $this->find('\G2\Entity\Matricula', $aluno->getId_matricula());
                        $en_matricula->setDt_ultimaoferta(Helper::converterData($novaVenda['dt_ultimaoferta'],
                            'datetime'));
                        $this->save($en_matricula);

                        // Buscar o semestre atual e somar 1 para definir o próximo semestre
                        $nu_semestre = $this->negocioMatricula->getSemestreAtual($aluno->getId_matricula()) + 1;

                        // Salvar o novo semestre na matricula
                        $this->negocioMatricula->alterarSemestreMatricula($aluno->getId_matricula(), $nu_semestre);

                        $this->commit();
                        $renovacoesSucesso++;
                        $this->negocioMatricula->destrancarDisciplinas($aluno->getId_matricula());
                        //envia e-mail de renovacao para o aluno
                        $this->enviaEmailRenovacaoGraduacao($this->retornaVenda($vendaSave->getFirstMensagem()),
                            (int)$aluno->getId_matricula());
                    }
                }

                return new \Ead1_Mensageiro($renovacoesSucesso . ' renovações executadas com sucesso. Total de '
                    . $nuAlunos . ' aluno (s). <br/>' . $erro, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum aluno para ser renovado.', \Ead1_IMensageiro::AVISO);
            }

        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Busca alunos aptos a renovação automatica de matricula
     * @param array $params
     * @param int $limit - Devido a grande quantidade de metodos chamados, é importante ter um limite de busca desses alunos
     * @return bool|\G2\Entity\VwAlunosRenovacao[]
     */
    public function findVwAlunosRenovacao($params = array(), $limit = 20)
    {
        try {
            return $this->findBy(
                '\G2\Entity\VwAlunosRenovacao',
                $params,
                null,
                (int)$limit
            );
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Gerar lançamentos para graduação
     * @param array $params
     * @param \G2\Entity\Venda $venda
     * @param int $meioPagamento
     * @param \G2\Entity\VwVendaLancamento $primeiroLancamento
     * @param string|\DateTime|\Zend_Date $dt_limiterenovacao
     * @return array
     */
    public function geraLancamentosGraduacao(
        $params,
        \G2\Entity\Venda $venda,
        $meioPagamento = \G2\Constante\MeioPagamentoGraduacao::BOLETO,
        $primeiroLancamento = null,
        $dt_limiterenovacao = null
    )
    {
        try {
            $arrayLancamentos = array();
            $dtVencimentoRenovacao = new \Zend_Date();
            $dt_limiterenovacao = $this->converterData($dt_limiterenovacao ?: date('Y-m-d'), 'Y-m-d');

            /*
             * GII-8091 - Alterar a lógica do dia do vencimento do boleto bancário no processo de renovação de matrícula;
             * REGRA: A última parcela original venceu no dia 08/08/2017, então o sistema deve gerar os próximos boletos
             * com os vencimentos para 08/09, 08/10, 08/11, 08/12, 08/01/2018 e 08/02/2018.
             *
             * Buscar o último lançamento vinculado a venda baseado na data de vencimento para tomar como base esta data
             * para gerar os próximos lançamentos da venda
             */

            /** @var \G2\Entity\VwVendaLancamento $ultimoLancamento */
            $ultimoLancamento = $this->findOneBy("\G2\Entity\VwVendaLancamento", array(
                'id_venda' => $venda->getId_venda(),
                'bl_ativo' => true,
                'bl_original' => true
            ), array(
                'dt_vencimento' => 'DESC'
            ));

            //encontrou o ultimo lançamento
            if ($ultimoLancamento) {
                $dtVencimentoRenovacao = Helper::converterData($ultimoLancamento->getDt_vencimento(), 'zend_date');
            }

            $diaVencimento = $this->getDiaVencimentoRenovacoes($primeiroLancamento);

            if ($diaVencimento > $dtVencimentoRenovacao->get(\Zend_Date::MONTH_DAYS)) {
                $dtVencimentoRenovacao->setDay($dtVencimentoRenovacao->get(\Zend_Date::MONTH_DAYS));
            } else {
                $dtVencimentoRenovacao->setDay($diaVencimento);
            }

            // Enquanto a NOVA data de vencimento do primeiro lançamento for MENOR
            // que a DATA LIMITE DA RENOVAÇÃO, somar 1 mês à nova data até que fique MAIOR
            while (Helper::converterData($dtVencimentoRenovacao, 'Y-m-d') < $dt_limiterenovacao) {
                $dtVencimentoRenovacao->addMonth(1);
            }

            // Gerar as datas de vencimento para os lançamentos
            $arr_datas = $this->gerarDatasMensais($dtVencimentoRenovacao, 6);

            // Se a PRIMEIRA NOVA data for IGUAL a DATA LIMITE DE RENOVAÇÃO, então adicionar 1 dia à nova data
            if (!empty($arr_datas[0]) && Helper::converterData($arr_datas[0], 'Y-m-d') == $dt_limiterenovacao) {
                $arr_datas[0]->add(new \DateInterval("P1D"));
            }

            $id_usuariolancamento = null;
            $id_entidadelancamento = null;

            if ($primeiroLancamento instanceof \G2\Entity\VwVendaLancamento) {
                $id_usuariolancamento = $primeiroLancamento->getId_usuariolancamento();
                $id_entidadelancamento = $primeiroLancamento->getId_entidadelancamento();
            }

            $bl_quitado = 0;
            $dt_quitado = null;

            // Quando o valor for ZERO, gerar apenas uma parcela com 1 centavo e já PAGA
            if ((int)$params['nu_valornegociacao'] <= 0) {
                $arr_datas = array(array_shift($arr_datas));

                $params['nu_valorentrada'] = 0.01;
                $params['nu_valormensalidade'] = 0.01;

                $bl_quitado = 1;
                $dt_quitado = date('Y-m-d');
            }

            // Entrada e Parcelas
            foreach ($arr_datas as $i => $data) {
                $index = $i + 1;
                $bl_entrada = !$i;

                $arrayLancamentos[] = array(
                    'id_venda' => '',
                    'bl_entrada' => $bl_entrada,
                    'nu_valor' => str_replace('.', ',', money_format(
                        '%.2n',
                        $bl_entrada ? $params['nu_valorentrada'] : $params['nu_valormensalidade']
                    )),
                    'nu_parcelas' => $index,
                    'nu_ordem' => $index,
                    'bl_quitado' => $bl_quitado,
                    'dt_quitado' => $dt_quitado,
                    'bl_ativo' => 1,
                    'id_tipolancamento' => 1,
                    'id_usuariolancamento' => $id_usuariolancamento,
                    'id_usuariocadastro' => $venda->getId_usuariocadastro(),
                    'id_entidade' => $venda->getId_entidade(),
                    'id_entidadelancamento' => $id_entidadelancamento,
                    'id_meiopagamento' => $meioPagamento,
                    'dt_vencimento' => Helper::converterData($data)
                );
            }

            return $arrayLancamentos;
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * @description Retorna o dia de vencimento para renovações.
     * @author Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param \G2\Entity\VwVendaLancamento $vw_vendalancamento
     * @return int
     */
    public function getDiaVencimentoRenovacoes($vw_vendalancamento)
    {
        if ($vw_vendalancamento instanceof \G2\Entity\VwVendaLancamento) {
            if ($vw_vendalancamento->getDt_vencimento()) {
                return Helper::converterData($vw_vendalancamento->getDt_vencimento(), 'd');
            }
        }

        return 1;
    }

    /**
     * Enviar e-mail para renovação de matricula por alunos
     * @param array $params
     * @param int $id_matricula
     * @param int $mensagemPadrao
     * @return \Ead1_Mensageiro|\Ead1_IMensageiro
     */
    public function enviaEmailRenovacaoGraduacao(
        $params,
        $id_matricula,
        $mensagemPadrao = \G2\Constante\MensagemPadrao::EMAIL_RENOVACAO_MATRICULA
    )
    {
        try {
            $dados = array(
                'id_matricula' => $id_matricula,
                'id_entidade' => $params['id_entidade'],
                'id_mensagempadrao' => $mensagemPadrao,
                'id_usuariocadastro' => $params['id_usuario'],
                'id_usuario' => $params['id_usuario'],
                'st_mensagem' => 'Renovação automática de matrícula'
            );

            $mensagemNegocio = new \G2\Negocio\Mensagem();
            $mensageiro = $mensagemNegocio->gerarEmailMensagemTextoSitema($dados);

            if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Exception($mensageiro->getFirstMensagem());
            }

            return $mensageiro;
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * @param int $id_matricula
     * @param int $id_projetopedagogico
     * @return int
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function getPercentualAprovado($id_matricula, $id_projetopedagogico)
    {
        $cargaHorariaDisciplinasAprovadas = 0;
        $cargaHorariaProjetoPedagogico = 0;
        $percentualDisciplinasAprovadas = 0;

        if ($id_matricula) {
            $ngDisciplina = new \G2\Negocio\Disciplina();
            $disciplinas = $ngDisciplina->retornarDisciplinasByMatricula(array(
                'id_matricula' => $id_matricula,
                'id_evolucao' => \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO
            ));

            if (!empty($disciplinas)) {
                // Somar carga horária das disciplinas aprovadas
                foreach ($disciplinas as $disciplina) {
                    $cargaHorariaDisciplinasAprovadas += $disciplina['nu_cargahoraria'];
                }
            }
        }

        //pesquisa o dados do projeto pedagogico do aluno
        $projetoPedagogico = $this->find('\G2\Entity\ProjetoPedagogico', $id_projetopedagogico);
        if ($projetoPedagogico instanceof \G2\Entity\ProjetoPedagogico) {
            // Pegar carga horária do projeto pedagógico
            $cargaHorariaProjetoPedagogico = $projetoPedagogico->getNu_cargahoraria();
        }

        // Calcular percentual das disciplinas aprovadas
        if ($cargaHorariaDisciplinasAprovadas && $cargaHorariaProjetoPedagogico) {
            $percentualDisciplinasAprovadas = ($cargaHorariaDisciplinasAprovadas / $cargaHorariaProjetoPedagogico) * 100;
        }

//        // Buscar o semestre atual e somar 1 para definir o próximo semestre
//        $nu_semestre = $this->negocioMatricula->getSemestreAtual($id_matricula);

        return $percentualDisciplinasAprovadas;
    }

    /**
     * @param int $nu_obrigatorioalocacao
     * @param int $nu_semestre
     * @param mixed $nu_percentualaprovado
     * @param int $nu_disponivelapartirde
     * @return array
     */
    public function verificarDisciplinaObrigatoriaOuDisponivel($nu_obrigatorioalocacao, $nu_semestre, $nu_percentualaprovado, $nu_disponivelapartirde)
    {
        $bl_obrigatorio = 0;
        $bl_disponivel = 1;

        //verifica se o $nu_semestre é diferente de 0 para evitar warning division by 0
        $nu_semestre = !($nu_semestre) ? 1 : $nu_semestre;

        if ($nu_obrigatorioalocacao) {

            // verifica se o atributo $nu_semestre tem valor positivo, pois o valor 0 inviabiliza o trecho abaixo
            if (!empty($nu_semestre) && $nu_semestre > 0) {

                // Calculo para verificar a Obrigatoriedade da disciplina
                $percentualSemestreDisciplina = ($nu_obrigatorioalocacao / $nu_semestre) * 100;

                // Se for primeiro semestre...
                if ($nu_obrigatorioalocacao == 1) {
                    $bl_obrigatorio = $nu_percentualaprovado <= $percentualSemestreDisciplina;
                } else {
                    $bl_obrigatorio = $nu_percentualaprovado >= $percentualSemestreDisciplina;
                }
            }
        }

        if ($nu_disponivelapartirde) {

            // verifica se o atributo $nu_semestre tem valor positivo, , pois o valor 0 inviabiliza o trecho abaixo
            if (!empty($nu_semestre) && $nu_semestre > 0) {

                // Calculo para verificar a Disponibilidade da disciplina
                $percentualSemestreDisciplina = ($nu_disponivelapartirde / $nu_semestre) * 100;

                // Se ele for no primeiro semestre
                if ($nu_disponivelapartirde == 1) {
                    $bl_disponivel = $nu_percentualaprovado <= $percentualSemestreDisciplina;
                    // Se ele for no segundo semestre pra frente
                } else {
                    $bl_disponivel = $nu_percentualaprovado >= $percentualSemestreDisciplina;
                }
            }
        }

        return array('bl_obrigatorio' => !!$bl_obrigatorio, 'bl_disponivel' => !!$bl_disponivel); // !! Converte para boolean
    }

}

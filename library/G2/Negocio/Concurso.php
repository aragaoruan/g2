<?php

namespace G2\Negocio;

/**
 * Classe negócio para Concurso
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-20-01
 */
use \G2\Entity\Concurso as ConcursoEntity;

class Concurso extends Negocio
{
 
    private $repositoryName = 'G2\Entity\Concurso';

    /**
     *
     * @var \Ead1_Mensageiro
     */
    private $mensageiro;

    public function __construct()
    {
        parent::__construct();
        $this->mensageiro = new \Ead1_Mensageiro();
    }

    /**
     * Método para salvar concurso
     * @param array $data
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function salvarConcurso(array $data)
    {
        try {
            $entity = $this->verificaConcursoExiste($data); //Verifica se existe
            if (!$entity) {
                $entity = new ConcursoEntity(); //Instancia a entity
            }

            //verifica se o st_concurso veio preenchido para salvar
            if (!isset($data['st_concurso'])) {
                throw new \Zend_Exception("st_concurso é de preenchimento obrigatório!");
            }

            //verifica se dt_inicioinscricao é menor que dt_fiminscricao
            $verificaDatas = $this->verificaDtInscricao($data);
            if ($verificaDatas) {
                if ($verificaDatas->getType() == 'error') {
                    throw new \Zend_Exception($verificaDatas->getText());
                }
            }
            //Set os valores
            $entity = $this->setEntityValues($entity, $data);
            $return = $this->save($entity); //salva
            return $this->mensageiro->setMensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Verifica se concurso existe de acordo com os parametros passados
     * @param array $data
     * @return boolean
     */
    public function verificaConcursoExiste(array $data)
    {
        foreach ($data as $key => $value) {
            if (!empty($value)) {
                if (substr($key, 0, 3) == 'dt_') {
                    $data[$key] = $this->converteDataBanco($value);
                } else {
                    $data[$key] = $value;
                }
            } else {
                unset($data[$key]);
            }
        }
        if (isset($data['id_concurso']) && !empty($data['id_concurso'])) {
            $result = $this->find($this->repositoryName, $data['id_concurso']);
        } else {
            $result = $this->em->getRepository($this->repositoryName)->findOneBy($data);
        }
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Preenche atributos da entidade de acordo com o parametros passado
     * @param \G2\Entity\Concurso $entity
     * @param array $data
     * @return \G2\Entity\Concurso
     */
    private function setEntityValues(ConcursoEntity $entity, array $data)
    {
        //setDt_fiminscricao
        if (isset($data['dt_fiminscricao'])) {
            $entity->setDt_fiminscricao($this->converteDataBanco($data['dt_fiminscricao']));
        }
        //setDt_inicioinscricao
        if (isset($data['dt_inicioinscricao'])) {
            $entity->setDt_inicioinscricao($this->converteDataBanco($data['dt_inicioinscricao']));
        }
        //setDt_prova
        if (isset($data['dt_prova'])) {
            $entity->setDt_prova($this->converteDataBanco($data['dt_prova']));
        }
        //setSt_slug
        if (isset($data['st_slug'])) {
            $entity->setSt_slug($data['st_slug']);
        }
        //setSt_imagem
        if (isset($data['st_imagem'])) {
            $entity->setSt_slug($data['st_imagem']);
        }
        //setSt_orgao
        if (isset($data['st_orgao'])) {
            $entity->setSt_orgao($data['st_orgao']);
        }
        //setNu_vagas
        if (isset($data['nu_vagas'])) {
            $entity->setNu_vagas((int)$data['nu_vagas']);
        }
        //setSt_banca
        if (isset($data['st_banca'])) {
            $entity->setSt_banca($data['st_banca']);
        }
        //setSt_concurso
        if (isset($data['st_concurso'])) {
            $entity->setSt_concurso($data['st_concurso']);
        }
        //setId_entidade
        if (isset($data['id_entidade'])) {
            $entidadeNegocio = new Entidade();
            $entidade = $entidadeNegocio->getReference($data['id_entidade']);
            $entity->setId_entidade($entidade);
        }
        //setDt_cadastro
        if (isset($data['dt_cadastro'])) {
            $entity->setDt_cadastro($this->converteDataBanco($data['dt_cadastro']));
        } else {
            $entity->setDt_cadastro(new \DateTime());
        }
        return $entity; //return
    }

    /**
     * Verifica se dt_inicio é menor que dt_fim e/ou se o definiu dt_fim sem definir dt_inicio
     * @param array $data
     * @return mixed/boolean Retorna mensagem de erro ou true se dt validar
     * @throws \Zend_Exception
     */
    private function verificaDtInscricao(array $data)
    {
        try {
//verfica se as duas datas estao setadas
            if (isset($data['dt_inicioinscricao']) && isset($data['dt_fiminscricao'])) {
//compara as duas datas
                $valid = $this->comparaData($data['dt_inicioinscricao'], $data['dt_fiminscricao']);
//se valida envia retorna suceesso
                if ($valid) {
                    return $this->mensageiro->setMensageiro('Data Válida', \Ead1_IMensageiro::SUCESSO);
                } else { //se não válida gera um exception
                    throw new \Zend_Exception('dt_fiminscricao deve ser maior que dt_inicioinscricao');
                }
            } else { //se as duas datas não estiverem setadas
//verifica se a dt_fiminscricao esta setada
                if (isset($data['dt_fiminscricao'])) {
//se dt_fiminscricao estiver setada verifica se dt_inicioinscricao não esta setado e retorna um exception
                    if (!isset($data['dt_inicioinscricao'])) {
                        throw new \Zend_Exception('Não é permitido definir dt_fiminscricao sem definir dt_inicioinscricao');
                    }
                }
            }
        } catch (\Exception $e) {
            return $this->mensageiro->setMensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna array de objetos com resultado da busca
     * @param array $params
     * @return \G2\Entity\Concurso
     */
    public function retornaConcursoByParams(array $params = array())
    {
        return $this->findBy($this->repositoryName, $params);
    }

    /**
     * Salva o Vínculo entre produto e concurso
     * @param array $data
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro|Ambigous <Ead1_Mensageiro, Ead1_Mensageiro>
     */
    public function salvarConcursoProduto(array $data)
    {

        try {

            if (isset($data['id_concursoproduto'])) {
                $result = $this->find('G2\Entity\ConcursoProduto', $data['id_concursoproduto']);
            } else {
                $result = $this->em->getRepository('G2\Entity\ConcursoProduto')->findOneBy($data);
            }

            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            }

            $entity = new \G2\Entity\ConcursoProduto(); //Instancia a entity
            $entity->montarEntity($data);

//verifica se o st_concurso veio preenchido para salvar
            if (!$entity->getId_concurso()) {
                throw new \Zend_Exception("id_concurso é de preenchimento obrigatório!");
            }
            if (!$entity->getId_produto()) {
                throw new \Zend_Exception("id_produto é de preenchimento obrigatório!");
            }

//Set os valores
            $return = $this->save($entity); //salva
            return new \Ead1_Mensageiro($entity, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Retorna os registros da tb_concursoproduto e tb_concurso
     * @param integer $id_produto
     * @return \Ead1_Mensageiro
     */
    public function retornarConcursosProduto($id_produto)
    {
        //Temporario para avaliacao de erro de execucao
        $stream = new \Zend_Log_Writer_Stream(realpath(APPLICATION_PATH . '/../docs/logs') . '/ws.log');
        $logger = new \Zend_Log($stream);
        
        try {

            $query = $this->em->createQueryBuilder('pc')
                ->select('c, pc.id_produto')->from('\G2\Entity\ConcursoProduto', 'pc') 
                ->innerJoin('\G2\Entity\Concurso','c','pc.id_concurso= c.id_concurso') 
                ->where('pc.id_produto = :id_produto')
                ->setParameter('id_produto', $id_produto)->getQuery();

            $result = $query->getResult();
            
            //Temporario para avaliacao de erro de execucao
            $logger->log($query->getDql(), \Zend_Log::INFO);
            $logger->log('ID: (' . $id_produto . ')', \Zend_Log::INFO);
            
            if ($result) {
                return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
            } else {
                return new \Ead1_Mensageiro('Nenhum registro encontrado!', \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $e) {
            
            //Temporario para avaliacao de erro de execucao
            $logger->log($query->getDql(), \Zend_Log::INFO);
            $logger->log('ID: (' . $id_produto . ')', \Zend_Log::INFO);
            
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        } catch (\Doctrine\ORM\Query\QueryException $dex) {
            
            //Temporario para avaliacao de erro de execucao
            $logger->log($query->getDql(), \Zend_Log::INFO);
            $logger->log('ID: (' . $id_produto . ')', \Zend_Log::INFO);
            
            return new \Ead1_Mensageiro('Erro - ' . $dex->getMessage());
        }
    }

    /**
     * Salva Concurso Uf
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaConcursoUf(array $data)
    {
        try {
            $entity = $this->verificaVinculoConcursoUf($data);
            if (!$entity) {
                $entity = new \G2\Entity\ConcursoUf(); //Instancia a Entity
            }

            $concurso = $this->getReference($this->repositoryName, $data['id_concurso']); //Get reference do concurso
            $uf = $this->getReference('\G2\Entity\Uf', $data['sg_uf']); //get reference do UF
            //Seta os atributos
            $entity->setId_concurso($concurso)
                ->setSg_uf($uf);
            //salva
            $return = $this->save($entity);
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Verifica vinculo de Concurso com UF
     * @param array $data
     * @return mixed
     */
    private function verificaVinculoConcursoUf(array $data)
    {
        try {
            if (isset($data['id_concursouf'])) {
                $result = $this->find('\G2\Entity\ConcursoUf', $data['id_concursouf']);
            } else {
                $result = $this->em->getRepository('\G2\Entity\ConcursoUf')->findOneBy($data);
            }

            if (!$result) {
                return false;
            }
            return $result;
        } catch (\Exception $e) {
            return 'Erro - ' . $e->getMessage();
        }
    }

    /**
     * Salva vinculo de Concurso com Nivel de Ensino
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaConcursoNivelEnsino(array $data)
    {
        try {
            $entity = $this->verificaConcursoNivelEnsino($data);
            if (!$entity) {
                $entity = new \G2\Entity\ConcursoNivelEnsino (); //Instancia a entiy
            }
            $concurso = $this->getReference($this->repositoryName, $data['id_concurso']); //Get reference do concurso
            $nivelEnsino = $this->getReference('\G2\Entity\NivelEnsino', $data['id_nivelensino']);
            //Set atributos
            $entity->setId_concurso($concurso)
                ->setId_nivelensino($nivelEnsino);
            $return = $this->save($entity);
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Verifica vinculo de Concurso com Nível de Ensino
     * @param array $data
     * @return mixed
     */
    private function verificaConcursoNivelEnsino(array $data)
    {
        try {
            if (isset($data['id_concursonivelensino'])) {
                $result = $this->find('\G2\Entity\ConcursoNivelEnsino', $data['id_concursonivelensino']);
            } else {
                $result = $this->em->getRepository('\G2\Entity\ConcursoNivelEnsino')->findOneBy($data);
            }

            if (!$result) {
                return false;
            }
            return $result;
        } catch (\Exception $e) {
            return 'Erro - ' . $e->getMessage();
        }
    }

    /**
     * Salva Cargos vinculados ao concurso
     * @param array $data
     * @return \Ead1_Mensageiro
     */
    public function salvaCargoConcurso(array $data)
    {
        try {
            $bo = new \Ead1_BO(); //Instancia a Bo para usar algum método de lá
            $entity = $this->verificaCargoConcurso($data); //Verifica se existe um cargo concurso já cadastrado com os dados
            if (!$entity) {//Se não existir instancia a entity
                $entity = new \G2\Entity\CargoConcurso();
            }
            //Recupera o Objeto de concurso preenchido
            $concurso = $this->getReference($this->repositoryName, $data['id_concurso']); //Get reference do concurso
            //seta os atributos
            $entity->setSt_cargoconcurso($data['st_cargoconcurso'])
                ->setNu_salario(str_replace(",", ".", $bo->converteMoeda($data['nu_salario'])))//Formata o valor
                ->setId_concurso($concurso);
            $return = $this->save($entity); //salva
            return new \Ead1_Mensageiro($return, \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            return new \Ead1_Mensageiro('Erro - ' . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
    }

    /**
     * Verifica se existe algum registro cadastro de acordo com os parametros passado
     * @param array $data
     * @return mixed
     */
    private function verificaCargoConcurso(array $data)
    {
        try {
            if (isset($data['id_cargoconcurso'])) {
                $result = $this->find('\G2\Entity\CargoConcurso', $data['id_cargoconcurso']);
            } else {
                $result = $this->em->getRepository('\G2\Entity\CargoConcurso')->findOneBy($data);
            }
            if (!$result) {
                return false;
            }
            return $result;
        } catch (\Exception $e) {
            return 'Erro - ' . $e->getMessage();
        }
    }

    public function retornaConcurso($id)
    {
        return $this->find('\G2\Entity\Concurso', $id);
    }

    public function enviarDadosConcurso($array, $arquivo = null)
    {
        try {
            $this->beginTransaction();

            //------------------RECUPERAÇÃO DOS MODELOS-----------------

            $arrayConcurso = $array['dados_concurso'];
            $arrayProdutos = null;
            if (array_key_exists('produtos', $array)) {
                $arrayProdutos = $array['produtos'];
            }


            //----------------------------------------------------------


            $objetoConcurso = new \G2\Entity\Concurso();
            $ent = $this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade);

            if (array_key_exists('id_concurso', $arrayConcurso) && $arrayConcurso['id_concurso']) {
                $objetoConcurso = $this->find("\G2\Entity\Concurso", $arrayConcurso['id_concurso']);
            };
            $objetoConcurso->setId_entidade($ent);
            $objetoConcurso->setSt_concurso($arrayConcurso['st_concurso']);
            $objetoConcurso->setNu_vagas($arrayConcurso['nu_vagas']);
            $objetoConcurso->setSt_banca($arrayConcurso['st_banca']);
            $objetoConcurso->setId_Situacao($this->find('\G2\Entity\Situacao', $arrayConcurso['id_situacao']));
            $objetoConcurso->setSt_orgao($arrayConcurso['st_orgao']);
            $objetoConcurso->setDt_cadastro(new \DateTime());
            if (strlen($arrayConcurso['dt_prova']) > 0) {
                $objetoConcurso->setDt_prova($this->converteDataBanco($arrayConcurso['dt_prova']));
            } else {
                $objetoConcurso->setDt_prova(null);
            };
            if (strlen($arrayConcurso['dt_prova']) > 0) {
                $objetoConcurso->setDt_inicioinscricao($this->converteDataBanco($arrayConcurso['dt_inicioinscricao']));
            } else {
                $objetoConcurso->setDt_inicioinscricao(null);
            };
            if (strlen($arrayConcurso['dt_fiminscricao']) > 0) {
                $objetoConcurso->setDt_fiminscricao($this->converteDataBanco($arrayConcurso['dt_fiminscricao']));
            } else {
                $objetoConcurso->setDt_fiminscricao(null);
            };

            $result = $this->save($objetoConcurso);
            if ($arrayProdutos) {
                foreach ($arrayProdutos as $value) {
                    $objetoConcursoProduto = new \G2\Entity\ConcursoProduto();
                    $resultado = $this->findBy('\G2\Entity\ConcursoProduto',
                        array('id_produto' => $this->getReference('\G2\Entity\Produto', $value['id_produto']),
                            'id_concurso' => $this->getReference('\G2\Entity\Concurso', $result->getId_concurso())));
                    if (!$resultado) {
                        $objetoConcursoProduto->setId_concurso($this->getReference('\G2\Entity\Concurso', $result->getId_concurso()));
                        $objetoConcursoProduto->setId_produto($this->getReference('\G2\Entity\Produto', $value['id_produto']));
                        $this->save($objetoConcursoProduto);
                    }
                }
            }
            if ($arquivo) {
                $tipo = pathinfo($arquivo['arquivo']['name'], PATHINFO_EXTENSION);

                //Setando configuracoes para subir arquivo
                $zfile = new \Zend_File_Transfer_Adapter_Http();
                $zfile->addFilter('Rename', $result->getId_concurso() . '.' . $tipo);
                $zfile->addValidator('ExcludeExtension', false, 'php,exe');
                $zfile->setDestination('upload/concurso');
                $zfile->receive();
                $info = $zfile->getFileInfo();

                if ($zfile->isReceived()) {
                    //Buscando avaliacao que foi enviada
                    $imagemConcurso = $this->find('\G2\Entity\Concurso', $result->getId_concurso());

                    //inserindo informacao do arquivo na tabela upload
                    $upload = new \G2\Entity\Upload();
                    $upload->setSt_upload($info['arquivo']['name']);
                    $newEntityUp = $this->save($upload);

                    //Alterando tabela avaliacao aluno com informacao do novo arquivo
                    $imagemConcurso->setSt_imagem($newEntityUp->getSt_upload());
                    $this->save($imagemConcurso);
                }

            }
            $this->commit();

            return $result;
        } catch (Exception $e) {
            $this->rollback();
            throw new \Zend_Exception($e->getMessage());
        }
    }


}

<?php

namespace G2\Negocio;

/**
 * Class JiraIssues Classe de negocios para ações que envolvem o jira.
 * @package G2\Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class JiraIssues extends Negocio
{

    /**
     * @var string
     */
    protected $jiraUrl;
    /**
     * @var string
     */
    protected $jiraUser;
    /**
     * @var string
     */
    protected $jiraPwd;

    /**
     * JiraIssues constructor.
     * @param array $dados array(chave => valor)
     */
    public function __construct(array $dados = array())
    {
        parent::__construct();

        if ($dados) {
            if (array_key_exists('jiraUrl', $dados) && !empty($dados['jiraUrl']))
                $this->jiraUrl = $dados['jiraUrl'];

            if (array_key_exists('jiraUser', $dados) && !empty($dados['jiraUser']))
                $this->jiraUser = $dados['jiraUser'];

            if (array_key_exists('jiraPwd', $dados) && !empty($dados['jiraPwd']))
                $this->jiraPwd = $dados['jiraPwd'];

        }

    }

    /**
     * Método para receber os dados do robo, setar os parametros e direcionar para o
     * método correto de salvar ou editar issue.
     * @param array $dados
     * @return mixed|string
     */
    public function enviarIssue(array $dados)
    {
        try {

            //seta os atributos para conexão com o serviço
            $this->jiraUrl = $dados['jiraUrl'];
            $this->jiraUser = $dados['jiraUser'];
            $this->jiraPwd = $dados['jiraPwd'];
            $result = array();
            //verifica se é para editar issue ou salvar uma nova no jira
            if ($dados['issueReference']) {
                $result = (Array)json_decode($this->atualizarIssue($dados['issueReference'], $dados['issue']));

                if (is_array($result) && !isset($result['errors'])) {
                    $result = array('Issue atualizada com sucesso!');
                } else {
                    $result = $this->verificarErros($dados, $result);
                }

            } else {
                //salva uma nova issue no jira
                $result = $this->salvarIssue($dados['issue']);

                if (is_array($result) && !isset($result['errors'])) {
                    //vincula a issue com a ocorrencia no g2
                    $result['ocorrencia'] = $this->vinculaIssueJiraComOcorrencia(
                        $result['id'],
                        $dados['issue']['fields']['customfield_10400']
                    );
                } else {
                    $result = $this->verificarErros($dados, $result);
                }
            }

            return $result;

        } catch (\Exception $e) {
            //criar a exception
            return $e->getMessage();
        }

    }

    /**
     * Método que verifica os erros se houver erros relacionados as atribuições da issue
     * tenta executar novamente o método de salvar a issue no jira.
     * @param array $dados
     * @param mixed $issueResult
     * @return mixed|string
     */
    private function verificarErros($dados, $issueResult)
    {
        try {
            $errorByUser = false;
            if ($issueResult['errors']) {
                foreach ($issueResult['errors'] as $key => $error) {
                    switch ($key) {
                        case 'reporter':
                            $errorByUser = true;
                            $dados['issue']['fields']['reporter']['name'] = $this->jiraUser;
                            break;
                        case 'assignee':
                            $errorByUser = true;
                            $dados['issue']['fields']['assignee']['name'] = $this->jiraUser;
                            break;
                    }
                }
            }
            if ($errorByUser) {
                return $this->enviarIssue($dados);
            } else {
                throw new \Exception(json_encode($issueResult));
            }

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método para vincular a issue do jira com a ocorrencia
     * @param $idIssue
     * @param $idOcorrencia
     * @return string
     */
    private function vinculaIssueJiraComOcorrencia($idIssue, $idOcorrencia)
    {
        try {
            $ocorrencia = $this->find('\G2\Entity\Ocorrencia', $idOcorrencia);
            if (!$ocorrencia) {
                throw new \Exception('Ocorrencia não encontrada.');
            }

            $ocorrencia->setSt_codissue($idIssue);
            $result = $this->save($ocorrencia);

            if (!$result || !$result->getSt_codissue()) {
                throw new \Exception("Erro ao vincular Issue a ocorrencia.");
            }

            return "Issue vinculada com sucesso. ";


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método para salvar uma nova issue no jira
     * @param $data
     * @return array|string
     */
    public function salvarIssue($data)
    {
        try {

            $cUrl = curl_init($this->jiraUrl);

            curl_setopt_array($cUrl, array(
                CURLOPT_POST => true,
                CURLOPT_USERPWD => $this->jiraUser . ':' . $this->jiraPwd,
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
                CURLOPT_RETURNTRANSFER => true
            ));

            $response = curl_exec($cUrl);

            $ch_error = curl_error($cUrl);
            if ($ch_error) {
                throw new \Exception("cURL Error: " . $ch_error);
            }

            curl_close($cUrl);
            //pega a resposta e decodifica o json
            $decoded = json_decode($response);
            return (Array)$decoded;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método para consultar um issue pelo ID ou pelo KEY
     * @param $idOrKey
     * @return mixed|string
     */
    public function consultarIssueById($idOrKey)
    {
        try {

            $url = $this->jiraUrl . "/" . $idOrKey;
            $cUrl = curl_init($url);

            curl_setopt_array($cUrl, array(
                CURLOPT_USERPWD => $this->jiraUser . ':' . $this->jiraPwd,
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
                CURLOPT_RETURNTRANSFER => true
            ));

            $response = curl_exec($cUrl);

            $ch_error = curl_error($cUrl);
            if ($ch_error) {
                throw new \Exception("cURL Error: " . $ch_error);
            }

            curl_close($cUrl);
            //pega a resposta e decodifica o json
            $decoded = json_decode($response);

            return $decoded;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Método para atualizar os dados da issue no jira
     * @param $idIssue
     * @param array $data
     * @return array|string
     */
    private function atualizarIssue($idIssue, array $data)
    {
        try {
            //busca a issue pelo id
            $issue = $this->consultarIssueById($idIssue);

            if (isset($issue->errors) || isset($issue->errorMessages)) {
                throw new  \Exception(json_encode($issue));
            }

            //pega a key da issue
            $issueKey = $issue->key;

            $url = $this->jiraUrl . '/' . $issueKey;
            $cUrl = curl_init($url);

            curl_setopt_array($cUrl, array(
                CURLOPT_CUSTOMREQUEST => "PUT",
                CURLOPT_USERPWD => $this->jiraUser . ':' . $this->jiraPwd,
                CURLOPT_POSTFIELDS => json_encode($data),
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ),
                CURLOPT_RETURNTRANSFER => true
            ));

            $response = curl_exec($cUrl);

            $ch_error = curl_error($cUrl);
            if ($ch_error) {
                throw new \Exception("cURL Error: " . $ch_error);
            }

            curl_close($cUrl);
            //pega a resposta e decodifica o json
//            $decoded = json_decode($response);

            return $response;


        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }


    /**
     * Método para salvar o comentário no jira.
     * @param array $dados
     * @return array|string
     */
    public function enviarInteracao(array $dados)
    {
        try {

            //seta os atributos para conexão com o serviço
            $this->jiraUrl = $dados['jiraUrl'];
            $this->jiraUser = $dados['jiraUser'];
            $this->jiraPwd = $dados['jiraPwd'];


            //busca a issue pelo id
            $issue = $this->consultarIssueById($dados['issueReference']);

            //pega a key da issue
            $issueKey = $issue->key;

            //monta a url
            $url = $this->jiraUrl . '/' . $issueKey . '/comment';

            $cUrl = curl_init($url);

            curl_setopt_array($cUrl, array(
                CURLOPT_POST => true,
                CURLOPT_USERPWD => $this->jiraUser . ':' . $this->jiraPwd,
                CURLOPT_POSTFIELDS => json_encode($dados['issueComment']),
                CURLOPT_HTTPHEADER => array('Content-type: application/json'),
                CURLOPT_RETURNTRANSFER => true
            ));

            $response = curl_exec($cUrl);

            $ch_error = curl_error($cUrl);
            if ($ch_error) {
                throw new \Exception("cURL Error: " . $ch_error);
            }

            curl_close($cUrl);
            //pega a resposta e decodifica o json
            $decoded = json_decode($response);
            return (Array)$decoded;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
<?php

namespace G2\Negocio;

/**
 * Negocio para funcionalidade professores
 *
 * @author denise.xavier@unyleya.com.br
 */
class Professores extends Negocio
{

    private $repositoryVwProfessorAulasMinistradas = '\G2\Entity\VwProfessorAulasMinistradas';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna Pesquisa de professores para adicionar em lote (array VwProfessorSalaDeAulaLote)
     *
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaPesquisaProfessores(array $params = [])
    {
        try {
            $params['id_entidade'] = $this->sessao->id_entidade;
            return $this->em->getRepository('\G2\Entity\VwProfessorSalaDeAulaLote')->pesquisaProfessores($params);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao consultar professores. ' . $e->getMessage());
        }
    }

    /**
     * Adiciona professore  a array de salas de aula
     *
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function addProfessores(array $params)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            //\Zend_Debug::dump($params);die;
            if (!array_key_exists('id_usuario', $params) || !array_key_exists('bl_titular', $params)) {
                throw new \Exception('Os parametros id_usuario e bl_tivo são obrigatórios e não foram passados');
            }
            $aux = explode('#', $params['id_usuario']);
            $id_usuario = $aux[0];
            $id_perfil = $aux[1];

            if (array_key_exists('salas', $params) && is_array($params['salas'])) {
                foreach ($params['salas'] as $id_saladeaula) {
                    if ((int)$params['bl_titular']) {
                        $this->desativaTitulares($id_saladeaula, $id_perfil);
                    }
                    $this->vinculaProfessorSala($id_saladeaula, $id_usuario, $id_perfil, $params['bl_titular']);
                }
            }

            $mensageiro->setMensageiro('Professores salvos com sucesso', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $ex) {
            $mensageiro->setMensageiro('Erro ao salvar professores: ' . $ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Se o novo professor for adicionado como titular, todos os outros são removidos
     *
     * @param $id_saladeaula
     * @param $id_perfil
     * @return mixed
     * @throws \Zend_Exception
     */
    public function desativaTitulares($id_saladeaula, $id_perfil)
    {
        try {
            $id_entidade = $this->sessao->id_entidade;
            return $this->em->getRepository('\G2\Entity\VwProfessorSalaDeAulaLote')->desativaTitulares($id_saladeaula, $id_perfil, $id_entidade);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao desativar titulares. ' . $e->getMessage());
        }

    }

    /**
     * Vincula professor a sala de aula
     *
     * @param $id_saladeaula
     * @param $id_usuario
     * @param $id_perfil
     * @param $bl_titular
     * @return \Ead1_Mensageiro
     */
    public function vinculaProfessorSala($id_saladeaula, $id_usuario, $id_perfil, $bl_titular)
    {
        try {
            $usuarioPerfil = new \UsuarioPerfilEntidadeReferenciaTO();
            $usuarioPerfil->setBl_ativo(1);
            $usuarioPerfil->setId_entidade($this->sessao->id_entidade);
            $usuarioPerfil->setBl_titular($bl_titular);
            $usuarioPerfil->setId_saladeaula((int)$id_saladeaula);
            $usuarioPerfil->setId_perfil((int)$id_perfil);
            $usuarioPerfil->setId_usuario((int)$id_usuario);


            $professorSala = new \SalaDeAulaProfessorTO();
            $professorSala->setBl_titular($bl_titular);

            $bo = new \SalaDeAulaBO();
            $retorno = $bo->cadastrarSalaDeAulaProfessor($usuarioPerfil, $professorSala);

        } catch (\Exception $e) {
            $retorno = new \Ead1_Mensageiro('Erro ao vincular professor: ' . $e->getMessage());
        }
        return $retorno;
    }


    /**
     * Remove professores da sala de aula
     * Marca para ser retirado do moodle - processo chamado via robo
     *
     * @param array $params
     * @return \Ead1_Mensageiro
     */
    public function removeProfessores(array $params)
    {
        $mensageiro = new \Ead1_Mensageiro();
        try {
            if (!array_key_exists('perfilRef', $params) || !is_array($params['perfilRef']) || !count($params['perfilRef'])) {
                throw new \Exception('Selecione salas para retirar o perfil dos professores.');
            }
            foreach ($params['perfilRef'] as $id_perfilreferencia) {
                $to = new \UsuarioPerfilEntidadeReferenciaTO();
                $to->setId_perfilReferencia((int)$id_perfilreferencia);
                $bo = new \ProjetoPedagogicoBO();
                $return = $bo->excluirCoordenador($to);
            }

            $mensageiro->setMensageiro('Professores removidos com sucesso', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $ex) {
            $mensageiro->setMensageiro('Erro ao remover professores: ' . $ex->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * Retorna um array separado como chave o mês de referência
     *
     * @param $params
     * @return array
     */
    public function retornaProfessorAulasMinistradas($params)
    {
        //Limpeza de parametros
        if ($params['id_unidade'] == 0) {
            unset($params['id_unidade']);
        }
        if ($params['id_professor'] == 0) {
            unset($params['id_professor']);
        } else {
            $params['id_usuario'] = $params['id_professor'];
            unset($params['id_professor']);
        }
        if ($params['id_areaconhecimento'] == 0) {
            unset($params['id_areaconhecimento']);
        }
        if (isset($params['dt_mes_ano']) && !empty($params['dt_mes_ano'])) {
            $dt = explode('/', $params['dt_mes_ano']);
            $params['nu_mes'] = $dt[0];
            $params['nu_ano'] = $dt[1];
        }
        unset($params['dt_mes_ano']);

        $returnProfessores = $this->findBy($this->repositoryVwProfessorAulasMinistradas, $params, array('dt_diasemana' => 'asc'));

        $entidadeNegocio = new Entidade();

        //busca os dados padrões
        $entidade = $entidadeNegocio->findEntidade($this->sessao->id_entidade, true);

        $aulasMinistradas = array();
        foreach ($returnProfessores as $key => $value) {
            $keyAreaConhecimento = $value->getId_areaconhecimento();
            $keyMes = $value->getNu_mes();
            $keyProfessor = $value->getId_usuario();
            $keyEntidade = $value->getId_unidade();

            if (!isset($aulasMinistradas[$keyEntidade][$keyAreaConhecimento])) {
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento] = array(
                    'mes' => $keyMes,
                    'ano' => $value->getNu_ano(),
                    'st_entidade' => $value->getSt_nomeentidade(),
                    'id_areaconhecimento' => $keyAreaConhecimento,
                    'st_areaconhecimento' => $value->getSt_areaconhecimento(),
                    'st_urlimglogo' => $entidade['st_urlimglogo'],
                    'TotalGeralAulas' => 0
                );

                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'][$keyProfessor] = array(
                    "id_professor" => $keyProfessor,
                    "st_nomecompleto" => $value->getSt_nomecompleto(),
                    "id_usuario" => $value->getId_usuario(),
                    'totalAulas' => 0
                );
            }

            if (!array_key_exists($key, $aulasMinistradas)) {
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['mes'] = $keyMes;
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['ano'] = $value->getNu_ano();
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['st_entidade'] = $value->getSt_nomeentidade();
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['id_areaconhecimento'] = $keyAreaConhecimento;
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['st_areaconhecimento'] = $value->getSt_areaconhecimento();
            }

            if (!array_key_exists($keyProfessor, $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'])) {
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'][$keyProfessor] = array(
                    "id_professor" => $keyProfessor,
                    "st_nomecompleto" => $value->getSt_nomecompleto(),
                    "id_usuario" => $value->getId_usuario(),
                    'totalAulas' => 0
                );
            }
        }

        //chama o metódo para motagem da grid de folha de pagamento
        $aulasMinistradas = $this->montaGridFolhaPagamento($aulasMinistradas, $returnProfessores);

        sort($aulasMinistradas);

        return $aulasMinistradas;
    }

    /**
     * @description Remove do Moodle, o papel de Professor em salas já encerradas, sem alterar a relação Sala/Professor no banco do G2
     * @param array $params
     * @return array
     */
    public function removerProfessoresSalasEncerradasMoodle($params = array())
    {

        $success = array();
        $error = array();
        try {
            /**
             * @var \G2\Entity\VwProfessoresSalasEncerradas[] $results
             */
            $results = $this->findBy(\G2\Entity\VwProfessoresSalasEncerradas::class,
                $params,
                array(
                    'id_perfilreferenciaintegracao' => 'ASC'
                ), 200);

            if (is_array($results) && $results) {
                foreach ($results as $result) {

                    $array = $this->toArrayEntity($result);
                    $moodle = new \MoodleAlocacaoWebServices(null, $result->getid_entidadeintegracao());

                    // Suspender o papel de professor (Inativa, mas mantém o papel)
                    $response = $moodle->cadastrarPapel(
                        $result->getId_usuario(),
                        $result->getId_saladeaula(),
                        $moodle::ROLEID_PROFESSOR,
                        $result->getId_entidade(),
                        true,
                        'alteraAlunosSalaPeriodica');

                    /**
                     * @var \G2\Entity\PerfilReferenciaIntegracao $en_perfilreferenciaintegracao
                     */
                    $en_perfilreferenciaintegracao = $this
                        ->find(\G2\Entity\PerfilReferenciaIntegracao::class, $result->getId_perfilreferenciaintegracao());

                    $en_perfilreferenciaintegracao->setDt_tentativaencerramento(new \DateTime());

                    if ($response->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                        $array['message'] = $response->getText();
                        $error[] = $array;
                    } else {
                        $en_perfilreferenciaintegracao->setBl_encerrado(true);
                        $success[] = $array;
                    }

                    if (!$this->save($en_perfilreferenciaintegracao)) {
                        $array['message'] = "Não foi possível salvar a data de tentativa de encerramento";
                        $error[] = $array;
                    }
                }
            }
        } catch (\Exception $e) {
            $array['message'] = $e->getMessage();
            $error[] = $array;
        }

        return array(
            'results' => $results,
            'error' => $error,
            'success' => $success
        );
    }

    /**
     * Método que monta a grid
     * Este método verifica quantas aulas esse professor, interando dentro do array do professor
     *
     * @param $aulasMinistradas
     * @param $returnProfessores
     * @return mixed
     */
    public
    function montaGridFolhaPagamento($aulasMinistradas, $returnProfessores)
    {

        /*
         * Monta a grade horaria geral
         * Pega todas as grades de insere dentro de todas as chaves
         */
        foreach ($returnProfessores as $key => $itensObj) {
            $keyMes = $itensObj->getNu_mes();
            $keyGrade = $itensObj->getId_gradehoraria();
            $keyEntidade = $itensObj->getId_unidade();
            $keyPeriodo = $itensObj->getDt_periodo();
            $keyAreaConhecimento = $itensObj->getId_areaconhecimento();

            if (isset($keyPeriodo) && !empty($keyPeriodo)) {
                $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['periodo'][$keyPeriodo] = array(
                    'dt_periodo' => $itensObj->getDt_periodo(),
                    'totalGrade' => 0
                );
            }
        }

        /*
         * Método de interação de aulas ministradas em cada professor, caso tenha tido aula nessa grade
         */
        foreach ($returnProfessores as $key => $itensObj) {

            $keyMes = $itensObj->getNu_mes();
            $keyProfessor = $itensObj->getId_usuario();
            $keyGrade = $itensObj->getId_gradehoraria();
            $keyEntidade = $itensObj->getId_unidade();
            $keyPeriodo = $itensObj->getDt_periodo();
            $keyAreaConhecimento = $itensObj->getId_areaconhecimento();


            if (isset($keyGrade) && !empty($keyGrade)) {
                /*
                 * laço para verificar em todas as grades se houveram aulas caso tenha intera o valor ou cria um
                 * item com valor 0
                 */
                foreach ($aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['periodo'] as $gradehoraria) {
                    $k = $gradehoraria['dt_periodo'];
                    if ($keyPeriodo == $k) {
                        //aqui intera a quantidade de aulas que a turma teve
                        $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'][$keyProfessor]['periodo'][$k][] = 1;
                        $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'][$keyProfessor]['totalAulas'] += 1;
                        $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['periodo'][$keyPeriodo]['totalGrade'] += 1;
                        $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['TotalGeralAulas'] += 1;
                    } else {
                        /*
                         * insere valor 0 para que o array vá com a quantidade de grades mesmo que não tenha tido aula,
                         * assim podemos montar as td vazias para completar a grid
                         */
                        $aulasMinistradas[$keyEntidade][$keyAreaConhecimento]['professores'][$keyProfessor]['periodo'][$k][] = 0;
                    }
                }
            }
        }
        return $aulasMinistradas;
    }

    /**
     * Função responsável por gerar um XLS a partir dos dados recebidos pelo filtro de pesquisa de Professores
     * em gestão de salas
     *
     * @author Ilson Nóbrega <ilson.nobrega@unyleya.com.br>
     * @param array $data
     * @throws \Zend_Exception
     */
    public function gerarXlsPesquisaProfessores(array $data)
    {
        //Dados de configuração do cabeçalho do XLS
        $cabecalho = array(
            'st_saladeaula' => 'Sala de Aula',
            'st_disciplina' => 'Disciplina',
            'st_nomecompleto' => 'Professor',
            'st_titular' => 'Titular',
            'st_dtabertura' => 'Abertura',
            'st_dtencerramento' => 'Encerramento',
        );

        //Tranforma o resultado obtido em array e trata as datas para o melhor funcionamento do gerador de relatório
        $results = array();
        foreach ($data as $entity) {
            $entity = $this->toArrayEntity($entity);
            $entity['dt_abertura'] = $entity['dt_abertura']['date'];
            $entity['dt_encerramento'] = $entity['dt_encerramento']['date'];
            $results[] = $entity;
        }

        //Gera o XLS
        $ng_relatorio = new \G2\Negocio\Relatorio();
        $ng_relatorio->export($results, $cabecalho, 'xls', 'pesquisa_professor_relatorio');
    }
}

<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Contas e relacionados
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-07-30
 */
class Conta extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados dos tipos de conta
     * @return array $return
     */
    public function retornaTipoContas()
    {
        try {
            $return = $this->findAll('G2\Entity\TipoConta');
            $resultReal = array();
            foreach ($return as $r) {
                $resultReal[] = array(
                    'id_tipodeconta' => $r->getId_tipodeconta(),
                    'st_tipodeconta' => $r->getSt_tipodeconta(),
                );
            }
            return $resultReal;
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    public function retornaContaEntidade($id_entidade)
    {
        try {
            $return = $this->findBy('G2\Entity\ContaEntidade', array('id_entidade' => $id_entidade));
            $resultReal = array();
            foreach ($return as $r) {
                $resultReal[] = array(
                    'id_contaentidade' => $r->getId_contaentidade(),
                    'st_conta' => $r->getSt_conta(),
                    'st_digitoconta' => $r->getSt_digitoconta(),
                    'st_agencia' => $r->getSt_agencia(),
                    'st_digitoagencia' => $r->getSt_digitoagencia(),
                    'st_banco' => $r->getSt_banco()->getSt_nomebanco(),
                    'st_tipodeconta' => $r->getId_tipodeconta()->getSt_tipodeconta()
                );
            }
            return $resultReal;
        } catch (\Zend_Exception $e) {
            return $e;
        }
    }

    public function retornaVwContaEntidadeFinanceiro($id_entidade)
    {
        try {
            $return = $this->findOneBy('G2\Entity\VwContaEntidadeFinanceiro', array('id_entidade' => $id_entidade));

            if ($return)
                return $this->toArrayEntity($return);

        } catch (\Zend_Exception $e) {
            return $e;
        }
    }
}

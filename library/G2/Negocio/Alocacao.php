<?php

namespace G2\Negocio;

use Doctrine\Entity;
use G2\Constante\PerfilPedagogico;
use G2\Constante\TipoEnvio;
use G2\Entity\DisciplinaSalaDeAula;
use G2\Entity\MatriculaDisciplina;
use G2\Entity\TipoDestinatario;
use G2\Entity\VwPesquisarPessoa;
use G2\Entity\VwSalaDisciplinaAlocacao;
use G2\Entity\VwUsuarioPerfilEntidadeReferencia;


use G2\Entity\VwAlunosSala;
use G2\Events\SituacaoTccChanged;
use G2\G2Entity;
use G2\Validator\SituacaoTccValidator;
use MensagemPadraoTO;

/**
 * Classe de negócio para Alocar Alunos
 * @author Debora Castro <debora.castro@unyleya.com.br>;
 */
class Alocacao extends Negocio
{

    public $matriculaDisciplina;

    private $mensagensTcc = [
        \G2\Constante\Situacao::TB_ALOCACAO_EM_ANALISE => MensagemPadraoTO::RECIBO_TCC_ALUNO,
        \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO => MensagemPadraoTO::DEVOLUCAO_TCC_ALUNO,
        \G2\Constante\Situacao::TB_ALOCACAO_AVALIADO_PELO_ORIENTADOR => MensagemPadraoTO::AVALIACAO_TCC_ALUNO,
        \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR => MensagemPadraoTO::DEVOLUCAO_TCC_TUTOR,
    ];

    private $mensagensTccPortal = [
        \G2\Constante\Situacao::TB_ALOCACAO_EM_ANALISE => [
            'st_mensagem' => 'TCC em Análise',
            'st_texto' => 'O seu orientador está analisando o seu TCC.',
        ],
        \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_ALUNO => [
            'st_mensagem' => 'TCC Devolvido ao Aluno',
            'st_texto' => 'O TCC que você enviou foi devolvido para reformulações.',
        ],
        \G2\Constante\Situacao::TB_ALOCACAO_AVALIADO_PELO_ORIENTADOR => [
            'st_mensagem' => 'TCC Entregue',
            'st_texto' => 'O seu Trabalho de conclusão de curso foi recebido pelo seu orientador.',
        ],
        \G2\Constante\Situacao::TB_ALOCACAO_DEVOLVIDO_AO_TUTOR => [
            'st_mensagem' => 'TCC Devolvido ao Tutor',
            'st_texto' => 'O TCC foi devolvido para seu Tutor.',
        ],
    ];

    public function __construct()
    {
        parent::__construct();

        $this->matriculaDisciplina = new MatriculaDisciplina();
    }

    /**
     * Retorna uma unica alocacao filtrada pelo atributo chave id_alocacao.
     * @return array
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function findAlocacao($id_alocacao)
    {
        return $this->find('\G2\Entity\Alocacao', $id_alocacao);
    }

    /**
     * Retorna uma ou mais alocacoes filtradas por qualquer parametro da
     * tabela alocacao.
     *
     * @param type $params
     * @return array
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function findAllBy(array $params = NULL)
    {
        return $this->findBy('G2\Entity\Alocacao', $params);
    }

    public function findByAlocacao($params)
    {
        $params['bl_ativo'] = 1;
        $repositoryName = 'G2\Entity\Alocacao';
        return $this->findBy($repositoryName, $params);
    }

    public function findByModulos($id_projeto)
    {
        try {
            $params['id_projetopedagogico'] = $id_projeto;
            $params['bl_ativo'] = 1;
            $repositoryName = 'G2\Entity\Modulo';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $params
     * @param bool $bl_encerradas
     * @return Object|string
     */
    public function findByVwSalaDisciplinaAlocacao($params, $bl_encerradas = false)
    {
        try {
            $repositoryName = 'G2\Entity\VwSalaDisciplinaAlocacaoEncerradas';
            if (!$bl_encerradas) {
                $repositoryName = 'G2\Entity\VwSalaDisciplinaAlocacao';
            }
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //VwModuloDisciplinaProjetoTrilha
    public function findByVwModuloDisciplinaProjetoTrilha($params)
    {
        try {
            $repositoryName = 'G2\Entity\VwModuloDisciplinaProjetoTrilha';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function findByVwAlocacaoAlunoSala($params)
    {
        try {
            $repositoryName = 'G2\Entity\VwAlocacaoAlunoSala';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function salvarAlocacao($data, $bl_naoalocar = false)
    {
        $to = new \AlocacaoTO();
        $to->setId_saladeaula($data['id_saladeaula']);
        $to->setId_matriculadisciplina($data['id_matriculadisciplina']);
        $to->setId_categoriasala($data['id_categoriasala']);
        $to->setDt_inicio($data['dt_inicio']);

        $bo = new \SalaDeAulaBO();
        return $bo->salvarAlocacaoAluno($to, $bl_naoalocar);
    }

    /**
     * Valida alocação de aluno
     * Verifica alocacao de TCC
     * @param $data
     * @return \Ead1_Mensageiro
     */
    public function validarAlocacao($data)
    {

        //validção ok. falta agora alterar o mensageiro pra concatenar caso necessário

        if (array_key_exists('id_saladeaula', $data) && ($data['id_saladeaula'] != '')) {
            $bl_maximoalunos = $this->validarNumeroMaximoAluno($data['id_saladeaula']);

            if (!$bl_maximoalunos) {
                $saladeaula = $this->find('G2\Entity\SalaDeAula', $data['id_saladeaula']);

                $mensageiro = new \Ead1_Mensageiro('<label>Caro usuário, foram encontradas pendências para alocação do aluno nessa sala de aula:</label>
                                                    <p class="text-error">* A sala de aula ' . $saladeaula->getSt_saladeaula() . ' já possui o número máximo de alunos: ' . $saladeaula->getNu_maxalunos() . ' </p>', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }

        }

        if ($data['id_tipodisciplina'] == 2) {
            $temTcc = $this->validarAlocacaoTcc($data);
            if (!empty($temTcc)) {
                $mensageiro = new \Ead1_Mensageiro('<label>Caro usuário, foram encontradas pendências para realocação do aluno:</label><p class="text-error">* TCC já foi enviado.</p>', \Ead1_IMensageiro::AVISO);
                return $mensageiro;
            }
        }
        $mensageiro = new \Ead1_Mensageiro(((int)$data['id_categoriasala'] == 1) ? 'Tem certeza de que deseja alocar esse aluno na sala normal?' : 'Tem certeza de que deseja alocar esse aluno na sala de prr?', \Ead1_IMensageiro::AVISO);
        return $mensageiro;

    }

    /**
     * Valida a alocacao de TCC
     * Verifica se o aluno já tem alocacao, e se ele tiver verifica se já enviou o TCC.
     * Caso o aluno não tenho nenhuma alocação com essa id_matriculadisciplina, retorna vazio
     * @param $data
     * @return null | string
     * @throws \Zend_Exception
     */
    public function validarAlocacaoTcc($data)
    {

        $to = new \MatriculaDisciplinaTO();
        $to->setId_matriculadisciplina((int)$data['id_matriculadisciplina']);
        $to->fetch(true, true, true);

        $toAlocacao = new \AlocacaoTO();
        $toAlocacao->setId_matriculadisciplina($to->getId_matriculadisciplina());
        $toAlocacao->setId_categoriasala((int)$data['id_categoriasala']);
        $toAlocacao->setBl_ativo(1);
        $toAlocacao->fetch(false, true, true);

        if ($toAlocacao->getId_saladeaula()) {
            $toAvaliacaoRef = new \AvaliacaoConjuntoReferenciaTO();
            $toAvaliacaoRef->setId_saladeaula($toAlocacao->getId_saladeaula());
            $toAvaliacaoRef->fetch(false, true, true);


            $toAvaliacaoAluno = new \AvaliacaoAlunoTO();
            $toAvaliacaoAluno->setId_matricula($to->getId_matricula());
            $toAvaliacaoAluno->setId_avaliacaoconjuntoreferencia($toAvaliacaoRef->getId_avaliacaoconjuntoreferencia());
            $toAvaliacaoAluno->setId_situacao(86);
            $toAvaliacaoAluno->fetch(false, true, true);

            $tcc_titulo = $toAvaliacaoAluno->getSt_tituloavaliacao();
            return $tcc_titulo;
        } else {
            return null;
        }


    }

    public function sincronizarAutoAlocar($data)
    {

        $bo = new \SalaDeAulaBO();
        $to = new \MatriculaTO();

        $to->setId_matricula($data['id_matricula']);
        $to->setId_projetopedagogico($data['id_projetopedagogico']);
        $retorno = $bo->sincronizarAutoAlocarPorAluno($to);
        return $retorno;
    }

    /**
     * @param $data
     * @return \Ead1_Mensageiro|null
     */
    public function reintegrarAlocacao($data)
    {
        $bo = new \SalaDeAulaBO();
        $retorno = null;
        for ($i = 0; $i < count($data['id_saladeaula']); $i++) {
            $to = new \AlocacaoTO();

            $to->setId_saladeaula(($data['id_saladeaula']));
            $to->setId_matriculadisciplina(($data['id_matriculadisciplina']));
            $to->setId_alocacao(($data['id_alocacao']));
            $retorno = $bo->reintegrarAlocacaoAluno($to);
        }
        return $retorno;
    }

    /**
     * @param array $data
     * @param $bl_trancamento
     * @return \Ead1_Mensageiro
     */
    public function desalocarAluno($data, $bl_trancamento = false)
    {
        if (empty($data['id_alocacao'])) {
            return new \Ead1_Mensageiro('Não foi encontrada uma alocação válida nesta sala!', \Ead1_IMensageiro::AVISO);
        }

        try {
            $en_alocacao = $this->find('\G2\Entity\Alocacao', $data['id_alocacao']);

            if ($en_alocacao instanceof \G2\Entity\Alocacao) {
                $id_alocacao = $en_alocacao->getId_alocacao();
                $id_saladeaula = $en_alocacao->getId_saladeaula()->getId_saladeaula();
                $id_sistema = null;

                /** Buscar alocações duplicadas**/
                $paramsAlocacao = array(
                    'id_matriculadisciplina' => $en_alocacao->getId_matriculadisciplina()->getId_matriculadisciplina(),
                    'bl_ativo' => true,
                    'id_categoriasala' => $en_alocacao->getId_categoriasala()
                );

                /** @var \G2\Entity\Alocacao[] $alocacoes */
                $alocacoes = $this->findBy('\G2\Entity\Alocacao', $paramsAlocacao);

                // Desativar todas as alocações encontradas
                foreach ($alocacoes as $alocacao) {
                    $alocacao->setBl_ativo(false);

                    //se for trancamento, recebe os parametros que informam o motivo da desalocacao pelo trancamento
                    if ($bl_trancamento) {
                        $alocacao->setBl_trancamento(true);
                        $alocacao->setDt_trancamento(new \DateTime);
                        $alocacao->setId_usuariotrancamento($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
                    }
                    $this->save($alocacao);

                    // Alterar Evolução da Matricula Disciplina para não alocado e Situação para Pendente.
                    /** @var \G2\Entity\MatriculaDisciplina $matriculadisciplina */
                    $matriculadisciplina = $this->find('\G2\Entity\MatriculaDisciplina', $alocacao->getId_matriculadisciplina()->getId_matriculadisciplina());
                    $matriculadisciplina->setId_evolucao($bl_trancamento ? \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_TRANCADA : \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_NAO_ALOCADO);
                    $matriculadisciplina->setId_situacao(\G2\Constante\Situacao::TB_MATRICULADISCIPLINA_PENDENTE);
                    $this->save($matriculadisciplina);
                }

                // Alterar Evolução da Matricula Disciplina para Não Cursado caso seja desalocado
                $salaintegracao = $this->findOneBy('\G2\Entity\SalaDeAulaIntegracao', array(
                    'id_saladeaula' => $id_saladeaula
                ));

                $id_sistema = null;
                // checa se a variável $salaintegracao não está nula E é um objeto válido
                if ($salaintegracao && $salaintegracao instanceof \G2\Entity\SalaDeAulaIntegracao) {
                    // caso passe no teste, chama a função getId_sistema
                    $id_sistema = $salaintegracao->getId_sistema();
                    if (!$id_sistema) {
                        throw new \Zend_Exception("ID do sistema inexistente!");
                    }
                }

                switch ($id_sistema) {
                    case \G2\Constante\Sistema::MOODLE:
                        /** @var \G2\Entity\Matricula $en_matricula */
                        $en_matricula = $this->find('\G2\Entity\Matricula',
                            $en_alocacao->getId_matriculadisciplina()->getId_matricula());

                        $vwAlocacaoTO = new \VwAlocacaoTO();
                        $vwAlocacaoTO->setId_alocacao($id_alocacao);
                        $vwAlocacaoTO->fetch(null, true, true);

                        $id_usuario = $vwAlocacaoTO->getId_usuario();

                        if (!$id_usuario) {
                            throw new \Zend_Exception("Aluno não alocado!");
                        }

                        // Suspender aluno de TODAS as salas que ele está alocado desde que a sala não esteja compartilhada com outra matrícula
                        $ws_moodle = new \MoodleAlocacaoWebServices(
                            $en_alocacao->getId_saladeaula()->getId_entidade(),
                            $en_alocacao->getId_saladeaula()->getid_entidadeintegracao()
                        );

                        $ws_moodle->suspenderAlunoSalas($en_matricula, array(
                            'id_saladeaula' => $id_saladeaula
                        ));

                        break;
                    case \G2\Constante\Sistema::BLACKBOARD:
                        $bo = new \IntegracaoBO();
                        $bo->inativarUsuarioCurso($this->sessao->id_entidade, $id_sistema, $id_alocacao);
                        break;
                    default:
                        break;
                }
            }

            $mensageiro = new \Ead1_Mensageiro('Alocações inativadas com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro(
                'Erro ao inativar Alocação. Motivo: ' . $e->getMessage(),
                \Ead1_IMensageiro::ERRO,
                $e->getMessage()
            );
        }

        return $mensageiro;
    }

    public function validarNumeroMaximoAluno($id_saladeaula)
    {
        try {

            $toSalaDeAula = new \SalaDeAulaTO();
            $toSalaDeAula->setId_saladeaula((int)$id_saladeaula);
            $toSalaDeAula->setBl_ativo(1);
            $toSalaDeAula->fetch(true, true, true);

            $repositoryName = 'G2\Entity\Alocacao';
            $result = $this->em->createQuery("SELECT COUNT(a.id_alocacao) AS nu_numeroalocacao
							   FROM " . $repositoryName . " a
			         			    WHERE a.bl_ativo = 1
			         			    AND a.id_saladeaula = " . (int)$id_saladeaula)->getResult();
            if (is_array($result)) {
                if ((int)$result[0]['nu_numeroalocacao'] >= (int)$toSalaDeAula->getNu_maxalunos()) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;

            }
            return $result;

        } catch (\Exception $e) {
            return false;
        }

    }

    public function salvarOpcoesSala($data)
    {
        if (empty($data['id_alocacao'])) {
            $mensageiro = new \Ead1_Mensageiro('Não foi encontrada uma alocação válida nesta sala!', \Ead1_IMensageiro::AVISO);
            return $mensageiro;
        }


        $to = new \AlocacaoTO();
        $to->setId_alocacao($data['id_alocacao']);
        $to->fetch(true, true, true);
        $to->setNu_diasextensao($data['nu_diasaluno']);

        $bo = new \SalaDeAulaBO();
        $retorno = $bo->editarAlocacao($to);

        return $retorno;

    }

    /**
     * Faz um foreach adicinando os dias de extensão
     * @param array $data
     */
    public function alteraDiasExtensao(array $data)
    {
        try {
            $mensageiro = new \Ead1_Mensageiro();


            $this->beginTransaction();
            //faz o foreache com todas as pessoas recebendo alterando a data termino recebida
            foreach ($data['collection'] as $value) {
                if ($value['bl_prorroga'] === 'true') {
                    $alocacao = new \G2\Entity\Alocacao();
                    $alocacao->setId_alocacao($value['id_alocacao']);
                    $alocacao->setNu_diasextensao($value['nu_dias']);
                    $this->save($alocacao);
                }
            }
            $this->commit();

            $mensageiro->setMensageiro("Data de término prorrogada com sucesso!", \Ead1_IMensageiro::SUCESSO);

        } catch (\Exception $e) {

            $this->rollback();
            //seta o mensageiro com a mensagem de erro
            $mensageiro->setMensageiro("Erro ao tentar alterar data de término da matricula."
                . $e->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;//retorna o mensageiro

    }

    /**
     * Atualiza a situação do TCC de acordo com a sala de aula e a matrícula do aluno
     * @param $parametrosDeBusca
     * @param $novaSituacaoTcc
     * @return bool
     * @throws \Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function atualizaSituacaoTcc($parametrosDeBusca, $novaSituacaoTcc, $mandarMensagem = true)
    {
        if ((new SituacaoTccValidator())->isValid($parametrosDeBusca, $novaSituacaoTcc)) {
            try {
                $alocacaoEN = $this->findOneBy(\G2\Entity\Alocacao::class, $parametrosDeBusca);
                $alocacaoEN->setId_situacaotcc($novaSituacaoTcc);

                $func_get_args = func_get_args();
                if (!empty($func_get_args[0]['bl_ativo'])) {
                    unset($func_get_args[0]['bl_ativo']);
                }

                return $this->saveAndDispatchTccChangedEvent($alocacaoEN, $func_get_args, $mandarMensagem);
            } catch (\Exception $e) {
                throw new \Zend_Exception($e->getMessage());
            }
        }
    }

    /**
     * Salva a situação do TCC e dispara o evento de atualização da situação do TCC
     * @param G2Entity $entity
     * @param array $arguments
     * @return bool
     * @throws \Zend_Exception
     */
    private function saveAndDispatchTccChangedEvent(G2Entity $entity, array $arguments, $mandarMensagem)
    {
        if ($this->save($entity) && $mandarMensagem == true) {

            $SituacaoTccChanged = new SituacaoTccChanged(new VwAlunosSala(), $this, new \MensagemBO());

            $SituacaoTccChanged->dispatch(['parametrosDeBusca' => $arguments[0], 'situacaoTcc' => $arguments[1]]);
            return true;
        }
        return false;
    }

    /**
     * Retorna a mensagem padrão de email de acordo com a situação do TCC
     * @param $situacaoTcc
     * @return mixed
     * Todo: Refatorar essa função e a de baixo (em outro branch) em uma só
     */
    public function getMensagemPadraoSituacaoTcc($situacaoTcc)
    {
        try {
            if ($this->situacaoTccHasMensagemPadrao($situacaoTcc))
                return $this->mensagensTcc[$situacaoTcc];
        } catch (\OutOfBoundsException $exception) {
            throw new \OutOfBoundsException($exception->getMessage());
        }
    }

    /**
     * Retorna a mensagem do portal do aluno de acordo com a situação do TCC
     * @param $situacaoTcc
     * @return mixed
     */
    public function getMensagemPortal($situacaoTcc)
    {
        try {
            if ($this->situacaoTccHasMensagemPortal($situacaoTcc))
                return $this->mensagensTccPortal[$situacaoTcc];
        } catch (\OutOfBoundsException $exception) {
            throw new \OutOfBoundsException($exception->getMessage());
        }
    }

    /**
     * Verifica se há uma mensagem padrão de email para a respectiva situação do TCC
     * @param $situacaoTcc
     * @return bool
     * Todo: Refatorar essa função e a de baixo (em outro branch) em uma só
     */
    public function situacaoTccHasMensagemPadrao($situacaoTcc)
    {
        return array_key_exists($situacaoTcc, $this->mensagensTcc);
    }

    /**
     * Verifica se há uma mensagem do portal para a respectiva situação do TCC
     * @param $situacaoTcc
     * @return bool
     */
    public function situacaoTccHasMensagemPortal($situacaoTcc)
    {
        return array_key_exists($situacaoTcc, $this->mensagensTccPortal);
    }
    /**
     * Função responsável por receber todos os parâmetros vindo do alocar PRR
     * para distribuir para cada perfil correspondente ( aluno, professor, coordenador)
     *
     * @param $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function notificarEnvolvidos($params)
    {
        $mensageiro = new \Ead1_Mensageiro();
        $mensageiroAluno = new \Ead1_Mensageiro();
        $mensageiroProf = new \Ead1_Mensageiro();
        $mensageiroCoord = new \Ead1_Mensageiro();
        $arrMensageiro = array();

        try {
            // Recupera a matricula do aluno, pois é exigido pelo metodo que trata as variaveis do sistema
            $whereMatDisciplina = array("id_matriculadisciplina" => $params["id_matriculadisciplina"]);
            $matDisciplina = $this->findOneBy(MatriculaDisciplina::class, $whereMatDisciplina);
            $matDisciplina = $this->toArrayEntity($matDisciplina);

        } catch (\Exception $e) {
            return $mensageiro->setMensageiro(
                'Erro Encontrar matricula do aluno. ' . $e->getMessage(),
                \Ead1_IMensageiro::ERRO);
        }

        //Envia e-mail para aluno, e cria um mensageiro para aviso
        $this->notificarAluno($params["id_aluno"], $matDisciplina["id_matricula"], $params["id_saladeaula"],
            $params["id_matriculadisciplina"]);
        $arrMensageiro["aluno"] = $mensageiroAluno->setMensageiro(
            "E-mail para aluno enviado com sucesso.", \Ead1_IMensageiro::SUCESSO);

        //Localiza o professor e o coordenador da sala
        $professor = $this->recuperarProfessorSala($params["id_saladeaula"]);
        $coordenador = $this->recuperarCoordenadorSala($params["id_prjPedagogico"]);

        //Envia e-mail para professor, e cria um mensageiro para aviso
        if ($professor) {
            $this->notificarTutor($professor, $matDisciplina["id_matricula"], $params["id_saladeaula"],
                $params["id_matriculadisciplina"]);
            $arrMensageiro["prof"] = $mensageiroProf->setMensageiro(
                "E-mail para professor enviado com sucesso.", \Ead1_IMensageiro::SUCESSO);
        } else {
            $arrMensageiro["prof"] = $mensageiroProf->setMensageiro(
                "E-mail para professor não enviado.", \Ead1_IMensageiro::ERRO);
        }

        //Envia e-mail para coordenador, e cria um mensageiro para aviso
        if ($coordenador) {
            $this->notificarCoordenador($coordenador, $matDisciplina["id_matricula"], $params["id_saladeaula"],
                $params["id_matriculadisciplina"]);
            $arrMensageiro["coord"] = $mensageiroCoord->setMensageiro(
                "E-mail para coordenador enviado com sucesso.", \Ead1_IMensageiro::SUCESSO);
        } else {
            $arrMensageiro["coord"] = $mensageiroCoord->setMensageiro(
                "E-mail para coordenador não enviado.", \Ead1_IMensageiro::ERRO);

        }

        return $arrMensageiro;

    }


    /**
     * Função responsável por buscar o professor responsável pela sala.
     * Para enviar uma notificação após a alocação na PRR
     * @param $id_saladeaula
     * @return array
     * @throws \Zend_Exception
     */
    public function recuperarProfessorSala($id_saladeaula)
    {
        try {

            $vwUper = $this->findBy(VwUsuarioPerfilEntidadeReferencia::class,
                ["id_saladeaula" => $id_saladeaula]);

            $professor = [];


            if ($vwUper) {
                foreach ($vwUper as $value) {
                    array_push($professor, $this->findOneBy(VwPesquisarPessoa::class,
                        array(
                            'id_usuario' => $value->getId_usuario(),
                            'id_entidade' => $this->getId_entidade()
                        )
                    ));
                }

                return $this->toArrayEntity($professor);
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar encontrar o professor." . $e->getMessage());
        }

        return $professor;
    }

    /**
     * Função responsável por recuperar o coordenador da sala do projeto especificado por parametro
     * Para enviar uma notificação após a alocação na PRR
     *
     * @param $idPrjPedagogico
     * @return array
     * @throws
     */
    public function recuperarCoordenadorSala($idPrjPedagogico)
    {
        try {

            $where = array(
                'id_projetopedagogico' => $idPrjPedagogico,
                'id_perfilpedagogico' => PerfilPedagogico::COORDENADOR_DE_PROJETO
            );

            $prjPedagogico = $this->findBy(VwUsuarioPerfilEntidadeReferencia::class, $where);

            $coord = [];

            if ($prjPedagogico) {
                foreach ($prjPedagogico as $value) {
                    array_push($coord, $this->findOneBy(VwPesquisarPessoa::class,
                        array(
                            'id_usuario' => $value->getId_usuario(),
                            'id_entidade' => $this->getId_entidade()
                        )
                    ));
                }

                return $this->toArrayEntity($coord);
            }

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar encontrar o coordenador." . $e->getMessage());
        }

        return $coord;
    }

    /**
     * Função responsável por fazer o tratamento do aluno e chamar a função que irá dar prosseguimento ao
     * disparo do e-mail
     * @param $idAluno
     * @param $matDisciplina
     * @param $idSaladeaula
     * @throws \Zend_Exception
     */
    public function notificarAluno($idAluno, $matDisciplina, $idSaladeaula, $idMatDisc)
    {
        $this->enviarEmailInteressadosPRR($idAluno, $matDisciplina, $idSaladeaula, $idMatDisc);
    }

    /**
     * Função responsável por fazer o tratamento do professor e chamar a função que irá dar prosseguimento ao
     * disparo do e-mail
     * @param $tutor
     * @param $matricula
     * @param $saladeaula
     * @throws \Zend_Exception
     */
    public function notificarTutor($tutor, $matricula, $saladeaula, $idMatDisc)
    {
        foreach ($tutor as $value) {
            $this->enviarEmailInteressadosPRR($value["id_usuario"], $matricula, $saladeaula, $idMatDisc);
        }
    }

    /**
     * Função responsável por fazer o tratamento do coordenador e chamar a função que irá dar prosseguimento ao
     * disparo do e-mail
     * @param $coordenador
     * @param $matricula
     * @param $saladeaula
     * @throws \Zend_Exception
     */
    public function notificarCoordenador($coordenador, $matricula, $saladeaula, $idMatDisc)
    {
        foreach ($coordenador as $value) {
            $this->enviarEmailInteressadosPRR($value["id_usuario"], $matricula, $saladeaula, $idMatDisc);
        }
    }

    /**
     * Função responsável por receber os interessados (Aluno, Tutor ou Coordenador)
     * E então buscar o texto padrão do sistema, registrar a mensagem de e-mail
     * E chamar a função responsável por fazer o disparo do e-mail
     * @param $idInteressado
     * @param $matricula
     * @param $saladeaula
     * @throws \Zend_Exception
     */
    public function enviarEmailInteressadosPRR($idInteressado, $matricula, $saladeaula, $idMatDisc)
    {
        try {
            $whereEmailMSG = array(
                "id_mensagempadrao" => \MensagemPadraoTO::ALOCACAO_PRR,
                "id_entidade" => $this->sessao->id_entidade,
                "bl_ativo" => 1
            );

            $emailMSGEntity = $this->findCustom(\G2\Entity\EmailEntidadeMensagem::class, $whereEmailMSG);
            $emailMSGEntity = $this->toArrayEntity($emailMSGEntity);

            if (!$emailMSGEntity) {
                throw new \Zend_Exception("Não encontramos uma mensagem padrão.");
            }

            $whereTextoSistema = array("id_textosistema" => $emailMSGEntity[0]['id_textosistema']);

            $textoSistema = $this->findOneBy(\G2\Entity\TextoSistema::class, $whereTextoSistema);
            $textoSistema = $this->toArrayEntity($textoSistema);

            if (!$textoSistema) {
                throw new \Zend_Exception("Não encontramos um texto de sistema.");
            }

            $textoSistemaTO = new \TextoSistemaTO();
            $textoSistemaTO->montaToDinamico($textoSistema);

            $mensagemBO = new \MensagemBO();

            $mensagemBO->enviarNotificacaoTextoSistema(
                $textoSistemaTO,
                array("id_matricula" => $matricula, "id_saladeaula" => $saladeaula, "idMatDisc" => $idMatDisc),
                $emailMSGEntity[0]['id_emailconfig']['id_emailconfig'],
                $idInteressado,
                $textoSistema['id_entidade']['id_entidade'],
                $saladeaula
            );

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao enviar Email para interessados. " . $e->getMessage());
        }

    }

    /**
     * Função responsável por buscar o nome da disciplina e a data de abertura e encerramento
     * para fazer a substuição da variavel de sistema
     * Função utilizada pela função gerarDeclaracao()
     *
     * @param $idSaladeaula
     * @return mixed
     * @throws \Zend_Exception
     */
    public function retornaDisciplinaSala($idMatDisc)
    {
        $salaAula = $this->toArrayEntity($this->findOneBy(VwSalaDisciplinaAlocacao::class, array(
            "id_matriculadisciplina" => $idMatDisc,
            "id_categoriasala" => \G2\Constante\CategoriaSala::PRR,
            "bl_ativo" => 1
        )));

        $dtAbertura = $this->toArrayEntity($this->findOneBy(\G2\Entity\Alocacao::class, array(
            "id_matriculadisciplina" => $idMatDisc,
            "id_categoriasala" => \G2\Constante\CategoriaSala::PRR,
            "bl_ativo" => 1
        )));

        $dtEncerramento = new \Zend_Date();
        $dtEncerramento->setDate($this->converterData($dtAbertura["dt_inicio"]["date"], 'd/m/Y'));
        $dtEncerramento->addDay($salaAula["nu_diasaluno"], \Zend_Date::DAY);

        return array(
            'st_disciplina'     => $salaAula["st_saladeaula"],
            'dt_abertura'       => $dtAbertura["dt_inicio"],
            'dt_encerramento'   => $dtEncerramento
        );
    }
}

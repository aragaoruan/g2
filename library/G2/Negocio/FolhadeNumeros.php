<?php

namespace G2\Negocio;

/**
 * Classe Negocio para FolhadeNumeros
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2014-08-14
 */
class FolhadeNumeros extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function executaRelatorio($parametros)
    {
        $parametros['dt_datainformada'] = $this->converteDate($parametros['dt_inicio']);
        $parametros['id_pai'] = $this->sessao->id_entidade;
        unset($parametros['dt_inicio']);
        $parametros['evolucao'] = array(\G2\Constante\Evolucao::TB_TURMA_PREVISTO, \G2\Constante\Evolucao::TB_TURMA_CONFIRMADO);
        $qb = $this->em->getRepository('\G2\Entity\Venda')->retornaSpMatriculas($parametros);
        return $qb;
    }

    public function findVwEntidadeRecursivaId($where)
    {
        return $this->findBy('\G2\Entity\VwEntidadeRecursivaId', $where, array('st_nomeentidade' => 'ASC'));
    }

    public function retornaTurmaEntidade($params){
        try {
            $repositoryName = 'G2\Entity\Turma';
            return $this->findBy($repositoryName, $params, array('st_turma' => 'ASC'));
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

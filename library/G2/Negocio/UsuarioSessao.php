<?php

namespace G2\Negocio;

/**
 * Classe de negócio para UsuarioSessao (tb_usuariosessao)
 * @author Helder Silva <helder.silva@unyleya.com.br>
 */
class UsuarioSessao extends Negocio
{

    private $repositoryName = 'G2\Entity\UsuarioSessao';

    public function __construct()
    {
        parent::__construct();
    }

    public function gravaSessao()
    {
        $usersession = $this->find('\G2\Entity\UsuarioSessao', \Ead1_Sessao::getSessaoUsuario()->id_usuario);
        if (!$usersession) {
            $objeto = new \G2\Entity\UsuarioSessao();
            $objeto->setId_usuario($this->getReference('\G2\Entity\Usuario', \Ead1_Sessao::getSessaoUsuario()->id_usuario));
            $objeto->setDt_sessao(new \DateTime);
            $objeto->setSt_hash(session_id());
            $this->save($objeto);
            return true;
        }
    }

    /**
     *
     * @param type $hash
     * @return boolean
     * @author helder silva <helder.silva@unyleya.com.br>
     * Método que verifica se existe na pasta tmp do sevidor
     * o hash que está gravado na tb_usuariosessao para o
     * usuario que efetua o login.
     */
    public function verificaHash($hash)
    {
        $usersession = $this->find('\G2\Entity\UsuarioSessao', \Ead1_Sessao::getSessaoUsuario()->id_usuario);

        if ($usersession) {
            $hash_tabela = $usersession->getSt_hash();
            $a = memcache_connect(session_save_path(), 11211);
            if ($a) {
                $var = memcache_get($a, $hash_tabela);
                //$hash_servidor = file_exists(sys_get_temp_dir() . '/sess_' . $hash_tabela);
                if ($var && $hash_tabela != $hash)
                    return true;
            }
            return false;
        }
    }

    /**
     *
     * @param type $hashs
     * @author helder silva <helder.silva@unyleya.com.br>
     * Método que grava e atualiza na tabela tb_usuariosessao,
     * usuário e hash utilizado na sessão.
     */
    public function gravaHash($hash)
    {

        $usersession = $this->find('\G2\Entity\UsuarioSessao', \Ead1_Sessao::getSessaoUsuario()->id_usuario);
        if ($usersession) {
            $usersession->setSt_hash($hash);
            $usersession->setDt_sessao(new \DateTime());
            $this->save($usersession);
        } else {
            $usersession = new \G2\Entity\UsuarioSessao();
            $usersession->setId_usuario($this->getReference('\G2\Entity\Usuario', \Ead1_Sessao::getSessaoUsuario()->id_usuario));
            $usersession->setSt_hash($hash);
            $usersession->setDt_sessao(new \DateTime());
            try {
                $result = $this->save($usersession);
            } catch (\Exception $e) {
                \Zend_Debug::dump($e->getMessage());
            }
        }
    }

    /**
     *
     * @param $hash
     * @return $resultado
     * @author helder silva <helder.silva@unyleya.com.br>
     * Método que deleta o hash do servidor
     */
    public function deletaHash($hash)
    {
        try {
            $usersession = $this->find('\G2\Entity\UsuarioSessao', \Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $hash_tabela = $usersession->getSt_hash();
            $a = memcache_connect(session_save_path(), 11211);
            $resultado = memcache_delete($a, $hash_tabela);
            //$resultado = unlink(sys_get_temp_dir() . '/sess_' . $hash_tabela);
            return $resultado;
        } catch (\Exception $e) {
            \Zend_Debug::dump("Erro ao excluir arquivo da sessao");
        }
    }

    /**
     *
     * @param $hash
     * @return boolean
     * @author helder silva <helder.silva@unyleya.com.br>
     * Método que deleta o hash caso o usuário não deseje
     * kickar a sessão do outro usuário.
     */
    public function cancelaHash($hash)
    {
        try {
            $a = memcache_connect(session_save_path(), 11211);
            $resultado = memcache_delete($a, $hash);
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    public function logoutHash()
    {
        try {
            $usersession = $this->find('\G2\Entity\UsuarioSessao', \Ead1_Sessao::getSessaoUsuario()->id_usuario);
            $this->delete($usersession);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

}

<?php
/**
 * Classe de negócio para VwComercialGeral
 * @author Neemias Santos <neemias.santos@unyleya.com.br>
 */

namespace G2\Negocio;


class VwComercialGeral extends Negocio
{

    private $repositoryName = '\G2\Entity\VwComercialGeral';

    public function retornaDados($params = null)
    {
        try {
            $params['id_evolucao'] = array(
                \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO,
                \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO
            );

            $result = $this->findBy($this->repositoryName, array(
                'id_entidadepai' => $params['id_entidade'],
                'id_entidade' => $params['id_entidade'],
                'id_turma' => $params['id_turma'],
                'id_evolucao' => $params['id_evolucao']
            ), array('st_nomecompleto' => 'ASC'));

            $arrayFinal = array(
                'valorTotal' => 0,
                'valorTotalTurma' => 0,
                'valorTotalCancelados' => 0,
                'valorTotalCursando' => 0,
                'valorTotalCartaDeCredito' => 0
            );
            foreach ($result as $value) {
                $arrayFinal['valorTotal'] += $value->getNu_valorliquido();
                if ($value->getId_evolucao() == \G2\Constante\Evolucao::TB_MATRICULA_CANCELADO) {
                    $arrayFinal['valorTotalCancelados'] += $value->getNu_valorliquido();
                }
                if ($value->getNu_valorcarta()) {
                    $arrayFinal['valorTotalCartaDeCredito'] += $value->getNu_valorcarta();
                }
                $arrayFinal['alunos'][] = $this->toArrayEntity($value);
            }

            /*
             * Total de Cursando Cálculo: (Total do Valor Negociado) - (Total de Cancelados)
             */
            $arrayFinal['valorTotalCursando'] = $arrayFinal['valorTotal'] - $arrayFinal['valorTotalCancelados'];
            /*
             * Valor da Turma:
             * Cálculo: ( Somatório Total do Valor Negociado) - (Somatório Total de Cancelados) - (Somatório Total de Carta de Crédito)
             */
            $arrayFinal['valorTotalTurma'] += $arrayFinal['valorTotal'] - $arrayFinal['valorTotalCancelados'] - $arrayFinal['valorTotalCartaDeCredito'];

            return $arrayFinal;

        } catch (\Doctrine\ORM\ORMException $e) {
            throw new \Zend_Exception("Erro ao Pesquisar." . $e->getMessage());
        }
    }
}
<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Entity Evolução
 */
class Evolucao extends Negocio
{

    private $repositoryName = 'G2\Entity\Evolucao';

    public function __construct ()
    {
        parent::__construct();
    }

    public function findAll ($repository = 'G2\Entity\Evolucao')
    {
        return $this->em->getRepository($this->repositoryName)->findAll();
    }

    public function getReference ($idEvolucao, $repository = 'G2\Entity\Evolucao')
    {
        return $this->em->getReference($this->repositoryName, $idEvolucao);
    }

    /**
     * Retorna Dados da Evolução de acordo com os parametros
     * @param array $where
     * @return \G2\Entity\Evolucao
     */
    public function retornarEvolucao (array $where = array())
    {
        return $this->findBy($this->repositoryName, $where, array('st_evolucao' => 'asc'));
    }

}

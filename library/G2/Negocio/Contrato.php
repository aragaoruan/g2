<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Contrato
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class Contrato extends Negocio
{

    /**
     * @var string
     */
    private $repositoryName = '\G2\Entity\Contrato';
    private $repositoryGrauAcademico = '\G2\Entity\GrauAcademico';
    private $repositoryContratoRegra = 'G2\Entity\ContratoRegra';

    /**
     * Retorna dados de Contratos
     * @param array $where
     * @return mixed | \G2\Entity\Contrato
     * @throws \Zend_Exception
     */
    public function retornarContratos(array $where)
    {
        try {
            return $this->findBy($this->repositoryName, $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar dados de Contrato. " . $e->getMessage());
        }
    }

    /**
     * Retorna todas os ContratoRegra da Entidade
     * @return \Ead1_Mensageiro
     */
    public function findAllRegra($id_entidade = null)
    {
        try {
            $id_entidade = $id_entidade ?: $this->sessao->id_entidade;

            $result = $this->em->getRepository($this->repositoryContratoRegra)
                ->findBy(array('id_entidade' => $id_entidade), array('st_contratoregra' => 'ASC'));

            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                    \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    public function findAllGrausAcademicos()
    {
        try {
            $results = $this->findBy($this->repositoryGrauAcademico, array());
            $arrResult = $this->toArrayEntity($results);
        } catch (\Exception $exc) {
            return new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $arrResult;
    }

    /**
     * Retorna a Regra do Contrato pelo ID
     * @param type $id_contratoregra
     * @return \Ead1_Mensageiro
     */
    public function findRegraById($id_contratoregra)
    {

        try {

            $result = $this->em->getRepository($this->repositoryContratoRegra)->findBy(array(
                'id_entidade' => $this->sessao->id_entidade,
                'id_contratoregra' => $id_contratoregra
            ));
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                    \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * @param \G2\Entity\ContratoRegra $contratoregra
     * @return \Ead1_Mensageiro
     */
    public function salvarRegra(\G2\Entity\ContratoRegra $contratoregra)
    {

        try {

            /**
             * Alteração do Gabriel
             *
             * $serialize = new \Ead1\Doctrine\EntitySerializer($this->em);
             * $vw = $serialize->arrayToEntity($params, new \G2\Entity\TotvsVwIntegraGestorFluxus());
             *
             */
            if (!$contratoregra->getId_entidade()) {
                $contratoregra->setId_entidade(array('id_entidade' => $this->sessao->id_entidade));
            }

            $arrId_entidade = $contratoregra->getId_entidade();
            $id_entidade = $arrId_entidade['id_entidade'];
            $entidade = $this->em->getReference('G2\Entity\Entidade', $id_entidade);
            $contratoregra->setId_entidade($entidade);

            if (!$contratoregra->getId_usuariocadastro()) {
                $contratoregra->setId_usuariocadastro(array('id_usuariocadastro' => $this->sessao->id_usuario));
            }
            $arrId_usuario = $contratoregra->getId_usuariocadastro();
            $id_usuario = isset($arrId_usuario['id_usuario']) ? $arrId_usuario['id_usuario'] : $arrId_usuario['id_usuariocadastro'];
            $usuario = $this->em->getReference('G2\Entity\Usuario', $id_usuario);
            $contratoregra->setId_usuariocadastro($usuario);

            if ($contratoregra->getId_projetocontratoduracaotipo()) {
                $arrId_projetocontratoduracaotipo = $contratoregra->getId_projetocontratoduracaotipo();
                $id_projetocontratoduracaotipo = $arrId_projetocontratoduracaotipo['id_projetocontratoduracaotipo'];
                $projetocontratoduracaotipo = $this->em->getReference('G2\Entity\ProjetoContratoDuracaoTipo',
                    $id_projetocontratoduracaotipo);
                $contratoregra->setId_projetocontratoduracaotipo($projetocontratoduracaotipo);
            }

            if (!$contratoregra->getId_contratoregra()) {
                $contratoregra->setDt_cadastro(new \DateTime());
            }

            $this->save($contratoregra);

            $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_SUCESSO, \Ead1_IMensageiro::SUCESSO);
            $mensageiro->setId($contratoregra->getId_contratoregra());
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    /**
     * @return \Ead1_Mensageiro
     */
    public function findAllProjetoContratoDuracaoTipo()
    {

        try {

            $result = $this->em->getRepository('G2\Entity\ProjetoContratoDuracaoTipo')->findAll();
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO,
                    \Ead1_IMensageiro::AVISO);
            }
        } catch (\Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Busca Contrato By Id
     * @param integer $id
     * @return \G2\Entity\Contrato
     * @throws \Zend_Exception
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     * @since 2014-06-23
     */
    public function findContrato($id)
    {
        try {
            return $this->find($this->repositoryName, $id);
        } catch (\Exception $ex) {
            throw new \Zend_Exception("Erro ao consultar Contrato. " . $ex->getMessage());
        }
    }

    /**
     * Busca dados de contrato regra by id_entidade e id_produto
     * @param integer $idProduto
     * @param integer $idEntidade
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     * Parametro $ws verifica se vem do webservice para fazer a troca do contrato.
     */
    public function buscarRegraContratoProduto($idProduto, $idEntidade, $ws = false)
    {
        try {

            if ($ws) {
                $nppp = new \G2\Negocio\ProdutoProjetoPedagogico();
                $regraPPP = $nppp->findOneBy('\G2\Entity\ProdutoProjetoPedagogico', array('id_produto' => $idProduto));

                if ($regraPPP && $regraPPP->getId_contratoregra()) {
                    $contratoRegraTO = new \ContratoRegraTO();
                    $contratoRegraTO->setId_contratoregra($regraPPP->getId_contratoregra()->getId_contratoregra());
                    $contratoRegraTO->fetch(false, true, true);
                    return new \Ead1_Mensageiro($contratoRegraTO, \Ead1_IMensageiro::SUCESSO);
                }
            }


            $vwProdutoProjetoTipoValorTO = new \VwProdutoProjetoTipoValorTO();
            $vwProdutoProjetoTipoValorTO->setId_produto($idProduto);

            $projetoPedagogicoBO = new \ProjetoPedagogicoBO();
            $produtoProjeto = $projetoPedagogicoBO->retornarVwProdutoProjetoTipoValor($vwProdutoProjetoTipoValorTO);

            if ($produtoProjeto->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                throw new \Zend_Exception('Produto não encontrado!');
            }

            $produtoProjetoTO = $produtoProjeto->getFirstMensagem();

            if ($produtoProjetoTO instanceof \VwProdutoProjetoTipoValorTO) {
                $contratoRegraTO = new \ContratoRegraTO();

                if (is_null($produtoProjetoTO->getId_contratoregra()) || $produtoProjetoTO->getId_contratoregra() == '') {
                    $contratoRegraTO->setId_entidade($idEntidade);
                } else {
                    $contratoRegraTO->setId_contratoregra($produtoProjetoTO->getId_contratoregra());
                }

                $matriculaBO = new \MatriculaBO();
                $contratoRegra = $matriculaBO->retornarContratoRegra($contratoRegraTO);

                if ($contratoRegra->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Zend_Exception('Regra não encontrada!');
                }

                if ($contratoRegra->getFirstMensagem() instanceof \ContratoRegraTO) {
                    return new \Ead1_Mensageiro($contratoRegra, \Ead1_IMensageiro::SUCESSO);
                } else {
                    throw new \Zend_Exception('O retorno não é um ContratoRegraTO');
                }
            } else {
                throw new \Zend_Exception('O resultado da VwProdutoProjeto deve ser um ProdutoProjetoTO');
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao retornar a regra do contrato para o produto especificado. ' . $e->getMessage());
        }
    }

    /**
     * Retorna contrato da venda
     * @param integer $id_venda
     * @return \G2\Entity\Contrato
     * @throws \Zend_Exception
     */
    public function retornaContratoVenda($id_venda, $id_entidade = null)
    {
        try {

            if (!$id_entidade) {
                $id_entidade = $this->sessao->id_entidade;
            }

            return $this->findOneBy($this->repositoryName, array(
                'id_venda' => $id_venda,
                'bl_ativo' => true,
                'id_entidade' => $id_entidade
            ));
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar contrato da venda. " . $e->getMessage());
        }
    }

    /**
     * Retorna os dados de vw_gerarcontrato
     * @param array $where
     * @throws \Zend_Exception
     * @return mixed NULL or VwGerarContrato
     */
    public function retornarVwGerarContrato(array $where = array())
    {
        try {
            return $this->findBy('\G2\Entity\VwGerarContrato', $where);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar contrato. " . $e->getMessage());
        }
    }

    /**
     * Retorna dados da vw_gerarcontrato pelo id_contrato
     * @param $id
     * @throws \Zend_Exception
     * @return mixed NULL or VwGerarContrato
     */
    public function retornarVwGerarContratoById($id)
    {
        try {
            return $this->findOneBy('\G2\Entity\VwGerarContrato', array('id_contrato' => $id));
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar contrato. " . $e->getMessage());
        }
    }

    /**
     * Retornar dados de vw_contratocarteirinha
     * @param array $params
     * @return mixed | \G2\Entity\VwContratoCarteirinha
     * @throws \Zend_Exception
     */
    public function retornarDadosVwContratoCarteirinha(array $params = [])
    {
        try {
            return $this->findBy('\G2\Entity\VwContratoCarteirinha', $params);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar consultar dados de contrato carteirinha. " . $e->getMessage());
        }
    }

    /**
     * Retorna contrato ativo buscando pelo id da matricula
     * @param integer $id_matricula
     * @return null|object
     * @throws \Zend_Exception
     */
    public function findContratoAtivoByMatricula($id_matricula)
    {
        try {
            $return = $this->findOneBy('\G2\Entity\ContratoMatricula',
                array('id_matricula' => $id_matricula, 'bl_ativo' => 1));
            if ($return) {
                return $this->findContrato($return->getId_contrato());
            }
            return null;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar contrato pela matricula. " . $e->getMessage());
        }
    }

    /**
     * Salva ou edita os dados do contrato
     * @param array $data
     * @return \Ead1_Mensageiro|int
     * @throws \Zend_Exception
     */
    public function salvarContrato(array $data)
    {
        try {
            $contratoTO = new \ContratoTO();
            $bo = new \MatriculaBO();
            $contratoTO->montaToDinamico($data);

            if ($contratoTO->getId_contrato()) {
                return $bo->editarContrato($contratoTO);
            } else {
                return $bo->cadastrarContrato($contratoTO);
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar dados do contrato. " . $e->getMessage());
        }
    }

    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     * Método utilizado para corrigir os contratos vindo da vw_errocontrato
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     */
    public function corrigirContratos($params = array())
    {
        try {
            $primeiroAcessoBO = new \PrimeiroAcessoBO();
            $contratosCorrigidos = array();
            $contratosNaoCorrigidos = array();

            //verifica se o parametro venda foi passado. Prioridade para ele

            if (array_key_exists('id_venda', $params) && $params['id_venda']) {
                $vendaObj = $this->find('\G2\Entity\Venda', $params['id_venda']);
                if ($vendaObj) {

                    //Chama o metodo da BO para gerar o pdf do contrato e armazenar na minha pasta
                    $resultado = $primeiroAcessoBO->gerarPDFContrato($vendaObj->getId_venda());

                    if ($resultado instanceof \Ead1_Mensageiro && $resultado->getTipo() == \Ead1_IMensageiro::SUCESSO) {
                        $entidade = $this->find('\G2\Entity\Entidade', $vendaObj->getId_entidade());

                        //marca como corrigido
                        $vendaObj->setBl_contratocorrigido(true);
                        $this->save($vendaObj);


                        array_push($contratosCorrigidos, array(
                            'id_venda' => $vendaObj->getId_venda(),
                            'st_nomecompleto' => $vendaObj->getId_usuario()->getSt_nomecompleto(),
                            'st_nomeentidade' => $entidade->getSt_nomeentidade()
                        ));
                    } else {

                        //marca como nao corrigido quando der erro

                        $vendaObj->setBl_contratocorrigido(false);
                        $this->save($vendaObj);

                        $entidade = $this->find('\G2\Entity\Entidade', $vendaObj->getId_entidade());

                        array_push($contratosNaoCorrigidos, array_push($contratosCorrigidos, array(
                            'id_venda' => $vendaObj->getId_venda(),
                            'st_nomecompleto' => $vendaObj->getId_usuario()->getSt_nomecompleto(),
                            'st_nomeentidade' => $entidade->getSt_nomeentidade()
                        )));
                    }
                } else {
                    throw new \Zend_Exception("Venda inexistente.");
                }
            } else {

                /**verifica se o parametro max foi passado, ele vai dizer o máximo de registros a serem corrigidos
                 * por vez, se nao faz para todos de uma vez.
                 **/


                if (array_key_exists('max', $params) && $params['max']) {
                    $objetos = $this->findby('\G2\Entity\VwErroContrato', array(),
                        array('st_nomecompleto' => 'ASC'), $params['max']);
                } else {
                    $objetos = $this->findCustom('\G2\Entity\VwErroContrato',
                        array(), array('st_nomecompleto' => 'ASC'));
                }

                if ($objetos) {
                    foreach ($objetos as $objeto) {
                        $resultado = $primeiroAcessoBO->gerarPDFContrato($objeto->getId_venda());

                        if ($resultado instanceof \Ead1_Mensageiro && $resultado->getTipo() == \Ead1_IMensageiro::SUCESSO) {

                            //Marca a venda como contrato corrigido
                            $vendaObj = $this->find('\G2\Entity\Venda', $objeto->getId_venda());
                            $vendaObj->setBl_contratocorrigido(true);
                            $this->save($vendaObj);

                            array_push($contratosCorrigidos, array(
                                'id_venda' => $objeto->getId_venda(),
                                'st_nomecompleto' => $objeto->getSt_nomecompleto(),
                                'st_nomeentidade' => $objeto->getSt_nomeentidade()
                            ));

                        } else {

                            //marca o contrato como nao corrigido quando gerar erro
                            $vendaObj = $this->find('\G2\Entity\Venda', $objeto->getId_venda());
                            $vendaObj->setBl_contratocorrigido(false);
                            $this->save($vendaObj);

                            array_push($contratosNaoCorrigidos, array(
                                'id_venda' => $objeto->getId_venda(),
                                'st_nomecompleto' => $objeto->getSt_nomecompleto(),
                                'st_nomeentidade' => $objeto->getSt_nomeentidade()
                            ));
                        }
                    }
                } else {
                    throw new \Zend_Exception('Nenhuma contrato a ser corrigido.');
                }
            }
            return array(
                'Contratos Corrigidos' => $contratosCorrigidos,
                'Contratos Nao Corrigidos' => $contratosNaoCorrigidos
            );
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro: " . $e->getMessage());
        }
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe negócio para ConcursoProduto
 * @author Helder Silva <helder.silva@unyleya.com.br>
 * @since 2014-20-01
 */

class ConcursoProduto extends Negocio
{
    public function __construct()
    {
        parent::__construct();
    }
 
    public function findConcursoProduto($id) {
        try {
            $params['id_concurso'] = $id;
            $repositoryName = 'G2\Entity\ConcursoProduto';
            return $this->findBy($repositoryName, $params);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
    public function deleteConcursoProduto($params) {
        try {
            $resultado = $this->findOneBy('\G2\Entity\ConcursoProduto',$params);
            return $this->delete($resultado);
        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


}
<?php

namespace G2\Negocio;

/**
 * Classe de negócio para VwRegraPagamento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class VwRegraPagamento extends Negocio {

    private $repositoryName = 'G2\Entity\VwRegraPagamento';

    public function __construct() {
        parent::__construct();
    }

    public function findById($id) {
        $result = parent::find($this->repositoryName, $id);
        if ($result) {

            $result = $this->toArrayEntity($result);

            return new \Ead1_Mensageiro($result, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

    public function findAll($repositoryName = null) {
        $result = parent::findAll($this->repositoryName);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }

}

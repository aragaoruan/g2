<?php

namespace G2\Negocio;

/**
 * Classe Negocio para Boleto
 * @author Felipe Pastor <felipe.pastor@unyleya.com.br>
 * @since 2014-07-30
 */
class Boleto extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados dos tipos de conta
     * @return array $return
     */
    public function retornaDadosBoletoConfig($id_contaentidade)
    {
        try {
            $return = $this->findOneBy('G2\Entity\BoletoConfig', array('id_contaentidade' => $id_contaentidade));

            if ($return)
                return $this->toArrayEntity($return);

        } catch (\Zend_Exception $e) {
            return $e;
        }
    }
}
<?php

namespace G2\Negocio;

use G2\Entity\PerfilEntidade;
use G2\Entity\PerfilFuncionalidade;
use G2\Entity\PerfilPermissaoFuncionalidade;
use G2\Entity\VwDesativarProfessoresMoodle;
use G2\Entity\UsuarioPerfilEntidadeReferencia;

/**
 * Classe Negocio para Entidades relacionadas a Perfil
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-08-01
 */
class Perfil extends Negocio
{

    private $repositoryName = 'G2\Entity\Perfil';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorna dados de Perfil
     * @param array $params
     * @return \G2\Entity\Perfil
     */
    public function retornaPerfil(array $params = array())
    {
        return $this->findBy($this->repositoryName, $params);
    }

    /**
     * Retorna referencia de perfil
     * @param integer $idPerfil
     * @return \G2\Entity\Perfil
     */
    public function getReferencePerfil($idPerfil)
    {
        return $this->getReference($this->repositoryName, $idPerfil);
    }


    /**
     * Retorna por padrão o Perfil de professor
     * @param array $params
     * @return array|\G2\Entity\Perfil
     * @author Kayo Silva <kayo.silva@unyleya.com.br>
     */
    public function retornarPerfilProfessor(array $params = array())
    {
        //define os parametros padrões para retornar um perfil de professor para o sistema gestor
        $params['id_perfilpedagogico'] = isset($params['id_perfilpedagogico']) ? $params['id_perfilpedagogico'] : 1;
        $params['id_situacao'] = isset($params['id_situacao']) ? $params['id_situacao'] : \G2\Constante\Situacao::TB_PERFIL_ATIVO;
        $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;
//        $params['id_sistema'] = isset($params['id_sistema']) ? $params['id_sistema'] : \G2\Constante\Sistema::GESTOR;
        $params['id_entidade'] = (isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade); //id_entidade Sessão

        $result = $this->retornaPerfil($params);
        if ($result) {
            if (count($result) === 1) {
                return $result[0];
            }
            return $result;
        } else {
            return array();
        }

    }

    /**
     * Marca os perfis que já foram removidos do moodle como 0 novamente para não serem lidos pela vw novamente
     * @param VwDesativarProfessoresMoodle $vw
     * @return bool
     */
    public function alteraRemocaoProfessorMoodle(VwDesativarProfessoresMoodle $vw)
    {
        try {
            $uper = $this->find('\G2\Entity\UsuarioPerfilEntidadeReferencia', $vw->getId_perfilreferencia());
            if ($uper instanceof UsuarioPerfilEntidadeReferencia) {
                $uper->setBl_desativarmoodle(0);
                $this->merge($uper);
            }
            return true;
        } catch (\Exception $e) {
            //\Zend_Debug::dump($e->getMessage());
            return false;
        }
    }


    /**
     * Metodo que retorna um ou mais registros da vw_perfientidaderelacao buscando tambem por parte
     * do nome do perfil.
     *
     * @param array $params
     * @return mixed
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwPerfilEntidadeRelacaoParteNomePerfil(array $params)
    {
        return $this->em->getRepository('\G2\Entity\VwPerfilEntidadeRelacao')->retornaVwPerfilEntidadeRelacaoParteNomePerfil($params);
    }

    /**
     * Metodo que retorna um ou mais registros da vw_perfientidadenotificacao.
     *
     * @param array $params
     * @return mixed
     *
     * @author Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function retornaVwPerfisEntidadeNotificacao(array $params)
    {
        return $this->findBy('\G2\Entity\VwPerfisEntidadeNotificacao', $params);
    }

    /**
     * Faz a persistência dos perfis
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param array $data
     * @return mixed
     * @throws \Zend_Exception
     * @throws \Zend_Validate_Exception
     */
    public function persistirPerfil(array $data)
    {
        try {

            $objPerfil = new \G2\Entity\Perfil();
            $objPerfil->setDt_cadastro(new \DateTime());

            if (!array_key_exists('st_nomeperfil', $data)) {
                throw new \Zend_Validate_Exception('Parâmetro faltante: Nome do Perfil (st_nomeperfil). ');
            } elseif (!$data['st_nomeperfil']) {
                throw new \Zend_Validate_Exception('O nome do perfil não pode ser vazio! ');
            }

            if (!array_key_exists('id_situacao', $data)) {
                throw new \Zend_Validate_Exception('Parâmetro faltante: Situação (id_situacao). ');
            } elseif (!$data['id_situacao']) {
                throw new \Zend_Validate_Exception('Selecione uma situação! ');
            }

            if (!array_key_exists('id_sistema', $data)) {
                throw new \Zend_Validate_Exception('Parâmetro faltante: Sistema (id_sistema). ');
            } elseif (!$data['id_sistema']) {
                throw new \Zend_Validate_Exception('Selecione um sistema! ');
            }

            //Verifica se tem id_perfil, se não, verifica se tem nome repetido.
            if (array_key_exists('id_perfil', $data) && $data['id_perfil']) {
                $objPerfil = $this->find($this->repositoryName, $data['id_perfil']);
            } else {
                $objPerfRep = $this->findBy('\G2\Entity\Perfil',
                    array(
                        'st_nomeperfil' => $data['st_nomeperfil'],
                        'id_entidade' => $this->sessao->id_entidade,
                        'bl_ativo' => true
                    ));
                if ($objPerfRep) {
                    throw new \Zend_Validate_Exception('Não é possível adicionar dois perfis com o mesmo nome. ');
                }
            }

            $objPerfil->setId_entidade($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
            $objPerfil->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objPerfil->setId_situacao($this->getReference('\G2\Entity\Situacao', $data['id_situacao']));
            $objPerfil->setId_sistema($this->getReference('\G2\Entity\Sistema', $data['id_sistema']));
            $objPerfil->setSt_nomeperfil($data['st_nomeperfil']);
            if (array_key_exists('id_perfilpedagogico', $data) && $data['id_perfilpedagogico']) {
                $objPerfil->setId_perfilpedagogico($this->getReference('\G2\Entity\PerfilPedagogico',
                    $data['id_perfilpedagogico']));
            }

            $objPerfil->setBl_ativo($data['bl_ativo']);
            $objPerfil->setBl_padrao(false);

            return $this->save($objPerfil);

        } catch (\Zend_Validate_Exception $ze) {
            throw new \Zend_Validate_Exception($ze->getMessage());
        } catch (\Exception $exception) {
            throw new \Zend_Exception('Erro ao persistir o perfil. ' . $exception->getMessage());
        }
    }

    /**
     *
     * Vincula o perfil a uma determinada entidade.
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param $perfil
     * @param $entidade
     * @return bool
     * @throws \Zend_Exception
     */
    public function persistirPerfilEntidade(\G2\Entity\Perfil $perfil, $entidade)
    {
        try {

            if (!$perfil instanceof \G2\Entity\Perfil) {
                throw new \Zend_Exception('Perfil não é uma instância de perfil.');
            }

            if (!$entidade) {
                throw new \Zend_Exception('Nenhuma entidade com parâmetro.');
            }

            $objPE = $this->findOneBy('\G2\Entity\PerfilEntidade',
                array(
                    'id_perfil' => $perfil->getId_perfil(),
                    'id_entidade' => $entidade
                ));
            if (!$objPE) {
                $obj = new \G2\Entity\PerfilEntidade();
                $obj->setId_perfil($perfil->getId_perfil());
                $obj->setId_entidade($entidade);
                $obj->setSt_nomeperfil($perfil->getSt_nomeperfil());
                $this->save($obj);
            }
            return true;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao vincular Perfil a Entidade: " . $e->getMessage());
        }
    }

    /**
     * Vincula as entidades ao perfil.
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param $entidades
     * @param $perfil
     * @return bool
     * @throws \Zend_Exception
     */
    public function persistirPerfilEntidadeArvore(array $entidades, \G2\Entity\Perfil $perfil)
    {
        try {

            if (!$perfil instanceof \G2\Entity\Perfil) {
                throw new \Zend_Exception('Parâmetro perfil não é uma instância de perfil.');
            }

            if (!is_array($entidades) || empty($entidades)) {
                throw new \Zend_Exception('É necessária uma entidade para vinculo.');
            }

            $this->beginTransaction();
            $objsPerfilEtidades = $this->findBy('\G2\Entity\PerfilEntidade',
                array('id_perfil' => $perfil->getId_perfil()));

            //Retira todos as entidades vinculadas aquele perfil
            if ($objsPerfilEtidades) {
                foreach ($objsPerfilEtidades as $key => $objsPerfilEtidade) {
                    $this->delete($objsPerfilEtidade);
                }
            }

            //Vincula novamente os perfis
            foreach ($entidades as $index => $entidade) {
                $objPerfilEntidade = new PerfilEntidade();
                $objPerfilEntidade->setId_perfil($perfil->getId_perfil())
                    ->setId_entidade($entidade);
                $objPerfilEntidade->setSt_nomeperfil($perfil->getSt_nomeperfil());
                $this->save($objPerfilEntidade);
            }

            $this->commit();

            return true;

        } catch (\Exception $exception) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao persistir as organizações . ' . $exception->getMessage());
        }
    }

    /**
     * Vincula o perfil a funcionalidade.
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param $funcionalidades
     * @param $perfil
     * @return bool
     * @throws \Zend_Exception
     */
    public function persistirPerfilFuncionalidade(array $funcionalidades, \G2\Entity\Perfil $perfil)
    {
        try {
            if (!$perfil instanceof \G2\Entity\Perfil) {
                throw new \Zend_Exception('Parâmetro perfil não é uma instância de perfil.');
            }

            if (!is_array($funcionalidades) || empty($funcionalidades)) {
                throw new \Zend_Exception('É necessária uma funcionalidade para vinculo.');
            }

            $objsPerfilFuncionalidades = $this->findBy('\G2\Entity\PerfilFuncionalidade',
                array(
                    'id_perfil' => $perfil->getId_perfil()
                ));

            //Verifica se existem registros de perfilfuncionalidade
            if ($objsPerfilFuncionalidades) {
                $this->beginTransaction();
                foreach ($objsPerfilFuncionalidades as $index => $objsPerfilFuncionalidade) {

                    //Verifica se a funcionalidade não foi vinculada, se não, remove a funcionalidade e suas permissões.
                    if (!in_array($objsPerfilFuncionalidade->getId_funcionalidade()->getId_funcionalidade(),
                        $funcionalidades)
                    ) {

                        $objPerfilPermissaoFuncionalidades = $this->findBy(
                            '\G2\Entity\PerfilPermissaoFuncionalidade',
                            array(
                                'id_perfil' => $perfil->getId_perfil(),
                                'id_funcionalidade' => $objsPerfilFuncionalidade->getId_funcionalidade()
                                    ->getId_funcionalidade()
                            ));
                        if ($objPerfilPermissaoFuncionalidades) {
                            foreach ($objPerfilPermissaoFuncionalidades as $index => $objPerfilPermissaoFuncionalidade) {
                                $this->delete($objPerfilPermissaoFuncionalidade);
                            }
                        }

                        $objsPerfilFuncionalidade->setBl_ativo(false);
                        $this->save($objsPerfilFuncionalidade);

                        //se ela existe e deve ser reativada
                    } else {
                        $objsPerfilFuncionalidade->setBl_ativo(true);
                        $this->save($objsPerfilFuncionalidade);
                    }

                }
                $this->commit();
            }


            $this->beginTransaction();

            //insere o restante das funcionalidades que não existem no perfil.
            foreach ($funcionalidades as $index => $funcionalidade) {
                if ($funcionalidade) {
                    $func = $this->find('\G2\Entity\Funcionalidade', $funcionalidade);
                    if ($func) {
                        $perfFun = $this->findBy('\G2\Entity\PerfilFuncionalidade',
                            array(
                                'id_perfil' => $perfil->getId_perfil(),
                                'id_funcionalidade' => $func->getId_funcionalidade()
                            ));

                        if (!$perfFun) {
                            $objPF = new PerfilFuncionalidade();
                            $objPF->setId_perfil($perfil);
                            $objPF->setId_funcionalidade($func);
                            $objPF->setBl_ativo(true);
                            $this->save($objPF);
                        }
                    }
                }
            }


            $this->commit();
            return true;

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao vincular funcionalidades ao perfil: " . $e->getMessage());
        }
    }

    /**
     * Vincula as funcionalidades a suas permissoes e a seu perfil.
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param $ppfs
     * @param $perfil
     * @throws \Zend_Exception
     */
    public function vincularPerfilPermissaoFuncionalidade(array $ppfs, \G2\Entity\Perfil $perfil)
    {

        try {
            if (!$perfil instanceof \G2\Entity\Perfil) {
                throw new \Zend_Exception('Parâmetro perfil não é uma instância de perfil.');
            }

            if (!is_array($ppfs) || empty($ppfs)) {
                throw new \Zend_Exception('É necessária uma permissão selecionada para vinculo.');
            }

            //Limpa todas as permissões para esse perfil e funcionalidae
            $this->beginTransaction();

            $objPPFPerfil = $this->findBy('\G2\Entity\PerfilPermissaoFuncionalidade',
                array(
                    'id_perfil' => $perfil->getId_perfil(),
                ));

            foreach ($objPPFPerfil as $index => $item) {
                $this->delete($item);
            }

            $this->commit();

            $this->beginTransaction();

            //Insere novamente todas as permissoees perfil funcionalidade
            foreach ($ppfs as $index => $ppf) {

                $objPF = $this->findBy('\G2\Entity\PerfilFuncionalidade',
                    array(
                        'id_perfil' => $perfil->getId_perfil(),
                        'id_funcionalidade' => $ppf['id_funcionalidade']
                    ));

                //verifica se existe a funcionalide para esse perfil e insere
                if ($objPF) {
                    $newPPF = new PerfilPermissaoFuncionalidade();
                    $newPPF->setId_funcionalidade($ppf['id_funcionalidade'])
                        ->setId_perfil($perfil->getId_perfil())
                        ->setId_permissao($ppf['id_permissao']);
                    $this->save($newPPF);
                }
            }
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao vincular perfil as permissão e as funcionalidades : " . $e->getMessage());
        }

    }

    /**
     * Limpa as permissões de determinado perfil.
     *
     * Author: Helder Silva <helder.silva@unyleya.com.br>
     *
     * @param \G2\Entity\Perfil $perfil
     * @return bool
     * @throws \Zend_Exception
     */
    public function excluirPermissoesPerfil(\G2\Entity\Perfil $perfil)
    {
        try {

            if (!$perfil instanceof \G2\Entity\Perfil) {
                throw new \Zend_Exception('Parâmetro perfil não é uma instância de perfil.');
            }

            $this->beginTransaction();
            if ($perfil instanceof \G2\Entity\Perfil && $perfil->getId_perfil()) {
                $objPPFs = $this->findBy('\G2\Entity\PerfilPermissaoFuncionalidade',
                    array(
                        'id_perfil' => $perfil->getId_perfil()
                    ));
                foreach ($objPPFs as $index => $objPPF) {
                    $this->delete($objPPF);
                }
            } else {
                throw new \Zend_Exception("O parâmetro deve ser uma instância de Perfil : ");
            }
            $this->commit();
            return true;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro: " . $e->getMessage());
        }
    }
}

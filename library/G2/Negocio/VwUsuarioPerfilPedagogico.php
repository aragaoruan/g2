<?php
namespace G2\Negocio;

/**
 * Classe de negócio para VwUsuarioPerfilPedagogico (tb_vwusuarioperfilpedagogico)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class VwUsuarioPerfilPedagogico extends Negocio
{

    private $repositoryName;

    public function __construct() {
        parent::__construct();
        $this->repositoryName = str_replace('\Negocio', '\Entity', __CLASS__);
    }

    public function findBy($repositoryName, array $where, array $order = null, $limit = null, $offset = null) {
        $repositoryName = $this->repositoryName;

        if (isset($where['search']) && $where['search']) {
            $likes = $where['search'];
            unset($where['search']);

            $dql  = "SELECT tb FROM {$repositoryName} AS tb ";
            $dql .= "WHERE tb.id_entidade = " . $this->sessao->id_entidade . " ";

            foreach ($where as $attribute => $value) {
                $dql .= "AND tb.{$attribute} = '{$value}' ";
            }

            foreach ($likes as $attribute => $value) {
                $dql .= "AND tb.{$attribute} LIKE '%{$value}%' ";
            }

            return $this->em->createQuery($dql)->getResult();
        } else {
            return parent::findBy($repositoryName, $where, $order, $limit, $offset);
        }

    }

}
<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Dashboard (tb_dashboard)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class Dashboard extends Negocio
{

    private $repositoryName = '\G2\Entity\Dashboard';

    public function getAll($where = array(), $order = null)
    {
        $results = array();

        $mensageiro = parent::findBy($this->repositoryName, $where, $order);

        if ($mensageiro) {
            foreach ($mensageiro as $entity) {
                $results[] = array(
                    'id_dashboard' => $entity->getId_dashboard(),
                    'st_dashboard' => $entity->getSt_dashboard(),
                    'st_descricao' => $entity->getSt_descricao(),
                    'st_origem' => $entity->getSt_origem(),
                    'st_tipochart' => $entity->getSt_tipochart(),
                    'st_xtipo' => $entity->getSt_xtipo(),
                    'st_xtitulo' => $entity->getSt_xtitulo(),
                    'st_ytipo' => $entity->getSt_ytipo(),
                    'st_ytitulo' => $entity->getSt_ytitulo(),
                    'st_campolabel' => $entity->getSt_campolabel(),
                    'id_funcionalidade' => array(
                        'id_funcionalidade' => $entity->getId_funcionalidade() ? $entity->getId_funcionalidade()->getId_funcionalidade() : null,
                        'st_funcionalidade' => $entity->getId_funcionalidade() ? $entity->getId_funcionalidade()->getSt_funcionalidade() : null
                    ),
                    'id_situacao' => array(
                        'id_situacao' => $entity->getId_situacao() ? $entity->getId_situacao()->getId_situacao() : null,
                        'st_situacao' => $entity->getId_situacao() ? $entity->getId_situacao()->getSt_situacao() : null,
                    ),
                    'bl_ativo' => $entity->getBl_ativo()
                );
            }
        }

        return $results;
    }

    /**
     * @description Retorna todos os dashboards que o perfil e entidade logado podem ver
     * @param array $where
     * @param array $order
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getBySession(array $where, array $order = null)
    {
        if (empty($this->sessao->id_entidade) && empty($this->sessao->id_perfil)) {
            return new \Ead1_Mensageiro("É necessário estar logado para acessar esse recurso!", \Ead1_IMensageiro::ERRO);
        }
        $sql = "SELECT * FROM vw_perfilentidadefuncionalidade AS vw
        JOIN tb_dashboard AS tb ON vw.id_funcionalidade = tb.id_funcionalidade
        WHERE vw.id_entidade = {$this->sessao->id_entidade} AND vw.id_perfil = {$this->sessao->id_perfil}";

        if (is_array($where) && $where) {
            foreach ($where as $attr => $value) {
                $sql .= " AND tb.{$attr} = '{$value}'";
            }
        }

        if (is_array($order) && $order) {
            $orders = array();
            foreach ($order as $field => $sort) {
                $orders[] = 'tb.' . $field . ' ' . $sort;
            }
            $sql .= ' ORDER BY ' . implode(', ', $orders);
        }

        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    /**
     * Retorna os dados do dash
     * @param null $id_dashboard
     * @return array
     * @throws \Exception
     */
    public function getJson($id_dashboard = null)
    {
        try {
            if (!$id_dashboard)
                throw new \Exception("Informe o id do dashboard");

            $dashboard = parent::find($this->repositoryName, $id_dashboard);

            if ($dashboard && $dashboard->getId_dashboard()) {
                $dashboard = $this->toArrayEntity($dashboard);

                // Buscar os campos que vao ter na serie do Dashboard
                $campos = $this->findBy('\G2\Entity\CampoDashboard', array('id_dashboard' => $dashboard['id_dashboard']));
                foreach ($campos as $key => $campo) {
                    $campos[$key] = $this->toArrayEntity($campo);
                }

                return $this->prepareJson($dashboard, $campos);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getJsonPreview($params)
    {
        $results = array();

        $prepared = $this->prepareJson($params, $params['tb_campodashboard']);
        if ($prepared)
            $results = $prepared;

        return $results;
    }

    public function prepareJson($params, $campos = array())
    {
        $result = array();

        $fields = array();
        foreach ($campos as $campo) {
            $fields[] = 'tb.' . $campo['st_campodashboard'] . ' AS ' . str_replace(' ', '_', trim($campo['st_titulocampo']));
        }

        if (!$fields)
            return $result;

        // Adicionar o Nome da Serie (st_campolabel de tb_dashboard) na consulta
        if (isset($params['st_campolabel']) && $params['st_campolabel'])
            array_unshift($fields, 'tb.' . $params['st_campolabel'] . ' AS st_seriename');

        // Conexao statement devido à origem ser dinamica
        $stmt = $this->em->getConnection()->prepare("SELECT " . implode(', ', $fields) . " FROM {$params['st_origem']} AS tb WHERE tb.id_entidade = {$this->sessao->id_entidade}");
        $stmt->execute();
        $values = $stmt->fetchAll();

        // Se nao tiver dados na tabela, nao retornar este dashboard
        if (!$values)
            return $result;

        $series = array();
        $somas = array();
        foreach ($values as $x => $value) {
            $serieName = 'Valor';

            // Fazer a somatoria total dos itens do mesmo campo
            foreach ($value as $field => $item) {
                if (!isset($somas[$field]))
                    $somas[$field] = 0;

                $somas[$field] += doubleval($item);
            }

            $data = array();
            foreach ($value as $field => $item) {
                if ($field == 'st_seriename') {
                    $serieName = $item;
                    continue;
                }

                $data[] = array(str_replace('_', ' ', $field) . ': ' . $somas[$field], doubleval($item));
            }

            $series[] = array(
                'showInLegend' => false,
                'name' => $serieName,
                'data' => $data
            );
        }

        $result = array(
            'chart' => array(
                'type' => $params['st_tipochart']
            ),
            'title' => array(
                'text' => $params['st_dashboard']
            ),
            'xAxis' => array(
                'type' => $params['st_xtipo'],
                'title' => array(
                    'text' => $params['st_xtitulo']
                )
            ),
            'yAxis' => array(
                'type' => $params['st_ytipo'],
                'title' => array(
                    'text' => $params['st_ytitulo']
                )
            ),
            'legend' => array(
                'enabled' => true
            ),
            'series' => $series
        );

        return $result;
    }

    public function save($data)
    {

        $id_attribute = 'id_dashboard';

        if (isset($data[$id_attribute]) && $data[$id_attribute]) {
            // Se existir o ID, buscar registro
            $entity = $this->find($this->repositoryName, $data[$id_attribute]);
        } else {
            // Se não existir o ID, instanciar novo registro
            $entity = new $this->repositoryName;
        }

        /**********************
         * Preparar atributos FK
         **********************/
        if (isset($data['id_situacao']))
            $data['id_situacao'] = $this->find('\G2\Entity\Situacao', $data['id_situacao']);

        if (isset($data['id_funcionalidade']['id_funcionalidade']))
            $data['id_funcionalidade'] = $this->find('\G2\Entity\Funcionalidade', $data['id_funcionalidade']['id_funcionalidade']);


        /**********************
         * Definindo atributos
         **********************/
        $this->arrayToEntity($entity, $data);


        /**********************
         * Acao de salvar
         **********************/
        if (isset($data[$id_attribute]) && $data[$id_attribute])
            $entity = $this->merge($entity);
        else
            $entity = $this->persist($entity);

        $getId = 'get' . ucfirst($id_attribute);
        $data[$id_attribute] = $entity->$getId();


        /*********************
         * Salvando relacao de CamposDashboard
         *********************/
        if (array_key_exists('tb_campodashboard', $data)) {
            // Primeiramente, remover todas as relacoes atuais
            $campos = $this->findBy('\G2\Entity\CampoDashboard', array('id_dashboard' => $data[$id_attribute]));
            foreach ($campos as $entity) {
                $this->delete($entity);
            }

            // Inserir CampoDashboard que foram adicionados
            if (is_array($data['tb_campodashboard']) && $data['tb_campodashboard']) {
                foreach ($data['tb_campodashboard'] as $campo) {
                    $novo = new \G2\Entity\CampoDashboard();
                    $novo->setId_dashboard($data[$id_attribute]);
                    $novo->setSt_campodashboard($campo['st_campodashboard']);
                    $novo->setSt_titulocampo($campo['st_titulocampo']);
                    parent::save($novo);
                }
            }
        }


        $mensageiro = new \Ead1_Mensageiro();
        return $mensageiro->setMensageiro($this->toArrayEntity($entity), \Ead1_IMensageiro::SUCESSO, $data[$id_attribute]);

    }


}
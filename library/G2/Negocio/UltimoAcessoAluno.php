<?php

namespace G2\Negocio;


/**
 * Class UltimoAcessoAluno
 * @package G2\Negocio
 */
class UltimoAcessoAluno extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array $params
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executaRelatorio($params = array(), $detalhado = false)
    {

        $linhadenegocio = (new \G2\Negocio\EsquemaConfiguracao())->retornaItemPorEntidade( \G2\Constante\ItemConfiguracao::LINHA_DE_NEGOCIO, $this->sessao->id_entidade);


        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])) {
            $sel .= " AND vw.id_entidadeatendimento = :id_entidadeatendimento";
        } else {
            $repo = $this->em->getRepository('\G2\Entity\VwEntidadeRecursivaId');
            $result = $repo->retornarEntidadesRecursivaById($this->sessao->id_entidade);
            $arrayEntidade = array();
            foreach ($result as $ent) {
                $arrayEntidade[] = $ent['id_entidade'];
            }
            if (!empty($arrayEntidade))
                $sel .= " AND vw.id_entidadeatendimento in(" . join(',', $arrayEntidade) . ")";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {
            $sel .= " AND vw.dt_matricula BETWEEN :dt_inicio AND :dt_fim ";
        } elseif (!empty($params['dt_inicio'])) {
            $sel .= " AND vw.dt_matricula > :dt_inicio ";
        } elseif (!empty($params['dt_fim'])) {
            $sel .= " AND vw.dt_matricula < :dt_fim ";
        }

        $sql = "SELECT id_contrato,
                       id_matricula,
                       st_evolucao,
                       st_nomecompleto,
                       dt_cadastro,
                       id_entidadeatendimento,
                       dt_matricula,
                       st_nomeentidade,
                       st_projetopedagogico,
                       nu_disciplina";

        //verifica se é para retornar os dados detalhados e concatena as colunas na query
        if ($detalhado) {
            $sql .= ",
                    nu_disciplinascomnota,
                    nu_disciplinasemnota,
                    nu_disciplinasnaoencerradas,
                    st_email";
        }

        if($linhadenegocio['st_valor'] == \G2\Constante\LinhaDeNegocio::POS_GRADUACAO){
            $sql .= ",st_situacaoagendamento";
        }else{
            $sql .= ",'-' as st_situacaoagendamento";
        }

        $sql .= " FROM(
                    SELECT CAST((vd.id_venda) AS VARCHAR)+'G2U' AS id_contrato,
                          mt.id_matricula,
                          ev.st_evolucao,
                          us.st_nomecompleto,
                          CONVERT(VARCHAR, mt.dt_ultimoacesso, 103) + ' ' + CONVERT(VARCHAR, mt.dt_ultimoacesso, 108) AS dt_cadastro,
                          mt.id_entidadeatendimento,
                          mt.dt_cadastro as dt_matricula,
                          e.st_nomeentidade,
                          aa.st_situacao as st_situacaoagendamento";

        //verifica se é para retornar os dados detalhados e concatena as colunas na query
        if ($detalhado) {
            $sql .= ", (select
                          count(distinct aprovado.id_disciplina)
                        from vw_avaliacaoaluno as aprovado
                        where aprovado.id_matricula = mt.id_matricula
                            and aprovado.id_evolucao = " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_APROVADO . "
                            and aprovado.id_situacao = " . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_ATIVA . "
                      ) as nu_disciplinascomnota,
		               (select
		                    count(distinct reprovado.id_disciplina)
		               from vw_avaliacaoaluno as reprovado
		               where  reprovado.id_matricula = mt.id_matricula
                           and  reprovado.id_evolucao = " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_INSATISFATORIO . "
                           and  reprovado.id_situacao = " . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_ATIVA . "
		               ) as nu_disciplinasemnota,
		               (select
		                    count(distinct cursando.id_disciplina)
		                from vw_avaliacaoaluno as cursando
		                where  cursando.id_matricula = mt.id_matricula
                            and  cursando.id_evolucao = " . \G2\Constante\Evolucao::TB_MATRICULADISCIPLINA_CURSANDO . "
                            and  cursando.id_situacao = " . \G2\Constante\Situacao::TB_MATRICULADISCIPLINA_ATIVA . "
		                ) as nu_disciplinasnaoencerradas,
		                ce.st_email,
		                (select
		                    st_projetopedagogico
		                from tb_projetopedagogico as pp
		                where pp.id_projetopedagogico = mt.id_projetopedagogico
		                ) as st_projetopedagogico,
		                (select
		                    count(*)
		                from vw_gradenota as gn
		                where gn.id_matricula = mt.id_matricula
		                ) as nu_disciplina";
        }


        $sql .= " FROM dbo.tb_matricula AS mt
                     JOIN dbo.tb_vendaproduto AS vd ON vd.id_matricula = mt.id_matricula";

        /*
         * if colocado pra adicionar esse trecho no join, também para não impactar nas demais funcionalidades que usam
         * essa query, pois não sei se todas atendem a mesma logica que o relatório de divisão de carteira usa
         */
        if ($detalhado) {
            $sql .= " or vd.id_matricula = mt.id_matriculaorigem ";
        }

        $sql .= "    JOIN dbo.tb_evolucao AS ev ON ev.id_evolucao = mt.id_evolucao
                     JOIN dbo.tb_usuario AS us ON us.id_usuario = mt.id_usuario
                     JOIN dbo.tb_entidade AS e ON e.id_entidade = mt.id_entidadematricula
                     LEFT JOIN dbo.tb_avaliacaoagendamento AS ava ON ava.id_matricula = mt.id_matricula AND ava.bl_ativo = 1
                     LEFT JOIN dbo.tb_situacao AS aa ON aa.id_situacao = ava.id_situacao";

        //verifica se é para retornar os dados detalhados e concatena os joins na query
        if ($detalhado) {
            $sql .= " join tb_contatosemailpessoaperfil as cepp on cepp.id_usuario = us.id_usuario and cepp.bl_padrao = 1 and cepp.id_entidade = e.id_entidade
                    join tb_contatosemail as ce on ce.id_email = cepp.id_email ";
        }

        $sql .= " WHERE mt.dt_ultimoacesso IS NOT NULL
                     GROUP BY
                        vd.id_venda,
                        mt.id_matricula,
                        mt.id_projetopedagogico,
                        us.st_nomecompleto,
                        ev.st_evolucao,
                        mt.id_entidadeatendimento,
                        mt.dt_cadastro,
                        mt.dt_ultimoacesso,
                        e.st_nomeentidade,
                        aa.st_situacao";

        //verifica se é para retornar os dados detalhados e concatena o group by
        if ($detalhado) {
            $sql .= ",ce.st_email ";
        }

        $sql .= ") AS vw
                WHERE 1 = 1 {$sel} ORDER BY st_nomecompleto";

        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])) {
            $statement->bindValue('id_entidadeatendimento', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])) {
            $dt_fim = $this->converteDate($params['dt_fim']);//(new \DateTime($params['dt_fim']))->format('d/m/Y');
            $dt_inicio = $this->converteDate($params['dt_inicio']);//(new \DateTime($params['dt_inicio']))->format('d/m/Y');

            //concatenando a hora de inicio e fim
            $statement->bindValue('dt_inicio', $dt_inicio . " 00:00:00");
            $statement->bindValue('dt_fim', $dt_fim . " 23:59:59");

        } elseif (!empty($params['dt_inicio'])) {

            $dt_inicio = $this->converteDate($params['dt_inicio']);
            $statement->bindValue('dt_inicio', $dt_inicio);

        } elseif (!empty($params['dt_fim'])) {
            $dt_fim = $this->converteDate($params['dt_fim']);
            $statement->bindValue('dt_fim', $dt_fim);

        }

        $statement->execute();
        $results = $statement->fetchAll();

        return $results;
    }

} 
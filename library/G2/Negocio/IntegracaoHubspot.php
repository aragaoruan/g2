<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03/06/2015
 * Time: 15:13
 */

namespace G2\Negocio;


class IntegracaoHubspot extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function enviaDadosHubspot()
    {
        try {
            $objetoVenda = $this->em->getRepository('\G2\Entity\Venda')->retornaVendasToHubspot();

            if ($objetoVenda) {

                $array_dados = array();
                foreach ($objetoVenda as $value) {

                    $array_valores = array();
                    $name = explode(" ", $value['st_nomecompleto']);
                    $firstName = $name[0];
                    $lastName = end($name);
//

                    $arrayenviar = array('properties' => array(
                        array('property' => 'email', 'value' => $value['st_email']),
                        array('property' => 'firstname', 'value' => $firstName),
                        array('property' => 'lastname', 'value' => $lastName),
                        array('property' => 'parceiro', 'value' => $value['st_nomeentidade']),
                        array('property' => 'produto_comprado', 'value' => $value['st_produto']),
                        array('property' => 'lifecyclestage', 'value' => 'customer'),
                    ));

                    $endpoint = 'https://api.hubapi.com/contacts/v1/contact/createOrUpdate/email/'.$value['st_email'].'/?hapikey=8a3f6a1f-d885-4d96-9952-5cf49b5fb371';
                    $ch = @curl_init();
                    @curl_setopt($ch, CURLOPT_POST, true);
                    @curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrayenviar));
                    @curl_setopt($ch, CURLOPT_URL, $endpoint);
                    @curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                    @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = @curl_exec($ch); //Log the response from HubSpot as needed.
                    $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
                    @curl_close($ch);
                    echo $status_code . " " . $response;
//                    if ($status_code != 204) {
//                        throw new \Zend_Exception('Erro ao enviar dados para a Hubspot. ');
//                    }
                }

            } else {
                echo('Nenhum dado para enviar ao Hubspot!');
                exit;
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao enviar dados para a Hubspot. ' . $e->getMessage());
            exit;
        }
        return true;
    }

}
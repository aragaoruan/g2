<?php

namespace G2\Negocio;


use Ead1\Financeiro\Fluxus\Integracao;
use G2\Entity\MotivoCancelamento;
use G2\Utils\Helper;

class CancelamentoPosGraduacao extends Cancelamento
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param null $id_usuario
     * @return array|\Ead1_Mensageiro|\Exception
     */
    public function retornarContratoMatriculaUsuario($id_usuario = null)
    {

        try {

            if (!isset($id_usuario)) {
                return new \Ead1_Mensageiro("ID do usuário não informado.",
                    \Ead1_IMensageiro::ERRO, 412);
            }


            $repo = $this->em->getRepository('\G2\Entity\VwContratoMatriculaUsuario');

            $query = $repo->createQueryBuilder("vw")
                ->select("vw")
                ->where("1=1")
                ->andWhere("vw.id_usuario = :id_usuario")
                ->setParameter('id_usuario', $id_usuario)
                ->andWhere("vw.id_evolucao = :id_evolucao")
                ->setParameter("id_evolucao", \G2\Constante\Evolucao::TB_MATRICULA_CURSANDO)
                ->andWhere("vw.id_cancelamento IS NULL")
                ->orderBy("vw.id_contrato", "DESC");


            $result = $query->getQuery()->getResult();

            return $this->toArrayEntity($result);

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param null $id_usuario
     * @return array|\Ead1_Mensageiro|\Exception
     */
    public function retornarProcessosDeCancelamentoUsuario($id_usuario = null)
    {

        try {

            if (!isset($id_usuario)) {
                return new \Ead1_Mensageiro("ID do usuário não informado.",
                    \Ead1_IMensageiro::ERRO, 412);
            }

            $result = $this->findBy('\G2\Entity\Cancelamento', array('id_usuario' => $id_usuario));

            return $this->toArrayEntity($result);

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param null $id_linhadenegocio
     * @return array|\Ead1_Mensageiro|\Exception
     */
    public function retornarMotivosCancelamento($id_linhadenegocio = null)
    {

        try {

            if (!isset($id_linhadenegocio)) {
                return new \Ead1_Mensageiro("ID da linha de negócio (graduação, 
                                                    pós-graduação, etc) não informado.",
                    \Ead1_IMensageiro::ERRO, 412);
            }

            $result = $this->findBy('\G2\Entity\Motivo', array('id_linhadenegocio' => $id_linhadenegocio));

            return $this->toArrayEntity($result);

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * @param $params
     * @param $id_matriculaorigem
     * @return \Ead1_Mensageiro
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarLancamentos($params, $idMatriculaorigem, $idAcordo = null)
    {
        $mensageiro = new \Ead1_Mensageiro();

        //Recupera o Contrato
        $objContratoMatricula = $this->findOneBy('\G2\Entity\ContratoMatricula',
            array('id_matricula' => $idMatriculaorigem));

        if (!$objContratoMatricula) {
            throw new \Exception('Não encontramos o contrato da matricula.');
        }

        $vendaBO = new \VendaBO;
        $negocio = new Transferencia();

        $params["venda_aditivo"]["bl_cancelamento"] = true;

        // Eu passo as matriculas iguais, pois não será uma trasnferencia e sim um cancelamento
        $id_vendaaditivo = $vendaBO->salvarAditivoTransferencia($objContratoMatricula,
            $idMatriculaorigem, //matricula origem esse parametro não é usado na outra função. Pois ele não cai na condição satisfatória para ser usado.
            $idMatriculaorigem, //matricula destino
            $params["venda_aditivo"],
            $params["id_venda"]);

        if (!$id_vendaaditivo) {
            throw new \Exception('Erro ao salvar o aditivo da venda.');
        }

        $mensageiro->setMensageiro('Venda aditivo salva com sucesso.', \Ead1_IMensageiro::TYPE_SUCESSO);

        //verifica se o valor para o aluno pagar, para gerar os lançamentos para o aluno
        if ($params["venda_aditivo"]["nu_valorapagar"] > 0) {
            //Salva os lançamentos e as  suas referencias na tb_lancamentovenda
            if (!empty($params["lancamentos"])) {
                $arrLancamentos = $params["lancamentos"];

                $mensageiro = $negocio->salvarLancamentosTransferencia(
                    $arrLancamentos, $params["id_venda"], $id_vendaaditivo, $idAcordo
                );

                if ($mensageiro->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                    throw new \Exception('Erro ao salvar os lançamentos.');
                }
            } else {
                throw new \Exception('Lançamentos não encontrados.');
            }
        }

        return $mensageiro;

    }

    /**
     * @param $where
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaLancamentos($where)
    {
        try {
            return $this->toArrayEntity($this->findBy('G2\Entity\VwLancamento', $where));
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar retornar dados de VwLancamento. " . $e->getMessage());
        }
    }


    /**
     * Recuperando o TID caso a transação tenha sido realizada com cartão de crédito
     * @param $idVenda
     * @return \G2\Entity\TransacaoFinanceira|mixed|null|string
     * @throws \Zend_Exception
     */
    public function retornaTID($idVenda)
    {
        /** @var \G2\Entity\TransacaoFinanceira $idTransacao */
        $idTransacao = null;

        $transacaoFinanceira = $this->findOneBy('\G2\Entity\VwResumoFinanceiro',
            array('id_venda' => $idVenda));

        if ($transacaoFinanceira instanceof \G2\Entity\VwResumoFinanceiro) {
            if ($transacaoFinanceira->getId_meiopagamento() == \MeioPagamentoTO::RECORRENTE) {
                $idTransacao = $transacaoFinanceira->getRecorrenteOrderid() ?
                    $transacaoFinanceira->getRecorrenteOrderid() : $idVenda . 'G2U';
            } else {
                $idTransacao = $transacaoFinanceira->getSt_codtransacaooperadora() ?
                    $transacaoFinanceira->getSt_codtransacaooperadora() : null;
            }
        }

        return $idTransacao;

    }


    /**
     * @param null $id_cancelamento
     * @return array|\Exception
     */
    public function retornarMotivosProcesso($id_cancelamento = null)
    {

        try {
            if (!isset($id_cancelamento)) {
                new \Exception("ID do cancelamento não informado.");
            }

            $motivos = new \G2\Entity\MotivoCancelamento();
            /** @var \G2\Entity\MotivoCancelamento $result */
            $result = $motivos->findBy([
                "id_cancelamento" => $id_cancelamento,
                "bl_ativo" => true
            ]);

            return $this->toArrayEntity($result);

        } catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Salva os motivos de cancelamento
     * @param array $params
     * @return array|\Exception
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function salvarMotivosPorProcesso(array $params = [])
    {

        $this->beginTransaction();
        try {

            if (empty($params["id_cancelamento"])) {
                throw new \Exception("Id cancelamento não informado.");
            }

            if (empty($params["motivos"])) {
                throw new \Exception("Motivos não informado.");
            }

            //verifica se não foi passado o usuario de cadastro
            if (empty($params['id_usuariocadastro'])) {
                $params['id_usuariocadastro'] = $this->sessao->id_usuario;
            }

            //pega a referencia do cancelamento e o usuário
            /** @var \G2\Entity\Cancelamento $cancelamento */
            $cancelamento = $this->getReference(\G2\Entity\Cancelamento::class, $params["id_cancelamento"]);
            /** @var \G2\Entity\Usuario $usuarioCadastro */
            $usuarioCadastro = $this->getReference(\G2\Entity\Usuario::class, $params['id_usuariocadastro']);

            if (!$cancelamento) {
                throw new \Exception("Cacelamento não encontrado.");
            }

            $motivoLog = [];
            //percorre os motivos
            foreach ($params['motivos'] as $key => $motivo) {
                $idMotivo = $motivo["id_motivo"];
                if (!empty($idMotivo)) {
                    /** @var MotivoCancelamento $entity */
                    $entity = $this->findOneBy(MotivoCancelamento::class, [
                        "id_motivo" => $idMotivo,
                        "id_cancelamento" => $cancelamento->getId_cancelamento()
                    ]);//procura se o motivo esta vinculado ao cancelamento

                    //se não encontrou cria uma nova instancia para o novo registro
                    if (!$entity) {
                        $entity = new MotivoCancelamento();
                    }

                    $entity->setBl_Ativo($motivo['bl_ativo'])
                        ->setId_cancelamento($cancelamento)
                        ->setId_motivo($this->getReference(\G2\Entity\Motivo::class, $idMotivo))
                        ->setId_usuariocadastro($usuarioCadastro);
                    $this->save($entity);

                    if ($entity->getId_motivo()) {
                        $string = $entity->getBl_Ativo() ? "<b>Selecionou motivo: </b>" : "<b>Removeu motivo: </b>";
                        $string .= $entity->getId_motivo()->getSt_motivo();
                        array_push($motivoLog, $string);
                    }

                }
            }
            $this->salvarLogMotivos($cancelamento, implode("<br />", $motivoLog));

            $this->commit();
            return $this->retornarMotivosProcesso($cancelamento->getId_cancelamento());

        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * @param \G2\Entity\Cancelamento $cancelamento
     * @param $stringLog
     * @return array
     * @throws \Exception
     * @throws \Zend_Exception
     */
    private function salvarLogMotivos(\G2\Entity\Cancelamento $cancelamento, $stringLog)
    {

        $this->beginTransaction();
        try {
            $arrLogs = [];

            $matriculas = $this->findBy(\G2\Entity\Matricula::class, [
                "id_cancelamento" => $cancelamento->getId_cancelamento()
            ]);

            if ($matriculas) {
                foreach ($matriculas as $matricula) {
                    /** @var \G2\Entity\LogCancelamento $log */
                    $log = $this->salvarLogCancelamento($matricula, $cancelamento, $stringLog);
                    array_push($arrLogs, $log->toBackboneArray());
                }
            }

            $this->commit();

            return $arrLogs;

        } catch (\Exception $e) {
            $this->rollback();
            throw $e;
        }
    }

    /**
     * @param $id_cancelamento
     * @return array|\Exception
     */
    public function retornarMatriculasPorCancelamento($id_cancelamento)
    {
        try {

            if (!isset($id_cancelamento)) {
                throw new \Exception("ID do cancelamento não informado.");
            }

            $result = $this->findBy('\G2\Entity\VwContratoMatriculaUsuario',
                array('id_cancelamento' => $id_cancelamento));

            return $this->toArrayEntity($result);

        } catch (\Exception $e) {
            return $e;
        }

    }

    /**
     * @param array $id_matriculas
     * @param $id_cancelamento
     * @return \Ead1_Mensageiro|\Exception
     * @throws \Zend_Exception
     */
    public function salvarCancelamentoMatriculas(array $id_matriculas, $id_cancelamento)
    {
        $this->beginTransaction();
        try {

            if (!isset($id_matriculas) || empty($id_matriculas)) {
                throw new \Exception("Nenhum ID de matrícula informado.");
            }

            foreach ($id_matriculas as $id_matricula) {
                $arrMatriculas[] = $id_matricula;
                //busca as matriculas vinculada a matricula passado por parametro
                $matriculas = $this->findBy(\G2\Entity\VwMatriculaRecursiva::class, ["id_matricula" => $id_matricula]);

                if ($matriculas) {
                    foreach ($matriculas as $matricula) {
                        if ($matricula->getNu_matriculaorigem()) {
                            $arrMatriculas[] = $matricula->getNu_matriculaorigem();
                        }

                    }
                }
                foreach ($arrMatriculas as $matricula) {
                    $entity = $this->findOneBy('\G2\Entity\Matricula', array('id_matricula' => $matricula));
                    $entity->setId_cancelamento($this->getReference("\G2\Entity\Cancelamento", $id_cancelamento));
                    $this->save($entity);

                }

            }
            $this->commit();
            return new \Ead1_Mensageiro("Sucesso",
                \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }

    }

    /**
     * Essa função é responsável por mudar a evolução da matricula e do cancelamento,
     * de acordo com as evoluçoes que for passado por parametro
     *
     * @param $id_cancelamento
     * @param $id_matricula
     * @param $evMatricula
     * @param $evCancelamento
     * @return \Ead1_Mensageiro|\Exception
     * @throws \Zend_Exception
     */
    public function alterarEvolucoesCancelamento($id_cancelamento, $id_matricula, $evMatricula, $evCancelamento)
    {
        try {
            $this->beginTransaction();

            $matricula = $this->findOneBy('G2\Entity\Matricula', array("id_matricula" => $id_matricula));
            $cancelamento = $this->findOneBy('\G2\Entity\Cancelamento', array("id_cancelamento" => $id_cancelamento));


            if (!$matricula) {
                return new \Ead1_Mensageiro("Matricula não encontrada.",
                    \Ead1_IMensageiro::ERRO, 404);
            }

            if (!$cancelamento) {
                return new \Ead1_Mensageiro("Cancelamento não encontrado.",
                    \Ead1_IMensageiro::ERRO, 404);
            }


            $matricula->setId_evolucao($this->getReference(\G2\Entity\Evolucao::class, array("id_evolucao" => $evMatricula)));
            $this->save($matricula);

            $cancelamento->setId_evolucao($this->getReference(\G2\Entity\Evolucao::class, array("id_evolucao" => $evCancelamento)));
            $cancelamento->setBl_finalizar(true);
            $this->save($cancelamento);

            $this->commit();

            $mensageiro = new \Ead1_Mensageiro();
            $mensageiro->setMensageiro("Evolução alterada com sucesso ", \Ead1_IMensageiro::TYPE_SUCESSO);

            return $mensageiro;
        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }

    }

    /**
     * Função responsavel por gerenciar o acordo das parcelas com o fluxus do lado do g2
     * Ele verifica qual o cenário das parcelas, se é apenas cancelamento simples, ou se será um acordo, com
     * cancelamento de parcelas existentes e criação de novas parcelas.
     * Após realizar este processo ele faz a sincronização com o fluxus
     *
     * @param $lancamentos
     * @param $idVenda
     * @param $status
     * @return mixed
     * @throws \Exception
     * @throws \Zend_Exception
     */
    public function cancelarLancamentosCancelamento($lancamentos, $idVenda, $status, $nuParcelasNovas)
    {
        $ws = new Integracao();
        $vwIntegracaoGestorFluxs = new TotvsVwIntegraGestorFluxus();

        $arrIdLan = [];

        //Percorre o todos os lançamentos para buscar o idlan
        foreach ($lancamentos as $lan) {
            /** @var \G2\Entity\LancamentoIntegracao $idlan */
            $idlan = $this->findOneBy('\G2\Entity\LancamentoIntegracao',
                ['id_lancamento' => $lan["id_lancamento"]]
            );

            if (!empty($idlan)) {
                array_push($arrIdLan, $idlan->getStCodlancamento());
            }
        }

        if ($status === Integracao::CANCELAMENTO) {
            $cancelamento = $ws->processoCancelamento([
                'idlan' => $arrIdLan,
                'codcoligada' => $this->getId_entidade(),
                'status' => $status
            ]);
        } else {
            $cancelamento = $ws->processoCancelamento([
                'idlan' => $arrIdLan,
                'codcoligada' => $this->getId_entidade(),
                'status' => $status,
                'primeirovencimento' => Helper::converterData($lancamentos[0]["dt_vencimento"]),
                'qtdparcelas' => $nuParcelasNovas
            ]);
        }


        if ($cancelamento->getTipo() == \Ead1_IMensageiro::ERRO) {
            throw new \Exception('Erro ao cancelar as parcelas');
        }

        //Faz a sincronização manual com o fluxus. Para não  ter que esperar o robo.
        $vwIntegracaoGestorFluxs->sincronizarDadosFluxusGestorWS(
            array(
                'contrato' => $idVenda . 'G2U',
            ));

        return $cancelamento->getFirstMensagem();
    }

    public function sincronizarFluxo($idVenda)
    {
        $vwIntegracaoGestorFluxs = new TotvsVwIntegraGestorFluxus();
        $vwIntegracaoGestorFluxs->sincronizarDadosFluxusGestorWS(
            array(
                'contrato' => $idVenda . 'G2U',
            ));
    }

    /**
     * Função responsavel por atualizar a data de referencia na tabela tb_cancelamento.
     * este campo é utilizado no relatório cancelamento pós graduação.
     * @param $data
     * @return \Exception
     * @throws \Zend_Exception
     */
    public function atualizarDataReferencia($data)
    {
        try {
            $this->beginTransaction();

            $cancelamento = $this->findOneBy(\G2\Entity\Cancelamento::class, array('id_cancelamento' => $data["id_cancelamento"]));
            $cancelamento->setDt_referencia(Helper::converterData($data["dtReferencia"]));
            $this->save($cancelamento);

            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }
    }

    /**
     * Função responsavel por atualizar a data de encaminhamento ao financeiro na tabela tb_cancelamento.
     * este campo é utilizado no relatório cancelamento pós graduação.
     *
     * @param $data
     * @return \Exception
     * @throws \Zend_Exception
     */
    public function atualizaDataEncaminharFinanceiro($data)
    {
        try {
            $this->beginTransaction();

            $cancelamentoTeste = $this->findOneBy(\G2\Entity\Cancelamento::class, array('id_cancelamento' => $data["id_cancelamento"]));
            $cancelamentoTeste->setDt_encaminhamentofinanceiro($data["dt_encaminhamentofinanceiro"]->format('Y-m-d'));
            $this->save($cancelamentoTeste);

            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }
    }

    public function atualizarDataDesistiu($data)
    {

        try {
            $this->beginTransaction();

            $cancelamento = $this->findOneBy(\G2\Entity\Cancelamento::class, array('id_cancelamento' => $data));
            $cancelamento->setDt_desistiu(Helper::converterData(new \DateTime()));
            $this->save($cancelamento);

            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }
    }

    public function atualizarBlConcluir($data)
    {

        try {
            $this->beginTransaction();

            $cancelamento = $this->findOneBy(\G2\Entity\Cancelamento::class, array('id_cancelamento' => $data));
            $cancelamento->setBl_concluir(true);
            $this->save($cancelamento);

            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }
    }

    public function atualizandoLancamentoCancelamento($datas)
    {

        try {
            $this->beginTransaction();

            foreach ($datas as $data) {

                $where = array(
                    "id_lancamento" => $data['id_lancamento']
                );
                $lancamento = $this->findOneBy(\G2\Entity\Lancamento::class, $where);
                $lancamento->setBl_cancelamento(true);
                $this->save($lancamento);
            }

            $this->commit();

        } catch (\Exception $e) {
            $this->rollback();
            return $e;
        }

    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Débora Castro
 * Date: 21/10/14
 * Time: 12:51
 */

namespace G2\Negocio;

use G2\Entity\AvaliacaoConjuntoDisciplina;

class AvaliacaoConjunto extends Negocio
{


    private $repositoryName;
    private $bo;

    public function __construct()
    {
        parent::__construct();
        $this->repositoryName = '\G2\Entity\AvaliacaoConjunto';
        $this->bo = new \AvaliacaoBO();
    }

    public function findAvaliacaoConjunto($id)
    {
        return parent::find($this->repositoryName, $id);
    }

    public function findVwAvaliacaoConjunto(array $where = NULL, array $order = NULL)
    {
        $result = $this->findBy('G2\Entity\VwAvaliacaoConjunto', $where, $order);
        return $result;
    }

    public function findByAvaliacaoConjuntoRelacao(array $where = NULL, array $order = NULL)
    {
        $result = $this->findBy('G2\Entity\VwAvaliacaoConjuntoRelacao', $where, $order);
        return $result;
    }

    public function findBySituacao(array $where = NULL, array $order = NULL)
    {
        $result = $this->findBy('G2\Entity\Situacao', $where, $order);
        return $result;
    }

    public function findAllTipoProva()
    {
        $result = $this->findAll('G2\Entity\TipoProva');
        return $result;
    }

    public function findAllTipoCalculoAvaliacao()
    {
        $result = $this->findAll('G2\Entity\TipoCalculoAvaliacao');
        return $result;
    }

    public function findAllTipoAvaliacao()
    {
        $result = $this->findAll('G2\Entity\TipoAvaliacao');
        return $result;
    }

    public function findAllTipoDisciplina()
    {
        $result = $this->findAll('G2\Entity\TipoDisciplina');
        return $result;
    }

    public function findByVwPesquisaAvaliacao(array $where = NULL, array $order = NULL)
    {
        $result = $this->findBy('G2\Entity\VwPesquisaAvaliacao', $where, $order);
        return $result;
    }

    public function findByAvaliacaoConjuntoDisciplina(array $where = NULL, array $order = NULL)
    {
        $result = $this->findBy('G2\Entity\AvaliacaoConjuntoDisciplina', $where, $order);
        return $result;
    }

    public function deletarAvaliacaoConjunto($dados)
    {

    }

    public function salvarAvaliacaoConjunto($data)
    {
        if (empty($data['id_tipocalculoavaliacao'])) {
            $data['id_tipocalculoavaliacao'] = null;
        }

        try {
            $entity = new \G2\Entity\AvaliacaoConjunto();
            if (isset($data['id_avaliacaoconjunto']) && !empty($data['id_avaliacaoconjunto'])) {
                $id_avaliacaoconjunto = $this->findAvaliacaoConjunto($data['id_avaliacaoconjunto']);
                if ($id_avaliacaoconjunto) {
                    $entity = $id_avaliacaoconjunto;
                }
            }
//\Zend_Debug::dump($entity);die;
            $entity->setSt_avaliacaoconjunto(isset($data['st_avaliacaoconjunto']) ? $data['st_avaliacaoconjunto'] : $entity->getSt_avaliacaoconjunto())
                ->setId_tipoprova(isset($data['id_tipoprova']) ? $data['id_tipoprova'] : $entity->getId_tipoprova())
                ->setId_situacao(isset($data['id_situacao']) ? $data['id_situacao'] : $entity->getId_situacao())
                ->setId_tipocalculoavaliacao(isset($data['id_tipocalculoavaliacao']) ? $data['id_tipocalculoavaliacao'] : $entity->getId_tipocalculoavaliacao())
                ->setId_usuariocadastro($this->sessao->id_usuario)
                ->setId_entidade($this->sessao->id_entidade)
                ->setDt_cadastro(new \DateTime($entity->getDt_cadastro()));

            $salvar = $this->save($entity);
            if ($salvar->getId_avaliacaoconjunto()) {
                $mensageiro = new \Ead1_Mensageiro('Conjunto Avaliação cadastrado com sucesso!', \Ead1_IMensageiro::SUCESSO);
                $mensageiro->setId($salvar->getId_avaliacaoconjunto());
                return $mensageiro;
            }
        } catch (\Exception $ex) {
            $mensageiro = new \Ead1_Mensageiro($ex->getMessage(), \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        }
    }

    public function salvarAvaliacaoConjuntoRelacao($data)
    {
        try {
            $return = $this->deletarAvaliacaoRelacao($data['id_avaliacaoconjunto']);
            if ($return->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return $return;
            } else {
                foreach ($data['ids_avaliacoes'] as $ids) {
                    $to = new \AvaliacaoConjuntoRelacaoTO();
                    $to->setId_avaliacaoconjunto((int)$data['id_avaliacaoconjunto']);
                    $to->setId_avaliacao((int)$ids);
                    $retorno = $this->bo->salvarAvaliacaoConjuntoRelacao($to);
                    if ($retorno->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                        return $retorno;
                    }
                }
                if (isset($data['id_avaliacaorecupera']) && !(empty($data['id_avaliacaorecupera']))):
                    $avaliacoesSubstitutivas = $this->vinculaProvaRecuperacao($data['id_avaliacaoconjunto'], $data['id_avaliacaorecupera']);
                    if ($avaliacoesSubstitutivas->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                        return $avaliacoesSubstitutivas;
                    }
                endif;
            }
            $mensageiro = new \Ead1_Mensageiro('Avaliações vinculadas ao conjunto com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;

        } catch (\Exception $ex) {
            $mensageiro = new \Ead1_Mensageiro($ex->getMessage(), \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        }
    }

    public function salvarAvaliacaoConjuntoDisciplina($data)
    {
        try {
            $return = $this->deletarDisciplinaRelacao($data['id_avaliacaoconjunto']);
            if ($return->getTipo() != \Ead1_IMensageiro::SUCESSO) {
                return $return;
            } else {
                foreach ($data['ids_tipodisciplina'] as $ids) {
                    $entity = new AvaliacaoConjuntoDisciplina();
                    $entity->setId_Avaliacaoconjunto($data['id_avaliacaoconjunto'])
                        ->setId_Tipodisciplina($ids);
                    $this->save($entity);
                }
            }
            $mensageiro = new \Ead1_Mensageiro('Tipo de Disciplinas vinculadas ao conjunto com sucesso!', \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;

        } catch (\Exception $ex) {
            $mensageiro = new \Ead1_Mensageiro($ex->getMessage(), \Ead1_IMensageiro::SUCESSO);
            return $mensageiro;
        }
    }

    public function deletarAvaliacaoRelacao($idca)
    {
        try {
            $ids = $this->findBy('G2\Entity\AvaliacaoConjuntoRelacao', array('id_avaliacaoconjunto' => $idca));

            if ($ids) {
                foreach ($ids as $row) {
                    $this->delete($row);

                }
            }
            $mensageiro = new \Ead1_Mensageiro('Avaliações desvinculadas com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);

        }
        return $mensageiro;
    }

    public function vinculaProvaRecuperacao($id_avaliacaoconjunto, array $ordem)
    {
        try {
        foreach ($ordem as $index => $linhas) {
            $avaliacao = explode('-', $linhas);
            $id_avaliacao = (int)$avaliacao[1];
            $id_avaliacaorecupera = (int)$avaliacao[2];

            if(empty($id_avaliacaorecupera)){
                $mensageiro = new \Ead1_Mensageiro('É necessário indicar a ordem das avaliações de recuperação!', \Ead1_IMensageiro::ERRO);
                return $mensageiro;
            }

            $ids = $this->findBy('G2\Entity\AvaliacaoConjuntoRelacao', array('id_avaliacaoconjunto' => $id_avaliacaoconjunto, 'id_avaliacao'=>$id_avaliacao));

            foreach ($ids as $entity){
                $entity->setId_Avaliacaorecupera($id_avaliacaorecupera);
                $resultado = $this->merge($entity);
            }
        }
            $mensageiro = new \Ead1_Mensageiro('Avaliações relacionadas com sucesso!', \Ead1_IMensageiro::SUCESSO);

        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);

        }
        return $mensageiro;
    }

    public function deletarDisciplinaRelacao($idca)
    {
        try {

            $ids = $this->findBy('G2\Entity\AvaliacaoConjuntoDisciplina', array('id_avaliacaoconjunto' => $idca));

        if ($ids) {
            foreach ($ids as $row) {
                $this->delete($row);

            }
        }
        $mensageiro = new \Ead1_Mensageiro('Disciplinas desvinculadas com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Zend_Exception $e) {
            $mensageiro = new \Ead1_Mensageiro($e->getMessage(), \Ead1_IMensageiro::ERRO);
        }
        return $mensageiro;
    }

    public function editarAvaliacaoConjunto($dados)
    {

    }
}

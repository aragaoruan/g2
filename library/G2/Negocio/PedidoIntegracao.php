<?php

namespace G2\Negocio;

/**
 * Classe de Negocio para PedidoIntegracao
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 * @since 2014-08-28
 */
class PedidoIntegracao extends Negocio {

    private $repositoryName = 'G2\Entity\PedidoIntegracao';

    public function __construct() {
        parent::__construct();
    }

    /**
     * Método que retorna o id_pedidointegracao, id_venda e dt_vencimento da tb_pedidointegracao
     * dos recorrentes que faltam um mês pra vencer.
     * @return array
     */
    public function getRecorrenteVencendo() {

        $sql = "SELECT * FROM vw_recorrentevencendo";

        $stmt = $this->em->getConnection()->prepare($sql);

        $stmt->execute();
        return $stmt->fetchAll();
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Tipo Area Conhecimento
 * @author DeniseXavier  <denise.xavier@unyleya.com.br>
 */
class TipoAreaConhecimento extends Negocio
{

    private $repositoryName = 'G2\Entity\TipoAreaConhecimento';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     *
     * @param integer $idTipoAreaConhecimento
     * @return G2\Entity\TipoAreaConhecimento
     */
    public function getReference ($idTipoAreaConhecimento)
    {
        return $this->em->getReference($this->repositoryName, $idTipoAreaConhecimento);
    }

    /**
     *
     * @return G2\Entity\TipoAreaConhecimento
     */
    public function findAll ()
    {
        return $this->em->getRepository($this->repositoryName)->findAll();
    }

    /**
     * Update data
     * @param array $data
     * @return G2\Entity\TipoAreaConhecimento
     */
    public function update ($data)
    {

    }

    /**
     *
     * @param array $data
     * @return \G2\Entity\TipoAreaConhecimento
     */
    public function save ($data)
    {

    }

    /**
     *
     * @param integer $id
     * @return object
     */
    public function delete ($id)
    {

    }

}
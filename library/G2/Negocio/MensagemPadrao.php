<?php

namespace G2\Negocio;

use G2\Constante\ItemConfiguracao;
use G2\Entity\Sistema;
use G2\Entity\SistemaEntidadeMensagem as SistemaEntidadeMensagem;

/**
 * Classe de negocio para funcionalidade
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2013-09-17
 */
class MensagemPadrao extends Negocio
{

    /**
     *
     * @var \G2\Entity\MensagemPadrao
     */
    private $repositoryName = 'G2\Entity\MensagemPadrao';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     *
     * @param integer $id
     * @return \G2\Entity\MensagemPadrao
     */
    public function getReference ($id, $repositoryName = NULL)
    {
        return $this->em->getReference($this->repositoryName, $id);
    }

    /**
     * Recupera contas de envio
     * @return object G2\Entity\EmailConfig
     */
    public function getContaEnvio ()
    {
        $emailConfig = new EmailConfig();
        return $emailConfig->findByEntidade();
    }

    /**
     * Find SMS Entidade Mensagem By Entidade
     *
     * Essa função mudou, de acordo com a demanda GII-9173, com a necessidade
     * de herdar as mensagens padrão das entidades mãe.
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     *
     * @return array
     * @throws \Zend_Exception
     */
    public function getSmsEntidadeMensagem()
    {
        return $this->toArrayEntity($this->herdarMensagemPadraoEntidadeMae('G2\Entity\VwSmsEntidadeMensagem',
            array('id_entidade' => $this->sessao->id_entidade)));
    }

    /**
     * Find VW Sistema Entidade Mensagem By Entidade
     *
     * Essa função mudou, de acordo com a demanda GII-9173, com a necessidade
     * de herdar as mensagens padrão das entidades mãe.
     * @author Alex Alexandre <alex.alexandre@unyleya.com.br>
     *
     * @return array
     * @throws \Zend_Exception
     */
    public function getVwSistemaEntidadeMensagem()
    {
        return $this->herdarMensagemPadraoEntidadeMae('G2\Entity\VwSistemaEntidadeMensagem',
            array('id_entidade' => $this->sessao->id_entidade));
    }

    /**
     * Update SMS Entidade Mensagem
     * @param array $data
     */
    public function updateSmsEntidadeMensagem (array $data)
    {
        $id = $data['id'];
        $id_entidade = $this->sessao->id_entidade;
        $textoCategoria = $this->getReference('G2\Entity\TextoCategoria', $data['id_textocategoria']);
        $entity = new \G2\Entity\SmsEntidadeMensagem();

        $colecao = $this->findOneBy('G2\Entity\SmsEntidadeMensagem', array('id_mensagempadrao'=>$id, 'id_entidade'=>$id_entidade));
        if($colecao){
            $entity = $colecao;
        }else{
            $entity->setDt_cadastro(new \DateTime());
            $entity->setId_mensagempadrao($this->find('\G2\Entity\MensagemPadrao', $id));
            $entity->setId_entidade($this->find('\G2\Entity\Entidade',$this->sessao->id_entidade));
            $entity->setId_textocategoria($textoCategoria);
            $entity->setBl_ativo(true);
            $entity->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $entity->setId_sistema($this->find('\G2\Entity\Sistema', 14));
        }
        $entity->setId_textosistema($this->find('G2\Entity\TextoSistema', $data['id_textosistema']));
        return $this->save($entity);
    }


    /**
     * Find Sistema Entidade Mensagem By Entidade
     * @param array $params
     * @return Object G2\Entity\SistemaEntidadeMensagem
     */
    public function getSistemaEntidadeMensagem (array $params = null)
    {
        $params['id_entidade'] = $this->sessao->id_entidade;
        return $this->findBy('G2\Entity\SistemaEntidadeMensagem', $params);
    }


    /**
     * Update Sistema Entidade Mensagem
     * @param array $data
     */
    public function updateSistemaEntidadeMensagem (array $data)
    {
        $textoSistema = $this->em->getReference('G2\Entity\TextoSistema', (int)$data['id_textosistema']);
        $entity = $this->find('G2\Entity\SistemaEntidadeMensagem', $data['id']);
        $entity->setId_textosistema($textoSistema);
        return $this->save($entity);
    }


    /**
     * Salvar Sistema Entidade Mensagem
     * @param array $data
     */
    public function saveSistemaEntidadeMensagem (array $data)
    {
        $textoSistema = $this->em->getReference('G2\Entity\TextoSistema', (int)$data['id_textosistema']);
        $mensagemPadrao = $this->em->getReference('G2\Entity\MensagemPadrao', (int)$data['id_mensagempadrao']);
//        $textoCategoria = $this->em->getReference('G2\Entity\TextoCategoria', $data['id_textocategoria']);
        $entidade =  $this->em->getReference('G2\Entity\Entidade', (int)$this->sessao->id_entidade);
        $usuario =  $this->em->getReference('G2\Entity\Usuario', (int)$this->sessao->id_usuario);
        $entity = new SistemaEntidadeMensagem();
        $entity->setId_entidade($entidade);
        $entity->setId_textosistema($textoSistema);
        $entity->setId_mensagempadrao($mensagemPadrao);
//        $entity->setId_textocategoria($textoCategoria);
        $entity->setId_usuariocadastro($usuario);
        $entity->setDt_cadastro(new \DateTime());
        $entity->setBl_ativo(1);

        return $this->save($entity);
    }

    /**
     * Função responsável por verificar se é para buscar as mensagens padrão da entidade da sessão
     * Ou da entidade mãe.
     *
     * @param $repository
     * @param $where
     * @return array
     * @throws \Zend_Exception
     */
    public function herdarMensagemPadraoEntidadeMae($repository, $where)
    {
        $entidadePai = $this->toArrayEntity($this->findOneBy('G2\Entity\Entidade',
            array('id_entidade' => $this->sessao->id_entidade)));

        $entity = $this->findoneBy('G2\Entity\EsquemaConfiguracaoItem',
            array("id_itemconfiguracao" => ItemConfiguracao::HERDAR_MSG_PADRAO_ENTIDADE_MAE,
                "id_esquemaconfiguracao" =>  $entidadePai["id_esquemaconfiguracao"]));

        if (($entidadePai["id_entidadecadastro"]["id_entidade"]) != 2 && $entity) {

            $herdarMsgPadrao = $this->toArrayEntity($entity);

            if ($herdarMsgPadrao["st_valor"] == 1) {
                $entidadePai = $this->toArrayEntity($this->findOneBy('G2\Entity\Entidade',
                    array('id_entidade' => $this->sessao->id_entidade)));
                $where['id_entidade'] = $entidadePai["id_entidadecadastro"]["id_entidade"];
                $msgPadraoEntidadePai = $this->findBy($repository, $where);

                return $msgPadraoEntidadePai;
            }
        }

        $msgPadraoEntidade = $this->findBy($repository, $where);

        return $msgPadraoEntidade;
    }

}

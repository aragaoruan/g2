<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Entity Motivo
 */
class Motivo extends Negocio
{

    private $repositoryName = 'G2\Entity\Motivo';

    public function __construct()
    {
        parent::__construct();

    }


    /**
     * Retorna Dados da Evolução de acordo com os parametros
     * @param array $where
     * @return \G2\Entity\Evolucao
     */
    public function retornarMotivos()
    {

        $params['id_entidadecadastro'] = $this->sessao->id_entidade;
        $objetoMotivoEntidade = $this->em->getRepository('\G2\Entity\MotivoEntidade')->retornarMotivosCancelamento($params);

        $array = [];
        foreach($objetoMotivoEntidade as $value){
            if($value['id_situacao'] == \G2\Constante\Situacao::TB_MOTIVO_ATIVO)
                array_push($array,$value);
        }
        return $array;
    }

    public function retornarPesquisaMotivo(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            //pega os parametros e remove algumas chaves que não precisaria
            unset($params['to'], $params['txtSearch'], $params['grid']);

            //seta o id_entidade
            $params['id_entidadecadastro'] = isset($params['id_entidadecadastro']) ? $params['id_entidadecadastro'] : $this->sessao->id_entidade;
            //busca os dados atraves de um método que esta no repository
            return $this->em->getRepository('\G2\Entity\Motivo')->retornarMotivos($params, $orderBy, $limit, $offset);

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }


    public function salvarMotivo($parametros)
    {


        try {
            $this->beginTransaction();

            $objeto = new \G2\Entity\Motivo();

            if (array_key_exists('id_motivo', $parametros) && !empty($parametros['id_motivo'])) {
                $res = $this->find('\G2\Entity\Motivo', array('id_motivo' => $parametros['id_motivo']));
                if ($res) {
                    $objeto = $res;
                }
            }

            $objeto->setSt_motivo($parametros['st_motivo']);
            $objeto->setBl_ativo(true);
            $objeto->setDt_cadastro(new \DateTime());
            $objeto->setId_entidadecadastro($this->getReference('\G2\Entity\Entidade', $this->sessao->id_entidade));
            $objeto->setId_usuariocadastro($this->getReference('\G2\Entity\Usuario', $this->sessao->id_usuario));
            $objeto->setId_situacao($this->getReference('G2\Entity\Situacao', $parametros['id_situacao']));
            $return = $this->save($objeto);

            if (!$return) {
                throw new \Exception('Erro ao salvar dados de motivo.');
            }
            if (isset($parametros['entidades_checked'])) {
                foreach ($parametros['entidades_checked'] as $value) {
                    $objetoEntidade = $this->findOneBy('\G2\Entity\MotivoEntidade', array('id_motivo' => $objeto->getId_motivo(), 'id_entidade' => $value));
                    if (!$objetoEntidade) {
                        $objetoEntidade = new \G2\Entity\MotivoEntidade();
                        $objetoEntidade->setDt_cadastro(new \DateTime());
                        $objetoEntidade->setId_entidade($this->find('\G2\Entity\Entidade', $value));
                        $objetoEntidade->setId_motivo($return);
                        $objetoEntidade->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
                    }
                    $objetoEntidade->setBl_ativo(true);
                    if (!$this->save($objetoEntidade)) {
                        throw new \Exception('Erro ao salvar dados de motivo Entidade.');
                    }
                }
            }

            if (isset($parametros['entidades_unchecked'])) {
                foreach ($parametros['entidades_unchecked'] as $value) {
                    $objetoEntidade = $this->findOneBy('\G2\Entity\MotivoEntidade', array('id_motivo' => $objeto->getId_motivo(), 'id_entidade' => $value));
                    if (!$objetoEntidade) {
                        $objetoEntidade = new \G2\Entity\MotivoEntidade();
                        $objetoEntidade->setDt_cadastro(new \DateTime());
                        $objetoEntidade->setId_entidade($this->find('\G2\Entity\Entidade', $value));
                        $objetoEntidade->setId_motivo($return);
                        $objetoEntidade->setId_usuariocadastro($this->find('\G2\Entity\Usuario', $this->sessao->id_usuario));
                    }
                    $objetoEntidade->setBl_ativo(false);
                    if (!$this->save($objetoEntidade)) {
                        throw new \Exception('Erro ao desvicular dados de motivo Entidade.');
                    }
                }
            }

            $this->commit();

            return $return;
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception('Erro ao tentar salvar dados,' . $e->getMessage());
        }

    }

    /**
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarMotivoEntidade(array $params = [])
    {
        try {
            return $this->findBy('\G2\Entity\MotivoEntidade', $params);
        } catch (\Exception $e) {
            throw new \Zend_Exception('Erro ao tentar consultar dados de Motivo Entidade. ' . $e->getMessage());
        }
    }

    public function deleteMotivo($id){
        try{
            $objeto = $this->find('\G2\Entity\Motivo', $id);
            $objeto->setBl_ativo(false);

            $motivoentidade = $this->findBy('\G2\Entity\MotivoEntidade',array('id_motivo'=> $objeto->getId_motivo()));
            if($motivoentidade){
                foreach($motivoentidade as $value){
                    $objMotivoEntidade = $this->find('\G2\Entity\MotivoEntidade', $value->getId_motivoentidade());
                    $objMotivoEntidade->setBl_ativo(false);
                    $this->save($objMotivoEntidade);
                }
            }

            return $this->save($objeto);
        }catch(\Exception $e){
            throw new \Zend_Exception('Erro ao tentar deletar dados de Motivo.' . $e->getMessage());
        }
    }
}

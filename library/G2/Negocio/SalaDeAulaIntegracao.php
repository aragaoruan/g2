<?php

namespace G2\Negocio;


use DoctrineTest\InstantiatorTestAsset\ExceptionAsset;

class SalaDeAulaIntegracao extends Negocio
{
    /** Atualiza o id_saladeaulaintegracao (sala de referência) de uma sala de aula.
     * @author - Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param $id_saladeaula - ID da Sala de Aula a ter a sala de referência atualizada.
     * @param $id_disciplinaintegracao - ID da sala de referência selecionada.
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function atualizaDisciplinaIntegracao($id_saladeaula, $id_disciplinaintegracao)
    {
        try {
            /** @var \G2\Entity\SalaDeAulaIntegracao $salaDeAulaIntegracao */
            $salaDeAulaIntegracao = $this->findOneBy('\G2\Entity\SalaDeAulaIntegracao',
                array('id_saladeaula' => $id_saladeaula));
            $this->beginTransaction();
            $salaDeAulaIntegracao->setId_disciplinaintegracao(
                $this->getReference('\G2\Entity\DisciplinaIntegracao', $id_disciplinaintegracao));
            $this->save($salaDeAulaIntegracao);
            $this->commit();
            return new \Ead1_Mensageiro('Sala de referência atualizada com sucesso!', \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception('Não foi possível atualizar a disciplina de integração da sala de aula.');
        }
    }

    /** Retorna a sala de aula integracao com base em parâmetros da tb_saladeaulaintegracao.
     * @author - Marcus Pereira <marcus.pereira@unyleya.com.br>
     * @param - parãmetros para busca da tb_saladeaulaintegracao
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaSalaDeAulaIntegracao($params)
    {
        try {
            $result = $this->findOneBy('\G2\Entity\SalaDeAulaIntegracao', $params);
            if ($result) {
                $result = $this->toArrayEntity($result);
            }
            return $result;
        } catch (\Zend_Exception $e) {
            throw new \Zend_Exception('Não foi possível buscar a Sala De Aula Integração ' . $e->getMessage());
        }
    }

}

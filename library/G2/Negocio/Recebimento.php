<?php

namespace G2\Negocio;

use G2\Repository\Lancamento;

/**
 * Classe de negócio para Recebimento
 * @author Rafael Rocha <rafael.rocha@unyleya.com.br>
 */
class Recebimento extends Negocio
{

    private $repositoryName = '';
    private $vendaBO;

    public function __construct()
    {
        set_time_limit(0);
        parent::__construct();
        $this->vendaBO = new \VendaBO();
    }

    public function retornarMeiosPagamentoDaVenda($id_venda)
    {

        if (!$id_venda) {
            throw new \Zend_Exception('Necessário informar id_venda');
        }

        $dql = 'SELECT DISTINCT vw.id_venda, vw.id_meiopagamento, vw.st_meiopagamento '
            . 'FROM G2\Entity\VwResumoFinanceiro vw '
            . 'WHERE vw.id_venda = :id_venda ORDER BY vw.id_venda, vw.id_meiopagamento';

        $query = $this->em->createQuery($dql);
        $query->setParameter('id_venda', $id_venda);

        return $query->getResult();
    }

    /**
     * Processo automatico de ativação e confirmação de venda
     * @param $id_venda
     * @throws \Exception
     * @return \Ead1_Mensageiro
     */
    public function processoAtivacaoAutomatica($id_venda)
    {
        try {
            $vendaTO = new \VendaTO();//instancia da venda TO
            $vendaNegocio = new Venda();
            $result = $vendaNegocio->retornaDadosVwVendasRecebimentoByVenda($id_venda);

            if ($result) {
                $vendaTO->setId_venda($id_venda);
                $vendaTO->setId_entidade($result->getId_entidade() ? $result->getId_entidade() : $this->sessao->id_entidade);
                $vendaTO->setId_operador((int)$this->sessao->id_usuario);
                $resultoProcessoAtivacao = $this->vendaBO->processoAtivacaoAutomatica($vendaTO);

                return $resultoProcessoAtivacao;

            } else {
                return new \Ead1_Mensageiro("Venda não pode ser confirmada, verifique se os lançamentos de entrada foram quitados.", \Ead1_IMensageiro::ERRO);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Salva a confirmação de recebimento
     * @param integer $id_venda
     * @return \Ead1_Mensageiro
     */
    public function confirmarRecebimento($id_venda)
    {
        $vendaTO = new \VendaTO();
        $vendaTO->setId_venda($id_venda);
        return $this->vendaBO->confirmaRecebimento($vendaTO);
    }

    public function ativarMatricula($id_venda)
    {
        $contratoTO = new \ContratoTO();
        $contratoTO->setId_venda($id_venda);
        $matriculaBO = new \MatriculaBO();
        return $matriculaBO->ativarMatricula($contratoTO);
    }

    public function pagarDinheiro($id_lancamento)
    {
        $lancamentoTO = new \LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);

        return $this->vendaBO->receberDinheiroLancamento($lancamentoTO);
    }

    public function pagarBoleto(\LancamentoTO $lancamentoTO)
    {
        return $this->vendaBO->quitarBoleto($lancamentoTO);
    }

    public function pagarCheque(\LancamentoTO $lancamentoTO)
    {
        return $this->vendaBO->quitarCheque($lancamentoTO);
    }

    public function pagarDeposito($id_lancamento)
    {
        $lancamentoTO = new \LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);

        return $this->vendaBO->receberDinheiroLancamento($lancamentoTO);
    }

    public function pagarCartaoDebito($arrayLancamentos)
    {
        return $this->vendaBO->quitarCredito($arrayLancamentos);
    }

    public function pagarCartaoCredito($arrayLancamentos)
    {
        return $this->vendaBO->quitarCredito($arrayLancamentos);
    }

    public function pagarEmpenho($id_lancamento)
    {
        $lancamentoTO = new \LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);

        return $this->vendaBO->receberDinheiroLancamento($lancamentoTO);
    }

    public function pagarTransferencia($id_lancamento)
    {
        $lancamentoTO = new \LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);

        return $this->vendaBO->receberDinheiroLancamento($lancamentoTO);
    }

    public function enviarBoleto($id_lancamento)
    {
        //$boletoPHP = new BoletoPHP();
        //gera o pdf do boleto
        //if ($boletoPHP->gerarPdfBoleto($id_lancamento)->getTipo() == \Ead1_IMensageiro::SUCESSO) {

            $lancamentoVendaTO = new \LancamentoVendaTO();
            $lancamentoVendaTO->setId_lancamento($id_lancamento);
            $lancamentoVendaTO->fetch(false, true, true);

            $vendaTO = new \VendaTO();
            $vendaTO->setId_venda($lancamentoVendaTO->getId_venda());
            $vendaTO->fetch(true, true, true);

        $lancamentoTO = new \LancamentoTO();
        $lancamentoTO->setId_lancamento($id_lancamento);
        $lancamentoTO->fetch(true, true, true);

        $negocio = new Negocio();
        /** @var \G2\Entity\VendaProduto $vendaProtudo */
        $vendaProtudo = $negocio->findOneBy(
            'G2\Entity\VendaProduto',
            array('id_venda' => $vendaTO->getId_venda())
        );

            $emailentidade = new \EmailEntidadeMensagemTO();
            $emailentidade->setId_entidade($vendaTO->getId_entidade());

        $id_mensagempadrao = 47;
        if (
            $vendaProtudo->getId_produto()->getId_tipoproduto()->getId_tipoproduto()
            == \G2\Constante\TipoProduto::PROJETO_PEDAGOGICO
            && empty($lancamentoTO->getId_acordo())
        ) {
            $id_mensagempadrao = 3;
        } else if (
            $vendaProtudo->getId_produto()->getId_tipoproduto()->getId_tipoproduto()
            == \G2\Constante\TipoProduto::TAXA
            && empty($lancamentoTO->getId_acordo())
        ) {
            $id_mensagempadrao = 44;
        }

        $emailentidade->setId_mensagempadrao(
            $id_mensagempadrao
        );
            $emailentidade->fetch(false, true, true);

        if (empty($emailentidade->getId_textosistema())) {
            return new \Ead1_Mensageiro(
                'Não existe um texto cadastrado para o envio do boleto.',
                \Ead1_IMensageiro::ERRO,
                null
            );
            }

            $textoSistemaTO = new \TextoSistemaTO();
            $textoSistemaTO->setId_textosistema($emailentidade->getId_textosistema());
            $textoSistemaTO->fetch(true, true, true);



            $arParametros = array(
                'id_venda' => $vendaTO->getId_venda(),
                'id_entidade' => $lancamentoTO->getId_entidade(),
                'id_lancamento' => $id_lancamento,
            );

            $emailConfigTO = new \EmailConfigTO();
            $emailConfigTO->setId_entidade($lancamentoTO->getId_entidade());
            $emailConfigTO->fetch(false, true, true);

            $mensagemBO = new \MensagemBO();

        $mensageiro = $mensagemBO->enviarNotificacaoTextoSistema(
            $textoSistemaTO,
            $arParametros,
            $emailConfigTO->getId_emailconfig(),
            $vendaTO->getId_usuario(),
            $vendaTO->getId_entidade()
        );


        if ($mensageiro->getTipo() == \Ead1_IMensageiro::SUCESSO) {
            $vwPessoaTO = new \VwPessoaTO ();
            $pessoaBO = new \PessoaBO ();

            $vwPessoaTO->setId_usuario($vendaTO->getId_usuario());
            $vwPessoaTO->setId_entidade(
                (
                is_null($vendaTO->getId_entidade()) ?
                    $vwPessoaTO->getSessao()->id_entidade :
                    $vendaTO->getId_entidade()
                )
            );

            if ($pessoa = $pessoaBO->retornarVwPessoa($vwPessoaTO)) {
                $vwPessoaTO = \Ead1_TO_Dinamico::encapsularTo($pessoa, new \VwPessoaTO());
                $vwPessoaTO = $vwPessoaTO [0];
            } else {
                throw new \Exception('Erro ao Retornar Pessoa.');
            }

            $tramiteNegocio = new Tramite();
            $tramiteNegocio->salvarTramite(
                array(
                    'st_tramite' => 'Boleto gerado e encaminhado ao e-mail ' . $vwPessoaTO->getSt_email(),
                    'id_categoriatramite' => 3,
                    'id_campo' => $vendaTO->getId_venda(),
                    'id_entidade' => $vendaTO->getId_entidade(),
                    'id_usuario' => $vendaTO->getId_atendente()
                )
            );

            /** @var \MensagemTO $mensagem */
            $mensagem = $mensageiro->getCodigo();
            //$lancamentoTO

            $negocio = new Negocio();
            /** @var \G2\Entity\TransacaoFinanceira $transacaofinanceira */
            $transacaofinanceira = $negocio->findOneBy(
                'G2\Entity\TransacaoFinanceira',
                array(
                    'st_idtransacaoexterna' => $lancamentoTO->getId_transacaoexterna(),
                    'id_venda' => $vendaTO->getId_venda()
                )
            );

            if (!empty($transacaofinanceira)) {
                $transacaofinanceira->setId_mensagem($mensagem->getId_mensagem());
                $negocio->save($transacaofinanceira);
            }

        }

            return $mensageiro;
        /*} else {
            return new \Ead1_Mensageiro('Erro ao gerar o boleto.', \Ead1_IMensageiro::ERRO, null);
        }*/
    }

    public function enviarLinkCartaoCredito($id_venda)
    {
        $vendaTO = new \VendaTO();
        $vendaTO->setId_venda($id_venda);
        $matriculaBO = new \MatriculaBO();
        return $matriculaBO->enviarEmailCartao($vendaTO);
    }

    public function enviarLinkCartaoCreditoAcordo($id_venda, $id_acordo, $id_lancamento)
    {
        $vwResumoFinanceiroTO = new \VwResumoFinanceiroTO();
        $vwResumoFinanceiroTO->setId_venda($id_venda);
        $vwResumoFinanceiroTO->setId_acordo($id_acordo);
        $vwResumoFinanceiroTO->setId_lancamento($id_lancamento);
        $matriculaBO = new \MatriculaBO();
        return $matriculaBO->enviarEmailCartaoAcordo($vwResumoFinanceiroTO);
    }

    public function cancelarTransacaoCartaoCredito($id_venda)
    {
        $vendaTO = new \VendaTO();
        $vendaTO->setId_venda($id_venda);
        $vendaTO->fetch(true, true, true);

        $transacaoTO = new \TransacaoFinanceiraTO();
        $transacaoTO->setId_venda($id_venda);
        $transacaoTO->fetch(false, true, true);

        if ($transacaoTO->getDt_cadastro() && $transacaoTO->getDt_cadastro()->isEarlier(\Zend_Date::now()->subHour(24))) {
            return new \Ead1_Mensageiro("Não é possível cancelar a transação após 24h.", \Ead1_IMensageiro::AVISO);
        }

        return $this->vendaBO->cancelarRecebimentoVenda($vendaTO);
    }

    /**
     * Método que recupera as informações do pagamento com o cartão recorrente
     * @param $id_venda
     * @throws \Exception
     * @return Array
     */
    public function recuperarInfoCartaoRecorrente($id_venda){
        $rep = $this->getRepository(\G2\Entity\Lancamento::class);
        if($rep instanceof Lancamento){
            $recorrente =  $rep->recuperarInfoCartaoRecorrente($id_venda);
        }else{
            throw new \Exception('Não foi possível recuperar o repositório');
        }

        $lancamentosIguais = Array();
        $index = 0;
        foreach ($recorrente as $key){
            if ($key['bl_entrada'] == 1 || ($key['bl_entrada'] == 0 && $key['nu_cobranca'] > 1)) {
                $lancamentosIguais[$index] [] = $key;
            } else if ($key['bl_entrada'] == 0 && $key['nu_cobranca'] == 1) {
                $index++;
                $lancamentosIguais[$index] [] = $key;
            }
        }

        return $lancamentosIguais;
    }

}

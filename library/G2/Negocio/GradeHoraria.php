<?php

namespace G2\Negocio;

use G2\Entity\Integracao\PontoSoft\TurmasAcessosSalas;


/**
 * Classe negocio para grade horaria
 * Class GradeHoraria
 * @package G2\Negocio
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 * @since 2014-12-03
 */
class GradeHoraria extends Negocio
{
    private $repositoryName = '\G2\Entity\GradeHoraria';
    private $repositoryItemName = '\G2\Entity\ItemGradeHoraria';
    private $repositoryNameItemGradeTurma = '\G2\Entity\ItemGradeHorariaTurma';
    private $repositoryNameVwControleTurma = '\G2\Entity\VwControleTurma';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Retorn pesquisa de grade horaria
     * @param array $params
     * @param null $orderBy
     * @param int $limit
     * @param int $offset
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarPesquisaGradeHoraria(array $params = array(), $orderBy = null, $limit = 0, $offset = 0)
    {
        try {
            //pega os parametros e remove algumas chaves que não precisaria
            unset($params['to'], $params['txtSearch'], $params['grid']);
            //Formata data
            if (isset($params['dt_iniciogradehoraria']) && !empty($params['dt_iniciogradehoraria'])) {
                $params['dt_iniciogradehoraria'] = $this->converteDate($params['dt_iniciogradehoraria']) . ' 00:00:00';
            }
            //Formata data
            if (isset($params['dt_fimgradehoraria']) && !empty($params['dt_fimgradehoraria'])) {
                $params['dt_fimgradehoraria'] = $this->converteDate($params['dt_fimgradehoraria']) . " 23:59:59";
            }

            //seta o id_entidade
            $params['id_entidade'] = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;

            //busca os dados atraves de um método que esta no repository
            $result = $this->em->getRepository($this->repositoryName)->retornarPesquisaGrade($params, $orderBy, $limit, $offset);

            if (isset($result->rows))
                $entities = $result->rows;
            else
                $entities = $result;

            $arrResult = array();//cria uma variavel atribuindo um array vazio nela para auxiliar

            //verifica se veio resultados
            if ($result) {
                //percorre os resultados
                foreach ($entities as $key => $row) {
                    //atribui na variavel auxiliar em determinada chave do loop os atributos para a pesquisa
                    $arrResult[$key]['id_gradehoraria'] = $row['id_gradehoraria'];
                    $arrResult[$key]['st_nomegradehoraria'] = $row['st_nomegradehoraria'];
                    $arrResult[$key]['st_situacao'] = $row['st_situacao'];

                    //monta a string para o periodo
                    $periodo = '';

                    //verifica se a data inicial é uma instancia de DateTime e monta um pedaço da string
                    if ($row['dt_iniciogradehoraria'] instanceof \DateTime) {
                        $dt_inicio = $row['dt_iniciogradehoraria'];
                        $periodo .= $dt_inicio->format('d/m/Y');
                    }

                    //verifica se a data fim é uma instancia de DateTime e monta outro pedaço da string
                    if ($row['dt_iniciogradehoraria'] instanceof \DateTime) {
                        $dt_fim = $row['dt_fimgradehoraria'];
                        $periodo .= ' a ';
                        $periodo .= $dt_fim->format('d/m/Y');
                    }

                    //atribui a string na chave do array
                    $arrResult[$key]['st_periodo'] = $periodo;
                }

                if (isset($result->rows))
                    $result->rows = $arrResult;
                else
                    $result = $arrResult;
            }
            //retorna o array auxliar
            return $result;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Retorna Grade Horaria por id
     * @param integer $id Id da grade horaria
     * @return mixed
     */
    public function findGradeHoraria($id, $array = false)
    {
        $return = $this->find($this->repositoryName, $id);

        if ($array) {
            $return = $this->toArrayEntity($return);
        }
        return $return;
    }

    /**
     * Salva ou edita dados da grade horaria
     * @param array $params array chave valor
     * @return mixed pode retornar null ou a entity
     * @throws \Zend_Exception
     */
    public function salvarGrade(array $params)
    {
        try {
            //verifica se tem dados
            if ($params) {
                //instancia a entity
                $gradeHoraria = new \G2\Entity\GradeHoraria();

                //Verifica se existe a posição no array e se ela esta preenchida
                if (array_key_exists('id_gradehoraria', $params) && !empty($params['id_gradehoraria'])) {
                    //procura os dados por id
                    $result = $this->findGradeHoraria($params['id_gradehoraria']);
                    if ($result) {
                        $gradeHoraria = $result;
                    }
                }

                //formata as datas
                $dt_inicio = isset($params['dt_iniciogradehoraria']) ? $this->converteDataBanco($params['dt_iniciogradehoraria']) : $gradeHoraria->getDt_iniciogradehoraria();
                $dt_fim = isset($params['dt_fimgradehoraria']) ? $this->converteDataBanco($params['dt_fimgradehoraria']) : $gradeHoraria->getDt_fimgradehoraria();

                //verifica data inicio e fim
                if (!$this->validaData($dt_inicio, $dt_fim)) {
                    throw new \Exception("Erro! Data de início é maior que a data final.");
                }

                //verifica data de cadastro
                if (!$gradeHoraria->getDt_cadastro()) {
                    $gradeHoraria->setDt_cadastro(new \DateTime());
                }

                //seta a entidade
                $params['id_entidade'] = isset($params['id_entidade']) && !empty($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
                if (isset($params['id_entidade'])) {
                    $entidadeNegocio = new Entidade();
                    $gradeHoraria->setId_entidade($entidadeNegocio->getReference($params['id_entidade']));
                }

                //seta a situação
                if (isset($params['id_situacao']) && !empty($params['id_situacao'])) {
                    $situacaoNegocio = new Situacao();
                    $gradeHoraria->setId_situacao($situacaoNegocio->getReference($params['id_situacao']));
                }

                //seta os demais atributos
                $gradeHoraria->setBl_ativo(isset($params['bl_ativo']) ? $params['bl_ativo'] : true)
                    ->setDt_fimgradehoraria($dt_fim)
                    ->setDt_iniciogradehoraria($dt_inicio)
                    ->setSt_nomegradehoraria(isset($params['st_nomegradehoraria']) ? $params['st_nomegradehoraria'] : $gradeHoraria->getSt_nomegradehoraria())
                    ->setBl_sincronizado(isset($params['bl_sincronizado']) ? $params['bl_sincronizado'] : false);


                //salva os dados
                return $this->save($gradeHoraria);

            } else {
                throw new \Exception("Impossível salvar Grade Horária sem dados.");
            }


        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar Grade Horária. " . $e->getMessage());
        }
    }

    /**
     * Compara a data inicio e data fim  e valida se a data fim é maior que a data inicio
     * @param \DateTime $dtInicio Data de Inicio
     * @param \DateTime $dtFim Data fim
     * @return boolean
     */
    private function validaData($dtInicio, $dtFim)
    {
        if (!empty($dtInicio) && !empty($dtFim)) {
            return $this->comparaData($dtInicio->format('Y-m-d'), $dtFim->format('Y-m-d'));
        } else {
            return true;
        }
    }

    /**
     * Retorna Item da grade pelo id
     * @param $id
     * @return mixed
     */
    public function findItemGrade($id)
    {
        return $this->find($this->repositoryItemName, $id);
    }


    /**
     * Valida informações para cadastrar itens na grade horaria
     * @param \G2\Entity\ItemGradeHoraria $entity
     * @throws \Exception
     * @return \Ead1_Mensageiro
     */
    private function validaItemGradeHoraria(\G2\Entity\ItemGradeHoraria $entity)
    {
        try {

            /*
             * Se o Local de Aula (sala) dessa unidade e turno, já possui algum Professor.
             */
//            if ($entity->getId_localaula() && $entity->getId_turno()) {
//                $result = $this->em->getRepository($this->repositoryItemName)->verificaLocalAulaUnidadeTurnoGrade(array(
//                    'id_unidade' => $entity->getId_unidade()->getId_entidade(),
//                    'id_gradehoraria' => $entity->getId_gradehoraria()->getId_gradehoraria(),
//                    'id_turno' => $entity->getId_turno()->getId_turno(),
//                    'id_localaula' => $entity->getId_localaula()->getId(),
//                    'id_diasemana' => $entity->getId_diasemana()->getId_diasemana(),
//                    'id_itemgradehoraria' => $entity->getId_itemgradehoraria()
//                ));
//                if ($result) {
//                    return new \Ead1_Mensageiro("A Sala {$result['st_localaula']} já está cadastrada com o (a) Professor (a) {$result['st_nomecompleto']} nesse turno e Unidade", \Ead1_IMensageiro::AVISO, $result);
//                }
//            }


            /*
             * Se a Turma já está tendo aula naquele Dia e Turno.
             */
            if ($entity->getId_turma() && $entity->getId_diasemana() && $entity->getId_turno() && $entity->getDt_diasemana()) {
                $result = $this->em->getRepository($this->repositoryItemName)->verificaTurmaDiaTurno(array(
                    'id_turma' => $entity->getId_turma()->getId_turma(),
                    'id_diasemana' => $entity->getId_diasemana()->getId_diasemana(),
                    'dt_diasemana' => $entity->getDt_diasemana()->format('Y-m-d'),
                    'id_turno' => $entity->getId_turno()->getId_turno(),
                    'id_unidade' => $entity->getId_unidade()->getId_entidade(),
                    'id_itemgradehoraria' => $entity->getId_itemgradehoraria(),
                    'id_gradehoraria' => $entity->getId_gradehoraria()->getId_gradehoraria(),
                    'id_turnoaula'=> $entity->getId_turnoaula()->getId_turno(),
                    'id_tipoaula'=> $entity->getId_tipoaula()->getId_tipoaula()
                ));
                if ($result) {
                    $mensagem = '';
                    foreach ($result as $key => $value) {
                        $key++;
                        $mensagem .= $key . "- A turma {$value['st_turma']} já está tendo aula da disciplina {$value['st_disciplina']} no dia {$value['st_diasemana']} no turno {$value['st_turnoaula']} na unidade {$value['st_nomeentidade']}. <br>";
                    }
                    //retorna o mensageiro
                    return new \Ead1_Mensageiro($mensagem, \Ead1_IMensageiro::AVISO, $result);
                }
            }

            /*
             * Se Já existe na grade aquele Professor, naquele Dia e Turno.
             */
            if ($entity->getId_professor() && $entity->getId_diasemana() && $entity->getId_turno()) {
                $result = $this->em->getRepository($this->repositoryItemName)->verificaProfessorDiaTurno(array(
                    'id_gradehoraria' => $entity->getId_gradehoraria()->getId_gradehoraria(),
                    'id_turno' => $entity->getId_turno()->getId_turno(),
                    'id_diasemana' => $entity->getId_diasemana()->getId_diasemana(),
                    'id_professor' => $entity->getId_professor()->getId_usuario(),
                    'id_itemgradehoraria' => $entity->getId_itemgradehoraria(),
                    'dt_inicio' => $entity->getId_gradehoraria()->getDt_iniciogradehoraria()->format('Y-m-d'),
                    'dt_fim' => $entity->getId_gradehoraria()->getDt_fimgradehoraria()->format('Y-m-d'),
                    'id_turnoaula'=> $entity->getId_turnoaula()->getId_turno(),
                    'id_tipoaula'=> $entity->getId_tipoaula()->getId_tipoaula()
                ));
                if ($result) {
                    $mensagem = '';
                    foreach ($result as $key => $value) {
                        $key++;
                        $refGradeHoraria = $value['id_gradehoraria'] != $entity->getId_gradehoraria()->getId_gradehoraria() ? "<br>Visualizar registro em: <b>{$value['st_nomegradehoraria']}</b>" : NULL;
                        $mensagem .= $key . "- O Professor(a) {$value['st_nomecompleto']} já possui horário cadastrado no dia {$value['st_diasemana']} turno {$value['st_turno']} na Unidade {$value['st_nomeentidade']}. {$refGradeHoraria} <BR>";
                    }
                    return new \Ead1_Mensageiro($mensagem, \Ead1_IMensageiro::AVISO, $result);
                }

            }


            return new \Ead1_Mensageiro("Sucesso ao validar Item Grade Horaria", \Ead1_IMensageiro::SUCESSO);

        } catch
        (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Salva dados de ItemGradeHoraria e ItemGradeHorariaTurma
     * @param array $params
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function procedimentoSalvarItemGradeHorariaComTurma(array $params)
    {
        try {
            //inicia transação.
            $this->beginTransaction();
            //cria variaveis vazias para auxiliar no retorno
            $entityItem = null;
            $arrEntityItemGradeTurma = array();
            $arrIdItensGradeTurma = array();

            //verifica se no array existe a chave itemgrade
            if (array_key_exists('itemgrade', $params) && $params['itemgrade']) {
                //chama o metodo de salvar o item da grade
                $resultItem = $this->salvarItemGradeHoraria($params['itemgrade']);

                //verifica se o retorno foi uma instancia da entity
                if ($resultItem instanceof \G2\Entity\ItemGradeHoraria) {
                    $entityItem = $resultItem;//atribui a entity a nova variavel
                    unset($resultItem);
                    /*
                     * verifica se o retorno do metodo é uma instancia do mensageiro,
                     * isso indica que houve alguma verificação retornou mensagem
                     */
                } elseif ($resultItem instanceof \Ead1_Mensageiro) {
                    $this->rollback();//vaz o rollback
                    return $resultItem; //retorna o mesageiro
                } else {// se não retornar nenhum dos acima foi alguma erro, para tudo
                    throw new \Exception('Erro ao salvar Item da Grade Horaria.');
                }
            }

            //verifica se no array que chegou ao metodo existe alguma chave arritemgradeturma
            if ($entityItem && array_key_exists('arritemgradeturma', $params) && $params['arritemgradeturma']) {
                //verifica se a chave contem alguma coisa
                if ($params['arritemgradeturma']) {
                    //percorre o array que chegou nessa chave, item por item e salva cada um
                    foreach ($params['arritemgradeturma'] as $itemGradeTurma) {
                        //atribui a chave do array o valor do item grade horaria que foi salvo antes
                        $itemGradeTurma['id_itemgradehoraria'] = $entityItem->getId_itemgradehoraria();
                        //chama o metodo de salvar passando o array que esta no loop
                        $resultItemGradeTurma = $this->salvarItemGradeHorarioTurma($itemGradeTurma);
                        //verifica se o retorno do metodo foi um mensageiro, se foi alguma validação retornou algo
                        if ($resultItemGradeTurma instanceof \Ead1_Mensageiro) {
                            $this->rollback();//faz o rollback
                            return $resultItemGradeTurma;//retorna o mensageiro
                            //verfifica se retornou uma instancia da entity
                        } elseif ($resultItemGradeTurma instanceof \G2\Entity\ItemGradeHorariaTurma) {
                            //atribui a entity a variavel auxiliar criando uma nova posição no array
                            $arrEntityItemGradeTurma[] = $resultItemGradeTurma;
                            $arrIdItensGradeTurma[] = $resultItemGradeTurma->getId_itemgradehorariaturma();
                        } else {//se o retorno não foi nenhum acima para tudo
                            throw new \Exception('Erro ao salvar Item da Grade Horaria Turma.');
                        }
                    }

                }
            }

            //Inativa os vinculos de grade com turma
            if ($entityItem->getId_itemgradehoraria()) {
                //chama o metodo passando os parametros
                $resultInativacao = $this->inativaItensGradeTurma($arrIdItensGradeTurma, $entityItem->getId_itemgradehoraria());

                //verifica se o retorno não é uma instancia do mensageiro, isso significa que deu erro, então criamos um exception
                if (!($resultInativacao instanceof \Ead1_Mensageiro)) {
                    throw new \Exception('Erro ao inativar vínculos de itens da grade com turma. ');
                }
            }

            //commit
            $this->commit();
            //retorna um mensageiro com os dados e mensagem
            return new \Ead1_Mensageiro("Dados de Item Grade Horaria e Item Grade Horaria Turma salvo com sucesso!", \Ead1_IMensageiro::SUCESSO, array(
                'itemgrade' => $entityItem,
                'arritemgradeturma' => $arrEntityItemGradeTurma
            ));

        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar dados. " . $e->getMessage());
        }
    }

    /**
     * Inativa vinculos de turma com item da grade horaria
     * @param array $arrIdItensGradeTurma array com id dos itens que não serão inativados ex.: array(1,2,...,n)
     * @param $id_itemgradehoraria id do item da grade para buscar todos os registros vinculadas
     * @throws \Zend_Exception
     * @return \Ead1_Mensageiro
     */
    public function inativaItensGradeTurma(array $arrIdItensGradeTurma, $id_itemgradehoraria)
    {
        try {
            $this->beginTransaction();
            //verifica se o id_itemgradehoraria foi passado
            if ($id_itemgradehoraria) {
                //busca todos os itens grade turma vinculados ao id_itemgradehoraria
                $resultItensTurma = $this->findBy($this->repositoryNameItemGradeTurma, array('id_itemgradehoraria' => $id_itemgradehoraria));
                //verifica se retornou algo
                if ($resultItensTurma) {

                    //percorre o resultado
                    foreach ($resultItensTurma as $itemTurma) {

                        //verifica se o item do loop atual não esta no array de id's,
                        //se ele não estiver significa que deveremos inativar o registro
                        if (!in_array($itemTurma->getId_itemgradehorariaturma(), $arrIdItensGradeTurma)) {

                            //então, setamos o bl_ativo para false e
                            $itemTurma->setBl_ativo(false);
                            //salvamos o registro
                            //como deletamos o registros de forma lógica ele continuara retornando o objeto com os novos atributos
                            $resultInativa = $this->save($itemTurma);

                            //verifica se ele não retornou uma instancia da entity, isso significa que deu algum erro
                            //então criamos um exception
                            if (!($resultInativa instanceof \G2\Entity\ItemGradeHorariaTurma)) {
                                throw new \Exception("Erro ao inativar item vinculado a grade horaria id: " . $itemTurma->getId_itemgradehorariaturma());
                            }
                        }
                    }
                }
            }
            //se os procedimentos acima não gerou nenhum exception então faz o commit da transão e retornar o mensageiro
            $this->commit();
            return new \Ead1_Mensageiro("Itens vinculados a grade horaria inativados com sucesso.", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao inativar vínculos de Item da Grade com Turma. " . $e->getMessage());
        }
    }

    /**
     * Salva o Item da Grade Horaria
     * @param array $params
     * @return mixed
     * @throws \Zend_Exception
     */
    public function salvarItemGradeHoraria(array $params, $valida = null)
    {
        try {
            if ($params) {

                $this->beginTransaction();


                //instancia a entity
                $entity = new \G2\Entity\ItemGradeHoraria();

                //verifica se o id da entity foi passado e procura o registro
                if (isset($params['id_itemgradehoraria']) && !empty($params['id_itemgradehoraria'])) {
                    $result = $this->findItemGrade($params['id_itemgradehoraria']);
                    //se encontrar o registro atribui o objeto a variavel da entity
                    if ($result) {
                        $entity = $result;
                    }
                }

                //seta o id_entidade
                if (!$entity->getId_entidadecadastro()) {
                    $params['id_entidadecadastro'] = isset($params['id_entidadecadastro']) && !empty($params['id_entidadecadastro']) ? $params['id_entidadecadastro'] : $this->sessao->id_entidade;
                    $entity->setId_entidadecadastro($this->getReference('\G2\Entity\Entidade', $params['id_entidadecadastro']));
                }

                //seta a data de cadastro
                if (!$entity->getDt_cadastro()) {
                    $params['dt_cadastro'] = isset($params['dt_cadastro']) && !empty($params['dt_cadastro']) ? $params['dt_cadastro'] : date('d/m/Y');
                    $entity->setDt_cadastro($this->converteDataBanco($params['dt_cadastro']));
                }

                //seta o bl_ativo
                if (isset($params['bl_ativo']) && !empty($params['bl_ativo'])) {
                    $params['bl_ativo'] = $params['bl_ativo'];
                } else {
                    $params['bl_ativo'] = true;
                }
                //divide a data e o turno
                $explodeDt = explode("-",$params['dt_diasemana'])[0];
                // Seta os atributos
                $entity->setBl_ativo(isset($params['bl_ativo']) ? $params['bl_ativo'] : $entity->getBl_ativo())
                    ->setId_diasemana(isset($params['id_diasemana']) ? $this->getReference('\G2\Entity\DiaSemana', $params['id_diasemana']) : $entity->getId_diasemana())
                    ->setId_disciplina(isset($params['id_disciplina']) ? $this->getReference('\G2\Entity\Disciplina', $params['id_disciplina']) : $entity->getId_disciplina())
                    ->setId_gradehoraria(isset($params['id_gradehoraria']) ? $this->getReference('\G2\Entity\GradeHoraria', $params['id_gradehoraria']) : $entity->getId_gradehoraria())
                    ->setId_unidade(isset($params['id_unidade']) ? $this->getReference('\G2\Entity\Entidade', $params['id_unidade']) : $entity->getId_unidade())
                    ->setId_turno(isset($params['id_turno']) ? $this->getReference('\G2\Entity\Turno', $params['id_turno']) : $entity->getId_turno())
                    ->setId_professor(isset($params['id_professor']) ? $this->getReference('\G2\Entity\Usuario', $params['id_professor']) : $entity->getId_professor())
                    ->setId_projetopedagogico(isset($params['id_projetopedagogico']) ? $this->getReference('\G2\Entity\ProjetoPedagogico', $params['id_projetopedagogico']) : $entity->getId_projetopedagogico())
                    ->setId_turma(isset($params['id_turma']) ? $this->getReference('\G2\Entity\Turma', $params['id_turma']) : $entity->getId_turma())
                    ->setDt_diasemana(isset($params['dt_diasemana']) ? $this->converteDataBanco($explodeDt) : $entity->getDt_diasemana())
                    ->setSt_observacao(isset($params['st_observacao']) ? $params['st_observacao'] : $entity->getSt_observacao())
                    ->setId_turnoaula(isset($params['id_turnoaula']) ? $this->find('\G2\Entity\Turno', $params['id_turnoaula']) : $entity->getId_turnoaula());

                //Por alguma razão divina, essa atribuição não funciona como nos atributos acima.
                $entity->setId_tipoaula(isset($params['id_tipoaula']) ? $this->find('\G2\Entity\TipoAula', $params['id_tipoaula']): $entity->getId_tipoaula());



                if ($valida) {
                    //valida a grade
                    $validaGrade = $this->validaItemGradeHoraria($entity);
                    if ($validaGrade->getType() == \Ead1_IMensageiro::TYPE_AVISO) {
                        $this->rollback();
                        return $validaGrade;
                    } else {
                        $validaGrade = false;
                        return $validaGrade;
                    }
                }

                //Salva o item da grade horaria
                $entity = $this->save($entity);
                if ($entity) {//verifica se salvou o item da grade horaria
                    if (!$this->salvarGrade(array(
                        'id_gradehoraria' => $entity->getId_gradehoraria()->getId_gradehoraria(),
                        'bl_sincronizado' => false
                    ))
                    ) {//salva a alteração já verificando se não salvou
                        throw new \Exception("Erro ao setar bl_sincronizado para 0 da Grade Horaria.");
                    }

                    $this->commit();//comita as alterações


                    $ordena = array(
//                        'id_gradehoraria' => ($params['id_gradehoraria']) ? $params['id_gradehoraria'] : $entity->getId_gradehoraria(),
                        'id_disciplina' => ($params['id_disciplina']) ? $params['id_disciplina'] : $entity->getId_disciplina(),
                        'id_turno' => ($params['id_turno']) ? $params['id_turno'] : $entity->getId_turno()->getId_turno(),
                        'id_turma' => ($params['id_turma']) ? $params['id_turma'] : $entity->getId_turma()->getId_turma(),
                        'bl_ativo' => 1
                    );

                    //Ordena os itens da grade horaria
                    $this->ordenaEncontrosAction($ordena);

                    $result = $this->findItemGrade($entity->getId_itemgradehoraria());
                    //se encontrar o registro atribui o objeto a variavel da entity
                    if ($result) {
                        $entity = $result;
                    }

                    return $entity;//retorna o item da grade horaria
                }

            }
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Zend_Exception("Erro ao tentar salvar Item Grade Horária. " . $e->getMessage());
        }
    }

    /**
     * Salva item grade horario turma
     * @param array $params
     * @return midex
     * @throws \Zend_Exception
     */
    public function salvarItemGradeHorarioTurma(array $params)
    {
        try {


            //instancia a entity
            $entity = new \G2\Entity\ItemGradeHorariaTurma();

            //procura pelo registro se houver alguma posição no array como a chave primaria
            if (isset($params['id_itemgradehorariaturma']) && !empty($params['id_itemgradehorariaturma'])) {
                $result = $this->find($this->repositoryNameItemGradeTurma, $params['id_itemgradehorariaturma']);
                //verifica se tem resultado e atribui o resultado na variavel da entity
                if ($result) {
                    $entity = $result;
                }

                unset($result);
            }

            //Procura se a turma já esta vinculada ao item
            if ((isset($params['id_itemgradehoraria']) && !empty($params['id_itemgradehoraria'])) && (isset($params['id_turma']) && !empty($params['id_turma']))) {
                $result = $this->findOneBy($this->repositoryNameItemGradeTurma, array(
                    'id_itemgradehoraria' => $params['id_itemgradehoraria'],
                    'id_turma' => $params['id_turma']
                ));
                //verifica se tem resultado e atribui o resultado na variavel da entity
                if ($result) {
                    $entity = $result;
                }
            }

            //seta o bl_ativo
            if (isset($params['bl_ativo'])) {
                $params['bl_ativo'] = $params['bl_ativo'];
            } else {
                $params['bl_ativo'] = true;
            }

            //seta a data de cadastro
            if (!$entity->getDt_cadastro()) {
                $params['dt_cadastro'] = isset($params['dt_cadastro']) && !empty($params['dt_cadastro']) ? $params['dt_cadastro'] : date('d/m/Y');
                $entity->setDt_cadastro($this->converteDataBanco($params['dt_cadastro']));
            }

            //seta o valores
            $entity->setBl_ativo($params['bl_ativo'] ? $params['bl_ativo'] : $entity->getBl_ativo())
                ->setId_itemgradehoraria($params['id_itemgradehoraria'] ? $this->getReference($this->repositoryItemName, $params['id_itemgradehoraria']) : $entity->getId_itemgradehoraria())
                ->setId_turma($params['id_turma'] ? $this->getReference('\G2\Entity\Turma', $params['id_turma']) : $entity->getId_turma());

            //valida os dados
            $resultValidacao = $this->validaItemGradeTurma($entity);

            //verifica se retornou uma aviso
            if ($resultValidacao->getType() == \Ead1_IMensageiro::TYPE_AVISO) {
                return $resultValidacao;
            }

            $gradeH = $this->em->getRepository('\G2\Entity\GradeHoraria')->find($entity->getId_itemgradehoraria()->getid_gradehoraria()->getId_gradehoraria());
            if ($gradeH instanceof \G2\Entity\GradeHoraria) {
                $gradeH->setbl_sincronizado(0);
                $this->em->persist($gradeH);
                $this->em->flush();
            }

            //salva
            return $this->save($entity);

        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar salvar Item Grade Horária Vinculado a Turma. " . $e->getMessage());
        }
    }

    /**
     * Valida se a Turma já está tendo aula naquele Dia e Turno.
     * @param \G2\Entity\ItemGradeHorariaTurma $entity
     * @return \Ead1_Mensageiro
     * @throws \Exception
     */
    private function validaItemGradeTurma(\G2\Entity\ItemGradeHorariaTurma $entity)
    {
        try {
            //pega o objeto de item grade horario
            $itemGradeHoraria = $entity->getId_itemgradehoraria() ? $entity->getId_itemgradehoraria() : null;

            //verifica os dados
            if ($itemGradeHoraria &&
                ($itemGradeHoraria->getId_gradehoraria() &&
                    $itemGradeHoraria->getId_turno() &&
                    $itemGradeHoraria->getId_diasemana()) &&
                $entity->getId_turma()
            ) {

                //busca os dados
                $result = $this->em->getRepository($this->repositoryNameItemGradeTurma)->verificaTurmaDiaTurno(array(
                    'id_gradehoraria' => $itemGradeHoraria->getId_gradehoraria()->getId_gradehoraria(),
                    'id_turno' => $itemGradeHoraria->getId_turno()->getId_turno(),
                    'id_diasemana' => $itemGradeHoraria->getId_diasemana()->getId_diasemana(),
                    'id_turma' => $entity->getId_turma()->getId_turma(),
                    'id_itemgradehorariaturma' => $entity->getId_itemgradehorariaturma()
                ));

                //verifica se achou dados, isso significa que os dados não são validos
                if ($result) {
                    $mensagem = '';
                    foreach ($result as $key => $value) {
                        $key++;
                        $mensagem .= $key . "- A turma {$result['st_turma']} já está tendo aula da disciplina {$result['st_disciplina']} no dia {$result['st_diasemana']} turno {$result['st_turno']} na unidade {$result['st_nomeentidade']}. <br>";
                    }
                    //retorna o mensageiro
                    return new \Ead1_Mensageiro($mensagem, \Ead1_IMensageiro::AVISO, $result);

                }

            }
            return new \Ead1_Mensageiro("Sucesso ao validar Item Grade Horaria Turma", \Ead1_IMensageiro::SUCESSO);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna dados da lista de itens da grade horaria
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornaListaItensGrade(array $params = array())
    {
        try {
            $params['bl_ativo'] = isset($params['bl_ativo']) ? $params['bl_ativo'] : true;

            //busca dados
            $result = $this->findBy($this->repositoryItemName, $params, array('dt_diasemana' => 'asc'));

            //cria variavel para armazenar os arrays
            $arrReturn = array();
            if ($result) {//verifica se retornou
                //percorre o registro
                foreach ($result as $key => $item) {

                    //monta o array de retorno
                    $flag = false;
                    foreach ($arrReturn as $k => $value) {
                        if ($item->getDt_diasemana()->format('d/m/Y') == $value['dt_diasemana']
                            && $item->getId_projetopedagogico()->getId_projetopedagogico() != $value['id_projetopedagogico']
                            && $item->getId_turno()->getId_turno() == $value['id_turno']
                        ) {
                            $arrReturn[$k]['st_projetopedagogico'] = $arrReturn[$k]['st_projetopedagogico'] . ' / ' . $item->getId_projetopedagogico()->getSt_projetopedagogico();
                            if ($item->getId_projetopedagogico()->getSt_apelido()) {
                                $arrReturn[$k]['st_apelidoprojetopedagogico'] = $arrReturn[$k]['st_apelidoprojetopedagogico'] . ' / ' . $item->getId_projetopedagogico()->getSt_apelido();
                            }
                            $flag = true;
                        }
                    }
                    if (!$flag) {


                        //constrói o dia da semana, se for sabado ou domingo monta com o turno, se não monta sem.

                        if($item->getId_diasemana()){
                            if($item->getId_diasemana()->getId_diasemana() == 6 || $item->getId_diasemana()->getId_diasemana() == 7){
                                $dt_diasemana = '('. $item->getId_turnoaula()->getSt_turno().') - '.$item->getDt_diasemana()->format('d/m/Y');
                                $dt_diasemanaTirinha = $item->getId_turnoaula()->getSt_turno();
                            }else{
                                $dt_diasemana = $item->getDt_diasemana()->format('d/m/Y');
                                $dt_diasemanaTirinha = null;
                            }
                        }else{
                            $dt_diasemana = null;
                            $dt_diasemanaTirinha = null;
                        }

                        $arrReturn[$key] = array(
                            'id' => $item->getId_itemgradehoraria(),
                            'id_gradehoraria' => $item->getId_gradehoraria() ? $item->getId_gradehoraria()->getId_gradehoraria() : null,
                            'id_unidade' => $item->getId_unidade() ? $item->getId_unidade()->getId_entidade() : null,
                            'st_unidade' => $item->getId_unidade() ? $item->getId_unidade()->getSt_nomeentidade() : null,
                            'st_apelido' => $item->getId_unidade() ? $item->getId_unidade()->getSt_apelido() : null,
                            'id_turno' => $item->getId_turno() ? $item->getId_turno()->getId_turno() : null,
                            'st_turno' => $item->getId_turno() ? $item->getId_turno()->getSt_turno() : null,
                            'id_turma' => $item->getId_turma() ? $item->getId_turma()->getId_turma() : null,
                            'st_turma' => $item->getId_turma() ? $item->getId_turma()->getSt_turma() : null,
                            'id_projetopedagogico' => $item->getId_projetopedagogico() ? $item->getId_projetopedagogico()->getId_projetopedagogico() : null,
                            'st_projetopedagogico' => $item->getId_projetopedagogico() ? $item->getId_projetopedagogico()->getSt_projetopedagogico() : null,
                            'st_apelidoprojetopedagogico' => $item->getId_projetopedagogico() ? $item->getId_projetopedagogico()->getSt_apelido() : null,
                            'id_diasemana' => $item->getId_diasemana() ? $item->getId_diasemana()->getId_diasemana() : null,
                            'st_diasemana' => $item->getId_diasemana() ? $item->getId_diasemana()->getSt_diasemana() : null,
                            'dt_diasemana' => $dt_diasemana,
                            'id_professor' => $item->getId_professor() ? $item->getId_professor()->getId_usuario() : null,
                            'st_professor' => $item->getId_professor() ? $item->getId_professor()->getSt_nomecompleto() : null,
                            'id_disciplina' => $item->getId_disciplina() ? $item->getId_disciplina()->getId_disciplina() : null,
                            'st_disciplina' => $item->getId_disciplina() ? $item->getId_disciplina()->getSt_disciplina() : null,
                            'st_apelidodisciplina' => $item->getId_disciplina() ? $item->getId_disciplina()->getSt_apelido() : null,
                            'st_observacao' => $item->getSt_observacao(),
                            'nu_encontro' => $item->getNu_encontro(),
                            'dt_diasemanaTirinha' => $dt_diasemanaTirinha,
                            'st_turnoaula'=>$item->getId_turnoaula() ? $item->getId_turnoaula()->getSt_turno():null,
                            'id_turnoaula'=>$item->getId_turnoaula() ? $item->getId_turnoaula()->getId_turno():null,
                            'id_tipoaula' => $item->getId_tipoaula() ? $item->getId_tipoaula()->getId_tipoaula():null,
                            'st_tipoaula' => $item->getId_tipoaula() ? $item->getId_tipoaula()->getSt_tipoaula():null
                        );

                        //Busca os dados da carga horaria do encontro
                        if ($arrReturn[$key]['id_unidade'] && $item->getId_disciplina()->getId_disciplina() && $item->getId_projetopedagogico()->getId_projetopedagogico()) {
                            $configEntidade = $this->retornarCalculoCarcaHorariaEncontro($arrReturn[$key]['id_unidade'], $item->getId_disciplina()->getId_disciplina(), $item->getId_projetopedagogico()->getId_projetopedagogico());
                            $arrReturn[$key]['nu_totalencontros'] = $configEntidade['nu_totalencontros'];//cria uma posição no array com a quantidade de encontros
                            $arrReturn[$key]['st_corentidade'] = $configEntidade['st_corentidade'];//cria uma posição no array com a cor da entidade
                        }
                    }

                }
            }
            unset($result);//apaga a variavel do resultado
            sort($arrReturn);
            return $arrReturn;//retorna o array montado

        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna o calculo do total de encontros baseado na carga horaria do encontro cadastrado na entidade
     * e na carga horaria da disciplina
     * @param integer $idEntidade
     * @param int $idDisciplina
     * @param int $idProjeto
     * @return array
     * @throws \Exception
     */
    private function retornarCalculoCarcaHorariaEncontro($idEntidade, $idDisciplina, $idProjeto)
    {
        try {
            $arrReturn = array();
            //Busca os dados da carga horaria do encontro
            $confEntidadeNegocio = new ConfiguracaoEntidade();
            $configEntidade = $confEntidadeNegocio->findConfiguracaoEntidade($idEntidade);

            if ($configEntidade) {

                //busca a carga horaria
                $esquemaNegocio = new EsquemaConfiguracao();
                $cargaHorariaEncontro = $esquemaNegocio->retornaConfiguracaoCargaHorariaEncontro();

                //verifica se não foi passado o id_disciplina e projeto, sem eles não é possivel buscar a carga horaria
                if (!$idDisciplina && !$idProjeto) {
                    throw new \Exception("Código da disciplina e do projeto não encontrado.");
                }

                //faz uma consulta no modulo disciplina procurando a carga horaria da disciplina no projeto
                $resultCargaHorariaDisciplina = $this->em->getRepository('\G2\Entity\ModuloDisciplina')
                    ->retornaCargaHorariaDisciplinaNoProjeto($idDisciplina, $idProjeto);

                //pega o resultado e força como inteiro
                $cargaHorariaDisciplina = (int)$resultCargaHorariaDisciplina['nu_cargahoraria'];

                //calcula a quantidade de encontros
                $nuEncontros = floor($cargaHorariaDisciplina / $cargaHorariaEncontro);
                $arrReturn['nu_totalencontros'] = $nuEncontros;//cria uma posição no array com a quantidade de encontros
                $arrReturn['st_corentidade'] = $configEntidade['st_corentidade'];//cria uma posição no array com a cor da entidade
            }
            return $arrReturn;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao calcular carga horária do encontro. " . $e->getMessage());
        }
    }

    /**
     * Retorn itens da grade horaria
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function retornarItensGradeHoraria(array $params)
    {
        try {
            return $this->findBy($this->repositoryItemName, $params);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna Itens da grade vinculadas com a turma
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public function retornarItemGradeHorariaTurma(array $params = array())
    {
        try {
            return $this->findBy($this->repositoryNameItemGradeTurma, $params);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Retorna dados da grade horaria baseado na matricula do aluno
     * @param $id_matricula
     * @throws \Exception
     * @return array
     */
    public function retornarGradeHorariaByMatricula($id_matricula)
    {
        try {
            //consulta os dados pelo id_matricula
            $result = $this->findBy('\G2\Entity\VwGradeHorariaMatricula', array('id_matricula' => $id_matricula));
            $arrReturn = array();//cria uma variavel para armazenar o array

            //verifica se retornou dado
            if ($result) {

                //percorre os dados para separar
                foreach ($result as $row) {
                    //atribui esses valores as variaveis para serem reutilizados
                    $id_gradehoraria = $row->getId_gradehoraria();
                    $id_itemgradehoraria = $row->getId_itemgradehoraria();

                    //verifica se o id_gradehoraria não existe no array para retornar
                    if (!array_key_exists($id_gradehoraria, $arrReturn)) {

                        //se não existe ele cria uma posição no array com o index igual o id da grade horaria
                        $arrReturn[$id_gradehoraria] = array(
                            'id_gradehoraria' => $id_gradehoraria,
                            'st_nomegradehoraria' => $row->getSt_nomegradehoraria(),
                            'dt_iniciogradehoraria' => new \DateTime($row->getDt_iniciogradehoraria()),
                            'dt_fimgradehoraria' => new \DateTime($row->getDt_fimgradehoraria()),
                            'id_matricula' => $row->getId_matricula(),
                            'dt_cadastro' => $row->getDt_cadastro(),
                            'dt_atualizacao' => $row->getDt_atualizacao()
                        );
                    }

                    //cria outras posições no array com a chave igual ao id da grade horaria
                    //cria uma nova posição chamada itens_grade horaria e o seu index será o id_itemgradehoraria
                    $arrReturn[$id_gradehoraria]['itens_grade'][$id_itemgradehoraria] = array(
                        'id_itemgradehoraria' => $id_itemgradehoraria,
                        'nu_quantidadeaula' => $row->getNu_totalencontros(),
                        'nu_numeroaula' => $row->getNu_encontros(),
                        'id_unidade' => $row->getId_unidade(),
                        'st_unidade' => $row->getSt_nomeunidade(),
                        'id_turno' => $row->getId_turno(),
                        'st_turno' => $row->getSt_turno(),
                        'id_diasemana' => $row->getId_diasemana(),
                        'st_diasemana' => $row->getSt_diasemana(),
                        'dt_diasemana' => $row->getDt_diasemana(),
                        'id_disciplina' => $row->getId_disciplina(),
                        'st_disciplina' => $row->getSt_disciplina(),
                        'id_professor' => $row->getId_professor(),
                        'st_professor' => $row->getSt_nomeprofessor(),
                        'id_turma' => $row->getId_turma(),
                        'st_turma' => $row->getSt_turma(),
                        'id_projetopedagogico' => $row->getId_projetoPedagogico(),
                        'st_projetopedagogico' => $row->getSt_projetopedagogico(),
                        'nu_encontro' => $row->getNu_encontro()
                    );


                }

            }
            unset($result);
            //retorna os dados
            return $arrReturn;

        } catch (\Exception $e) {
            throw new \Zend_Exception("ERRO ao consultar dados da grade horaria por matricula. " . $e->getMessage());
        }
    }

    /**
     * Retorna Lista de Professores vinculados a uma grade horaria
     * @param integer $id_gradehoraria
     * @throws \Zend_Exception
     * @return array
     */
    public function retornarProfessoresItensGradeByGrade($arrayParams)
    {
        try {

            if (
                isset($arrayParams['dt_inicial']) && !empty($arrayParams['dt_inicial']) &&
                isset($arrayParams['dt_termino']) && !empty($arrayParams['dt_termino'])
            ) {
                //Converte o formato da data para pesquisa no banco
                $arrayParams['dt_inicial'] = $this->converteDataBanco($arrayParams['dt_inicial']);
                $arrayParams['dt_termino'] = $this->converteDataBanco($arrayParams['dt_termino']);
            }
            $result = $this->em->getRepository($this->repositoryItemName)->retornarProfessoresItens($arrayParams);

            foreach ($result as &$value) {
                $periodo = '';
                //verifica se a data inicial é uma instancia de DateTime e monta um pedaço da string
                if ($value['dt_iniciogradehoraria'] instanceof \DateTime) {
                    $dt_inicio = $value['dt_iniciogradehoraria'];
                    $periodo .= $dt_inicio->format('d/m/Y');
                }

                //verifica se a data fim é uma instancia de DateTime e monta outro pedaço da string
                if ($value['dt_fimgradehoraria'] instanceof \DateTime) {
                    $dt_fim = $value['dt_fimgradehoraria'];
                    $periodo .= ' a ';
                    $periodo .= $dt_fim->format('d/m/Y');
                }
                $value['periodo'] = $periodo;
            }
            return $result;
        } catch
        (\Exception $e) {
            throw new \Zend_Exception("ERRO ao consultar professores vinculados a grade horaria. " . $e->getMessage());
        }
    }

    /**
     * Faz exclusão lógia do item da grade horaria e pesquisa re-organizando a quantidade de encontros/itens que existem
     * @param $id
     * @param $id_gradehoraria
     * @param $id_disciplina
     * @return bool|string
     * @throws \Zend_Exception
     */
    public function deletarItemGradeHoraria($id, $id_gradehoraria, $id_disciplina)
    {
        try {
            //procura o registro
            $item = $this->find($this->repositoryItemName, $id);
            //verifica se retornou algo
            if ($item) {
                //seta o atributo ativo para false
                $item->setBl_ativo(false);
                //salva
                if ($this->save($item)) {
                    //seta o bl_sincronizado para false
                    if (!$this->salvarGrade(array(
                        'id_gradehoraria' => $item->getId_gradehoraria()->getId_gradehoraria(),
                        'bl_sincronizado' => false
                    ))
                    ) {//salva a alteração já verificando se não salvou
                        throw new \Exception("Erro ao setar bl_sincronizado para 0 da Grade Horaria.");
                    }

                    //monta o array de pesquisa para organizar os dados
                    $dados = array(
//                        'id_gradehoraria' => $id_gradehoraria,
                        'id_disciplina' => $id_disciplina,
                        'id_turno' => $item->getId_turno()->getId_turno(),
                        'id_turma' => $item->getId_turma()->getId_turma(),
                        'bl_ativo' => 1
                    );

                    $this->ordenaEncontrosAction($dados);

                    return true;
                } else {
                    return false;
                }
            } else {
                return "Registro não encontrado.";
            }
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar deletar item da grade horaria. " . $e->getMessage());
        }
    }

    /**
     * Retorna os dados para montar a grade horaria dos professores
     * @param $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornaDadosAgendaProfessor($params)
    {
        try {
            $id_entidade = isset($params['id_entidade']) ? $params['id_entidade'] : $this->sessao->id_entidade;
            //instancia das negocios
            $horarioAulaNegocio = new HorarioAula();
            $diasSemanaNegocio = new DiaSemana();
            $turnoNegocio = new Turno();
            $pessoaNegocio = new Pessoa();
            $entidadeNegocio = new Entidade();

            //busca os dados padrões
            $entidade = $entidadeNegocio->findEntidade($id_entidade, true);
            $diasSemana = $diasSemanaNegocio->findAll();
            $turnos = $turnoNegocio->findAll('', false);
            $professor = $pessoaNegocio->retornaPessoaById(array('id_usuario' => $params['id_professor'], 'id_entidade' => $id_entidade), true);
            if (isset($params['id_gradehoraria']) && !empty($params['id_gradehoraria'])) {
                $gradeHoraria = $this->findGradeHoraria($params['id_gradehoraria']);
            }
            //organiza os turnos
            $arrTurnoHorario = array();
            if ($turnos) {
                foreach ($turnos as $key => $turno) {
                    if ($turno->getId_turno() != 4) {
                        $arrTurnoHorario[$key] = array(
                            'id_turno' => $turno->getId_turno(),
                            'st_turno' => $turno->getSt_turno(),
                            'hr_inicio' => null,
                            'hr_fim' => null,
                        );

                        $resultHorarioAula = $horarioAulaNegocio->retornarHorarioAulaByTurno($turno->getId_turno(), $id_entidade);
                        if ($resultHorarioAula) {
                            $hr_inicio = new \DateTime($resultHorarioAula->getHr_inicio());
                            $hr_fim = new \DateTime($resultHorarioAula->getHr_fim());
                            $arrTurnoHorario[$key]['hr_inicio'] = $hr_inicio->format('H:i:s');
                            $arrTurnoHorario[$key]['hr_fim'] = $hr_fim->format('H:i:s');
                            $arrTurnoHorario[$key]['tipoaula'] = $resultHorarioAula->getId_tipoaula()?$resultHorarioAula->getId_tipoaula()->getSt_tipoaula():null;
                            $arrTurnoHorario[$key]['turno'] = $resultHorarioAula->getId_turno()?$resultHorarioAula->getId_turno()->getSt_turno():null;
                            unset($hr_inicio, $hr_fim);
                        }
                        unset($resultHorarioAula);
                    }
                }
            }
            $result = array();
            $result['professor'] = $professor;
            $result['gradeHoraria'] = isset($gradeHoraria) ? $gradeHoraria : null;
            $result['diasSemana'] = $diasSemana;
            $result['turnos'] = $arrTurnoHorario;
            $result['params'] = $params;
            $result['entidade'] = $entidade;

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao retornar dados da agenda do professor. " . $e->getMessage());
        }
    }

    /**
     * Envia email com dados da grade
     * @param string $html
     * @param array $professor
     * @param bool $enviarAgora
     * @return \Ead1_Mensageiro
     * @throws \Zend_Exception
     */
    public function enviaEmailGradeProfessor($html, array $professor, $enviarAgora = false)
    {
        try {

            //instancia as classes
            $bo = new \MensagemBO();
            $envioMensagemTO = new \EnvioMensagemTO();
            $mensagemTO = new \MensagemTO();
            $envioDestinatarioTO = new \EnvioDestinatarioTO();
            $emailConfigNegocio = new EmailConfig();
            $pessoaNegocio = new Pessoa();

            //busca os dados de pessoa
            $pessoa = $pessoaNegocio->retornaVwPessoaOne(array('id_entidade' => $professor['id_entidade'], 'id_usuario' => $professor['id_usuario']));
            //verifica se encontrou a pessoa
            if ($pessoa->getType() != \Ead1_IMensageiro::TYPE_SUCESSO) {
                throw new \Exception("Dados do professor não encontrado.");
            } else {
                $pessoa = $pessoa->getFirstMensagem();
                $envioDestinatarioTO->setId_usuario($pessoa->getid_usuario());
                $envioDestinatarioTO->setId_tipodestinatario(1);
                $envioDestinatarioTO->setSt_nome($pessoa->getst_nomecompleto());
                $envioDestinatarioTO->setSt_endereco($pessoa->getst_email());
            }

            //verifica se o texto do email foi passado
            if ($html) {
                $mensagemTO->setSt_mensagem('Grade Horaria');
                $mensagemTO->setSt_texto($html);
                $mensagemTO->setId_entidade($this->sessao->id_entidade);
                $mensagemTO->setId_usuariocadastro($this->sessao->id_usuario);
                $mensagemTO->setBl_importante(true);
            }

            //busca os dados de configuação de email da entidade
            $resultEmailConfig = $emailConfigNegocio->findByEntidade();


            //verifica se os dados de configuração de email foi passado
            if ($resultEmailConfig) {
                $resultEmailConfig = $resultEmailConfig[0];
                $envioMensagemTO->setId_emailconfig($resultEmailConfig->getId_emailconfig());
                $envioMensagemTO->setId_tipoenvio(\G2\Constante\TipoEnvio::EMAIL);
                $envioMensagemTO->setId_evolucao(\G2\Constante\Evolucao::TB_ENVIOMENSAGEM_NAO_ENVIADO);
            } else {
                throw new \Exception("E-mail config não localizado.");
            }
            //envia
            return $bo->procedimentoCadastrarMensagemEnvioDestinatarios($envioMensagemTO, $mensagemTO, array($envioDestinatarioTO), $enviarAgora);
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar enviar mensagem professor. " . $e->getMessage());
        }
    }

    /**
     * Retorna projetos por entidade
     * @param $id_entidade
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarProjetosRelatorioGrade($id_entidade)
    {
        try {
            //pega o repositorio da vw_turmaprojetopedagogico e chama o método
            $result = $this->em->getRepository('\G2\Entity\VwTurmaProjetoPedagogico')
                ->retornaTurmaProjetoPedagogicoEntidadeRecursiva(array($id_entidade));

            return $result;
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao tentar buscar projetos." . $e->getMessage());
        }

    }

    /**
     * Consulta, monta e retorna os para
     * @param array $params
     * @return array
     * @throws \Zend_Exception
     */
    public function retornarDadosWSGradeHoraria(array $params)
    {
        try {

            $result = $this->retornarItensGradeHoraria(array('id_turma' => $params['id_turma'], 'bl_ativo' => true));
            //busca os dados baseado na turma
//            $result = $this->retornarItemGradeHorariaTurma(array('id_turma' => $params['id_turma'], 'bl_ativo' => true));
            $arrReturn = array();//cria um array vazio

            //verifica se retornou dados
            if ($result) {
                //percorre os dados
                foreach ($result as $key => $row) {
                    //sepera os objetos atribuindo nas variaveis para recuperar os valores abaixo
                    $turma = $row->getId_turma();
                    $gradeHoraria = $row->getId_gradehoraria();


                    //verifica se todos os objetos tem valor e estão com bl_ativo true
                    if (($turma && $turma->getBl_ativo())
                        && ($gradeHoraria && $gradeHoraria->getBl_ativo())
                    ) {

                        //turma
                        $arrReturn[$turma->getId_turma()]['id_turma'] = $turma->getId_turma();
                        $arrReturn[$turma->getId_turma()]['st_turma'] = $turma->getSt_turma();

                        //grade horaria
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['id_gradehoraria'] = $gradeHoraria->getId_gradehoraria();
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['st_gradehoraria'] = $gradeHoraria->getSt_nomegradehoraria();
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['dt_iniciogradehoraria'] = $gradeHoraria->getDt_iniciogradehoraria()->format('Y-m-d');
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['dt_fimgradehoraria'] = $gradeHoraria->getDt_fimgradehoraria()->format('Y-m-d');
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['dt_cadastro'] = $gradeHoraria->getDt_cadastro();


                        //item da grade
                        $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['item_grade_horaria'][$row->getId_itemgradehoraria()] = array(
                            'id_itemgradehoraria' => $row->getId_itemgradehoraria(),
                            'nu_numeroaula' => $row->getNu_encontro(),
                            'nu_quantidadeaulas' => null,
                            'id_unidade' => $row->getId_unidade() ? $row->getId_unidade()->getId_entidade() : NULL,
                            'st_unidade' => $row->getId_unidade() ? $row->getId_unidade()->getSt_nomeentidade() : NULL,
                            'id_turno' => $row->getId_turno() ? $row->getId_turno()->getId_turno() : NULL,
                            'st_turno' => $row->getId_turno() ? $row->getId_turno()->getSt_turno() : NULL,
                            'id_diasemana' => $row->getId_diasemana() ? $row->getId_diasemana()->getId_diasemana() : NULL,
                            'st_diasemana' => $row->getId_diasemana() ? $row->getId_diasemana()->getSt_diasemana() : NULL,
                            'dt_diasemana' => $row->getId_diasemana() ? $row->getDt_diasemana() : NULL,
                            'id_disciplina' => $row->getId_disciplina() ? $row->getId_disciplina()->getId_disciplina() : NULL,
                            'st_disciplina' => $row->getId_disciplina() ? $row->getId_disciplina()->getSt_disciplina() : NULL,
                            'id_professor' => $row->getId_professor() ? $row->getId_professor()->getId_usuario() : NULL,
                            'st_professor' => $row->getId_professor() ? $row->getId_professor()->getSt_nomecompleto() : NULL,
                            'id_projetopedagogico' => $row->getId_projetopedagogico() ? $row->getId_projetopedagogico()->getId_projetopedagogico() : NULL,
                            'st_projetopedagogico' => $row->getId_projetopedagogico() ? $row->getId_projetopedagogico()->getSt_projetopedagogico() : NULL,
                            'st_observacao' => $row->getSt_observacao()
                        );

                        //Busca os dados da carga horaria do encontro
                        if ($row->getId_unidade()->getId_entidade() && $row->getId_disciplina()->getNu_cargahoraria()) {
                            $configEntidade = $this->retornarCalculoCarcaHorariaEncontro($row->getId_unidade()->getId_entidade(), $row->getId_disciplina()->getNu_cargahoraria(), $row->getId_projetopedagogico()->getId_projetopedagogico());
                            $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['item_grade_horaria'][$row->getId_itemgradehoraria()]['nu_quantidadeaulas'] = $configEntidade['nu_totalencontros'];//cria uma posição no array com a quantidade de encontros
                            $arrReturn[$turma->getId_turma()]['grade_horaria'][$gradeHoraria->getId_gradehoraria()]['item_grade_horaria'][$row->getId_itemgradehoraria()]['st_corentidade'] = $configEntidade['st_corentidade'];//cria uma posição no array com a cor da entidade
                        }
                    }
                }

            }
            return $arrReturn;//retorna os dados
        } catch (\Exception $e) {
            throw new \Zend_Exception("Erro ao retornar dados para WS. " . $e->getMessage());
        }
    }

    /**
     * Retorna uma array com as datas entre o intervalo
     * @param \DateTime $dt_inicio
     * @param \DateTime $dt_fim
     * @return array
     */
    private function retornaDatasDeUmIntervalo(\DateTime $dt_inicio, \DateTime $dt_fim)
    {
        $intervalo = $dt_inicio->diff($dt_fim);
        $arrDatas = array();
        for ($i = 0; $i <= $intervalo->days; $i++) {
            $dt_current = new \Zend_Date($dt_inicio->format('Y-m-d'), null, 'pt-br');
            $arrDatas[$i] = $dt_current->addDay($i);
        }
        return $arrDatas;
    }


    /**
     * Retorna dados para grade horaria
     * @param array $params
     * @return array
     * @throws \Exception
     */
    public function retornarDadosRelatorioGradeHoraria(array $params)
    {
        //verifica se tem parametros
        if ($params) {
            //percorre os parametros
            foreach ($params as $key => $value) {
                if (!$value) {//verifica se não tem valor e remove
                    unset($params[$key]);
                } else {//se tiver valor
                    if (substr($key, 0, 3) == 'dt_') {//verifica se é data e trata a data
                        $params[$key] = $this->converteDate($value);
                    }
                }
            }
        }
        //pega o repositório e chama o método
        $result = $this->em->getRepository($this->repositoryItemName)->retornarDadosRelatorioGrade($params);

        $arrReturn = array();
        if ($result) {
            foreach ($result as $key => $row) {

                //monta os dados da grade
                $idGradeHoraria = $row['id_gradehoraria'];
                $idProjeto = $row['id_projetopedagogico'];

                //cria as posições no array para o cabeçalho
                if (!isset($arrReturn[$idGradeHoraria]['cabecalho']['id_gradehoraria'])) {
                    $arrReturn[$idGradeHoraria]['cabecalho'] = array(
                        'id_gradehoraria' => $row['id_gradehoraria'],
                        'dt_iniciograde' => $row['dt_iniciogradehoraria'],
                        'dt_fimgrade' => $row['dt_fimgradehoraria'],
                        'id_projeto' => $row['id_projetopedagogico'],
                        'st_projeto' => $row['st_projetopedagogico'],
                        'st_urlimglogo' => $row['st_urlimglogo'],
                        'entidade' => array($row['id_entidade'] => $row['st_nomeentidade']),
                    );
                }

//                \Zend_Debug::dump($arrReturn[$idGradeHoraria]['cabecalho']['entidade']);
                if (!array_key_exists($row['id_entidade'], $arrReturn[$idGradeHoraria]['cabecalho']['entidade'])) {
                    $arrReturn[$idGradeHoraria]['cabecalho']['entidade'][$row['id_entidade']] = $row['st_nomeentidade'];
                }

                //cria as posições no array para as turmas
                if (!isset($arrReturn[$idGradeHoraria]['turmas'][$row['id_turma']])) {
                    $arrReturn[$idGradeHoraria]['turmas'][$row['id_turma']] = array(
                        'id_turma' => $row['id_turma'],
                        'st_turma' => $row['st_turma'],
                        'dt_inicioturma' => $row['dt_inicioturma'],
                        'dt_fimturma' => $row['dt_fimturma'],
                        'st_codigoturma' => $row['st_codigoturma']
                    );
                }

                //Calcula a carga horaria do encontro
                $configEntidade = $this->retornarCalculoCarcaHorariaEncontro($row['id_entidade'], $row['id_disciplina'], $idProjeto);
                if(!isset($arrReturn[$idGradeHoraria]['locais'][$row['id_turma']])){
                    $arrReturn[$idGradeHoraria]['locais'][$row['id_turma']] = array(
                        'id_unidade' => $row['id_entidade'],
                        'st_unidade' => $row['st_nomeentidade'],
                        'st_corunidade' => $configEntidade['st_corentidade'],
                    );
                }

                //busca os turnos
                $turmaNegocio = new Turma();
                $resultTurmaTurno = $turmaNegocio->retornaTurmaTurnoAtivos(array('id_turma' => $row['id_turma']));
                if ($resultTurmaTurno) {
                    $resultTurmaTurno = $resultTurmaTurno[0];
                    if(!isset($arrReturn[$idGradeHoraria]['turnos'][$row['id_turma']])){
                        $arrReturn[$idGradeHoraria]['turnos'][$row['id_turma']] = array(
                            'id_turma' => $row['id_turma'],
                            'id_turno' => $resultTurmaTurno->getId_turno() ? $resultTurmaTurno->getId_turno()->getId_turno() : null,
                            'st_turno' => $resultTurmaTurno->getId_turno() ? $resultTurmaTurno->getId_turno()->getSt_turno() : null
                        );
                    }
                }

                /*
                 * calcula o inicio da semana baseado na data de inicio da grade, se a grade não começar na segunda
                 * subtrai os dias da semana até a segunda e seta a nova data de inicio
                 */
                $dt_iniciogradehoraria = clone $row['dt_iniciogradehoraria'];
                if ($row['dt_iniciogradehoraria']->format('w') != 1) {
                    $diaSemanaIni = 1;
                    $diaSemanaData = (int)$row['dt_iniciogradehoraria']->format('w');
                    $dt_iniciogradehoraria->modify('-' . ($diaSemanaData - $diaSemanaIni) . ' day');
                }

                //calcula os dias baseado na data de inicio e fim e retorna uma array com esses dias
                $intervaloDatas = $this->retornaDatasDeUmIntervalo($dt_iniciogradehoraria, $row['dt_fimgradehoraria']);

                //monta as posições dos dias da semana
                if ($intervaloDatas) {

                    //percorre os dias
                    foreach ($intervaloDatas as $keyDia => $dia) {


                        //monta o array com os dias da semana para dentro de cada dia colocar quais sao as aulas do dia
                        $nuDiaSemana = $dia->toString(\Zend_Date::WEEKDAY_DIGIT);
                        if(!isset($arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana])) {
                            $arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana] = array(
                                'dataCompleta' => $dia->toString('dd-M-Y'),
                                'data' => $dia->toString('dd/') . $dia->toString(\Zend_Date::MONTH_NAME_SHORT),
                                'diaSemana' => $dia->toString(\Zend_Date::WEEKDAY),
                                'nuDiaSemana' => $nuDiaSemana,
                                'aulas' => array()
                            );
                        }

                        //verifica a data do dia da semana da grade com o calculado e atribui o valor no array se o dia for igual
//                        foreach ($result as $keyGrade => $grade) {
                            if ($idGradeHoraria == $row['id_gradehoraria']) {
                                if(!isset($arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana]['aulas'][$row['id_turma']])){
//                                    $arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana]['aulas'][$grade['id_turma']] = array();
                                    $arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana]['aulas'][$row['id_turma']] = array();
                                }
                                if ($dia->toString('dd/MM/Y') == $row['dt_diasemana']->format('d/m/Y')) {
                                    $configEntidade = $this->retornarCalculoCarcaHorariaEncontro($row['id_entidade'], $row['id_disciplina'], $row['id_projetopedagogico']);
                                    $row['nu_totalencontros'] = $configEntidade['nu_totalencontros'];
                                    $arrReturn[$idGradeHoraria]['diasSemana'][$nuDiaSemana]['aulas'][$row['id_turma']][] = $row;
                                }
                            }
//                        }
                    }
                }

            }
//\Zend_Debug::dump($arrReturn);
//exit;
            //ordena os dias da semana pela menor data
            foreach ($arrReturn as &$valueArrReturn) {
                $diasSemana = array();
                foreach ($valueArrReturn['diasSemana'] as $value) {
                    $chave = strtotime($value['dataCompleta']);
                    $diasSemana[$chave] = $value;
                }
                $valueArrReturn['diasSemana'] = $diasSemana;
            }

        }
        return $arrReturn;
    }

    public function retornarImprimiGradeHorariaCombo($params)
    {
        try {
            //verifica se existe id_entidade, se não remove para trazer todos
            if (array_key_exists('id_unidade',$params) && !$params['id_unidade']) {
                unset($params['id_unidade']);
            }

            if (
                isset($params['dt_iniciogradehoraria']) && !empty($params['dt_iniciogradehoraria']) &&
                isset($params['dt_fimgradehoraria']) && !empty($params['dt_fimgradehoraria'])
            ) {
                //Converte o formato da data para pesquisa no banco
                $params['dt_iniciogradehoraria'] = $this->converteDataBanco($params['dt_iniciogradehoraria'] . ' 00:00:00');
                $params['dt_fimgradehoraria'] = $this->converteDataBanco($params['dt_fimgradehoraria'] . ' 23:59:59');
            }

            //faz a consulta nos itens retornando tod
            $retornaItens = $this->em->getRepository($this->repositoryItemName)->retornarPesquisaItemGradeHoraria($params);

            $retornaGrade = array();
            foreach ($retornaItens as $item) {
                //verifica se já inseriu aquela unidade no array
                if (!array_key_exists($item['id_gradehoraria'], $retornaGrade)) {
                    $id_grade = $item['id_gradehoraria'];

                    $retornaGrade[$id_grade]['id_gradehoraria'] = $id_grade;
                    $retornaGrade[$id_grade]['st_nomegradehoraria'] = $item['st_nomegradehoraria'];
                    $retornaGrade[$id_grade]['st_unidades'] = array();

                    //monta a string para o periodo
                    $periodo = '';

                    //verifica se a data inicial é uma instancia de DateTime e monta um pedaço da string
                    if ($item['dt_iniciogradehoraria'] instanceof \DateTime) {
                        $dt_inicio = $item['dt_iniciogradehoraria'];
                        $periodo .= $dt_inicio->format('d/m/Y');
                    }

                    //verifica se a data fim é uma instancia de DateTime e monta outro pedaço da string
                    if ($item['dt_fimgradehoraria'] instanceof \DateTime) {
                        $dt_fim = $item['dt_fimgradehoraria'];
                        $periodo .= ' a ';
                        $periodo .= $dt_fim->format('d/m/Y');
                    }

                    //atribui a string na chave do array
                    $retornaGrade[$id_grade]['st_periodo'] = $periodo;
                }

                //Verifica se a unidade não esta
                if (!in_array($item['st_unidades'], $retornaGrade[$id_grade]['st_unidades'])) {
                    $retornaGrade[$id_grade]['st_unidades'][] = $item['st_unidades'];
                }
            }
            array_multisort($retornaGrade);

            foreach ($retornaGrade as &$value) {
                $value['st_unidades'] = implode(', ', $value['st_unidades']);
            }
            return $retornaGrade;

        } catch (\Exception $e) {
            throw new \Zend_Exception($e->getMessage());
        }
    }

    /**
     * Método que orderna os encontros de acordo com a quantidade de itens que existem relacionados
     * a grade e a disciplina
     *
     * @param $dados
     * @throws \Zend_Exception
     */
    public function ordenaEncontrosAction($dados)
    {
        //busca todos os dados
        $return = $this->findBy($this->repositoryItemName, $dados, array('dt_diasemana' => 'asc'));

        //efetua toda a ordenação, respeitando encontros de turmas diferentes com disciplinas iguais
        $arr_ordena = array();
        foreach ($return as $key=>$value){
            $dt_data = $value->getDt_diasemana()->format('Y/m/d');
            $id_disciplina = $value->getId_disciplina()->getId_disciplina();
            $id_diasemana = $value->getId_diasemana()->getId_diasemana();
            $id_turnoaula = $value->getId_turnoaula()->getId_turno();
            
            //Verifica se existe index no array com esse id de disciplina
            if(array_key_exists($id_disciplina, $arr_ordena) && $arr_ordena[$id_disciplina]){

                // Verifica se já existe a data no array de disciplinas
                if (array_key_exists($dt_data, $arr_ordena[$id_disciplina]['data'])) {
                    //Verifica se existe index no array com id da disciplina correspondente com o mesmo id do dia da semana
                    if (array_key_exists($id_diasemana, $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'])) {

                        //verifica se existe index no array com id do mesmo turno de aula
                        if (array_key_exists($id_turnoaula, $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'])) {
                            array_push($arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula], $value);
                        } else {
                            $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula] = array();
                            array_push($arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula], $value);
                        }
                        //insere no array na disciplina correspondente, no dia da semana correpondente
                    } else {
                        //se não houver dia da semana na disciplina correspondente, cria uam posição e insere o elemento.
                        $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana] = array();
                        $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'] = array();
                        $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula] = array();
                        array_push($arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula], $value);
                    }
                } else {
                    //Caso não exista inicializa um novo ramo no array;
                    $arr_ordena[$id_disciplina]['data'][$dt_data] = array();
                    $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'] = array();
                    $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana] = array();
                    $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'] = array();
                    $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula] = array();
                    array_push($arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula], $value);
                }
            }else{
                //Caso não exista index com o id da disciplina no array, inicializa, criando um array com o id da disciplina e o dia da semana.

                $arr_ordena[$id_disciplina] = array();
                $arr_ordena[$id_disciplina]['data'] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula] = array();
                $arr_ordena[$id_disciplina]['data'][$dt_data]['id_disciplina'] = $id_disciplina;
                array_push($arr_ordena[$id_disciplina]['data'][$dt_data]['diasemana'][$id_diasemana]['turnoaula'][$id_turnoaula], $value);
            }
        }
        //reseta os index principais do array
        $arr_ordena = array_values($arr_ordena);

        //reseta os index dos dias das semanas.
        foreach ($arr_ordena as $key=>$disciplina){
            $arr_ordena[$key]['data'] = array_values($disciplina['data']);
        }


        //insere o numero do encontro em cada elemento, respeitando a ordem de dias da semana e itens com disciplinas iguais
        $this->beginTransaction();
        $lastkey = 0;
        foreach ($arr_ordena as $disciplina){
            foreach ($disciplina['data'] as $data){
                foreach ($data['diasemana'] as $diasemana){
                    foreach ($diasemana['turnoaula'] as $turnoaula) {
                        $lastkey++;
                        foreach ($turnoaula as $value){
                            $value->setNu_encontro($lastkey);
                            $this->save($value);
                        }
                    }
                }
            }
        }

        $this->commit();
    }

    /**
     * Método que busca e monta os arrays com as informações para cada turma, usando a vw_controleturma
     * em uma única consulta traz todos os dados para serem tratados
     *
     * @param $params
     * @return mixed
     */
    public function relatorioControleTurmaGradeHoraria($params)
    {

        /*
         * Busca os dados sempre ordenando pela Data de inicio da grade para que a montagem da grande no front fique crescente
         */
        $retornaItens = $this->findBy($this->repositoryNameVwControleTurma, $params, array('dt_iniciogradehoraria' => 'asc'));
        $gradeTurma = array();

        foreach ($retornaItens as $key => $itensObj) {

            /*
             * Cria algumas chaves que serão utilizados na montagem do array
             */
            $keyTurma = $itensObj->getId_turma();
            $keyGrade = $itensObj->getId_gradehoraria();
            $keyModulo = $itensObj->getId_modulo();
            $keyDisciplina = $itensObj->getId_disciplina();

            if (isset($keyGrade) && !empty($keyGrade)) {

                /*
                 * Método de montagem do array da turma
                 * O laço do foreach vai passar dentro dessa condição somente na primeira vez
                 * ele monta o primeiro array da turma com os módulos e disciplinas
                 */
                if (!array_key_exists($keyTurma, $gradeTurma)) {
                    $gradeTurma[$keyTurma]['id_turma'] = $keyTurma;
                    $gradeTurma[$keyTurma]['id_projetopedagogico'] = $itensObj->getId_projetopedagogico();
                    $gradeTurma[$keyTurma]['st_projetopedagogico'] = $itensObj->getSt_projetopedagogico();
                    $gradeTurma[$keyTurma]['st_turno'] = $itensObj->getSt_turno();
                    $gradeTurma[$keyTurma]['st_turma'] = $itensObj->getSt_turma();
                    $gradeTurma[$keyTurma]['dt_inicio'] = $itensObj->getDt_inicio()->format('d/m/Y');
                    $gradeTurma[$keyTurma]['dt_fim'] = $itensObj->getDt_fim()->format('d/m/Y');
                    $gradeTurma[$keyTurma]['st_codigo'] = $itensObj->getSt_codigo();
                    $gradeTurma[$keyTurma]['nu_cargahorariaprojeto'] = $itensObj->getNu_cargahorariaprojeto();
                    $gradeTurma[$keyTurma]['nu_cargahorariadisciplina'] = $itensObj->getNu_cargahorariadisciplina();
                    $gradeTurma[$keyTurma]['nu_cargahorariadisciplinaTotal'] = 0;
                    $gradeTurma[$keyTurma]['nu_cargahorariagrade'] = $itensObj->getNu_cargahorariagrade();
                    $gradeTurma[$keyTurma]['nu_cargarestante'] = $itensObj->getNu_cargarestante();

                    $gradeTurma[$keyTurma]['modulos'][$keyModulo] = array(
                        'id_modulo' => $keyModulo,
                        'st_modulo' => $itensObj->getSt_modulo()
                    );


                    $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina] = array(
                        'id_gradehoraria' => $keyGrade,
                        'st_disciplina' => $itensObj->getSt_disciplina(),
                        'st_turma' => $itensObj->getSt_turma(),
                        'nu_cargahorariadisciplina' => $itensObj->getNu_cargahorariadisciplina(),
                        'st_professor' => $itensObj->getSt_professor(),
                        'nu_cargahorariagrade' => $itensObj->getNu_cargahorariagrade(),
                        'nu_cargahorariaaulasministradas' => 0,
                        'nu_cargahorariaaulasrestantes' => $itensObj->getNu_cargahorariadisciplina(),
                        'nu_cargarestante' => $itensObj->getNu_cargarestante(),
                        'aulasminitradas' => array()
                    );
                    $gradeTurma[$keyTurma]['nu_cargahorariadisciplinaTotal'] += $itensObj->getNu_cargahorariadisciplina();
                }
                /*
                 * Fim método de montagem do array da turma
                 */

                /*
                 * Método de inserção de modulos do projeto pedagógico
                 * Após o array de turma montado ele vai continuar o laço verificando se na turma indicada toda a quantidade módulos cadastrados no
                 * projeto pedagógico foi inserida no array da turma
                 */
                if (!array_key_exists($keyModulo, $gradeTurma[$keyTurma]['modulos'])) {
                    $gradeTurma[$keyTurma]['modulos'][$keyModulo]['id_modulo'] = $keyModulo;
                    $gradeTurma[$keyTurma]['modulos'][$keyModulo]['st_modulo'] = $itensObj->getSt_modulo();
                    $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'] = array();
                }
                /*
                 * Fim método de insersão de modulos do projeto pedagógico
                 */

                /*
                 * Método de inserção de disciplina no módulo
                 * Após montar o módulo cadastrado no array ele vai verificar se todas as disciplinas do módulo foi inserida no array de turma
                 */
                if (!array_key_exists($keyDisciplina, $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'])) {
                    $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina] = array(
                        'id_gradehoraria' => $keyGrade,
                        'st_disciplina' => $itensObj->getSt_disciplina(),
                        'st_turma' => $itensObj->getSt_turma(),
                        'nu_cargahorariadisciplina' => $itensObj->getNu_cargahorariadisciplina(),
                        'st_professor' => $itensObj->getSt_professor(),
                        'nu_cargahorariagrade' => $itensObj->getNu_cargahorariagrade(),
                        'nu_cargahorariaaulasministradas' => $itensObj->getNu_cargahorariagrade(),
                        'nu_cargarestante' => $itensObj->getNu_cargarestante(),
                        'nu_cargahorariaaulasrestantes' => $itensObj->getNu_cargahorariadisciplina(),
                    );
                    $gradeTurma[$keyTurma]['nu_cargahorariadisciplinaTotal'] += $itensObj->getNu_cargahorariadisciplina();
                }
            }
        }

        /*
         * Metodo de montagem de grid
         */
        $gradeTurmaCompleto = $this->montaGridControleTurma($gradeTurma, $retornaItens);
        return $gradeTurmaCompleto;
    }

    /**
     * Método que monta a grid
     * Este método verifica quantas grade horaria foram geradas para a turma e quantas aulas de cada disciplina teve
     * em cada grade e add a aula no array da grade na disciplina
     * @param $gradeTurma
     * @param $retornaItens
     * @return mixed
     */
    public function montaGridControleTurma($gradeTurma, $retornaItens)
    {

        /*
         * Monta a grade horaria geral
         * Pega todas as grades de insere dentro de um array add essas grades no array da turma
         */
        foreach ($retornaItens as $key => $itensObj) {
            $keyTurma = $itensObj->getId_turma();
            $keyGrade = $itensObj->getId_gradehoraria();

            if (isset($keyGrade) && !empty($keyGrade)) {
                $gradeTurma[$keyTurma]['gradehoraria'][$keyGrade] = array(
                    'id_gradehoraria' => $itensObj->getId_gradehoraria(),
                    'dt_iniciogradehoraria' => $itensObj->getDt_iniciogradehoraria()->format('d/m'),
                    'dt_fimgradehoraria' => $itensObj->getDt_fimgradehoraria()->format('d/m'),
                    'nu_totalAulasMinistradas' => 0
                );
            }
        }

        /*
         * Método de interação de aulas ministradas em cada disciplina, caso tenha tido aula nessa disciplina
         * para a determinada grade, ele vai inserir o valor de uma nu_cargahorariagrade
         */
        foreach ($retornaItens as $key => $itensObj) {

            $keyTurma = $itensObj->getId_turma();
            $keyGrade = $itensObj->getId_gradehoraria();
            $keyModulo = $itensObj->getId_modulo();
            $keyDisciplina = $itensObj->getId_disciplina();

            if (isset($keyGrade) && !empty($keyGrade)) {
                /*
                 * laço para verificar em todas as grades se houveram aulas caso tenha intera o valor de uma nu_cargahorariagrade, ou cria um
                 * item com valor 0
                 */
                foreach ($gradeTurma[$keyTurma]['gradehoraria'] as $k => $gradehoraria) {
                    if ($keyGrade == $k) {
                        //aqui intera a quantidade de aulas que a turma teve
                        $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina]['aulasministradas'][$k][] = $itensObj->getNu_cargahorariagrade();
                        //aqui faz uma operação para ter o valor das aulas restantes da turma em uma materia
                        $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina]['nu_cargahorariaaulasrestantes'] = $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina]['nu_cargahorariaaulasrestantes'] - $itensObj->getNu_cargahorariagrade();
                        //aqui é o totalizador para saber a garga horaria total da turma em todas as disciplinas
                        $gradeTurma[$keyTurma]['gradehoraria'][$keyGrade]['nu_totalAulasMinistradas'] += $itensObj->getNu_cargahorariagrade();
                    } else {
                        /*
                         * insere valor 0 para que o array vá com a quantidade de grades mesmo que não tenha tido aula,
                         * assim podemos montar as td vazias para completar a grid
                         */
                        $gradeTurma[$keyTurma]['modulos'][$keyModulo]['disciplinas'][$keyDisciplina]['aulasministradas'][$k][] = 0;
                    }
                }
            }
        }
        return $gradeTurma;
    }
}

<?php

namespace G2\Negocio;


class FinalidadeCampanha extends Negocio
{

    private $repositoryName = '\G2\Entity\FinalidadeCampanha';


    public function __construct()
    {
        parent::__construct();
    }

    public function retornaFinalidadesCampanhaAtivas($id = '') {
        if ($id) {
            $results = $this->findBy($this->repositoryName, array('id_finalidadecampanha' => $id, 'bl_ativo'=> 1));
            return $this->converteEntityParaArray($results);
        } else {
            $results = $this->findBy($this->repositoryName, array('bl_ativo' => 1));
            return $this->converteEntityParaArray($results);
        }
    }

    public function converteEntityParaArray($results) {

        $arrReturn = [];

        foreach ($results as $result) {
            $arrReturn[] = array( 'id_finalidadecampanha' => $result->getId_finalidadecampanha(),
                                  'st_finalidadecampanha' => $result->getSt_finalidadecampanha(),
                                  'bl_ativo'              => $result->getBl_ativo());
        }

        return $arrReturn;
    }

}
<?php

namespace G2\Negocio;
use G2\Entity\AreaEntidade as AreaEntidadeEntity;

/**
 * Classe de negócio para Entity Situacao
 * @author Denise Xavier <denise.xavier@unyleya.com.br>
 */
class AreaEntidade extends Negocio
{

    private $repositoryName = 'G2\Entity\AreaEntidade';

    public function __construct ()
    {
        parent::__construct();
    }

    public function findAllAreaEntidade ()
    {
        return $this->findAll($this->repositoryName);
    }

    public function getReferenceArea ($idAreaConhecimento)
    {
        return $this->getReference($this->repositoryName, $idAreaConhecimento);
    }


    /**
     *
     * @param array $data
     * @param integer $id_areaconhecimento
     */
    public function salvarAreaEntidade (array $data, $id_areaconhecimento )
    {
        try{

            foreach($data as $entidade){
                $entity = new AreaEntidadeEntity();
                //seta os atributos
                $entity->setId_entidade($entidade)
                    ->setId_areaconhecimento($id_areaconhecimento)
                ;
                $this->save($entity);
            }

            return array(
                'type' => 'success',
                'title' => 'Sucesso :)',
                'text' => 'Entidades vinculadas com Sucesso!'
            );

        }catch (Exception $e){
            return array(
                'type' => 'error',
                'title' => 'Erro',
                'text' => 'Erro ao vincular entidades com o sistema: '.$e->getMessage()
            );
        }


    }

    /**
     * @param integer $id
     * @return true
     */
    public function deletarAreaEntidade ($id_areaconhecimento)
    {
        $ids = $this->findBy('G2\Entity\AreaEntidade',array('id_areaconhecimento'=>$id_areaconhecimento));

        foreach($ids as $id){
            $this->em->remove($id);
            $this->em->flush();
        }

        return true;

    }

}
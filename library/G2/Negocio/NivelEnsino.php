<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Produto
 * @author Elcio Mauro Guimarães <elcioguimaraes@gmail.com>
 */
class NivelEnsino extends Negocio
{

    private $repositoryName = 'G2\Entity\NivelEnsino';

    /**
     * Retorna todas os Níveis de Ensino
     * @return \Ead1_Mensageiro
     */
    public function findAll ($repositoryName = null)
    {

        try {

            $result = $this->em->getRepository($this->repositoryName)
                    ->findAll();
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            if ($arrReturn) {
                $mensageiro = new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
            } else {
                $mensageiro = new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
            }
        } catch (Exception $exc) {
            $mensageiro = new \Ead1_Mensageiro($exc->getMessage(), \Ead1_IMensageiro::ERRO);
        }

        return $mensageiro;
    }

    /**
     * Find Nivel Ensino by Id
     * @param integer $id
     * @return Entity
     */
    public function findNivelEnsino ($id)
    {
        return $this->find($this->repositoryName, $id);
    }

    /**
     * Return \G2\Entity\VwSerieNivelEnsino
     * @param array $params
     * @return \G2\Entity\VwSerieNivelEnsino
     */
    public function findSerieNivelEnsino (array $params = array())
    {
        return $this->findBy('\G2\Entity\VwSerieNivelEnsino', $params);
    }

    /**
     * Find Serie by ID
     * @param integer $idSerie
     * @return Entity
     */
    public function findSerie ($idSerie)
    {
        return $this->find('\G2\Entity\Serie', $idSerie);
    }

    /**
     * Reescreve metodo getReference()
     * @param integer $id
     * @return G2\Entity\NivelEnsino
     * @author Kayo Silva <kayo.silva@unyleya.com.br> 2014-01-20
     */
    public function getReference ($id, $repositoryName = null)
    {
        return parent::getReference($this->repositoryName, $id);
    }

}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para Entity Situacao
 * @author Kayo Silva <kayo.silva@unyleya.com.br>
 */
class Situacao extends Negocio
{

    private $repositoryName = '\G2\Entity\Situacao';

    public function __construct ()
    {
        parent::__construct();
    }

    /**
     * Metodo que retorna array de entidades de Situacao por filtro.
     * @param array $where
     * @return type
     */
    public function retornaSituacaoFindBy (array $where = array())
    {
        $result = $this->findBy($this->repositoryName, $where);
        if ($result) {
            $arrReturn = array();
            foreach ($result as $row) {
                $arrReturn[] = $this->toArrayEntity($row);
            }

            return new \Ead1_Mensageiro($arrReturn, \Ead1_IMensageiro::SUCESSO);
        } else {
            return new \Ead1_Mensageiro(\Ead1_IMensageiro::MENSAGEM_NADA_ENCONTRADO, \Ead1_IMensageiro::AVISO);
        }
    }
    /*e te devolvendo a pergunta... Se depender de vc terá uma próxima?*/

    /**
     * @param integer $idSituacao
     * @param string $repositoryName
     * @return \G2\Entity\Situacao
     */
    public function getReference ($idSituacao, $repositoryName = NULL)
    {
        return parent::getReference($this->repositoryName, $idSituacao);
    }
    public function retornarSituacao(array $where = array())
    {
        return $this->findBy('\G2\Entity\Situacao', $where);
    }
    
}

<?php

namespace G2\Negocio;

/**
 * Classe de negócio para View (sys.views)
 * @author Caio Eduardo <caio.teixeira@unyleya.com.br>
 */
class View extends Negocio
{

    private $repositoryName = 'G2\Entity\View';

    public function findAllByNotSchemaId($where) {
        unset($where['module'], $where['controller'], $where['action']);

        /*
         * Montar os filtros para a Query
         */
        $conds = array();
        // Verificar se possui o parametro SCHEMA_ID e trazer os que forem DIFERENTE do que passado
        if (isset($where['schema_id'])) {
            $conds[] = "table.schema_id != '{$where['schema_id']}'";
            unset($where['schema_id']);
        }
        // Adicionar os outro parametros ao filto, verificando se são IGUAIS
        foreach ($where as $key => $cond) {
            $conds[] = "table.{$key} = '{$cond}'";
        }
        $conds = implode(" AND ", $conds);

        $query = "SELECT table FROM {$this->repositoryName} table WHERE {$conds} ORDER BY table.st_view ASC";

        $query = $this->em->createQuery($query);
        $results = $query->execute();

        $arrReturn = array();

        foreach ($results as $result) {
            $arrReturn[] = $this->toArrayEntity($result);
        }

        return $arrReturn;
    }


}
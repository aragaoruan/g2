<?php

namespace G2\Negocio;
use G2\Entity\AvaliacaoAgendamento;

/**
 * Classe de negócio para Provas por pólo
 * @author Denise Xavier <denise.xavier@unyleya.com.br>;
 */
class ProvasPolo extends Negocio {

    /**
     * Retorna pesquisa de provas por polo
     * @param array $params
     * @return bool
     * @throws Zend_Exception
     * @throws \Zend_Validate_Exception
     *
     * Modificado Por: Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function pesquisaProvasPolo(array $params = array())
    {
        try {

            //Verifica se há o id de um polo sendo passado como parâmetro para a pesquisa e se o valor não é zero, pois zero corresponde a todos.
            if(array_key_exists('id_aplicadorprova', $params) && $params['id_aplicadorprova'] != 0){
                if (!isset($params['id_aplicadorprova']) || empty($params['id_aplicadorprova'])) {
                    throw new \Zend_Exception('O aplicador (pólo) é obrigatório para a pesquisa!', 500);
                }
            }

            $repo = $this->em->getRepository('G2\Entity\VwProvasPorPolo');
            $array_search = array('vw.id_aplicadorprova', 'vw.st_aplicadorprova', 'vw.st_dtaplicacao', 'vw.hr_inicio', 'vw.hr_fim', 'vw.nu_vagasexistentes', 'vw.total_agendamentos', 'vw.nu_provafinal', 'vw.nu_provarecuperacao', 'vw.nu_restantes', 'vw.id_avaliacaoaplicacao');

            //Cria a Query utilizando DQL.

            $query = $repo->createQueryBuilder("vw")
                ->select('vw')
                ->where('1=1')
                ->orderBy('vw.dt_aplicacao');

            if (isset($params['id_aplicadorprova']) && $params['id_aplicadorprova']) {
                $query->andWhere("vw.id_aplicadorprova = {$params['id_aplicadorprova']}");
            }

            $dt_inicio = isset($params['dt_inicio']) && $params['dt_inicio'] ?  new \Zend_Date(implode('-', array_reverse(explode('/', $params['dt_inicio']))), \Zend_Date::ISO_8601) : false;
            $dt_termino = isset($params['dt_termino']) && $params['dt_termino'] ?  new \Zend_Date(implode('-', array_reverse(explode('/', $params['dt_termino']))), \Zend_Date::ISO_8601) : false;

            if ($dt_inicio && $dt_termino) {
                if ($dt_inicio->isLater($dt_termino)) {
                    return new \Ead1_Mensageiro('A data de início não deve ser maior que a data de término!', \Ead1_IMensageiro::AVISO);
                }
            }

            if ($dt_inicio) {
                $query->andWhere("vw.dt_aplicacao >= '{$dt_inicio->toString('yyyy-MM-dd')}'");
            }

            if ($dt_termino) {
                $query->andWhere("vw.dt_aplicacao <= '{$dt_termino->toString('yyyy-MM-dd')}'");
            }

            $query
                ->andWhere("vw.id_entidade = {$this->sessao->id_entidade}")
                ->andWhere('vw.dt_aplicacao IS NOT NULL');

            $result = $query->getQuery()->getResult();

            return $result;

        } catch (Zend_Exception $e) {
            $this->excecao = $e->getMessage();
            return $e->getMessage();
        }
    }

    /**
     * Retorna array no formato para relatorio de alunos por prova (lista de presença)
     * @param array $params
     * @return array|string
     *
     * Modificado Por: Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function pesquisaVwAlunosAgendamento($params = array())
    {
        try {
            $params['id_situacao'] = AvaliacaoAgendamento::AGENDADO;

            $repositoryName = 'G2\Entity\VwAvaliacaoAgendamento';

            $dados = $this->findBy($repositoryName, $params, array('id_tipodeavaliacao' => 'ASC', 'st_nomecompleto' => 'ASC'));

            $array_return = array();
            $id_tipoprova = NULL;

                $zd = new \Zend_Date();

                foreach($dados as $vw){
                    if($vw->getId_tipodeavaliacao() !== $id_tipoprova){
                        $id_tipoprova = $vw->getId_tipodeavaliacao();
                        if($vw->getId_tipodeavaliacao() == 0){
                            $st_tipoprova = 'Prova Final';
                        }else{
                            $st_tipoprova = 'Prova Recuperação '.$vw->getId_tipodeavaliacao();
                        }
                        $array_return[$vw->getId_tipodeavaliacao()]['st_email'] = $vw->getSt_email();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_projetopedagogico'] = $vw->getSt_projetopedagogico();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_nomecompleto'] = $vw->getSt_nomecompleto();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_tipoprova'] = $st_tipoprova;
                        $array_return[$vw->getId_tipodeavaliacao()]['st_dataaplicacao'] = $vw->getDt_aplicacao()->format('d/m/Y');
                        $array_return[$vw->getId_tipodeavaliacao()]['hr_inicio'] = $zd->set($vw->getHr_inicio())->toString('H:m');
                        $array_return[$vw->getId_tipodeavaliacao()]['hr_termino'] = $zd->set($vw->getHr_fim())->toString('H:m');
                        $array_return[$vw->getId_tipodeavaliacao()]['st_aplicadorprova'] = $vw->getSt_aplicadorprova();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_endereco'] = $vw->getSt_endereco();
                        $array_return[$vw->getId_tipodeavaliacao()]['nu_numero'] = $vw->getNu_numero();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_bairro'] = $vw->getSt_bairro();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_cidade'] = $vw->getSt_cidade();
                        $array_return[$vw->getId_tipodeavaliacao()]['sg_uf'] = $vw->getSg_uf();
                        $array_return[$vw->getId_tipodeavaliacao()]['st_telefoneaplicador'] = $vw->getSt_telefoneaplicador();
                    }
                    $array_return[$vw->getId_tipodeavaliacao()]['alunos'][]= $vw;

            }

            return $array_return;

        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * Relatório de aluno por prova (lista as disciplinas) Relatórios » Pedagógicos » Provas por pólo -> Relatório de alunoss
     * @param array $params
     * @return array|string
     *
     * Modificado Por: Rafael Bruno (RBD) <rafael.oliveira@unyleya.com.br>
     */
    public function relatorioAlunosAgendamento($params = array())
    {
        try {
            $response = array();

            $params['id_situacao'] = AvaliacaoAgendamento::AGENDADO;
            $avalAgend = $this->findBy('\G2\Entity\VwAvaliacaoAgendamento', $params);

            $ng_gerenciaprova = new \G2\Negocio\GerenciaProva();

            foreach ($avalAgend as $value) {
                $response[] = array(
                    'avaliacao' => $value,
                    'disciplinas' => $ng_gerenciaprova->getDisciplinasAgendamento($value->getId_avaliacaoagendamento())
                );
            }

            return $response;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

}

<?php
/**
 * Created by PhpStorm.
 * User: Gabriel Resende
 * Date: 12/03/15
 * Time: 14:54
 */

namespace G2\Negocio;

class QuantidadeTransacoesCartaoCredito extends Negocio
{

    public function __construct()
    {
        parent::__construct();
    }

    public function executaRelatorio($params = array()){
        $connection = $this->em->getConnection();
        $sel = "";
        if (!empty($params['id_entidade'])){
            $sel .= " AND id_entidade = :id_entidade";
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])){
            $sel .= " AND dt_cadastro BETWEEN :dt_inicio AND :dt_fim ";
        }elseif(!empty($params['dt_inicio'])){
            $sel .= " AND dt_cadastro > :dt_inicio ";
        }elseif(!empty($params['dt_fim'])){
            $sel .= " AND dt_cadastro < :dt_fim ";
        }

        $sql = "SELECT DISTINCT
                        id_entidade,
                        st_nomeentidade ,
                        COUNT(nu_visa) AS nu_visa,
                        COUNT(nu_master) AS nu_master,
                        COUNT(nu_amex) AS nu_amex,
                        COUNT(nu_elo) AS nu_elo,
                        COUNT(nu_totaltransacoes) AS nu_totaltransacoes,
                        SUM(nu_totalsomatorio) AS nu_totalsomatorio
                FROM    rel.vw_quantidade_transacoes_cartao_credito2
                WHERE   1 = 1 {$sel}
                GROUP BY st_nomeentidade, id_entidade
                ORDER BY st_nomeentidade";

        $statement = $connection->prepare($sql);
        if (!empty($params['id_entidade'])){
            $statement->bindValue('id_entidade', $params['id_entidade']);
        }

        if (!empty($params['dt_inicio']) && !empty($params['dt_fim'])){

            $dt_fim = (new \DateTime($params['dt_fim']))->format('d/m/Y');
            $dt_inicio = (new \DateTime($params['dt_inicio']))->format('d/m/Y');

            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        }elseif(!empty($params['dt_inicio'])){

            $dt_inicio = (new \DateTime($params['dt_inicio']))->format('d/m/Y');
            $statement->bindValue('dt_inicio', $dt_inicio->format('Y-m-d'));

        }elseif(!empty($params['dt_fim'])){

            $dt_fim = (new \DateTime($params['dt_fim']))->format('d/m/Y');
            $statement->bindValue('dt_fim', $dt_fim->format('Y-m-d'));

        }

        $statement->execute();
        $results = $statement->fetchAll();

        foreach($results as &$result){
            $result['nu_totalsomatorio'] = number_format($result['nu_totalsomatorio'], 2, ',', '.');
        }

        return $results;
    }
}
<?php
namespace util;

/**
 * cifra o texto informado usando uma chave de 256bits
 *
 * @author J. Augusto <augustowebd@gmail.com>
 * @exemple
 * <?php
 *     $passwd = 'qaz123';
 *     $plainText   = 'texto a ser cifrado';
 *
 *      # cifra o $plainText usando como chave $passwd
 *     $encripted   = \util\Cipher::encrypt($plainText, $passwd);
 *
 *      // decripta $encripted usando a mesma chave $passwd
 *     $decrypt     = \util\Cipher::decrypt($encripted, $passwd);
 * */
class Cipher
{
    const KEY_LEN = 256;

    /**
     * cifra o texto informado por $plainText usando como chave $secretKey
     *
     * @param string $plainText [description]
     * @param string $secretKey [description]
     * @return string
     * */
    public static function encrypt($plainText, $secretKey)
    {
        return base64_encode(
            AesCtr::encrypt(
                $plainText
                , $secretKey
                , self::KEY_LEN
            )
        );
    }

    /**
     * decifra o texto informado por $plainText usando como chave $secretKey
     *
     * @param string $plainText
     * @param string $secretKey
     * @return string
     * */
    public static function decrypt($encripted, $secretKey)
    {
        return AesCtr::decrypt(
                base64_decode($encripted)
                , $secretKey
                , self::KEY_LEN
            );
    }
}

<?php
/**
 * Classe de Controle de Sessao
 * @author eduardoromao
 */
class ControleSessao
{   

	/**
	 * Método que gera os dados de Sessão de Usuário para o Portal do Aluno
	 * @param array $usuario
	 */
    public static function setSessionUsuario($usuario){
		$p_usuario = new Zend_Session_Namespace('p_usuario');
		foreach($usuario as $dado => $valor){
			$p_usuario->$dado = $valor;
		}
    }
    
	/**
	 * Método que gera os dados de Sessão de Entidade para o Portal do Aluno
	 * @param array $entidade
	 */
    public static function setSessionEntidade($entidade){
		$p_entidade = new Zend_Session_Namespace('p_entidade');
		foreach($entidade as $dado => $valor){
			$p_entidade->$dado = $valor;
		}
		 if(file_exists(APPLICATION_REAL_PATH.'/layout/'.$p_entidade->id_entidade.'/layout.ok')):
		 	$p_entidade->layout = $p_entidade->id_entidade;
		 else:
		 	$p_entidade->layout = 'padrao';
		 endif;	
    }    
    
	/**
	 * Método que gera os dados de Sessão de perfil para o Portal do Aluno
	 * @param array $perfil
	 */
    public static function setSessionPerfil($perfil){
		$p_perfil = new Zend_Session_Namespace('p_perfil');
		foreach($perfil as $dado => $valor){
			$p_perfil->$dado = $valor;
		}
    }  

	/**
	 * Método que gera os dados de Sessão de projeto pedagógico para o Portal do Aluno
	 * @param array $projeto
	 */
    public static function setSessionProjetoPedagogico($projeto){
		$p_projetopedagogico = new Zend_Session_Namespace('p_projetopedagogico');
		foreach($projeto as $dado => $valor){
			$p_projetopedagogico->$dado = $valor;
		}
    }

	/**
	 * Método que gera os dados de Sessão gerais para o Portal do Aluno
	 * @param array $dados
	 */
    public static function setSessionGeral($dados){
		$geral = new Zend_Session_Namespace('geral');
    	foreach($dados as $chave => $valor){
    		if(!isset($geral->$chave)){
	    		$geral->$chave = $valor;
    		}
    	}
    }
    
    /**
     * Método Estatico que retorna a sessao de usuario
     */
    public static function getSessaoUsuario(){
    	return new Zend_Session_Namespace('p_usuario');
    }
    
    /**
     * Método Estatico que retorna a sessao de entidade
     */
    public static function getSessaoEntidade(){
    	return new Zend_Session_Namespace('p_entidade');
    }
    
    /**
     * Método Estatico que retorna a sessao de perfil
     */
    public static function getSessaoPerfil(){
    	return new Zend_Session_Namespace('p_perfil');
    }
    
    /**
     * Método Estatico que retorna a sessao de projeto pedagogico
     */
    public static function getSessaoProjetoPedagogico(){
    	return new Zend_Session_Namespace('p_projetopedagogico');
    }
    
    /**
     * Método Estatico que retorna a sessao geral
     */
    public static function getSessaoGeral(){
    	return new Zend_Session_Namespace('geral');
    }
}
?>